LHCB-DAQPIPE
============

[![build status](https://gitlab.cern.ch/ci/projects/3/status.png?ref=master)](https://gitlab.cern.ch/ci/projects/3?ref=master)
[![coverity status](https://scan.coverity.com/projects/8055/badge.svg)](https://scan.coverity.com/projects/lhcb-daqpipe-v2)

What it does
------------

LHCB-DAQPIPE (DAQ Protocol-Independent Performance Evaluator) is a small benchmark application to test  network fabrics for the future 
LHCb upgrade. It has to be able to support transport over the wall network at 100 GBit/s on each
link.

DAQPIPE emulates an event-builder based on a local area network. It allows using multiple network technologies
and event-building protocols (which amount to a certain "traffic-shaping" strategy). Technologies and protocols 
can be mixed in a plug-and-play fashion.

DAQPIPE emulates reading  data from input "cards", which in a real DAQ would be connected to the detector.
Data from an "event" are correlated and need to be brought together in a single place. The event-data are spread out 
over multiple nodes (~ 500 to 1000 for the LHCb DAQ upgrade) so DAQPIPE must aggregate all the parts of the
event before sending them to a "filter unit", where event-selection would be performed (this is not part of DAQPIPE anymore).


DAQPIPE is formed on top of 4 components :
 - The ReadoutUnit which has to read the data from detectors and send to BuilderUnit
 - The BuilerUnit which has to aggregate the data of an event and to send it to a FilterUnit.
 - The FilterUnit which has to select or not the event for storage (not currently represented in DAQPIPE)
 - The EventManager which is a sort of leader to manage the job dispatching between the ReadoutUnit and BuilderUnit.
 
You can find more deeper details in the `./doc` directory or a more updated version in the gitlab [wiki](https://gitlab.cern.ch/svalat/lhcb-daqpipe-v2/wikis/home).
 
Dependencies
------------

By default DAQPIPE depends on :

 - CMake (required, tested 2.8.11) for the build system : http://www.cmake.org/
 - MPICH Hydra launcher, required for TCP/UDP/verbs/OFI/PSM2 support (tested : 3.2.1) : https://www.mpich.org/downloads/
 - MPI required for use of MPI as network support. Tested is OpenMPI version 1.6.4.
 - Hwloc is optional but used to automatically bind the process close the the requested network device (to be enabled by config file).
 - OpenPA is experimental (don't use it by default) and permit to remove mutexes into GoChannels : http://trac.mpich.org/projects/openpa/. Tested version is 1.0.4.
 - Libfabric optional, to run on top of Omni-Path PSM and IB verbs, tested 1.2.0 : http://ofiwg.github.io/libfabric/
 
Embeded in sources (`./extern-deps`) :

 - gtest/gmock version 1.7.0 to run unit tests : https://code.google.com/p/googletest/
 - jsoncpp to load the config file : https://github.com/open-source-parsers/jsoncpp

How to compile
--------------

The basic way to compile DAQPIPE is just using `cmake` or `configure` without arguments :

```shell
	mkdir build
	cd build
	../configure
	make
```

So you can just run it in sequential mode :

```shell
	./src/daqpipe
```

Select your transport driver
----------------------------

DAQPIPE support several transport driver which can be selected at build time with `--with-transport=` :

 * MPI
 * LIBFABRIC, you will need to provide the libfabric path via `--with-libfabric` and hydra path with `--with-hydra`.
 * TCP, you will need to provide the hydra path with `--with-hydra`.
 * RAPIDIO, you will need to provide the rapidio path via `--with-libfabric` and hydra path with `--with-hydra`.
 * PSM2, you will need  to provide the PSM2 path via `--with-psm2` and hydra path with `--with-hydra`.
 * VERBS, you will need to provide the verbs path via `--with-verbs` and hydra path with `--with-hydra`.
 * UDP, you will need to provide the verbs path via `--with-verbs` and hydra path with `--with-hydra`.
 * LOCAL


About MPI support
-----------------

To use the MPI support you just need to setup the `cmake` `TRANSPORT` variable to `MPI` :

```shell
	mkdir build-mpi
	cd build-mpi
	../configure --with-transport=MPI
	make
```

Now you can play by running with :

```shell
	mpirun -np X ./src/daqpipe
```

Notice that you need at least 2 processes to make it run, one for the event manager and one for
BuilderUnit and ReadoutUnit which by default share the nodes.

If you need to change the compiler to be used, you can provide it with (might be better to do
it on a clean build directory) :

```shell
	../configure --with-mpicxx=mpicxx
```

About Hydra/PMI support
-----------------------

This is required to **libfabric**, **tcp**, **rapidio**, **infiniband** and **udp** network and provide process launching support.

To compile you will first need to install the hydra library from mpich, you can extract the source from their sources
on https://www.mpich.org/downloads/ :

```shell
	patch -p1 < ${DAQPIPE_SOURCE_DIR}/extern-deps/hydra-3.2.1.patch
	./configure --prefix={YOUR_PREFIX} --program-prefix=lhcb-
	make
	make install
```

If you are using mpich on the node, it is preferable to not interfere with his own hydra installation so
you might prefer to add prefix/postfix to the executable by using options on configure :

```shell
	./configure --program-prefix=lhcb-
	#or
	./configure --program-postfix=-lhcb
```

Then you need to say to DAQPIPE where it is (except if you place it in /usr or /usr/local) :

```shell
	mkdir build
	cd build
	../configure --with-hydra={YOUR_PREFIX} --with-hydra-mpiexec=lhcb-mpiexec
	make
```

To run now, just be sure to have exported the `PATH` variable to get the `mpiexec` command :

```shell
	#once for the session or put in you ~/.bashrc to not to every session
	export PATH={YOUR_PREFIX}/bin:$PATH
	lhcb-mpiexec -np 4 ./src/daqpipe
```

As an alternative you can look on usage of the internal launcher, in this case, you have to launch
by hand all the processes to make them connecting to a central server to coordinate them. This is more
like what will be used as a final system.

Configuration file
------------------

DAQPIPE can use a configuration file in JSON format loaded with `-f` parameter and following the given format. You can get
a dump of the current config file at exit with `-d` option in case this file is not updated. Remark that instead of a value you
can provide an array of value for each rank and it will automatically select the right one. If you want to change for only certain
nodes you can also provide an object with rank index as key.

```json
{
	"general": {
		"credits": 1,
		"ruStoredEvents": 1024,
		"eventRailMaxDataSize": 1048576,
		"collisionSize": 100,
		"runTimeout": 5,
		"bandwidthDelay": 2,
		"mode":"PULL",
		"gatherType": "sched", //sched | simple
		"dataSizeRandomRadio": 10,
		"dataSource": "dummy",
		"writeFile": false,
		"gatherTimer": 0,
		"startDelay": 1
	},
	"dataSource": {
		"amc40": {
			"packetMaxSize": 9216,
			"dataPort": 3600,
			"tfcPort": 3601,
			"lines": 2,
			"iface-rail1-1": "eth0",
			"iface-rail1-2": "eth1",
			"threads": 4
		}, 
		"pcie40": {
			"baseDeviceId": 0,
			"pollingDelay": 100,
			"hwRails": {
				"default": [0,1],
				"155": [0,2],
				"156": [1,3]
			}
		},
		"dummy": {
			"threads": 1,
			"realSize" : false,
			"realSizeMode": "grouped",
			"realSizeRandomSeed": 0
		}
	},
	"filter": {
		"credits": 1,
		"type": "local",
		"subFarms": 0,
		"farmSize": 0,
		"mode": "farms"
	},
	"gather":{
		"sched":{
			"parallel":2,
			"policy": "barrel"
		}
	},
	"debug": {
		"show": [] 
	},
	"testing": {
		"skipMemcpy": false,
		"skipMeta": false,
		"useUnitThreads": false,
		"useDriverThreads": false,
		"useLocalDriverThreads": true,
		"useTracer": false,
		"oneWay": false,
		"realisticMemcpyRate": 0,
		"sameSegment": false,
		"failureRate": 0,
		"customTopology": false,
		"dataChecking": false,
		"warmup": 0,
		"EventManagerAlone": true,
		"profiler": false,
		"keepLocalBw": false
	},
	"drivers": {
		"slots": {
			"maxOnFly": 18,
			"cmdRecver": 2,
			"maxRdmaRecv": 8,
			"maxRdmaSend": 8,
			"listenerSleep": 100
		},
		"libfabric": {
			"first port":4444,
			"port range": 1024,
			"wait sleep": 256,
			"ep type": "RDM",
			"iface": "",
			"rdma threashold": 1048576,
			"provider name": ""
		},
		"tcp" : {
			"iface" : "eth0",
			"ipv6" : false,
			"portBase" : 2000,
			"portRange" : 256,
			"rdmaChannels": 1,
			"rdmaThreads" : 8,
			"cmdThreads": 8
		},
		"udp": {
			"ipv6": false,
			"portBase": 2000,
			"portRange": 512,
			"cmdIface": "eth0",
			"rdmaIface": "eth0",
			"rdmaChannels": 4,
			"rdmaThreads": 4,
			"cmdThreads": 4,
			"packetSize": 9000,
			"delay": 1000,
			"kernelBuf": 1677721
		}, 
		"verbs": {
			"ipv6": false,
			"portBase": 2000,
			"portRange": 256,
			"iface": "ib0",
			"resolveTimeout": 5000,
			"listenBackLog": 128,
			"totalSGEs": 8
		}
	},
	"binding": {
		"netDevice": "eth0"
	},
	"topology": [
		[ "EM" ],
		[ "BU", "RU", "PU" ],
		[ "BU", "RU" ],
		[ "BU", "RU" ],
		[ "FM", "FU" ],
		[ "FU" ],
		[ "FM", "FU" ],
		[ "FU" ]
	],
	"writer": {
		"meps": 20,
		"filename": "lhcb-daq-%06lu.raw",
		"threads": 4,
		"maxBuffers": 32
	},
	"launcher": {
		"internal": {
			"server": "127.0.0.1",
			"port": 8086
		}
	},
	"monitor": {
		"ipv6": false,
		"clients": 2,
		"ip": "",
		"ratio": 1000
	}, 
	"filterUnitTCP": {
		"basePort": 9600,
		"portRange": 128,
		"threads": 4,
		"hosts": [ "localhost" ],
		"storage": 16
	}
}
```

Code coverage
-------------

As a developer if you want to run the code coverage on top of DAQPIPE unit tests, you can build
the project with options :

```shell
	../configure --enable-gcc-coverage
```

Then run and call coverage tools :

```shell
	make
	make test
	lcov -o out.info -c -d . && genhtml -o html out.info
```

Usage with Htopml
-----------------

DAQPIPE support integration with htopml (https://github.com/svalat/htopml) to provide some view
on the fly of what append inside the code. You can compile support with :

```shell
	../configure --with-htopml=PREFIX
```

Now to use it, just run daqpipe with the htopml wrapper. Htopml will automatically use the
modules inside daqpipe to provide specific pages.

```shell
	mpirun -np X htopml ./daqpipe
```

Now you can connect on a specific node to redirect port and connect your browser on http://localhost:8080 :

```shell
	ssh node -L8080:localhost:8080
```

Benchmarking
------------

If you want to run some benchmarking of daqpipe, here we provide several scripts to automate it and
explore config parameters. Look inside `benchmarking/runner`. First you will find a script to setup and
install all the deps. It also compiles daqpipe V1 & V2 to compare them. Create an empty directory, copy this
script in and run it.

```shell
	./setup.sh
```

Then you can run the full benchmark suite with :

```shell
	./run.sh
```

While it is running you can connect to the web GUI to get the online charts via http://localhost:8080 or using
ssh port forwarding if you are using it remotly.

If you want to update this config, check `config.json` for example to setup node number and parameter 
values to explore depending on your cluster. Can can also edit the global configuration file of daqpipe
into `benchmarking/lhcb-daqpipe-v2/runner/configs`.

You can get a web report to the browser into charts by using the files into `benchmarking/webreport`, see the dedicated 
README to see how to use it.

Licence
-------

This software is distributed under open source licence CeCILL-C : http://www.cecill.info/licences/Licence_CeCILL-C_V1-en.html 
which is close and compatible with LGPL.
