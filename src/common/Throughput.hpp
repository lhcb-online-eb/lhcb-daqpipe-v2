/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Daniel Campora - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_THROUGHPUT_HPP 
#define DAQ_THROUGHPUT_HPP

/********************  HEADERS  *********************/
#include <ostream>
#include "Timer.hpp"

/********************  NAMESPACE  *******************/
namespace DAQ
{

/*********************  CLASS  **********************/
/**
 * Help class to measure throughput into DAQPIPE.
**/
class Throughput : public Timer
{
	public:
		Throughput();
		void restart();
		void flush();
		void reg(unsigned long count,unsigned long size);
		float getBw() const;
		void setTime(double value);
		friend std::ostream & operator << (std::ostream & out,const Throughput & value);
	private:
		double getTime(void) const;
	private:
		/** Number of messages registered since start or flush to measure message rate. **/
		unsigned long count;
		/** Total size we have exchanged since start or flush. **/
		unsigned long size;
		/** for unit test **/
		double testTime;
};

}

#endif //DAQ_THROUGHPUT_HPP
