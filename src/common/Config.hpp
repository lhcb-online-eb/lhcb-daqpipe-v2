/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_OPTIONS_HPP
#define DAQ_OPTIONS_HPP

/********************  HEADERS  *********************/
//must be as first
#include "config.h"
//std
#include <string>
#include <chrono>
//from jsoncpp
#ifdef HAVE_JSON_CPP
	#include <json/json.h>
#endif //HAVE_JSON_CPP


/*******************  NAMESPACE  ********************/
namespace DAQ
{

/********************  MACRO  ***********************/
#define DAQ_MAX_UNITS_PER_PROCESS 8
#define DAQ_MAX_CREDIT 128
/**
 * This shift the first event ID to make the life simpler in the AMC40 driver.
 * AMC40 driver use 0 & 1 for internal usage.
**/
#define DAQ_FIRST_EVENT_ID 10

/********************  ENUM  ************************/
/**
 * Select the communication mode to use.
**/
enum Mode
{
	/** 
	 * In PUSH model, the EventManager units send query to the ReadoutUnit
	 * and then the ReadoutUnit units will push their results to the BuilderUnit.
	**/
	MODE_PUSH,
	/** 
	 * In PULL model, the EventManager units send query to the BuilderUnit
	 * and then the BuilderUnit units will query the datas from the ReadoutUnit.
	**/
	MODE_PULL,
	/**
	 * Same as bool but use only round robin and don't do load balancing through the
	 * central Event Manager. We still keep and Event Manager but it just manage
	 * profling and exit control.
	**/
	MODE_PULL_NO_EM,
};

/********************  GLOBALS  *********************/
extern const char * cstModeStr[3];

/********************  STRUCT  **********************/
/**
 * Structure to store the configuration of DAQPIPE.
**/
struct Config
{
	//functions
	Config();
	void parseArgs( int argc, const char* argv[], bool exitOnHelp = true );
	void check();
	void loadConfigFile(const std::string & filename);
	void loadConfigFile(Json::Value & configFile);
	void dumpConfigFile();
	std::string getAndSetConfigString( const std::string& path, const std::string & elementName, const std::string& defaultValue) const;
	std::string getAndSetConfigString( const std::string& path, const std::string & elementName, const char * defaultValue) const;
	int getAndSetConfigInt( const std::string& path, const std::string & elementName, int defaultValue) const;
	bool getAndSetConfigBool(const std::string& path, const std::string & elementName, bool defaultValue) const;
	uint64_t getAndSetConfigUInt64(const std::string& path, const std::string & elementName, uint64_t defaultValue) const;
	float getAndSetConfigFloat(const std::string& path, const std::string & elementName, float defaultValue) const;
	Json::Value & getConfig(const std::string & path) const;
	size_t getDefaultMetaDataSize() const;
	size_t getMaxDataPacketSize() const;
	int getEventCollisions() const;
	int localProcesses( int worldSize ) const;
	#ifdef HAVE_JSON_CPP
		/**
		* Give access to the config file for extra value to be taken in
		* optional components like network drivers.
		**/
		mutable Json::Value configFile;
	#endif //HAVE_JSON_CPP

	/** 
	 * Number of credit to attribute to each BuilderUnit (max number of event it 
	 * can process at same time) 
	**/
	int builderCredits;
	/**
	 * Number of credit to attribute to each FilterUnit (max number of event it 
	 * can process at same time)
	**/
	int filterCredits;
	/**
	 * Number of event stored into readout unit.
	**/
	int readoutUnitEvents;
	/** 
	 * Size of an event (taking account of the fact that an event is composed 
	 * of multiple collisions). 
	**/
	size_t eventRailMaxDataSize;
	/**
	 * size of a collision data (per RU, so to be multiple by 500 for full event)
	**/
	size_t collisionRailDataSize;
	/**
	 * Timout in second to limit the time of execution, use 0 for none.
	**/
	int runTimeout;
	/** Select the way to manage the exhange between ReadoutUnit and BuilderUnit **/
	Mode mode;
	/** Say if show list of verbose categoty at exit. **/
	bool showVerboseList;
	/** Delay between two print of the BU rates. **/
	float bandwidthDelay;
	/** Delay between two sends of balance notification. **/
	float balanceDelay;
	/** Do not transfert meta-data **/
	bool skipMeta;
	/** Enable tracine of network comm scheduling. **/
	bool useTracer;
	/** Dump config file into this file at end of exec. **/
	std::string dumpConfig;
	/** Enable usage of threads on units **/
	bool useUnitThreads;
	/** Enable usage of threads on drivers **/
	bool useDriverThreads;
	/** Enable usage of thread of local driver **/
	bool useLocalDriverThreads;
	/** Enable checking of data between data source and bu **/
	bool dataChecking;
	/** Radio to variate the random size of datas (max-(X*max/100)rank() with X the parameter) **/
	size_t dataSizeRandomRatio;
	/** Number of filter subfarm (0 to disable) **/
	int filterSubfarm;
	/** Size of a subfarm (0 to disable) **/
	int subFarmSize;
	/** Farm mode (local | farms) **/
	std::string farmMode;
	/** Select the datasource type **/
	std::string dataSource;
	/* Launcher rank for internal launcher */
	int launcherRank;
	/** List of bool to know which process is on the same node **/
	bool * localMap;
	/** Use timings and use this value to wait between two gather requests **/
	uint64_t gatherTimer;
	/** Keep a reference time shared at startup when timing is enabled **/
	std::chrono::high_resolution_clock::duration::rep refTime;
	/** Number of events stored into BU & FU (multiple of credits) **/
	size_t builderStored;
	/** Keep track of rank for array parameters **/
	int rank;
	/** Leep local transfers **/
	bool keepLocalBw;
};

}

#endif //DAQ_OPTIONS_HPP
