/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Daniel Campora - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include "Throughput.hpp"
#include "FormattedMessage.hpp"

/********************  NAMESPACE  *******************/
namespace DAQ
{

/*******************  FUNCTION  *********************/
/**
 * Constructor of Throughput class, setup all internal counters to 0.
**/
Throughput::Throughput ()
{
	this->count = 0;
	this->size = 0;
	this->testTime = 0.0;
}

/*******************  FUNCTION  *********************/
/**
 * Flush the counter to 0 and automatically flush the internal timer.
**/
void Throughput::flush ()
{
	this->count = 0;
	this->size = 0;
	Timer::flush();
}

/*******************  FUNCTION  *********************/
/**
 * Register to datas.
 * @param count Number of messages.
 * @param size Total size of the exchanged messages.
**/
void Throughput::reg ( long unsigned int count, long unsigned int size )
{
	this->count += count;
	this->size += size;
}

/*******************  FUNCTION  *********************/
float Throughput::getBw() const
{
	float bw = (float)(8*size) / getTime();
	return bw;
}

/*******************  FUNCTION  *********************/
/**
 * Manage serialization to output stream by taking care of units (Kb,Mb...)
 * @param out Output stream to use
 * @param value Current instance to print into output stream.
 * @return Return the given output stream.
**/
std::ostream & operator<< ( std::ostream& out, const Throughput& value )
{
	FormattedMessage message("%1 , %2");
	float rate = (float)value.count / value.getTime();
	float bw = (float)(8*value.size) / value.getTime();
	message.argUnit1000(rate,"msg/s");
	message.argUnit1000(bw,"b/s",3);
	out << message;
	return out;
}

/*******************  FUNCTION  *********************/
void Throughput::setTime ( double value )
{
	this->testTime = value;
}

/*******************  FUNCTION  *********************/
double Throughput::getTime ( void ) const
{
	if (testTime == 0.0)
		return getElapsedTime();
	else
		return testTime;
}

/*******************  FUNCTION  *********************/
/**
 * Stop, flush and restart all the internal counters.
**/
void Throughput::restart ()
{
	stop();
	flush();
	start();
}

}
