/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include <cerrno>
#include <gtest/gtest.h>
#include <common/Config.hpp>
#include <Debug.hpp>

/***************** USING NAMESPACE ******************/
using namespace DAQ;

/*******************  FUNCTION  *********************/
TEST(Config,constructor)
{
	Config config;
}

/*******************  FUNCTION  *********************/
TEST(Config,parseArgs)
{
	Config config;
	const char * argv[] = {
		"TestConfig",
		"-f",TEST_DATA_PATH "/config.json",
		"-t","155",
		"-c","4",
		"-v","mpi",
		"-m",
		"-d","dumped-config.json",
		"-h",
		nullptr
	};
	
	//reset getopt
	optind=0;
	
	//parse
	config.parseArgs(13,argv,false);
	
	EXPECT_EQ(4,config.builderCredits);
	EXPECT_EQ(2097152,config.eventRailMaxDataSize);
	//EXPECT_EQ(2048,config.eventMetaDataSize);
	EXPECT_EQ(512,config.readoutUnitEvents);
	EXPECT_EQ(155,config.runTimeout);
	EXPECT_EQ(MODE_PUSH,config.mode);
	EXPECT_TRUE(Debug::showCat("em"));
	EXPECT_TRUE(Debug::showCat("bu"));
	EXPECT_TRUE(Debug::showCat("ru"));
	EXPECT_TRUE(Debug::showCat("mpi"));
}

/*******************  FUNCTION  *********************/
TEST(Config,parseArgsLong)
{
	Config config;
	const char * argv[] = {
		"TestConfig",
		"--file",TEST_DATA_PATH "/config.json",
		"--timeout","155",
		"--credit","4",
		"--verbose","mpi",
		"--help",
		"--no-meta",
		"--dump-config","dumped-config.json",
		nullptr
	};
	
	//reset getopt
	optind=0;
	
	//parse
	config.parseArgs(13,argv,false);
	
	EXPECT_EQ(4,config.builderCredits);
	EXPECT_EQ(2097152,config.eventRailMaxDataSize);
	//EXPECT_EQ(2048,config.eventMaxMetaSize);
	EXPECT_EQ(512,config.readoutUnitEvents);
	EXPECT_EQ(155,config.runTimeout);
	EXPECT_EQ(MODE_PUSH,config.mode);
	EXPECT_TRUE(Debug::showCat("em"));
	EXPECT_TRUE(Debug::showCat("bu"));
	EXPECT_TRUE(Debug::showCat("ru"));
	EXPECT_TRUE(Debug::showCat("mpi"));
}

/*******************  FUNCTION  *********************/
TEST(Config,loadFile)
{
	Config config;
	config.loadConfigFile(TEST_DATA_PATH "/config.json");
	
	EXPECT_EQ(2,config.builderCredits);
	EXPECT_EQ(2097152,config.eventRailMaxDataSize);
	//EXPECT_EQ(2048,config.eventMaxMetaSize);
	EXPECT_EQ(512,config.readoutUnitEvents);
	EXPECT_EQ(10,config.runTimeout);
	EXPECT_EQ(MODE_PUSH,config.mode);
	EXPECT_TRUE(Debug::showCat("em"));
	EXPECT_TRUE(Debug::showCat("bu"));
	EXPECT_TRUE(Debug::showCat("ru"));
}

/*******************  FUNCTION  *********************/
TEST(Config,check)
{
	Config config;
	config.check();
}

/*******************  FUNCTION  *********************/
TEST(Config,getEventCollisions)
{
	Config config;
	int c = config.getEventCollisions();
	ASSERT_NE(c,0);
}

/*******************  FUNCTION  *********************/
TEST(Config,checkInvalid)
{
	Config config;
	config.runTimeout = -1;
	::testing::FLAGS_gtest_death_test_style = "threadsafe";
	ASSERT_DEATH(config.check(),"");
}

/*******************  FUNCTION  *********************/
TEST(Config,dumpConfigFile)
{
	Config config;
	const char * argv[] = {
		"TestConfig",
		"-d","dumped-config.json",
		nullptr
	};
	config.parseArgs(3,argv);
	config.dumpConfigFile();
}

/*******************  FUNCTION  *********************/
TEST(Config,getMaxDataPacketSize)
{
	Config config;
	EXPECT_NE(0,config.getMaxDataPacketSize());
}

/*******************  FUNCTION  *********************/
TEST(Config,getAndSetConfigString)
{
	Config config;
	std::string v1 = config.getAndSetConfigString("test","parent.child","value");
	std::string v2 = config.getAndSetConfigString("test","parent.child","v");
	
	EXPECT_EQ("value",v1);
	EXPECT_EQ("value",v2);
}

/*******************  FUNCTION  *********************/
TEST(Config,perRankConfigEntry)
{
	Config config;
	config.rank = 1;
	config.loadConfigFile(TEST_DATA_PATH "/config.json");
	std::string valueNone = config.getAndSetConfigString("perRank","entryNone","default");
	std::string valueOne = config.getAndSetConfigString("perRank","entryOne","default");
	std::string valueRank = config.getAndSetConfigString("perRank","entryRank","default");
	
	EXPECT_EQ("default",valueNone);
	EXPECT_EQ("value",valueOne);
	EXPECT_EQ("rank1",valueRank);
	
	config.rank = 2;
	std::string valueRank2 = config.getAndSetConfigString("perRank","entryRank","default");
	EXPECT_EQ("rank2",valueRank2);
}

/*******************  FUNCTION  *********************/
TEST(Config,perRankConfigEntryObject)
{
	Config config;
	config.loadConfigFile(TEST_DATA_PATH "/config.json");
	
	config.rank = 0;
	std::string value = config.getAndSetConfigString("perRank","entryRankObj","default");
	EXPECT_EQ("rank0",value);
	
	config.rank = 1;
	value = config.getAndSetConfigString("perRank","entryRankObj","default");
	EXPECT_EQ("defaultRank",value);
	
	config.rank = 2;
	value = config.getAndSetConfigString("perRank","entryRankObj","default");
	EXPECT_EQ("rank2",value);
}

/*******************  FUNCTION  *********************/
TEST(Config,perRankConfigEntryObject_2)
{
	Config config;
	config.loadConfigFile(TEST_DATA_PATH "/config.json");
	
	config.rank = 0;
	std::string value = config.getAndSetConfigString("perRank","entryRankObj2","default");
	EXPECT_EQ("rank0",value);
	
	config.rank = 1;
	value = config.getAndSetConfigString("perRank","entryRankObj2","default");
	EXPECT_EQ("default",value);
	
	config.rank = 2;
	value = config.getAndSetConfigString("perRank","entryRankObj2","default");
	EXPECT_EQ("rank2",value);
}
