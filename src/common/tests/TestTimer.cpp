/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include <cerrno>
#include <gtest/gtest.h>
#include "common/Timer.hpp"
#include <Debug.hpp>

/***************** USING NAMESPACE ******************/
using namespace DAQ;

/*******************  FUNCTION  *********************/
TEST(Timer,constructor)
{
	Timer timer;
}

/*******************  FUNCTION  *********************/
TEST(Timer,startStop_1)
{
	Timer timer;
	timer.start();
	timer.stop();
}

/*******************  FUNCTION  *********************/
TEST(Timer,getElapsedTime)
{
	Timer timer;
	timer.start();
	usleep(50000);
	EXPECT_LE(0.05,timer.getElapsedTime());
}

/*******************  FUNCTION  *********************/
TEST(Timer,getElapsedTimeLong)
{
	Timer timer;
	timer.start();
	usleep(50000);
	EXPECT_LE(50000,timer.getElapsedTimeLong());
}

/*******************  FUNCTION  *********************/
TEST(Timer,get_1)
{
	Timer timer;
	timer.start();
	usleep(50000);
	timer.stop();
	EXPECT_LE(0.05,timer.get());
}

/*******************  FUNCTION  *********************/
TEST(Timer,get_2)
{
	Timer timer;
	//first round
	timer.start();
	usleep(50000);
	timer.stop();
	
	//second round
	timer.start();
	usleep(50000);
	timer.stop();
	
	//check
	EXPECT_LE(0.10,timer.get());
	EXPECT_LE(100000,timer.getLong());
}

/*******************  FUNCTION  *********************/
TEST(Timer,flush)
{
	Timer timer;
	timer.start();
	usleep(50000);
	timer.stop();
	EXPECT_LE(0.05,timer.get());
	timer.flush();
	EXPECT_EQ(0,timer.get());
}
