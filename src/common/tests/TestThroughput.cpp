/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include <cerrno>
#include <gtest/gtest.h>
#include "common/Throughput.hpp"

/***************** USING NAMESPACE ******************/
using namespace DAQ;

/*******************  FUNCTION  *********************/
TEST(Throughput,constructor)
{
	Throughput throughput;
	throughput.setTime(1.0);
}

/*******************  FUNCTION  *********************/
TEST(Throughput,test_bits)
{
	Throughput throughput;
	throughput.setTime(1.0);
	throughput.start();
	throughput.reg(1,16);
	throughput.reg(1,16);
	std::stringstream out;
	out << throughput;
	//EXPECT_EQ("2.00 msg/s , 256.00 b/s",out.str());
	EXPECT_EQ("2.00 msg/s , 0.00 Gb/s",out.str());
}

/*******************  FUNCTION  *********************/
TEST(Throughput,test_kbits)
{
	Throughput throughput;
	throughput.setTime(1.0);
	throughput.start();
	throughput.reg(1,16*1000);
	throughput.reg(1,16*1000);
	std::stringstream out;
	out << throughput;
	//EXPECT_EQ("2.00 msg/s , 256.00 Kb/s",out.str());
	EXPECT_EQ("2.00 msg/s , 0.00 Gb/s",out.str());
}

/*******************  FUNCTION  *********************/
TEST(Throughput,test_mbits)
{
	Throughput throughput;
	throughput.setTime(1.0);
	throughput.start();
	throughput.reg(1,16*1000*1000UL);
	throughput.reg(1,16*1000*1000UL);
	std::stringstream out;
	out << throughput;
	//EXPECT_EQ("2.00 msg/s , 256.00 Mb/s",out.str());
	EXPECT_EQ("2.00 msg/s , 0.26 Gb/s",out.str());
}

/*******************  FUNCTION  *********************/
TEST(Throughput,test_gbits)
{
	Throughput throughput;
	throughput.setTime(1.0);
	throughput.start();
	throughput.reg(1,16*1000*1000UL*1000UL);
	throughput.reg(1,16*1000*1000UL*1000UL);
	std::stringstream out;
	out << throughput;
	EXPECT_EQ("2.00 msg/s , 256.00 Gb/s",out.str());
}

/*******************  FUNCTION  *********************/
TEST(Throughput,test_tbits)
{
	Throughput throughput;
	throughput.setTime(1.0);
	throughput.start();
	throughput.reg(1,16*1000*1000UL*1000UL*1000UL);
	throughput.reg(1,16*1000*1000UL*1000UL*1000UL);
	std::stringstream out;
	out << throughput;
	//EXPECT_EQ("2.00 msg/s , 256.00 Tb/s",out.str());
	EXPECT_EQ("2.00 msg/s , 256000.00 Gb/s",out.str());
}

/*******************  FUNCTION  *********************/
TEST(Throughput,test_flush)
{
	Throughput throughput;
	throughput.setTime(1.0);
	throughput.start();
	throughput.reg(1,16*1000*1000UL*1000UL*1000UL);
	throughput.reg(1,16*1000*1000UL*1000UL*1000UL);
	
	//check
	std::stringstream out;
	out << throughput;
	//EXPECT_EQ("2.00 msg/s , 256.00 Tb/s",out.str());
	EXPECT_EQ("2.00 msg/s , 256000.00 Gb/s",out.str());
	
	//restart
	throughput.restart();
	
	throughput.reg(1,16*1000*1000UL*1000UL);
	throughput.reg(1,16*1000*1000UL*1000UL);
	
	std::stringstream out2;
	out2 << throughput;
	EXPECT_EQ("2.00 msg/s , 256.00 Gb/s",out2.str());
}
