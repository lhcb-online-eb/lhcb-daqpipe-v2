/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include <cerrno>
#include <gtest/gtest.h>
#include "common/TopoRessource.hpp"
#include "transport/TransportMT.hpp"
#include "launcher/LauncherLocal.hpp"
#include "drivers/DriverLocal.hpp"

/***************** USING NAMESPACE ******************/
using namespace DAQ;

/*******************  FUNCTION  *********************/
TEST(TopoRessource,constructor)
{
	Config config;
	LauncherLocal launcher(&config);
	DriverLocal driver;
	TransportMT transport(&config,&launcher,&driver);
	transport.setUnitType(0,0,DAQ::UNIT_BUILDER_UNIT);
	transport.setUnitType(0,1,DAQ::UNIT_BUILDER_UNIT);
	transport.setUnitType(0,3,DAQ::UNIT_BUILDER_UNIT);
	transport.setUnitType(0,4,DAQ::UNIT_BUILDER_UNIT);
	
	TopoRessource<int> topo(&transport,&config,10,UNIT_BUILDER_UNIT);
}

TEST(TopoRessource,fillAll)
{
	Config config;
	LauncherLocal launcher(&config);
	DriverLocal driver;
	TransportMT transport(&config,&launcher,&driver);
	transport.setUnitType(0,0,DAQ::UNIT_BUILDER_UNIT);
	transport.setUnitType(0,1,DAQ::UNIT_BUILDER_UNIT);
	transport.setUnitType(0,2,DAQ::UNIT_BUILDER_UNIT);
	transport.setUnitType(0,3,DAQ::UNIT_BUILDER_UNIT);
	
	TopoRessource<int> topo(&transport,&config,10,UNIT_BUILDER_UNIT);
	
	int cnt = 0;
	for (int unitId = 0 ; unitId < 4 ; unitId++)
		for (int i = 0 ; i < 10 ; i++)
			topo.set(0,unitId,i,cnt++);
		
	//check
	cnt = 0;
	for (int unitId = 0 ; unitId < 4 ; unitId++)
		for (int i = 0 ; i < 10 ; i++)
			EXPECT_EQ(cnt++,topo.get(0,unitId,i));
}

TEST(TopoRessource,reset)
{
	Config config;
	LauncherLocal launcher(&config);
	DriverLocal driver;
	TransportMT transport(&config,&launcher,&driver);
	transport.setUnitType(0,0,DAQ::UNIT_BUILDER_UNIT);
	transport.setUnitType(0,1,DAQ::UNIT_BUILDER_UNIT);
	transport.setUnitType(0,2,DAQ::UNIT_BUILDER_UNIT);
	transport.setUnitType(0,3,DAQ::UNIT_BUILDER_UNIT);
	
	TopoRessource<int> topo(&transport,&config,10,UNIT_BUILDER_UNIT);
	
	int cnt = 0;
	for (int unitId = 0 ; unitId < 4 ; unitId++)
		for (int i = 0 ; i < 10 ; i++)
			topo.set(0,unitId,i,cnt++);
	
	EXPECT_TRUE(topo.hasAllRessources());
	topo.reset(1);
	EXPECT_FALSE(topo.hasAllRessources());
		
	//check
	cnt = 0;
	for (int unitId = 0 ; unitId < 4 ; unitId++)
		for (int i = 0 ; i < 10 ; i++)
			EXPECT_EQ(1,topo.get(0,unitId,i));
}
