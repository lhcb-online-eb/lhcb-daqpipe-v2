/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
//std
#include <fstream>
#include <iostream>
#include <cstdlib>
#include <cassert>
//unix
#include <unistd.h>
#include <getopt.h>
//internals
#include "common/Debug.hpp"
#include "datasource/DataSource.hpp"
//current
#include "Config.hpp"

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/********************  GLOBALS  *********************/
/**
 * Convert protocole mode to string format.
**/
const char * cstModeStr[3] = {
	"PUSH",
	"PULL",
	"PULL_NO_EM"
};

/********************  GLOBALS  *********************/
/**
 * Define the help message to print on -h/--help or error.
**/
static const char cstHelpMessage[] = "Usage : daqpipe [OPTIONS]\n\
Options :·\n\
    -h, --help              Display help message.\n\
    -f, --file              JSON configuration file to load.\n\
    -t, --timeout {T}       Limit the execution to the given time in second.\n\
    -c, --credit {C}        Number of credit per BuilderUnit.\n\
    -m, --no-meta           Skip meta-data.\n\
    -v, --verbose {V1,V2..} Enable verbose mode and optionaly provide a filter.\n\
    -d, --dump-config {F}   Dump the config file into the given file at exit.\n\
    -r, --rank {R}          Rank of the process when using internal launcher\n";
/**
 * Define the lon option names.
**/
static struct option cstLongOptions[] = {
	{"help",0,nullptr,0},
	{"file",1,nullptr,0},
	{"timeout",1,nullptr,0},
	{"credit",1,nullptr,0},
	{"verbose",2,nullptr,0},
	{"no-meta",0,nullptr,0},
	{"dump-config",1,nullptr,0},
	{"rank",1,nullptr,0},
	{nullptr, 0, nullptr, 0}
};

/*******************  FUNCTION  *********************/
/**
 * Constructor of Config structuer to setup the default values.
**/
Config::Config ()
{
	this->localMap = nullptr;
	this->builderCredits = 1;
	this->filterCredits = 1;
	this->collisionRailDataSize = 100;
	this->eventRailMaxDataSize = 1024*1024;
	this->readoutUnitEvents = 128;
	this->runTimeout = 5;
	this->bandwidthDelay = 2;
	this->balanceDelay = 2;
	this->mode = MODE_PULL;
	this->showVerboseList = false;
	this->skipMeta = false;
	this->useTracer = false;
	this->dumpConfig = "";
	this->useUnitThreads = false;
	this->useDriverThreads = false;
	this->useLocalDriverThreads = false;
	this->dataSizeRandomRatio = 20;
	this->filterSubfarm = 0;
	this->subFarmSize = 0;
	this->farmMode = "none";
	this->dataSource = "dummy";
	this->launcherRank = -1;
	this->gatherTimer = 0;
	this->refTime = 0;
	this->builderStored = 20;
	this->keepLocalBw = false;
	#ifdef NDEBUG
		this->dataChecking = false;
	#else
		this->dataChecking = true;
	#endif

}

/*******************  FUNCTION  *********************/
size_t Config::getDefaultMetaDataSize () const
{
	int collisions = eventRailMaxDataSize / collisionRailDataSize;
	return EventMetaData::getTotalSize(collisions);
}

/*******************  FUNCTION  *********************/
/**
 * Parse the arguments passed to the main function as program parameters.
 * @param argc Number of arguments recieved.
 * @param argv List of arguments.
**/
void Config::parseArgs ( int argc, const char** argv, bool exitOnHelp )
{
	//vars
	int c;
	int optionIndex;
	
	//reset for unit tests
	optind = 1;

	//loop on all
	while((c = getopt_long(argc,(char**)argv,"hmf:t:c:v:d:r:",cstLongOptions,&optionIndex)) != -1) {
		//fallback long options
		if (c == 0)
		{
			switch(optionIndex)
			{
				case 0:
					c = 'h';
					break;
				case 1:
					c = 'f';
					break;
				case 2:
					c = 't';
					break;
				case 3:
					c = 'c';
					break;
				case 4:
					c = 'v';
					break;
				case 5:
					c = 'm';
					break;
				case 6:
					c = 'd';
					break;
				case 7:
					c = 'r';
					break;
				default:
					DAQ_FATAL_ARG("Invalid option index : %1").arg(optionIndex).end();
					break;
			}
		}
		
		//apply case
		switch(c) {
			case '?':
			case 'h':
				std::cout << cstHelpMessage;
				if (exitOnHelp)
					exit(0);
				break;
			case 'f':
				this->loadConfigFile(optarg);
				break;
			case 't':
				this->runTimeout = atoi(optarg);
				break;
			case 'c':
				this->builderCredits = atoi(optarg);
				this->filterCredits = atoi(optarg);
				break;
			case 'v':
				if (optarg == nullptr)
					Debug::enableAll();
				else if (std::string(optarg) == "help")
					showVerboseList = true;
				else
					Debug::setVerbosity(optarg);
				break;
			case 'm':
				this->skipMeta = true;
				break;
			case 'd':
				this->dumpConfig = optarg;
				break;
			case 'r':
				this->launcherRank = atoi(optarg);
				break;
			default:
				DAQ_FATAL_ARG("Invalid option : %1").arg(c).end();
				break;
		}
	}
	
	//sanity check
	this->check();
}

/*******************  FUNCTION  *********************/
size_t Config::getMaxDataPacketSize () const
{
	return eventRailMaxDataSize * collisionRailDataSize;
}

/*******************  FUNCTION  *********************/
/**
 * Methode to made sanity check on configure options
**/
void Config::check ()
{
	assume(builderCredits > 0 && builderCredits < DAQ_MAX_CREDIT,"Invalid builder credit number, out of bound");
	assume(filterCredits > 0 && filterCredits < DAQ_MAX_CREDIT,"Invalid filter credit number, out of bound");
	assume(eventRailMaxDataSize > 0,"Invalid event data size");
	assume(mode == MODE_PULL || mode == MODE_PUSH || mode == MODE_PULL_NO_EM,"Invalid mode, must be PULL or PUSH");
	assume(runTimeout >= 0,"Invalid timeout, must be positive");
	assume(bandwidthDelay >= 1,"Invalid bandwidthDelay, must be larger or equal to 1");
	assume(collisionRailDataSize > 0,"Invalid collisionSize, must be positive");
	assume(filterSubfarm >= 0, "Invalid sub farm number, must be positive");
	assume(filterSubfarm == 0 || subFarmSize > 0, "If have filter farm, farm size must be positive");
	assume(builderStored >= (size_t)builderCredits,"builderStored must be at least equal to credits");
}

/*******************  FUNCTION  *********************/
/**
 * Load the confif file from JSON tree and apply all the default json default value.
 * @param configFile Define the JSON tree to use to load the parameters.
**/
void Config::loadConfigFile ( Json::Value & configFile )
{
	#ifdef HAVE_JSON_CPP
		//load values
		this->builderCredits = getAndSetConfigInt("general","credits",this->builderCredits);
		this->readoutUnitEvents = getAndSetConfigInt("general","ruStoredEvents",this->readoutUnitEvents);
		this->eventRailMaxDataSize = getAndSetConfigUInt64("general","eventRailMaxDataSize",this->eventRailMaxDataSize);
		this->runTimeout = getAndSetConfigInt("general","runTimeout",this->runTimeout);
		this->bandwidthDelay = getAndSetConfigFloat("general","bandwidthDelay",this->bandwidthDelay);
		this->skipMeta = getAndSetConfigBool("testing","skipMeta",this->skipMeta);
		this->useTracer = getAndSetConfigBool("testing","useTracer",this->useTracer);
		this->useUnitThreads = getAndSetConfigBool("testing","useUnitThreads",this->useUnitThreads);
		this->useDriverThreads = getAndSetConfigBool("testing","useDriverThreads",this->useDriverThreads);
		this->useLocalDriverThreads = getAndSetConfigBool("testing","useLocalDriverThreads",this->useLocalDriverThreads);
		this->collisionRailDataSize = getAndSetConfigUInt64("general","collisionSize",this->collisionRailDataSize);
		this->dataChecking = getAndSetConfigBool("testing","dataChecking",this->dataChecking);
		this->dataSizeRandomRatio = getAndSetConfigInt("general","dataSizeRandomRatio",this->dataSizeRandomRatio);
		this->filterSubfarm = getAndSetConfigInt("filter","subFarms",this->filterSubfarm);
		this->subFarmSize = getAndSetConfigInt("filter","farmSize",this->subFarmSize);
		this->farmMode = getAndSetConfigString("filter","mode",this->farmMode);
		this->dataSource = getAndSetConfigString("general","dataSource",this->dataSource);
		this->launcherRank = getAndSetConfigInt("launcher.internal","rank",this->launcherRank);
		this->gatherTimer = getAndSetConfigUInt64("general","gatherTimer",0);
		this->builderStored = getAndSetConfigUInt64("general","builderStored",this->builderStored);
		this->filterCredits = getAndSetConfigInt("filter","credits",this->filterCredits);
		this->keepLocalBw = getAndSetConfigBool("testing","keepLocalBw",this->keepLocalBw);
		
		if (eventRailMaxDataSize % (64*1024))
			DAQ_WARNING("Caution, it is better to align eventRailMaxDataSize to some page size at least otherwise performance of RDMA might be lower");
		
		//verbosity
		if (configFile["debug"]["show"].isArray()) {
			int cnt = configFile["debug"]["show"].size();
			for (int i = 0 ; i < cnt ; i++)
				Debug::enableCat(configFile["debug"]["show"][i].asString().c_str());
		}
		
		//mode (PUSH | PULL)
		std::string tmp = getAndSetConfigString("general","mode","PULL");
		if (tmp == "PULL")
			mode = MODE_PULL;
		else if (tmp == "PUSH")
			mode = MODE_PUSH;
		else if (tmp == "PULL_NO_EM")
			mode = DAQ::MODE_PULL_NO_EM;
		else
			DAQ_FATAL_ARG("Invalid value for 'mode' parameter in config file : %1, exptect PUSH or PULL or PULL_NO_EM").arg(tmp);
	#endif //HAVE_JSON_CPP
}

/*******************  FUNCTION  *********************/
// Recursively copy the values of b into a. Both a and b must be objects.
//from http://stackoverflow.com/questions/22512420/is-there-an-elegant-way-to-cascade-merge-two-json-trees-using-jsoncpp
void update(Json::Value& a, Json::Value& b) {
	if (!b.isObject()) return;

	for (const auto& key : b.getMemberNames()) {
		if (a[key].isObject()) {
			update(a[key], b[key]);
		} else {
			a[key] = b[key];
		}
	}
}

/*******************  FUNCTION  *********************/
/**
 * Load the config file and overrite the given fields.
 * @param filename Path to the file to load.
**/
void Config::loadConfigFile ( const std::string& filename )
{
	#ifdef HAVE_JSON_CPP
		//setup json file reader
		Json::Reader reader;
		
		//setup stream and open file
		std::ifstream file(filename.c_str(), std::ifstream::binary);
		assumeArg(file.is_open(),"Fail to open configuration file : %1").arg(filename).end();
		
		//load
		Json::Value tmpObj;

		//parse
		bool status = reader.parse(file, tmpObj);
		assumeArg(status,"Fail to load configuration file '%1' : \n%2")
			.arg(filename)
			.arg(reader.getFormattedErrorMessages())
			.end();
			
		//merge to support cascading
		update(configFile,tmpObj);
		
		//really load it
		this->loadConfigFile(configFile);
	#else
		DAQ_FATAL("Project was compiled without jsonspp so cannot load configuration files. \
			Check -DJSON_CPP_PREFIX on you cmake command line at compile time if it is not installed in /usr.");
	#endif
}

/*******************  FUNCTION  *********************/
/**
 * Create a dump of the current config in JSON format to be ready to be reused
 * as a config file.
**/
void Config::dumpConfigFile ()
{
	//if not file exit
	if (dumpConfig.empty())
		return;
	
	//info
	DAQ_INFO_ARG("Dump config file into %1").arg(dumpConfig).end();
	
	//open file
	std::ofstream out(dumpConfig);
	assumeArg(out.fail() == false,"Fail to open file %1 to dump config : %2").arg(dumpConfig).argStrErrno().end();
	
	//reapply
	this->loadConfigFile(this->configFile);
	
	//load -v
	configFile["debug"]["show"].isArray();
	const DebugCategoryMap* v = Debug::getCatMap();
	if (v == nullptr) {
		configFile["debug"]["show"].append("all");
	} else {
		for (auto const& e : *v) {
			if (e.second) {
				configFile["debug"]["show"].append(e.first);
			}
		}
	}
	
	//push
	out << this->configFile;
	
	//close
	out.close();
}

/*******************  FUNCTION  *********************/
/**
 * Return the JSON value available at the position given by PATH.
 * @param path Path to use inside the config file to extract the requested value.
 * It uses dot (.) as separator of path components.
 * @return Return a JSON value.
**/
Json::Value& Config::getConfig ( const std::string& path ) const
{
	Json::Value * v = &this->configFile;
	
	//loop on childs
	std::stringstream ss(path);
	std::string item;
	while (std::getline(ss, item, '.')) {
		v = &((*v)[item]);
	}
	return *v;
}

/*******************  FUNCTION  *********************/
/**
 * @return Compute the size of an event collition depending on the parameters
 * loaded from config file.
**/
int Config::getEventCollisions () const
{
	assert(eventRailMaxDataSize > collisionRailDataSize);
	return eventRailMaxDataSize / collisionRailDataSize;
}

/*******************  FUNCTION  *********************/
/**
 * Load the value at given path from config file or use the default value.
 * It also register the current value to be used by the dumpConfigFile function.
 * @param path Define the path to search for (element separator is dot).
 * @param elementName Name of the entry to use inside path to get the value (no sub-path, so, no dots).
 * @param defaultValue Define the default value if the entry is not present in the config file.
 * @return Return the value as string.
**/
std::string Config::getAndSetConfigString ( const std::string& path, const std::string & elementName, const char * defaultValue ) const
{
	Json::Value & json = getConfig(path);
	Json::Value value = json.get(elementName,defaultValue);

	//handle per rank
	if(value.isArray())
	{
		assumeArg(value.size() > (size_t)rank,"Missing definition in %1.%2 for rank %3").arg(path).arg(elementName).arg(rank).end();
		value = value[rank];
	} else if (value.isObject()) {
		char buffer[16];
		sprintf(buffer,"%d",rank);
		value = value.get(buffer,value.get("default",defaultValue));
	} else {
		json[elementName] = value;
	}

	return value.asString();
}

/*******************  FUNCTION  *********************/
/**
 * Load the value at given path from config file or use the default value.
 * It also register the current value to be used by the dumpConfigFile function.
 * @param path Define the path to search for (element separator is dot).
 * @param elementName Name of the entry to use inside path to get the value (no sub-path, so, no dots).
 * @param defaultValue Define the default value if the entry is not present in the config file.
 * @return Return the value as string.
**/
std::string Config::getAndSetConfigString ( const std::string& path, const std::string & elementName, const std::string& defaultValue ) const
{
	Json::Value & json = getConfig(path);
	Json::Value value = json.get(elementName,defaultValue);
	
	//handle per rank
	if(value.isArray())
	{
		assumeArg(value.size() > (size_t)rank,"Missing definition in %1.%2 for rank %3").arg(path).arg(elementName).arg(rank).end();
		value = value[rank];
	} else if (value.isObject()) {
		char buffer[16];
		sprintf(buffer,"%d",rank);
		value = value.get(buffer,value.get("default",defaultValue));
	} else {
		json[elementName] = value;
	}
	
	return value.asString();
}

/*******************  FUNCTION  *********************/
/**
 * Load the value at given path from config file or use the default value.
 * It also register the current value to be used by the dumpConfigFile function.
 * @param path Define the path to search for (element separator is dot).
 * @param elementName Name of the entry to use inside path to get the value (no sub-path, so, no dots).
 * @param defaultValue Define the default value if the entry is not present in the config file.
 * @return Return the value as integer.
**/
int Config::getAndSetConfigInt ( const std::string& path, const std::string& elementName, int defaultValue ) const
{
	Json::Value & json = getConfig(path);
	Json::Value value = json.get(elementName,defaultValue);

	//handle per rank
	if(value.isArray())
	{
		assumeArg(value.size() > (size_t)rank,"Missing definition in %1.%2 for rank %3").arg(path).arg(elementName).arg(rank).end();
		value = value[rank];
	} else if (value.isObject()) {
		char buffer[16];
		sprintf(buffer,"%d",rank);
		value = value.get(buffer,value.get("default",defaultValue));
	} else {
		json[elementName] = value;
	}
	
	return value.asInt();
}

/*******************  FUNCTION  *********************/
/**
 * Load the value at given path from config file or use the default value.
 * It also register the current value to be used by the dumpConfigFile function.
 * @param path Define the path to search for (element separator is dot).
 * @param elementName Name of the entry to use inside path to get the value (no sub-path, so, no dots).
 * @param defaultValue Define the default value if the entry is not present in the config file.
 * @return Return the value as boolean.
**/
bool Config::getAndSetConfigBool ( const std::string& path, const std::string& elementName, bool defaultValue ) const
{
	Json::Value & json = getConfig(path);
	Json::Value value = json.get(elementName,defaultValue);

	//handle per rank
	if(value.isArray())
	{
		assumeArg(value.size() > (size_t)rank,"Missing definition in %1.%2 for rank %3").arg(path).arg(elementName).arg(rank).end();
		value = value[rank];
	} else if (value.isObject()) {
		char buffer[16];
		sprintf(buffer,"%d",rank);
		value = value.get(buffer,value.get("default",defaultValue));
	} else {
		json[elementName] = value;
	}

	return value.asBool();
}

/*******************  FUNCTION  *********************/
/**
 * Load the value at given path from config file or use the default value.
 * It also register the current value to be used by the dumpConfigFile function.
 * @param path Define the path to search for (element separator is dot).
 * @param elementName Name of the entry to use inside path to get the value (no sub-path, so, no dots).
 * @param defaultValue Define the default value if the entry is not present in the config file.
 * @return Return the value as unsigned long.
**/
uint64_t Config::getAndSetConfigUInt64 ( const std::string& path, const std::string& elementName, uint64_t defaultValue ) const
{
	Json::Value & json = getConfig(path);
	Json::Value value = json.get(elementName,(Json::UInt64)defaultValue);

	//handle per rank
	if(value.isArray())
	{
		assumeArg(value.size() > (size_t)rank,"Missing definition in %1.%2 for rank %3").arg(path).arg(elementName).arg(rank).end();
		value = value[rank];
	} else if (value.isObject()) {
		char buffer[16];
		sprintf(buffer,"%d",rank);
		value = value.get(buffer,value.get("default",(Json::UInt64)defaultValue));
	} else {
		json[elementName] = value;
	}

	return value.asUInt64();
}

/*******************  FUNCTION  *********************/
/**
 * Load the value at given path from config file or use the default value.
 * It also register the current value to be used by the dumpConfigFile function.
 * @param path Define the path to search for (element separator is dot).
 * @param elementName Name of the entry to use inside path to get the value (no sub-path, so, no dots).
 * @param defaultValue Define the default value if the entry is not present in the config file.
 * @return Return the value as float.
**/
float Config::getAndSetConfigFloat ( const std::string& path, const std::string& elementName, float defaultValue ) const
{
	Json::Value & json = getConfig(path);
	Json::Value value = json.get(elementName,defaultValue);

	//handle per rank
	if(value.isArray())
	{
		assumeArg(value.size() > (size_t)rank,"Missing definition in %1.%2 for rank %3").arg(path).arg(elementName).arg(rank).end();
		value = value[rank];
	} else if (value.isObject()) {
		char buffer[16];
		sprintf(buffer,"%d",rank);
		value = value.get(buffer,value.get("default",defaultValue));
	} else {
		json[elementName] = value;
	}

	return value.asFloat();
}

/*******************  FUNCTION  *********************/
/**
 * Count the number of local processes.
 * @param workdSize Define the total number of processes in the session.
**/
int Config::localProcesses ( int worldSize ) const
{
	int cnt = 0;
	for (int i = 0 ; i < worldSize ; i++)
		if (localMap[i])
			cnt++;
	return cnt;
}

}
