/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_TOPO_ATTACHED_RESSOURCES_HPP
#define DAQ_TOPO_ATTACHED_RESSOURCES_HPP

/********************  HEADERS  *********************/
//internals
#include "transport/Transport.hpp"
//htopml
#ifdef HAVE_HTOPML
	#include <htopml/TypeToJson.h>
	#include <htopml/JsonState.h>
#endif //HAVE_HTOPML

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*********************  CLASS  **********************/
template <class T>
class TopoRessource
{
	public:
		TopoRessource( DAQ::Transport* transport, const DAQ::Config* config, int maxRessources, DAQ::UnitType unitType, bool transportAllocate = false );
		~TopoRessource();
		T & get(int rank,int unitId,int ressourceId);
		const T & get(int rank,int unitId,int ressourceId) const;
		T & get(const UnitId & unitId,int ressourceId);
		void set(int rank,int unitId,int ressourceId,const T & value);
		void set(const UnitId & unitId,int ressourceId,const T & value);
		bool hasAllRessources();
		void reset(const T & value);
		#ifdef HAVE_HTOPML
			template <class U> friend void convertToJson(htopml::JsonState & json, const TopoRessource<U> & topo);
		#endif //HAVE_HTOPML
	private:
		int maxRessources;
		int cntTotRessources;
		T * ressources;
		bool * mask;
		int worldSize;
		int units;
		bool transportAllocate;
		Transport * transport;
};

}

/********************  HEADERS  *********************/
#include "TopoRessource_impl.hpp"

#endif //DAQ_TOPO_ATTACHED_RESSOURCES_HPP
