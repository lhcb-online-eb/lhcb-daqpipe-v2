/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Daniel Campora - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_TIMER_HPP 
#define DAQ_TIMER_HPP 1

/********************  HEADERS  *********************/
#include <chrono>

/********************  MACRO  ***********************/
#define GET_TIME(__TIMER__, __FOO__) __TIMER__.start(); __FOO__; __TIMER__.stop()

/*********************  CLASS  **********************/
class Timer {
	public:
		Timer();
		void start();
		void stop();
		void flush();
		double getElapsedTime() const;
		long long int getElapsedTimeLong() const;
		double get() const;
		long long int getLong() const;
	private:
		std::chrono::high_resolution_clock::time_point tstart, tend;
		std::chrono::duration<double> timeDiff;
};

#endif
