/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Machen Jonanthan - INTEL
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_HISTOGRAM_HPP
#define DAQ_HISTOGRAM_HPP

/********************  HEADERS  *********************/
#include "config.h"
#ifdef HAVE_HTOPML
	#include <htopml/JsonHttpNode.h>
#endif //HAVE_HTOPML

/*******************  NAMESPACE  ********************/
namespace DAQ
{
    
/*********************  CLASS  **********************/
class Histogram 
{
	public:
		Histogram(float min = 0, float max = 1, int steps = 100 );
		virtual ~Histogram();
		
		void push( float value );
		long * results();
		#ifdef HAVE_HTOPML
			friend void convertToJson(htopml::JsonState & json,const Histogram & histogram );
		#endif //HAVE_HTOPML
	private:
		Histogram(const Histogram& orig);
		int steps;
		long * data;
		float range;
		float min;
		float max;
};

}
#endif /* DAQ_HISTOGRAM_HPP */

