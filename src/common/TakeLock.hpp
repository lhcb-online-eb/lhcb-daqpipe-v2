/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_TAKE_LOCK_HPP
#define DAQ_TAKE_LOCK_HPP

/********************  INFO   ***********************/
/**
 * This file orginally came from MPC_Allocator_CPP project
 * written under CeCILL-C licence by Sébastien Valat.
**/

/********************  HEADERS  *********************/
//std
#include <cstdlib>
#include <cassert>

/********************  MACRO  ***********************/
/**
 * Simple macro to start an optional critical section by using TakeLock mechanism. It
 * support internal use of return, exceptions... The section must be ended by END_CRITICAL
 * macro. Variables declared in that scope are not visible beyond the END_CRITICAL limit.
 * 
 * @code{c++}
Mutex mylock;
MALT_OPTIONAL_CRITICAL(mylock,isParallel)
	//to your stuff
	if (error)
		return -1;
	//final stiff
MALT_END_CRITICAL
 * @endcode
 * 
 * @param lock Define the lock to take. A lock object just has to provide lock() and unlock() methods.
 * @param takeLock Take the lock if true otherwise consider a sequential use and do not take the lock.
**/
#define DAQ_OPTIONAL_CRITICAL(lock,takeLock) do { DAQ::TakeLock<__typeof__(lock)> _local_take_lock__(&(lock),(takeLock));

/********************  MACROS  **********************/
/**
 * Simple macro to start a critical section by using TakeLock mechanism. It
 * support internal use of return, exceptions... The section must be ended by END_CRITICAL
 * macro. Variables declared in that scope are not visible beyond the END_CRITICAL limit.
 * 
 * @code{c++}
Mutex mylock;
MALT_START_CRITICAL(mylock)
	//to your stuff
	if (error)
		return -1;
	//final stiff
MALT_END_CRITICAL
 * @endcode
 * 
 * @param lock Define the lock to take. A lock object just has to provide lock() and unlock() methods.
**/
#define DAQ_START_CRITICAL(lock) { DAQ::TakeLock<__typeof__(lock)> _local_take_lock__(&(lock));

/********************  MACROS  **********************/
/** Close a critical region defined by START_CRITICAL of OPTIONAL_CRITICAL. **/
#define DAQ_END_CRITICAL }while(0);

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*********************  CLASS  **********************/
/**
 * Simple class used to manage locking/unlocking of a lock on a region bases by supported
 * exceptions and return inside it.
 * 
 * This is the concept of RAII which use the capability of an object to be
 * automatically destroyed when going out of its declaration scope. This way exceptions
 * and return are automatically support.
 * 
 * To be supported the lock classes must profive a lock() and unlock() method.
 * 
 * @code{c++}
//... Your non locked code here ...
{
	TakeLock<Mutex> safeAccess(&this->mutex);
	// .... Your locked code here ....
	// Can use safetly use return/throw here, it will unlock automatically.
}
//... You non locked code here...
 * @endcode
 *
 * @brief Wrapper class to implement RAII method for thread locking.
**/
template <class T>
class TakeLock
{
	public:
		TakeLock(T * lock,bool takeLock = false);
		~TakeLock();
		void unlock();
		bool isLock();
	private:
		/** Pointer to the lock to unlock in destructor, nullptr if none. **/
		T * lock;
};

/*******************  FUNCTION  *********************/
/**
 * Constructor used to take the lock at init time.
 * @param lock Pointer to the luck to use. 
 * @param takeLock Boolean to make the lock optional of not depending on runtime behavior.
**/
template <class T>
TakeLock<T>::TakeLock(T* lock, bool takeLock)
{
	if (takeLock) {
		assert(lock != nullptr);
		lock->lock();
		this->lock = lock;
	} else {
		this->lock = nullptr;
	}
}

/*******************  FUNCTION  *********************/
/**
 * Automatically released the lock when current object (so scope) is destroyed.
**/
template <class T>
TakeLock<T>::~TakeLock(void )
{
	unlock();
}

/*******************  FUNCTION  *********************/
/**
 * Short function to check the locking status.
**/
template <class T>
inline bool TakeLock<T>::isLock(void )
{
	return lock != nullptr;
}

/*******************  FUNCTION  *********************/
/**
 * Permit to manually unlock inside the context if required.
**/
template <class T>
inline void TakeLock<T>::unlock(void )
{
	if (lock != nullptr) {
		lock->unlock();
		this->lock = nullptr;
	}
}

}

#endif //DAQ_TAKE_LOCK_HPP
