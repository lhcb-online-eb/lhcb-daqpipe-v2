/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Balazs Voneki - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_CRC_HPP
#define DAQ_CRC_HPP

/********************  HEADERS  *********************/
#include <stdint.h>

/********************  NAMESPACE  *******************/
namespace DAQ
{

/*******************  FUNCTION  *********************/
uint16_t crc16(const uint8_t *data, uint16_t size);

}

#endif //DAQ_CRC_HPP
