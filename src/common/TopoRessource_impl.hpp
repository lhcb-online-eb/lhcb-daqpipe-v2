/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_TOPO_ATTACHED_RESSOURCES_IMPL_HPP
#define DAQ_TOPO_ATTACHED_RESSOURCES_IMPL_HPP

/********************  HEADERS  *********************/
#include <common/Debug.hpp>
#include <cassert>
#include <cstring>
#include "TopoRessource.hpp"

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*******************  FUNCTION  *********************/
template <class T>
TopoRessource<T>::TopoRessource(Transport * transport,const Config * config,int maxRessources,UnitType unitType,bool transportAllocate)
{
	//errors
	assert(transport != nullptr);
	assert(config != nullptr);
	assert(maxRessources > 0);
	assert(unitType != UNIT_TYPE_COUNT && unitType != UNIT_EMPTY);
	
	//allocate memory
	if (transportAllocate)
		this->ressources = (T*)transport->allocateRdmaSegment(sizeof(T)*maxRessources * transport->getWorldSize() * DAQ_MAX_UNITS_PER_PROCESS,false);
	else
		this->ressources = new T[maxRessources * transport->getWorldSize() * DAQ_MAX_UNITS_PER_PROCESS];
	this->mask = new bool[transport->getWorldSize() * DAQ_MAX_UNITS_PER_PROCESS];
	this->maxRessources = maxRessources;
	this->cntTotRessources = 0;
	this->units = transport->countUnits(unitType);
	this->worldSize = transport->getWorldSize();
	this->transport = transport;
	this->transportAllocate = transportAllocate;
	
	//fill mask
	memset(mask,0,sizeof(bool)*worldSize*DAQ_MAX_UNITS_PER_PROCESS);
	for (int rank = 0 ; rank < worldSize ; rank++)
		for (int unit = 0 ; unit < DAQ_MAX_UNITS_PER_PROCESS ; unit++)
			if (transport->getUnitType(rank,unit) == unitType)
				mask[rank * DAQ_MAX_UNITS_PER_PROCESS + unit] = true;
	
}

/*******************  FUNCTION  *********************/
template <class T>
TopoRessource<T>::~TopoRessource()
{
	delete [] mask;
	if (transportAllocate)
		transport->deallocateRdmaSegment(ressources,sizeof(T)*maxRessources * transport->getWorldSize() * DAQ_MAX_UNITS_PER_PROCESS,false);
	else
		delete [] ressources;
}

/*******************  FUNCTION  *********************/
template <class T>
T & TopoRessource<T>::get(const UnitId & unitId,int ressourceId)
{
	return get(unitId.rank,unitId.id,ressourceId);
}

/*******************  FUNCTION  *********************/
template <class T>
const T & TopoRessource<T>::get(int rank,int unitId,int ressourceId) const
{
	//checks
	assert(rank >= 0 && rank < worldSize);
	assert(unitId >= 0 && unitId < DAQ_MAX_UNITS_PER_PROCESS);
	assert(ressourceId >= 0 && ressourceId < maxRessources);
	assert(mask[rank*DAQ_MAX_UNITS_PER_PROCESS+unitId]);
	
	//done
	return this->ressources[ressourceId * worldSize * DAQ_MAX_UNITS_PER_PROCESS + unitId * worldSize + rank];
}

/*******************  FUNCTION  *********************/
template <class T>
T & TopoRessource<T>::get(int rank,int unitId,int ressourceId)
{
	//checks
	assert(rank >= 0 && rank < worldSize);
	assert(unitId >= 0 && unitId < DAQ_MAX_UNITS_PER_PROCESS);
	assert(ressourceId >= 0 && ressourceId < maxRessources);
	assert(mask[rank*DAQ_MAX_UNITS_PER_PROCESS+unitId]);
	
	//done
	return this->ressources[ressourceId * worldSize * DAQ_MAX_UNITS_PER_PROCESS + unitId * worldSize + rank];
}

/*******************  FUNCTION  *********************/
template <class T>
void TopoRessource<T>::set(const UnitId & unitId,int ressourceId,const T & value)
{
	assert(mask[unitId.rank*DAQ_MAX_UNITS_PER_PROCESS + unitId.id]);
	this->set(unitId.rank,unitId.id,ressourceId,value);
}

/*******************  FUNCTION  *********************/
template <class T>
void TopoRessource<T>::set(int rank,int unitId,int ressourceId,const T & value)
{
	//checks
	assert(rank >= 0 && rank < worldSize);
	assert(unitId >= 0 && unitId < DAQ_MAX_UNITS_PER_PROCESS);
	assert(ressourceId >= 0 && ressourceId < maxRessources);
	assert(mask[rank*DAQ_MAX_UNITS_PER_PROCESS + unitId]);
	
	//update counter
	this->cntTotRessources++;
	assert(cntTotRessources <= units * maxRessources);
	
	//done
	this->ressources[ressourceId * worldSize * DAQ_MAX_UNITS_PER_PROCESS + unitId * worldSize + rank] = value;
}

/*******************  FUNCTION  *********************/
template <class T>
bool TopoRessource<T>::hasAllRessources()
{
	DAQ_DEBUG_ARG("TopoRessource","Check hasAllRessources : %1/%2").arg(cntTotRessources).arg(units*maxRessources).end();
	return cntTotRessources == units * maxRessources;
}

/*******************  FUNCTION  *********************/
template <class T>
void TopoRessource<T>::reset(const T & value)
{
	//reset content
	for (int rank = 0 ; rank < worldSize ; rank++)
		for (int unitId = 0 ; unitId < DAQ_MAX_UNITS_PER_PROCESS ; unitId++)
			for (int i = 0 ; i < maxRessources ; i++)
				if (mask[rank * DAQ_MAX_UNITS_PER_PROCESS + unitId]) {
					this->cntTotRessources = 0;
					set(rank,unitId,i,value);
				}

	//reset counter
	this->cntTotRessources = 0;
}

/*******************  FUNCTION  *********************/
#ifdef HAVE_HTOPML
	template <class U>
	void convertToJson(htopml::JsonState & json, const TopoRessource<U> & topo)
	{
		json.openStruct();
			json.printField("maxRessources",topo.maxRessources);
			json.printField("cntTotRessources",topo.cntTotRessources);
			json.openFieldArray("ressources");
				for (int rank = 0 ; rank < topo.worldSize ; rank++) {
					json.printListSeparator();
					json.openArray();
						for (int unitId = 0 ; unitId < DAQ_MAX_UNITS_PER_PROCESS ; unitId++) {
							if (topo.mask[rank * DAQ_MAX_UNITS_PER_PROCESS + unitId]) {
								json.printListSeparator();
								json.openArray();
								for (int i = 0 ; i < topo.maxRessources ; i++)
										json.printValue(topo.get(rank,unitId,i));
								json.closeArray();
							}
						}
					json.closeArray();
				}
			json.closeFieldArray("ressources");
			json.printField("worldSize",topo.worldSize);
			json.printField("units",topo.units);
		json.closeStruct();
	}
#endif //HAVE_HTOPML

}

#endif //DAQ_TOPO_ATTACHED_RESSOURCES_IMPL_HPP
