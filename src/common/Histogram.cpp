/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Machen Jonanthan - INTEL
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include <iostream>
#include "Histogram.hpp"

/********************  NAMESPACE  *******************/
using namespace std;

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*******************  FUNCTION  *********************/
Histogram::Histogram(const Histogram& orig) {
	this->min = orig.min;
	this->max = orig.max;
	this->steps = orig.steps;
	this->data = new long[orig.steps];
	this->range = orig.range;

	for( int i = 0; i < steps; i++ ) {
		data[i] = orig.data[i];
	}
}

/*******************  FUNCTION  *********************/
Histogram::Histogram( float min, float max, int steps )
{
	this->min = min;
	this->max = max;
	this->steps = steps;
	this->data = new long[steps];
	this->range = (max - min) / steps;

	for( int i = 0; i < steps; i++ ) {
		data[i] = 0;
	}
}

/*******************  FUNCTION  *********************/
Histogram::~Histogram() {
	delete [] data;
}

/*******************  FUNCTIONS  *******************/
void Histogram::push(float value) 
{
	while( value > this->max ) {
		this->max = ( ( max - min ) * 2 ) + min;  // double the full range
		this->range = ( max - min ) / steps; // recalculate the range
		long * data2 = data;  // store values in temp array
		for( int i = 0; i < steps; i++ ) { // reincrement array indexes according to new ranges
		if( ( i * 2 ) + 1 > steps ) {
				data[i] = 0;
			} else {
				data[i] = data2[i*2] + data2[i*2+1];
			}
		}
	}

	int index = ( ( value - this->min ) / range ) -1;

	if( index >= 0 ) {
		this->data[index]++;
	}
}

/*******************  FUNCTION  *********************/
long * Histogram::results() {
    return this->data;
}

/*******************  FUNCTION  *********************/
#ifdef HAVE_HTOPML
	void convertToJson(htopml::JsonState & json,const Histogram & histogram )
	{
		json.openStruct();
			json.printField( "min", histogram.min );
			json.printField( "max", histogram.max );
			json.printField( "stepCount", histogram.steps );
			json.printFieldArray( "data", histogram.data, histogram.steps );
		json.closeStruct();
	}
#endif //HAVE_HTOPML
}

