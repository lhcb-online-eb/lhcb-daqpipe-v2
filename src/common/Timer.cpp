/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Daniel Campora - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include "Timer.hpp"

/*******************  FUNCTION  *********************/
Timer::Timer() 
      : timeDiff(0)
{
}

/*******************  FUNCTION  *********************/
void Timer::start()
{
  tstart = std::chrono::high_resolution_clock::now();
}

/*******************  FUNCTION  *********************/
void Timer::stop()
{
  tend = std::chrono::high_resolution_clock::now();
  timeDiff += tend - tstart;
}

/*******************  FUNCTION  *********************/
void Timer::flush()
{
  timeDiff = std::chrono::duration<double>::zero();
}

/*******************  FUNCTION  *********************/
/**
 * Gets the elapsed time since tstart
 * @return
*/
double Timer::getElapsedTime() const
{
  auto const ttemp = std::chrono::high_resolution_clock::now();
  return std::chrono::duration<double>(ttemp - tstart).count();
}

/*******************  FUNCTION  *********************/
long long int Timer::getElapsedTimeLong() const
{
  auto const ttemp = std::chrono::high_resolution_clock::now();
  auto const dtemp = std::chrono::duration<double>(ttemp - tstart);
  return std::chrono::duration_cast<std::chrono::microseconds>(dtemp).count();
}

/*******************  FUNCTION  *********************/
/**
 * Gets the accumulated time in timeDiff
 * @return
*/
double Timer::get() const
{
  return timeDiff.count();
}

/*******************  FUNCTION  *********************/
long long int Timer::getLong() const
{
  return std::chrono::duration_cast<std::chrono::microseconds>(timeDiff).count();
}
