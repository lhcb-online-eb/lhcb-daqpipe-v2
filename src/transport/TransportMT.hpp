/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_TRANSPORT_QUEUE_HPP
#define DAQ_TRANSPORT_QUEUE_HPP

/********************  HEADERS  *********************/
#include <queue>
#include "Transport.hpp"
#include "units/Unit.hpp"
#include "portability/Spinlock.hpp"
#include "drivers/DriverLocal.hpp"

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*********************  CLASS  **********************/
/**
 * Multi-threaded implementation of Transport layer. This is currently the only implementation
 * in use as the non multi-threaded one has been removed. It optinaly support to run one thread
 * per unit and per driver.
**/
class TransportMT : public Transport
{
	public:
		TransportMT( const DAQ::Config* config, DAQ::Launcher* launcher, DAQ::Driver* driver );
		virtual ~TransportMT();
		virtual void initialize ( const DAQ::Config& config, DAQ::Launcher& launcher );
		virtual void finalize ();
		virtual void postWaitRemoteRDMAWrite ( Unit* srcUnit, int rank, int unitId, void* ptr, size_t size, int uuid, const UnitCommand& postCommand );
		virtual void rdmaWrite ( DAQ::Unit* srcUnit, int rank, int unitId, void* src, void* dest, uint64_t destKey, size_t size, int uuid, const DAQ::UnitCommand& postCommand );
		virtual void run ();
		virtual void sendCommand ( DAQ::Unit* unit, int rank, int unitId, const DAQ::UnitCommand& command );
		virtual void sendCommand ( const UnitCommand& command );
		virtual RemoteIOV getRemoteIOV ( int targetRank, void* addr, size_t size );
	protected:
		void updateNonThreaded();
		void updateFailedStatus();
		bool rejectFailed(const UnitCommand * command = nullptr);
	private:
		/** Keep track of the local driver for intra-process communications **/
		Driver * localDriver;
		/** Channel to be used to wait exit **/
		GoChannel<bool> exitChannel;
		/** Start one thread per driver **/
		bool useDriverThreads;
		/** Start a thread for the local driver **/
		bool useLocalDriverThreads;
		/** Simulate node failure. **/
		long failureRate;
		/** Is the current node failed for failure simulation **/
		bool failed;
};

}

#endif //DAQ_TRANSPORT_QUEUE_HPP
