/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_MOCK_TRANSPORT_HPP
#define DAQ_MOCK_TRANSPORT_HPP

/********************  HEADERS  *********************/
#include <gmock/gmock.h>
#include "../Transport.hpp"

/********************  NAMESPACE  *******************/
namespace DAQ {

class MockTransport : public Transport {
	public:
		MockTransport(const std::string& name, const Config* config, Launcher* launcher, Driver* driver):Transport("mock",config,launcher,driver){};
		MOCK_METHOD2(initialize,
			void(const Config & config, Launcher & launcher));
		MOCK_METHOD0(finalize,
			void());
		MOCK_METHOD0(run,
			void());
		MOCK_METHOD7(postWaitRemoteRDMAWrite,
			void(Unit* srcUnit,int rank,int unitId,void * ptr,size_t size,int uuid,const UnitCommand & postCommand));
		MOCK_METHOD9(rdmaWrite,
			void(Unit* srcUnit,int rank,int unitId,void * src,void * dest,uint64_t destKey,size_t size, int uuid,const UnitCommand & postCommand));
		MOCK_METHOD3(sendCommand,
			void(Unit * srcUnit,const UnitId destUnit,const UnitCommand & command));
		MOCK_METHOD4(sendCommand,
			void(Unit * srcUnit,int rank,int unitId,const UnitCommand & command));
		MOCK_METHOD3(sendCommand,
			void(Unit * srcUnit,UnitType unitType,const UnitCommand & command));
		MOCK_METHOD1(sendCommand,
			void(const UnitCommand & command));
		MOCK_METHOD3(getRemoteIOV,
			RemoteIOV(int targetRank, void* addr, size_t size));
};

}  // namespace DAQ

#endif //DAQ_MOCK_TRANSPORT_HPP
