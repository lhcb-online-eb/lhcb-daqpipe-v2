/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_UNIT_TYPE_HPP
#define DAQ_UNIT_TYPE_HPP

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/********************  ENUM  ************************/
/**
 * Define the type of unit we want to instanciate on a particular cell of the
 * transport layout.
**/
enum UnitType
{
	/** No unit, let the pointer nullptr. **/
	UNIT_EMPTY,
	/** Setup an EventManager **/
	UNIT_EVENT_MANAGER,
	/** Setup a BuilderUnit **/
	UNIT_BUILDER_UNIT,
	/** Setup a ReadoutUnit **/
	UNIT_READOUT_UNIT,
	/** Unit for global profiling **/
	UNIT_PROFILE_UNIT,
	/** Unit for schedule sending to filter units **/
	UNIT_FILTER_MANAGER,
	/** Filter unit **/
	UNIT_FILTER_UNIT,
	/** Dummy unit **/
	UNIT_DUMMY,
	/** Unit custom **/
	UNIT_CUSTOM,
	/** All units wildcard **/
	UNIT_ALL,
	/**
	 * Number of available types.
	 * @caution keep this one on last position
	**/
	UNIT_TYPE_COUNT
};

}

#endif
