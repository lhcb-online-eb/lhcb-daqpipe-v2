/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include <cassert>
#include "Transport.hpp"
#include "units/EventManager.hpp"
#include "units/BuilderUnit.hpp"
#include "units/ReadoutUnit.hpp"
#include "units/ProfileUnit.hpp"
#include "units/FilterManager.hpp"
#include "units/FilterUnitTCP.hpp"
#include "units/FilterUnit.hpp"
#include "units/DummyUnit.hpp"
#include "common/Debug.hpp"
#ifdef HAVE_HTOPML
	#include "htopml/HtopmlLayout.hpp"
    #include "htopml/HtopmlBalancing.hpp"
#endif

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/********************  GLOBALS  *********************/
/**
 * Converter array for unit types.
**/
static const char * cstTypeNames[] = {
	"EMPTY",
	"EVENT_MANAGER",
	"BUILDER_UNIT",
	"READOUT_UNIT",
	"PROFILE_UNIT",
	"FILTER_MANAGER",
	"FILTER_UNIT",
	"DUMMY_UNIT",
	"UNIT_CUSTOM"
};

/*******************  FUNCTION  *********************/
/**
 * Transport constructor. It mostly init the unit arrays.
 * @param name Name of the current transport layer.
 * @param config Pointer to the global config object.
 * @param launcher Pointer the the launcher.
 * @param driver Pointer to the inter-process driver.
**/
Transport::Transport(
		const std::string& name,
		const DAQ::Config* config,
		DAQ::Launcher* launcher,
		DAQ::Driver* driver) {
	//check errors
	assert(config != nullptr);
	assert(name.empty() == false);
	assert(launcher != nullptr);
	
	//setup
	this->name = name;
	this->config = config;
	this->launcher = launcher;
	this->driver = driver;
	this->hasExit = false;
	this->useUnitThreads = config->useUnitThreads;
	
	//reg transport
	this->driver->setTransport(this);
	
	//init type map
	int ranks = launcher->getWorldSize();
	unitTypes = new UnitType[DAQ_MAX_UNITS_PER_PROCESS * ranks];
	
	//config
	fuType = config->getAndSetConfigString("filter","type","local");
	
	//init arrays
	for (int i = 0 ; i < DAQ_MAX_UNITS_PER_PROCESS * ranks ; i++) {
		unitTypes[i] = UNIT_EMPTY;
	}

	//dead nodes
	this->deadNodes = new bool[launcher->getWorldSize()];
	for (int i = 0 ; i < launcher->getWorldSize() ; i++)
		this->deadNodes[i] = false;
	#ifdef HAVE_HTOPML
		HtopmlLayoutHttpNode::registerUnitArray(unitTypes, ranks, DAQ_MAX_UNITS_PER_PROCESS, deadNodes);
	#endif
}

/*******************  FUNCTION  *********************/
/**
 * Check if the given unit identifyer is valid (getting a valid rank and valid id)
**/
bool UnitId::isValid() const
{
	return rank != -1 && id != -1;
}

/*******************  FUNCTION  *********************/
/**
 * Construct a unit identifyer composed of a rank and a unit id in this rank.
 * @param rank Define the rank containing the unit.
 * @param id Define the ID of the unit in the given rank.
**/
UnitId::UnitId(int rank,int id)
{
	this->id = id;
	this->rank = rank;
}

/*******************  FUNCTION  *********************/
/**
 * Clear all units and allocated arrays.
**/
void Transport::clear ()
{
	for (int i = 0 ; i < DAQ_MAX_UNITS_PER_PROCESS ; i++)
		if (localUnits[i] != nullptr)
			delete localUnits[i];
	delete [] unitTypes;
	delete [] deadNodes;
}

/*******************  FUNCTION  *********************/
/**
 * Get a unit based on hist type and its index in the list of units of this type.
 * The units are ordered by rank and id.
 * @param type Define the type of unit we consider.
 * @param index Define the index of the unit we want.
**/
UnitId Transport::getIndexedUnitId ( UnitType type, int index )
{
	for (int rank = 0 ; rank < getWorldSize() ; rank++)
		for (int unit = 0 ; unit < DAQ_MAX_UNITS_PER_PROCESS ; unit++)
			if (getUnitType(rank,unit) == type)
				if (index-- == 0)
					return UnitId(rank,unit);
	
	DAQ_FATAL_ARG("Fail to find indexed unit id for %1").arg(type).end();
	return UnitId(-1,-1);
}

/*******************  FUNCTION  *********************/
/**
 * Return the transport driver name.
**/
const std::string& Transport::getName () const
{
	return name;
}

/*******************  FUNCTION  *********************/
/**
 * Manually point a unit in the current process with given ID.
 * This function is available more for testing. Prefet to use setUnitType
 * and letting the transport layer building the units by itself.
 * @param unitId ID of the unit in the current rank.
 * @param unit Pointer to the allocated unit. Caution it will be freed by the 
 * transport layer at exit.
**/
void Transport::setLocalUnit ( int unitId, Unit* unit )
{
	//errors
	assert(unit != nullptr);
	assert(unitId >=0 && unitId < DAQ_MAX_UNITS_PER_PROCESS);
	assert(localUnits[unitId] == nullptr);
	
	//setup
	this->localUnits[unitId] = unit;
}

/*******************  FUNCTION  *********************/
/**
 * Set type of unit to affect to one location. It fill a global map of all units
 * available in every rank. 
 * @param rank Define the rank we are interested in.
 * @param unitId Define the ID of the unit at the given rank.
 * @param type Define the type of unit to assign to this location.
**/
void Transport::setUnitType ( int rank, int unitId, UnitType type )
{
	//errors
	assert(type != UNIT_EMPTY);
	assert(unitId >=0 && unitId < DAQ_MAX_UNITS_PER_PROCESS);
	assert(rank >= 0 && rank < this->getWorldSize());
	
	//get cell
	UnitType & cell = unitTypes[rank * DAQ_MAX_UNITS_PER_PROCESS + unitId];
	
	//check
	assert(cell == UNIT_EMPTY);
	
	//update count
	count[type]++;
	
	//setup
	cell = type;
}

/*******************  FUNCTION  *********************/
/**
 * Return the unit type at given rank and id.
 * @param rank Define the rank we are interested in.
 * ^param unitId Define the ID we are interested in for the given rank.
**/
UnitType Transport::getUnitType ( int rank, int unitId )
{
	//errors
	assert(unitId >=0 && unitId < DAQ_MAX_UNITS_PER_PROCESS);
	assert(rank >= 0 && rank < this->getWorldSize());
	
	//return
	return unitTypes[rank * DAQ_MAX_UNITS_PER_PROCESS + unitId];
}

/*******************  FUNCTION  *********************/
/**
 * After setting all the unit type this function is called to instantiate
 * the units for the local rank.
**/
void Transport::buildLocalUnits ()
{
	//create at leat each once temporarly to be sure to get then in dumped config on rank 0
	if (this->getRank() == 0 && config->mode != MODE_PULL_NO_EM)
	{
		BuilderUnit bu(this,config);
		ReadoutUnit ru(this,config,true);
		EventManager em(this,config);
	}
	
	//create
	int rank = this->getRank();
	for (int i = 0 ; i < DAQ_MAX_UNITS_PER_PROCESS ; i++) {
		switch(getUnitType(rank,i)) {
			case UNIT_EMPTY:
				break;
			case UNIT_EVENT_MANAGER:
				setLocalUnit(i,new EventManager(this,config));
				break;
			case UNIT_BUILDER_UNIT:
				setLocalUnit(i,new BuilderUnit(this,config));
				break;
			case UNIT_READOUT_UNIT:
				setLocalUnit(i,new ReadoutUnit(this,config,false));
				break;
			case UNIT_PROFILE_UNIT:
				setLocalUnit(i,new ProfileUnit(this,config));
				break;
			case UNIT_FILTER_UNIT:
				setLocalUnit(i,buildFilterUnit());
				break;
			case UNIT_FILTER_MANAGER:
				setLocalUnit(i,new FilterManager(this,config));
				break;
			case UNIT_DUMMY:
				setLocalUnit(i,new DummyUnit());
				break;
			case UNIT_CUSTOM:
				DAQ_FATAL("You can't use buildLocalUnits if you use some CUSTOM units");
				break;
			case UNIT_ALL:
				DAQ_FATAL("Invalid UNIT_ALL, it cannot be user here.");
				break;
			case UNIT_TYPE_COUNT:
				break;
		}
	}
}

/*******************  FUNCTION  *********************/
Unit* Transport::buildFilterUnit ( void )
{
	if (fuType == "local") {
		return new FilterUnit(this,config);
	} else if (fuType == "tcp") {
		return new FilterUnitTCP(this,config);
	} else {
		DAQ_FATAL_ARG("Invalid unit type : %1").arg(fuType).end();
		return NULL;
	}
}

/*******************  FUNCTION  *********************/
/**
 * Send a START command to all avilable units in the current process.
**/
void Transport::sendStartToAllLocalUnits ()
{
	//setup command to send
	UnitCommand command;
	command.type = UNIT_CMD_START;
	
	//send to all
	for (int i = 0 ; i < DAQ_MAX_UNITS_PER_PROCESS ; i++)
		if (localUnits[i] != nullptr)
			this->sendCommand(localUnits[0],getRank(),i,command);
}

/*******************  FUNCTION  *********************/
/**
 * Send a command to all units of a given type.
 * @param srcUnit Definethe local source unit address.
 * @param unitType Define to which type of unit we want to send commands.
 * @param command Command to be sent to all units of the given type.
**/
void Transport::sendCommand ( Unit* srcUnit, UnitType unitType, const UnitCommand& command )
{
	for (int rank = 0 ; rank < getWorldSize() ; rank++)
		for (int unitId = 0 ; unitId < DAQ_MAX_UNITS_PER_PROCESS ; unitId++)
			if (getUnitType(rank,unitId) == unitType || (unitType == UNIT_ALL && getUnitType(rank,unitId) != UNIT_EMPTY))
				sendCommand(srcUnit,rank,unitId,command);
}

/*******************  FUNCTION  *********************/
/**
 * Get local ID of a given unit identified by its pointer.
 * @param unit Address of the unit.
**/
int Transport::getUnitId ( Unit* unit ) const
{
	//errors
	assert(unit != nullptr);
	
	//search
	for (int i = 0 ; i < DAQ_MAX_UNITS_PER_PROCESS ; i++)
		if (localUnits[i] == unit)
			return i;
	
	//error
	DAQ_FATAL("Fail to find unit ID");
	return -1;
}

/*******************  FUNCTION  *********************/
/**
 * Return number of units of a given type.
 * @param type Define the type of units we are interested in.
**/
int Transport::countUnits ( UnitType type ) const
{
	return count[type];
}

/*******************  FUNCTION  *********************/
/**
 * Call start function of every units using the threaded or non threaded mode
 * depending on current configuration.
**/
void Transport::startLocalUnitThreads ()
{
	DAQ_DEBUG("unit","Start unit threads");
	for (int i = 0 ; i < DAQ_MAX_UNITS_PER_PROCESS ; i++) {
		if (localUnits[i] != nullptr) {
			if (this->useUnitThreads)
				localUnits[i]->startThread();
			else
				localUnits[i]->startNoThreads();
		}
	}
}

/*******************  FUNCTION  *********************/
/**
 * In non multi-threaded mode call the updateNoThread function of every unit one
 * by one. This function need to return at some point to not deadlock the
 * event loop.
**/
void Transport::updateUnits ()
{
	assume(this->useUnitThreads == false,"Need to be called only without unit threads");

	for (int i = 0 ; i < DAQ_MAX_UNITS_PER_PROCESS ; i++)
		if (localUnits[i] != nullptr)
			localUnits[i]->updateNoThread();
}

/*******************  FUNCTION  *********************/
/**
 * In multi-threaded mode stop the unit threads.
**/
void Transport::stopLocalUNitThreds ()
{
	DAQ_DEBUG("unit","Stop unit threads");
	for (int i = 0 ; i < DAQ_MAX_UNITS_PER_PROCESS ; i++)
		if (localUnits[i] != nullptr)
			localUnits[i]->stop();
}

/*******************  FUNCTION  *********************/
/**
 * Stream unit type by making the string conversion.
 * @parma out Output stream to use.
 * @param type Unit type to use.
**/
std::ostream& operator<< ( std::ostream& out, UnitType type )
{
	out << cstTypeNames[type];
	return out;
}

/*******************  FUNCTION  *********************/
/**
 * Get unit name for given rank and ID.
 * @param rank Define the rank we are interested int.
 * @param unitId Define the unitID we are interested int.
**/
const char* Transport::getUnitName ( int rank, int unitId )
{
	UnitType type = getUnitType(rank,unitId);
	return cstTypeNames[type];
}

/*******************  FUNCTION  *********************/
/**
 * Use the launcher to return the current rank.
**/
int Transport::getRank ()
{
	return launcher->getRank();
}

/*******************  FUNCTION  *********************/
/**
 * Use the lanucer to return the current world size.
**/
int Transport::getWorldSize ()
{
	return launcher->getWorldSize();
}

/*******************  FUNCTION  *********************/
/**
 * Register a segment into the driver for latter use in RDMA operatoins.
 * @param addr Base address of the segment
 * @param size Size of the segment.
**/
void Transport::registerSegment ( void* addr, size_t size )
{
	driver->registerSegment(addr,size);
}

/*******************  FUNCTION  *********************/
/**
 * Unregister a segment into the network driver.
 * @param addr Base address of the segment
 * @param size Size of the segment.
**/
void Transport::unregisterSegment ( void* addr, size_t size )
{
	driver->unregisterSegment(addr,size);
}

/*******************  FUNCTION  *********************/
/**
 * Get the remote key to be used remotly for an RDMA transfert.
 * @param addr Base address of the segment
 * @param size Size of the segment.
**/
uint64_t Transport::getSegmentRemoteKey ( void* addr, size_t size )
{
	return driver->getSegmentRemoteKey(addr,size);
}

/*******************  FUNCTION  *********************/
/**
 * Retuen pointer to the local unit of given ID.
 * @param unitId Local ID of the requested unit.
**/
Unit* Transport::getLocalUnit ( int unitId )
{
	assert(unitId >= 0 && unitId < DAQ_MAX_UNITS_PER_PROCESS);
	return localUnits[unitId];
}

/*******************  FUNCTION  *********************/
/**
 * Enable exit to quit the event loop.
**/
void Transport::setExit ()
{
	this->hasExit = true;
}

/*******************  FUNCTION  *********************/
/**
 * Stream conversion of a unitID (rank and ID)
**/
std::ostream& operator<< ( std::ostream& out, const UnitId& unitId )
{
	out << unitId.rank << ':' << unitId.id;
	return out;
}

/*******************  FUNCTION  *********************/
/**
 * Build a unitId from rank and id.
 * @param rank The rank to be used.
 * @param id The ID to be used.
**/
UnitId buildUnitId ( int rank, int id )
{
	UnitId unit;
	unit.rank = rank;
	unit.id = id;
	return unit;
}

/*******************  FUNCTION  *********************/
/**
 * Send a command to the given unit idenentified by a UnitId.
 * @param srcUnit The unit sending the command identified by his pointer.
 * @param destUnit The destination unit (containing rank and id)
 * @param command The command content to transmit.
**/
void Transport::sendCommand ( Unit* srcUnit, const UnitId destUnit, const UnitCommand& command )
{
	this->sendCommand(srcUnit,destUnit.rank,destUnit.id,command);
}

/*******************  FUNCTION  *********************/
/**
 * Get the global ID of a given unit. It correspond the ID of this unit listing only
 * the units of its type.
 * @param unit Pointer to the unit.
**/
int Transport::getGblIdOfUnit ( Unit* unit )
{
	return getGblIdOfUnit(getRank(), getUnitId(unit));
}

/*******************  FUNCTION  *********************/
/**
 * Get the global ID of a unit. It correspond the ID of this unit listing only
 * the units of its type.
 * @param rank Rank of the unit we are interested in.
 * @param unitId ID of the unit we are interested in considering the given rank.
**/
int Transport::getGblIdOfUnit ( int rank, int unitId )
{
	UnitType type = this->getUnitType(rank,unitId);
	int counter = 0;
	for (int r = 0 ; r <= rank ; r++) {
		for (int u = 0 ; u < DAQ_MAX_UNITS_PER_PROCESS ; u++)
			if (type == getUnitType(r,u) && (r != rank || u < unitId))
				counter++;
	}
	return counter;
}

/*******************  FUNCTION  *********************/
/**
 * Allocate an RDMA segment by the network driver. It still beed to be registed.
 * This is requierd in drivers like RapidIO.
 * @param size Define the size of the segment.
 * @param write Definethe if the segment is to be used for write or read.
**/
void* Transport::allocateRdmaSegment ( size_t size, bool write )
{
	return driver->allocateRdmaSegment(size,write);
}

/*******************  FUNCTION  *********************/
/**
 * Deallocate an RDMA segment by the network driver. It still beed to be registed.
 * This is requierd in drivers like RapidIO.
 * @param size Define the size of the segment.
 * @param write Definethe if the segment is to be used for write or read.
**/
void Transport::deallocateRdmaSegment ( void* ptr, size_t size, bool write )
{
	return driver->deallocateRdmaSegment(ptr,size,write);
}

/*******************  FUNCTION  *********************/
/**
 * Check if a given rank is dead.
 * @param rank Rank to be tested.
**/
bool Transport::isDead ( int rank )
{
	return deadNodes[rank];
}

/*******************  FUNCTION  *********************/
/**
 * Mark a given rank as dead.
**/
void Transport::markDead ( int rank )
{
	deadNodes[rank] = true;
	assume(rank != 0,"Cannot continue if node 0 (EM) is dead !");

	//safety for unsupported failure mitigation
	for (int i = 0 ; i < DAQ_MAX_UNITS_PER_PROCESS ; i++)
		if (getUnitType(rank,i) == UNIT_EVENT_MANAGER)
			DAQ_FATAL("Cannot continue to run with a failure of the EventManager, abort !");

	//check if all dead
	for (int i = 1 ; i < launcher->getWorldSize() ; i++)
		if (deadNodes[i] == false)
			return;

	DAQ_FATAL("All nodes becomes dead, cannot continue without deadlocking, try a smaller value of testing.failureRate !");
}

}
