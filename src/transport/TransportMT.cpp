/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include "TransportMT.hpp"
#include "launcher/Launcher.hpp"
#include "common/Debug.hpp"
#include "units/Unit.hpp"
#include <cassert>
#ifdef HAVE_HTOPML
	#include "htopml/DynamicConfigHttpNode.hpp"
#endif //HAVE_HTOPML

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*******************  FUNCTION  *********************/
/**
 * Constructor of transport MT. It mostly setup the local configs.
 * @param config Define the global config object.
 * @param launcher Define the launcher to be used.
 * @param driver Define the driver to be used for inter-process communications.
**/
TransportMT::TransportMT ( const DAQ::Config* config, DAQ::Launcher* launcher, DAQ::Driver* driver ) 
                  :Transport ( "TransportMT", config, launcher , driver)
{
	this->localDriver = new DriverLocal();
	this->localDriver->setTransport(this);
	this->useDriverThreads = config->useDriverThreads;
	this->useLocalDriverThreads = config->useLocalDriverThreads;
	this->failureRate = config->getAndSetConfigUInt64("testing","failureRate",0) * launcher->getWorldSize();
	this->failed = false;
	this->hasExit = false;
	srand(launcher->getRank());
}

/*******************  FUNCTION  *********************/
/**
 * Delete the local driver.
**/
TransportMT::~TransportMT ()
{
	delete localDriver;
}

/*******************  FUNCTION  *********************/
/**
 * Event dispatcher unsed in non multi-threaded mode. It will call driver and units.
**/
void TransportMT::updateNonThreaded ()
{
	if (!this->useLocalDriverThreads)
		localDriver->updateNoThread();
	if (!this->useDriverThreads)
		driver->updateNoThread();
	if (!this->useUnitThreads)
		updateUnits();
}

/*******************  FUNCTION  *********************/
/**
 * Main function to be called by the handler.
**/
void TransportMT::run ()
{
	//track
	this->hasExit = false;
	
	//setup threads of drivers
	if (this->useDriverThreads)
		this->driver->startThreads();
	else
		this->driver->startNoThreads();
	
	//setup local driver threads
	if (this->useLocalDriverThreads)
		this->localDriver->startThreads();
	else
		this->localDriver->startNoThreads();
	
	//start local threads of units
	this->startLocalUnitThreads();
	
	//send start to all
	this->sendStartToAllLocalUnits();
	
	//wait exit
	if (this->useUnitThreads && this->useDriverThreads && this->useLocalDriverThreads) {
		bool exit;
		exit << exitChannel;
	} else {
		//loop to give cpu
		while (!hasExit)
			updateNonThreaded();
		//do a last round in case
		updateNonThreaded();
	}
	
	//debug
	DAQ_INFO("Nothing else to do, exit, closing threads");
	
	//stop threads
	this->stopLocalUNitThreds();
	this->driver->stop();
	this->localDriver->stop();
}

/*******************  FUNCTION  *********************/
/**
 * Send a command to a given unit. The target must be defined inside the command
 * struct to be used by this function. This function will intercept EXIT command
 * to handle exit. It also intercept local communications to directly send them
 * to the target unit. If not local communications it send them to the 
 * network driver.
 * @param commmand Command to be transmitted.
**/
void TransportMT::sendCommand ( const UnitCommand& command )
{
	//checks
	assumeArg(command.dest.rank >= 0 && command.dest.rank < getWorldSize(),"Invalid rank id to send action : %1").arg(command.dest.rank).end();
	assumeArg(getUnitType(command.dest.rank,command.dest.id) != UNIT_EMPTY,"Invalid unitID to send action : %1").arg(command.dest.id).end();
	assert(command.type != UNIT_CMD_INVALID);
	
	#ifdef HAVE_HTOPML
		if (command.type == UNIT_CMD_DYNAMIC_CONFIG && command.dest.rank == getRank()) {
			DynamicConfigHttpNode::updateParam(command.args.dynamicConfig.id,command.args.dynamicConfig.value);
			return;
		}
	#endif
	
	//post to driver
	DAQ_DEBUG_ARG("TransportMT","sendCommand(%1 -> %2 => %3)")
		.arg(command.src)
		.arg(command.dest)
		.arg(command.type)
		.end();
	
	//capture dead
	if (command.type == UNIT_CMD_NODE_FAILED)
		markDead(command.args.nodeFailed.rank);
	
	//failure simulation
	if (rejectFailed(&command))
		return;
	
	//setup action
	if (command.type == UNIT_CMD_EXIT && command.dest.rank == getRank()) {
		DAQ_DEBUG("TransportMT","Get exit message in transport");
		exitChannel.close();
		this->hasExit = true;
	} else if (command.dest.rank == getRank()) {
		Unit * unit = getLocalUnit(command.dest.id);
		assert(unit != nullptr);
		unit->pushCommand(command);
	} else {
		this->driver->sendRemoteCommand(command);
	}
}

/*******************  FUNCTION  *********************/
/**
 * Setup a command to be send to a given unit and call the generak sendCommand
 * fuction.
 * @param unit Address of the source unit.
 * @param rank rank hosting the targeted unit.
 * @param unitId ID of the target unit in the given rank.
 * @param command Contenent of the command to send. Function will setup
 * field src and dest.
**/
void TransportMT::sendCommand ( Unit * unit,int rank, int unitId, const UnitCommand& command )
{
	//checks
	assumeArg(rank >= 0 && rank < getWorldSize(),"Invalid rank id to send action : %1").arg(rank).end();
	assumeArg(getUnitType(rank,unitId) != UNIT_EMPTY,"Invalid unitID to send action : %1").arg(unitId).end();
	assume(!command.dest.isValid(),"Try to setup a command which already has target setup");
	assume(!command.src.isValid(),"Try to setup a command which already has src setup");
	assert(command.type != UNIT_CMD_INVALID);
	
	//setup
	UnitCommand cmd = command;
	cmd.dest.rank = rank;
	cmd.dest.id = unitId;
	cmd.src.id = getUnitId(unit);
	cmd.src.rank = getRank();
	
	//push to queue
	this->sendCommand(cmd);
}

/*******************  FUNCTION  *********************/
/**
 * Init the transport, mostly init the local driver.
**/
void TransportMT::initialize ( const Config & config, Launcher & launcher )
{
	this->localDriver->initialize(config, launcher,0,nullptr);
}

/*******************  FUNCTION  *********************/
/**
 * Finalize the transport, mostly the local driver.
**/
void TransportMT::finalize ()
{
	this->localDriver->finalize();
}

/*******************  FUNCTION  *********************/
/**
 * Post a wait RDMA operation to the given driver. It will select the local or
 * global driver depening on the location of the source and destination.
 * @param srcUnit Address of the source unit in local rank.
 * @param rank Rank of the target unit.
 * @param unitId ID of the target unit in given rank.
 * @param ptr Address of the segment to receive on.
 * @param size Maximal size of the transfert.
 * @param uuid Unit ID to identify the transfert. Typically translated to MPI_TAG when using MPI driver.
 * @param postCommand Command to send back to the src unit when data has been received.
**/
void TransportMT::postWaitRemoteRDMAWrite ( Unit* srcUnit, int rank, int unitId, void* ptr, size_t size, int uuid, const UnitCommand& postCommand )
{
	//checks
	assumeArg(rank >= 0 && rank < getWorldSize(),"Invalid rank id to send action : %1").arg(rank).end();
	assumeArg(getUnitType(rank,unitId) != UNIT_EMPTY,"Invalid unitID to send action : %1").arg(unitId).end();
	assert(postCommand.type != UNIT_CMD_INVALID);
	
	Driver * driver = this->driver;
	
	//failure simulation
	if (rejectFailed())
		return;
	
	//switch to local optim if possible
	if (rank == getRank())
		driver = localDriver;
	
	//setup postCommand
	UnitCommand postCmd = postCommand;
	postCmd.src =buildUnitId(rank,unitId);
	postCmd.dest = buildUnitId(getRank(),getUnitId(srcUnit));
	
	//post to driver
	DAQ_DEBUG_ARG("postWaitRemoteRDMAWrite","rdmaWrite(%1:%2 -> %3:%4 (%5) => %6)")
		.arg(getRank())
		.arg(getUnitId(srcUnit))
		.arg(rank)
		.arg(unitId)
		.arg(uuid)
		.arg(postCommand.type)
		.end();
	driver->postWaitRemoteRDMAWrite(srcUnit,rank,unitId,ptr,size,uuid,postCmd);
}

/*******************  FUNCTION  *********************/
/**
 * Run an RDMA write operation.
 * @param srcUnit Address of the source unit in local rank.
 * @param rank Rank of the target unit.
 * @param unitId ID of the target unit in given rank.
 * @parma src Source segment containing the data to send.
 * @param dest Destination address (remote) in which to write the data.
 * @param destKey Key of the remote destination segment (be ignored by drivers like MPI).
 * @param size Size of the data to transfert.
 * @param uuid Unit ID to identify the transfert. Typically translated to MPI_TAG when using MPI driver.
 * @param postCommand Command to send back to the src unit when data has been sent.
**/
void TransportMT::rdmaWrite ( Unit* srcUnit, int rank, int unitId, void* src, void* dest, uint64_t destKey, size_t size, int uuid, const UnitCommand& postCommand )
{
	//checks
	assumeArg(rank >= 0 && rank < getWorldSize(),"Invalid rank id to send action : %1").arg(rank).end();
	assumeArg(getUnitType(rank,unitId) != UNIT_EMPTY,"Invalid unitID to send action : %1").arg(unitId).end();
	assert(postCommand.type != UNIT_CMD_INVALID);
	
	Driver * driver = this->driver;
	
	//failure simulation
	if (rejectFailed())
		return;
	
	//switch to local optim if possible
	if (rank == getRank())
		driver = localDriver;
	
	//setup postCommand
	UnitCommand postCmd = postCommand;
	postCmd.src =buildUnitId(rank,unitId);
	postCmd.dest = buildUnitId(getRank(),getUnitId(srcUnit));
	
	//post to driver
	DAQ_DEBUG_ARG("TransportMT","rdmaWrite(%1:%2 -> %3:%4 (%5) => %6)")
		.arg(getRank())
		.arg(getUnitId(srcUnit))
		.arg(rank)
		.arg(unitId)
		.arg(uuid)
		.arg(postCommand.type)
		.end();
	driver->rdmaWrite(srcUnit,rank,unitId,src,dest,destKey,size,uuid,postCmd);
}

/*******************  FUNCTION  *********************/
/**
 * Get the description of the given segment from the driver to be identified
 * remotly by RDMA operation.
 * @param targetRank To know which driver to call (local or inter-process)
 * @param add Base address of the segment we are interested in.
 * @param size Define the size of the segment.
**/
RemoteIOV TransportMT::getRemoteIOV ( int targetRank, void* addr, size_t size )
{
	if (targetRank == getRank())
		return localDriver->getRemoteIOV(addr,size);
	else
		return driver->getRemoteIOV(addr,size);
}

/*******************  FUNCTION  *********************/
/**
 * Called for simulatoin of faiure, will randomly choose if a node fail and
 * which node failed.
**/
void TransportMT::updateFailedStatus ()
{
	//trivial
	if (failed || failureRate == 0)
		return;

	//check ratio
	int r = (int) (failureRate *rand()/(RAND_MAX+1.0));
	//DAQ_INFO_ARG("%1").arg(r).end();

	//make failure
	if (r == 0)
	{
		//info
		DAQ_INFO("Gen node failure on current node");
		
		//mark failed
		this->failed = true;
		
		//notify
		UnitCommand cmd;
		cmd.type = UNIT_CMD_NODE_FAILED;
		cmd.args.nodeFailed.rank = this->getRank();
		((Transport*)this)->sendCommand(localUnits[0],UNIT_ALL,cmd);
	}
}

/*******************  FUNCTION  *********************/
/**
 * Check if a node is failed to not transmit commands to those nodes.
 * There is an exception for command type EXIT and NODE_FAILED which
 * are transmitted anyway.
**/
bool TransportMT::rejectFailed ( const DAQ::UnitCommand* command )
{
	//update & check status
	updateFailedStatus();
	if (failed == false)
		return false;

	//reject all rdma
	if (command == nullptr)
		return true;
	
	//keep transmission of exit & failure
	return (command->type != UNIT_CMD_EXIT && command->type != UNIT_CMD_NODE_FAILED);
}

}
