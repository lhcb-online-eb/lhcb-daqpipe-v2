/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_TRANSPORT_HPP
#define DAQ_TRANSPORT_HPP

/********************  HEADERS  *********************/
#include <array>
#include <string>
#include <ostream>
#include "common/Config.hpp"
#include "launcher/Launcher.hpp"
#include "drivers/Driver.hpp"
#include "UnitType.hpp"

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*********************  TYPES  **********************/
class Unit;
struct UnitCommand;

/********************  STRUCT  **********************/
/**
 * Identify one unit by its rank and unit id.
**/
struct UnitId
{
	UnitId(int rank = -1,int id = -1);
	bool isValid() const;
	/** Rank hosting the unit. **/
	int rank;
	/** Id of the unit **/
	int id;
};

/*********************  CLASS  **********************/
/**
 * The transport layer handle the units and manage the glue between units and driver.
 * It mostly implement the command dispatching between units.
**/
class Transport
{
	public:
		Transport( const std::string& name, const DAQ::Config* config, DAQ::Launcher* launcher, DAQ::Driver* driver );
		virtual ~Transport() = default;
		virtual void initialize(const Config & config, Launcher & launcher) = 0;
		virtual void finalize() = 0;
		virtual void run() = 0;
		virtual void postWaitRemoteRDMAWrite(Unit* srcUnit,int rank,int unitId,void * ptr,size_t size,int uuid,const UnitCommand & postCommand) = 0;
		virtual void rdmaWrite(Unit* srcUnit,int rank,int unitId,void * src,void * dest,uint64_t destKey,size_t size, int uuid,const UnitCommand & postCommand) = 0;
		virtual void sendCommand(Unit * srcUnit,const UnitId destUnit,const UnitCommand & command);
		virtual void sendCommand(Unit * srcUnit,int rank,int unitId,const UnitCommand & command) = 0;
		virtual void sendCommand(Unit * srcUnit,UnitType unitType,const UnitCommand & command);
		virtual void sendCommand(const UnitCommand & command) = 0;
		virtual void * allocateRdmaSegment(size_t size, bool write);
		void deallocateRdmaSegment( void* ptr, size_t size, bool write );
		virtual RemoteIOV getRemoteIOV( int targetRank, void* addr, size_t size ) = 0;
		void registerSegment(void * addr,size_t size);
		void unregisterSegment(void * addr,size_t size);
		uint64_t getSegmentRemoteKey(void * addr,size_t size);
		int getRank();
		int getWorldSize();
		const std::string & getName() const;
		UnitType getUnitType(int rank,int unitId);
		void setUnitType(int rank,int unitId, UnitType type);
		void setLocalUnit(int unitId,Unit * unit);
		void buildLocalUnits();
		int getUnitId(Unit * unit) const;
		int countUnits(UnitType type) const;
		const char * getUnitName(int rank,int unitId);
		Unit * getLocalUnit(int unitId);
		void setExit();
		int getGblIdOfUnit( int rank, int unitId );
		int getGblIdOfUnit( Unit * unit );
		bool isDead(int rank);
		UnitId getIndexedUnitId(UnitType type, int index);
		void clear();
	protected:
		void sendStartToAllLocalUnits();
		void startLocalUnitThreads();
		void stopLocalUNitThreds();
		void updateUnits();
		void markDead(int rank);
		Unit * buildFilterUnit(void);
	protected:
		/** Keep track of the global config object **/
		const Config * config;
		/** Keep track of the launcher **/
		Launcher * launcher;
		/** Array of unit type **/
		UnitType * unitTypes;
		/** Pointer to the network driver. **/
		Driver * driver;
		/** Instantiation of the localUnits **/
		std::array<Unit*, DAQ_MAX_UNITS_PER_PROCESS> localUnits {};
		bool hasExit;
		/** Enable usage of threads in units. **/
		bool useUnitThreads;
	private:
		/** Name of the driver **/
		std::string name;
		/** Number of units of each type **/
		std::array<int, UNIT_TYPE_COUNT> count {};
		/** List of dead nodes for failure mitigation **/
		bool * deadNodes;
		/** Filter unit type **/
		std::string fuType;
};

/*******************  FUNCTION  *********************/
std::ostream & operator << (std::ostream & out,const UnitId & unitId);
std::ostream & operator << (std::ostream & out,UnitType type);
UnitId buildUnitId(int rank,int id);

}

#endif //DAQ_TRANSPORT_HPP
