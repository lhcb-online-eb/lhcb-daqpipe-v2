/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include <gtest/gtest.h>
#include "DummyDataSource.hpp"
#include <launcher/LauncherLocal.hpp>
#include <drivers/DriverLocal.hpp>
#include <transport/TransportMT.hpp>

/***************** USING NAMESPACE ******************/
using namespace DAQ;

/*******************  FUNCTION  *********************/
TEST(DummyDataSource,constructor)
{
	Config config;
	DriverLocal driver;
	LauncherLocal launcher(&config);
	TransportMT transport(&config,&launcher,&driver);
	DummyDataSource datasource(&config, &transport);
}

/*******************  FUNCTION  *********************/
TEST(DummyDataSource,registerSegments)
{
	Config config;
	LauncherLocal launcher(&config);
	DriverLocal driver;
	TransportMT transport(&config,&launcher,&driver);
	DummyDataSource datasource(&config, &transport);
	
	datasource.registerSegments(&transport);
}

/*******************  FUNCTION  *********************/
TEST(DummyDataSource,getBucket)
{
	Config config;
	config.dataSizeRandomRatio = 0;
	DriverLocal driver;
	LauncherLocal launcher(&config);
	TransportMT transport(&config,&launcher,&driver);
	DummyDataSource datasource(&config, &transport);

	datasource.setId(0);
	
	BucketInfo bucket;
	datasource.getBucket(bucket,10);
	EXPECT_EQ(10,bucket.eventId);
	EXPECT_EQ( config.getEventCollisions() * config.collisionRailDataSize,bucket.dataFragments[0].size);
	EXPECT_EQ( EventMetaData::getTotalSize(config.getEventCollisions()),bucket.metaFragments[0].size);
}

/*******************  FUNCTION  *********************/
TEST(DummyDataSource,ackSent)
{
	Config config;
	config.dataSizeRandomRatio = 0;
	DriverLocal driver;
	LauncherLocal launcher(&config);
	TransportMT transport(&config,&launcher,&driver);
	DummyDataSource datasource(&config, &transport);

	datasource.setId(0);
	
	datasource.ackSent(10);
}

/*******************  FUNCTION  *********************/
TEST(DummyDataSource,ackSent2)
{
	Config config;
	config.dataSizeRandomRatio = 0;
	DriverLocal driver;
	LauncherLocal launcher(&config);
	TransportMT transport(&config,&launcher,&driver);
	DummyDataSource datasource(&config, &transport);

	datasource.setId(0);
	
	BucketInfo bucket;
	datasource.getBucket(bucket,10);
	datasource.ackSent(bucket);
}

/*******************  FUNCTION  *********************/
TEST(DummyDataSource,getTotalSize)
{
	Config config;
	config.dataSizeRandomRatio = 0;
	DriverLocal driver;
	LauncherLocal launcher(&config);
	TransportMT transport(&config,&launcher,&driver);
	DummyDataSource datasource(&config, &transport);

	datasource.setId(0);
	
	EXPECT_EQ(DAQ_DATA_SOURCE_RAILS*(config.getEventCollisions() * config.collisionRailDataSize+EventMetaData::getTotalSize(config.getEventCollisions())),datasource.getTotalSize(10));
}

/*******************  FUNCTION  *********************/
TEST(DummyDataSource,isEventAvailale)
{
	Config config;
	config.dataSizeRandomRatio = 0;
	DriverLocal driver;
	LauncherLocal launcher(&config);
	TransportMT transport(&config,&launcher,&driver);
	DummyDataSource datasource(&config, &transport);

	datasource.setId(0);
	
	EXPECT_TRUE(datasource.IsEventAvailable(10));
	EXPECT_FALSE(datasource.IsEventAvailable(100000));
}

/*******************  FUNCTION  *********************/
TEST(DummyDataSource,operatorStream)
{
	BucketCheck bcheck;
	bcheck.fill(16,0,0,0,0);
	std::stringstream out;
	out << bcheck;
}
