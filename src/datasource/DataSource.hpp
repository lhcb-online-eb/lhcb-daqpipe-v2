/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_DATA_SOURCE_HPP
#define DAQ_DATA_SOURCE_HPP

/********************  HEADERS  *********************/
#include <cstdlib>
#include <ostream>

/********************  MACRO  ***********************/
/** Number of source to read, will be 2 with the current pcie40 board driver.**/
#define DAQ_DATA_SOURCE_RAILS 2

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*********************  TYPES  **********************/
class Config;
class Transport;

/*********************  STRUCT  *********************/
/**
 * Define the content of a meta-data entry.
**/
struct EventMetaDataEntry
{
    /** Define the size of data segment in bytes **/
	uint16_t size;
};

/*********************  STRUCT  *********************/
/**
 * Describe a meta-data segment. This use functions to compute the daata location because
 * this struct has not a fixed size.
**/
struct __attribute__((__packed__)) EventMetaData
{
    /** Event Id of the first entry **/
	char magic[6]; //"META40", but can be overwritten by the EB
	uint16_t frags;
	uint64_t baseEventId;
	uint64_t offset; 
	static size_t getTotalSize(size_t collisions);
	EventMetaData & getEvent(size_t id,size_t collisions);
	EventMetaDataEntry & getEntry(size_t id);
};

/*********************  STRUCT  *********************/
/**
 * Define a data fragment.
**/
struct Fragment
{
    /** Base address of the fragment. **/
	char * base;
    /** Size of the fragment. **/
	size_t size;
};

/********************  STRUCT  **********************/
/**
 * Checking structure to be put inside fragment for checking.
**/
struct BucketCheck
{
    /** Size of the fragment. **/
	size_t size;
    /** Current position of the bucket checker **/
	size_t pos;
    /** Event id **/
	size_t eventId;
    /** Rail id **/
	short int rail;
    /** Source unit id **/
	short int unitId;
	//functions
	void fill(size_t size,size_t pos,short int rail,short int unitId, size_t eventId);
	void check(size_t size,size_t pos,short int rail,short int unitId, size_t eventId);
};

/*******************  FUNCTION  *********************/
bool operator == (const BucketCheck & a,const BucketCheck & b);
std::ostream & operator << ( std::ostream& out, const DAQ::BucketCheck& bucket );

/*********************  STRUCT  *********************/
/**
 * Description of a bucket to be used when requesting data. If stores the fragments for data
 * and meta data, this for each rail.
**/
struct BucketInfo
{
    /** Data fragments for each rail **/
	Fragment dataFragments[DAQ_DATA_SOURCE_RAILS];
    /** Meta-data fragment for each rail **/
	Fragment metaFragments[DAQ_DATA_SOURCE_RAILS];
    /** Id refering to the position in the memory buffer **/
	int id;
    /** Event ID **/
	uint64_t eventId;
	//functions
	void check(const DAQ::Config& config, int unitId );
	void fillForcheck(const DAQ::Config& config, int unitId );
};

/*********************  CLASS  **********************/
/**
 * Abstract definition of a data source.
**/
class DataSource
{
	public:
		DataSource(const Config * config, Transport * transport);
		virtual ~DataSource() = default;
		virtual void getBucket(BucketInfo & bucket,uint64_t eventId) = 0;
		virtual void ackSent(BucketInfo & bucket) = 0;
		virtual void ackSent( uint64_t eventId) = 0;
		virtual size_t getTotalSize(uint64_t eventId) = 0;
		virtual bool IsEventAvailable(uint64_t currentEventId) = 0;
		virtual void registerSegments( Transport * transport ) = 0;
		virtual int getFilledPercentage() = 0;
		virtual void setId ( int unitId ) {};//for debugging
	protected:
        /** Keep track of the config object. **/
		const Config * config;
        /** Keep trakc of the transport object **/
		Transport * transport;
};

/*******************  FUNCTION  *********************/
inline EventMetaDataEntry& EventMetaData::getEntry ( size_t id )
{
	EventMetaDataEntry * entries = reinterpret_cast<EventMetaDataEntry*>(this+1);
	return entries[id];
}

/*******************  FUNCTION  *********************/
inline EventMetaData& EventMetaData::getEvent ( size_t id, size_t collisions )
{
	char * buffer =  reinterpret_cast<char*>(this);
	buffer += getTotalSize(collisions) * id;
	return *reinterpret_cast<EventMetaData*>(buffer);
}

/*******************  FUNCTION  *********************/
inline size_t EventMetaData::getTotalSize ( size_t collisions )
{
	return sizeof (EventMetaData) + sizeof(EventMetaDataEntry) * collisions;
}

}

#endif //DAQ_DATA_SOURCE_HPP
