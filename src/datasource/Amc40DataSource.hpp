/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_AMC40_DATA_SOURCE_HPP
#define DAQ_AMC40_DATA_SOURCE_HPP

/********************  HEADERS  *********************/
#include "DataSource.hpp"
#include "amc40/Amc40Reader.hpp"
#include <transport/Transport.hpp>

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*********************  CLASS  **********************/
/**
 * Implement a data source for the AMC40. It uses intrenally the AMC40 reader to read the data
 * and convert them in a format which is compatible with the PCIe-40 format.
**/
class Amc40DataSource : public DataSource
{
	public:
		Amc40DataSource( const DAQ::Config* config, DAQ::Transport* transport );
		virtual ~Amc40DataSource();
		virtual void getBucket ( BucketInfo& bucket, uint64_t eventId );
		virtual void ackSent ( BucketInfo& bucket );
		virtual void ackSent ( uint64_t eventId);
		virtual void registerSegments( Transport * transport );
		virtual size_t getTotalSize ( uint64_t eventId );
		virtual bool IsEventAvailable(uint64_t currentEventId);
		virtual int getFilledPercentage();
	private:
		int getDataSize( int id, int rail );
		int getId(uint64_t eventId);
	private:
		/** Pointer for data buffer for each rail **/
		char * data[DAQ_DATA_SOURCE_RAILS];
		/** Pointer for meta-data buffer for each rail **/
		EventMetaData * meta[DAQ_DATA_SOURCE_RAILS];
		/** Pointer to the array of event ids (mapping) **/
		uint64_t * eventIds;
		/** Keep track of the number of event stored into the buffers **/
		int storedEvents;
		/** Keep track of the maximal size of an event (mep) **/
		size_t maxEventSize;
		/** NUmber of collisions into an event (mep). **/
		size_t eventCollisions;
		/** AMC 40 reader to be used to fetch and convert the data from the AMC40 **/
		Amc40Reader * reader;
};

}

#endif //DAQ_AMC40_DATA_SOURCE_HPP
