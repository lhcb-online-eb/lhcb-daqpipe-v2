/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include <cassert>
#include <cstring>
#include <common/Debug.hpp>
#include "Amc40DataSource.hpp"

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*******************  FUNCTION  *********************/
/**
 * Constructor of the data source.
 * @param config Define the global confiration object.
 * @param transport Define the transport object to allocate buffers via driver function.
**/
Amc40DataSource::Amc40DataSource ( const Config* config, Transport * transport )
                :DataSource(config,transport)
{
	DAQ_DEBUG("amc40","Create amc40 data source");
	
	//extract
	this->storedEvents = config->readoutUnitEvents;
	this->maxEventSize = config->eventRailMaxDataSize;
	this->eventCollisions = config->getEventCollisions();

	//TODO remove from config or as function
	assume(config->getDefaultMetaDataSize() >= EventMetaData::getTotalSize(eventCollisions),"Invalid too small meta data size configured in config object");
	
	//allocate buffers
	for (int rail = 0 ; rail < DAQ_DATA_SOURCE_RAILS ; rail++) {
		this->data[rail] = (char*)transport->allocateRdmaSegment(maxEventSize * storedEvents,false);
		this->meta[rail] = (EventMetaData*)transport->allocateRdmaSegment(EventMetaData::getTotalSize(eventCollisions) * storedEvents,false);
		memset(this->data[rail],0,maxEventSize * storedEvents);
		memset(this->meta[rail],0,EventMetaData::getTotalSize(eventCollisions) * storedEvents);
	}
	
	this->eventIds = new uint64_t[storedEvents];
	
	//clear ids
	for (int i = 0 ; i < storedEvents ; i++)
		eventIds[i] = 0;
	
	//setup AMC40 reader
	assert(DAQ_DATA_SOURCE_RAILS >= 2);
	reader = new Amc40Reader(config,this->meta[0],this->data[0],this->meta[1],this->data[1],eventIds,storedEvents,0,true);
}

/*******************  FUNCTION  *********************/
/**
 * Destructor of the data source. It mostly deallocate the segments and stop the reader.
**/
Amc40DataSource::~Amc40DataSource ()
{
	//stop thre aders
	delete reader;
	
	//now can free the memory
	for (int rail = 0 ; rail < DAQ_DATA_SOURCE_RAILS ; rail++) {
		this->transport->deallocateRdmaSegment(this->data[rail],maxEventSize * storedEvents,false);
		this->transport->deallocateRdmaSegment(this->meta[rail],EventMetaData::getTotalSize(eventCollisions) * storedEvents,false);
	}
	delete [] this->eventIds;
}

/*******************  FUNCTION  *********************/
/**
 * Function used to register the segment to the driver.
 * @param transport Transport object to get indirect access to the driver.
**/
void Amc40DataSource::registerSegments ( Transport* transport )
{
	for (int rail = 0 ; rail < DAQ_DATA_SOURCE_RAILS ; rail++) {
		transport->registerSegment(this->data[rail],maxEventSize * storedEvents);
		transport->registerSegment(this->meta[rail],EventMetaData::getTotalSize(eventCollisions) * storedEvents);
	}
}

/*******************  FUNCTION  *********************/
/**
 * Get the size of the data segment for given id and rail. To obtain it read the meta data entries.
 * @param id Define the ID of the data we want the size.
 * @param rail Define the rail for which to read the meta-data.
**/
int Amc40DataSource::getDataSize ( int id, int rail )
{
	size_t size = 0;
	for (size_t colId = 0 ; colId < eventCollisions ; colId++ )
		size += meta[rail]->getEvent(id,eventCollisions).getEntry(colId).size;
	assert(size <= maxEventSize);
	return size;
}

/*******************  FUNCTION  *********************/
/**
 * Build a bucket with the pointer to the data and meta data for the given event ID.
 * @param bucket Bucket to fill with the requested pointers.
 * @param eventId Define the event ID to search for in the event list. It will crash if not found.
**/
void Amc40DataSource::getBucket ( BucketInfo& bucket, uint64_t eventId )
{
	//search event Id in list
	int id = getId(eventId);
	
	//setup meta
	bucket.eventId = eventId;
	bucket.id = id;
	
	for (int rail = 0 ; rail < DAQ_DATA_SOURCE_RAILS ; rail++) {
		//compute read ids
		int rrail = rail;
		int rid = id;
		
		//seutp segments
		bucket.metaFragments[rrail].base = reinterpret_cast<char*>(&(meta[rrail]->getEvent(rid,eventCollisions)));
		bucket.metaFragments[rrail].size = EventMetaData::getTotalSize(eventCollisions);
		bucket.dataFragments[rrail].base = &data[rrail][rid * maxEventSize];
		bucket.dataFragments[rrail].size = getDataSize(rid,rrail);
		
		//some debugging
		DAQ_DEBUG_ARG("datasource","setup meta %1").arg((void*)bucket.metaFragments[rrail].base).end();
		DAQ_DEBUG_ARG("datasource","setup data %1").arg((void*)bucket.dataFragments[rrail].base).end();
	}
}

/*******************  FUNCTION  *********************/
/**
 * Ack that the bucket has been sent to we can free the related buffers to next reuse.
 * @param bucket Define the bucket to free.
**/
void Amc40DataSource::ackSent ( BucketInfo& bucket )
{
	//checks
	assume(bucket.id >= 0 && bucket.id < storedEvents,"Invalid ID");
	assume(bucket.eventId == eventIds[bucket.id],"Invalid event ID to ack event sending");
	DAQ_DEBUG_ARG("Amc40DataSource","Ack Sent (bucket) with ID = %1").arg(bucket.id).end();
	
	//update statuss
	eventIds[bucket.id] = 0;
}

/*******************  FUNCTION  *********************/
/**
 * Ack that the bucket has been sent to we can free the related buffers to next reuse.
 * @param eventId Define the eventID to free.
**/
void Amc40DataSource::ackSent ( uint64_t eventId )
{
	//search event Id in list
	int id = 0;
	while (id < this->storedEvents && this->eventIds[id] != eventId)
		id++;
	
	//check if no room
	assumeArg(id < this->storedEvents,"Cannot find requested eventId (%1) to make it done").arg(eventId).end();
	DAQ_DEBUG_ARG("Amc40DataSource","Ack Sent (eventOd) with ID = %1 : %2").arg(eventId).arg(id).end();
	
	//update statuss
	eventIds[id] = 0;
}

/*******************  FUNCTION  *********************/
/**
 * Search for the position into the data for the given eventId.
 * @param eventId Define the event id to search. If not found it will crash.
 * @return The position of the given event in the data/meta buffers.
**/
int Amc40DataSource::getId ( uint64_t eventId )
{
	//search event Id in list
	int id = 0;
	while (id < this->storedEvents && this->eventIds[id] != eventId)
		id++;

	//check if no room
	/*if (id >= this->storedEvents)
	{
		for (int i = 0 ; i < this->storedEvents ; i++)
			DAQ_INFO_ARG("%1").arg(this->eventIds[i]).end();
	}*/
	assumeArg(id < this->storedEvents,"Request invalid event, not in available data, maybe there is contention somewhere and ressources are exhausted (evtId = %1)").arg(eventId).end();
	//DAQ_DEBUG_ARG("Amc40DataSource","Get Id %1").arg(id).end();

	return id;
}

/*******************  FUNCTION  *********************/
/**
 * Compute the total data and meta data size for the given event id.
 * @param eventId Define the event ID for which to compute the size. If not found it will abort.
**/
size_t Amc40DataSource::getTotalSize ( uint64_t eventId )
{
	int id = getId(eventId);
	size_t res = 0;
	for (int rail = 0 ; rail < DAQ_DATA_SOURCE_RAILS ; rail++)
		res += getDataSize(id,rail) + config->getDefaultMetaDataSize();
	return res;
}

/*******************  FUNCTION  *********************/
/**
 * Check if the given eventId is available.
 * @param currentEventId Define the event ID to check.
**/
bool Amc40DataSource::IsEventAvailable ( uint64_t currentEventId )
{
	//search event Id in list
	int id = 0;
	while (id < this->storedEvents && this->eventIds[id] != currentEventId)
		id++;

	return id < this->storedEvents;
}

/*******************  FUNCTION  *********************/
/**
 * Check how the current buffers are field to be reported into htopml.
**/
int Amc40DataSource::getFilledPercentage()
{
	int nonZeroEvents = 0; 
	for(int i = 0; i < storedEvents; i++ )
	{
		if (this->eventIds[i] != 0)
			nonZeroEvents ++;
	}
	return int((nonZeroEvents/storedEvents)*100);
}

}
