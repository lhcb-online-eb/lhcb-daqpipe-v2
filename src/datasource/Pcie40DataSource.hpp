/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_PCIE40_DATA_SOURCE_HPP
#define DAQ_PCIE40_DATA_SOURCE_HPP

/********************  HEADERS  *********************/
#include "DataSource.hpp"
#include "gochannels/GoChannel.hpp"
#include "transport/Transport.hpp"
#include <mutex>
#include <thread>

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/********************  MACROS  **********************/
#define DAQ_STREAM_DATA 0
#define DAQ_STREAM_META 1
#define DAQ_STREAM_COUNT 2

/*********************  CLASS  **********************/
/**
 * Implement a data source for the PCIe40. 
**/
class Pcie40DataSource : public DataSource
{
	public:
		Pcie40DataSource( const DAQ::Config* config, DAQ::Transport* transport );
		virtual ~Pcie40DataSource();
		virtual void getBucket ( BucketInfo& bucket, uint64_t eventId );
		virtual void ackSent ( BucketInfo& bucket );
		virtual void ackSent ( uint64_t eventId);
		virtual void registerSegments( Transport * transport );
		virtual size_t getTotalSize ( uint64_t eventId );
		virtual bool IsEventAvailable(uint64_t currentEventId);
		virtual int getFilledPercentage();
	private:
		size_t pollMeta( size_t& baseOffset );
		void internalAck(int id);
		void polling(void);
		int getDataSize( int id, int rail );
		int getId(uint64_t eventId);
		size_t computeDataSize ( int rail, int activeBase, int cnt );
		EventMetaData * getEventMetaData(int id, int rail);
		void flush(void);
		void buildHwRail(int * hwRails,const Config * config, int rank);
	private:
		/** Driver stream handler **/
		int streamHandler[DAQ_DATA_SOURCE_RAILS][DAQ_STREAM_COUNT];
		/** Pointer for data buffer for each rail **/
		char * data[DAQ_DATA_SOURCE_RAILS];
		/** Pointer for meta-data buffer for each rail **/
		EventMetaData * meta[DAQ_DATA_SOURCE_RAILS];
		/** Pointer to the array of event ids (mapping) **/
		uint64_t * eventIds;
		/** Event offset **/
		size_t * eventOffset;
		/** Keep track of the number of event stored into the buffers **/
		int storedEvents;
		/** Keep track of the maximal size of an event (mep) **/
		size_t maxEventSize;
		/** NUmber of collisions into an event (mep). **/
		size_t eventCollisions;
		/** Handler of polling thread **/
		std::thread pollingThreadHandler;
		/** Permit to exit the polling thread **/
		bool continuePolling;
		/** Base id of active part of the segment **/
		/** Data buffer size **/
		size_t dataBufferSize;
		/** polling delay to wait **/
		int pollingDelay;
		/** Size of a collission fragment **/
		size_t collisionRailDataSize;
		/** Store the meta size to apply modulos **/
		size_t metaSize;
		/** Say to polling thread that he can flush **/
		bool needFlush;
		/** base event id **/
		size_t baseEventId;
		/** base id **/
		size_t baseId;
};

}

#endif //DAQ_PCIE40_DATA_SOURCE_HPP
