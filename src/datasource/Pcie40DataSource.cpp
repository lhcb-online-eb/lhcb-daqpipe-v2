/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
//std
#include <cassert>
#include <cstring>
//PCIE40 driver
#include <lhcb/pcie40/daq.h>
//daqpipe
#include <common/Debug.hpp>
//impl
#include "Pcie40DataSource.hpp"

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/********************  MACRO  ***********************/
#define min(a,b) (((a) < (b))?(a):(b))
#define max(a,b) (((a) > (b))?(a):(b))

/*******************  FUNCTION  *********************/
/**
 * Constructor of the data source.
 * @param config Define the global confiration object.
 * @param transport Define the transport object to allocate buffers via driver function.
**/
Pcie40DataSource::Pcie40DataSource ( const Config* config, Transport * transport )
                :DataSource(config,transport)
{
	//info
	DAQ_DEBUG("pcie40","Create pci40 data source");
	DAQ_DEBUG_ARG("pcie40","MEP collisions : %1").arg(config->getEventCollisions()).end();
	
	//check
	assume(sizeof(EventMetaData) == sizeof(p40_hdr_meta),"Struct sizes do not match !");
	
	//extract
	this->storedEvents = config->readoutUnitEvents;
	this->maxEventSize = config->eventRailMaxDataSize;
	this->eventCollisions = config->getEventCollisions();
	this->collisionRailDataSize = config->collisionRailDataSize;
	this->dataBufferSize = 0;
	this->needFlush = false;
	this->baseEventId = 10;
	this->baseId = 0;
	
	//param
	int railBase = config->getAndSetConfigInt("dataSource.pcie40","baseDeviceId",0);
	this->pollingDelay = config->getAndSetConfigInt("dataSource.pcie40","pollingDelay",100);

	//TODO remove from config or as function
	assume(config->getDefaultMetaDataSize() >= EventMetaData::getTotalSize(eventCollisions),"Invalid too small meta data size configured in config object");
	
	//check size
	assume(sizeof(EventMetaData) == sizeof(p40_hdr_meta),"Invalid size for EventMetaData, do not match the driver one !");
	
	//build hwrails
	int hwRails[DAQ_DATA_SOURCE_RAILS];
	this->buildHwRail(hwRails,config, transport->getRank());
	
	//open device & streams & map to memory
	for (int rail = 0 ; rail < DAQ_DATA_SOURCE_RAILS ; rail++)
	{
		//debug
		DAQ_DEBUG_ARG("pcie40","Openn map and enable streams on rail %1 (%2)").arg(rail).arg(rail+railBase).end();
		
		//open
		this->streamHandler[rail][DAQ_STREAM_DATA] = p40_stream_open(hwRails[rail]+railBase,P40_DAQ_STREAM_MAIN);
		this->streamHandler[rail][DAQ_STREAM_META] = p40_stream_open(hwRails[rail]+railBase,P40_DAQ_STREAM_META);
		
		//check
		assume(this->streamHandler[rail][DAQ_STREAM_DATA] >= 0,"Fail to open data stream !");
		assume(this->streamHandler[rail][DAQ_STREAM_META] >= 0,"Fail to open meta stream !");

		//compute store event
		//size_t cardStoreEvents = p40_stream_get_host_buf_bytes(this->streamHandler[rail][DAQ_STREAM_META]) / EventMetaData::getTotalSize(eventCollisions);
		//assumeArg(cardStoreEvents == storedEvents,"Incalid number for storedEvents, card: %1, daqpipe %2").arg(cardStoreEvents).arg(storedEvents).end();
		if (rail == 0)
			metaSize = (size_t)p40_stream_get_host_buf_bytes(this->streamHandler[rail][DAQ_STREAM_META]);
		else
			assume(metaSize == (size_t)p40_stream_get_host_buf_bytes(this->streamHandler[rail][DAQ_STREAM_META]),"Invalid meta size on one rail !");
			
		//check meta data size
		//assumeArg((size_t)p40_stream_get_host_buf_bytes(this->streamHandler[rail][DAQ_STREAM_META]) == (size_t)storedEvents * EventMetaData::getTotalSize(eventCollisions)
		//          ,"Invalid size of meta-data card: %1, daqpipe: %2").arg(p40_stream_get_host_buf_bytes(this->streamHandler[rail][DAQ_STREAM_META])).arg((size_t)storedEvents * EventMetaData::getTotalSize(eventCollisions)).end();
		
		//TODO remove
		size_t cardStoreEvents = (p40_stream_get_host_buf_bytes(this->streamHandler[rail][DAQ_STREAM_META]) / EventMetaData::getTotalSize(eventCollisions)) + 1;
		assumeArg(cardStoreEvents == (size_t)storedEvents,"Invalid number for storedEvents, card: %1, daqpipe %2").arg(cardStoreEvents).arg(storedEvents).end();
		
		//map to memory
		DAQ_DEBUG_ARG("pcie40","Map memory on rail %1").arg(rail).end();
		this->data[rail] = static_cast<char*>(p40_stream_map(this->streamHandler[rail][DAQ_STREAM_DATA]));
		this->meta[rail] = static_cast<EventMetaData*>(p40_stream_map(this->streamHandler[rail][DAQ_STREAM_META]));
		
		//check
		assumeArg(this->data[rail] != NULL,"Fail to map data stream for rail %1!").arg(rail).end();
		assumeArg(this->meta[rail] != NULL,"Fail to map meta stream for rail %1!").arg(rail).end();
		
		//size
		if (dataBufferSize == 0)
			dataBufferSize = p40_stream_get_host_buf_bytes(this->streamHandler[rail][DAQ_STREAM_DATA]);
		else
			assume(dataBufferSize == (size_t)p40_stream_get_host_buf_bytes(this->streamHandler[rail][DAQ_STREAM_DATA]),"The two data buffer do not have the same size !");
	}

	//enable all
	for (int rail = 0 ; rail < DAQ_DATA_SOURCE_RAILS ; rail++)
	{
		//enable
		//DAQ_DEBUG_ARG("pcie40","Disable streams on rail %1").arg(rail).end();
		//assumeArg(p40_stream_disable(this->streamHandler[rail][DAQ_STREAM_DATA]) >= 0,"Fail to disable data stream for rail %1").arg(rail).end();
		//assumeArg(p40_stream_disable(this->streamHandler[rail][DAQ_STREAM_META]) >= 0,"Fail to disable meta stream for rail %1").arg(rail).end();
		
		//enable
		DAQ_DEBUG_ARG("pcie40","Enable streams on rail %1").arg(rail).end();
		assumeArg(p40_stream_enable(this->streamHandler[rail][DAQ_STREAM_DATA]) >= 0,"Fail to enable data stream for rail %1").arg(rail).end();
		assumeArg(p40_stream_enable(this->streamHandler[rail][DAQ_STREAM_META]) >= 0,"Fail to enable meta stream for rail %1").arg(rail).end();
		
		//check ready
		DAQ_DEBUG_ARG("pcie40","Check ready on rail %1").arg(rail).end();
		assumeArg(p40_stream_ready(this->streamHandler[rail][DAQ_STREAM_DATA]) >= 0,"Fail to check ready data stream for rail %1").arg(rail).end();
		assumeArg(p40_stream_ready(this->streamHandler[rail][DAQ_STREAM_META]) >= 0,"Fail to check ready meta stream for rail %1").arg(rail).end();
	}
	
	this->eventIds = new uint64_t[storedEvents];
	this->eventOffset = new size_t[storedEvents];
	
	//clear ids
	for (int i = 0 ; i < storedEvents ; i++)
	{
		eventIds[i] = 0;
		eventOffset[i] = -1;
	}
	
	//spawn polling threads
	this->continuePolling = true;
	this->pollingThreadHandler = std::thread([this]{
		this->polling();
	});
}

/*******************  FUNCTION  *********************/
/**
 * Destructor of the data source. It mostly deallocate the segments and stop the reader.
**/
Pcie40DataSource::~Pcie40DataSource ()
{
	//stop polling thread
	this->continuePolling = false;
	this->pollingThreadHandler.join();
	
	//open device & streams & map to memory
	for (int rail = 0 ; rail < DAQ_DATA_SOURCE_RAILS ; rail++)
	{
		//debug
		DAQ_DEBUG_ARG("pcie40","Unmap and disable rail %1").arg(rail).end();
		
		transport->unregisterSegment(this->data[rail],dataBufferSize);//maxEventSize * storedEvents;
		transport->unregisterSegment(this->meta[rail],EventMetaData::getTotalSize(eventCollisions) * storedEvents);
		
		//disable
		assume(p40_stream_disable(this->streamHandler[rail][DAQ_STREAM_DATA]) >= 0,"Fail to enable data stream");
		assume(p40_stream_disable(this->streamHandler[rail][DAQ_STREAM_META]) >= 0,"Fail to enable meta stream");
		
		//close & munmap
		p40_stream_close(this->streamHandler[rail][DAQ_STREAM_DATA], this->data[rail]);
		p40_stream_close(this->streamHandler[rail][DAQ_STREAM_META], this->meta[rail]);
	}
	
	delete [] eventIds;
	delete [] eventOffset;
}

/*******************  FUNCTION  *********************/
/**
 * Used to extract the harware rails mapping to possibly mix them into multiple
 * readout units on the same node.
 * @param hwRails The mapping storage to fill, used by the caller afterwards.
 * @param config The config object to access the json content.
 * @param rank The current rank to extract the node specific data.
**/
void Pcie40DataSource::buildHwRail(int * hwRails,const Config * config, int rank)
{
	//get config
	Json::Value infos = config->getConfig("dataSource.pcie40.hwRails");
	
	//mode
	if (infos.isNull())
	{
		for (int i = 0 ; i < DAQ_DATA_SOURCE_RAILS ; i++)
			hwRails[i] = i;
	} else if (infos.isObject()) {
		char buffer[64];
		sprintf(buffer,"%d",rank);
		Json::Value v = infos.get(buffer,infos.get("default",Json::Value()));
		if (v.isNull())
		{
			for (int i = 0 ; i < DAQ_DATA_SOURCE_RAILS ; i++)
				hwRails[i] = i;
		} else if (v.isArray()) {
			assume(v.size() == DAQ_DATA_SOURCE_RAILS,"Invalid size, missing hwrails ID !");
			for (int i = 0 ; i < DAQ_DATA_SOURCE_RAILS ; i++)
			{
				hwRails[i] = v[i].asInt();
				DAQ_INFO_ARG("Swap PCIe40 hardware rail : %1 -> %2").arg(i).arg(hwRails[i]).end();
			}
		} else {
			DAQ_FATAL("Invalid entry for dataSource.pcie40.hwRails[x], should be an array !");
		}
	} else {
		DAQ_FATAL("Invalid entry for dataSource.pcie40.hwRails, should be an object with ranks as index !");
	}
}

/*******************  FUNCTION  *********************/
/**
 * Function used to register the segment to the driver.
 * @param transport Transport object to get indirect access to the driver.
**/
void Pcie40DataSource::registerSegments ( Transport* transport )
{
	for (int rail = 0 ; rail < DAQ_DATA_SOURCE_RAILS ; rail++) {
		transport->registerSegment(this->data[rail],dataBufferSize);//maxEventSize * storedEvents;
		transport->registerSegment(this->meta[rail],EventMetaData::getTotalSize(eventCollisions) * storedEvents);
	}
}

/*******************  FUNCTION  *********************/
/**
 * Return the MetaData struct for a given ID and rail
 * @parma id Define which ID we want to fetch.
 * @param rail Define which rail we want.
**/
EventMetaData * Pcie40DataSource::getEventMetaData(int id, int rail)
{
	//check
	assert(id < storedEvents);
	assert(rail < DAQ_DATA_SOURCE_RAILS);
	//assert(eventIds[id] != 0);
	assert(eventOffset[id] != -1ul);
	
	//return
	return static_cast<EventMetaData*>((void*)(((char*)meta[rail])+eventOffset[id]));
}

/*******************  FUNCTION  *********************/
/**
 * Get the size of the data segment for given id and rail. To obtain it read the meta data entries.
 * @param id Define the ID of the data we want the size.
 * @param rail Define the rail for which to read the meta-data.
**/
int Pcie40DataSource::getDataSize ( int id, int rail )
{
	//TODO use entry in header
	size_t size = 0;
	EventMetaData * m = getEventMetaData(id,rail);
	for (size_t colId = 0 ; colId < eventCollisions ; colId++ )
		size += m->getEntry(colId).size;
	assumeArg(size <= maxEventSize,"Invalid size, too large : %1 > %2").arg(size).arg(maxEventSize).end();
	assert(size <= maxEventSize);
	return size;
}

/*******************  FUNCTION  *********************/
/**
 * Build a bucket with the pointer to the data and meta data for the given event ID.
 * @param bucket Bucket to fill with the requested pointers.
 * @param eventId Define the event ID to search for in the event list. It will crash if not found.
**/
void Pcie40DataSource::getBucket ( BucketInfo& bucket, uint64_t eventId )
{
	//search event Id in list
	int id = getId(eventId);
	
	//setup meta
	bucket.eventId = eventId;
	bucket.id = id;
	
	for (int rail = 0 ; rail < DAQ_DATA_SOURCE_RAILS ; rail++) {
		//compute read ids
		int rrail = rail;
		int rid = id;
		
		//get event meta
		EventMetaData * eventMeta = getEventMetaData(id,rail);
		
		//setup segments
		bucket.metaFragments[rrail].base = reinterpret_cast<char*>(eventMeta);
		bucket.metaFragments[rrail].size = EventMetaData::getTotalSize(eventCollisions);
		bucket.dataFragments[rrail].base = &data[rrail][eventMeta->offset];
		bucket.dataFragments[rrail].size = getDataSize(rid,rrail);

		//check
		assumeArg(bucket.dataFragments[rrail].size <= this->maxEventSize,"Invalid data size, collisionRailDataSize is too small to accept it on BU side (%1 > %2) !").arg(bucket.dataFragments[rrail].size).arg(this->maxEventSize).end();
		
		//some debugging
		DAQ_DEBUG_ARG("datasource","setup meta %1").arg((void*)bucket.metaFragments[rrail].base).end();
		DAQ_DEBUG_ARG("datasource","setup data %1").arg((void*)bucket.dataFragments[rrail].base).end();
	}
}

/*******************  FUNCTION  *********************/
/**
 * Ack that the bucket has been sent to we can free the related buffers to next reuse.
 * @param bucket Define the bucket to free.
**/
void Pcie40DataSource::ackSent ( BucketInfo& bucket )
{
	//checks
	assume(bucket.id >= 0 && bucket.id < storedEvents,"Invalid ID");
	assume(bucket.eventId == eventIds[bucket.id],"Invalid event ID to ack event sending");
	DAQ_DEBUG_ARG("Pcie40DataSource","Ack Sent (bucket) with ID = %1").arg(bucket.id).end();
	
	//update statuss
	internalAck(bucket.id);
}

/*******************  FUNCTION  *********************/
/**
 * Ack that the bucket has been sent to we can free the related buffers to next reuse.
 * @param eventId Define the eventID to free.
**/
void Pcie40DataSource::ackSent ( uint64_t eventId )
{
	//search event Id in list
	int id = 0;
	while (id < this->storedEvents && this->eventIds[id] != eventId)
		id++;
	
	//check if no room
	assumeArg(id < this->storedEvents,"Cannot find requested eventId (%1) to make it done").arg(eventId).end();
	DAQ_DEBUG_ARG("Pcie40DataSource","Ack Sent (eventOd) with ID = %1 : %2").arg(eventId).arg(id).end();
	
	//update status
	internalAck(id);
}

/*******************  FUNCTION  *********************/
/**
 * Compute the total size of data we can flush
**/
size_t Pcie40DataSource::computeDataSize ( int rail, int activeBase, int cnt )
{
	//id of next after last
	int next = (activeBase+cnt)%storedEvents;
	assume(eventIds[next] != 0,"Should have next to work !");//we might need to handle this....
	
	//get base offset
	size_t baseOffset = getEventMetaData(activeBase,rail)->offset;
	
	//get end offset
	size_t endOffset = getEventMetaData(next,rail)->offset;
	
	//apply modulo
	if (endOffset >= baseOffset)
		return endOffset - baseOffset;
	else
		return endOffset + dataBufferSize - baseOffset;
}

/*******************  FUNCTION  *********************/
/**
 * Update status and flush segement on driver if have free part.
 * @param id Id of the event to ack.
**/
void Pcie40DataSource::internalAck ( int id )
{
	DAQ_DEBUG_ARG("pcie40","Ack %1").arg(id).end();

	//update statuss
	eventIds[id] = 0;
	//eventOffset[id] = -1;
	
	//mark flush
	if ((size_t)id == baseId)
		needFlush = true;
}

/*******************  FUNCTION  *********************/
/**
 * Perform flush, moving back the event ids to beginning of segemnt and calling free opertation
 * on the board META/DATA segements.
**/
void Pcie40DataSource::flush ( void )
{
	//info
	//DAQ_DEBUG("pcie40","Do check flush");

	//search number
	int cnt = 0;
	while (cnt < storedEvents && eventIds[(baseId + cnt)%storedEvents] == 0) //&& eventOffset[cnt] != -1ul)
		cnt++;
		
	//nothing
	if (cnt <= 1)
		return;
			
	//we keep one to get offset of next
	//cnt--;

	//info
	DAQ_DEBUG("pcie40","Really do flush");

	//debug
	DAQ_DEBUG_ARG("pcie40","Flush %1 mep in the card !").arg(cnt).end();
		
	//flush data
	for (int rail = 0 ; rail < DAQ_DATA_SOURCE_RAILS ; rail++)
		p40_stream_free_host_buf_bytes(this->streamHandler[rail][DAQ_STREAM_DATA],computeDataSize(rail,baseId,cnt));
	
	//flush meta
	size_t metaSize = cnt * EventMetaData::getTotalSize(eventCollisions);
	for (int rail = 0 ; rail < DAQ_DATA_SOURCE_RAILS ; rail++)
		p40_stream_free_host_buf_bytes(this->streamHandler[rail][DAQ_STREAM_META],metaSize);
	
	//reset
	for (int i = 0 ; i < cnt ; i++)
	{
		eventIds[(baseId + i)%storedEvents] = 0;
		eventOffset[(baseId + i)%storedEvents] = -1ul;
	}

	//update base
	baseId = (baseId + cnt) % storedEvents;

	//mark done
	needFlush = false;
}

/*******************  FUNCTION  *********************/
/**
 * Search for the position into the data for the given eventId.
 * @param eventId Define the event id to search. If not found it will crash.
 * @return The position of the given event in the data/meta buffers.
**/
int Pcie40DataSource::getId ( uint64_t eventId )
{
	//search event Id in list
	int id = 0;
	while (id < this->storedEvents && this->eventIds[id] != eventId)
		id++;

	//check if no room
	/*if (id >= this->storedEvents)
	{
		for (int i = 0 ; i < this->storedEvents ; i++)
			DAQ_INFO_ARG("%1").arg(this->eventIds[i]).end();
	}*/
	assumeArg(id < this->storedEvents,"Request invalid event, not in available data, maybe there is contention somewhere and ressources are exhausted (evtId = %1)").arg(eventId).end();
	//DAQ_DEBUG_ARG("Pcie40DataSource","Get Id %1").arg(id).end();

	return id;
}

/*******************  FUNCTION  *********************/
/**
 * Compute the total data and meta data size for the given event id.
 * @param eventId Define the event ID for which to compute the size. If not found it will abort.
**/
size_t Pcie40DataSource::getTotalSize ( uint64_t eventId )
{
	int id = getId(eventId);
	size_t res = 0;
	for (int rail = 0 ; rail < DAQ_DATA_SOURCE_RAILS ; rail++)
		res += getDataSize(id,rail) + config->getDefaultMetaDataSize();
	return res;
}

/*******************  FUNCTION  *********************/
/**
 * Check if the given eventId is available.
 * @param currentEventId Define the event ID to check.
**/
bool Pcie40DataSource::IsEventAvailable ( uint64_t currentEventId )
{
	//search event Id in list
	int id = 0;
	while (id < this->storedEvents && this->eventIds[id] != currentEventId)
		id++;

	return id < this->storedEvents;
}

/*******************  FUNCTION  *********************/
/**
 * Check how the current buffers are field to be reported into htopml.
**/
int Pcie40DataSource::getFilledPercentage()
{
	int nonZeroEvents = 0; 
	for(int i = 0; i < storedEvents; i++ )
	{
		if (this->eventIds[i] != 0)
			nonZeroEvents ++;
	}
	return int((nonZeroEvents/storedEvents)*100);
}

/*******************  FUNCTION  *********************/
/**
 * Poll all the meta data rails and return common progress
**/
size_t Pcie40DataSource::pollMeta(size_t & baseOffset)
{
	//var
	baseOffset = -1ul;
	size_t finalSize = -1ul;
	
	//loop on all rails
	for (int rail = 0 ; rail < DAQ_DATA_SOURCE_RAILS ; rail++)
	{
		//poll
		loff_t tmpOffset = 0;
		ssize_t size = p40_stream_poll(this->streamHandler[rail][DAQ_STREAM_META],&tmpOffset,this->pollingDelay);
		assume(size >= 0,"Failure on p40_stream_poll() !");
		DAQ_DEBUG_ARG("pcie40v","p40_stream_poll(rail:%3) : %1 -> (%2)").arg(tmpOffset).arg(size).arg(rail).end();

		//store
		if (baseOffset == -1ul)
			baseOffset = tmpOffset;
		else
			assumeArg(baseOffset == (size_t)tmpOffset,"Not same offset on the two meta segment : %1 != %2 !").arg(baseOffset).arg(tmpOffset).end();
		
		//compute min common size
		finalSize = min(finalSize,(size_t)size);
	}

	//return
	return finalSize;
}

/*******************  FUNCTION  *********************/
/**
 * Perform the active polling on meta-data to track send progress and mark recived event
 * as ready.
**/
void Pcie40DataSource::polling ( void )
{
	while(continuePolling)
	{
		//flush
		if (needFlush)
			flush();
		
		//poll the two meta data
		size_t offset;
		size_t size = pollMeta(offset);
		
		//cnt
		size_t cnt = size / EventMetaData::getTotalSize(eventCollisions);
		DAQ_DEBUG_ARG("pcie40v","Get %1 meps from board from offset %2").arg(cnt).arg(offset).end();
		
		//import
		for (size_t i = 0 ; i < cnt ; i++)
		{			
			//current
			size_t off = (offset + i * EventMetaData::getTotalSize(eventCollisions))%metaSize;
			EventMetaData * cur = static_cast<EventMetaData*>((void*)((char*)meta[0] + off));
			assumeArg(cur->frags == eventCollisions,"Invalid fragment count in mep : offset %3 / %4 => %1 != %2 (magic : %5)").arg(cur->frags).arg(eventCollisions).arg(offset).arg(i).arg(cur->magic).end();
			
			//check
			for (int rail = 1 ; rail < DAQ_DATA_SOURCE_RAILS ; rail++)
				assumeArg(cur->baseEventId == static_cast<EventMetaData*>((void*)((char*)meta[rail] + off))->baseEventId,"Invalid event id %1 != %2 (magic = %3 = %4) (offset = %5 = %6) (frags = %7 = %8)")
					.arg(cur->baseEventId)
					.arg(static_cast<EventMetaData*>((void*)((char*)meta[rail] + off))->baseEventId)
					.arg(cur->magic)
					.arg(static_cast<EventMetaData*>((void*)((char*)meta[rail] + off))->magic)
					.arg(cur->offset)
					.arg(static_cast<EventMetaData*>((void*)((char*)meta[rail] + off))->offset)
					.arg(cur->frags)
					.arg(static_cast<EventMetaData*>((void*)((char*)meta[rail] + off))->frags)
					.end();
		
			//calc real id
			int id = (i + baseId)%storedEvents;
			
			//apply if need
			if (eventOffset[id] == -1ul)
			{
				eventOffset[id] = off;
				eventIds[id] = baseEventId + i;
			} else if (eventOffset[id] != off) {
				DAQ_FATAL_ARG("Invalid offset, baseId = %1, i = %2, %3 != %4!").arg(baseId).arg(i).arg(eventOffset[id]).arg(off).end();
			}
		}
	}
}

}
