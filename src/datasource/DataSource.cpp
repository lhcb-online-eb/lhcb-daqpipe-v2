/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include "DataSource.hpp"
#include "common/Debug.hpp"
#include <cassert>

/********************  MACROS  **********************/
/**
 * It does checking with more info to give hints on what is wrong in the data
 * but it is less strict by not comparing the content of meta data coherency with data.
**/
#define DETAIL_CHECK

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*******************  FUNCTION  *********************/
/**
 * Constructor of a data source.
 * @param config Pointer to the config object.
 * @param transport Pointer to the transport object.
**/
DataSource::DataSource ( const Config* config, Transport* transport )
{
	this->config = config;
	this->transport = transport;
}

/*******************  FUNCTION  *********************/
/**
 * Fill the local data for future checking.
 * @param size Size of the fragment.
 * @param pos Position inside the fragment (will be multiplied by BucketCheck size).
 * @param rail Define the rail.
 * @param unitId Define the source unit id.
 * @param eventId define the current event ID.
**/
void BucketCheck::fill ( size_t size, size_t pos, short int rail, short int unitId, size_t eventId )
{
	this->size = size;
	this->pos = pos;
	this->rail = rail;
	this->unitId = unitId;
	this->eventId = eventId;
}

/*******************  FUNCTION  *********************/
/**
 * Function used for validation, it checks the content of the bucket.
 * In case of failure it abort with user message.
 * @param size Size of the fragment.
 * @param pos Position inside the fragment (will be multiplied by BucketCheck size).
 * @param rail Define the rail.
 * @param unitId Define the source unit id.
 * @param eventId define the current event ID. 
**/
void BucketCheck::check ( size_t size, size_t pos, short int rail, short int unitId, size_t eventId )
{
	BucketCheck ref;
	ref.fill(size,pos,rail,unitId,eventId);
	assumeArg(ref == *this,"Invalid bucket checker : has %1 , expect %2").arg(*this).arg(ref).end();
}

/*******************  FUNCTION  *********************/
/**
 * Compare content of two buck chcker.
 * @param a left value
 * @param b right value
**/
bool operator== ( const BucketCheck& a, const BucketCheck& b )
{
	return (a.size == b.size 
		&& a.pos == b.pos
		&& a.rail == b.rail
		&& a.unitId == b.unitId
		&& a.eventId == b.eventId);
}

/*******************  FUNCTION  *********************/
/**
 * Permit to dump the bucket checker in output stream as text for debugging.
 * @param out Define the stream to use.
 * @param bucket Define the bucket to print.
**/
std::ostream & operator<< ( std::ostream& out, const BucketCheck& bucket )
{
	out << "[ size: " << bucket.size;
	out << ", pos: " << bucket.pos;
	out << ", rail: " << bucket.rail;
	out << ", unitId: " << bucket.unitId;
	out << ", eventId: " << bucket.eventId;
	out << "]";
	return out;
}

/*******************  FUNCTION  *********************/
/**
 * Fill the bucket fragments for later checking.
 * @param config Reference to the config object.
 * @param unitId Define the current unit Id.
**/
void BucketInfo::fillForcheck ( const Config& config, int unitId )
{
	assert(unitId != -1);
	#ifndef DETAIL_CHECK
		size_t cur = 0;
		size_t maxEventSize = config.eventRailMaxDataSize;
		size_t eventCollisions = config.getEventCollisions();
		for (size_t rail = 0 ; rail < DAQ_DATA_SOURCE_RAILS ; rail++) {
			cur = 0;
			EventMetaData * meta = reinterpret_cast<EventMetaData*>(this->metaFragments[rail].base);
			DAQ_DEBUG_ARG("datasource","fillForCheck() check meta %1").arg((void*)meta).end();
			DAQ_DEBUG_ARG("datasource","fillForCheck() check data %1").arg((void*)this->dataFragments[rail].base).end();
			size_t curVal = meta[cur].size;
			assumeArg(curVal <= config.collisionRailDataSize,"Invalid data size").arg((void*)this->dataFragments[rail].base).end();
			
			//fill with some real data to be checked
			for (size_t i = 0 ; i < maxEventSize ; i++) {
				if (i >= curVal) {
					cur++;
					if (cur < eventCollisions) {
						curVal += meta[cur].size;
						assert(meta[cur].size <= config.collisionRailDataSize);
						assert(curVal <= maxEventSize);
					} else {
						curVal = maxEventSize;
					}
				}
				if (curVal == 0) {
					this->dataFragments[rail].base[i] = 0;
				} else {
					this->dataFragments[rail].base[i] = (char)(curVal+(size_t)i+rail+(size_t)unitId+(size_t)this->eventId);
				}
			}
		}
	#else //DETAIL_CHECK
		for (int rail = 0 ; rail < DAQ_DATA_SOURCE_RAILS ; rail++)
		{
			size_t size = this->dataFragments[rail].size;
			BucketCheck * data = reinterpret_cast<BucketCheck*>(this->dataFragments[rail].base);
			DAQ_DEBUG_ARG("datasource","fillForcheck() rail %4 data %1 data->size %2 dataFragments[rail].size %3").arg(data).arg(data->size).arg(size).arg(rail).end();
			for (size_t i = 0 ; i < size / sizeof(BucketCheck) ; i++) {
				//DAQ_DEBUG_ARG("datasource", "fillForcheck() filling data[%1] with size %2, rail %3, unitId %4, eventId %5").arg(i).arg(size).arg(rail).arg(unitId).arg(eventId).end();
				data[i].fill(size,i,rail,unitId,eventId);
				//DAQ_DEBUG_ARG("datasource", "fillForcheck() data[%1].size %2").arg(i).arg(data[i].size).end();
			}
		}
	#endif //DETAIL_CHECK
}

/*******************  FUNCTION  *********************/
/**
 * Check the content of given bucket.
 * @param config Define the config object.
 * @param unitId Defnie the source unit id to use for checking.
**/
void BucketInfo::check ( const Config& config, int unitId )
{
	#ifndef DETAIL_CHECK
		size_t cur = 0;
		size_t maxEventSize = config.eventRailMaxDataSize;
		size_t eventCollisions = config.getEventCollisions();
		for (size_t rail = 0 ; rail < DAQ_DATA_SOURCE_RAILS ; rail++) {
			cur = 0;
			EventMetaData * meta = reinterpret_cast<EventMetaData*>(this->metaFragments[rail].base);
			DAQ_DEBUG_ARG("datasource","check() check meta %1").arg((void*)meta).end();
			DAQ_DEBUG_ARG("datasource","check() check data %1").arg((void*)this->dataFragments[rail].base).end();
			size_t curVal = meta[cur].size;
			assumeArg(curVal <= config.collisionRailDataSize,"Invalid data size").arg((void*)this->dataFragments[rail].base).end();
			
			//fill with some real data to be checked
			for (size_t i = 0 ; i < maxEventSize ; i++) {
				if (i >= curVal) {
					cur++;
					if (cur < eventCollisions) {
						curVal += meta[cur].size;
						assert(meta[cur].size <= config.collisionRailDataSize);
						assert(curVal <= maxEventSize);
					} else {
						curVal = maxEventSize;
					}
				}
				if (curVal < maxEventSize) {
					char ref = (curVal+(size_t)i+rail+(size_t)unitId+(size_t)this->eventId);
					assumeArg(this->dataFragments[rail].base[i] == ref,"Invalid value (%1) for (pos=%2, rail=%3, unitid=%4, event=%5, curval=%6, cur=%7, expect=%8 %9))")
						.arg((void*)this->dataFragments[rail].base)
						.arg(i)
						.arg(rail)
						.arg(unitId)
						.arg(this->eventId)
						.arg(curVal)
						.arg(cur)
						.arg((int)ref)
						.arg((int)this->dataFragments[rail].base[i])
						.end();
				}
			}
		}
	#else //DETAIL_CHECK
		for (int rail = 0 ; rail < DAQ_DATA_SOURCE_RAILS ; rail++)
		{
			BucketCheck * data = reinterpret_cast<BucketCheck*>(this->dataFragments[rail].base);
			//some debugging
			DAQ_DEBUG_ARG("datasource","BucketInfo::check() rail %1 (BucketCheck) data %2 data->size %3").arg(rail).arg((void*)data).arg(data->size).end();
			DAQ_DEBUG_ARG("datasource","BucketInfo::check() dataFragments %1 dataFragments.size %2").arg((void*)this->dataFragments[rail].base).arg(this->dataFragments[rail].size).end();
			size_t size = data->size;
			assert(size == this->dataFragments[rail].size || this->dataFragments[rail].size == 0);
			assumeArg(size > 0, "Check failed, size <= 0 for rail %1 data %2").arg(rail).arg(data).end();
			for (size_t i = 0 ; i < size / sizeof(BucketCheck) ; i++)
				data[i].check(size,i,rail,unitId,eventId);
			DAQ_DEBUG("datasource", "CHECK SUCCESSFUL ***************");	
		}
	#endif //DETAIL_CHECK
}

}

