/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_DUMMY_DATA_SOURCE_HPP
#define DAQ_DUMMY_DATA_SOURCE_HPP

/********************  HEADERS  *********************/
#include "DataSource.hpp"
#include <transport/Transport.hpp>
#include <gochannels/GoChannel.hpp>
#include <thread>
#include <array>

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/********************  MACROS  **********************/
/** Maximum number of threads to be used. **/
#define DAQ_DUMMY_MAX_THREADS 32

/********************  STRUCT  **********************/
/**
 * Define a job to generate data.
**/
struct DummyDataSourceJob
{
    /** Position of the data to generate **/
	int id;
    /** ID of the event to generate. **/
	uint64_t eventId;
};

/*********************  CLASS  **********************/
/**
 * Implement a datasource using fake generated data by the CPU.
**/
class DummyDataSource : public DataSource
{
	public:
		DummyDataSource( const DAQ::Config* config, Transport * transport);
		virtual ~DummyDataSource();
		virtual void getBucket ( BucketInfo& bucket, uint64_t eventId );
		virtual void ackSent ( BucketInfo& bucket );
		virtual void ackSent ( uint64_t eventId);
		virtual void registerSegments( Transport * transport );
		virtual size_t getTotalSize ( uint64_t eventId );
		virtual bool IsEventAvailable(uint64_t currentEventId);
		virtual void setId ( int unitId );
		virtual int getFilledPercentage();
	private:
		bool generateEvent( int id, uint64_t eventId );
		size_t getRandomEventSize();
		int getDataSize( int id, int rail );
		int getId(uint64_t eventId);
		void worker();
		void setupSize( int rank, int nodes );
		size_t setupSizeRandom( int rank, int nodes );
		size_t setupSizeGrouped( int rank, int nodes );
	private:
        /** Data generation jobs **/
		GoChannel<DummyDataSourceJob> jobs;
        /** memory buffer for the data, one per rail **/
		char * data[DAQ_DATA_SOURCE_RAILS];
        /** memory buffer for the meta-data, one per rail **/
		std::array<EventMetaData*, DAQ_DATA_SOURCE_RAILS>  meta;
        /** Array of event IDs to match their position in the memory buffers **/
		uint64_t * eventIds;
        /** Keep track of the next event id to generate **/
		uint64_t nextEventId;
        /** Number of event stored into the memory buffers **/
		int storedEvents;
        /** Maximual size of an event **/
		size_t maxEventSize;
        /** Number of collisions in one event (define the meta-data size) **/
		size_t eventCollisions;
        /** Enable of disable data checking **/
		bool dataChecking;
        /** Define the current unit id **/
		int unitId;
        /** Define the random ratio to use for size generation **/
		int randomSize;
        /** Enable usage of a uniq segment. **/
		bool sameSegmentMode;
        /** Size of a data fragment for one collision **/
		long int collisionRailDataSize; 
        /** Keep track of the transport object **/
		Transport * transport;
        /** Thread handlers (workers for data generation) **/
		std::thread threads[DAQ_DUMMY_MAX_THREADS];
        /** Number of thread in use. **/
		int threadCnt;
		/** Exact size to simulate (%) **/
		size_t realSizePercentage;
		/** Enable or disable real size mode **/
		bool realSize;
		/** real size mode **/
		std::string realSizeMode;
		/** random seed **/
		size_t realSizeRandomSeed;
};

}

#endif //DAQ_DUMMY_DATA_SOURCE_HPP
