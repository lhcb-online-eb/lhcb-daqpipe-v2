/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include <config.h>
#include <cstdio>
#include <arpa/inet.h>
#include <common/Debug.hpp>
#include <cassert>
#include "Amc40Generator.hpp"
#include "portability/Endian.hpp"

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*******************  FUNCTION  *********************/
/**
 * Default constructor of the state.
**/
Amc40GenState::Amc40GenState ()
{
	collisionId = seqNum = count = meps = 0;
}

/*******************  FUNCTION  *********************/
/**
 * Function to align sizes to 8 bytes.
 * @param size in bits
 * @return Output size in bytes and aligned to 8 bytes.
**/
static size_t alignedByteSize(size_t size)
{
	//use padding
	if (size % 64 != 0)
		size += 64 - size % 64;
	return size / 8;
}

/*******************  FUNCTION  *********************/
/**
 * Generate a data packet using the current state and filling the given buffer.
 * @param state State to use to generate the packet
 * @param buffer Buffer in which to store the generated packet.
 * @param maxSize Size of the buffer to avoid overflow.
 * @param eventCollisions Max number (total) of collisions to generate before cuting the packet.
 * @return Size of the generated packet.
**/
size_t genPacket(Amc40GenState &state,char * buffer,size_t maxSize, uint64_t eventCollisions)
{
	//allocate buffer
	DAQ::Amc40MEPHeader * mep = (DAQ::Amc40MEPHeader*)buffer;
	char * data = (char*)(mep+1);

	//setup mep header
	//mep->evid = htobe64(collisionId);
	mep->evid = htobe64(state.collisionId);
	mep->seqnum = htonl(state.seqNum++);
	
	//debug
	DAQ_DEBUG_ARG("amc40","Gen packet of MEP %1 starting at collision %2").arg(state.meps).arg(mep->evid).end();
	
	//build udp packet
	size_t size = sizeof(*mep);
	uint16_t frags = 0;
	//while (state.count < eventCollisions && size < MAX_UDP_SIZE) {
	while (size < MAX_UDP_SIZE) {
		//setup pointers
		DAQ::Amc40MEPFragmentHeader * dataHeader = (DAQ::Amc40MEPFragmentHeader*)data;
		char * tmp = (char*)(dataHeader+1);
		
		//build data
		uint32_t s = sprintf(tmp,"collision %lu",state.collisionId);
		//keep \0
		s++;
		
		//aligned
		size_t alignedSize = alignedByteSize((s+sizeof(*dataHeader))*8);
		
		//cut
		if (size + sizeof(*dataHeader) + alignedSize >= MAX_UDP_SIZE)
			break;
		
		//update header
		dataHeader->bxi = 0;
		dataHeader->size = s*8;//htonl(s * 8); TODO
		
		//convert
		uint32_t * word = (uint32_t*)dataHeader;
		*word = htonl(*word);
		
		//inc size
		size += alignedSize;
		state.collisionId++;
		state.count++;
		frags++;
		
		//move
		data += sizeof(*dataHeader) + alignedSize;
	}
	
	//setup mep headers
	mep->bytes = htobe64(size - sizeof(*mep));
	mep->frags = htons(frags);
	
	//reset
	if (state.count >= eventCollisions) {
		state.meps++;
		state.count = 0;
	}
	
	return size;
}

/*******************  FUNCTION  *********************/
/**
 * Generate a TFC packet using the current state and filling the given buffer.
 * @param state State to use to generate the packet
 * @param buffer Buffer in which to store the generated packet.
 * @param maxSize Size of the buffer to avoid overflow.
 * @param eventCollisions Max number (total) of collisions to generate before cuting the packet.
 * @return Size of the generated packet.
**/
size_t genTfcPacket(Amc40GenState &state,char * buffer,size_t maxSize, uint64_t eventCollisions)
{
	//allocate buffer
	DAQ::Amc40MEPHeader * mep = (DAQ::Amc40MEPHeader*)buffer;
	char * data = (char*)(mep+1);

	//setup mep header
	//mep->evid = htobe64(collisionId);
	mep->evid = htobe64(state.collisionId);
	mep->seqnum = htonl(state.seqNum++);
	
	//debug
	DAQ_DEBUG_ARG("amc40","Gen packet of MEP %1 starting at collision %2").arg(state.meps).arg(mep->evid).end();
	
	//build udp packet
	size_t size = sizeof(*mep);
	uint16_t frags = 0;
	while (state.count < eventCollisions && size < MAX_UDP_SIZE) {
		//setup pointers
		Amc40TfcFragment * tfcData = (DAQ::Amc40TfcFragment*)data;
		
		//build data
		uint32_t s = sprintf(tfcData->bytes,"tfc %lu",state.collisionId);
		assert(s < sizeof(Amc40TfcFragment));
		//keep \0
		s = sizeof(Amc40TfcFragment);
		
		//cut
		if (size + s >= MAX_UDP_SIZE)
			break;
		
		//inc size
		size += s;
		state.collisionId++;
		state.count++;
		frags++;
		
		//move
		data += s;
	}
	
	//setup mep headers
	mep->bytes = htobe64(size - sizeof(*mep));
	mep->frags = htons(frags);
	
	//reset
	if (state.count >= eventCollisions) {
		state.meps++;
		state.count = 0;
	}
	
	return size;
}

}
