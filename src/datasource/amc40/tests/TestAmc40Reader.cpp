/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include <gtest/gtest.h>
#include <datasource/amc40/Amc40Reader.hpp>
#include <datasource/amc40/Amc40Generator.hpp>
#include <datasource/DataSource.hpp>
#include <iostream>

using namespace std;

/***************** USING NAMESPACE ******************/
using namespace DAQ;

/*******************  FUNCTION  *********************/
TEST(Amc40Generator,generatePacket)
{
	Amc40GenState state;
	char buffer[2*MAX_UDP_SIZE];
	size_t size = genPacket(state,buffer,sizeof(buffer), 20);
	size_t size2 = genPacket(state,buffer,sizeof(buffer), 40);
	
	EXPECT_GE(size2,size);
};

/*******************  FUNCTION  *********************/
TEST(Amc40Reader,constructor)
{
	//config
	Config config;
	config.getAndSetConfigString("dataSource.amc40","iface-rail0-1","lo");
	size_t storedEvents = config.readoutUnitEvents;
	size_t maxEventSize = config.eventRailMaxDataSize;
	size_t eventCollisions = config.getEventCollisions();
	
	//allocate mem
	char * data = new char[maxEventSize * storedEvents];
	EventMetaData * meta = (EventMetaData *)new char [EventMetaData::getTotalSize(eventCollisions)* storedEvents];
	char * tfcData = new char[maxEventSize * storedEvents];
	EventMetaData * tfcMeta = (EventMetaData *)new char [EventMetaData::getTotalSize(eventCollisions)* storedEvents];
	uint64_t * eventIds = new uint64_t[storedEvents];
	
	//setup reader
	Amc40Reader * reader = new Amc40Reader(&config,meta,data,tfcMeta,tfcData, eventIds , storedEvents,0,false);
	
	//free mem
	delete reader;
	delete [] data;
	delete [] meta;
	delete [] tfcData;
	delete [] tfcMeta;
	delete [] eventIds;
}

/*******************  FUNCTION  *********************/
TEST(Amc40Reader,pushOnePacket)
{
	//config
	Config config;
	config.getAndSetConfigString("dataSource.amc40","iface-rail0-1","lo");
	size_t storedEvents = config.readoutUnitEvents;
	size_t maxEventSize = config.eventRailMaxDataSize;
	size_t eventCollisions = config.getEventCollisions();
	
	//allocate mem
	char * data = new char[maxEventSize * storedEvents];
	EventMetaData * meta = (EventMetaData *)new char [EventMetaData::getTotalSize(eventCollisions)* storedEvents];
	char * tfcData = new char[maxEventSize * storedEvents];
	EventMetaData * tfcMeta = (EventMetaData *)new char [EventMetaData::getTotalSize(eventCollisions)* storedEvents];
	uint64_t * eventIds = new uint64_t[storedEvents];
	
	//setup reader
	Amc40Reader * reader = new Amc40Reader(&config,meta,data,tfcMeta,tfcData, eventIds , storedEvents,0,false);
	
	//gen one packet
	Amc40GenState state;
	char buffer[MAX_UDP_SIZE*2];
	size_t size = genPacket(state,buffer,sizeof(buffer), 20);
	
	//send it to reader
	reader->pushDataPacket(0,buffer,size);
	
	//free mem
	delete reader;
	delete [] data;
	delete [] meta;
	delete [] tfcData;
	delete [] tfcMeta;
	delete [] eventIds;
}

/*******************  FUNCTION  *********************/
TEST(Amc40Reader,startSequIdNot0)
{
	//config
	Config config;
	config.getAndSetConfigString("dataSource.amc40","iface-rail0-1","lo");
	size_t storedEvents = config.readoutUnitEvents;
	size_t maxEventSize = config.eventRailMaxDataSize;
	size_t eventCollisions = config.getEventCollisions();
	
	//allocate mem
	char * data = new char[maxEventSize * storedEvents];
	EventMetaData * meta = (EventMetaData *)new char [EventMetaData::getTotalSize(eventCollisions)* storedEvents];
	char * tfcData = new char[maxEventSize * storedEvents];
	EventMetaData * tfcMeta = (EventMetaData *)new char [EventMetaData::getTotalSize(eventCollisions)* storedEvents];
	uint64_t * eventIds = new uint64_t[storedEvents];
	
	//setup reader
	Amc40Reader * reader = new Amc40Reader(&config,meta,data,tfcMeta,tfcData, eventIds , storedEvents,0,false);
	
	//gen one packet
	Amc40GenState state;
	state.seqNum = 20;
	char buffer[MAX_UDP_SIZE*2];
	size_t size = genPacket(state,buffer,sizeof(buffer), 20);
	
	//send it to reader
	reader->pushDataPacket(0,buffer,size);
	
	//free mem
	delete reader;
	delete [] data;
	delete [] meta;
	delete [] tfcData;
	delete [] tfcMeta;
	delete [] eventIds;
}

/*******************  FUNCTION  *********************/
TEST(Amc40Reader,fillTenMEPs)
{
	//config
	Config config;
	config.getAndSetConfigString("dataSource.amc40","iface-rail0-1","lo");
	size_t storedEvents = config.readoutUnitEvents;
	size_t maxEventSize = config.eventRailMaxDataSize;
	size_t eventCollisions = config.getEventCollisions();
	
	//allocate mem
	char * data = new char[maxEventSize * storedEvents];
	EventMetaData * meta = (EventMetaData *)new char [EventMetaData::getTotalSize(eventCollisions)* storedEvents];
	char * tfcData = new char[maxEventSize * storedEvents];
	EventMetaData * tfcMeta = (EventMetaData *)new char [EventMetaData::getTotalSize(eventCollisions)* storedEvents];
	volatile uint64_t * eventIds = new uint64_t[storedEvents];
	memset((void*)eventIds,0,sizeof(uint64_t)*storedEvents);
	
	//setup reader
	Amc40Reader * reader = new Amc40Reader(&config,meta,data,tfcMeta,tfcData, (uint64_t*)eventIds , storedEvents,0,false);
	
	//gen one packet
	Amc40GenState state;
	
	//gen all packets
	while (state.meps < 10) {
		char * buffer = new char[MAX_UDP_SIZE*2];
		size_t size = genPacket(state,buffer,MAX_UDP_SIZE, eventCollisions);
		reader->pushDataPacket(0,buffer,size);
	}
	
	//check they have beed received
	for (size_t i = 0 ; i < 8 ; i++) {
		EXPECT_EQ(10+i,eventIds[i]);
		EventMetaData & curMeta = meta->getEvent(i,eventCollisions);
		char * cur = data + i*maxEventSize;
		
		for (size_t j = 0 ; j < eventCollisions ; j++) {
			EXPECT_EQ(20,(int)curMeta.getEntry(j).size);
			Amc40MEPFragmentHeader * header = (Amc40MEPFragmentHeader *)cur;
			char buffer[64];
			sprintf(buffer,"collision %lu",j + i*eventCollisions);
			EXPECT_EQ(8*strlen(buffer)+8,header->size);
			EXPECT_EQ(std::string(buffer),std::string(cur+sizeof(Amc40MEPFragmentHeader)));
			cur += curMeta.getEntry(j).size;
		}
	}
	
	//free mem
	delete reader;
	delete [] data;
	delete [] meta;
	delete [] tfcData;
	delete [] tfcMeta;
	delete [] eventIds;
}

/*******************  FUNCTION  *********************/
TEST(Amc40Reader,fillTenMEPsWithOneMissing_1)
{
	//config
	Config config;
	config.getAndSetConfigString("dataSource.amc40","iface-rail0-1","lo");
	size_t storedEvents = config.readoutUnitEvents;
	size_t maxEventSize = config.eventRailMaxDataSize;
	size_t eventCollisions = config.getEventCollisions();
	
	//allocate mem
	char * data = new char[maxEventSize * storedEvents];
	EventMetaData * meta = (EventMetaData *)new char [EventMetaData::getTotalSize(eventCollisions)* storedEvents];
	char * tfcData = new char[maxEventSize * storedEvents];
	EventMetaData * tfcMeta = (EventMetaData *)new char [EventMetaData::getTotalSize(eventCollisions)* storedEvents];
	volatile uint64_t * eventIds = new uint64_t[storedEvents];
	memset((void*)eventIds,0,sizeof(uint64_t)*storedEvents);
	
	//setup reader
	Amc40Reader * reader = new Amc40Reader(&config,meta,data,tfcMeta,tfcData, (uint64_t*)eventIds , storedEvents,0,false);
	
	//gen one packet
	Amc40GenState state;
	
	//gen all packets
	int cnt = 0;
	while (state.meps < 10) {
		char * buffer = new char[MAX_UDP_SIZE*2];
		size_t size = genPacket(state,buffer,MAX_UDP_SIZE, eventCollisions);
		//we skip one over 2
		if (cnt != 2)
			reader->pushDataPacket(0,buffer,size);
		else
			delete [] buffer;
		cnt++;
	}
	
	//check they have beed received
	cnt = 0;
	for (size_t i = 0 ; i < 8 ; i++) {
		EXPECT_EQ(10+i,eventIds[i]);
		EventMetaData & curMeta = meta->getEvent(i,eventCollisions);
		char * cur = data + i*maxEventSize;
		
		for (size_t j = 0 ; j < eventCollisions ; j++) {
			if (curMeta.getEntry(j).size != 0)
			{
				EXPECT_EQ(20,(int)curMeta.getEntry(j).size);
				Amc40MEPFragmentHeader * header = (Amc40MEPFragmentHeader *)cur;
				char buffer[64];
				sprintf(buffer,"collision %lu",j + i*eventCollisions);
				EXPECT_EQ(8*strlen(buffer)+8,header->size);
				EXPECT_EQ(std::string(buffer),std::string(cur+sizeof(Amc40MEPFragmentHeader)));
				cur += curMeta.getEntry(j).size;
			} else {
				cnt++;
			}
		}
	}
	
	EXPECT_EQ(459,cnt);
	
	//free mem
	delete reader;
	delete [] data;
	delete [] meta;
	delete [] tfcData;
	delete [] tfcMeta;
	delete [] eventIds;
}

/*******************  FUNCTION  *********************/
TEST(Amc40Reader,fillTenMEPsWithOneMissing_2)
{
	//config
	Config config;
	config.getAndSetConfigString("dataSource.amc40","iface-rail0-1","lo");
	size_t storedEvents = config.readoutUnitEvents;
	size_t maxEventSize = config.eventRailMaxDataSize;
	size_t eventCollisions = config.getEventCollisions();
	
	//allocate mem
	char * data = new char[maxEventSize * storedEvents];
	EventMetaData * meta = (EventMetaData *)new char [EventMetaData::getTotalSize(eventCollisions)* storedEvents];
	char * tfcData = new char[maxEventSize * storedEvents];
	EventMetaData * tfcMeta = (EventMetaData *)new char [EventMetaData::getTotalSize(eventCollisions)* storedEvents];
	volatile uint64_t * eventIds = new uint64_t[storedEvents];
	memset((void*)eventIds,0,sizeof(uint64_t)*storedEvents);
	
	//setup reader
	Amc40Reader * reader = new Amc40Reader(&config,meta,data,tfcMeta,tfcData, (uint64_t*)eventIds , storedEvents,0,false);
	
	//gen one packet
	Amc40GenState state;
	
	//gen all packets
	int cnt = 0;
	while (state.meps < 10) {
		char * buffer = new char[MAX_UDP_SIZE];
		size_t size = genPacket(state,buffer,MAX_UDP_SIZE, eventCollisions);
		//we skip one over 2
		if (cnt != 1)
			reader->pushDataPacket(0,buffer,size);
		else
			delete [] buffer;
		cnt++;
	}
	
	//check they have beed received
	cnt = 0;
	for (size_t i = 0 ; i < 8 ; i++) {
		EXPECT_EQ(10+i,eventIds[i]);
		EventMetaData & curMeta = meta->getEvent(i,eventCollisions);
		char * cur = data + i*maxEventSize;
		
		for (size_t j = 0 ; j < eventCollisions ; j++) {
			if (curMeta.getEntry(j).size != 0)
			{
				EXPECT_EQ(20,(int)curMeta.getEntry(j).size);
				Amc40MEPFragmentHeader * header = (Amc40MEPFragmentHeader *)cur;
				char buffer[64];
				sprintf(buffer,"collision %lu",j + i*eventCollisions);
				EXPECT_EQ(8*strlen(buffer)+8,header->size);
				EXPECT_EQ(std::string(buffer),std::string(cur+sizeof(Amc40MEPFragmentHeader)));
				cur += curMeta.getEntry(j).size;
			} else {
				cnt++;
			}
		}
	}
	
	EXPECT_EQ(459,cnt);
	
	//free mem
	delete reader;
	delete [] data;
	delete [] meta;
	delete [] tfcData;
	delete [] tfcMeta;
	delete [] eventIds;
}

/*******************  FUNCTION  *********************/
TEST(Amc40Reader,fillTenMEPsWithManyMissing)
{
	//config
	Config config;
	config.getAndSetConfigString("dataSource.amc40","iface-rail0-1","lo");
	size_t storedEvents = config.readoutUnitEvents;
	size_t maxEventSize = config.eventRailMaxDataSize;
	size_t eventCollisions = config.getEventCollisions();
	
	//allocate mem
	char * data = new char[maxEventSize * storedEvents];
	EventMetaData * meta = (EventMetaData *)new char [EventMetaData::getTotalSize(eventCollisions)* storedEvents];
	char * tfcData = new char[maxEventSize * storedEvents];
	EventMetaData * tfcMeta = (EventMetaData *)new char [EventMetaData::getTotalSize(eventCollisions)* storedEvents];
	volatile uint64_t * eventIds = new uint64_t[storedEvents];
	memset((void*)eventIds,0,sizeof(uint64_t)*storedEvents);
	
	//setup reader
	Amc40Reader * reader = new Amc40Reader(&config,meta,data,tfcMeta,tfcData, (uint64_t*)eventIds , storedEvents,0,false);
	reader->disableWarnSkip();

	//gen one packet
	Amc40GenState state;
	
	//gen all packets
	int cnt = 0;
	while (state.meps < 10) {
		char * buffer = new char[MAX_UDP_SIZE*2];
		size_t size = genPacket(state,buffer,MAX_UDP_SIZE, eventCollisions);
		//we skip one over 2
		if (cnt % 2 || cnt == 0)
			reader->pushDataPacket(0,buffer,size);
		else
			delete [] buffer;
		cnt++;
	}
	
	//check they have beed received
	cnt = 0;
	for (size_t i = 0 ; i < 4 ; i++) {
		EXPECT_EQ(10+i,eventIds[i]);
		EventMetaData & curMeta = meta->getEvent(i,eventCollisions);
		char * curData = data + i*maxEventSize;
		
		for (size_t j = 0 ; j < eventCollisions ; j++) {
			if (curMeta.getEntry(j).size != 0) {
				EXPECT_EQ(20,(int)curMeta.getEntry(j).size);
				Amc40MEPFragmentHeader * header = (Amc40MEPFragmentHeader *)curData;
				char buffer[64];
				sprintf(buffer,"collision %lu",j + i*eventCollisions);
				EXPECT_EQ(8*strlen(buffer)+8,header->size);
				EXPECT_EQ(std::string(buffer),std::string(curData+sizeof(Amc40MEPFragmentHeader)));
				curData += curMeta.getEntry(j).size;
			} else {
				cnt++;
			}
		}
	}
	
	EXPECT_EQ(20655,cnt);
	
	//free mem
	delete reader;
	delete [] data;
	delete [] meta;
	delete [] tfcData;
	delete [] tfcMeta;
	delete [] eventIds;
}

/*******************  FUNCTION  *********************/
TEST(Amc40Reader,fillTenMEPsTfc)
{
	//config
	Config config;
	config.getAndSetConfigString("dataSource.amc40","iface-rail0-1","lo");
	size_t storedEvents = config.readoutUnitEvents;
	size_t maxEventSize = config.eventRailMaxDataSize;
	size_t eventCollisions = config.getEventCollisions();
	
	//allocate mem
	char * data = new char[maxEventSize * storedEvents];
	EventMetaData * meta = (EventMetaData *)new char [EventMetaData::getTotalSize(eventCollisions)* storedEvents];
	char * tfcData = new char[maxEventSize * storedEvents];
	EventMetaData * tfcMeta = (EventMetaData *)new char [EventMetaData::getTotalSize(eventCollisions)* storedEvents];
	volatile uint64_t * eventIds = new uint64_t[storedEvents];
	memset((void*)eventIds,0,sizeof(uint64_t)*storedEvents);
	
	//setup reader
	Amc40Reader * reader = new Amc40Reader(&config,meta,data,tfcMeta,tfcData, (uint64_t*)eventIds , storedEvents,0,false);
	
	//gen one packet
	Amc40GenState state;
	Amc40GenState tfcState;
	
	//gen all packets
	while (state.meps < 10) {
		//tfc
		if (tfcState.meps == state.meps) {
			char * buffer = new char[MAX_UDP_SIZE*2];
			size_t size = genTfcPacket(tfcState,buffer,MAX_UDP_SIZE, eventCollisions);
			reader->pushTfcPacket(0,buffer,size);
		}
		
		//data
		char * buffer = new char[MAX_UDP_SIZE*2];
		size_t size = genPacket(state,buffer,MAX_UDP_SIZE, eventCollisions);
		reader->pushDataPacket(0,buffer,size);
	}
	
	//check they have beed received
	for (size_t i = 0 ; i < 8 ; i++) {
		EXPECT_EQ(10+i,eventIds[i]);
		EventMetaData & curMeta = meta->getEvent(i,eventCollisions);
		EventMetaData & curTfcMeta = tfcMeta->getEvent(i,eventCollisions);
		char * cur = data + i*maxEventSize;
		char * curTfc = tfcData + i*maxEventSize;
		
		for (size_t j = 0 ; j < eventCollisions ; j++) {
			//data
			EXPECT_EQ(20,(int)curMeta.getEntry(j).size);
			Amc40MEPFragmentHeader * header = (Amc40MEPFragmentHeader *)cur;
			char buffer[64];
			sprintf(buffer,"collision %lu",j + i*eventCollisions);
			EXPECT_EQ(8*strlen(buffer)+8,header->size);
			EXPECT_EQ(std::string(buffer),std::string(cur+sizeof(Amc40MEPFragmentHeader)));
			cur += curMeta.getEntry(j).size;
			
			//tfc
// 			EXPECT_EQ(16,(int)curTfcMeta[j].size);
// 			sprintf(buffer,"tfc %u",j + i*eventCollisions);
// 			EXPECT_EQ(std::string(buffer),std::string(curTfc));
// 			curTfc += curTfcMeta[j].size;
			
			//tfc
			EXPECT_EQ(20,(int)curTfcMeta.getEntry(j).size);
			header = (Amc40MEPFragmentHeader *)curTfc;
			sprintf(buffer,"tfc %lu",j + i*eventCollisions);
			EXPECT_EQ(8*16,header->size);
			EXPECT_EQ(std::string(buffer),std::string(curTfc+sizeof(Amc40MEPFragmentHeader)));
			curTfc += curMeta.getEntry(j).size;
		}
	}
	
	//free mem
	delete reader;
	delete [] data;
	delete [] meta;
	delete [] tfcData;
	delete [] tfcMeta;
	delete [] eventIds;
}

/*******************  FUNCTION  *********************/
TEST(Amc40Reader,fillTenMEPsSkipFirst)
{
	//config
	Config config;
	config.getAndSetConfigString("dataSource.amc40","iface-rail0-1","lo");
	size_t storedEvents = config.readoutUnitEvents;
	size_t maxEventSize = config.eventRailMaxDataSize;
	size_t eventCollisions = config.getEventCollisions();
	
	//allocate mem
	char * data = new char[maxEventSize * storedEvents];
	EventMetaData * meta = (EventMetaData *)new char [EventMetaData::getTotalSize(eventCollisions)* storedEvents];
	char * tfcData = new char[maxEventSize * storedEvents];
	EventMetaData * tfcMeta = (EventMetaData *)new char [EventMetaData::getTotalSize(eventCollisions)* storedEvents];
	volatile uint64_t * eventIds = new uint64_t[storedEvents];
	memset((void*)eventIds,0,sizeof(uint64_t)*storedEvents);
	
	//setup reader
	Amc40Reader * reader = new Amc40Reader(&config,meta,data,tfcMeta,tfcData, (uint64_t*)eventIds , storedEvents,0,false);
	
	//gen one packet
	Amc40GenState state;
	
	//skip first
	do {
		char * buffer = new char[MAX_UDP_SIZE*2];
		genPacket(state,buffer,MAX_UDP_SIZE, eventCollisions);
		delete [] buffer;
	} while(0);
	
	//gen all packets
	while (state.meps < 10) {
		char * buffer = new char[MAX_UDP_SIZE*2];
		size_t size = genPacket(state,buffer,MAX_UDP_SIZE, eventCollisions);
		reader->pushDataPacket(0,buffer,size);
	}
	
	//check they have beed received
	for (size_t i = 0 ; i < 8 ; i++) {
		EXPECT_EQ(10+i,eventIds[i]);
		EventMetaData & curMeta = meta->getEvent(i,eventCollisions);
		char * cur = data + i*maxEventSize;
		
		for (size_t j = 0 ; j < eventCollisions ; j++) {
			EXPECT_EQ(20,(int)curMeta.getEntry(j).size);
			Amc40MEPFragmentHeader * header = (Amc40MEPFragmentHeader *)cur;
			char buffer[64];
			sprintf(buffer,"collision %lu",j + i*eventCollisions+459);
			ASSERT_EQ(8*strlen(buffer)+8,header->size);
			ASSERT_EQ(std::string(buffer),std::string(cur+sizeof(Amc40MEPFragmentHeader)));
			cur += curMeta.getEntry(j).size;
		}
	}
	
	//free mem
	delete reader;
	delete [] data;
	delete [] meta;
	delete [] tfcData;
	delete [] tfcMeta;
	delete [] eventIds;
}
