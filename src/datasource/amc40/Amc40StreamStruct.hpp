/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_AMC40_STREAM_STRUCT_HPP
#define DAQ_AMC40_STREAM_STRUCT_HPP

/********************  HEADERS  *********************/
#include <cstdint>

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/********************  STRUCT  **********************/
struct __attribute__((__packed__)) Amc40MEPHeader
{
	/** Define the UDP packed global sequence ID **/
	uint32_t seqnum;
	/** Define the data (no header) bytes in the packet **/
	uint16_t bytes;
	/** Define the number of fragments inside the packet **/
	uint16_t frags;
	/** Define the base event ID (first fragment) **/
	uint64_t evid;
};

/********************  STRUCT  **********************/
struct __attribute__((__packed__)) Amc40MEPFragmentHeader
{
	/** Size of data (no header) **/
	uint32_t size:20;
	/** bxi used to index the data source (for checking) **/
	uint16_t bxi:12;
};

/********************  STRUCT  **********************/
struct __attribute__((__packed__)) Amc40TfcFragment
{
	char bytes[16];
	//SODIN_DATA_BANK(127 downto 64) <= i_utc_timestamp(63 downto 0);
	//SODIN_DATA_BANK(63 downto 0)     <= C_ORBIT(31 downto 0) & C_STEPRUN(8 downto 0) & i_nzs_mode_out(0) & i_calib_type_out(3 downto 0) & i_trg_type_out(3 downto 0) & i_bx_type_out(1 downto 0) & i_bxid_out(11 downto 0);
};

};

#endif //DAQ_AMC40_STREAM_STRUCT_HPP
