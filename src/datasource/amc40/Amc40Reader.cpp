/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include <cassert>
#include <cstring>
#include <cstdio>
#include <common/Debug.hpp>
#include "Amc40Reader.hpp"
#include <datasource/DataSource.hpp>
#include <portability/TcpHelper.hpp>
#include "portability/Endian.hpp"
//for sockets
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>


/*******************  NAMESPACE  ********************/
namespace DAQ
{

/********************  MACROS  **********************/
/**
 * Packet might arrive out of order, hence we wait to have N time
 * the required number of packets before building the mep.
 * This ensure that we receive all the pending packets from the network
 * event if they are out of order.
**/
#define DAQ_AMC40_BUFFERING_MARGIN 2

/*******************  FUNCTION  *********************/
/**
 * Default constuctor of a line.
**/
Amc40Line::Amc40Line ()
{
	expectedDataSeqNum = 0;
	expectedTfcSeqNum = 0;
	dataSock = -1;
	tfcSock = -1;
}

/*******************  FUNCTION  *********************/
/**
 * Reader constructor. It basicaly load the config, setup the listening sockets and start the threads.
 * @param config Define the glocal configuration object to load parameters.
 * @param meta Base address of the meta data from rail 0 (for amc40 data stream).
 * @param data Base address of the data from rail 0 (for amc40 data stream).
 * @param tfcMeta Base address of the meta data from rail 1 (for amc40 tfc stream).
 * @param tfcData Base address of the data from rail 1 (for amc40  tfc stream).
 * @param eventIds Array storing the mapping of event ids.
 * @param storedEvents Numbef event stored into the data/meta/eventId arrays.
 * @param railId To be used to use more that one AMC40 per process/node.
 * @param listen Enable the listening ports (disable for unit tests).
**/
Amc40Reader::Amc40Reader ( const DAQ::Config* config, DAQ::EventMetaData* meta, char* data, DAQ::EventMetaData* tfcMeta, char* tfcData, uint64_t* eventIds, size_t storedEvents, int railId, bool listen )
{
	//keep track
	this->meta = meta;
	this->data = data;
	this->tfcData = tfcData;
	this->tfcMeta = tfcMeta;
	this->eventIds = eventIds;
	this->storedEvents = storedEvents;
	this->nextBaseCollisionId = 0;
	this->queuedFragments = 0;
	this->eventId = DAQ_FIRST_EVENT_ID;
	this->warnSkip = true;

	//config
	lineCnt = config->getAndSetConfigInt("dataSource.amc40","lines",1);
	packetMaxSize = config->getAndSetConfigUInt64("dataSource.amc40","packetMaxSize",9*1024);
	dataPort = config->getAndSetConfigInt("dataSource.amc40","dataPort",3600);
	tfcPort = config->getAndSetConfigInt("dataSource.amc40","tfcPort",3601);
	threadCnt = config->getAndSetConfigInt("dataSource.amc40","threads",8);
	eventCollisions = config->getEventCollisions();
	maxEventSize = config->eventRailMaxDataSize;
	maxCollisionSize = config->collisionRailDataSize;

	//check
	assumeArg(lineCnt <= DAQ_AMC40_MAX_LINES, "Invalid number of lines, limit is %1, used is %2 !").arg(DAQ_AMC40_MAX_LINES).arg(lineCnt).end();
	assumeArg(threadCnt <= DAQ_AMC40_MAX_THREADS, "Invalid number of conversion thread for amc40 : %1, limit is %2 !").arg(threadCnt).arg(DAQ_AMC40_MAX_THREADS).end();
	assumeArg(threadCnt >= lineCnt * 2,"It is recommended to use at least linesCnt * 2 (%2) threads for the AMC40 data source, otherwise you are likely to miss packets, current is %1 !").arg(threadCnt).arg(lineCnt * 2).end();

	//load ifaces, except for unit tests
	if (listen)
	{
		for (int i = 0 ; i < lineCnt ; i++) {
			char buffer[128];
			sprintf(buffer,"iface-rail%d-%d",railId+1,i+1);
			lines[i].iface = config->getAndSetConfigString("dataSource.amc40",buffer,"");
			assumeArg(lines[i].iface.empty() == false,"Missing definition of %1 for amc40 !").arg(buffer).end();
			lines[i].setup(16777216,dataPort,tfcPort);
		}
	}

	//start
	startThreads(listen);
}

/*******************  FUNCTION  *********************/
/**
 * Destructor, will stop the threads and close the sockets.
**/
Amc40Reader::~Amc40Reader ()
{
	//stop threads
	stopThreads();

	//close lines
	for (int i = 0 ; i < lineCnt ; i++)
		lines[i].close();
}

/*******************  FUNCTION  *********************/
/**
 * Open the UDP socket to listent for incomming packet from the AMC40 board.
 * @param bufferSize setup the linux buffer size.
 * @param port Define the port on which to listen.
 * @return Return the socket handler.
**/
/**
 * Setup the current line by opening the sockets and init the sequence numbers.
 * @param linuxBufSize Define the buffer size to be used by the kernel.
 * @param dataPort Defnie the port to listen for data stream
 * @param tfcPort Define the port to listent for TFC.
**/
void Amc40Line::setup ( size_t linuxBufSize, int dataPort, int tfcPort)
{
	this->expectedDataSeqNum = 0;
	this->expectedTfcSeqNum = 0;
	this->dataSock = TcpHelper::makeUdpListen(this->iface,linuxBufSize,dataPort);
	this->tfcSock = TcpHelper::makeUdpListen(this->iface,linuxBufSize,tfcPort);
}

/*******************  FUNCTION  *********************/
/**
 * Close the sockets.
**/
void Amc40Line::close ()
{
	::close(dataSock);
	::close(tfcSock);
}

/*******************  FUNCTION  *********************/
/**
 * Setup the complete list of jobs and start the threads.
**/
void Amc40Reader::startThreads ( bool listen )
{
	this->running = true;
	this->hasListen = listen;
	Amc40Reader * cur = this;

	//start udp reader threads
	if (listen) {
		//init jobs
		DAQ_DEBUG("amc40", "Init jobs");
		for (int line = 0 ; line < lineCnt ; line++) {
			Amc40Job job;
			job.line = &lines[line];
			job.tfc = true;
			job >> jobs;
			job.tfc = false;
			job >> jobs;
		}

		//start threads
		DAQ_DEBUG("amc40", "Start threads");
		//start threads
		for (int i = 0 ; i < threadCnt ; i++) {
			threads[i] = std::thread([=] {
				cur->threadMain();
			});
		}
	}
}

/*******************  FUNCTION  *********************/
/**
 * Close the job queue and wait the threads to be stopped.
**/
void Amc40Reader::stopThreads ()
{
	DAQ_DEBUG("amc40","Stop threads");

	this->running = false;
	this->jobs.close();

	if (hasListen)
		for (int i = 0 ; i < lineCnt*2 ; i++)
			this->threads[i].join();
}

/*******************  FUNCTION  *********************/
/**
 * Main function of reader threads. Each thread took a job in the queue. Allocate a buffer
 * then read the data from the given socket (defined by the job). Then it put again the job
 * in the queue to possibly be taken by another thread whil the current one finish to handle
 * the recieved data. Then it push the packet into the right list (tfc or data). 
**/
void Amc40Reader::threadMain ()
{
	//vars
	socklen_t addr_len = 0;
	sockaddr address;
	size_t size;

	//debug
	DAQ_DEBUG("amc40","AMC40 reader thread");

	//run on job queue
	Amc40Job job;
	while (job << jobs) {
		Amc40Line & line = *job.line;

		void * buffer = allocateBuffer();

		//debug
		DAQ_DEBUG_ARG("amc40","Waiting data packet (tfc=%1)").arg(job.tfc).end();

		//get TFC or data on line
		if (job.tfc)
			size = recvfrom(line.tfcSock, buffer, packetMaxSize, 0, &address, &addr_len);
		else
			size = recvfrom(line.dataSock, buffer, packetMaxSize, 0, &address, &addr_len);


		//debug
		DAQ_DEBUG_ARG("amc40","Get packet of size %1").arg(size).end();

		//warning
		if (size <= 0)
			DAQ_WARNING_ARG("Fail to recvfrom, get invalid size (%1) : %2").arg(size).argStrErrno().end();

		//push again the job in the queue so another thread can take the socket
		job >> jobs;

		//push
		if (job.tfc)
			pushTfcPacket(*job.line,buffer,size);
		else
			pushDataPacket(*job.line,buffer,size);
	}

	DAQ_DEBUG("amc40","Exit thread");
}

/*******************  FUNCTION  *********************/
/**
 * Allocate a new buffer or reuse one from the buffer queue.
 * @return Address of the buffer.
**/
void* Amc40Reader::allocateBuffer ( bool skipMutex )
{
	//safe
	if (!skipMutex)
		mutex.lock();

	void * ret = nullptr;
	if (memory.empty()) {
		ret = new char[packetMaxSize];
	} else {
		ret = memory.back();
		memory.pop_back();
	}
	
	if (!skipMutex)
		mutex.unlock();
	
	return ret;
}

/*******************  FUNCTION  *********************/
/**
 * Select the line by id and push packets to it.
 * @param lineId Define the line to use.
 * @param buffer Define the buffer containing the packet.
 * @param size Define the size of the packet.
**/
void Amc40Reader::pushDataPacket ( int lineId, void* buffer, size_t size )
{
	pushDataPacket(lines[lineId],buffer,size);
}


/*******************  FUNCTION  *********************/
/**
 * Select the line by id and push packets to it.
 * @param lineId Define the line to use.
 * @param buffer Define the buffer containing the packet.
 * @param size Define the size of the packet.
**/
void Amc40Reader::pushTfcPacket ( int lineId, void* buffer, size_t size )
{
	pushTfcPacket(lines[lineId],buffer,size);
}

/*******************  FUNCTION  *********************/
/**
 * Use the line to check the packet consistency and ordering the push the packet to the related tfc queue.
 * @param line Define the line to use.
 * @param buffer Define the buffer to push.
 * @param size Define the size of the packet.
**/
void Amc40Reader::pushTfcPacket ( Amc40Line& line, void* buffer, size_t size )
{
	//check
	Amc40MEPHeader * packet = (Amc40MEPHeader*)buffer;
	uint32_t seqNum = ntohl(packet->seqnum);
	DAQ_DEBUG_ARG("amc40","check %1").arg(be64toh(packet->evid)).end();

	//check sequences and update
	if (line.expectedTfcSeqNum == 0) {
		DAQ_INFO_ARG("Initial TFC sync sequence number of UDP packets to %1").arg(seqNum).end();
		line.expectedTfcSeqNum = seqNum + 1;
	} else if (line.expectedTfcSeqNum == seqNum) {
		line.expectedTfcSeqNum = seqNum + 1;
	} else if (line.expectedTfcSeqNum < seqNum) {
		// We lost something!
		line.expectedTfcSeqNum = seqNum + 1;
		if (warnSkip)
			DAQ_WARNING_ARG("UDP packet loss : %1").arg(line.expectedTfcSeqNum - seqNum).end();
	} else {
		//WTF? The sequence went backwards?
		DAQ_WARNING_ARG("TFC Packet id out of sequence! (expected %1, got %2)").arg(line.expectedTfcSeqNum).arg(seqNum).end();
	}

	//flush packet to pending
	pushTfcPacket(packet);
}

/*******************  FUNCTION  *********************/
/**
 * Use the line to check the packet consistency and ordering the push the packet to the related data queue.
 * @param line Define the line to use.
 * @param buffer Define the buffer to push.
 * @param size Define the size of the packet.
**/
void Amc40Reader::pushDataPacket ( Amc40Line& line,void * buffer, size_t size )
{
	//check
	Amc40MEPHeader * packet = (Amc40MEPHeader*)buffer;
	uint32_t seqNum = ntohl(packet->seqnum);
	DAQ_DEBUG_ARG("amc40","check %1").arg(be64toh(packet->evid)).end();

	//check sequences and update
	if (line.expectedDataSeqNum == 0) {
		DAQ_INFO_ARG("Initial sync sequence number of UDP packets to %1").arg(seqNum).end();
		line.expectedDataSeqNum = seqNum + 1;
	} else if (line.expectedDataSeqNum == seqNum) {
		line.expectedDataSeqNum = seqNum + 1;
	} else if (line.expectedDataSeqNum < seqNum) {
		// We lost something!
		line.expectedDataSeqNum = seqNum + 1;
		if (warnSkip)
			DAQ_WARNING_ARG("UDP packet loss : %1").arg(line.expectedDataSeqNum - seqNum).end();
	} else {
		//WTF? The sequence went backwards?
		DAQ_WARNING_ARG("Packet id out of sequence! (expected %1, got %2)").arg(line.expectedDataSeqNum).arg(seqNum).end();
	}

	//flush packet to pending
	pushDataPacket(packet);
}

/*******************  FUNCTION  *********************/
/**
 * Push the packet to the related list by taking the related locks.
**/
void Amc40Reader::pushTfcPacket ( Amc40MEPHeader* packet )
{
	DAQ_DEBUG_ARG("amc40","Push packet %1").arg(be64toh(packet->evid)).end();
	//push into the queue
	mutex.lock();
	tfcFetched.push_back(packet);
	DAQ_DEBUG_ARG("amc40","Push TFC %1 fragments").arg(ntohs(packet->frags)).end();
	mutex.unlock();
}

/*******************  FUNCTION  *********************/
/**
 * Push the packet to the related list by taking the related locks.
 * If get enought packet to build a mep, then start to convert.
**/
void Amc40Reader::pushDataPacket ( Amc40MEPHeader* packet )
{
	//vars
	bool doConvert = false;

	DAQ_DEBUG_ARG("amc40","Push packet %1").arg(be64toh(packet->evid)).end();
	//push into the queue
	mutex.lock();
	dataFetched.push_back(packet);
	queuedFragments += ntohs(packet->frags);
	DAQ_DEBUG_ARG("amc40","Push %1 fragments").arg(ntohs(packet->frags)).end();
	if (queuedFragments >= DAQ_AMC40_BUFFERING_MARGIN*eventCollisions) {
		DAQ_DEBUG("amc40","Have enougth data, push to queue");
		doConvert = true;
	} else {
		DAQ_DEBUG_ARG("amc40","Wait to have enougth data, continue to buffer. progress : %1 / %2 (+%3)").arg(queuedFragments).arg(2*eventCollisions).arg(ntohs(packet->frags)).end();
	}
	mutex.unlock();

	//convert
	if (doConvert)
		convert();
}

/*******************  FUNCTION  *********************/
bool Amc40Reader::contain ( uint64_t refStart, uint64_t refSize, uint64_t segStart, uint64_t segSize )
{
	//compute
	uint64_t refEnd = refStart + refSize;
	uint64_t segEnd = segStart + segSize;
	
	//check
	return ( (segStart >= refStart && segStart < refEnd) 
	       ||(segEnd   > refStart  && segEnd  <= refEnd));
}

/*******************  FUNCTION  *********************/
Amc40MEPHeader * Amc40Reader::dup ( Amc40MEPHeader* packet )
{
	Amc40MEPHeader * buf = (Amc40MEPHeader*)allocateBuffer(true);
	memcpy(buf,packet,packetMaxSize);
	return buf;
}

/*******************  FUNCTION  *********************/
/**
 * Extract the data and tfc needed to build a mep (with id starting fomr nextBaseCollisionId
 * and up to eventCollisions).
 * @param outQueue Define the output queue.
 * @param inQueue Define the intput queue.
 * @return Number of fragments selected (sum of fragments from each packets).
**/
size_t Amc40Reader::extractMepPackets ( Amc40MepPacketList& outQueue, Amc40MepPacketList& inQueue )
{
	//extract the fragme of the current event
	Amc40MepPacketList::iterator it = inQueue.begin();
	size_t totalFrags = 0;
	while (it != inQueue.end()) {
		uint64_t evid = be64toh((*it)->evid);
		size_t frags = ntohs((*it)->frags);
		DAQ_DEBUG_ARG("amc40","check %1").arg(evid).end();
		if (contain(nextBaseCollisionId,eventCollisions, evid, frags))
		{
			DAQ_DEBUG_ARG("amc40","take %1 and remove").arg(evid).end();
			if (evid + frags > nextBaseCollisionId + eventCollisions) {
				outQueue.push_back(dup(*it));
				++it;
			} else {
				outQueue.push_back(*it);
				it = inQueue.erase(it);
			}
			totalFrags += frags;
		} else {
			++it;
		}
	}

	return totalFrags;
}

/*******************  FUNCTION  *********************/
uint64_t Amc40Reader::initBaseCollision ( Amc40MepPacketList& queue )
{
	uint64_t min = -1;
	for (auto & it : queue) {
		uint64_t evid = be64toh(it->evid);
		if (evid < min)
			min = evid;
	}
	return min;
}

/*******************  FUNCTION  *********************/
/**
 * Start to convert the AMC40 packets to something like what will be used by the PCIe-40 to feed
 * the DAQPIPE RU.
**/
void Amc40Reader::convert ()
{
	size_t sel = 0;
	uint64_t base;
	uint64_t id;

	//prepare tmp queue
	Amc40MepPacketList dataQueue;
	Amc40MepPacketList tfcQueue;

	//lock the struct & extract all the interesting entries
	mutex.lock();
	//already done by another thread
	if (queuedFragments < DAQ_AMC40_BUFFERING_MARGIN*eventCollisions) {
		mutex.unlock();
		return;
	}

	//debug
	DAQ_DEBUG("amc40","Unqueue and convert");
	
	//init
	if (nextBaseCollisionId == 0) {
		nextBaseCollisionId = initBaseCollision(dataFetched);
		DAQ_DEBUG_ARG("amc40","Init base collision id : %1").arg(nextBaseCollisionId).end();
	}

	//extract
	extractMepPackets(tfcQueue,tfcFetched);
	queuedFragments -= extractMepPackets(dataQueue,dataFetched);

	//find a free id to store it
	sel = 0;
	while (sel < storedEvents && eventIds[sel] != DAQ_EVENT_ID_NOT_USED)
		sel++;

	//check if have position
	if (sel == storedEvents)
	{
		//if not found, whipe out
		DAQ_WARNING_ARG("Not place in readout buffers, skip the event !");
		sel = -1;
	} else {
		//mark as reserved
		eventIds[sel] = DAQ_EVENT_ID_RESERVED;
		//update base collition for next thread
		base = nextBaseCollisionId;
		nextBaseCollisionId += eventCollisions;
	}

	//get id
	id = eventId;
	eventId++;
	mutex.unlock();

	//if have data convert them
	if (sel != -1ul) {
		DAQ_DEBUG("amc40","Start conversion");
		dataConvert(dataQueue,sel,base);
		tfcConvert(tfcQueue,sel,base);

		//mark as usable by the RU
		eventIds[sel] = id;
		assert(id != DAQ_EVENT_ID_RESERVED && id != DAQ_EVENT_ID_NOT_USED);
	}

	//flush memory for reuse
	mutex.lock();
	for (Amc40MepPacketList::iterator it = dataQueue.begin() ; it != dataQueue.end() ; ++it)
		memory.push_back(*it);
	for (Amc40MepPacketList::iterator it = tfcQueue.begin() ; it != tfcQueue.end() ; ++it)
		memory.push_back(*it);
	mutex.unlock();

	//empty queue
	dataQueue.clear();
	tfcQueue.clear();
}

/*******************  FUNCTION  *********************/
/**
 * compute aligned size from bit to byte. (aliend to 8 bytes).
**/
static size_t alignedByteSize(size_t size)
{
	//use padding
	if (size % 64 != 0)
		size += 64 - size % 64;
	return size / 8;
}

/*******************  FUNCTION  *********************/
/**
 * Disable printing of warning when observe packet loss.
**/
void Amc40Reader::disableWarnSkip ()
{
	this->warnSkip = false;
}

/*******************  FUNCTION  *********************/
/**
 * Convert the data packet to the output format.
 * @param pos The output position in the output buffer (not the offset, need to by multiplied by maxEventSize).
 * @param base Define the base collision id for the mep.
**/
void Amc40Reader::dataConvert ( Amc40MepPacketList& queue, int pos, uint64_t base )
{
	//vars
	char * curData = this->data + pos * maxEventSize;
	EventMetaData & curMeta = this->meta->getEvent(pos,eventCollisions);
	uint64_t end = base + eventCollisions;
	curMeta.baseEventId = base;

	//check
	assume(queue.empty() == false,"Get an empty queue, nothing to convert ?");

	size_t fragmentId = 0;
	Amc40MepPacketList tmpQueue;
	while (queue.empty() == false)
	{
		//search lower event id
		Amc40MepPacketList::iterator lower = queue.begin();
		Amc40MepPacketList::iterator it = queue.begin();
		++it;
		while (it != queue.end()) {
			if (be64toh((*it)->evid) < be64toh((*lower)->evid))
				lower = it;
			++it;
		}

		//ease access
		Amc40MEPHeader* cur = *lower;
		uint64_t evid = be64toh(cur->evid);
		uint32_t frags = ntohs(cur->frags);
		char * packetData = (char*)(cur+1);
		//DAQ_DEBUG_ARG("AMC40","Data : %1").arg(packetData+4).end();

		//check
		//assert(base <= evid);

		//skip missing
		if (base < evid && warnSkip)
			DAQ_WARNING_ARG("Have to skip some fragments (%1)").arg(evid - base).end();

		while (base < evid) {
			for (size_t i = 0 ; i < evid - base ; i++) {
				curMeta.getEntry(fragmentId).size = 0;
			}
			base++;
			fragmentId++;
		}

		//check non complete contained packets (overlapping 2 meps)
		size_t skip = 0;
		assert(base >= evid);
		skip = base-evid;

		//loop on collisions inside packet
		for (size_t i = 0 ; i < frags ; i++)
		{
			//invert bit orders
			uint32_t * h = (uint32_t *)packetData;
			*h = ntohl(*h);
			
			//extract header
			Amc40MEPFragmentHeader * header = (Amc40MEPFragmentHeader*)packetData;
			size_t size = alignedByteSize((header->size) + 8*sizeof(Amc40MEPFragmentHeader)); //TODO ntohl
			assumeArg(size <= maxCollisionSize,"Data is too big (%1), expect smaller than %2 !").arg(size).arg(maxCollisionSize).end();
			
			//check if need to skip
			if (i >= skip)
			{
				curMeta.getEntry(fragmentId).size = size;
				memcpy(curData,packetData,size);
				curData += size;
				packetData += size;
				fragmentId++;
				base++;
			} else {
				packetData += size;
			}
			
			//skip if packet is larger
			if (fragmentId >= eventCollisions)
				break;
		}

		tmpQueue.push_back(cur);
		queue.erase(lower);
	}

	//skip missing
	if (base < end && warnSkip)
		DAQ_WARNING_ARG("Have to skip some fragments (%1)").arg(end - base).end();

	while (base < end) {
		for (size_t i = 0 ; i < end - base ; i++) {
			curMeta.getEntry(fragmentId).size = 0;
		}
		base++;
		fragmentId++;
	}

	//if missing last packet
	if (fragmentId < eventCollisions && warnSkip)
		DAQ_WARNING_ARG("Have to skip some fragments (%1)").arg(eventCollisions - base).end();

	while (fragmentId < eventCollisions) {
		for (size_t i = 0 ; i < eventCollisions - fragmentId ; i++) {
			curMeta.getEntry(fragmentId).size = 0;
		}
		base++;
		fragmentId++;
	}

	//re-push the content so the caller can unallocate the buffers (or register in pool for later use)
	queue.splice(queue.begin(),tmpQueue);
}

/*******************  FUNCTION  *********************/
/**
 * Convert the data packet to the output format.
 * @param pos The output position in the output buffer (not the offset, need to by multiplied by maxEventSize).
 * @param base Define the base collision id for the mep.
**/
void Amc40Reader::tfcConvert ( Amc40MepPacketList& queue, int pos, uint64_t base )
{
	//vars
	char * curData = this->tfcData + pos * maxEventSize;
	EventMetaData & curMeta = this->tfcMeta->getEvent(pos,eventCollisions);
	uint64_t end = base + eventCollisions;
	curMeta.baseEventId = base;

	//check
	//assume(queue.empty() == false,"Get an empty queue, nothing to convert ?");
	static bool once = true;
	if (once && queue.empty()) {
		DAQ_WARNING("Have no TFC data !");
		once = false;
	}

	size_t fragmentId = 0;
	Amc40MepPacketList tmpQueue;
	while (queue.empty() == false)
	{
		//search lower event id
		Amc40MepPacketList::iterator lower = queue.begin();
		Amc40MepPacketList::iterator it = queue.begin();
		++it;
		while (it != queue.end()) {
			if (be64toh((*it)->evid) < be64toh((*lower)->evid))
				lower = it;
			++it;
		}

		//ease access
		Amc40MEPHeader* cur = *lower;
		uint64_t evid = be64toh(cur->evid);
		uint32_t frags = ntohs(cur->frags);
		char * packetData = (char*)(cur+1);

		//skip missing
		if (base < evid && warnSkip)
			DAQ_WARNING_ARG("Have to skip some fragments (%1)").arg(evid - base).end();

		while (base < evid) {
			for (size_t i = 0 ; i < evid - base ; i++) {
				curMeta.getEntry(fragmentId).size = 0;
			}
			base++;
			fragmentId++;
		}

		//check non complete contained packets (overlapping 2 meps)
		size_t skip = 0;
		assert(base >= evid);
		skip = base-evid;

		//loop on collisions inside packet
		for (size_t i = 0 ; i < frags ; i++)
		{
			//invert bit orders
			uint32_t * h = (uint32_t *)packetData;
			*h = ntohl(*h);
			
			//extract header
			size_t size = sizeof(Amc40TfcFragment);
			Amc40MEPFragmentHeader * header = (Amc40MEPFragmentHeader *)curData;
			
			//check if need to skip
			if (i >= skip)
			{
				header->bxi = 0;
				header->size = size*8;
				curData = (char*)(header+1);
				curMeta.getEntry(fragmentId).size = size + sizeof(Amc40MEPFragmentHeader);
				memcpy(curData,packetData,size);
				curData += size;
				packetData += size;
				fragmentId++;
				base++;
			} else {
				packetData += size;
			}
			
			//skip if packet is larger
			if (fragmentId >= eventCollisions)
				break;
		}

		tmpQueue.push_back(cur);
		queue.erase(lower);
	}

	//skip missing
	if (base < end && warnSkip)
		DAQ_WARNING_ARG("Have to skip some fragments (%1)").arg(end - base).end();

	while (base < end) {
		for (size_t i = 0 ; i < end - base ; i++) {
			curMeta.getEntry(fragmentId).size = 0;
		}
		base++;
		fragmentId++;
	}

	//if missing last packet
	if (fragmentId < eventCollisions && warnSkip)
		DAQ_WARNING_ARG("Have to skip some fragments (%1)").arg(eventCollisions - base).end();

	while (fragmentId < eventCollisions) {
		for (size_t i = 0 ; i < eventCollisions - fragmentId ; i++) {
			curMeta.getEntry(fragmentId).size = 0;
		}
		base++;
		fragmentId++;
	}

	//re-push the content so the caller can unallocate the buffers (or register in pool for later use)
	queue.splice(queue.begin(),tmpQueue);
}

}
