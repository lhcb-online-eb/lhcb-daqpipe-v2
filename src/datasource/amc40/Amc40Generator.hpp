/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_AMC40_GENERATOR_HPP
#define DAQ_AMC40_GENERATOR_HPP

/********************  HEADERS  *********************/
#include <cstdlib>
#include "Amc40StreamStruct.hpp"

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/********************  MACROS  **********************/
/**
 * Max size for the UDB packets to generate.
**/
#define MAX_UDP_SIZE (9*1024)

/********************  STRUCT  **********************/
/**
 * State attached to each connection to track the current packet content to generate.
**/
struct Amc40GenState
{
	Amc40GenState();
	/** Teack the global collision ID (the one to generate) **/
	uint64_t collisionId;
	/** 
	 * Packet sequence number to detect packet drop on the reciever side. 
	 * Incremented after each packet be sent on the network.
	**/
	uint32_t seqNum;
	/** Number of collisions already generated for the current MEP **/
	uint64_t count;
	/** Number of MEPs generated, so current MEP id **/
	uint64_t meps;
};

/*******************  FUNCTION  *********************/
size_t genPacket(Amc40GenState &state,char * buffer,size_t maxSize, uint64_t eventCollisions);
size_t genTfcPacket(Amc40GenState &state,char * buffer,size_t maxSize, uint64_t eventCollisions);

}

#endif //DAQ_AMC40_GENERATOR_HPP

