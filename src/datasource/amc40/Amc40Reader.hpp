/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_AMC40_READER_HPP
#define DAQ_AMC40_READER_HPP

/********************  HEADERS  *********************/
#include <cstdint>
#include <cstdlib>
#include <list>
#include <mutex>
#include <thread>
#include "gochannels/GoChannel.hpp"
#include "common/Config.hpp"
#include "Amc40StreamStruct.hpp"

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/********************  MACROS  **********************/
#define DAQ_AMC40_MAX_LINES 2
#define DAQ_AMC40_MAX_THREADS 64
#define DAQ_MEP_SIZE 64
#define DAQ_EVENT_ID_NOT_USED 0
#define DAQ_EVENT_ID_RESERVED 1

/*********************  TYPES  **********************/
struct EventMetaData;

/********************  STRUCT  **********************/
/**
 * Define on incoming line (link) from the AMC40. This class track its state.
**/
struct Amc40Line
{
	//setup socket
	Amc40Line();
	void setup( size_t packetMaxSize, int dataPort, int tfcPort);
	void close();
	int listen(size_t linuxBufSize,int port);
	//members
	/** Network interface to use (eth0...) **/
	std::string iface;
	/** Keep track of the packet sequence number to detect packet loss for data stream **/
	uint32_t expectedDataSeqNum;
	/** Keep track of the packet sequence number to detect packet loss for tfc stram **/
	uint32_t expectedTfcSeqNum;
	/** Socket handler for data stream **/
	int dataSock;
	/** Socket handler for tfc stream **/
	int tfcSock;
	/** Protect access to the struct **/
	std::mutex mutex;
};

/********************  STRUCT  **********************/
/**
 * Define a job to be used by the running threads.
**/
struct Amc40Job
{
	/** Define the line to use **/
	Amc40Line * line;
	/** Is it TFC or DATA **/
	bool tfc;
};

/*********************  TYPES  **********************/
/** Define a list of packet to build a MEP **/
typedef std::list< DAQ::Amc40MEPHeader* > Amc40MepPacketList;

/*********************  CLASS  **********************/
/**
 * Define the main AMC40 reader class. It can handle multiple links and two streams per link
 * (data and tfc).
**/
class Amc40Reader
{
	public:
		Amc40Reader( const DAQ::Config* config, DAQ::EventMetaData* meta, char* data, DAQ::EventMetaData* tfcMeta, char* tfcData, uint64_t* eventIds, size_t storedEvents, int railId,bool listen );
		~Amc40Reader();
		//for unit tests
		void pushDataPacket(int lineId,void * buffer,size_t size);
		void pushTfcPacket(int lineId,void * buffer,size_t size);
		void disableWarnSkip();
	private:
		void startThreads( bool listen );
		void stopThreads();
		void threadMain();
		void convert();
		void * allocateBuffer( bool skipMutex = false );
		void pushDataPacket(Amc40MEPHeader * packet);
		void pushTfcPacket(Amc40MEPHeader * packet);
		void pushDataPacket(Amc40Line& line,void * buffer,size_t size);
		void pushTfcPacket(Amc40Line& line,void * buffer,size_t size);
		void dataConvert( Amc40MepPacketList& queue, int pos, uint64_t base );
		void tfcConvert( Amc40MepPacketList& queue, int pos, uint64_t base );
		size_t extractMepPackets ( DAQ::Amc40MepPacketList& outQueue, DAQ::Amc40MepPacketList& inQueue );
		uint64_t initBaseCollision(Amc40MepPacketList& queue);
		bool contain( uint64_t refStart, uint64_t refSize, uint64_t segStart, uint64_t segSize );
		Amc40MEPHeader* dup( DAQ::Amc40MEPHeader* packet );
	private:
		//config
		/** Count the number of lins (links) incoming from the AMC40 **/
		int lineCnt;
		/** Define the data port to use on each link **/
		int dataPort;
		/** Define the tfc port to use on each link **/
		int tfcPort;
		/** Number of thread to use to schedule the jobs **/
		int threadCnt;
		/** Define the current event id in use **/
		int eventId;
		/** Define the maximal size of a packer **/
		size_t packetMaxSize;
		/** Define the number of collisions in a MEP **/
		size_t eventCollisions;
		//lines & threads
		/** Array of lines **/
		Amc40Line lines[DAQ_AMC40_MAX_LINES];
		/** Thread handlers **/
		std::thread threads[DAQ_AMC40_MAX_THREADS];
		//output
		/** Output meta data pointer (rail 0). **/
		EventMetaData * meta;
		/** Output meta data pointer (rail 0). **/
		char * data;
		/** Output meta data pointer for TFC (rail 1) **/
		EventMetaData * tfcMeta;
		/** Output data pointer for TFC (rail 1) **/
		char * tfcData;
		/** Array of event ids to map evendID to position **/
		uint64_t * eventIds;
		/** Number of mep stored into the data/meta data pointers **/
		size_t storedEvents;
		/** Maximal size of an event. **/
		size_t maxEventSize;
		/** Max size of a single collision **/
		size_t maxCollisionSize;
		//packet tracking
		/** Keep track of the number of segment in queue **/
		uint64_t queuedFragments;
		/** Keep track of the next base collision id (for current mep) **/
		uint64_t nextBaseCollisionId;
		/** Keep track of data packet unitil we get enougth to build a mep **/
		Amc40MepPacketList dataFetched;
		/** Keep track of tfc packet unitil we get enougth to build a mep **/
		Amc40MepPacketList tfcFetched;
		/** Keep track of unused memory buffer for future reuse and avoiding to malloc/free every time. **/
		std::list<void*> memory;
		//thread managment
		/** protect access to the shared elements in threads **/
		std::mutex mutex;
		/** bool used to stop the running threads **/
		bool running;
		/** List of job to be used by threads **/
		GoChannel<Amc40Job> jobs;
		/** enable socket usage (be disable for unit tests) **/
		bool hasListen;
		/** Enable warning on packet loss detection **/
		bool warnSkip;
};

}

#endif //DAQ_AMC40_READER_HPP

