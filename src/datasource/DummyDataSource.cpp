/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include <cassert>
#include <cstring>
#include<iostream>
#include <vector>
#include <common/Debug.hpp>
#include "DummyDataSource.hpp"
#ifdef HAVE_HTOPML
	#include "htopml/DynamicConfigHttpNode.hpp"
#endif //HAVE_HTOPML

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/********************  STRUCT  **********************/
struct DummySize
{
	size_t cnt;
	size_t bw;
	size_t from;
	size_t to;
};

/*******************  CONSTS  ***********************/
#define cstRealSizesCnt 7
static DummySize cstRealSizes[cstRealSizesCnt] = {
	{52,55},//Velo
	{68,70},//UT
	{144,46},//SCIFI
	{65,71},//Rich1
	{36,73},//Rich2
	{52,90},//Calo
	{28,92},//muron
};

/*******************  FUNCTION  *********************/
/**
 * Constructor of the dummy data source. It mostly allocate the memory, fetch all the configuration params
 * and start the worker threads.
 * @param config Define the config object.
 * @param transport Define the transport object.
**/
DummyDataSource::DummyDataSource ( const Config* config, Transport * transport)
                :DataSource(config,transport)
{
	//extract
	this->storedEvents = config->readoutUnitEvents;
	this->maxEventSize = config->eventRailMaxDataSize;
	this->eventCollisions = config->getEventCollisions();
	this->dataChecking = config->dataChecking;
	this->randomSize = config->dataSizeRandomRatio;
	this->collisionRailDataSize = config->collisionRailDataSize;
	this->threadCnt = config->getAndSetConfigInt("dataSource.dummy","threads",1);
	this->transport = transport;
	this->realSize = config->getAndSetConfigBool("dataSource.dummy","realSize",false);
	this->realSizeMode = config->getAndSetConfigString("dataSource.dummy","realSizeMode","grouped");
	this->realSizeRandomSeed = config->getAndSetConfigInt("dataSource.dummy","realSizeRandomSeed",0);
	
	//register parameter config->collisionRailDataSize for dynamic configuration
	#ifdef HAVE_HTOPML
		DynamicConfigHttpNode::registerParam("collisionRailDataSize", collisionRailDataSize,1,collisionRailDataSize,[this](long value){
			this->collisionRailDataSize=value;
			std::cout<<value;
		});
	#endif //HAVE_HTOPML

	DAQ_DEBUG_ARG("rapidio", "maxEventSize %1 storedEvents %2 sizeof(EventMetaData) %3 eventCollisions %4")
		.arg(maxEventSize)
		.arg(storedEvents)
		.arg(sizeof(EventMetaDataEntry))
		.arg(eventCollisions)
		.end();
	//TODO remove from config or as function
	assume(config->getDefaultMetaDataSize() >= EventMetaData::getTotalSize(eventCollisions),"Invalid too small meta data size configured in config object");
	assumeArg(threadCnt < DAQ_DUMMY_MAX_THREADS,"Invalid number of thread %1, must be lower than %2").arg(threadCnt).arg(DAQ_DUMMY_MAX_THREADS).end();
	
	//same segment mode
	this->sameSegmentMode = config->getAndSetConfigBool("testing","sameSegment",false);
	
	//allocate buffers
	for (int rail = 0 ; rail < DAQ_DATA_SOURCE_RAILS ; rail++) {
		this->data[rail] = (char *) transport->allocateRdmaSegment(maxEventSize * storedEvents, false);
		this->meta[rail] = (EventMetaData *) transport->allocateRdmaSegment(EventMetaData::getTotalSize(eventCollisions) * storedEvents, false);
		memset(this->data[rail],0,maxEventSize * storedEvents);
		memset(this->meta[rail],0,EventMetaData::getTotalSize(eventCollisions) * storedEvents);
	}
	
	this->eventIds = new uint64_t[storedEvents];
	this->unitId = -1;
	
	//setup real size if enabled
	this->setupSize(transport->getRank(),transport->getWorldSize());
	
	//generate events
	this->nextEventId = 10;
	for (int i = 0 ; i < storedEvents ; i++) {
		generateEvent(i,nextEventId++);
	}
	
	//start threads
	for (int i = 0 ; i < threadCnt ; i++) {
		threads[i] = std::thread([this](){
			this->worker();
		});
	}
	
	//print memory usage
	size_t mem = (maxEventSize * storedEvents + EventMetaData::getTotalSize(eventCollisions) * storedEvents) * DAQ_DATA_SOURCE_RAILS;
	DAQ_DEBUG_ARG("mem","Using %1 of memory for %2").argUnit1024(mem).arg("datasource").end();
}

/*******************  FUNCTION  *********************/
/**
 * Stop the worker threads and free the memory buffers.
**/
DummyDataSource::~DummyDataSource ()
{
	//stop threads
	jobs.close();
	for (int i = 0 ; i < threadCnt ; i++)
		threads[i].join();
	
	//free mem
	for (int rail = 0 ; rail < DAQ_DATA_SOURCE_RAILS ; rail++) {
		transport->unregisterSegment(this->data[rail], maxEventSize * storedEvents);
		transport->unregisterSegment(this->meta[rail], EventMetaData::getTotalSize(eventCollisions) * storedEvents);
		transport->deallocateRdmaSegment(this->data[rail], maxEventSize * storedEvents, false);
		transport->deallocateRdmaSegment(this->meta[rail], EventMetaData::getTotalSize(eventCollisions) * storedEvents, false);
	}
	delete [] this->eventIds;
}

/*******************  FUNCTION  *********************/
void DummyDataSource::setupSize ( int rank, int nodes )
{
	//trivial if disabled
	if (!realSize)
	{
		realSizePercentage = 100;
		return;
	}
	
	//select mode
	if (this->realSizeMode == "grouped")
	{
		realSizePercentage = this->setupSizeGrouped(rank,nodes);
	} else if (this->realSizeMode == "random") {
		realSizePercentage = this->setupSizeRandom(rank,nodes);
	} else {
		DAQ_FATAL_ARG("Invalid realSizeMode in dummy data source : %1, should be 'grouped' or 'random'"); 
	}
	
	DAQ_INFO_ARG("datasource percentage : %1").arg(realSizePercentage).end();
}

/*******************  FUNCTION  *********************/
size_t DummyDataSource::setupSizeGrouped ( int rank, int nodes )
{	
	//compute sum and max and prepare from to
	size_t max = 0;
	size_t cnt = 0;
	size_t size = 0;
	for (int i = 0 ; i < cstRealSizesCnt ; i++)
	{
		if (cstRealSizes[i].bw > max)
			max = cstRealSizes[i].bw;
		cstRealSizes[i].from = cnt;
		cnt += cstRealSizes[i].cnt;
		cstRealSizes[i].to = cnt;
	}
	
	//compute position
	size_t pos = (cnt * rank) / nodes;
	
	//extract size
	for (int i = 0 ; i < cstRealSizesCnt ; i++)
		if (pos >= cstRealSizes[i].from && pos < cstRealSizes[i].to)
			size = (100*cstRealSizes[i].bw);
		
	//ret
	return size / max;
}

/*******************  FUNCTION  *********************/
size_t DummyDataSource::setupSizeRandom ( int rank, int nodes )
{	
	//compute sum and max and prepare from to
	size_t max = 0;
	size_t cnt = 0;
	size_t size = 0;
	for (int i = 0 ; i < cstRealSizesCnt ; i++)
	{
		if (cstRealSizes[i].bw > max)
			max = cstRealSizes[i].bw;
		cstRealSizes[i].from = cnt;
		cnt += cstRealSizes[i].cnt;
		cstRealSizes[i].to = cnt;
	}
	
	//randomize ranks
	srand(realSizeRandomSeed);
	
	//build list
	std::vector<int> ranks;
	for (int i = 0 ; i < nodes ; i++)
		ranks.push_back(i);
	
	//randomize nodes ordering
	std::vector<int> rranks;
	for (int i = 0 ; i < nodes ; i++)
	{
		int v = rand()%ranks.size();
		rranks.push_back(ranks[v]);
		ranks.erase(ranks.begin()+v);
	}
	
	//conver rank
	rank = rranks[rank];
	
	//compute position
	size_t pos = (cnt * rank) / nodes;
	
	//extract size
	for (int i = 0 ; i < cstRealSizesCnt ; i++)
		if (pos >= cstRealSizes[i].from && pos < cstRealSizes[i].to)
			size = (100*cstRealSizes[i].bw);
		
	//ret
	return size / max;
}

/*******************  FUNCTION  *********************/
/**
 * Main function of a worker thread.
 * It simply loop on incomming job and generate the requested data.
**/
void DummyDataSource::worker ()
{
	DummyDataSourceJob job;
	while (job << jobs) {
		generateEvent(job.id,job.eventId);
	}
}

/*******************  FUNCTION  *********************/
/**
 * Register the internal buffers into the network drive for usage in RDMA opations.
 * @param transport Provide the transport object to be used for registration.
**/
void DummyDataSource::registerSegments ( Transport* transport )
{
	for (int rail = 0 ; rail < DAQ_DATA_SOURCE_RAILS ; rail++) {
		transport->registerSegment(this->data[rail], maxEventSize * storedEvents);
		transport->registerSegment(this->meta[rail], EventMetaData::getTotalSize(eventCollisions) * storedEvents);
	}
}

/*******************  FUNCTION  *********************/
/**
 * Return the data size of the given fragment, identified by id and rail.
 * @param id Id identifying the position of the fragment in buffers.
 * @param rail Rail to be used.
**/
int DummyDataSource::getDataSize ( int id, int rail )
{
	size_t size = 0;
	for (size_t colId = 0 ; colId < eventCollisions ; colId++ )
		size += meta[rail]->getEvent(id,eventCollisions).getEntry(colId).size;
	assert(size <= maxEventSize);
	return size;
}

/*******************  FUNCTION  *********************/
/**
 * Build a bucket for the given event id
 * @param bucket Reference to the bucket object to fill
 * @param eventId ID of the event to load into the bucket.
**/
void DummyDataSource::getBucket ( BucketInfo& bucket, uint64_t eventId )
{
	//search event Id in list
	int id = getId(eventId);
	
	//setup meta
	bucket.eventId = eventId;
	bucket.id = id;
	
	for (int rail = 0 ; rail < DAQ_DATA_SOURCE_RAILS ; rail++) {
		//compute read ids
		int rrail = rail;
		int rid = id;
		if (sameSegmentMode)
			rrail = rid = 0;
		
		//seutp segments
		bucket.metaFragments[rrail].base = reinterpret_cast<char*>(&(meta[rrail]->getEvent(rid,eventCollisions)));
		bucket.metaFragments[rrail].size = EventMetaData::getTotalSize(eventCollisions);
		bucket.dataFragments[rrail].base = &data[rrail][rid * maxEventSize];
		bucket.dataFragments[rrail].size = getDataSize(rid,rrail);
		
		//some debugging
		DAQ_DEBUG_ARG("datasource","getBucket() setup meta %1 meta.size %2").arg((void*)bucket.metaFragments[rrail].base).arg(bucket.metaFragments[rrail].size).end();
		DAQ_DEBUG_ARG("datasource","getBucket() setup data %1 data.size %2").arg((void*)bucket.dataFragments[rrail].base).arg(bucket.dataFragments[rrail].size).end();
	}
	
	//setup content to validate in debug mode
	if (dataChecking) {
		assume(sameSegmentMode == false,"Cannot use checking at same time as sameSegmentMode !");
		DAQ_DEBUG("datasource","getBucket() calling BucketInfo::fillForcheck() and BucketInfo::check()");
		bucket.fillForcheck(*config,unitId);
		//bucket.check(*config,unitId);
	}
}

/*******************  FUNCTION  *********************/
/**
 * Run data generation for the given event Id a the given position in memory.
 * @param id Define the position in memory.
 * @param eventId Define the event id to generate.
**/
bool DummyDataSource::generateEvent ( int id, uint64_t eventId )
{
	//setup event id
	DAQ_DEBUG_ARG("DummyDataSource","Gen event %1").arg(id).end();
	//for rails
	for (auto& e : meta) {
		//setup event id in header
		e->getEvent(id,eventCollisions).baseEventId = eventId * eventCollisions;
		//for each events
		size_t sum = 0;
		for (size_t colId = 0 ; colId < eventCollisions ; colId++) {
			sum += e->getEvent(id,eventCollisions).getEntry(colId).size = getRandomEventSize();
			assert(sum <= config->eventRailMaxDataSize);
		}
	}
	
	//mark ready for usage
	this->eventIds[id] = eventId;
	
	return true;
}

/*******************  FUNCTION  *********************/
/**
 * Generate a random size.
**/
size_t DummyDataSource::getRandomEventSize ()
{
	size_t s = (this->realSizePercentage*this->collisionRailDataSize)/100;
	if (randomSize == 0)
		return s;
	else
		return s - rand() % ((randomSize*s)/100);
// 	size_t cnt = 1 + (int) ((config->eventMaxDataSize / 1024) * (rand() / (RAND_MAX + 1.0)));
// 	assert(cnt * 1024UL <= config->eventMaxDataSize);
// 	return cnt * 1024;
}

/*******************  FUNCTION  *********************/
/**
 * Acknowledge that the given bucket has been sent through the network.
 * Then we can generate a new event at this location.
 * @param bucket Define the bucket to validate (mostly the id/eventID).
**/
void DummyDataSource::ackSent ( BucketInfo& bucket )
{
	//checks
	assume(bucket.id >= 0 && bucket.id < storedEvents,"Invalid ID");
	assume(bucket.eventId == eventIds[bucket.id],"Invalid event ID to ack event sending");
	DAQ_DEBUG_ARG("DummyDataSource","Ack Sent (bucket) with ID = %1").arg(bucket.id).end();
	
	//update status
	if (threadCnt == 0) {
		generateEvent(bucket.id,nextEventId++);
	} else {
		DummyDataSourceJob job;
		job.id = bucket.id;
		job.eventId = nextEventId++;
		job >> jobs;
	}
}

/*******************  FUNCTION  *********************/
/**
 * Acknowledge that the given bucket has been sent through the network.
 * Then we can generate a new event at this location.
 * @param eventId Define the event id to validate.
**/
void DummyDataSource::ackSent ( uint64_t eventId )
{
	//search event Id in list
	int id = 0;
	while (id < this->storedEvents && this->eventIds[id] != eventId)
		id++;
	
	//check if no room
	assumeArg(id < this->storedEvents,"Cannot find requested eventId (%1) to make it done").arg(eventId).end();
	DAQ_DEBUG_ARG("DummyDataSource","Ack Sent (eventOd) with ID = %1 : %2").arg(eventId).arg(id).end();
	
	//update status
	if (threadCnt == 0) {
		generateEvent(id,nextEventId++);
	} else {
		DummyDataSourceJob job;
		job.id = id;
		job.eventId = nextEventId++;
		job >> jobs;
	}
}

/*******************  FUNCTION  *********************/
/**
 * Setup the current unit id.
**/
void DummyDataSource::setId ( int unitId )
{
	this->unitId = unitId;
}

/*******************  FUNCTION  *********************/
/**
 * Return the ID (position) of a given eventId.
 * @param eventId ID of the event we want to search the position.
**/
int DummyDataSource::getId ( uint64_t eventId )
{
	//search event Id in list
	int id = 0;
	while (id < this->storedEvents && this->eventIds[id] != eventId)
		id++;

	//check if no room
	/*if (id >= this->storedEvents)
	{
		for (int i = 0 ; i < this->storedEvents ; i++)
			DAQ_INFO_ARG("%1").arg(this->eventIds[i]).end();
	}*/
	assumeArg(id < this->storedEvents,"Request invalid event, not in available data, maybe there is contention somewhere and ressources are exhausted (evtId = %1, next = %2)").arg(eventId).arg(nextEventId).end();
	//DAQ_DEBUG_ARG("DummyDataSource","Get Id %1").arg(id).end();

	return id;
}

/*******************  FUNCTION  *********************/
/**
 * Compute the total size of a bucket (data and meta-data of each rails).
 * @param eventId Event ID to request.
**/
size_t DummyDataSource::getTotalSize ( uint64_t eventId )
{
	int id = getId(eventId);
	size_t res = 0;
	for (int rail = 0 ; rail < DAQ_DATA_SOURCE_RAILS ; rail++)
		res += getDataSize(id,rail) + config->getDefaultMetaDataSize();
	return res;
}

/*******************  FUNCTION  *********************/
/**
 * Check if the given event id has already been generated and is available for sending.
 * This function is the point of sync between the workers and the main thread.
 * @param currentEventId Define the wanted event Id.
**/
bool DummyDataSource::IsEventAvailable ( uint64_t currentEventId )
{
	//search event Id in list
	int id = 0;
	while (id < this->storedEvents && this->eventIds[id] != currentEventId)
		id++;

	return id < this->storedEvents;
}

/*******************  FUNCTION  *********************/
/**
 * Return the occupency of the buffers.
**/
int DummyDataSource::getFilledPercentage()
{
	int nonZeroEvents = 0; 
	for(int i = 0; i < storedEvents; i++ )
	{
		if (this->eventIds[i] != 0)
			nonZeroEvents ++;
	}
	return int((nonZeroEvents/storedEvents)*100);
}


}
