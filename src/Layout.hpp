/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/**
 * This file is used to setup the way to assemble daqpipe by selecting
 * the wanted components for every abstraction (launcher, transport and driver).
**/

#ifndef DAQ_LAYOUT_HPP
#define DAQ_LAYOUT_HPP

/********************  HEADERS  *********************/
//must be on top
#include "config.h"

/********************  MACROS  **********************/
#ifdef HAVE_HYDRA_LAUNCHER
	#include "launcher/LauncherHydra.hpp"
	#define LAUNCHER DAQ::LauncherHydra
#endif
#ifdef HAVE_INTERNAL_LAUNCHER
	#include "launcher/LauncherInternal.hpp"
	#define LAUNCHER DAQ::LauncherInternal
#endif

/********************  MACROS  **********************/
#ifdef TRANSPORT_LOCAL
	#include "transport/TransportMT.hpp"
	#include "launcher/LauncherLocal.hpp"
	#include "drivers/DriverLocal.hpp"
	namespace Layout
	{
		typedef DAQ::LauncherLocal Launcher;
		typedef DAQ::TransportMT Transport;
		typedef DAQ::DriverLocal Driver;
	};
#elif defined(TRANSPORT_MPI)
	#include "transport/TransportMT.hpp"
	#include "launcher/LauncherMPI.hpp"
	#include "drivers/DriverMPI.hpp"
	namespace Layout
	{
		typedef DAQ::LauncherMPI Launcher;
		typedef DAQ::TransportMT Transport;
		typedef DAQ::DriverMPI Driver;
	};
#elif defined(TRANSPORT_TCP)
	#include "transport/TransportMT.hpp"
	#include "drivers/DriverTCP.hpp"
	namespace Layout
	{
		typedef LAUNCHER Launcher;
		typedef DAQ::TransportMT Transport;
		typedef DAQ::DriverTCP Driver;
	};
#elif defined(TRANSPORT_UDP)
	#include "transport/TransportMT.hpp"
	#include "drivers/DriverUDP.hpp"
	namespace Layout
	{
		typedef LAUNCHER Launcher;
		typedef DAQ::TransportMT Transport;
		typedef DAQ::DriverUDP Driver;
	};
#elif defined(TRANSPORT_VERBS)
	#include "transport/TransportMT.hpp"
	#include "drivers/DriverVerbs.hpp"
	namespace Layout
	{
		typedef LAUNCHER Launcher;
		typedef DAQ::TransportMT Transport;
		typedef DAQ::DriverVerbs Driver;
	};

#elif defined(TRANSPORT_LIBFABRIC)
	#include "transport/TransportMT.hpp"
	#include "drivers/DriverLibFabric.hpp"
	namespace Layout
	{
		typedef LAUNCHER Launcher;
		typedef DAQ::TransportMT Transport;
		typedef DAQ::DriverLibFabric Driver;
	};
#elif defined(TRANSPORT_RAPIDIO)
	#include "transport/TransportMT.hpp"
	#include "drivers/DriverRapidIO.hpp"
	namespace Layout
	{
		typedef LAUNCHER Launcher;
		typedef DAQ::TransportMT Transport;
		typedef DAQ::DriverRapidIO Driver;
	};
#elif defined(TRANSPORT_PSM2)
	#include "transport/TransportMT.hpp"
	#include "drivers/DriverPSM2.hpp"
	namespace Layout
	{
		typedef LAUNCHER Launcher;
		typedef DAQ::TransportMT Transport;
		typedef DAQ::DriverPSM2 Driver;
	};
#else
	#error "Unsupported transport, check your TRANSPORT variable (support : LOCAL|MPI)"
#endif

//to select topology of units
#if defined(TRANSPORT_LOCAL)
	#define TOPOLOGY_LOCAL true
#elif defined(TRANSPORT_MPI) || defined(TRANSPORT_LIBFABRIC) || defined(TRANSPORT_TCP) || defined(TRANSPORT_RAPIDIO)|| defined(TRANSPORT_VERBS) || defined(TRANSPORT_PSM2) || defined(TRANSPORT_UDP)
	#define TOPOLOGY_LOCAL false
#else
	#error "Unsupported topology selection, please check TRANSPORT and TRANSPORT_* variables"
#endif

#endif //DAQ_LAYOUT_HPP
