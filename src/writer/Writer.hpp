/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_WRITER_HPP
#define DAQ_WRITER_HPP

/********************  HEADERS  *********************/
#include <common/Config.hpp>
#include <units/BuilderUnit.hpp>
#include <cstdio>
#include "FileStruct.hpp"
#include "MonitorSender.hpp"
#include <functional>

/*******************  NAMESPACE  ********************/
namespace DAQ
{
	
/*
 * FileHeader
 * eventMap[]
 * --event0---
 * segMap-rail0[]
 * segMap-rail1[]
 * data-rail0
 * data-rail1
 * --event1---
 * segMap-rail0[]
 * segMap-rail1[]
 * data-rail0
 * data-rail1
 * 
*/

/*********************  STRUCT  *********************/
/**
 * Define a copy job to reshape the data.
**/
struct CopyJob
{
	/** Size of the segment to copy **/
	size_t size;
	/** Source address of the segment to copy **/
	char * source;
	/** Destination address of the segment to copy **/
	char * dest;
};

/********************  STRUCT  **********************/
/**
 * Used to notify the minitor of a job.
**/
struct MonitorJob
{
	/** base address of the segment to give to the monitor. **/
	void * base;
	/** Size of the segment to give to the minotor. **/
	size_t size;
	/** Event ID given to the monitor. **/
	uint64_t eventId;
};

/*********************  CLASS  **********************/
/**
 * A writer take the data layout from the core part of daqpipe.
 * Meaning each RU segment contain the fragments which need to be
 * gathers before writing the file. This class is responsible of the
 * data reshaping.
 * It also extract some of the collisions for optional mintoring.
**/
class Writer
{
	public:
		Writer(const Config & config,int units, BuilderUnitData * meta,BuilderUnitData * data);
		~Writer();
		void flush(int creditId,std::function<void()> callback);
	protected:
		void startNewFile();
		void flushToFile();
		char* allocate();
		void deallocate( char* ptr );
		void threadMain();
	private:
		/** Keep track of the data storage from the filter unit **/
		BuilderUnitData * data;
		/** Keep track of the meta data storage from the filter unit **/
		BuilderUnitData * meta;
		/** Cursor in the current file content being built **/
		char * cursor;
		/** Buffer in which to build the current file content **/
		char * buffer;
		/** Current meps done **/
		size_t meps;
		/** How many meps to put in the file **/
		size_t mepsPerFile;
		/** Number of readout units to consider **/
		size_t units;
		/** Max number of events per mep **/
		size_t maxEventsPerMep;
		/** Pointer to the file header to be filled **/
		FileHeader * fileHeader;
		/** Fill the event map after the file header **/
		FileEventMapEntry * eventMap;
		/** List of copy job to be used in multi-threaded mode **/
		CopyJob * jobs;
		/** Keep track of events addresses to feed the monitor after data reshaping **/
		MonitorJob * mjobs;
		/** Keep track of the current offset to use to write into file buffer **/
		size_t * offsetBuffer;
		/** Current file name **/
		std::string filename;
		/** Current file ID **/
		size_t fileId;
		/** Monitor manager **/
		MonitorSender sender;
		/** Current event ID **/
		uint64_t eventId;
		/** Keep track of memory segments for reuse **/
		GoChannel<char*> memQueue;
		/** List of tasks to write the file **/
		GoChannel<std::function<void()>> tasks;
		/** Buffer size to allocate (max size of a file) **/
		size_t segSize;
		/** Number of worker threads to use to write files on disk **/
		int threadCnt;
		/** Kepp track of the running threads used to write file on disk. **/
		std::thread * threads;
		/** 
		 * Limit the number of buffers to allocated for pending file writing.
		 * It permit to detect too slow file writing while accumulating pending buffers
		**/
		int maxBuffers;
};

}

#endif //DAQ_WRITER_HPP 