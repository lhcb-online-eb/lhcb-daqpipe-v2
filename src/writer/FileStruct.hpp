/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_OUTPUT_FILE_STRUCT_HPP
#define DAQ_OUTPUT_FILE_STRUCT_HPP

/********************  HEADERS  *********************/
#include <cstdlib>
#include <cstdint>

/*******************  NAMESPACE  ********************/
namespace DAQ
{
	
/********************  MACRO  ***********************/
#define DAQ_FILE_FORMAT_VERSION 0
#define DAQ_FILE_MAGIK 0x314

/*********************  STRUCT  *********************/
/**
 * Structur of the file header.
**/
struct FileHeader
{
	/** Magik number to identify the file **/
	uint64_t magik;
	/** Date of file generation **/
	uint64_t dateTime;
	/** Id of the first event in the file **/
	uint64_t baseEventId;
	/** Unused bits for future **/
	uint64_t unused;
	/** Number of events in the file. **/
	uint32_t events;
	/** Number of read unit used to aggregate the datas. **/
	uint16_t units;
	/** Number of rails per unit **/
	uint8_t rails;
	/** Identify the format version of the current file. **/
	int8_t fileFormatVersion;
};

/*********************  STRUCT  *********************/
/**
 * Just after the file header, this struct contain the size and event ID
 * of each event in the file to be used as a map to jump to the data address.
**/
struct FileEventMapEntry
{
	/** Global event ID **/
	uint64_t eventId;
	/** Size of the data and meta data for this event **/
	uint32_t size;
};

/*********************  STRUCT  *********************/
/**
 * Content of a meta-data entry. This is used in the meta-data map at the base
 * of each event before the data itself.
**/
struct FileMetaEntry
{
	/** Aligned size of the data (aligned on 8 bytes) **/
	uint8_t size;
};

/********************  STRUCT  **********************/
struct __attribute__((__packed__)) Amc40MEPFragmentHeader
{
	/** Size of data (no header) **/
	uint32_t size:20;
	/** bxi **/
	uint16_t bxi:12;
};

/********************  STRUCT  **********************/
typedef Amc40MEPFragmentHeader FileFragmentHeader;

/*********************  STRUCT  *********************/
/**
 * Layout of the header in use to communicate with the minotor client.
**/
struct MonitorDataHeader
{
	/** Global event id of the event **/
	uint64_t eventId;
	/** Number of read unit used to aggregate the datas. **/
	uint16_t units;
	/** Number of rails per unit **/
	uint16_t rails;
	/** size of data to read **/
	uint32_t size;
};

}

#endif //DAQ_OUTPUT_FILE_STRUCT_HPP
