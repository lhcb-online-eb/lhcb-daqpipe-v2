/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include "MonitorSender.hpp"
#include <portability/TcpHelper.hpp>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <ifaddrs.h>
#include <netinet/in.h> 
#include <arpa/inet.h>
#include <cstring>
#include <unistd.h>

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*******************  FUNCTION  *********************/
/**
 * Constructor of monoitor
 * @param config Define the global config to be used.
**/
MonitorSender::MonitorSender ( const DAQ::Config& config )
{
	//vars
	struct sockaddr_in sin;
	int status;
	
	//config
	bool useIpV6 = config.getAndSetConfigBool("monitor","ipv6",false);
	std::string ip = config.getAndSetConfigString("monitor","ip","");
	int port = config.getAndSetConfigInt("monitor","port",3800);
	eventRatio = config.getAndSetConfigUInt64("monitor","ratio",1000);
	
	if (ip.empty()) {
		fd = -1;
	} else {
		/* Allocate a new socket */
		fd = ::socket( useIpV6 ? AF_INET6 : AF_INET , SOCK_STREAM, 0);
		assumeArg(fd>=0, "Error while calling socket() : %1").argStrErrno().end();
		
		/* Prepare connection parameters. */
		sin.sin_family = useIpV6 ? AF_INET6 : AF_INET;
		sin.sin_port = htons(port);
		inet_aton(ip.c_str(), &sin.sin_addr);
		DAQ_DEBUG_ARG("tcp","Try to connect to %1:%2").arg(ip).arg(sin.sin_port).end();
		
		/* Connect to the remote host. */
		status = connect(fd, (struct sockaddr*) &sin, sizeof(sin));
		assumeArg(status == 0, "Error while calling connect() : %1").argStrErrno().end();
		
		//start thread
		thread = std::thread([this](){this->threadMain();});
	}
}

/*******************  FUNCTION  *********************/
/**
 * Monitor destructor : to be used to stop the worker thread
 * and close the client connection.
**/
MonitorSender::~MonitorSender ()
{
	jobs.close();
	if (fd != -1) {
		thread.join();
		::close(fd);
	}
}

/*******************  FUNCTION  *********************/
/**
 * Main function for the worker thread.
**/
void MonitorSender::threadMain ()
{
	std::function<void()> task;
	DAQ_DEBUG("monitor","start");
	while (task << jobs)
		task();
	DAQ_DEBUG("monitor","stopped");
}

/*******************  FUNCTION  *********************/
/**
 * Filtering function to say which event to accept. It is currently a simple
 * modulo accepting one event every "eventRatio".
**/
bool MonitorSender::filter ( uint64_t eventId )
{
	return (fd != -1 && eventId % eventRatio == 0);
}

/*******************  FUNCTION  *********************/
/**
 * Be notified by the writer that one event is ready and giving its informations.
 * @param eventId Define the event ID
 * @param units Define the number of readout units
 * @param rails Define the number of rails.
 * @param base Define the base address containing the data.
 * @param size Define the size of the data and headers.
**/
void MonitorSender::notify ( uint64_t eventId, uint16_t units, uint16_t rails, void* base, size_t size )
{
	//check if interesting
	if (filter(eventId) == false) {
		return;
	} else {
		DAQ_DEBUG_ARG("monitor","Keep event %1").arg(eventId).end();
	}
	
	//copy data
	char * data = new char[size];
	memcpy(data,base,size);
	
	//build job
	std::function<void()> f = [=]() {		
		//build struct
		MonitorDataHeader header;
		header.eventId = eventId;
		header.rails = rails;
		header.units = units;
		header.size = size;
		
		//send header & data
		TcpHelper::safeSend(fd,&header,sizeof(header));
		TcpHelper::safeSend(fd,data,size);
		
		//free
		delete [] data;
	};
	
	//push job
	f >> jobs;
}

}
