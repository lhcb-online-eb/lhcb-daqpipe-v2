/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include "Writer.hpp"
#include <portability/TcpHelper.hpp>
#include <cstdio>
#include <unistd.h>

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*******************  FUNCTION  *********************/
/**
 * Constructor of the file writer.
 * @param config Define the global config in use.
 * @parma units Define the number of readout units in use.
 * @param meta Define the container of meta data from the filter unit.
 * @praam data Define the container of data from the filter unit.
**/
Writer::Writer(const Config& config, int units, BuilderUnitData* meta, BuilderUnitData* data)
	:sender(config)
{
	//setup
	this->meta = meta;
	this->data = data;
	this->meps = 0;
	this->fileId = 0;
	this->eventId = 0;
	this->mepsPerFile = config.getAndSetConfigUInt64("writer","meps",20);
	this->filename = config.getAndSetConfigString("writer","filename","lhcb-daq-%06lu.raw");
	this->maxEventsPerMep = config.getEventCollisions();
	this->threadCnt = config.getAndSetConfigInt("writer","threads",4);
	this->maxBuffers = config.getAndSetConfigInt("writer","maxBuffers",32);
	this->units = units;
	
	//setup buffer
	size_t mepSize = (config.getDefaultMetaDataSize() + config.eventRailMaxDataSize) * DAQ_DATA_SOURCE_RAILS * units;
	segSize = sizeof(FileHeader) + mepsPerFile * maxEventsPerMep * sizeof(FileEventMapEntry) + mepsPerFile * mepSize;
	
	//setup job array
	jobs = new CopyJob[maxEventsPerMep*DAQ_DATA_SOURCE_RAILS*units];
	mjobs = new MonitorJob[maxEventsPerMep];
	
	this->offsetBuffer = new size_t[units * DAQ_DATA_SOURCE_RAILS];
	
	//setup mem queue to not wait
	//memQueue.disableWaitOnEmpty();
	//fill memqueue
	for (int i = 0 ; i < maxBuffers ; i++)
		memQueue << new char[segSize];
	
	//start
	startNewFile();
	
	//create threads
	threads = new std::thread[threadCnt];
	for (int i = 0 ; i < threadCnt ; i++)
		threads[i] = std::thread([=](){this->threadMain();});
}

/*******************  FUNCTION  *********************/
/**
 * Destructor of the writer class.
**/
Writer::~Writer()
{
	//free buffers
	char * ptr;
	while (ptr << memQueue)
		delete [] ptr;
	
	//close threads
	tasks.close();
	for (int i = 0 ; i < threadCnt ; i++)
		threads[i].join();
	delete [] threads;
	
	//mem
	delete [] offsetBuffer;
	delete [] mjobs;
	delete [] jobs;
}

/*******************  FUNCTION  *********************/
/**
 * Start to prepare a new file. It allocate a buffer, setup the hreader, setup the event map
 * and place the cursor.
**/
void Writer::startNewFile()
{
	//setup header
	this->buffer = (char*)allocate();
	
	//reset counter
	this->meps = 0;
	
	//if buffer NULL (backpressure)
	if (this->buffer == nullptr)
	{
		static size_t cnt = 0;
		DAQ_WARNING_ARG("Get backpressure in file writer, skip data : %1 !").arg(cnt++).end();
		return;
	}
	
	//setup header
	this->fileHeader = (FileHeader*)buffer;
	this->fileHeader->baseEventId = eventId;
	this->fileHeader->dateTime = time(nullptr);
	this->fileHeader->events = 0;
	this->fileHeader->units = units;
	this->fileHeader->fileFormatVersion = DAQ_FILE_FORMAT_VERSION;
	this->fileHeader->rails = DAQ_DATA_SOURCE_RAILS;
	this->fileHeader->magik = DAQ_FILE_MAGIK;
	
	//setup event map
	this->eventMap = (FileEventMapEntry*)(fileHeader+1);
	memset(eventMap,0,sizeof(*eventMap) * mepsPerFile * maxEventsPerMep);
	
	//base data
	this->cursor = (char*)(this->eventMap + mepsPerFile * maxEventsPerMep);
}

/*******************  FUNCTION  *********************/
/**
 * Flush the data associated to the given credit into the file buffer.
 * It is responsible of the data conversion.
 * @param callback A callback to be call when the work is finished.
**/
void Writer::flush(int id,std::function<void()> callback)
{
	//debug
	DAQ_DEBUG_ARG("writer","flush mep %1 / %2").arg(meps).arg(mepsPerFile).end();
	
	//if not skip due to backpressue
	if (this->buffer != nullptr)
	{
		//keep track of offset progression in all data front
		size_t entries = units*DAQ_DATA_SOURCE_RAILS;
		memset(offsetBuffer,0,entries*sizeof(size_t));
		
		EventMetaData * mm = (EventMetaData*)meta->getIdPtr(id,0,0);
		uint64_t baseEventId = mm->baseEventId;
		
		//checking
		for (size_t unit = 0 ; unit < units ; unit++)
			for (int rail = 0 ; rail < DAQ_DATA_SOURCE_RAILS ; rail++)
				assume(baseEventId == ((EventMetaData*)meta->getIdPtr(id,rail,unit))->baseEventId,"Invalid event ID, not same in rails/unit !");

		//start loop on all events in mep
		int jobId = 0;
		for (size_t evt_id = 0 ; evt_id < maxEventsPerMep ; evt_id++) {
			//keep track of size
			char * eventBaseCursor = cursor;
			
			//setup meta data pointer
			FileMetaEntry * fmeta = (FileMetaEntry*)cursor;
			
			//move foward
			cursor = (char*)(fmeta+entries);
			
			//align
			if ((size_t)cursor * 8 != 0)
				cursor += 8 - (size_t)cursor % 8;
			
			//copy sizes and compute pointers for later copy
			int copyId = 0;
			for (size_t unit = 0 ; unit < units ; unit++) {
				for (int rail = 0 ; rail < DAQ_DATA_SOURCE_RAILS ; rail++) {
					
					//extract addresses
					EventMetaData * m = (EventMetaData*)meta->getIdPtr(id,rail,unit);
					char * d = data->getIdPtr(id,rail,unit);
					
					//check
					assert(m->baseEventId == baseEventId);
					
					//copy size
					fmeta[copyId].size = m->getEntry(evt_id).size;
					
					//setup job
					jobs[jobId].size = m->getEntry(evt_id).size;
					jobs[jobId].source = d + offsetBuffer[copyId];
					jobs[jobId].dest = cursor;
					
					//increm
					offsetBuffer[copyId] += m->getEntry(evt_id).size;
					copyId++;
					jobId++;
					cursor += m->getEntry(evt_id).size;
				}
			}
			
			//send
			size_t size = cursor - eventBaseCursor;
			mjobs[evt_id].base = eventBaseCursor;
			mjobs[evt_id].size = size;
			mjobs[evt_id].eventId = baseEventId + evt_id;
			
			//update event table
			eventMap[fileHeader->events].eventId = baseEventId + evt_id;
			//DAQ_DEBUG_ARG("writer","EventID = %1").arg(eventMap[fileHeader->events].eventId).end();
			eventMap[fileHeader->events].size = size;
			fileHeader->events++;
			eventId++;
		}

		//run the copies
		for (int i = 0 ; i < jobId ; i++)
			memcpy(jobs[i].dest,jobs[i].source,jobs[i].size);
		
		//monitor
		for (size_t id = 0 ; id < maxEventsPerMep ; id++)
			sender.notify(mjobs[id].eventId,units,DAQ_DATA_SOURCE_RAILS,mjobs[id].base,mjobs[id].size);
	}
	
	//incr
	meps++;
	if (meps == mepsPerFile) {
		this->flushToFile();
		this->startNewFile();
	}
	
	//callback
	callback();
}

/*******************  FUNCTION  *********************/
/**
 * Flush the memory content to the file on disk.
 * This function just setup the job which will be taken by a worker thread.
**/
void Writer::flushToFile()
{
	int fid = fileId;
	char * cur = cursor;
	char * buf = buffer;
	
	//backporessure, skip
	if (this->buffer == nullptr)
		return;
	
	//ok do write async
	std::function<void()> f = [this,fid,cur,buf]() {
		//compute filename
		char * fname = new char[filename.size()+32];
		sprintf(fname,filename.c_str(),fid);
		DAQ_DEBUG_ARG("writer","open file %1").arg(fname).end();
		
		//check if exist
		if ( access( fname, F_OK ) == 0 )
			DAQ_WARNING_ARG("Caution, will overwrite existing file : %1").arg(fname).end();
		
		//open
		FILE * fp = fopen(fname,"w");
		assumeArg(fp != nullptr,"Fail to open file %1 : %2").arg(fname).argStrErrno().end();
		
		//write to file
		TcpHelper::safeFWrite(fp,buf,cur - buf);
		
		//close
		fclose(fp);
		delete [] fname;
		
		//return buffer
		deallocate(buf);
	};

	//inc
	fileId++;
	f >> tasks;
}

/*******************  FUNCTION  *********************/
/**
 * Allocate a segment for the current file to be built. This permit to reuse
 * segment and not returning them to the OS every time.
**/
char* Writer::allocate ()
{
	//search in queue
	char * ptr = nullptr;
	ptr << memQueue;
	
	//no from buffer
	/*if (!status) {
		//assume(maxBuffers-- > 0,"Max buffer exhausted, your certainly write on file to slow for the input stream !");
		if (maxBuffers-- > 0)
			ptr = new char[segSize];
		else
			ptr = nullptr;
	}*/
	
	//ok return
	return ptr;
}

/*******************  FUNCTION  *********************/
/**
 * Register the free segment for latter resuse.
**/
void Writer::deallocate ( char* ptr )
{
	ptr >> memQueue;
}

/*******************  FUNCTION  *********************/
/**
 * Main function for the worker threads.
**/
void Writer::threadMain ()
{
	std::function<void()> task;
	while (task << tasks)
		task();
}

}
