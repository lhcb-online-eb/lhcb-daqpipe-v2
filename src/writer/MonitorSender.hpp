/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_MONITOR_SENDER_HPP
#define DAQ_MONITOR_SENDER_HPP

/********************  HEADERS  *********************/
#include <cstdlib>
#include <cstdint>
#include "FileStruct.hpp"
#include "gochannels/GoChannel.hpp"
#include "common/Config.hpp"
#include <functional>
#include <thread>

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*********************  CLASS  **********************/
/**
 * The monitor is a component inside the file writer to transmit some events (sampling)
 * to a monitoring client via TCP connection.
**/
class MonitorSender
{
	public:
		MonitorSender(const Config & config);
		~MonitorSender();
		void notify(uint64_t eventId,uint16_t units,uint16_t rails,void * base,size_t size);
	private:
		void threadMain();
		bool filter(uint64_t eventId);
	private:
		/** Handler of network connnection. **/
		int fd;
		/** List of jobs to transmit **/
		GoChannel<std::function<void()>> jobs;
		/** Keep track of the worker thread transmitting the data **/
		std::thread thread;
		/** Event ratio to extract and send to the client. **/
		uint64_t eventRatio;
};

}

#endif //DAQ_MONITOR_SENDER_HPP
