/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C

	     Purpose of this test program is to send
	     many messages around all the nodes.
	     Right now there is one message sent per
	     iteration, from one node to another.

	     An improvement would be to increase the 
	     frequency of the message sending to stress
	     test the system a bit more.


*****************************************************/

/********************  HEADERS  *********************/
#include <units/Unit.hpp>
#include <Layout.hpp>

/*******************  NAMESPACE  ********************/
using namespace DAQ;

/*********************  CLASS  **********************/
class PingPongMany : public Unit
{
	public:
		PingPongMany ( DAQ::Transport* transport );
		virtual void main ();
		void sendNext(int curr_id);
	private:
		Transport * transport;
};

/*******************  FUNCTION  *********************/
PingPongMany::PingPongMany ( Transport* transport ) : Unit("PingPong")
{
	this->transport = transport;
}

/*******************  FUNCTION  *********************/
void PingPongMany::main ()
{
	UnitCommand command;
	while(command << commandChannel) {
		switch(command.type) {
			// Transport will send UNIT_CMD_START to all units on program start
			case UNIT_CMD_START:
				this->sendNext(0);
				break;
			case UNIT_PING_PONG:
				DAQ_INFO_ARG("Recv ping %1").arg(command.args.pingPong.id).end();

				/* TODO: can we verify correct ping somehow?
				 *
				 * assumeArg(command.args.pingPong.id == id,"Invalid ID in ping pong of commands, get %1, expect %2")
					.arg(command.args.pingPong.id)
					.arg(id)
					.end();
				*/

				// The pattern of sending will be circular, starting with yourself.
				// rank 0 will start with 0 and then send to 1,2,3... etc.
				// rank 1 will start with 1 and then send to 2,3,4... 0 etc.
				// Note that the command id's will grow, though!
				this->sendNext(command.args.pingPong.id + 1);
				break;
			default:
				DAQ_FATAL_ARG("Get unexpected command in ReadoutUnit : %1").arg(command.type).end();
				break;
		}
	}
}

/*******************  FUNCTION  *********************/
void PingPongMany::sendNext(int curr_id)
{
	UnitCommand command;
	
	//exit or send
	if (curr_id >= 10000 ) {
		if( transport->getRank() == 0) {
			DAQ_INFO("Rank 0 has reached curr_id >= MAX, sending UNIT_CMD_EXIT to all.");
			command.type = DAQ::UNIT_CMD_EXIT;
			for (int i = 0; i < transport->getWorldSize(); i++) {
				transport->sendCommand(this,i,0,command);
			}
		} else {
			DAQ_INFO_ARG("Rank %1 has reached curr_id >= MAX").arg(transport->getRank()).end();
		}
	} else {
		command.type = UNIT_PING_PONG; // request for RDMA
		command.args.pingPong.id = curr_id;
		int dest = (transport->getRank() + curr_id) % transport->getWorldSize();
		DAQ_INFO_ARG("Send ping %1 to %2").arg(command.args.pingPong.id).arg(dest).end();
		
		// At some point (the start for example) we will send to ourselves.
		// This is taken care of the in the Transport, it checks if the send is local and 
		// will then push to the local command queue instead of sending a remote command.
		transport->sendCommand(this,(transport->getRank() + curr_id) % transport->getWorldSize(),0,command);
	}
}

/*******************  FUNCTION  *********************/
int main(int argc,char ** argv)
{
	//config
	Config config;
	config.parseArgs(argc,(const char**)argv);
	
	//launcher
	Layout::Launcher launcher(&config);
	launcher.initialize(argc,argv);
	Debug::setRank(launcher.getRank());
	
	//transport
	//setup Driver
	Layout::Driver driver;
	driver.initialize(config, launcher,argc,argv);
	
	//setup transport
	Layout::Transport transport(&config,&launcher,&driver);
	transport.initialize(config, launcher);
	
	//setup units
	for (int i = 0; i < transport.getWorldSize(); i++) {
		transport.setUnitType(i,0,UNIT_CUSTOM);
	}
	transport.setLocalUnit(0,new PingPongMany(&transport));
	
	//run
	transport.run();
	
	//finalize all
	transport.finalize();
	driver.finalize();
	launcher.finalize();
}
