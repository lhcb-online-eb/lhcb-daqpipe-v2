/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include "config.h"

//undefine all
#undef TRANSPORT_MPI
#undef TRANSPORT_TCP
#undef TRANSPORT_UDP
#undef TRANSPORT_VERBS
#undef TRANSPORT_LIBFABRIC
#undef TRANSPORT_LIBFABRIC_LEGACY
#undef TRANSPORT_RAPIDIO
#undef TRANSPORT_PSM2

//fallback on local
#define TRANSPORT_LOCAL

//take back the normal code
#include "../main.cpp"
