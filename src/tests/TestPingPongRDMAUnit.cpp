/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include <units/Unit.hpp>
#include <Layout.hpp>

/*******************  NAMESPACE  ********************/
using namespace DAQ;

/********************  MACROS  **********************/
#define SIZE 1024
#define WRITE_OFFSET (4096/sizeof(int))
#define READ_OFFSET (4096/sizeof(int))

/*********************  CLASS  **********************/
class PingPongUnit : public Unit
{
	public:
		PingPongUnit ( DAQ::Transport* transport );
		virtual void main ();
		void doNext();
		void regBuffer();
		void check();
		int getNextRank();
	private:
		Transport * transport;
		int id;
		int * dataWrite;
		int * dataRead;
		RemoteIOV iov;
};

/*******************  FUNCTION  *********************/
PingPongUnit::PingPongUnit ( Transport* transport ) : Unit("PingPong")
{
	assume(transport->getWorldSize() == 2,"Work only between 2 process");
	this->transport = transport;
	this->id = 0;
	this->iov.addr = nullptr;
	this->iov.key = 0;
	
	//aloc
	this->dataWrite = (int*)transport->allocateRdmaSegment(sizeof(int) * (SIZE + WRITE_OFFSET), true);
	this->dataRead = (int*)transport->allocateRdmaSegment(sizeof(int) * (SIZE + READ_OFFSET), false);
}

/*******************  FUNCTION  *********************/
void PingPongUnit::main ()
{
	UnitCommand command;
	while(command << commandChannel) {
		switch(command.type) {
			case UNIT_CMD_START:
				this->regBuffer();
				break;
			case UNIT_CMD_INIT_REG_BU_BUFFER:
				iov.addr = command.args.regBuffer.data[0].addr;
				iov.key = command.args.regBuffer.data[0].key;
				DAQ_INFO_ARG("Write IOV info: iov.key (target addr) %1 iov.addr(offset) %2").arg(iov.key).arg(iov.addr).end();
				this->doNext();
				break;
			case UNIT_PING_PONG:
				DAQ_INFO_ARG("Finish ping %1 on rank %2").arg(id).arg(transport->getRank()).end();
				if (transport->getRank() != id % 2) {
					this->check();
				}
				id++;
				this->doNext();
				break;
			default:
				DAQ_FATAL_ARG("Get unexpected command in ReadoutUnit : %1").arg(command.type).end();
				break;
		}
	}
}

/*******************  FUNCTION  *********************/
void PingPongUnit::regBuffer()
{
	//get iov
	transport->registerSegment(this->dataWrite, sizeof(int) * (SIZE + WRITE_OFFSET));
	RemoteIOV iovWrite = transport->getRemoteIOV(-1,this->dataWrite + WRITE_OFFSET, sizeof(int) * SIZE);

	transport->registerSegment(this->dataRead,sizeof(int) * (SIZE + READ_OFFSET));

	//prep command
	UnitCommand command;
	command.type = UNIT_CMD_INIT_REG_BU_BUFFER;
	command.args.regBuffer.data[0].addr = iovWrite.addr;
	command.args.regBuffer.data[0].key = iovWrite.key;

	//send
	transport->sendCommand(this,getNextRank(),0,command);
}

/*******************  FUNCTION  *********************/
void PingPongUnit::doNext()
{
	UnitCommand command;
	command.type = UNIT_PING_PONG;
	command.args.pingPong.id = id;
	
	DAQ_INFO("--------------------------------------------------");
	
	//exit or send
	if (id >= 10 && transport->getRank() == 0) {
		command.type = DAQ::UNIT_CMD_EXIT;
		transport->sendCommand(this,1,0,command);
		transport->sendCommand(this,0,0,command);
	} else if (transport->getRank() == id % 2) {
		DAQ_INFO_ARG("Is write, Address of dataRead[]: %1").arg((void*)dataRead).end();
		for (int i = 0 ; i < SIZE ; i++)
			dataRead[i + READ_OFFSET] = id + 20;
		command.args.pingPong.write = true;
		transport->rdmaWrite(this,getNextRank(),0,this->dataRead + READ_OFFSET,iov.addr,iov.key,sizeof(int)*SIZE,id,command);
	} else {
		DAQ_INFO_ARG("Is read, Address of dataWrite[]: %1").arg((void*)dataWrite).end();
		command.args.pingPong.write = false;
		//TODO is this correct with getNextRank and dataWrite?
		transport->postWaitRemoteRDMAWrite(this,getNextRank(),0,this->dataWrite + WRITE_OFFSET,sizeof(int)*SIZE,id,command);
	}
}

/*******************  FUNCTION  *********************/
int PingPongUnit::getNextRank()
{
	return (transport->getRank() + 1) % 2;
}

/*******************  FUNCTION  *********************/
void PingPongUnit::check()
{
	for (int i = 0 ; i < SIZE; i++) {
		if (dataWrite[i+WRITE_OFFSET] != id + 20)
			DAQ_FATAL_ARG("Invalid value in data buffer: dataWrite[%1]==%2 != id+20==%3").arg(i).arg(dataWrite[i+ WRITE_OFFSET]).arg(id+20).end();
//		DAQ_INFO_ARG("dataWrite[%1]==%2").arg(i).arg(dataWrite[i]).end();
		//TODO put back the assume, removed just to see what happens further on
		//assume(dataWrite[i] == id + 20,"Invalid value in data buffer");
	}
	DAQ_INFO("dataWrite validated");
}

/*******************  FUNCTION  *********************/
int main(int argc,char ** argv)
{
	//config
	Config config;
	config.parseArgs(argc,(const char**)argv);
	
	//launcher
	Layout::Launcher launcher(&config);
	launcher.initialize(argc,argv);
	Debug::setRank(launcher.getRank());
	
	//transport
	//setup Driver
	Layout::Driver driver;
	driver.initialize(config, launcher,argc,argv);
	
	//setup transport
	Layout::Transport transport(&config,&launcher,&driver);
	transport.initialize(config, launcher);
	
	//setup units
	assume(transport.getWorldSize() == 2,"Work ongly between 2 processes");
	transport.setUnitType(0,0,UNIT_CUSTOM);
	transport.setUnitType(1,0,UNIT_CUSTOM);
	transport.setLocalUnit(0,new PingPongUnit(&transport));
	
	//run
	transport.run();
	
	//finalize all
	transport.finalize();
	driver.finalize();
	launcher.finalize();
}
