######################################################
#            PROJECT  : lhcb-daqpipe
#            DATE     : 12/2017
#            AUTHOR   : Sébastien Valat - CERN
######################################################

######################################################
add_executable(TestFullLocal TestFullLocal.cpp ${DAQPIPE_SUB_LIBS})
target_link_libraries(TestFullLocal ${DAQPIPE_EXTERN_LIBS})
#add_test(NAME TestPingPong-MSG COMMAND ${HYDRA_MPIEXEC} -np 2 ${CMAKE_CURRENT_BINARY_DIR}/TestPingPong MSG)
add_test(TestFullLocal TestFullLocal)

######################################################
add_executable(TestPingPongCmdUnit TestPingPongCmdUnit.cpp ${DAQPIPE_SUB_LIBS})
target_link_libraries(TestPingPongCmdUnit ${DAQPIPE_EXTERN_LIBS})
#add_test(NAME TestPingPong-MSG COMMAND ${HYDRA_MPIEXEC} -np 2 ${CMAKE_CURRENT_BINARY_DIR}/TestPingPong MSG)

######################################################
add_executable(TestPingPongRDMAUnit TestPingPongRDMAUnit.cpp ${DAQPIPE_SUB_LIBS})
target_link_libraries(TestPingPongRDMAUnit ${DAQPIPE_EXTERN_LIBS})
#add_test(NAME TestPingPong-MSG COMMAND ${HYDRA_MPIEXEC} -np 2 ${CMAKE_CURRENT_BINARY_DIR}/TestPingPong MSG)

######################################################
add_executable(TestPingPongCmdMany TestPingPongCmdMany.cpp ${DAQPIPE_SUB_LIBS})
target_link_libraries(TestPingPongCmdMany ${DAQPIPE_EXTERN_LIBS})
#add_test(NAME TestPingPong-MSG COMMAND ${HYDRA_MPIEXEC} -np 2 ${CMAKE_CURRENT_BINARY_DIR}/TestPingPong MSG)
