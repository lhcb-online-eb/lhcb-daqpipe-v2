/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include <units/Unit.hpp>
#include <Layout.hpp>

/*******************  NAMESPACE  ********************/
using namespace DAQ;

/*********************  CLASS  **********************/
class PingPongUnit : public Unit
{
	public:
		PingPongUnit ( DAQ::Transport* transport );
		virtual void main ();
		void sendNext();
	private:
		Transport * transport;
		int id;
};

/*******************  FUNCTION  *********************/
PingPongUnit::PingPongUnit ( Transport* transport ) : Unit("PingPong")
{
	assume(transport->getWorldSize() == 2,"Work only between 2 process");
	this->transport = transport;
	this->id = 0;
}

/*******************  FUNCTION  *********************/
void PingPongUnit::main ()
{
	UnitCommand command;
	while(command << commandChannel) {
		switch(command.type) {
			case UNIT_CMD_START:
				if (this->transport->getRank() == 0)
					this->sendNext();
				break;
			case UNIT_PING_PONG:
				DAQ_INFO_ARG("Recv ping %1").arg(command.args.pingPong.id).end();
				assumeArg(command.args.pingPong.id == id,"Invalid ID in ping pong of commands, get %1, expect %2")
					.arg(command.args.pingPong.id)
					.arg(id)
					.end();
				id++;
				this->sendNext();
				break;
			default:
				DAQ_FATAL_ARG("Get unexpected command in ReadoutUnit : %1").arg(command.type).end();
				break;
		}
	}
}

/*******************  FUNCTION  *********************/
void PingPongUnit::sendNext()
{
	UnitCommand command;
	
	//exit or send
	if (id >= 10000 && transport->getRank() == 0) {
		command.type = DAQ::UNIT_CMD_EXIT;
		transport->sendCommand(this,1,0,command);
		transport->sendCommand(this,0,0,command);
	} else {
		command.type = UNIT_PING_PONG;
		command.args.pingPong.id = id++;
		DAQ_INFO_ARG("Send ping %1").arg(command.args.pingPong.id).end();
		transport->sendCommand(this,(transport->getRank() + 1) % 2,0,command);
	}
}

/*******************  FUNCTION  *********************/
int main(int argc,char ** argv)
{
	//config
	Config config;
	config.parseArgs(argc,(const char**)argv);
	
	//launcher
	Layout::Launcher launcher(&config);
	launcher.initialize(argc,argv);
	Debug::setRank(launcher.getRank());
	
	//transport
	//setup Driver
	Layout::Driver driver;
	driver.initialize(config, launcher,argc,argv);
	
	//setup transport
	Layout::Transport transport(&config,&launcher,&driver);
	transport.initialize(config, launcher);
	
	//setup units
	assume(transport.getWorldSize() == 2,"Work ongly between 2 processes");
	transport.setUnitType(0,0,UNIT_CUSTOM);
	transport.setUnitType(1,0,UNIT_CUSTOM);
	transport.setLocalUnit(0,new PingPongUnit(&transport));
	
	//run
	transport.run();
	
	//finalize all
	transport.finalize();
	driver.finalize();
	launcher.finalize();
}
