/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include <units/Unit.hpp>
#include <units/BuilderUnit.hpp>
#include "common/Debug.hpp"
#include "Gather.hpp"
#include "units/Command.hpp"

#include <sys/types.h>
#include <signal.h>
#include <unistd.h>

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*******************  FUNCTION  *********************/
/**
 * Gather constructor
 * @param config Give access to the golbal config struct.
 * @param unit parent (BU) unit to be used when sending data to transport layer.
 * @param transport Transport layer to be used to send messages to RU and ack to BU.
 * @param creditId The creditId it manage.
 * @param meta Pointer to the meta data buffers.
 * @param data Pointer to the data buffers.
**/
Gather::Gather ( const DAQ::Config* config, DAQ::Unit* unit, DAQ::Transport* transport, int creditId, DAQ::BuilderUnitData* meta, DAQ::BuilderUnitData* data )
       :state(transport,config,2*DAQ_DATA_SOURCE_RAILS,UNIT_READOUT_UNIT)
{
	this->config = config;
	this->unit = unit;
	this->transport = transport;
	this->creditId = creditId;
	this->meta = meta;
	this->data = data;
	this->inUse = false;
	this->dataChecking = config->dataChecking;
	this->timer.start();
	this->push = false;
	this->eventId = -1;
	this->hasFailure = false;
}

/*******************  FUNCTION  *********************/
/**
 * Make progress by validating an exchange.
 * @param rank The rank which has finished.
 * @param id The unit ID which has finished.
 * @param rail The rail which has beed recieved.
 * @param target The target which has been recieved.
**/
bool Gather::markProgress ( int rank, int id,int rail, GatherTarget target )
{
	return this->markProgress(buildUnitId(rank,id),rail,target);
}

/*******************  FUNCTION  *********************/
/**
 * Make progress by validating an exchange.
 * @param unitId The unit ID which has finished.
 * @param rail The rail which has beed recieved.
 * @param target The target which has been recieved.
**/
bool Gather::markProgress ( const UnitId & unitId, int rail,GatherTarget target )
{
	//id
	int id = ressourceId(rail,target);
	
	//trivial
	if (transport->isDead(unitId.rank))
	{
		DAQ_DEBUG_ARG("failure","Skip make progress for rank %1").arg(unitId.rank).end();
		assume(state.get(unitId,id) == true,"Invalid status of dead segment !");
		return false;
	}
	
	if (inUse == false)
		return false;
	
	//do it
	DAQ_DEBUG_ARG("gather","Gather %1 : progress %2:%3").arg(this).arg(unitId).arg(target).end();
	assert(inUse);
	assert(rail >= 0 && rail < DAQ_DATA_SOURCE_RAILS);
	assume(state.get(unitId,id) == false,"Get 2 times the same segment !");
	state.set(unitId,id,true);
	if (state.hasAllRessources() && hasFailure)
		DAQ_WARNING("Flush incomplete event");
	return state.hasAllRessources();
}

/*******************  FUNCTION  *********************/
/**
 * Handle a node failure so we can mark the event as incomplete if not all parts have been received from
 * this node.
**/
void Gather::markNodeFailure ( int rank )
{
	DAQ_DEBUG_ARG("failure","Mark gather as failed for rank %1").arg(rank).end();
	for (int unitId = 0 ; unitId < DAQ_MAX_UNITS_PER_PROCESS ; unitId++)
	{
		if (transport->getUnitType(rank,unitId) == UNIT_READOUT_UNIT)
		{
			UnitId uid(rank,unitId);
			for (int rail = 0 ; rail < DAQ_DATA_SOURCE_RAILS ; rail++)
			{
				int id = ressourceId(rail,TARGET_DATA);
				if (state.get(uid,id) == false)
					state.set(uid,id,true);
				
				id = ressourceId(rail,TARGET_META);
				if (state.get(uid,id) == false)
					state.set(uid,id,true);
			}
		}
	}
	
	//ok make it simpler for first shoot, we make all as incomplete
	this->hasFailure = true;
	
	//TODO need improvement in code
	if(state.hasAllRessources() && inUse)
		doGatherFinish();
}

/*******************  FUNCTION  *********************/
/**
 * Just check timeout and print message if it append.
 * @todo Take some actions.
**/
void Gather::checkTimeout ()
{
	if (timer.getElapsedTime() > 4)
		DAQ_WARNING("Timeout on some credit pull gather");
}

/*******************  FUNCTION  *********************/
/**
 * Reset the state of the gather tracker to start a new run
**/
void Gather::reset ()
{
	DAQ_DEBUG("failure","Reset");
	assume(inUse == false,"Try to use a gather manager which is already in use !");
	this->inUse = true;
	this->state.reset(false);
	
	//manage dead nodes
	for (int rank = 0 ; rank < transport->getWorldSize() ; rank++)
	{
		if (transport->isDead(rank))
		{
			DAQ_DEBUG_ARG("failure","Skip rank %1 in gather").arg(rank).end();
			for (int unitId = 0 ; unitId < DAQ_MAX_UNITS_PER_PROCESS ; unitId++)
			{
				if (transport->getUnitType(rank,unitId) == UNIT_READOUT_UNIT)
				{
					UnitId uid(rank,unitId);
					for (int rail = 0 ; rail < DAQ_DATA_SOURCE_RAILS ; rail++)
					{
						int id = ressourceId(rail,TARGET_DATA);
						state.set(uid,id,true);
						id = ressourceId(rail,TARGET_META);
						state.set(uid,id,true);
					}
				}
			}
		}
	}
	
	timer.stop();
	timer.start();

}

/*******************  FUNCTION  *********************/
/**
 * Compute a uniq ID to identify communications from the given parameters.
 * @param targetRank The remote rank to communicate with.
 * @param targetUnitId The remote unit id to communicate with.
 * @param rail The rail to fetch.
 * @param targetVar the target to fetch.
**/
int Gather::getUUID(int targetRank, int targetUnitId, int rail, GatherTarget targetVar)
{
	return getUUID(this->creditId,transport->getWorldSize(),targetRank,targetUnitId,rail,targetVar);
}

/*******************  FUNCTION  *********************/
int Gather::getUUID ( int creditId,int worldSize, int targetRank, int targetUnitId, int rail , GatherTarget targetVar )
{
	int uuid = creditId * (worldSize * DAQ_MAX_UNITS_PER_PROCESS * 2 * DAQ_DATA_SOURCE_RAILS)
	     + targetRank * (DAQ_MAX_UNITS_PER_PROCESS * 2 * DAQ_DATA_SOURCE_RAILS)
	     + targetUnitId * 2 * DAQ_DATA_SOURCE_RAILS
	     + rail * 2
	     + targetVar;
	assert(uuid >= 0);
	return uuid;
}

/*******************  FUNCTION  *********************/
/**
 * Used for sanity check in debug mode.
**/
void Gather::checkStatus()
{
	//loop on all units
	for (int rank = 0 ; rank < transport->getWorldSize() ; rank++) {
		for (int unitId = 0 ; unitId < DAQ_MAX_UNITS_PER_PROCESS ; unitId++) {
			if (transport->getUnitType(rank,unitId) == UNIT_READOUT_UNIT) {
				for (int rail = 0 ; rail < DAQ_DATA_SOURCE_RAILS ; rail++) {
					assert(state.get(rank,unitId,ressourceId(rail,TARGET_DATA)) == true);
					assert(state.get(rank,unitId,ressourceId(rail,TARGET_META)) == true);
				}
			}
		}
	}
}

/*******************  FUNCTION  *********************/
/**
 * Compute a ressource uniq ID from rail and target.
**/
int Gather::ressourceId ( int rail, GatherTarget target )
{
	return target * DAQ_DATA_SOURCE_RAILS + rail;
}

/*******************  FUNCTION  *********************/
/**
 * Run the data checking if requested to validate the received data.
 * It might hurt the performance so run it only in debug mode.
**/
void Gather::doDataChecking ()
{
	BucketInfo bucket;
	int units = transport->countUnits(UNIT_READOUT_UNIT);
	
	//nothing to do
	if (hasFailure)
		return;
	
	for (int unit = 0 ; unit < units ; unit++) {
		bucket.eventId = this->eventId;
		bucket.id = 0;
		for (int rail = 0 ; rail < DAQ_DATA_SOURCE_RAILS ; rail++) {
			bucket.dataFragments[rail].base = data->getCreditPtr(creditId,rail,unit);
			bucket.dataFragments[rail].size = 0;
			bucket.metaFragments[rail].base = meta->getCreditPtr(creditId,rail,unit);
			bucket.metaFragments[rail].size = 0;
		}
		DAQ_DEBUG_ARG("datasource"," Gather::doDataChecking() unitId %1").arg(unit).end();
		bucket.check(*config,unit);
	}
}

/*******************  FUNCTION  *********************/
void Gather::doGatherFinish ()
{
	DAQ_DEBUG_ARG("gather","Finish gather %1").arg(this).end();
	//mark unused
	DAQ_DEBUG("failure","Mark non used");
	this->inUse = false;
	
	//data checking
	if (this->dataChecking) {
		checkStatus();
		doDataChecking();
	}
	
	//setup command
	UnitCommand sendCommand;
	if (push)
		sendCommand.type = UNIT_CMD_PUSH_GATHER_FINISHED;
	else
		sendCommand.type = UNIT_CMD_PULL_GATHER_FINISHED;
	sendCommand.args.gatherAck.creditId = creditId;
	sendCommand.args.gatherAck.eventId = eventId;
	sendCommand.args.gatherAck.rail = 0;
	sendCommand.args.gatherAck.target = TARGET_DATA;
	sendCommand.args.gatherAck.uuid = 0;
	
	DAQ_DEBUG_ARG("tcp","eventId %1 (from Gather::onChunkFinished)").arg(sendCommand.args.pullGather.eventId).end();
	
	//push it
	unit->pushCommand(sendCommand);
}

/*******************  FUNCTION  *********************/
/**
 * Handle the recieved command which notify ending of an RDMA operation.
 * @param command The command ack to handle.
**/
bool Gather::onChunkFinished ( const UnitCommand& command )
{
	//trivial
	if (transport->isDead(command.src.rank))
	{
		DAQ_DEBUG_ARG("failure","Skip on chunk finish for rank %1").arg(command.src.rank).end();
		return false;
	}
	
	//check
	assert(command.type == UNIT_CMD_PULL_WRITE_FINISHED || command.type == UNIT_CMD_PUSH_WRITE_FINISHED);
	assumeArg(eventId == command.args.gatherAck.eventId,"Get invalid EventId = %1, expect %2").arg(command.args.gatherAck.eventId).arg(eventId).end();
	const ArgGatherAck & args = command.args.gatherAck;
	
	DAQ_DEBUG_ARG("gather","Get gather ack %1:%2:%3 for eventId %4").arg(this).arg(command.src).arg(args.target).arg(args.eventId).end();
	
	//progress and if finished, send gather finished
	if (markProgress(command.src,args.rail,args.target)) {
		doGatherFinish();
		return true;
	} else {
		return false;
	}
}

/*******************  FUNCTION  *********************/
#ifdef HAVE_HTOPML
	/**
	 * Used to convert the Gather object into JSON format to be used by Htopml rendering.
	**/
	void convertToJson ( htopml::JsonState& json, const Gather& gather )
	{
		json.openStruct();
			json.printField("creditId",gather.creditId);
			json.printField("state",gather.state);
			json.printField("inUse",gather.inUse);
			json.printField("eventId",gather.eventId);
		json.closeStruct();
	}
#endif //HAVE_HTOPML

}
