/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_FILTER_FETCHER_HPP
#define DAQ_FILTER_FETCHER_HPP

/********************  HEADERS  *********************/
#include "transport/Transport.hpp"
#include "units/Unit.hpp"
#include <list>

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/********************  STRUCT  **********************/
struct FilterFetcherStep
{
	int unitId;
	int rail;
	GatherTarget target;
};

/*********************  TYPES  **********************/
typedef std::list<FilterFetcherStep> FilterFetcherStepList;

/*********************  TYPES  **********************/
struct BuilderUnitData;

/*********************  CLASS  **********************/
class FilterFetcher
{
	public:
		FilterFetcher(Transport * transport, Unit * unit,const Config * config,BuilderUnitData * data,BuilderUnitData * meta,int units,int creditId);
		void start( const DAQ::UnitId& unitId, int buCredit, long eventId );
		bool stepFinish(uint32_t uuid);
	protected:
		void runStep(FilterFetcherStep & step);
		void runNextStep();
		uint32_t getUuid(FilterFetcherStep & step);
		bool finished();
		void dataChecking();
	private:
		FilterFetcherStepList steps;
		FilterFetcherStepList running;
		Transport * transport;
		Unit * unit;
		BuilderUnitData * data;
		BuilderUnitData * meta;
		int fuCreditId;
		int units;
		int cur;
		int parallel;
		UnitId builderUnitId;
		int buCredit;
		long eventId;
		const Config * config;
};

}

#endif //DAQ_FILTER_FETCHER_HPP
