/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "PullGatherSched.hpp"
#include "transport/tests/MockTransport.hpp"
#include "units/tests/MockUnit.hpp"
#include "drivers/tests/MockDriver.hpp"
#include "launcher/LauncherLocal.hpp"
#include "units/BuilderUnit.hpp"

/***************** USING NAMESPACE ******************/
using namespace DAQ;
using namespace testing;

/*******************  FUNCTION  *********************/
TEST(PullGatherSched,constructor)
{
	Config config;
	LauncherLocal launcher(&config);
	MockDriver driver;
	
	EXPECT_CALL(driver,registerSegment(_,_)).Times(8);
	EXPECT_CALL(driver,unregisterSegment(_,_)).Times(8);
	
	MockTransport transport("mock",&config,&launcher,&driver);
	MockUnit * unit = new MockUnit();
	transport.setUnitType(0,0,DAQ::UNIT_CUSTOM);
	transport.setLocalUnit(0,unit);
	BuilderUnitData meta(1024,2,2,1,&transport);
	BuilderUnitData data(102400,2,2,1,&transport);
	
	PullGatherSched gather(&config,unit,&transport,0,&meta,&data);
}

/*******************  FUNCTION  *********************/
TEST(PullGatherSched,startSimple)
{
	Config config;
	config.getAndSetConfigString("gather.sched","policy","simple");
	LauncherLocal launcher(&config);
	MockDriver driver;
	
	EXPECT_CALL(driver,registerSegment(_,_)).Times(8);
	EXPECT_CALL(driver,unregisterSegment(_,_)).Times(8);
	
	MockTransport transport("mock",&config,&launcher,&driver);
	MockUnit * unit = new MockUnit();
	transport.setUnitType(0,0,DAQ::UNIT_READOUT_UNIT);
	transport.setLocalUnit(0,unit);
	transport.setUnitType(0,1,DAQ::UNIT_READOUT_UNIT);
	transport.setLocalUnit(2,new MockUnit());
	transport.setUnitType(0,2,DAQ::UNIT_READOUT_UNIT);
	transport.setLocalUnit(3,new MockUnit());
	transport.setUnitType(0,3,DAQ::UNIT_READOUT_UNIT);
	transport.setLocalUnit(1,new MockUnit());
	BuilderUnitData meta(1024,2,2,4,&transport);
	BuilderUnitData data(102400,2,2,4,&transport);
	
	PullGatherSched gather(&config,unit,&transport,0,&meta,&data);
	meta.assign(0);
	data.assign(0);
	
	RemoteIOV iov = {nullptr,0};
	EXPECT_CALL(transport,getRemoteIOV(::testing::_,::testing::_,::testing::_)).WillRepeatedly(::testing::Return(iov));
	EXPECT_CALL(transport,sendCommand(::testing::_,0,0,::testing::_)).Times(1);
	EXPECT_CALL(transport,sendCommand(::testing::_,0,1,::testing::_)).Times(1);
	EXPECT_CALL(transport,postWaitRemoteRDMAWrite(::testing::_,0,0,::testing::_,::testing::_,::testing::_,::testing::_)).Times(4);
	EXPECT_CALL(transport,postWaitRemoteRDMAWrite(::testing::_,0,1,::testing::_,::testing::_,::testing::_,::testing::_)).Times(4);
	
	gather.start(0);
	
	UnitCommand command;
	command.type = DAQ::UNIT_CMD_PULL_WRITE_FINISHED;
	ArgGatherAck & args = command.args.gatherAck;
	args.creditId = 0;
	args.eventId = 0;
	args.rail = 0;
	args.target = DAQ::TARGET_DATA;
	args.uuid = gather.getUUID(0,0,0,DAQ::TARGET_DATA);
	command.src.rank = 0;
	command.src.id = 0;
	
	
	EXPECT_CALL(transport,getRemoteIOV(::testing::_,::testing::_,::testing::_)).WillRepeatedly(::testing::Return(iov));
	//EXPECT_CALL(transport,sendCommand(::testing::_,0,2,::testing::_)).Times(1);
	//EXPECT_CALL(transport,postWaitRemoteRDMAWrite(::testing::_,0,2,::testing::_,::testing::_,::testing::_,::testing::_)).Times(4);
	
	gather.onChunkFinished(command);
}

/*******************  FUNCTION  *********************/
TEST(PullGatherSched,startBarrel)
{
	Config config;
	config.getAndSetConfigString("gather.sched","policy","barrel");
	LauncherLocal launcher(&config);
	MockDriver driver;
	
	EXPECT_CALL(driver,registerSegment(_,_)).Times(8);
	EXPECT_CALL(driver,unregisterSegment(_,_)).Times(8);
	
	MockTransport transport("mock",&config,&launcher,&driver);
	MockUnit * unit = new MockUnit();
	transport.setUnitType(0,0,DAQ::UNIT_READOUT_UNIT);
	transport.setLocalUnit(0,unit);
	transport.setUnitType(0,1,DAQ::UNIT_READOUT_UNIT);
	transport.setLocalUnit(2,new MockUnit());
	transport.setUnitType(0,2,DAQ::UNIT_READOUT_UNIT);
	transport.setLocalUnit(3,new MockUnit());
	transport.setUnitType(0,3,DAQ::UNIT_READOUT_UNIT);
	transport.setLocalUnit(1,new MockUnit());
	BuilderUnitData meta(1024,2,2,4,&transport);
	BuilderUnitData data(102400,2,2,4,&transport);
	meta.assign(0);
	data.assign(0);
	
	PullGatherSched gather(&config,unit,&transport,0,&meta,&data);
	
	RemoteIOV iov = {nullptr,0};
	EXPECT_CALL(transport,getRemoteIOV(::testing::_,::testing::_,::testing::_)).WillRepeatedly(::testing::Return(iov));
	EXPECT_CALL(transport,sendCommand(::testing::_,0,0,::testing::_)).Times(1);
	EXPECT_CALL(transport,sendCommand(::testing::_,0,1,::testing::_)).Times(1);
	EXPECT_CALL(transport,postWaitRemoteRDMAWrite(::testing::_,0,0,::testing::_,::testing::_,::testing::_,::testing::_)).Times(4);
	EXPECT_CALL(transport,postWaitRemoteRDMAWrite(::testing::_,0,1,::testing::_,::testing::_,::testing::_,::testing::_)).Times(4);
	
	gather.start(0);
}

/*******************  FUNCTION  *********************/
TEST(PullGatherSched,startRandom)
{
	Config config;
	config.getAndSetConfigString("gather.sched","policy","random");
	LauncherLocal launcher(&config);
	MockDriver driver;
	
	EXPECT_CALL(driver,registerSegment(_,_)).Times(8);
	EXPECT_CALL(driver,unregisterSegment(_,_)).Times(8);
	
	MockTransport transport("mock",&config,&launcher,&driver);
	MockUnit * unit = new MockUnit();
	transport.setUnitType(0,0,DAQ::UNIT_READOUT_UNIT);
	transport.setLocalUnit(0,unit);
	transport.setUnitType(0,1,DAQ::UNIT_READOUT_UNIT);
	transport.setLocalUnit(2,new MockUnit());
	transport.setUnitType(0,2,DAQ::UNIT_READOUT_UNIT);
	transport.setLocalUnit(3,new MockUnit());
	transport.setUnitType(0,3,DAQ::UNIT_READOUT_UNIT);
	transport.setLocalUnit(1,new MockUnit());
	BuilderUnitData meta(1024,2,2,4,&transport);
	BuilderUnitData data(102400,2,2,4,&transport);
	meta.assign(0);
	data.assign(0);
	
	PullGatherSched gather(&config,unit,&transport,0,&meta,&data);
	
	RemoteIOV iov = {nullptr,0};
	EXPECT_CALL(transport,getRemoteIOV(::testing::_,::testing::_,::testing::_)).WillRepeatedly(::testing::Return(iov));
	EXPECT_CALL(transport,sendCommand(::testing::_,::testing::_,::testing::_,::testing::_)).Times(2);
	EXPECT_CALL(transport,postWaitRemoteRDMAWrite(::testing::_,::testing::_,::testing::_,::testing::_,::testing::_,::testing::_,::testing::_)).Times(8);
	
	gather.start(0);	
}

/*******************  FUNCTION  *********************/
TEST(PullGatherSched,startRdBarrel)
{
	Config config;
	config.getAndSetConfigString("gather.sched","policy","rdbarrel");
	LauncherLocal launcher(&config);
	MockDriver driver;
	
	EXPECT_CALL(driver,registerSegment(_,_)).Times(8);
	EXPECT_CALL(driver,unregisterSegment(_,_)).Times(8);
	
	MockTransport transport("mock",&config,&launcher,&driver);
	MockUnit * unit = new MockUnit();
	transport.setUnitType(0,0,DAQ::UNIT_READOUT_UNIT);
	transport.setLocalUnit(0,unit);
	transport.setUnitType(0,1,DAQ::UNIT_READOUT_UNIT);
	transport.setLocalUnit(2,new MockUnit());
	transport.setUnitType(0,2,DAQ::UNIT_READOUT_UNIT);
	transport.setLocalUnit(3,new MockUnit());
	transport.setUnitType(0,3,DAQ::UNIT_READOUT_UNIT);
	transport.setLocalUnit(1,new MockUnit());
	BuilderUnitData meta(1024,2,2,4,&transport);
	BuilderUnitData data(102400,2,2,4,&transport);
	meta.assign(0);
	data.assign(0);
	
	PullGatherSched gather(&config,unit,&transport,0,&meta,&data);
	
	RemoteIOV iov = {nullptr,0};
	EXPECT_CALL(transport,getRemoteIOV(::testing::_,::testing::_,::testing::_)).WillRepeatedly(::testing::Return(iov));
	EXPECT_CALL(transport,sendCommand(::testing::_,::testing::_,::testing::_,::testing::_)).Times(2);
	EXPECT_CALL(transport,postWaitRemoteRDMAWrite(::testing::_,::testing::_,::testing::_,::testing::_,::testing::_,::testing::_,::testing::_)).Times(8);
	
	gather.start(0);		
}
