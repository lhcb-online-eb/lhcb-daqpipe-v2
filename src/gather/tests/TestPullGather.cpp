/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "PullGather.hpp"
#include "MockPullGather.hpp"
#include "transport/tests/MockTransport.hpp"
#include "units/tests/MockUnit.hpp"
#include "drivers/tests/MockDriver.hpp"
#include <launcher/LauncherLocal.hpp>
#include <units/BuilderUnit.hpp>

/***************** USING NAMESPACE ******************/
using namespace DAQ;

/*******************  FUNCTION  *********************/
TEST(PullGather,constructor)
{
	Config config;
	LauncherLocal launcher(&config);
	MockDriver driver;
	MockTransport transport("mock",&config,&launcher,&driver);
	MockUnit * unit = new MockUnit();
	transport.setUnitType(0,0,DAQ::UNIT_CUSTOM);
	transport.setLocalUnit(0,unit);
	BuilderUnitData meta(1024,2,2,1,&transport);
	BuilderUnitData data(102400,2,2,1,&transport);
	
	MockPullGather gather(&config,unit,&transport,0,&meta,&data);
}

/*******************  FUNCTION  *********************/
TEST(PullGather,reset)
{
	Config config;
	LauncherLocal launcher(&config);
	MockDriver driver;
	MockTransport transport("mock",&config,&launcher,&driver);
	MockUnit * unit = new MockUnit();
	transport.setUnitType(0,0,DAQ::UNIT_CUSTOM);
	transport.setLocalUnit(0,unit);
	BuilderUnitData meta(1024,2,2,1,&transport);
	BuilderUnitData data(102400,2,2,1,&transport);
	
	MockPullGather gather(&config,unit,&transport,0,&meta,&data);
	EXPECT_FALSE(gather.isInUse());
	gather.reset();
	EXPECT_TRUE(gather.isInUse());
	gather.checkTimeout();
}

/*******************  FUNCTION  *********************/
TEST(PullGather,markProgress)
{
	Config config;
	LauncherLocal launcher(&config);
	MockDriver driver;
	MockTransport transport("mock",&config,&launcher,&driver);
	MockUnit * unit = new MockUnit();
	transport.setUnitType(0,0,DAQ::UNIT_READOUT_UNIT);
	transport.setLocalUnit(0,unit);
	transport.setUnitType(0,1,DAQ::UNIT_READOUT_UNIT);
	transport.setLocalUnit(1,new MockUnit());
	BuilderUnitData meta(1024,2,2,1,&transport);
	BuilderUnitData data(102400,2,2,1,&transport);
	
	MockPullGather gather(&config,unit,&transport,0,&meta,&data);
	EXPECT_FALSE(gather.isInUse());
	gather.reset();
	EXPECT_TRUE(gather.isInUse());
	
	//node 0
	EXPECT_FALSE(gather.markProgress(0,0,0,DAQ::TARGET_DATA));
	EXPECT_FALSE(gather.markProgress(0,0,1,DAQ::TARGET_DATA));
	EXPECT_FALSE(gather.markProgress(0,0,0,DAQ::TARGET_META));
	EXPECT_FALSE(gather.markProgress(0,0,1,DAQ::TARGET_META));
	
	//node 1
	EXPECT_FALSE(gather.markProgress(0,1,0,DAQ::TARGET_DATA));
	EXPECT_FALSE(gather.markProgress(0,1,1,DAQ::TARGET_DATA));
	EXPECT_FALSE(gather.markProgress(0,1,0,DAQ::TARGET_META));
	EXPECT_TRUE(gather.markProgress(0,1,1,DAQ::TARGET_META));
}

/*******************  FUNCTION  *********************/
TEST(PullGather,markProgressErr)
{
	Config config;
	LauncherLocal launcher(&config);
	MockDriver driver;
	MockTransport transport("mock",&config,&launcher,&driver);
	MockUnit * unit = new MockUnit();
	transport.setUnitType(0,0,DAQ::UNIT_READOUT_UNIT);
	transport.setLocalUnit(0,unit);
	transport.setUnitType(0,1,DAQ::UNIT_READOUT_UNIT);
	transport.setLocalUnit(1,new MockUnit());
	BuilderUnitData meta(1024,2,2,1,&transport);
	BuilderUnitData data(102400,2,2,1,&transport);
	
	MockPullGather gather(&config,unit,&transport,0,&meta,&data);
	EXPECT_FALSE(gather.isInUse());
	gather.reset();
	EXPECT_TRUE(gather.isInUse());
	
	//node 0
	EXPECT_FALSE(gather.markProgress(0,0,0,DAQ::TARGET_DATA));
	ASSERT_DEATH(gather.markProgress(0,0,0,DAQ::TARGET_DATA),"");
}

/*******************  FUNCTION  *********************/
TEST(PullGather,getUUID)
{
	Config config;
	LauncherLocal launcher(&config);
	MockDriver driver;
	MockTransport transport("mock",&config,&launcher,&driver);
	MockUnit * unit = new MockUnit();
	transport.setUnitType(0,0,DAQ::UNIT_CUSTOM);
	transport.setLocalUnit(0,unit);
	BuilderUnitData meta(1024,2,2,1,&transport);
	BuilderUnitData data(102400,2,2,1,&transport);
	
	MockPullGather gather(&config,unit,&transport,0,&meta,&data);
	
	int uuid1 = gather.getUUID(0,0,0,DAQ::TARGET_DATA);
	int uuid2 = gather.getUUID(0,0,0,DAQ::TARGET_DATA);
	int uuid3 = gather.getUUID(0,1,0,DAQ::TARGET_DATA);
	int uuid4 = gather.getUUID(1,0,0,DAQ::TARGET_DATA);
	
	EXPECT_EQ(uuid1,uuid2);
	EXPECT_NE(uuid3,uuid2);
	EXPECT_NE(uuid4,uuid2);
}

/*******************  FUNCTION  *********************/
TEST(PullGather,onChunkFinishedNotOk)
{
	Config config;
	LauncherLocal launcher(&config);
	MockDriver driver;
	MockTransport transport("mock",&config,&launcher,&driver);
	MockUnit * unit = new MockUnit();
	transport.setUnitType(0,0,DAQ::UNIT_READOUT_UNIT);
	transport.setLocalUnit(0,unit);
	transport.setUnitType(0,1,DAQ::UNIT_READOUT_UNIT);
	transport.setLocalUnit(1,new MockUnit());
	BuilderUnitData meta(1024,2,2,1,&transport);
	BuilderUnitData data(102400,2,2,1,&transport);
	
	MockPullGather gather(&config,unit,&transport,0,&meta,&data);
	EXPECT_FALSE(gather.isInUse());
	gather.reset();
	EXPECT_TRUE(gather.isInUse());
	
	UnitCommand command;
	command.type = DAQ::UNIT_CMD_PULL_WRITE_FINISHED;
	ArgGatherAck & args = command.args.gatherAck;
	args.creditId = 0;
	args.eventId = 0;
	args.rail = 0;
	args.target = DAQ::TARGET_DATA;
	args.uuid = gather.getUUID(0,0,0,DAQ::TARGET_DATA);
	command.src.rank = 0;
	command.src.id = 0;
	
	EXPECT_FALSE(gather.onChunkFinished(command));
	
	delete unit;
}

/*******************  FUNCTION  *********************/
TEST(PullGather,onChunkFinishedOk)
{
	Config config;
	LauncherLocal launcher(&config);
	MockDriver driver;
	MockTransport transport("mock",&config,&launcher,&driver);
	MockUnit * unit = new MockUnit();
	transport.setUnitType(0,0,DAQ::UNIT_READOUT_UNIT);
	transport.setLocalUnit(0,unit);
	transport.setUnitType(0,1,DAQ::UNIT_READOUT_UNIT);
	transport.setLocalUnit(1,new MockUnit());
	BuilderUnitData meta(1024,2,2,1,&transport);
	BuilderUnitData data(102400,2,2,1,&transport);
	
	MockPullGather gather(&config,unit,&transport,0,&meta,&data);
	EXPECT_FALSE(gather.isInUse());
	gather.reset();
	EXPECT_TRUE(gather.isInUse());
	
	UnitCommand command;
	command.type = DAQ::UNIT_CMD_PULL_WRITE_FINISHED;
	ArgGatherAck & args = command.args.gatherAck;
	args.creditId = 0;
	args.eventId = 0;
	args.rail = 0;
	args.target = DAQ::TARGET_DATA;
	args.uuid = gather.getUUID(0,0,0,DAQ::TARGET_DATA);
	command.src.rank = 0;
	command.src.id = 0;
	
	//node 0
	EXPECT_FALSE(gather.markProgress(0,0,1,DAQ::TARGET_DATA));
	EXPECT_FALSE(gather.markProgress(0,0,0,DAQ::TARGET_META));
	EXPECT_FALSE(gather.markProgress(0,0,1,DAQ::TARGET_META));
	
	//node 1
	EXPECT_FALSE(gather.markProgress(0,1,0,DAQ::TARGET_DATA));
	EXPECT_FALSE(gather.markProgress(0,1,1,DAQ::TARGET_DATA));
	EXPECT_FALSE(gather.markProgress(0,1,0,DAQ::TARGET_META));
	EXPECT_FALSE(gather.markProgress(0,1,1,DAQ::TARGET_META));
	
	EXPECT_CALL(*unit,pushCommand(::testing::_)).Times(1);
	EXPECT_TRUE(gather.onChunkFinished(command));
	
	delete unit;
}