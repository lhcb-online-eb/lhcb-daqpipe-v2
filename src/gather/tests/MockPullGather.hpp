/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_MOCK_PULL_GATHER_HPP
#define DAQ_MOCK_PULL_GATHER_HPP

/********************  HEADERS  *********************/
#include <gmock/gmock.h>
#include "PullGather.hpp"

/********************  NAMESPACE  *******************/
namespace DAQ {

class MockPullGather : public PullGather {
	public:
		MockPullGather(const Config* config, Unit* unit, Transport* transport, int creditId, BuilderUnitData* meta, BuilderUnitData* data)
			:PullGather(config,unit,transport,creditId,meta,data){eventId = 0;dataChecking = false;};
		MOCK_METHOD1(start,
			void(long eventId));
};

}  // namespace DAQ

#endif //DAQ_MOCK_PULL_GATHER_HPP
