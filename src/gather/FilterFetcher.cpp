/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include "FilterFetcher.hpp"
#include <units/BuilderUnit.hpp>

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*******************  FUNCTION  *********************/
FilterFetcher::FilterFetcher ( Transport* transport, Unit* unit, const Config* config,BuilderUnitData * data,BuilderUnitData * meta, int units, int creditId )
{
	this->transport = transport;
	this->unit = unit;
	this->fuCreditId = creditId;
	this->data = data;
	this->meta = meta;
	this->units = units;
	this->parallel = config->getAndSetConfigInt("filter","parallel",2);
	this->config = config;
	this->eventId = -1;
}

/*******************  FUNCTION  *********************/
void FilterFetcher::start ( const DAQ::UnitId& unitId, int buCredit, long int eventId )
{
	assert(running.empty());
	assert(steps.empty());
	
	this->builderUnitId = unitId;
	this->buCredit = buCredit;
	this->cur = 0;
	this->eventId = eventId;
	
	for (int rail = 0 ; rail < DAQ_DATA_SOURCE_RAILS ; rail++)
	{
		for (int unit = 0 ; unit < units ; unit++)
		{
			FilterFetcherStep step;
			step.rail = rail;
			step.unitId = unit;
			
			step.target = TARGET_DATA;
			steps.push_back(step);
			
			step.target = TARGET_META;
			steps.push_back(step);
		}
	}
	
	for (int i = 0 ; i < parallel ; i++)
		runNextStep();
}

/*******************  FUNCTION  *********************/
void FilterFetcher::runNextStep ()
{
	if (steps.size() > 0)
	{
		runStep(steps.front());
		running.push_back(steps.front());
		steps.pop_front();
	}
}

/*******************  FUNCTION  *********************/
uint32_t FilterFetcher::getUuid ( DAQ::FilterFetcherStep& step )
{
	assert(step.rail < DAQ_DATA_SOURCE_RAILS);
	assert(step.unitId < units);
	assert(step.target < 2);
	return step.rail + (step.target * DAQ_DATA_SOURCE_RAILS) + (step.unitId * DAQ_DATA_SOURCE_RAILS * 2);
}

/*******************  FUNCTION  *********************/
void FilterFetcher::runStep ( FilterFetcherStep & step )
{
	DAQ_DEBUG_ARG("FilterFetcher","Start step rail=%1, unit=%2, data=%3").arg(step.rail).arg(step.unitId).arg(step.target).end();
	
	UnitCommand command;
	command.type = UNIT_CMD_FILTER_FETCH_REQUEST;
	command.args.filterDataRequest.buStorageId = buCredit;
	command.args.filterDataRequest.fuCreditId = fuCreditId;
	command.args.filterDataRequest.rail = step.rail;
	command.args.filterDataRequest.target = step.target;
	command.args.filterDataRequest.unitId = step.unitId;
	command.args.filterDataRequest.uuid = getUuid(step);
	
	void * ptr = nullptr;
	size_t size = 0;
	switch(step.target)
	{
		case TARGET_DATA:
			ptr = data->getCreditPtr(fuCreditId,step.rail,step.unitId);
			size = data->getEventSize();
			break;
		case TARGET_META:
			ptr = meta->getCreditPtr(fuCreditId,step.rail,step.unitId);
			size = meta->getEventSize();
			break;
	}
	command.args.filterDataRequest.iov = transport->getRemoteIOV(builderUnitId.rank,ptr,size);
	
	//post notif
	UnitCommand postCommand;
	postCommand.type = UNIT_CMD_FILTER_FETCH_FINISHED;
	postCommand.args.filterDataRequest = command.args.filterDataRequest;
	DAQ_DEBUG_ARG("FilterFetcher","Expect recv data on addr = %1, size = %2, uuid = %3, type = %4").arg(ptr).arg(size).arg(command.args.filterDataRequest.uuid).arg(step.target).end();
	transport->postWaitRemoteRDMAWrite(unit,builderUnitId.rank,builderUnitId.id,ptr,size,command.args.filterDataRequest.uuid,postCommand);
	
	//send request
	transport->sendCommand(unit,builderUnitId,command);
}

/*******************  FUNCTION  *********************/
bool FilterFetcher::stepFinish ( uint32_t uuid )
{
	FilterFetcherStepList::iterator it = running.begin(); 
	while (it != running.end())
	{
		if (getUuid(*it) == uuid)
			break;
		++it;
	}
	
	assumeArg(it != running.end(),"Fail to find uuid = %1 in running list !").arg(uuid).end();
	running.erase(it);
	
	if (running.empty()) {
		if (config->dataChecking)
			dataChecking();
		return true;
	} else {
		runNextStep();
		return false;
	}
}

/*******************  FUNCTION  *********************/
void FilterFetcher::dataChecking ()
{
	BucketInfo bucket;
	int units = transport->countUnits(UNIT_READOUT_UNIT);
	
	//nothing to do
	//if (hasFailure)
	//	return;
	
	for (int unit = 0 ; unit < units ; unit++) {
		bucket.eventId = this->eventId;
		bucket.id = 0;
		for (int rail = 0 ; rail < DAQ_DATA_SOURCE_RAILS ; rail++) {
			bucket.dataFragments[rail].base = data->getCreditPtr(fuCreditId,rail,unit);
			bucket.dataFragments[rail].size = 0;
			bucket.metaFragments[rail].base = meta->getCreditPtr(fuCreditId,rail,unit);
			bucket.metaFragments[rail].size = 0;
		}
		bucket.check(*config,unit);
	}
}


}
