/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_PULL_GATHER_HPP
#define DAQ_PULL_GATHER_HPP

/********************  HEADERS  *********************/
#include "Gather.hpp"

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*********************  CLASS  **********************/
/**
 * Class used to handle the tracking of gather requestes. It is affected to one specific creditId
 * and instantiated into the builder units.
 * @brief Keep track of gather status in builder units
**/
class PullGather : public Gather
{
	public:
		PullGather(const Config * config,Unit * unit,Transport * transport,int creditId,BuilderUnitData * meta,BuilderUnitData * data);
	protected:
		void postRequests( int rank, int id, long int eventId );
};

}

#endif //DAQ_PULL_GATHER_HPP
