/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_GATHER_HPP
#define DAQ_GATHER_HPP

/********************  HEADERS  *********************/
#include <cstdlib>
#include "transport/Transport.hpp"
#include "common/TopoRessource.hpp"
#include "units/Unit.hpp"
#ifdef HAVE_HTOPML
	#include <htopml/TypeToJson.h>
#endif //HAVE_HTOPML

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*********************  TYPES  **********************/
struct BuilderUnitData;

/*********************  CLASS  **********************/
/**
 * Class used to handle the tracking of gather requestes. It is affected to one specific creditId
 * and instantiated into the builder units.
 * @brief Keep track of gather status in builder units
**/
class Gather
{
	public:
		Gather(const Config * config,Unit * unit,Transport * transport,int creditId,BuilderUnitData * meta,BuilderUnitData * data);
		virtual ~Gather() = default;
		void reset();
		bool markProgress(const UnitId & unitId,int rail, GatherTarget target);
		bool markProgress(int rank,int id, int rail, GatherTarget target);
		virtual void start(long eventId) = 0;
		virtual bool onChunkFinished( const DAQ::UnitCommand& command );
		virtual void markNodeFailure(int rank);
		bool isInUse() {return inUse;};
		int getUUID( int targetRank, int targetUnitId, int rail, DAQ::GatherTarget targetVar );
		static int getUUID( int creditId,int worldSize,int targetRank, int targetUnitId, int rail, DAQ::GatherTarget targetVar );
		void checkTimeout();
		#ifdef HAVE_HTOPML
			friend void convertToJson(htopml::JsonState & json,const Gather & gather);
		#endif //HAVE_HTOPML
	protected:
		int ressourceId(int rail,GatherTarget target);
		void doDataChecking();
		void checkStatus();
		void doGatherFinish();
	protected:
		/** Keep track of the config struct **/
		const Config * config;
		/** Keep ref to parent unit to exchange with transport layer and send back ack to parent BU.**/
		Unit * unit;
		/** Keep track of transport layer to send requests to RU and ack to parent BU **/
		Transport * transport;
		/** Remind the associated creditId it manage. **/
		int creditId;
		/** ref to the BU meta data buffers. **/
		BuilderUnitData * meta;
		/** ref to the BU data buffers. **/
		BuilderUnitData * data;
		/** Keep track of the progress status of the gather ops**/
		TopoRessource<bool> state;
		/** 
		 * Set to true when on going gather are running. False when it is finished and unused 
		 *(ready for the next one) 
		**/
		bool inUse;
		/** Keep track of the event ID we are fetching **/
		uint64_t eventId;
		/** From config file, true if need to run data checking when a gather finished. **/
		bool dataChecking;
		/** Help detection of timeout. **/
		Timer timer;
		/** say if it is pull or push **/
		bool push;
		/** Remember if the current gather as some failure so if event will be incomplete **/
		bool hasFailure;
};

}

#endif //DAQ_GATHER_HPP
