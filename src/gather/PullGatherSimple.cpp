/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include "PullGatherSimple.hpp"
#include "units/Unit.hpp"
#include "units/BuilderUnit.hpp"
#include "common/Debug.hpp"

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*******************  FUNCTION  *********************/
PullGatherSimple::PullGatherSimple ( const DAQ::Config* config, DAQ::Unit* unit, DAQ::Transport* transport, int creditId, DAQ::BuilderUnitData* meta, DAQ::BuilderUnitData* data ) 
                 :PullGather ( config, unit, transport, creditId, meta, data )
{
	this->eventId = -1;
}

/*******************  FUNCTION  *********************/
/**
 * Send all the requests in one Go.
**/
void PullGatherSimple::start ( long int eventId )
{
	//setup event id
	this->eventId = eventId;
	
	//check state
	assume(inUse == false,"Try to use a gather manager which is already in use !");
	
	//reset state
	this->reset();
	
	//loop on all units
	for (int rank = 0 ; rank < transport->getWorldSize() ; rank++) {
		for (int unitId = 0 ; unitId < DAQ_MAX_UNITS_PER_PROCESS ; unitId++) {
			if (transport->getUnitType(rank,unitId) == UNIT_READOUT_UNIT) {
				if (transport->isDead(rank) == false)
					this->postRequests(rank,unitId,eventId);
			}
		}
	}
}

}
