/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_GATHER_SCHED_HPP
#define DAQ_GATHER_SCHED_HPP

/********************  HEADERS  *********************/
#include <queue>
#include "PullGather.hpp"


/*******************  NAMESPACE  ********************/
namespace DAQ
{

/********************  STRUCT  **********************/
struct PullGatherSchedTask
{
	int rank;
	int unitId;
};

/********************  ENUM  ************************/
enum PullGatherSchedPolicy
{
	SCHED_POLICY_SIMPLE,
	SCHED_POLICY_BARREL,
	SCHED_POLICY_RANDOM,
	SCHED_POLICY_RDBARREL,
	SCHED_POLICY_XOR
};

/*********************  TYPES  **********************/
typedef GoChannel<PullGatherSchedTask> PullGatherSchedTaskQueue;

/*********************  CLASS  **********************/
class PullGatherSched : public PullGather
{
	public:
		PullGatherSched ( const DAQ::Config* config, DAQ::Unit* unit, DAQ::Transport* transport, int creditId, DAQ::BuilderUnitData* meta, DAQ::BuilderUnitData* data );
		virtual ~PullGatherSched();
		void start(long eventId);
		bool onChunkFinished(const UnitCommand & command);
		bool doNextTask();
	private:
		void startSimple(long eventId);
		void startBarrel(long eventId);
		void startRdBarrel(long eventId);
		void startRandom(long eventId);
		void startXor(long eventId);
		void threadMain();
	private:
		int eventId;
		int parallel;
		PullGatherSchedPolicy policy;
		PullGatherSchedTaskQueue tasks;
		std::thread thread;
		bool runThread;
};

}

#endif //DAQ_GATHER_SCHED_HPP
