/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include "PushGather.hpp"
#include <units/BuilderUnit.hpp>

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*********************  CLASS  **********************/
PushGather::PushGather(const Config* config, Unit* unit, Transport* transport, int creditId, BuilderUnitData* meta, BuilderUnitData* data)
	: Gather(config, unit, transport, creditId, meta, data)
{
	this->push = true;
}

/*******************  FUNCTION  *********************/
void PushGather::start(long int eventId)
{
	//setup event id
	this->eventId = eventId;
	
	//check state
	assume(inUse == false,"Try to use a gather manager which is already in use !");
	
	//reset state
	this->reset();
	
	//loop on all units
	postRdmaRecvRequest();
}

/*******************  FUNCTION  *********************/
void PushGather::postRdmaRecvRequest()
{
	for (int rail = 0 ; rail < DAQ_DATA_SOURCE_RAILS ; rail++) {
		for (int rank = 0 ; rank < transport->getWorldSize() ; rank++) {
			for (int unitId = 0 ; unitId < DAQ_MAX_UNITS_PER_PROCESS ; unitId++) {
				if (transport->getUnitType(rank,unitId) == UNIT_READOUT_UNIT) {
					//setup post command for notification of the end of write action
					UnitCommand postCommand;
					postCommand.type = UNIT_CMD_PUSH_WRITE_FINISHED;
					postCommand.args.gatherAck.creditId = creditId;
					postCommand.args.gatherAck.rail = rail;
					postCommand.args.gatherAck.eventId = this->eventId;
					postCommand.src.id = unitId;
					postCommand.src.rank = rank;
					postCommand.dest.rank = this->transport->getRank();
					postCommand.dest.rank = this->transport->getUnitId(unit);
					
					//addr
					int ruId = transport->getGblIdOfUnit(rank,unitId);
					void * metaAddr = meta->getCreditPtr(creditId,rail,ruId);
					void * dataAddr = data->getCreditPtr(creditId,rail,ruId);
					
					if (config->skipMeta == false) {
						//post recv for META
						postCommand.args.gatherAck.uuid = getUUID(rank,unitId,rail,TARGET_META);
						postCommand.args.gatherAck.target = TARGET_META;
						transport->postWaitRemoteRDMAWrite(
							unit,
							rank,
							unitId,
							metaAddr,
							config->getDefaultMetaDataSize(),
							postCommand.args.gatherAck.uuid,
							postCommand
						);
					}
					
					//post recv for DATA
					postCommand.args.gatherAck.uuid = getUUID(rank,unitId,rail,TARGET_DATA);
					postCommand.args.gatherAck.target = TARGET_DATA;
					transport->postWaitRemoteRDMAWrite(
						unit,
						rank,
						unitId,
						dataAddr,
						config->eventRailMaxDataSize,
						postCommand.args.gatherAck.uuid,
						postCommand
					);
				}
			}
		}
	}
}

}
