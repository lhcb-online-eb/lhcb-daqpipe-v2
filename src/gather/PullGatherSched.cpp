/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include "PullGatherSched.hpp"
#include "units/Unit.hpp"
#include "units/BuilderUnit.hpp"
#include "common/Debug.hpp"
#include <list>
#include <unistd.h>
#ifdef HAVE_HTOPML
	#include "htopml/DynamicConfigHttpNode.hpp"
#endif //HAVE_HTOPML
#include<iostream>
#include<chrono>

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*******************  FUNCTION  *********************/
PullGatherSched::PullGatherSched ( const DAQ::Config* config, DAQ::Unit* unit, DAQ::Transport* transport, int creditId, DAQ::BuilderUnitData* meta, DAQ::BuilderUnitData* data ) 
                 :PullGather ( config, unit, transport, creditId, meta, data )
{
	this->eventId = -1;
	this->parallel = config->getAndSetConfigInt("gather.sched","parallel",2);
	this->tasks.disableWaitOnEmpty();
	
	//registerParameter parallel for dynamic configuration
	#ifdef HAVE_HTOPML
		DynamicConfigHttpNode::registerParam("parallel",this->parallel,1,50,[&](long value){
			this->parallel = value;
			std::cout<<value;
		});
	#endif //HAVE_HTOPML
	
	//extract policy
	std::string tmp = config->getAndSetConfigString("gather.sched","policy","barrel");
	if (tmp == "simple")
		policy = SCHED_POLICY_SIMPLE;
	else if (tmp == "barrel")
		policy = SCHED_POLICY_BARREL;
	else if (tmp == "random")
		policy = SCHED_POLICY_RANDOM;
	else if (tmp == "rdbarrel")
		policy = SCHED_POLICY_RDBARREL;
	else if (tmp == "xor")
		policy = SCHED_POLICY_XOR;
	else
		DAQ_FATAL_ARG("Invalid policy : %1, check gather.sched.policy").arg(tmp).end();
	
	//thread
	if (config->gatherTimer > 0)
	{
		runThread = true;
		thread = std::thread([=](){this->threadMain();});
	} else {
		runThread = false;
	}
}

/*******************  FUNCTION  *********************/
PullGatherSched::~PullGatherSched ()
{
	if (runThread)
	{
		runThread = false;
		thread.join();
	}
}

/*******************  FUNCTION  *********************/
void PullGatherSched::threadMain ()
{
	using Clock = std::chrono::high_resolution_clock;
	Clock::time_point next(Clock::duration(config->refTime));
	while (runThread) {
		//get time
		auto cur = Clock::now();
		//if later than next
		if (cur > next) {
			//do job
			for (int i = 0 ; i < parallel ; i++)
				this->doNextTask();
			
			//compute next
			next += std::chrono::microseconds(config->gatherTimer);
		} else {
			//DAQ_INFO_ARG("wait %1.%2 == %3.%4").arg(cur.tv_sec).arg(cur.tv_nsec).arg(next.tv_sec).arg(next.tv_nsec).end();
		}
		
		//to not trash the cpu
		std::this_thread::sleep_for(std::chrono::microseconds(1));
	}
}

/*******************  FUNCTION  *********************/
bool PullGatherSched::doNextTask ()
{
	//extract
	PullGatherSchedTask task;
	if (task << this->tasks) {
		//run
		if (transport->isDead(task.rank))
			doNextTask();
		else
			this->postRequests(task.rank,task.unitId,eventId);
		
		return true;
	} else {
		return false;
	}
}

/*******************  FUNCTION  *********************/
void PullGatherSched::start ( long int eventId )
{
	this->eventId = eventId;
	
	//check state
	DAQ_DEBUG("failure","start");
	assume(inUse == false,"Try to use a gather manager which is already in use !");
	
	//reset state
	this->reset();
	

	//select
	switch (policy) {
		case SCHED_POLICY_SIMPLE:
			this->startSimple(eventId);
			break;
		case SCHED_POLICY_BARREL:
			this->startBarrel(eventId);
			break;
		case SCHED_POLICY_RANDOM:
			this->startRandom(eventId);
			break;
		case SCHED_POLICY_RDBARREL:
			this->startRdBarrel(eventId);
			break;
		case SCHED_POLICY_XOR:
			this->startXor(eventId);
			break;
		default:
			DAQ_FATAL("Invalid value for policy");
	}
	
	//run first
	if (config->gatherTimer == 0)
		for (int i = 0 ; i < parallel ; i++)
			doNextTask();
}

/*******************  FUNCTION  *********************/
void PullGatherSched::startRdBarrel ( long int eventId )
{
	int shift = rand() % transport->getWorldSize();
	for (int rank = 0 ; rank < transport->getWorldSize() ; rank++) {
		int r = (rank + shift) % transport->getWorldSize();
		for (int unitId = 0 ; unitId < DAQ_MAX_UNITS_PER_PROCESS ; unitId++) {
			if (transport->getUnitType(r,unitId) == UNIT_READOUT_UNIT) {
				PullGatherSchedTask task;
				task.rank = r;
				task.unitId = unitId;
				tasks.push(task);
			}
		}
	}
}

/*******************  FUNCTION  *********************/
void PullGatherSched::startSimple ( long int eventId )
{
	for (int rank = 0 ; rank < transport->getWorldSize() ; rank++) {
		for (int unitId = 0 ; unitId < DAQ_MAX_UNITS_PER_PROCESS ; unitId++) {
			if (transport->getUnitType(rank,unitId) == UNIT_READOUT_UNIT) {
				PullGatherSchedTask task;
				task.rank = rank;
				task.unitId = unitId;
				tasks.push(task);
			}
		}
	}
}

/*******************  FUNCTION  *********************/
void PullGatherSched::startBarrel ( long int eventId )
{
	int shift = transport->getRank();
	for (int rank = 0 ; rank < transport->getWorldSize() ; rank++) {
		int r = (rank + shift) % transport->getWorldSize();
		for (int unitId = 0 ; unitId < DAQ_MAX_UNITS_PER_PROCESS ; unitId++) {
			if (transport->getUnitType(r,unitId) == UNIT_READOUT_UNIT) {
				PullGatherSchedTask task;
				task.rank = r;
				task.unitId = unitId;
				tasks.push(task);
			}
		}
	}
}

/*******************  FUNCTION  *********************/
void PullGatherSched::startXor ( long int eventId )
{
	int shift = transport->getRank();
	for (int rank = 0 ; rank < transport->getWorldSize() ; rank++) {
		int r = (int)((unsigned int)rank ^ (unsigned int)shift) % transport->getWorldSize();
		for (int unitId = 0 ; unitId < DAQ_MAX_UNITS_PER_PROCESS ; unitId++) {
			if (transport->getUnitType(r,unitId) == UNIT_READOUT_UNIT) {
				PullGatherSchedTask task;
				task.rank = r;
				task.unitId = unitId;
				tasks.push(task);
			}
		}
	}
}

/*******************  FUNCTION  *********************/
void PullGatherSched::startRandom ( long int eventId )
{
	//fill all
	std::list<PullGatherSchedTask> ltasks;
	for (int rank = 0 ; rank < transport->getWorldSize() ; rank++) {
		for (int unitId = 0 ; unitId < DAQ_MAX_UNITS_PER_PROCESS ; unitId++) {
			if (transport->getUnitType(rank,unitId) == UNIT_READOUT_UNIT) {
				PullGatherSchedTask task;
				task.rank = rank;
				task.unitId = unitId;
				ltasks.push_back(task);
			}
		}
	}
	
	//extract in random order to push in tasks
	while (ltasks.empty() == false) {
		//select one
		int id = rand() % ltasks.size();
		//get it
		auto it = ltasks.begin();
		while (id > 0) {
			--id;
			++it;
		}
		assert(it != ltasks.end());
		//extract it and push
		tasks.push(*it);
		ltasks.erase(it);
	}
}

/*******************  FUNCTION  *********************/
bool PullGatherSched::onChunkFinished ( const UnitCommand& command )
{
	DAQ_DEBUG("gather","Check and run next");
	bool ret = PullGather::onChunkFinished(command);
	if (config->gatherTimer != 0)
		return ret;
	if (!ret)
		doNextTask();
	else
		assume(tasks.empty(),"Expect to get empty task when finished but still get somes !");
	return ret;
}

}
