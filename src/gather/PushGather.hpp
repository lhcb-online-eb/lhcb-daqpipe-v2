/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_PUSH_GATHER_HPP
#define DAQ_PUSH_GATHER_HPP

/********************  HEADERS  *********************/
#include "Gather.hpp"

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*********************  CLASS  **********************/
/**
 * Class used to handle the tracking of gather requestes. It is affected to one specific creditId
 * and instantiated into the builder units.
 * @brief Keep track of gather status in builder units
**/
class PushGather : public Gather
{
	public:
		PushGather(const Config * config,Unit * unit,Transport * transport,int creditId,BuilderUnitData * meta,BuilderUnitData * data);
		virtual void start(long int eventId);
	protected:
		void postRdmaRecvRequest();
};

}

#endif //DAQ_PUSH_GATHER_HPP
