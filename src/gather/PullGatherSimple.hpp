/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_GATHER_ALL_HPP
#define DAQ_GATHER_ALL_HPP

/********************  HEADERS  *********************/
#include "PullGather.hpp"


/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*********************  CLASS  **********************/
/**
 * Provide implementation of realy simple scheduling : by default send all requests in one Go.
**/
class PullGatherSimple : public PullGather
{
	public:
		PullGatherSimple ( const Config* config, Unit* unit, Transport* transport, int creditId, BuilderUnitData* meta, BuilderUnitData* data );
		virtual ~PullGatherSimple() = default;
		void start(long eventId);
	private:
		int eventId;
};

}

#endif //DAQ_GATHER_ALL_HPP
