/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include <units/Unit.hpp>
#include <units/BuilderUnit.hpp>
#include "common/Debug.hpp"
#include "PullGather.hpp"

#include <sys/types.h>
#include <signal.h>
#include <unistd.h>

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*******************  FUNCTION  *********************/
PullGather::PullGather(const Config* config, Unit* unit, Transport* transport, int creditId, BuilderUnitData* meta, BuilderUnitData* data)
	: Gather(config, unit, transport, creditId, meta, data)
{

}

/*******************  FUNCTION  *********************/
/**
 * Post a request. It post 2 meta + 2 data segment recive via RDMA and send the request command.
 * @param rank Then target rank to communicate with.
 * @param unitId the target unit ID to communicate with.
 * @param eventId The event ID to fetched.
**/
void PullGather::postRequests ( int rank, int unitId, long int eventId )
{	
	//check
	assume(transport->getUnitType(rank,unitId) == UNIT_READOUT_UNIT,"Invalid unit type, expect RU !");
	DAQ_DEBUG_ARG("gather","Emit request for %1:%2 on rank %3").arg(rank).arg(unitId).arg(transport->getRank()).end();
	
	//setup command
	UnitCommand command;
	ArgPullGather & args = command.args.pullGather;
	command.type = UNIT_CMD_PULL_REQUEST_WRITE;
	args.creditId = creditId;
	args.eventId = eventId;
	this->eventId = eventId;
	
	//stup signal command when op is finished
	UnitCommand signalCommand;
	signalCommand.type = UNIT_CMD_PULL_WRITE_FINISHED;
	
	//get ru id
	int ruId = transport->getGblIdOfUnit(rank,unitId);
	
	//setup
	for (int rail = 0 ; rail < DAQ_DATA_SOURCE_RAILS ; rail++) {
		//addrs
		void * metaAddr = meta->getCreditPtr(creditId,rail,ruId);
		void * dataAddr = data->getCreditPtr(creditId,rail,ruId);
					
		//get iovs
		RemoteIOV metaiov = transport->getRemoteIOV(rank,metaAddr,config->getDefaultMetaDataSize());
		RemoteIOV dataiov = transport->getRemoteIOV(rank,dataAddr,config->eventRailMaxDataSize);
					
		//TODO remove this hack (move this insive getRemoteIOV by giving rank as parameter)
		if (rank == this->transport->getRank())
		{
			metaiov.addr = metaAddr;
			dataiov.addr = dataAddr;
		}

		//check
		assert(state.get(rank,unitId,ressourceId(rail,TARGET_META)) == false);
		assert(state.get(rank,unitId,ressourceId(rail,TARGET_DATA)) == false);
		
		//setup
		args.metaIov[rail] = metaiov;
		args.dataIov[rail] = dataiov;
		args.uuidMeta[rail] = getUUID(rank,unitId,rail,TARGET_META);
		args.uuidData[rail] = getUUID(rank,unitId,rail,TARGET_DATA);

		//setup signal
		signalCommand.args.gatherAck.creditId = creditId;
		signalCommand.args.gatherAck.eventId = eventId;
		signalCommand.args.gatherAck.rail = rail;
		signalCommand.src.rank = rank;
		signalCommand.src.id = unitId;
		signalCommand.dest.rank = this->transport->getRank();
		signalCommand.dest.id = this->transport->getUnitId(unit);
		
		//post wait meta
		if (config->skipMeta == false) {
			signalCommand.args.gatherAck.target = TARGET_META;
			signalCommand.args.gatherAck.uuid = args.uuidMeta[rail];
			transport->postWaitRemoteRDMAWrite(
				unit,
				rank,
				unitId,
				metaAddr,
				config->getDefaultMetaDataSize(),
				args.uuidMeta[rail],
				signalCommand
			);
		}
		
		//post wait data
		signalCommand.args.gatherAck.target = TARGET_DATA;
		signalCommand.args.gatherAck.uuid = args.uuidData[rail];
		transport->postWaitRemoteRDMAWrite(
			unit,
			rank,
			unitId,
			dataAddr,
			config->eventRailMaxDataSize,
			args.uuidData[rail],
			signalCommand
		);
	}
	
	//send request to remote
	transport->sendCommand(this->unit,rank,unitId,command);
}

}
