/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
//must be on top
#include "config.h"
//std
#include <iostream>
#include <cstdlib>
#include <unistd.h>
#include <cstring>
#include <thread>
#include <chrono>
//internals
#include "common/Config.hpp"
#include "common/Debug.hpp"
#include "portability/Hwloc.hpp"
#include "topology/Topology.hpp"
//layout
#include "Layout.hpp"
#ifdef HAVE_HTOPML
	#include "htopml/DynamicConfigHttpNode.hpp"
#endif //HAVE_HTOPML
#ifdef TRANSPORT_MPI
	#include <mpi.h>
#endif


/**********************  USING  *********************/
using namespace DAQ;

/*******************  FUNCTION  *********************/
/**
 * Build local map to know which ranks are hosted on the same node.
 * This is used to now accound local communications into the printed
 * bandwidth.
 * @param launcher Access to launcher to register/fetch hostnames of
 * all ranks.
**/
bool * buildLocalMap(Launcher & launcher)
{
	#ifdef TRANSPORT_MPI
		char buffer[2048];
		bool * map = new bool[launcher.getWorldSize()];
		gethostname(buffer,sizeof(buffer));
		char * all = new char[2048 * launcher.getWorldSize()];
		MPI_Allgather(buffer, 2048, MPI_CHAR, all, 2048, MPI_CHAR, MPI_COMM_WORLD);
		
		//build local map
		for (int rank = 0 ; rank < launcher.getWorldSize() ; rank++) {
			map[rank] = (strcmp(all+(rank*2048),buffer) == 0);
		}
		
		return map;
	#elif defined(TRANSPORT_LOCAL)
		bool * map = new bool;
		*map = true;
		return map;
	#else
		char buffer[2048];
		bool * map = new bool[launcher.getWorldSize()];
		gethostname(buffer,sizeof(buffer));
		launcher.set("hostname",buffer);
		launcher.commit();
		launcher.barrier();
		
		//build local map
		for (int rank = 0 ; rank < launcher.getWorldSize() ; rank++)
			map[rank] = (launcher.get("hostname",rank) == buffer);
		
		return map;
	#endif
}

/*******************  FUNCTION  *********************/
/**
 * Function used to init timings in DAQPIPE, it mostly compute
 * a starting time to be shared to everybody via the launcher.
 * @param config Config object to fetch the sarting delay.
 * @param launcher Launcher used to send the starting time to everybody.
**/
void setupTimings(Config & config, Launcher & launcher)
{
	//master compute ref
	if (launcher.getRank() == 0)
	{
		auto cur = std::chrono::high_resolution_clock::now();
		//add 1 second
		int startDelta = config.getAndSetConfigInt("general","startDelay",1);
		cur += std::chrono::seconds(startDelta);
		config.refTime = cur.time_since_epoch().count();
		
		//share
		#if defined(TRANSPORT_MPI)
			//nothing
		#elif defined(TRANSPORT_LOCAL)
			//no need to share
		#else
		std::stringstream ss;
		ss << config.refTime;
		std::string const s = ss.str();
		launcher.set("refTime",s);
		#endif
	}
	
	//share
	launcher.commit();
	launcher.barrier();
	
	//setup
	#if defined(TRANSPORT_MPI)
		MPI_Bcast( &config.refTime, sizeof(config.refTime), MPI_BYTE, 0, MPI_COMM_WORLD);
	#elif defined(TRANSPORT_LOCAL)
		//nothing
	#else
		std::string const s = launcher.get("refTime",0);
		std::stringstream ss;
		ss.str(s);
		ss >> config.refTime;
	#endif
}

/*******************  FUNCTION  *********************/
/**
 * Main function to run DAQPIPE
 * @param config Config object to transmit to all sub-objects.
 * @param argc Number of arguments recived by the application (to be send to launcher)
 * @param argv Arguments recived by the application (to be send to launcher)
**/
void run(Config & config,int argc, char ** argv)
{
	//keep track of rank to check at exit
	int rank;
	
	//setup launcher
	Layout::Launcher launcher(&config);
	launcher.initialize(argc,argv);
	rank = launcher.getRank();
	Debug::setRank(rank);
	config.rank = rank;
	
	//build local map
	config.localMap = buildLocalMap(launcher);
	
	//setup Driver
	Layout::Driver driver;
	driver.initialize(config, launcher,argc,argv);

	//some printing
	if (rank == 0) {
		DAQ_MESSAGE("====================================================");
		DAQ_MESSAGE_ARG("Version : %1").arg(DAQPIPE_VERSION).end();
		DAQ_MESSAGE_ARG("Driver : %1").arg(driver.getName()).end();
		DAQ_MESSAGE_ARG("Credits : %1").arg(config.builderCredits).end();
		DAQ_MESSAGE_ARG("Rail size : %1").arg(config.eventRailMaxDataSize).end();
		DAQ_MESSAGE_ARG("Meta size : %1").arg(config.getDefaultMetaDataSize()).end();
		DAQ_MESSAGE_ARG("Mode : %1").arg(cstModeStr[config.mode]).end();
		DAQ_MESSAGE_ARG("Source : %1").arg(config.dataSource).end();
		DAQ_MESSAGE("====================================================");

		//warning if in debug mode
		#ifndef NDEBUG
			DAQ_WARNING("Caution, you are in debug mode, performance are impacted !");
		#endif //NDEBUG
	}
	
	//setup transport
	Layout::Transport transport(&config,&launcher,&driver);
	transport.initialize(config, launcher);
	#ifdef HAVE_HTOPML
		DynamicConfigHttpNode::registerTransport(transport);
	#endif
		
	//timings
	setupTimings(config,launcher);
	
	//setup topology
	Topology::setupTopology(transport,config,TOPOLOGY_LOCAL);
	
	//instantiate units
	transport.buildLocalUnits();
	
	//safety check
	if (config.readoutUnitEvents < transport.countUnits(DAQ::UNIT_BUILDER_UNIT) * config.builderCredits)
		DAQ_WARNING_ARG("Not enought stored event in RU (%1), expect more then %2 due to world size and credits, code might be slower !")
			.arg(config.readoutUnitEvents)
			.arg(transport.countUnits(DAQ::UNIT_BUILDER_UNIT) * config.builderCredits)
			.end();
	
	//run
	transport.run();
	
	//show list of verbose options
	if (config.showVerboseList && launcher.getRank() == 0)
		Debug::showList();
	
	//finalize all
	transport.clear();
	transport.finalize();
	driver.finalize();
	launcher.finalize();
	
	//dump config file if rank is 0
	if (rank == 0)
		config.dumpConfigFile();
}

/*******************  FUNCTION  *********************/
int main(int argc, char **argv) 
{
	// set stdout to be unbuffered
	// this helps when output is redirected and the application aborts
	std::cout.setf(std::ios::unitbuf);
	//load config
	Config config;
	config.parseArgs(argc,(const char**)argv);
	
	//pin threads
	std::string deviceName = config.getAndSetConfigString("binding","netDevice","");
	pinProcessNearNetDevice(deviceName);
	
	//start deadlock killer in case of trouble
	if (config.runTimeout > 0) {
		std::thread th([&config] {
			std::this_thread::sleep_for(std::chrono::seconds(2*config.runTimeout));
			DAQ_ERROR("Get deadlock timeout, exiting to not eat ressources !");
			exit(1);
		});
		th.detach();
	}
	
	//run
	run(config,argc,argv);
	
	//return
	return EXIT_SUCCESS;
}
