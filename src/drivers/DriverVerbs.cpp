/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.1.0
             DATE     : 12/2017
             AUTHOR   : Krawczyk Rafal - CERN/Warsaw University of Technology
             LICENSE  : CeCILL-C
*****************************************************/

/****************************************************
For additional information see following links
Reference RDMA IB application:
http://www.digitalvampire.org/rdma-tutorial-2007/notes.pdf

A manual with <rdma_cma.h> and <infiniband/verbs.h> functions description
http://www.mellanox.com/related-docs/prod_software/RDMA_Aware_Programming_user_manual.pdf 

Tutorial to use ibv - no RDMA info is given
http://www.csm.ornl.gov/workshops/openshmem2013/documents/presentations_and_tutorials/Tutorials/Verbs%20programming%20tutorial-final.pdf

/********************  HEADERS  *********************/
#include "DriverVerbs.hpp"
//#include <rdma_cma.h>
//#include <infiniband/verbs.h>
#include <portability/TcpHelper.hpp>
#include <arpa/inet.h>
#include <iostream>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h> 


/********************  NAMESPACE  *******************/
namespace DAQ
{

/*******************  FUNCTION  *********************/
DriverVerbs::DriverVerbs()
	:DriverSlots<VerbsRequest>("DriverVerbs")
{
	
}

/*******************  FUNCTION  *********************/
DriverVerbs::~DriverVerbs()
{
	
}

/*******************  FUNCTION  *********************/
void DriverVerbs::initialize( const DAQ::Config& config, DAQ::Launcher& launcher, int argc, char** argv )
{
	//call parent
	DriverSlots<VerbsRequest>::initialize(config,launcher,argc,argv);
	//Setup of DriverSlots for IB
	setNoAnySource();
	setNoRdmaAck();
	
	//load and setup global config
	this->useIpV6        = config.getAndSetConfigBool("drivers.verbs","ipv6",false);
	this->portBase       = config.getAndSetConfigInt("drivers.verbs","portBase",2000);
	this->portRange      = config.getAndSetConfigInt("drivers.verbs","portRange",256);
	this->iface          = config.getAndSetConfigString("drivers.verbs","iface","ib0");
	this->resolveTimeout = config.getAndSetConfigInt("drivers.verbs","resolveTimeout",5000);
	this->backLogSize    = config.getAndSetConfigInt("drivers.verbs","listenBackLog",128);
	
	//PLEASE NOTE -the following 3 parameters are of importance for performance reasons and insufficient number of allocated resources can lead to improper working of a code
	this->totalStoredCompletions= config.getAndSetConfigInt("drivers.slots","maxOnFly",64);	
	this->totalWRs= config.getAndSetConfigInt("drivers.slots","maxOnFly",64);
	this->totalSGEs= config.getAndSetConfigInt("drivers.verbs","totalSGEs",8);
	int cmdPost = config.getAndSetConfigInt("drivers.slots","cmdRecver",2);
	if (cmdPost < 4)
		DAQ_WARNING("Using a low value for drivers.slots.cmdRecver might lead to poor performance !");
	
	DAQ_DEBUG_ARG("verbs","Read WRs:  %1").arg(this->totalWRs).end();
	
	//assign a launcher - further needed in PostRecvCommand
	this->launcher=&launcher;
	//get rank and worls size not to call launcher
	procRank=this->launcher->getRank();
	worldSize=this->launcher->getWorldSize();
	
	//common pd for the IB device - assuming that only one device will be in a system
	this->pd=nullptr;
	//for each connection of node process pairs connection manager and completion queue  structures must be separate
	this->cm_id=(struct rdma_cm_id**)malloc(worldSize*sizeof(struct rdma_cm_id*));	
	//completion queue- to store completed events -polled later on in doTest
	this->cq=(struct ibv_cq**)malloc(worldSize*sizeof(struct ibv_cq*));
	// work completion structures for polling
	m_wcs.resize(totalStoredCompletions);
	
	//add device IP in the global database
	std::string ip = TcpHelper::getLocalIP(this->iface,this->useIpV6);
	this->launcher->set("ip",ip);
	
	this->launcher->commit();
	DAQ_DEBUG_ARG("verbs","Added device IP to laucher database:  %1").arg(ip).end();
	
	//first barrier after setting all IPs
	this->launcher->barrier();
	
	//create communication channels -2 to avoid race at address resolving 
	//it is an alternative approach for waiting for appropriate event in a loop - see DAQPIPEv1
	this->cm_channel = rdma_create_event_channel();
	this->cm_listen_channel = rdma_create_event_channel();
	
	//create communication identifiers for all devices to whom node connects
	for(int cmIt=0;cmIt!=this->procRank ;cmIt++) {	
		assume(cm_channel, "Failed to create channel");
		//allocate communication identifiers
		//with the equivalent of TCP socket
		assume(!rdma_create_id (this->cm_channel,&(this->cm_id[cmIt]),nullptr,RDMA_PS_TCP),"Failed to create communication identifier");
	}
	
	DAQ_DEBUG("verbs","Created CM ID");
	
	//associate source address with rdma_cm_id
	struct sockaddr_in sin;
	struct sockaddr_in6 sin6;
	if(this->useIpV6) {
	memset(&sin6, 0, sizeof(sin6));
	sin6.sin6_family= AF_INET6;
	sin6.sin6_port= htons(getPort(procRank));
	sin6.sin6_addr = in6addr_any;
	} else {
	memset(&sin, 0, sizeof(sin));
	sin.sin_family= AF_INET;
	sin.sin_port= htons(getPort(procRank));
	sin.sin_addr.s_addr = INADDR_ANY; //bind to all interfaces on the local machine- ow whom we assume to have only one	
	}
	
	struct sockaddr * sa = (this->useIpV6 ? ( struct sockaddr *) (&sin6) : ( struct sockaddr *) (&sin));
	//create listening socket, bind and start to listen for connection requests
	assume( !rdma_create_id ( this->cm_listen_channel, &this->listen_id,nullptr, RDMA_PS_TCP),"Failed to create communication listening identifier");
	assume( !rdma_bind_addr (this->listen_id,sa),"Failed to bind to listening socket");
	assume(!rdma_listen (this->listen_id,this->backLogSize),"Failed initiate listen"); 
	//barrier to be sure that all nodes listen for connection requests        	
	this->launcher->barrier();
	DAQ_DEBUG("verbs","AFTER LISTENING BARRIER");
	doRouteNResolveAll();
	this->launcher->barrier();
	DAQ_DEBUG("verbs","AFTER RESOLVE BARRIER");
	//connect to devices and setup Ctx while connecting
	doConnectNSetupCtxAll();
	
	//auxiliary structure to map incoming events with rank they have
	int *connRank=(int *)malloc(worldSize*sizeof(int));
	
	//listen for other devices
	doAcceptAll(connRank);
	DAQ_DEBUG("verbs","Got all requests- responding to connect requests and setting ctx");
	//setup Ctx for devices that were connecting to a server
	doServerSetupCtxAll(connRank);
	free(connRank);
	DAQ_DEBUG("verbs","Accepting done- setting barrier");
	this->launcher->barrier();
	doCommandRegistration();
	DAQ_DEBUG("verbs","AFTER INIT BARRIER- init complete");
}

/*******************  FUNCTION  *********************/
void DriverVerbs::finalize()
{
	DAQ_DEBUG("verbs","Finalize  invoked");
	doCommandDeRegistration();
	rdma_destroy_id(listen_id);
	rdma_destroy_event_channel(cm_listen_channel);
	
	for(int ctxIt=0;ctxIt!=this->worldSize;ctxIt++) {
		if(ctxIt==procRank)
			continue;
		ibv_destroy_cq(this->cq[ctxIt]);
		rdma_destroy_qp(this->cm_id[ctxIt]);
		rdma_destroy_id(this->cm_id[ctxIt]);
	}
	
	free(this->cq);
	free(this->cm_id);
	ibv_dealloc_pd(this->pd);
	rdma_destroy_event_channel(cm_channel);
}

/*******************  FUNCTION  *********************/
void DriverVerbs::registerSegment( void * addr, size_t size )
{
	//DAQ_DEBUG("verbs","registerSegment invoked ");
	assert( addr!= nullptr);
	assert(size > 0);
	//create and setup region
	MemoryRegion region;
	region.ptr = addr;
	region.size = size;
	region.mr = nullptr;
	//DAQ_DEBUG_ARG("verbs","Register new memory segment into driver : %1 of size %2 and id %3").arg(addr).arg(size).arg(segments.size()).end();
	if (getIbvMR(addr,size) != nullptr) {
		DAQ_DEBUG("verbs","Segment already exists ");
		return;
	}
	//TODO - check flags if all are necessary
	region.mr= ibv_reg_mr ( this->pd,addr,size, IBV_ACCESS_LOCAL_WRITE|IBV_ACCESS_REMOTE_WRITE|IBV_ACCESS_REMOTE_READ );
	assume(region.mr,"Failed to register memory region");
	segments.push_back(region);
}

/*******************  FUNCTION  *********************/
void DriverVerbs::unregisterSegment( void * addr, size_t size )
{
	//DAQ_DEBUG("verbs","unregisterSegment invoked");
	MemoryRegion *MReg  = getMR(addr,size);
	ibv_dereg_mr(MReg->mr);
	for (verbsMemoryRegionVector::iterator it = segments.begin(); it != segments.end() ; ++it) {
		if (it->ptr <=  addr && (char*)it->ptr + it->size > addr) {
			it = segments.erase(it,it+1);
			break;
		}
	}
}

/*******************  FUNCTION  *********************/
uint64_t DriverVerbs::getSegmentRemoteKey ( void* addr, size_t size )
{
	/*Not used in this particular interface */
	return 0;
}

/*******************  FUNCTION  *********************/
RemoteIOV DriverVerbs::getRemoteIOV( void * addr, size_t size )
{
	//DAQ_DEBUG("verbs","getRemoteIOV invoked");
	
	assert(addr != nullptr);
	assert(size > 0);
	
	RemoteIOV iov;
	MemoryRegion * MReg = getMR(addr,size);
	assert(MReg != nullptr);
	DAQ_DEBUG_ARG("verbs","getRemoteIOV:addr %1 mr addr:%2").arg(addr).arg(MReg->mr->addr).end();
	iov.addr =addr;
	iov.key = MReg->mr->rkey;
	return iov;
}

/*******************  FUNCTION  *********************/
void DriverVerbs::doInitRequest( VerbsRequest & request )
{
	//DAQ_DEBUG("verbs","doInitRequest invoked ");
	request.send_wr= {};
	request.recv_wr= {};
}

/*******************  FUNCTION  *********************/
void DriverVerbs::doSendCommand ( UnitCommand& command, VerbsRequest& request )
{
	//DAQ_DEBUG("verbs","sendCommand invoked ");
	request.is_rdma=false;
	request.is_recs_req=false;
	request.send_wr.wr_id = (uint64_t)(&request);
	
	setupCmdReq(command,request);
	DAQ_DEBUG_ARG("verbs","doSendCommand postsend to rank %1  with request addr %2").arg(command.dest.rank).arg(&request).end();
	assume(!ibv_post_send ( cm_id[command.dest.rank]->qp ,&( request.send_wr) ,&( request.bad_send_wr )),"Failed to post send");
}

/*******************  FUNCTION  *********************/
void DriverVerbs::doPostCommandRecv ( DAQ::UnitCommand& command, VerbsRequest& request, int rank  )
{
	assert(rank != -1);
	//DAQ_DEBUG_ARG("verbs","doPostCommandRecv invoked:  connecting to %2").arg(rank).end();
	
	//set request info
	request.is_rdma=false;
	request.is_recs_req=true;
	request.recv_wr.wr_id = (uint64_t)(&request);
	
	setupCmdReq(command,request);
	//add a receive request to the receive queue
	DAQ_DEBUG_ARG("verbs","doPostCommandRecv invoking postrecv  with addr %1 ").arg(&request).end();
	assumeArg(!ibv_post_recv( this->cm_id[rank]->qp , &(request.recv_wr) ,&(request.bad_recv_wr) ),"Failed to post recv errno %1").arg(errno).end();
	
	DAQ_DEBUG("verbs","Rank SYNC BARRIER recv");
	this->launcher->barrier();
	DAQ_DEBUG("verbs","doPostCommandRecv ended AFTER SYNC BARRIER");
}

/*******************  FUNCTION  *********************/
void DriverVerbs::doRdmaWrite ( int rank, int uuid, void* src,void * dest, uint64_t destKey, size_t size, VerbsRequest& request )
{
	DAQ_DEBUG("verbs","doRdmaWrite invoked");
	setupRdmaReq(request,src,dest,destKey,size);
	assume(!ibv_post_send ( cm_id[rank]->qp ,&( request.send_wr) ,&( request.bad_send_wr )),"Failed to post send");
}

/*******************  FUNCTION  *********************/
void DriverVerbs::doPostRdmaRecv ( int rank, int uuid, void* ptr, size_t size, VerbsRequest& request )
{
	/*Not used in this particular interface */
	DAQ_DEBUG("verbs","postRdmaRecv invoked");
	return;
}

/*******************  FUNCTION  *********************/
int DriverVerbs::doTest ( VerbsRequest* requests, size_t count, bool useWait )
{
//	DAQ_DEBUG("verbs","doTest invoked ");
	struct ibv_wc wc;
	do {
		VerbsRequest * context;
		if (context << pollingChannel) {
			if (context >= requests && context < requests + count) {
				//DAQ_WARNING("FIRST QUEUE");
				return context - requests;
			} else {	//DAQ_WARNING("SECOND QUEUE");
				this->noAnySourceCmdFinsished(*context);
				//we can continue but prefer to get sans event scheduling that RDM
				return -1;
			}
		}
	} while(useWait);
	return -1;
}

/*******************  FUNCTION  *********************/
void DriverVerbs::doRepublishCommandRecv( DAQ::UnitCommand& command, VerbsRequest& request, int rank  )
{
	assert(rank != -1);
	//DAQ_DEBUG_ARG("verbs","doRepublishCommandRecv invoked:  connecting to %1").arg(rank).end();
	//add a receive request to the receive queue
	DAQ_DEBUG_ARG("verbs","doRepublishCommandRecv invoking postrecv  with request addr %1 ").arg((VerbsRequest*)(request.recv_wr.wr_id)).end();
	assumeArg(!ibv_post_recv( this->cm_id[rank]->qp , &(request.recv_wr) ,&(request.bad_recv_wr) ),"Failed to post recv errno %1").arg(errno).end();
	return;
}

/*******************  FUNCTION  *********************/
void DriverVerbs:: doRouteNResolveAll()
{
	for (int targetRank = 0 ; targetRank < this->procRank; targetRank++) {
		
		
		struct sockaddr_in6 dest_addr6;
		struct sockaddr_in dest_addr;
		if(this->useIpV6) {
		memset(&dest_addr6, 0, sizeof(dest_addr6));
		dest_addr6.sin6_family = AF_INET6; 
		dest_addr6.sin6_port = htons(getPort(targetRank));
		inet_pton(AF_INET6,this->launcher->get("ip",targetRank).c_str(),&(dest_addr6.sin6_addr));
		}else {
		memset(&dest_addr, 0, sizeof(dest_addr));
		dest_addr.sin_family = AF_INET; 
		dest_addr.sin_port = htons(getPort(targetRank));
		dest_addr.sin_addr.s_addr = inet_addr(this->launcher->get("ip",targetRank).c_str());
		}
		struct sockaddr * sa = (this->useIpV6 ? ( struct sockaddr *) (&dest_addr6) : ( struct sockaddr *) (&dest_addr));
		
		//Resolve IP to an RDMA address - will bind a rdma_cm_id to an id.
		assumeArg( !rdma_resolve_addr(this->cm_id[targetRank],nullptr,sa,this->resolveTimeout),"Failed to resolve addr to %1, errno: %2").arg(targetRank).arg(errno).end();
		DAQ_DEBUG_ARG("verbs","Checking the ibv_context for a created cm_id after addr resolve: device addr %1 ").arg(cm_id[targetRank]->verbs).end();
		
		//retreive comunication event
		//it is by default a blocking function !
		assume( !rdma_get_cm_event(this->cm_channel,&event),"Error getting an event");
		assumeArg(event->event==RDMA_CM_EVENT_ADDR_RESOLVED ,"Addr not resolved- got instead %1").arg(event->event).end();
		//free the communication event- should be done after each retreival
		rdma_ack_cm_event(event);
		
		//now resolving route to establish a connection
		//the destination is given
		assume( !rdma_resolve_route ( this->cm_id[targetRank] , this->resolveTimeout),"Failed to route to addr");
		DAQ_DEBUG_ARG("verbs","Checking the ibv_context for a created cm_id after route resolve: device addr %1 name %2 ").arg(this->cm_id[targetRank]->verbs).arg(this->cm_id[targetRank]->verbs->device->name).end();
		//get context - here or when getting event as a server in doAcceptAll
		this->common_ibv_ctx=this->cm_id[targetRank]->verbs;
		
		//wait for an event from server
		assume( !rdma_get_cm_event ( this->cm_channel ,&event),"Failed to get an event");
		assume( event->event==RDMA_CM_EVENT_ROUTE_RESOLVED,"Failed to resolve route");
		rdma_ack_cm_event(event);
		DAQ_DEBUG_ARG("verbs","Resolved a route to %1").arg(targetRank).end();
	}	
}

/*******************  FUNCTION  *********************/
void DriverVerbs::doConnectNSetupCtxAll ()
{
	for (int targetRank = 0 ; targetRank < this->procRank; targetRank++) {
		if(this->pd==nullptr) {
			this->pd= ibv_alloc_pd ( this->common_ibv_ctx );
				assume(this->pd,"Failed allocate Protection domain");
		}
		//create completion queue
		//in a known context
		//2nd argument - total completions to store
		//3rd argument - no private context
		//completion channel- to check if the data was received - not needed - no sleeping
		//no completion vector needed
		this->cq[targetRank] = ibv_create_cq (this->cm_id[targetRank]->verbs,this->totalStoredCompletions ,nullptr, nullptr , 0);
		assume(this->cq[targetRank],"Failed to create cq");
		
		//set maximum number of outstangind WRs and maximum number of SGEs
		
		struct ibv_qp_init_attr qp_attr={};
		qp_attr.cap.max_send_wr =  this->totalWRs;
		qp_attr.cap.max_send_sge = this->totalSGEs;
		qp_attr.cap.max_recv_wr =  this->totalWRs;
		qp_attr.cap.max_recv_sge = this->totalSGEs;
		//assign same completion queue for receive and send
		qp_attr.send_cq =  cq[targetRank] ;
		qp_attr.recv_cq =  cq[targetRank] ;
		//Set TCP-Like RC - Reliable Connection 
		qp_attr.qp_type = IBV_QPT_RC ;
		
		//create a queue pair
		//in a given context
		//in a given domain
		//with given attributes
		assumeArg(!rdma_create_qp ( this->cm_id[targetRank] , this->pd , & qp_attr ),"Failed to create queue pair: pd addr %1 errno %2").arg(this->pd).arg(errno).end();
		DAQ_DEBUG("verbs","Created qp ");
		
		conn_param.initiator_depth = 1;
		conn_param.retry_count = 7;
		conn_param.private_data=(void*)(&(this->procRank));
		conn_param.private_data_len=sizeof(unsigned int);
		conn_param.rnr_retry_count=7;
		
		//connect to server
		DAQ_DEBUG_ARG("verbs","Sending connect request to %1, pd =%2 ").arg(targetRank).arg(this->pd).end();
		assume(!rdma_connect ( this->cm_id[targetRank] , & conn_param ),"Failed to send connect request");
		
		//get next pending RDMA event
		assume(!rdma_get_cm_event ( this->cm_channel ,& event ),"Failed to get event");
		assumeArg(event->event == RDMA_CM_EVENT_ESTABLISHED ,"Failed to establish connection - obtained instead event %1").arg(event->event).end();
		DAQ_DEBUG("verbs","Succesfuly connected");
		
		rdma_ack_cm_event(event);
	}
	DAQ_DEBUG("verbs","Connecting done - now listening and accepting as a server");
}

/*******************  FUNCTION  *********************/
void DriverVerbs::doAcceptAll ( int* connRank )
{
	DAQ_DEBUG("verbs","Starting to accept in a loop");
	
	for (int  connIt = worldSize-1 ; connIt > this->procRank; connIt--) {
		
		DAQ_DEBUG("verbs","Waiting for event");
		//blocking retrieve next communication event
		assume( !rdma_get_cm_event ( this->cm_listen_channel ,&event),"Failed to get an event");
		DAQ_DEBUG("verbs","Got the event");
		assume( event->event==RDMA_CM_EVENT_CONNECT_REQUEST,"Failed to get connection request");
		
		//get rank of a connecting device from a private data- see ConnectAll
		unsigned int foundRank= *((unsigned int *)event->param.conn.private_data);
		connRank[connIt]=foundRank;	
		DAQ_DEBUG_ARG("verbs","Got request from process rank %1").arg(connRank[connIt]).end();
		this->cm_id[connRank[connIt]]=event->id;
		
		DAQ_DEBUG_ARG("verbs","Checking the ibv_context for an acquired cm_id from rank %1: device addr %2 name %3 ").arg(connRank[connIt]).arg(cm_id[connRank[connIt]]->verbs).arg(cm_id[connRank[connIt]]->verbs->device->name).end();
		//obtain device context - now non-null after obraining connection request
		this->common_ibv_ctx=this->cm_id[connRank[connIt]]->verbs;
		//free the communication event
		DAQ_DEBUG_ARG("verbs","Sending ack to rank %1").arg(connRank[connIt]).end();
		rdma_ack_cm_event ( event );
	}
}

/*******************  FUNCTION  *********************/
void DriverVerbs::doServerSetupCtxAll( int *connRank )
{
	//create one protection domain associated with a device context - if we do not have one from connectAll
	if(this->pd==nullptr) {
		this->pd= ibv_alloc_pd ( this->common_ibv_ctx );
		assume(this->pd,"Failed to allocate protection domain");
	}
	for (int  connIt = worldSize-1 ; connIt > this->procRank; connIt--) {
		
		this->cq[connRank[connIt]] = ibv_create_cq (this->cm_id[connRank[connIt]]->verbs, this->totalStoredCompletions,nullptr,nullptr , 0);
		assume(this->cq[connRank[connIt]],"Failed to allocate completion queue");
		
		struct ibv_qp_init_attr qp_attr={};
		//set maximum number of outstangind WRs and maximum number of SGEs
		qp_attr.cap.max_send_wr  =  this->totalWRs;
		qp_attr.cap.max_send_sge = this->totalSGEs;
		qp_attr.cap.max_recv_wr  =  this->totalWRs;
		qp_attr.cap.max_recv_sge = this->totalSGEs;
		
		//assign same completion queue for receive and send
		qp_attr.send_cq =  this->cq[connRank[connIt]];
		qp_attr.recv_cq =  this->cq[connRank[connIt]];
		//Set TCP-Like RC - Reliable Connection 
		qp_attr.qp_type = IBV_QPT_RC ;
		
		//create a queue pair
		//in a given context
		//in a given domain
		//with given attributes
		assumeArg( !rdma_create_qp ( this->cm_id[connRank[connIt]] ,this->pd , & qp_attr ),"Failed to create queue pair: errno %1").arg(errno).end();
		DAQ_DEBUG_ARG("verbs","Created qp with rank %1").arg(connRank[connIt]).end();
		
		memset(&conn_param, 0, sizeof(conn_param));
		conn_param.rnr_retry_count=7;
		
		//call function to listen to accept connection
		DAQ_DEBUG_ARG("verbs","Attempting to accept request of rank %1").arg(connRank[connIt]).end();
		assumeArg( !rdma_accept (this->cm_id[connRank[connIt]],&conn_param ),"Failed to accept - error code %1").arg(errno).end();
		DAQ_DEBUG_ARG("verbs","Accept sent - context addr %1").arg(&cm_id[connRank[connIt]]).end();
		
		//blocking retrieve next communication event
		assume( !rdma_get_cm_event ( this->cm_listen_channel ,& event ),"Failed to get an event" );
		assume( event->event == RDMA_CM_EVENT_ESTABLISHED,"Failed - event not established" );
		
		//free a communication event
		rdma_ack_cm_event ( event );
		DAQ_DEBUG("verbs","Communication established");
	}
}

/*******************  FUNCTION  *********************/
int DriverVerbs::getPort ( int rank )
{
	return this->portBase + rank % this->portRange;
}

/*******************  FUNCTION  *********************/
ibv_mr* DriverVerbs::getIbvMR ( void* ptr, size_t size )
{
//	DAQ_DEBUG("GET IBV MR INVOKED");
	MemoryRegion * region = getMR(ptr,size);
	if (region == nullptr)
		return nullptr;
	else
		return region->mr;
}

/*******************  FUNCTION  *********************/
MemoryRegion* DriverVerbs::getMR ( void* ptr, size_t size )
{
	//DAQ_DEBUG("GET MR INVOKED");
	MemoryRegion * mr = nullptr;
	for (verbsMemoryRegionVector::iterator it = this->segments.begin(); it != this->segments.end() ; ++it) {
		if (it->ptr <= ptr && (char*)it->ptr + it->size > ptr) {
			if((char*)ptr+size > (char*)it->ptr + it->size)
				DAQ_WARNING("Caution, a segment from verbs not completetly fit with the request which is larger");
			mr = &(*it);
			break;
		}
	}
		return mr;
}

/*******************  FUNCTION  *********************/
void DriverVerbs::setupCmdReq( DAQ::UnitCommand& command, struct DAQ::VerbsRequest& request )
{
	//DAQ_DEBUG("CMD REQUEST SETUP INVOKED");         
	MemoryRegion *MReg = getMR(&command,sizeof(command));
	
	request.command = &command;
	request.sge.addr = ( uintptr_t )(&command) ;
	request.sge.length = sizeof (command);
	request.sge.lkey = MReg->mr->lkey ;            
	
	if(request.is_recs_req) {
		request.recv_wr.num_sge = 1;
		request.recv_wr.sg_list = &(request.sge) ;
	} else {
		request.send_wr.opcode = IBV_WR_SEND ;
		request.send_wr.send_flags = IBV_SEND_SIGNALED ;
		request.send_wr.num_sge = 1;
		request.send_wr.sg_list = &(request.sge) ;
	}
}

/*******************  FUNCTION  *********************/
void DriverVerbs::setupRdmaReq( struct DAQ::VerbsRequest& request, void* src, void * dest, uint64_t destKey, size_t size )
{
    // DAQ_DEBUG("RDMA REQUEST SETUP INVOKED");         
	MemoryRegion *MReg = getMR(src,size);
	
	request.sge.addr = (uintptr_t)src;
	request.sge.length = size;
	request.sge.lkey = MReg->mr->lkey ;
	request.is_rdma = true; 
	request.is_recs_req= false;
	request.send_wr.wr_id = (uint64_t) &request;
	request.send_wr.opcode = IBV_WR_RDMA_WRITE ;
	request.send_wr.send_flags = IBV_SEND_SIGNALED;
	request.send_wr.sg_list = &(request.sge);
	request.send_wr.num_sge = 1;
	request.send_wr.wr.rdma.rkey = destKey;
	request.send_wr.wr.rdma.remote_addr = (uint64_t)dest;         
}

/*******************  FUNCTION  *********************/
void DriverVerbs::startNoThreads ()
{
	DriverSlots<DAQ::VerbsRequest>::startNoThreads();
	//setup properties
	this->pollingChannel.disableWaitOnEmpty();
	this->running = true;
	pollingThread=std::thread([this]() 
	{
		this->polling();
	});
}

/*******************  FUNCTION  *********************/
void DriverVerbs::startThreads ()
{
	DriverSlots<DAQ::VerbsRequest>::startThreads();
	//setup properties
	this->pollingChannel.disableWaitOnEmpty();
	this->running = true;
	pollingThread = std::thread([this]() 
	{
		this->polling();
	});
}

/*******************  FUNCTION  *********************/
void DriverVerbs::stop ()
{
	this->running = false;
	this->pollingThread.join();
	DriverSlots<DAQ::VerbsRequest>::stop();
}

/*******************  FUNCTION  *********************/
void DriverVerbs::polling ()
{
	while(running) {
		for (int i = 0 ; i < worldSize ; i++) {
			if (i != procRank) {
				int ret = ibv_poll_cq ( this->cq[i] ,m_wcs.size(), &m_wcs.front() );
				if(ret != 0) {
					for (int j = 0 ; j < ret ; j++) {
						VerbsRequest * context = (VerbsRequest *)m_wcs[j].wr_id;
						if (m_wcs[j].status==IBV_WC_SUCCESS) {
							DAQ_DEBUG_ARG("verbs","polling SUCCESS: message received: id %1 receive= %2 rdma=%3").arg(context).arg(context->is_recs_req).arg(context->is_rdma).end();
							if (context != nullptr) {
								context >> pollingChannel;
							}
							else {
							  DAQ_WARNING("Caution, found a nullptr pointer in the work completion vector");
							}
						}else {
							DAQ_INFO_ARG("Failed: poll did not get success message, got status %1 id %2 for receive %3 rdma = %4 instead").arg(m_wcs[j].status).arg(context).arg(context->is_recs_req).arg(context->is_rdma).end();
						}
					}
				}
			}
		}
	}
}

}
