/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_DRIVER_LOCAL_SEQ_HPP
#define DAQ_DRIVER_LOCAL_SEQ_HPP

/********************  HEADERS  *********************/
#include <map>
#include "units/Unit.hpp"
#include "Driver.hpp"
#include <thread>
#include <gochannels/GoChannel.hpp>
#include <functional>
#include <list>

/********************  NAMESPACE  *******************/
namespace DAQ
{

/*********************  TYPES  **********************/
typedef std::map<void*,UnitCommand> LocalSeqRDMANotif;
typedef std::list<void*> LocalPendingRdma;

/*********************  CLASS  **********************/
class DriverLocal : public Driver
{
	public:
		DriverLocal();
		virtual ~DriverLocal() = default;
		virtual void initialize( const DAQ::Config& config, DAQ::Launcher& launcher, int argc, char** argv );
		virtual void finalize();
		virtual void registerSegment(void * addr,size_t size);
		virtual void unregisterSegment(void * addr,size_t size);
		virtual uint64_t getSegmentRemoteKey ( void* addr, size_t size );
		virtual void postWaitRemoteRDMAWrite ( Unit* srcUnit, int rank, int unitId, void* ptr, size_t size, int uuid, const DAQ::UnitCommand& postCommand );
		virtual void rdmaWrite ( Unit* srcUnit,int rank, int unitId, void* src, void* dest, uint64_t destKey, size_t size, int uuid, const UnitCommand& postCommand );
		virtual void postCommandRecv();
		virtual void sendRemoteCommand(const UnitCommand & command);
		virtual RemoteIOV getRemoteIOV ( void* addr, size_t size );
		virtual void startThreads ();
		virtual void stop ();
		virtual void startNoThreads ();
		virtual void updateNoThread ();
	private:
		void main();
	private:
		LocalSeqRDMANotif rdmaRemoteWriteNotif;
		std::thread thread;
		std::mutex lock;
		GoChannel<std::function<void()>> tasks;
		bool hasNoThread;
		bool skipMemcpy;
		int realisticMemcpyRate;
		int memcpyRateTracking;
		int memcpyToSkip;
		LocalPendingRdma pendings;
};

}

#endif //DAQ_DRIVER_LOCAL_SEQ_HPP
