/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include "common/Debug.hpp"
#include "DriverLibFabric.hpp"
//std
#include <cstring>
#include <cassert>
//unix
#include <unistd.h>
//headers from libfabric
#include <rdma/fabric.h>
#include <rdma/fi_errno.h>
#include <rdma/fi_endpoint.h>
#include <rdma/fi_cm.h>
#include <rdma/fi_rma.h>

/********************  MACROS  **********************/
#define DAQ_LF_QUEUE_SIZE 100

/********************  NAMESPACE  *******************/
namespace DAQ
{

/*******************  FUNCTION  *********************/
std::string Helper::convertAddrToHex ( const void* addr, int size )
{
	unsigned char * taddr = (unsigned char*)addr;
	std::string ret;
	
	//add size in hexa
	char tmp[4];
	sprintf(tmp,"%03x",size);
	ret+=tmp;

	//content
	for (int i = 0 ; i < size ; i++)
	{
		ret += ('A' + ((taddr[i]&0x0F)));
		ret += ('A' + ((taddr[i]&0xF0)>>4));
	}

	return ret;
}

/*******************  FUNCTION  *********************/
void Helper::convertAddrFromHex ( void* addr, int size, const std::string& value )
{
	int sizeCheck;

	//extract size
	sscanf(value.c_str(),"%03x",&sizeCheck);
	assert(sizeCheck == size);
	
	//extract data
	for (int i = 0 ; i < size ; i++)
		((unsigned char*)addr)[i] = (value[3+2*i] - 'A') + ((value[3+2*i+1] - 'A')<<4);
}

/*******************  FUNCTION  *********************/
std::string Helper::getEqErrString(struct fid_eq *eq, const char *eq_str)
{
	fi_eq_err_entry eq_err;
	const char *err_str;
	int rd;
	std::string res;

	rd = fi_eq_readerr(eq, &eq_err, 0);
	if (rd != sizeof(eq_err)) {
		LIBFABRIC_CHECK_STATUS("fi_eq_readerr",rd);
	} else {
		err_str = fi_eq_strerror(eq, eq_err.prov_errno, eq_err.err_data, nullptr, 0);
		res = eq_str;
		res += ": ";
		res += fi_strerror(eq_err.err);
		res += "\n";
		res += eq_str;
		res += ": prov_err: ";
		res += err_str;
	}

	return res;
}

/*******************  FUNCTION  *********************/
std::string Helper::getCqErrString(struct fid_cq *cq, const char *eq_str)
{
	fi_cq_err_entry eq_err;
	const char *err_str;
	int rd;
	std::string res;

	rd = fi_cq_readerr(cq, &eq_err, 0);
	if (rd < 0) {
		LIBFABRIC_CHECK_STATUS("fi_cq_readerr",rd);
	} else {
		err_str = fi_cq_strerror(cq, eq_err.prov_errno, eq_err.err_data, nullptr, 0);
		res = eq_str;
		res += ": ";
		res += fi_strerror(eq_err.err);
		res += "\n";
		res += eq_str;
		res += ": prov_err: ";
		res += err_str;
	}

	return res;
}

/*******************  FUNCTION  *********************/
int Helper::waitForCompletion ( fid_cq* cq, int num_completions )
{
	int ret;
	struct fi_cq_data_entry comp;
	while (num_completions > 0) {
		ret = fi_cq_read(cq, &comp, 1);
		if (ret > 0) {
			num_completions--;
		} else if (ret < 0 && ret != -FI_EAGAIN) {
			if (ret == -FI_EAVAIL) {
				LIBFABRIC_CQ_FATAL_ERROR("fi_cq_read",cq);
			} else {
				LIBFABRIC_CHECK_STATUS("fi_cq_read", ret);
			}
			return ret;
		}
	}
	return 0;
}

/*******************  FUNCTION  *********************/
std::string Helper::getHostIp(const std::string & hostname)
{
	struct hostent *he;
	struct in_addr **addr_list;
	
	//get entry
	he = gethostbyname( hostname.c_str() );
	assumeArg(he != nullptr,"Get error on gethostbyname : %1")
		.arg(hstrerror(h_errno))
		.end();

	//extract addr
	addr_list = (struct in_addr **) he->h_addr_list;
	
	//convert first one
	char buffer[256];
	strcpy(buffer , inet_ntoa(*addr_list[0]) );

	return buffer;
}

/*******************  FUNCTION  *********************/
std::string Helper::getLocalIP(const std::string & iface)
{
	//locals
	struct ifaddrs * ifAddrStruct=nullptr;
	struct ifaddrs * ifa=nullptr;
	void * tmpAddrPtr=nullptr;
	
	//if iface is empty, search with hostname
	if (iface.empty()) {
		char hostname[1024];
		gethostname(hostname,sizeof(hostname));
		std::string ip = getHostIp(hostname);
		DAQ_DEBUG_ARG("libfabric","Get ip for %1 = %2").arg(hostname).arg(ip).end();
		return ip;
	}

	//get addresses
	getifaddrs(&ifAddrStruct);

	//loop to seatch
	for (ifa = ifAddrStruct; ifa != nullptr; ifa = ifa->ifa_next) {
		//if no address
		if (!ifa->ifa_addr) {
			continue;
		}
		
		//check if requested one
		if (iface == ifa->ifa_name)
		{
			//check type
			if (ifa->ifa_addr->sa_family == AF_INET) { // check it is IP4
				// is a valid IP4 Address
				tmpAddrPtr=&((struct sockaddr_in *)ifa->ifa_addr)->sin_addr;
				char addressBuffer[INET_ADDRSTRLEN];
				inet_ntop(AF_INET, tmpAddrPtr, addressBuffer, INET_ADDRSTRLEN);
				DAQ_DEBUG_ARG("libfabric","Get ip for %1 = %2").arg(iface).arg(addressBuffer).end();
				return addressBuffer; 
			} else if (ifa->ifa_addr->sa_family == AF_INET6) { // check it is IP6
				// is a valid IP6 Address
				tmpAddrPtr=&((struct sockaddr_in6 *)ifa->ifa_addr)->sin6_addr;
				char addressBuffer[INET6_ADDRSTRLEN];
				inet_ntop(AF_INET6, tmpAddrPtr, addressBuffer, INET6_ADDRSTRLEN);
				DAQ_DEBUG_ARG("libfabric","Get ip for %1 = %2").arg(iface).arg(addressBuffer).end();
				return addressBuffer;
			}
		}
	}
	
	//error not found
	DAQ_FATAL_ARG("Fail to find address of interface %1").arg(iface).end();
	return "";
}

/*******************  FUNCTION  *********************/
std::string Helper::getRankIface(const Config& config, int rank)
{
	Json::Value & ifaces = config.getConfig("drivers.libfabric.ifaces");
	assume(ifaces.isArray(),"The ifaces field must be an array !");
	assumeArg(ifaces.size() > (size_t)rank,"Missing definition in drivers.libfabric.ifaces for rank %1").arg(rank).end();
	return ifaces[rank].asString();
}

/*******************  FUNCTION  *********************/
LibFabricMSGEndpoint::LibFabricMSGEndpoint ()
{
	this->cq = nullptr;
	this->ep = nullptr;
	this->eq = nullptr;
}

/*******************  FUNCTION  *********************/
void LibFabricMSGEndpoint::setup ( fid_domain * domain, fid_fabric * fabric, fi_info * fi )
{
	//setup
	struct fi_cq_attr cq_attr;

	//setup some attributes
	memset(&cq_attr, 0, sizeof cq_attr);
	cq_attr.format = FI_CQ_FORMAT_DATA;
	cq_attr.wait_obj = FI_WAIT_NONE;
	cq_attr.size = DAQ_LF_QUEUE_SIZE;
	
	//build endpoint
	int err = fi_endpoint(domain, fi, &(this->ep), nullptr);
	LIBFABRIC_CHECK_STATUS("fi_endpoint",err);
	
	//setup completion queue
	err = fi_cq_open(domain, &cq_attr, &cq, nullptr);
	LIBFABRIC_CHECK_STATUS("fi_cq_open",err);
	
	//bind
	err = fi_ep_bind(this->ep, &(this->cq->fid), FI_RECV | FI_SEND);
	LIBFABRIC_CHECK_STATUS("fi_ep_bind",err);
	
	//event queue
	struct fi_eq_attr cm_attr;
	memset(&cm_attr, 0, sizeof cm_attr);
	cm_attr.wait_obj = FI_WAIT_FD;
	ret = fi_eq_open(fabric, &cm_attr, &(this->eq), nullptr);
	LIBFABRIC_CHECK_STATUS("fi_eq_open", ret);
	
	ret = fi_ep_bind(this->ep, &(this->eq->fid), 0);
	LIBFABRIC_CHECK_STATUS("fi_ep_bind", ret);
}

/*******************  FUNCTION  *********************/
void LibFabricConfig::loadConfig ( const Config& config , int rank)
{
	//load
	std::string epType = config.getAndSetConfigString("drivers.libfabric","ep type","RDM");
	this->iface        = config.getAndSetConfigString("drivers.libfabric","iface","");
	this->firstPort    = config.getAndSetConfigInt("drivers.libfabric","first port",4444);
	this->portRange    = config.getAndSetConfigInt("drivers.libfabric","port range",1024);
	//this->rdmaThresh   = config.getAndSetConfigUInt64("drivers.libfabric","rdma threashold",1024*1024);
	this->providerName = config.getAndSetConfigString("drivers.libfabric","provider name",""); 
	
	//special case with multi-interface per node
	if (this->iface == "{rank}")
	{
		this->iface = Helper::getRankIface(config,rank);
		DAQ_INFO_ARG("Using per rank interface : %1").arg(iface).end();
	}
	
	//convert
	if (epType == "RDM") {
		this->epType = FI_EP_RDM;
	} else if (epType == "MSG") {
		this->epType = FI_EP_MSG;
	} else {
		this->epType = FI_EP_UNSPEC;
		assumeArg (epType.empty(),"Get invalid EP type from config file : %1").arg(epType).end();
	}
}

/*******************  FUNCTION  *********************/
int LibFabricConfig::getServicePort ( int rank )
{
	return firstPort + rank % portRange;
}

/*******************  FUNCTION  *********************/
DriverLibFabric::DriverLibFabric () 
                :DriverSlots<Request>("DriverLibFabric")
{
	this->msgEndpoints = nullptr;
	this->directAddrs = nullptr;
	this->allAddrs = nullptr;
}

/*******************  FUNCTION  *********************/
DriverLibFabric::~DriverLibFabric ()
{
	if (this->msgEndpoints != nullptr)
		delete [] this->msgEndpoints;
}

/*******************  FUNCTION  *********************/
void DriverLibFabric::initialize (const Config & config, Launcher & launcher, int argc, char** argv )
{
	//load config
	lfConfig.loadConfig(config,launcher.getRank());
	
	//parent
	DriverSlots<Request>::initialize(config,launcher,argc,argv);
	
	//setup connections
	int err;
	struct fi_info *hints, *fi;

	//gbl
	nodes = launcher.getWorldSize();
	worldSize = nodes;
	localRank = launcher.getRank();
	
	//get IP
	std::string ip = Helper::getLocalIP(lfConfig.iface);

	directAddrs = new fi_addr_t[nodes];
	allAddrs = new LibFabricMyAddr[nodes];
	if (!directAddrs || !allAddrs) {
		printf("calloc failed\n");
		exit(1);
	}
	hints = fi_allocinfo();
	if (!hints) {
		printf("fi_allocinfo failed\n");
		exit(1);
	}
	hints->caps = FI_MSG | FI_RMA;
	//hints->caps = FI_MSG;
	hints->mode = FI_CONTEXT | FI_RX_CQ_DATA;
	//hints->mode = FI_LOCAL_MR;
	hints->ep_attr->type = lfConfig.epType;
	//provider name if forced
	if (this->lfConfig.providerName.empty() == false)
		hints->fabric_attr->prov_name = strdup(this->lfConfig.providerName.c_str());
	
	//open
	char port[32];
	sprintf(port,"%d",lfConfig.getServicePort(rank));
	DAQ_DEBUG_ARG("libfabric2","Rank is %1").arg(localRank).end();
	err = fi_getinfo(FI_VERSION(1,2), ip.c_str(), port, FI_SOURCE, hints, &fi);
	LIBFABRIC_CHECK_STATUS("fi_getinfo",err);
	
	//check if RDM or MSG
	useRDM = (fi->ep_attr->type == FI_EP_RDM);
	if (fi->ep_attr->type == FI_EP_RDM)
		useRDM = true;
	else if (fi->ep_attr->type == FI_EP_MSG)
		useRDM = false;
	else if (fi->ep_attr->type == FI_EP_UNSPEC)
		useRDM = (lfConfig.epType == FI_EP_RDM);
	else
		DAQ_FATAL("Don't known which protocole to use !");
	
	//print provider
	/*DAQ_INFO_ARG("On rank %1, use fabric provider '%2' and domain '%3'")
				.arg(launcher.getRank())
				.arg(fi->fabric_attr->prov_name)
				.arg(fi->domain_attr->name)
				.end();*/
	this->name = this->name + std::string(":") + std::string(fi->fabric_attr->prov_name)
		+std::string(":") + std::string(useRDM ? "RDM": "MSG");
	
	//RDM || MSG setup
	if (useRDM) {
		initRDM(launcher,fi);
	} else {
		initMSG(launcher,hints,fi);
		setNoAnySource();
	}
	
	//debug
	//this->testDebug();
	
	//check mr mode to use
	mr_mode = fi->domain_attr->mr_mode;

	fi_freeinfo(hints);
	fi_freeinfo(fi);
}

/*******************  FUNCTION  *********************/
void DriverLibFabric::initMSG ( Launcher& launcher, fi_info * hints,fi_info* fi )
{
	//setup listen EP
	initMSGListen(launcher,fi);
	
	//wait for all that listen be ok
	launcher.barrier();
	
	//share addresses
	std::string ip = Helper::getLocalIP(lfConfig.iface);
	launcher.set("hostname",ip);
	launcher.commit();
	launcher.barrier();
	
	//create endpoints
	this->msgEndpoints = new LibFabricMSGEndpoint[launcher.getWorldSize()];
	
	//infos
	int rank = launcher.getRank();
	int nodes = launcher.getWorldSize();
	
	//apply connection technics described in wiki
	//first wait rank connections
	for (int i = 0 ; i < rank ; i++) {
		DAQ_DEBUG("libfabric2","Wait MSG connection");
		initMSGWaitConnection(launcher);
	}
	
	//connect to the nodes-rank other nodes
	for (int i = rank + 1 ; i < nodes ; i++) {
		DAQ_DEBUG_ARG("libfabric2","MSG try to connect on %1").arg(i).end();
		initMSGConnect(launcher,i,hints,fi);
	}
	
	//wait all
	launcher.barrier();
	
	//we can close the listening ep
	fi_close(&listenEndpoint->fid);
	fi_close(&listenQueue->fid);
}

/*******************  FUNCTION  *********************/
void DriverLibFabric::initMSGListen ( Launcher& launcher, fi_info* fi )
{
	//setup fabric
	int ret = fi_fabric(fi->fabric_attr, &fabric, nullptr);
	LIBFABRIC_CHECK_AND_GOTO("fi_fabric",ret,err0);
	assert((fi->mode & FI_MSG_PREFIX) == 0);
	
	//create passive endpoint
	ret = fi_passive_ep(fabric, fi, &listenEndpoint, nullptr);
	LIBFABRIC_CHECK_AND_GOTO("fi_passive_ep",ret,err0);
	
	//setup connection management queue
	struct fi_eq_attr cm_attr;
	memset(&cm_attr, 0, sizeof cm_attr);
	cm_attr.wait_obj = FI_WAIT_FD;
	ret = fi_eq_open(fabric, &cm_attr, &listenQueue, nullptr);
	LIBFABRIC_CHECK_AND_GOTO("fi_eq_open",ret,err1);
	
	//bind it to endpoint
	ret = fi_pep_bind(listenEndpoint, &listenQueue->fid, 0);
	LIBFABRIC_CHECK_AND_GOTO("fi_pep_bind",ret,err2);

	//start listen
	ret = fi_listen(listenEndpoint);
	LIBFABRIC_CHECK_AND_GOTO("fi_listen",ret,err3);
	
	//will set later
	this->domain = nullptr;
	
	//ok exit
	return;

	//errors
	err3:
		fi_close(&listenEndpoint->fid);
	err2:
		fi_close(&listenQueue->fid);
	err1:
		fi_close(&fabric->fid);
	err0:
		fi_freeinfo(fi);
		listenQueue = nullptr;
		listenEndpoint = nullptr;
		fabric = nullptr;
		abort();
}

/*******************  FUNCTION  *********************/
int DriverLibFabric::initMSGWaitConnection ( Launcher& launcher )
{
	//check mode
	assume(useRDM == false,"You try to use waitConnection() while libfabric is started in RDM mode !");
	
	//some vars
	struct fi_eq_cm_entry entry;
	uint32_t event;
	struct fi_info *info = nullptr;
	ssize_t rd;
	int ret;
	fid_mr * mr;
	void * mrDesc;
	LibFabricMSGEndpoint tmpEP;

	//wait on queue
	rd = fi_eq_sread(listenQueue, &event, &entry, sizeof entry, -1, 0);
	if (rd != sizeof entry)
		LIBFABRIC_EQ_FATAL_ERROR("fi_eq_sread",listenQueue);

	//check
	info = entry.info;
	if (event != FI_CONNREQ) {
		DAQ_ERROR_ARG("Unexpected CM event %1").arg(event).end();
		ret = -FI_EOTHER;
		goto err1;
	}
	
	//check mr mode to use
	mr_mode = info->domain_attr->mr_mode;

	//build domain
	if (domain == nullptr) {
		ret = fi_domain(fabric, info, &domain, nullptr);
		LIBFABRIC_CHECK_AND_GOTO("fi_domain",ret,err1);
	
		//register segment
		this->registerSegment((void*)&ackListen,sizeof(ackListen));
	}
	
	//accept
	ret = initMSGGetConnectionAccept(tmpEP,launcher,info);
	assumeArg(ret == 0,"Invalid status returned from MESGGetConnectionAccept : %1").arg(ret).end();
	
// 	//wait to get first message with remote rank ID
	mr = getFidMR(&ackListen,sizeof(ackListen));
	assume(mr != nullptr,"Fail to find segment in list, might be not registered, please use registerSegment() before using addresses to send/recv");
	mrDesc = fi_mr_desc(mr);
	
	//wait recv
	fi_context context;
	ret = fi_recv(tmpEP.ep, &ackListen,sizeof(ackListen), mrDesc, 0, &context);
	LIBFABRIC_CHECK_STATUS("fi_recv", ret);
	Helper::waitForCompletion(tmpEP.cq,1);

	//debug
	DAQ_DEBUG_ARG("libfabric","Ack rank is : %1\n").arg(ackListen.rank).end();
	
	//setup local rank endpoint
	assert(msgEndpoints[ackListen.rank].ep == nullptr);
	msgEndpoints[ackListen.rank] = tmpEP;

	//free tmp
	fi_freeinfo(info);
	return 0;

err1:
	fi_reject(listenEndpoint, info->handle, nullptr, 0);
	fi_freeinfo(info);
	return ret;
}

/*******************  FUNCTION  *********************/
int DriverLibFabric::initMSGConnect ( Launcher & launcher, int rank, fi_info * hints, fi_info * fi )
{
	//some checks
	assert(rank >= 0 && rank < worldSize);
	assert(fi != nullptr);
	
	//vars
	struct fi_eq_cm_entry entry;
	uint32_t event;
	ssize_t rd;
	int ret;
	fid_mr * mr;
	void * mrDesc;
	fi_context context;
	
	//locals
	LibFabricMSGEndpoint & tmpEP = msgEndpoints[rank];
	
	//errors
	if (tmpEP.ep != nullptr) {
		DAQ_WARNING_ARG("Enpoint %1 already active on %1, do nothing.").arg(rank).arg(launcher.getRank()).end();
		return 0;
	}
	
	//get hostname
	//TODO replace which more lowlevel addr in hints
	std::string hostname = launcher.get("hostname",rank);
	char serviceStr[32];
	sprintf(serviceStr,"%d",lfConfig.getServicePort(rank));
	
	//get info
	ret = fi_getinfo(LIBFABRIC_VERSION, hostname.c_str(), serviceStr, 0, hints, &fi);
	LIBFABRIC_CHECK_AND_GOTO("fi_getinfo",ret,err0);
	
	//setup fabric
	if (fabric == nullptr) {
		ret = fi_fabric(fi->fabric_attr, &fabric, nullptr);
		LIBFABRIC_CHECK_AND_GOTO("fi_fabric",ret,err1);
	}
	
	//check mr mode
	mr_mode = fi->domain_attr->mr_mode;

	//setup domain
	if (domain == nullptr) {
		ret = fi_domain(fabric, fi, &domain, nullptr);
		LIBFABRIC_CHECK_AND_GOTO("fi_domain",ret,err2);
	}
	
	//register segments use for connexion setup
	if (getFidMR(&ackConnect,sizeof(ackConnect)) == nullptr)
		this->registerSegment(&ackConnect,sizeof(ackConnect));
	
	//setup
	tmpEP.setup(domain,fabric,fi);

	//etablish connection
	ret = fi_connect(tmpEP.ep, fi->dest_addr, nullptr, 0);
	LIBFABRIC_CHECK_AND_GOTO("fi_connect", ret,err4);

	//check event in event queue
	rd = fi_eq_sread(tmpEP.eq, &event, &entry, sizeof entry, -1, 0);
	if (rd != sizeof entry) {
		DAQ_ERROR_ARG("%1").arg(Helper::getEqErrString(tmpEP.eq,"fi_eq_sread")).end();
		ret = (int) rd;
		goto err4;
	}

	//check event type
	if (event != FI_CONNECTED || entry.fid != &(tmpEP.ep->fid)) {
		DAQ_ERROR_ARG("Unexpected CM event %1 fid %2 (ep %3)").arg(event).arg(entry.fid).arg(tmpEP.ep).end();
		ret = -FI_EOTHER;
		goto err4;
	}
	
	//send rank
	ackConnect.rank = launcher.getRank();
	mr = getFidMR(&ackConnect,sizeof(ackConnect));
	assume(mr != nullptr,"Fail to find segment in list, might be not registered, please use registerSegment() before using addresses to send/recv");
	mrDesc = fi_mr_desc(mr);
	
	//send and wait
	ret = fi_send(tmpEP.ep, &ackConnect,sizeof(ackConnect), mrDesc, 0, &context);
	LIBFABRIC_CHECK_STATUS("fi_send", ret);
	Helper::waitForCompletion(tmpEP.cq,1);

	//ok
	return 0;
	
	//manage errors
err4:
// 	free_ep_res();
	fi_close(&domain->fid);
err2:
	fi_close(&fabric->fid);
err1:
	fi_freeinfo(fi);
err0:
	domain =nullptr;
	fabric = nullptr;
	abort();
}

/*******************  FUNCTION  *********************/
int DriverLibFabric::initMSGGetConnectionAccept (LibFabricMSGEndpoint & tmpEP, Launcher& launcher, fi_info* fi )
{
	//some checks
	assert(fabric != nullptr);
	assert(domain != nullptr);
	assert(fi != nullptr);
	
	//vars
	int ret;
	ssize_t rd;
	uint32_t event;
	struct fi_eq_cm_entry entry;
	
	//locals
	tmpEP.setup(domain,fabric,fi);

	//accept the connection
	ret = fi_accept(tmpEP.ep, nullptr, 0);
	LIBFABRIC_CHECK_STATUS("fi_accept", ret);

	//check in event queue
	rd = fi_eq_sread(tmpEP.eq, &event, &entry, sizeof entry, -1, 0);
	if (rd != sizeof entry)
		LIBFABRIC_EQ_FATAL_ERROR("fi_eq_sread",tmpEP.eq);

	//check event type to be correct
	if (event != FI_CONNECTED || entry.fid != &(tmpEP.ep->fid)) {
		DAQ_ERROR_ARG("Unexpected CM event %1 != %2 fid %3 (ep %4)").arg(event).arg(FI_CONNECTED).arg(entry.fid).arg(&(tmpEP.ep->fid)).end();
		ret = -FI_EOTHER;
		return ret;
	}
	
	return 0;
}

/*******************  FUNCTION  *********************/
void DriverLibFabric::initRDM ( Launcher & launcher, fi_info * fi )
{
	struct fi_cq_attr cq_attr;
	struct fi_av_attr av_attr;
	
	//init
	memset(&cq_attr, 0, sizeof(cq_attr));
	memset(&av_attr, 0, sizeof(av_attr));
	myAddrLen = 0;
	
	//setup fabric
	int err = fi_fabric(fi->fabric_attr, &fabric, nullptr);
	LIBFABRIC_CHECK_STATUS("fi_fabric",err);
	
	//setup domain
	err = fi_domain(fabric, fi, &domain, nullptr);
	LIBFABRIC_CHECK_STATUS("fi_domain",err);
	
	//setup attr
	cq_attr.format = FI_CQ_FORMAT_DATA;
	cq_attr.wait_obj = FI_WAIT_NONE;
	cq_attr.size = DAQ_LF_QUEUE_SIZE;
	
	//setup cq read
	err = fi_cq_open(domain, &cq_attr, &cq, nullptr);
	LIBFABRIC_CHECK_STATUS("fi_cq_open",err);
	
	err = fi_endpoint(domain, fi, &ep, nullptr);
	LIBFABRIC_CHECK_STATUS("fi_endpoint",err);
	
	err = fi_ep_bind(ep, (struct fid *)cq, FI_RECV | FI_SEND);
	LIBFABRIC_CHECK_STATUS("fi_ep_bind",err);
	
	err = fi_enable(ep);
	LIBFABRIC_CHECK_STATUS("fi_enable", err);
	
	av_attr.type = FI_AV_MAP;
	err = fi_av_open(domain, &av_attr, &av, nullptr);
	LIBFABRIC_CHECK_STATUS("fi_av_open",err);
	
	//bind
	err = fi_ep_bind(ep, &av->fid, 0);
	LIBFABRIC_CHECK_STATUS("fi_ep_bind", err);
	
	//get size (addrlen 0 say to not write anything into localAddr
	err = fi_getname(&ep->fid, nullptr, &myAddrLen);
	DAQ_DEBUG_ARG("libfabric2","Addr size = %1").arg(myAddrLen).end();
	//LIBFABRIC_CHECK_STATUS("fi_getname",err);
	
	err = fi_getname(&ep->fid, &myAddr, &myAddrLen);
	LIBFABRIC_CHECK_STATUS("fi_getname",err);

	//exchange addresses and fill allAddrs
	fillAddrVector(launcher);

	//wait all
	launcher.barrier();
	
	//enable
	err = fi_enable(ep);
	LIBFABRIC_CHECK_STATUS("fi_enable", err);
}

/*******************  FUNCTION  *********************/
void DriverLibFabric::fillAddrVector ( Launcher & launcher )
{
	//store
	std::string local = Helper::convertAddrToHex(myAddr.raw,myAddrLen);
	DAQ_DEBUG_ARG("libfabric2","Local is %1 == %2").arg(local).arg(*(size_t *)&myAddr).end();
	launcher.set("addr",local);
	
	//exchange
	launcher.commit();
	launcher.barrier();
	
	//merge
	for (int i = 0 ; i < launcher.getWorldSize() ; i++) {
		std::string cur = launcher.get("addr",i);
		Helper::convertAddrFromHex(allAddrs[i].raw,myAddrLen,cur);
		DAQ_DEBUG_ARG("libfabric2","Setup vector entry %1 = %2 => %3").arg(i).arg(cur).arg(*(size_t *)&allAddrs[i]).end();
	}
	
	//insert one by one except local otherwise get error on top of PSM
	int nodes = launcher.getWorldSize();
	for (int i = 0 ; i < nodes ; i++) {
		if (i != rank) {
			int err = fi_av_insert(av, &allAddrs[i], 1, &directAddrs[i], 0, nullptr);
			assumeArg(err == 1,"fi_av_insert returns err %1 instead of 2").arg(err).arg(nodes).end();
		}
	}
}

/*******************  FUNCTION  *********************/
void DriverLibFabric::finalize ()
{
	DriverSlots<Request>::finalize();
	if (useRDM) {
		fi_close((fid_t)ep);
		fi_close((fid_t)av);
		fi_close((fid_t)cq);
	} else {
		//already closed but in case one day....
		//fi_close((fid_t)listenEndpoint);
		//fi_close((fid_t)listenQueue);
		//close the eps
		for (int i = 0 ; i < nodes ; i++)
		{
			if (i != localRank)
			{
				fi_close((fid_t)msgEndpoints[i].ep);
				fi_close((fid_t)msgEndpoints[i].eq);
				fi_close((fid_t)msgEndpoints[i].cq);
			}
		}
	}
	
	fi_close((fid_t)domain);
	fi_close((fid_t)fabric);
}

/*******************  FUNCTION  *********************/
void DriverLibFabric::doInitRequest ( Request& request )
{
	//nothing to do
}

/*******************  FUNCTION  *********************/
void DriverLibFabric::registerSegment ( void* ptr, size_t size )
{
	//checks
	assert(domain != nullptr);
	assert(ptr != nullptr);
	assert(size > 0);
	
	//setup
	MemoryRegion region;
	region.ptr = ptr;
	region.size = size;
	region.mr = nullptr;
	
	//debug
	DAQ_DEBUG_ARG("libfabric2","Register new segment into driver : %1 of size %2 and id %3")
		.arg(ptr)
		.arg(size)
		.arg(segments.size())
		.end();
	
	//check if already registred
	if (getFidMR(ptr,size) != nullptr)
		return;

	//reg into OFI
	int ret = fi_mr_reg(domain, ptr, size, FI_REMOTE_WRITE, 0, segments.size(), 0, &(region.mr), nullptr);
	LIBFABRIC_CHECK_STATUS("fi_mr_reg",ret);
	
	//reg
	segments.push_back(region);
}

/*******************  FUNCTION  *********************/
fid_mr* DriverLibFabric::getFidMR ( void* ptr, size_t size )
{
	MemoryRegion * region = getMR(ptr,size);
	if (region == nullptr)
		return nullptr;
	else
		return region->mr;
}

/*******************  FUNCTION  *********************/
MemoryRegion* DriverLibFabric::getMR ( void* ptr, size_t size )
{
	//search
	MemoryRegion * mr = nullptr;
	for (LibfabricMemoryRegionVector::iterator it = segments.begin() ; it != segments.end() ; ++it) {
		if (it->ptr <= ptr && (char*)it->ptr + it->size > ptr) {
			if ((char*)ptr+size > (char*)it->ptr + it->size)
				DAQ_WARNING("Caution, a segment from libfabric not completetly fit with the request which is larger.");
			mr = &(*it);
			break;
		}
	}
	return mr;
}

/*******************  FUNCTION  *********************/
RemoteIOV DriverLibFabric::getRemoteIOV ( void* addr, size_t size )
{
	RemoteIOV lfiov = getIOV(addr,size);
	DAQ::RemoteIOV iov = {lfiov.addr,lfiov.key};
	return iov;
}

/*******************  FUNCTION  *********************/
RemoteIOV DriverLibFabric::getIOV ( void* ptr, size_t size )
{
	//erros
	assert(ptr != nullptr);
	assert(size > 0);
	
	//vars
	RemoteIOV iov;
	
	//get mr
	MemoryRegion * mr = getMR(ptr,size);
	assert(mr != nullptr);

	//setup
	iov.addr = (mr_mode == FI_MR_SCALABLE) ? (void*)((char*)ptr - (char*)mr->ptr) : ptr;
	iov.key = fi_mr_key(mr->mr);
	
	if (mr_mode == FI_MR_SCALABLE && ptr != mr->ptr)
		DAQ_DEBUG_ARG("libfabric2","Use offset inside segment for %1 (key=%2, ptr=%3)").arg(iov.addr).arg(iov.key).arg(ptr).end();
	
	return iov;
}

/*******************  FUNCTION  *********************/
void DriverLibFabric::doPostCommandRecv ( DAQ::UnitCommand& command, Request& request, int rank )
{
	DAQ_DEBUG("libfabric2","Setup get command");
	if (useRDM) {
		assert(rank == -1);
		//unspec & nullptr for ANY_SOURCE
		fi_recv(ep,&command,sizeof(UnitCommand),nullptr,FI_ADDR_UNSPEC,&request);
	} else {
		assert(msgEndpoints != nullptr);
		fi_recv(msgEndpoints[rank].ep,&command,sizeof(UnitCommand),nullptr,FI_ADDR_UNSPEC,&request);
	}
}

/*******************  FUNCTION  *********************/
void DriverLibFabric::doRepublishCommandRecv ( DAQ::UnitCommand& command, Request& request, int rank )
{
	DAQ_DEBUG("libfabric2","Republish command request");
	this->doPostCommandRecv(command,request,rank);
}

/*******************  FUNCTION  *********************/
void DriverLibFabric::unregisterSegment(void* addr, size_t size)
{
  //checks
  assert(addr != nullptr);
  assert(size > 0);

  //search (code similar to the getMR function, but here we need also the position inside the vector)
  auto it = std::begin(segments);
  auto end = std::end(segments);
  for (; it != end; ++it) {
    if (it->ptr <= addr && (char*) it->ptr + it->size > addr) {
      if ((char*) addr + size > (char*) it->ptr + it->size)
        DAQ_WARNING(
            "Caution, a segment from libfabric not completely fit with the request which is larger.");
      break;
    }
  }

  //check if registred
  if (it == end) {
    DAQ_WARNING("Trying to unregister a not registered segment.");
    return;
  }

  MemoryRegion const& region = *it;

  if (fi_close(&(region.mr->fid)) == -FI_EBUSY) {
    DAQ_WARNING(
        "Probably unable to unregister busy memory region. Continue anyway.");
  }

  segments.erase(it);
}

/*******************  FUNCTION  *********************/
void DriverLibFabric::doSendCommand ( UnitCommand& command, Request& request )
{
	DAQ_DEBUG_ARG("libfabric2","Send command => %1").arg(command.type).end();
	if (useRDM)
		fi_send(ep,&command,sizeof(UnitCommand),nullptr,directAddrs[command.dest.rank],&request);
	else
		fi_send(msgEndpoints[command.dest.rank].ep,&command,sizeof(UnitCommand),nullptr,directAddrs[command.dest.rank],&request);
}

/*******************  FUNCTION  *********************/
void DriverLibFabric::doRdmaWrite ( int targetRank, int uuid, void* src, void* dest, uint64_t destKey, size_t size, Request& request )
{
	//compute tag
	uint64_t fulltag = (uint64_t)((uint32_t)localRank) + (((uint64_t)uuid) << 16);
	
	//check
	assert(localRank != -1);
	DAQ_DEBUG_ARG("libfabric2","Send RDMA tag = %1, fulltag = %2, ptr = %3").arg(uuid).arg(fulltag).arg(dest).end();
	
	//get addr
	fi_addr_t addr = 0;
	if (targetRank >= 0 && directAddrs != nullptr)
		addr = directAddrs[targetRank];
	
	//get local mr
	void * mrDesc = nullptr;
	fid_mr * mr = getFidMR(src,size);
	if (mr != nullptr)
		mrDesc = fi_mr_desc(mr);
	
	//do write with remote cq ack
	int ret;
	if (useRDM)
		ret = fi_writedata(ep, src, size, mrDesc, fulltag,addr, (uint64_t)dest, destKey,&request);
	else
		ret = fi_writedata(msgEndpoints[targetRank].ep, src, size, mrDesc, fulltag,addr, (uint64_t)dest, destKey,&request);
	LIBFABRIC_CHECK_STATUS("fi_writedata", ret);
}

/*******************  FUNCTION  *********************/
bool DriverLibFabric::checkRdmaPending ( int targetRank, int uuid, void* ptr, size_t size, Request& request )
{
	//compute
	uint64_t fulltag = (uint64_t)((uint32_t)targetRank) + (((uint64_t)uuid) << 16);
	assert(targetRank != 0);
	
	//check if match with pending
	for (RDMAPendingList::iterator it = rdmaPending.begin() ; it != rdmaPending.end() ; ++it) {
		if (it->tag == fulltag) {
			if (it->ptr != ptr && it->ptr != 0)
				DAQ_WARNING_ARG("Don't get same pointer on pending RDMA recv (%1 != %2)").arg(it->ptr).arg(ptr).end();
			if (it->size > size)
				DAQ_WARNING_ARG("Don't get same size on pending RDMA recv (%1 != %2)").arg(it->size).arg(size).end();
			//send back directly to the caller
			DAQ_DEBUG_ARG("libfabric2","Match pending RDMA, call the slot function (%1)").arg(fulltag).end();
			rdmaPending.erase(it);
			rdmaWriteAlreadyFinished(request);
			return true;
		}
	}
	
	return false;
}

/*******************  FUNCTION  *********************/
void DriverLibFabric::doPostRdmaRecv ( int rank, int uuid, void* ptr, size_t size, Request& request )
{
	//setup context
	RDMAContext context;
	context.request = &request;
	context.ptr = ptr;
	context.size = size;
	context.tag = uuid;
	uint64_t fulltag = (uint64_t)((uint32_t)rank) + (((uint64_t)uuid) << 16);
	assert(rank != 0);
	
	//register to wait list if not in pending
	if (checkRdmaPending(rank,uuid,ptr,size,request) == false) {
		assumeArg(this->rdmaContextMap.find(fulltag) == this->rdmaContextMap.end(),"Tag already in use %1:%2").arg(fulltag).arg(uuid).end();
		this->rdmaContextMap[fulltag] = context;
	}
}

/*******************  FUNCTION  *********************/
int DriverLibFabric::doTest ( Request* requests, size_t count, bool wait )
{
	if (useRDM)
		return doTestRDM(requests,count,wait);
	else
		return doTestMSG(requests,count,wait);
}

/*******************  FUNCTION  *********************/
int DriverLibFabric::doTestMSG ( Request* requests, size_t count, bool wait )
{
	int ret;
	struct fi_cq_data_entry comp;
	
	do {
		for (int i = 0 ; i < nodes ; i++) {
			if (i != localRank) {
				//get cq entry
				ret = fi_cq_read(msgEndpoints[i].cq, &comp, 1);
				
				//if get one
				if (ret > 0) {
					DAQ_DEBUG("libfabric2v","Finish comm");
					fi_context * context = getContext(comp);
					if (context != nullptr)
					{
						if (context >= requests && context < requests + count)
						{
							return context - requests;
						} else {
							assert(useRDM == false);
							this->noAnySourceCmdFinsished(*context);
							//we can continue but prefer to get sans event scheduling that RDM
							return -1;
						}
					}
				} else if (ret < 0 && ret != -FI_EAGAIN) {
					if (ret == -FI_EAVAIL) {
						LIBFABRIC_CQ_FATAL_ERROR("fi_cq_read",cq);
					} else {
						LIBFABRIC_CHECK_STATUS("fi_cq_read", ret);
					}
				} else {
					DAQ_DEBUG("libfabric2v","Get EAGAIN");
				}
			}
		}
	} while(wait);

	return -1;
}

/*******************  FUNCTION  *********************/
int DriverLibFabric::doTestRDM ( Request* requests, size_t count, bool wait )
{
	int ret;
	struct fi_cq_data_entry comp;
	
	do {
		//get cq entry
		ret = fi_cq_read(cq, &comp, 1);
		
		//if get one
		if (ret > 0) {
			DAQ_DEBUG("libfabric2v","Finish comm");
			fi_context * context = getContext(comp);
			if (context != nullptr)
			{
				int id = context - requests;
				DAQ_DEBUG_ARG("libfabric2","Get context id = %1").arg(id).end();
				assumeArg(id >= 0 && id < (int)count,"Invalid id : %1").arg(id).end();
				return id;
			}
		} else if (ret < 0 && ret != -FI_EAGAIN) {
			if (ret == -FI_EAVAIL) {
				LIBFABRIC_CQ_FATAL_ERROR("fi_cq_read",cq);
			} else {
				LIBFABRIC_CHECK_STATUS("fi_cq_read", ret);
			}
		} else {
			DAQ_DEBUG("libfabric2v","Get EAGAIN");
		}
	} while(wait);

	return -1;
}

/*******************  FUNCTION  *********************/
int DriverLibFabric::doWaitRequests ( Request* requests, size_t size )
{
	return doTest(requests,size,true);
}

/*******************  FUNCTION  *********************/
uint64_t DriverLibFabric::getSegmentRemoteKey ( void* addr, size_t size )
{
	DAQ_FATAL("Must not be called");
	return -1;
}

/*******************  FUNCTION  *********************/
fi_context* DriverLibFabric::getContext ( fi_cq_data_entry& comp )
{
	if (comp.op_context != nullptr)
	{
		DAQ_DEBUG("libfabric2","Is std send/recv");
		return (fi_context*)comp.op_context;
	} else if (comp.flags & FI_REMOTE_CQ_DATA) {
		DAQ_DEBUG("libfabric2","Finish RDMA");
		RDMAContextMap::iterator it = rdmaContextMap.find(comp.data);
		
		assumeArg((comp.data & ((1UL<<16)-1)) < (uint64_t)worldSize,"Get invalid rank in fulltag %1 >= %2")
			.arg(comp.data & ((1UL<<16)-1))
			.arg(worldSize)
			.end();
		
		//checks
		if (it == rdmaContextMap.end()) {
			//warn
			DAQ_DEBUG_ARG("libfabric2","Fail to get rdmaContextMap entry for tag %1 (%2:%3), use pending storage for future matching. "
				"Please, try to increase number of RDMA recv slots to match at least with sched.parallel and credits !")
				.arg(comp.data)
				.arg(comp.data & ~((uint64_t)(1UL<<16)-1))
				.arg(comp.data & ((uint64_t)(1UL<<16)-1))
				.end();
			
			//setup pending
			RDMAContext context;
			context.ptr = comp.buf;
			context.size = comp.len;
			context.tag = comp.data;
			context.request = nullptr;
			
			//push to pending list
			rdmaPending.push_back(context);

			//get nothing
			return nullptr;
		} else {
			DAQ_DEBUG_ARG("libfabric2","Get RDMA write finished on recv side, tag = %1").arg(comp.data).end();
		}

		//Ok, need to update the API as ptr is not true on PSM due to relativ addresses.
		/*assumeArg(comp.buf == it->second.ptr,"Get mismatch on completion event addresses, expect %1, get %2")
			.arg(it->second.ptr)
			.arg(comp.buf)
			.end();*/
		assumeArg(comp.len <= it->second.size,"Get mismatch on completion event size expect %1, get %2")
			.arg(it->second.size)
			.arg(comp.len)
			.end();

		//Extract request
		fi_context * ret = it->second.request;
		
		//remove
		rdmaContextMap.erase(it);
		
		//ok return
		return ret;
	} else {
		DAQ_FATAL_ARG("Get invalid completion event in endpoint queue (%1) !").arg(comp.flags).end();
		return nullptr;
	}
}

/*******************  FUNCTION  *********************/
void DriverLibFabric::testDebug ( Launcher & launcher )
{
	launcher.barrier();
	
	DAQ_DEBUG("libfabric2","Start exchange message");

	//test exchange for debug
	char buffer[1024];
	struct fi_context context;
	if (rank == 0)
		fi_recv(ep,buffer,sizeof(buffer),nullptr,FI_ADDR_UNSPEC,&context);
	else if (rank == 1)
		fi_send(ep,buffer,sizeof(buffer),nullptr,directAddrs[0],&context);
	
	if (rank <= 1) {
		DAQ_DEBUG("libfabric2","Wait exchange message");
		struct fi_cq_entry cqe;
		while(fi_cq_read(cq,&cqe,1)== -FI_EAGAIN);
	}
	
	DAQ_DEBUG("libfabric2","Finish exchange message");
	
	launcher.barrier();
	
	DAQ_DEBUG("libfabric2","Post-barrier");
}

}
