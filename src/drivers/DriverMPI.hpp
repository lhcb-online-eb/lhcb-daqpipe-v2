/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_DRIVER_MPI_HPP
#define DAQ_DRIVER_MPI_HPP

/********************  HEADERS  *********************/
#include "DriverSlots.hpp"
#include <mpi.h>

/********************  NAMESPACE  *******************/
namespace DAQ
{

/*********************  CLASS  **********************/
class DriverMPI : public DriverSlots<MPI_Request>
{
	public:
		DriverMPI();
		virtual ~DriverMPI();
		virtual void registerSegment(void * addr,size_t size);
		virtual void unregisterSegment(void * addr,size_t size);
		virtual uint64_t getSegmentRemoteKey ( void* addr, size_t size );
		virtual RemoteIOV getRemoteIOV(void * addr, size_t size);
	protected:
		//TO export in outter claass by splitting slots/impl
		void doSendCommand ( UnitCommand& command, MPI_Request& request );
		void doPostCommandRecv ( DAQ::UnitCommand& command, MPI_Request& request, int rank );
		void doRdmaWrite ( int rank, int uuid, void* src,void * dest, uint64_t destKey, size_t size, MPI_Request& request );
		void doPostRdmaRecv ( int rank, int uuid, void* ptr, size_t size, MPI_Request& request );
		int doTest ( MPI_Request* requests, size_t count, bool useWait );
		void doRepublishCommandRecv(DAQ::UnitCommand& command, MPI_Request& request, int rank );
		void doInitRequest ( MPI_Request& request );
};

}

#endif //DAQ_DRIVER_MPI_HPP
