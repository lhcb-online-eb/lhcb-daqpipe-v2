/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_COMM_TRACER_HPP
#define DAQ_COMM_TRACER_HPP

/********************  HEADERS  *********************/
#include <array>
#include <stdint.h>
#include <sys/time.h>
#include "common/Config.hpp"
#include "common/Histogram.hpp"
#ifdef HAVE_HTOPML
	#include <htopml/TypeToJson.h>
#endif //HAVE_HTOPML

/********************  MACROS  **********************/
#define COMM_TRACKER_SIZE 1024

/********************  NAMESPACE  *******************/
namespace DAQ
{

/********************  STRUCT  **********************/
/**
 * Store statistics about a given communication.
**/
struct CommStats
{
	/** Start time of the communication (when pushed into driver) **/
	uint64_t start;
	/** End time of communicaton (when send back the notification command) **/
	uint64_t end;
	/** Size of the message to be transmitted. **/
	size_t size;
	/** Define if the communication if a command or not (data) **/
	bool isCommand;
	/** Degine if it is a send or a recieve. **/
	bool isSend;
	CommStats()
			: start(0),
				end(0),
				size(0),
				isCommand(false),
				isSend(false) {

	}
};

/*********************  TYPES  **********************/
/**
 * Define a map indexed by a pointer to the context of the communication.
 * and storing the communications stats.
**/
typedef std::map<void*,CommStats> CommMap;

/*********************  CLASS  **********************/
/**
 * Object used to track the communication and report scheduling as a dump file or on-fly
 * thanks to htopml.
 * @brief Communication tracer.
**/
class CommTracer
{
	public:
		CommTracer();
		~CommTracer();
		void startComm( void* id, size_t size, bool isCommand, bool isSend );
		void finishComm(void * id);
		void save(const char * filename);
		#ifdef HAVE_HTOPML
			friend void convertToJson(htopml::JsonState & json,const CommTracer & tracer);
		#endif //HAVE_HTOPML
	protected:
		/** Keep track of the active communications. **/
		CommMap actives;
		/** Use a circular buffer to store the last N communications done. **/
		std::array<CommStats, COMM_TRACKER_SIZE> log {};
		/** Cursor in the log **/
		int cursor;
		/** Used to build statistics about communication bandwidth. **/
		Histogram histogram;
};

double getTicksPerSecond();
}

#endif //DAQ_COMM_TRACER_HPP
