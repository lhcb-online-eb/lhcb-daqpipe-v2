/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_DRIVER_UDP_HPP
#define DAQ_DRIVER_UDP_HPP

/********************  HEADERS  *********************/
#include <map>
#include "units/Unit.hpp"
#include "Driver.hpp"
#include <thread>
#include <gochannels/GoChannel.hpp>
#include <functional>
#include <list>
#include <mutex>

/********************  NAMESPACE  *******************/
namespace DAQ
{

/********************  STRUCT  **********************/
struct UdpInitHandshake
{
	int rank;
};

/********************  STRUCT  **********************/
struct UdpConfig
{
	int portBase;
	int portRange;
	bool useIpV6;
	int * cmdFd;
	std::string cmdIface;
	std::string rdmaIface;
	int rdmaChannels;
	int rdmaThreadNumber;
	int cmdThreadNumber;
	int packetSize;
	int maxPackets;
	size_t delay;
	size_t kernelBuf;
	void loadFromConfig(const Config & config);
};

/********************  STRUCT  **********************/
struct UdpRdmaSeg
{
	char * addr;
	size_t size;
};

/********************  STRUCT  **********************/
struct UdpRdmaJob
{
	void * baseAddr;
	void * remoteAddr;
	size_t size;
	uint64_t guuid;
	int targetRank;
	int missing[DAQ_UDP_RDMA_MISSING];
	int missingCnt;
	int blocks;
};

/********************  STRUCT  **********************/
struct UpdRdmaMsgHdr
{
	void * destAddr;
	size_t size;
	uint64_t guuid;
	int srcRank;
	int id;
};

/********************  STRUCT  **********************/
struct UdmRdmaState
{
	UnitCommand ack;
	UdpRdmaJob job;
};

/*********************  TYPES  **********************/
typedef std::map<uint64_t,UnitCommand> UdpRdmaNotifMap;
typedef std::vector<UdpRdmaSeg> UdpRdmaSegVector;

/*********************  CLASS  **********************/
class DriverUDP : public Driver
{
	public:
		DriverUDP();
		virtual ~DriverUDP();
		virtual void initialize( const DAQ::Config& config, DAQ::Launcher& launcher, int argc, char** argv );
		virtual void finalize();
		virtual void registerSegment(void * addr,size_t size);
		virtual void unregisterSegment(void * addr,size_t size);
		virtual uint64_t getSegmentRemoteKey ( void* addr, size_t size );
		virtual void postWaitRemoteRDMAWrite ( Unit* srcUnit, int rank, int unitId, void* ptr, size_t size, int uuid, const DAQ::UnitCommand& postCommand );
		virtual void rdmaWrite ( Unit* srcUnit,int rank, int unitId, void* src, void* dest, uint64_t destKey, size_t size, int uuid, const UnitCommand& postCommand );
		virtual void postCommandRecv();
		virtual void sendRemoteCommand(const UnitCommand & command);
		virtual RemoteIOV getRemoteIOV ( void* addr, size_t size );
		virtual void startThreads ();
		virtual void stop ();
		virtual void startNoThreads ();
		virtual void updateNoThread ();
	private:
		int getPort(int rank, int id);
		int doConnect( DAQ::Launcher& launcher, int rank);
		void doConnectAll(DAQ::Launcher& launcher);
		void doAccept( Launcher &launcher,int listenSocket );
		void doAcceptAll(Launcher &launcher,int listenSocket);
		void setupEpolls( DAQ::Launcher& launcher );
		bool checkRdmaReg(void * addr,size_t size);
		void cmdRecvMain(int threadId);
		void cmdSendMain(int threadId);
		void rdmaRecvMain(int threadId);
		void rdmaSendMain(int threadId);
		void sendRdmaMessage( const DAQ::UdpRdmaJob& job, int threadId, int id );
		void filterCommand(UnitCommand & command);
		void markRecv(uint64_t uuid,int id);
		bool checkFull( uint64_t uuid, int blocks, DAQ::ArgUdpResend& missing );
		void sendClosePacket();
		uint64_t getGuuid(int rank, int uuid);
	private:
		UdpConfig udpConfig;
		int * udpRdmaSockets;
		int * cmdFd;
		int * udpRdmaSendSockets;
		UdpRdmaNotifMap notifMap;
		UdpRdmaSegVector rdmaSegs;
		std::mutex lock;
		GoChannel<UdpRdmaJob> rdmaChan;
		GoChannel<std::function<void()>> * cmdSendChan;
		bool threadRunning;
		std::thread* cmdRecvThread;
		std::thread* cmdSendThread;
		std::thread* rdmaRecvThread;
		std::thread* rdmaSendThread;
		int * epollCmdFd;
		std::map<int,std::string> addresses;
		std::map<uint64_t,UdmRdmaState> rdmaPostCommands;
		std::map<uint64_t,bool*> stateMap;
		std::mutex stateMapLock;
};

}

#endif //DAQ_DRIVER_UDP_HPP
