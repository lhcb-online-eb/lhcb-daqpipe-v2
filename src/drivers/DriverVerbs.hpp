/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.1.0
             DATE     : 12/2017
             AUTHOR   : Krawczyk Rafal - CERN/WUT
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_DRIVER_LIBVERBS_HPP
#define DAQ_DRIVER_LIBVERBS_HPP

/********************  HEADERS  *********************/
#include "DriverSlots.hpp"
#include <rdma_cma.h>
#include <infiniband/verbs.h>

/********************  NAMESPACE  *******************/
namespace DAQ
{

/********************  STRUCT  **********************/
struct VerbsRequest
{
	bool is_recs_req; //true for receive request, false for send request
	bool is_rdma;
	struct ibv_send_wr send_wr;
	struct ibv_send_wr *bad_send_wr;
	struct ibv_recv_wr recv_wr;
	struct ibv_recv_wr *bad_recv_wr;
	struct ibv_sge  sge;
	DAQ::UnitCommand * command;
};

/********************  STRUCT  **********************/
struct MemoryRegion
{
	void * ptr;
	size_t size;
	ibv_mr * mr;

};

/*********************  TYPES  **********************/
typedef std::vector<MemoryRegion> verbsMemoryRegionVector;


/*********************  CLASS  **********************/
class DriverVerbs : public DriverSlots<VerbsRequest>
{
	public:
		///INHERITED METHODS - same as for any inheriting from DriverSlots
		DriverVerbs ();
		virtual ~DriverVerbs ();
		virtual void initialize ( const DAQ::Config& config, DAQ::Launcher& launcher, int argc, char** argv );
		virtual void finalize ();
		virtual void registerSegment ( void * addr, size_t size );
		virtual void unregisterSegment ( void * addr, size_t size );
		virtual uint64_t getSegmentRemoteKey ( void* addr, size_t size );
		virtual RemoteIOV getRemoteIOV ( void * addr, size_t size );
		virtual void stop ();
		virtual void startThreads ();
		virtual void startNoThreads ();
	protected:
		virtual void doInitRequest ( VerbsRequest & request );
		virtual void doSendCommand ( UnitCommand& command, VerbsRequest& request );
		virtual void doPostCommandRecv ( DAQ::UnitCommand& command, VerbsRequest& request, int rank = -1 );
		virtual void doRdmaWrite ( int rank, int uuid, void* src,void * dest, uint64_t destKey, size_t size, VerbsRequest& request );
		virtual void doPostRdmaRecv ( int rank, int uuid, void* ptr, size_t size, VerbsRequest& request );
		virtual int doTest ( VerbsRequest* requests, size_t count, bool useWait );
		virtual void doRepublishCommandRecv ( DAQ::UnitCommand& command, VerbsRequest& request, int rank = -1 );
	protected:
		//VERBS SPECIFIC METHODS		
		//function to route and resolve RDMA adresses between proc Ranks
		void doRouteNResolveAll ();
		//function to connect and set IBV to devices as a Client
		void doConnectNSetupCtxAll ();
		//function to accept connections as a server
		void doAcceptAll ( int *connRank );
		//setup Ctx for all requesting clients
		void doServerSetupCtxAll ( int *connRank );
		ibv_mr * getIbvMR ( void * ptr, size_t size );
		//get memory region object containing  for a given address and given size
		MemoryRegion * getMR ( void * ptr, size_t size );
		//get port for the process rank
		int getPort ( int rank );
		//sefup fields of a given request for a RDMA transmission
		void setupRdmaReq ( struct DAQ::VerbsRequest& request, void* src, void * dest, uint64_t destKey, size_t size );
		//sefup fields of a given request for a Cmd IBV Send
		void setupCmdReq ( DAQ::UnitCommand& command, struct VerbsRequest& request );
		//poll for incoming messages using ibv_poll_cq and then stream to a channel for doTest
		void polling ();
	private:
		/*********************  TYPES  **********************/
		int portBase;
		int portRange;
		int backLogSize;
		bool useIpV6;
		std::string iface;
		int rdmaChannels;
		struct rdma_event_channel *cm_channel;
		struct rdma_event_channel *cm_listen_channel;
		struct rdma_cm_id **cm_id;
		struct rdma_cm_event *event;
		struct rdma_conn_param conn_param = {};
		struct rdma_cm_id *listen_id;
		GoChannel< VerbsRequest * > pollingChannel;
		bool running;
		unsigned int totalSGEs;
		unsigned int totalWRs;
		unsigned int totalStoredCompletions;
		unsigned int resolveTimeout;
		unsigned int procRank;
		unsigned int worldSize;
		std::vector<ibv_wc> m_wcs;
		struct ibv_context *common_ibv_ctx;
		struct ibv_pd *pd;
		struct ibv_cq **cq;
		struct ibv_cq *evt_cq;
		DAQ::Launcher * launcher;
		std::thread pollingThread;
		//aggregate of memory regions for verbs
		verbsMemoryRegionVector segments;           
};

}

#endif //DAQ_DRIVER_LIBVERBS_HPP
