/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Balazs Voneki - CERN
                        Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include "DriverTCP.hpp"
#include <portability/TcpHelper.hpp>
#include <common/CRC.hpp>
#include <cstring>
//to get ip
#include <unistd.h>
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <ifaddrs.h>
#include <netinet/in.h> 
#include <arpa/inet.h>
#include <fcntl.h>
#include <sys/epoll.h>

/********************  NAMESPACE  *******************/
namespace DAQ
{

/*******************  FUNCTION  *********************/
TcpRdmaChannelHandler::TcpRdmaChannelHandler ()
{
	this->fd = -1;
	this->cursor = 0;
	this->state = TCP_RDMA_STATE_WAIT_HEADER;
	this->header.addr = nullptr;
	this->header.uuid = 0;
	this->header.size = 0;
}

/*******************  FUNCTION  *********************/
DriverTCP::DriverTCP () : Driver("tcp2")
{
	this->rdmaChanRoundRobin = 0;
}

/*******************  FUNCTION  *********************/
DriverTCP::~DriverTCP ()
{
	delete [] epollCmdFd;
	delete [] epollRdmaFd;
	delete [] this->cmdRecvThread;
	delete [] this->cmdSendThread;
	delete [] this->rdmaRecvThread;
	delete [] this->rdmaSendThread;
	
	delete [] cmdFd;
	delete [] rdmaChan;
	delete [] this->rdmaSendChan;
	delete [] this->cmdSendChan;
}

/*******************  FUNCTION  *********************/
void DriverTCP::initialize ( const Config& config, Launcher& launcher, int argc, char** argv )
{
	//load config
	this->useIpV6      = config.getAndSetConfigBool("drivers.tcp","ipv6",false);
	this->portBase     = config.getAndSetConfigInt("drivers.tcp","portBase",2000);
	this->portRange    = config.getAndSetConfigInt("drivers.tcp","portRange",256);
	this->iface        = config.getAndSetConfigString("drivers.tcp","iface","eth0");
	this->rdmaChannels = config.getAndSetConfigInt("drivers.tcp","rdmaChannels",1);
	this->rdmaThreadNumber = config.getAndSetConfigInt("drivers.tcp","rdmaThreads",8);
	this->cmdThreadNumber = config.getAndSetConfigInt("drivers.tcp","cmdThreads",8);
	
	//setup some struct
	this->rdmaSendChan = new GoChannel<std::function<void()>>[rdmaThreadNumber];
	this->cmdSendChan = new GoChannel<std::function<void()>>[cmdThreadNumber];
	
	//twick
	if (rdmaChannels * launcher.getWorldSize() < rdmaThreadNumber)
		rdmaThreadNumber = rdmaChannels * launcher.getWorldSize();
	if (cmdThreadNumber > launcher.getWorldSize())
		cmdThreadNumber = launcher.getWorldSize();
	
	//get local IP and share
	std::string ip = TcpHelper::getLocalIP(this->iface,this->useIpV6);
	launcher.set("ip",ip);
	launcher.commit();
	
	//get port
	int port = getPort(launcher.getRank());
	
	//setup listening socket
	DAQ_DEBUG_ARG("tcp","Listen on port : %1:%2").arg(ip).arg(port).end();
	int listenSocket = TcpHelper::makeListeningSocket(port,useIpV6);
	
	//sync
	launcher.barrier();
	
	//allocate sockets
	this->cmdFd = new int[launcher.getWorldSize()];
	this->rdmaChan = new TcpRdmaChannelHandler[launcher.getWorldSize() * rdmaChannels];
	for (int i = 0 ; i < launcher.getWorldSize() ; i++)
		cmdFd[i] = -1;
	
	//setup conections
	doConnectAll(launcher);
	doAcceptAll(launcher,listenSocket);
	
	//setup usage of epools to manage many connection per threads
	setupEpolls(launcher);

	//can close listening socket, useless now
	close(listenSocket);
}

/*******************  FUNCTION  *********************/
void DriverTCP::setupEpolls ( Launcher & launcher )
{
	//allocates
	epollRdmaFd = new int[this->rdmaThreadNumber];
	epollCmdFd = new int[this->cmdThreadNumber];
	
	//setup cmd
	for (int i = 0 ; i < this->cmdThreadNumber ; i++)
	{
		epollCmdFd[i] = epoll_create1(0);
		assumeArg(epollCmdFd[i] > 0,"Error at epoll_create1() : %1").argStrErrno().end();
	}
	
	//setup rdma
	for (int i = 0 ; i < this->rdmaThreadNumber ; i++)
	{
		epollRdmaFd[i] = epoll_create1(0);
		assumeArg(epollRdmaFd[i] > 0,"Error at epoll_create1() : %1").argStrErrno().end();
	}
	
	//fill
	struct epoll_event event;
	for (int i = 0 ; i < launcher.getWorldSize() ; i++)
	{
		//insert if not local (not setup => -1)
		if (cmdFd[i] != -1)
		{
			event.data.fd = cmdFd[i];
			event.events = EPOLLIN;
		
			//regs and check
			int status = epoll_ctl(epollCmdFd[i % cmdThreadNumber], EPOLL_CTL_ADD, cmdFd[i], &event);
			assumeArg(status >= 0,"Error at epoll_ctl() : %1").argStrErrno().end();
		}
	}
	
	for (int i = 0 ; i < launcher.getWorldSize() * rdmaChannels ; i++)
	{
		//insert rdma if not local
		if (rdmaChan[i].fd != -1)
		{
			event.data.fd = rdmaChan[i].fd;
			event.events = EPOLLIN;
		
			//regs and check
			int status = epoll_ctl(epollRdmaFd[i % rdmaThreadNumber], EPOLL_CTL_ADD, rdmaChan[i].fd, &event);
			assumeArg(status >= 0,"Error at epoll_ctl() : %1").argStrErrno().end();
		}
	}
}

/*******************  FUNCTION  *********************/
void DriverTCP::doAcceptAll ( Launcher& launcher, int listenSocket )
{
	//wait (worldSize - rank - 1) connections for CommandChannal + the N rdma channels
	int cnt = (launcher.getWorldSize() - launcher.getRank() - 1) * (1 + rdmaChannels);
	for (int i = 0 ; i < cnt ; i++)
		doAccept(launcher,listenSocket);
}

/*******************  FUNCTION  *********************/
void DriverTCP::doConnectAll ( Launcher& launcher )
{
	//connect to the "rank" previous nodes
	for (int targetRank = 0 ; targetRank < launcher.getRank() ; targetRank++)
	{
		//for command
		cmdFd[targetRank] = doConnect(launcher,targetRank,TCP_HANDSHAKE_COMMAND,0);
		//for RDMA, setup N sockets to manage more parallism
		for (int j = 0 ; j < rdmaChannels ; j++)
			rdmaChan[j * launcher.getWorldSize() + targetRank].fd = doConnect(launcher,targetRank,TCP_HANDSHAKE_RDMA,j);
	}
}

/*******************  FUNCTION  *********************/
void DriverTCP::finalize ()
{
	//vars
	int worldSize = transport->getWorldSize();
	int rank = transport->getRank();
	
	//close command sockets
	for (int targetRank = 0 ; targetRank < worldSize ; targetRank++)
		if (rank != targetRank)
			close(cmdFd[targetRank]);
	
	//close rdma sockets
	for (int targetRank = 0 ; targetRank < worldSize ; targetRank++)
		for (int i = 0 ; i < rdmaChannels ; i++)
			close(rdmaChan[targetRank + i * worldSize].fd);
}

/*******************  FUNCTION  *********************/
void DriverTCP::doAccept ( Launcher &launcher, int listenSocket )
{
	//vars
	struct sockaddr_storage ss;
	socklen_t slen = sizeof(ss);
	
	//accept the incomming connection
	DAQ_DEBUG("tcp","Waiting for accept");
	int fd = accept(listenSocket, (struct sockaddr*)&ss, &slen);
	assumeArg(fd > 2,"Invalid file descriptor on accept : %1").argStrErrno().end();
	DAQ_DEBUG("tcp","Has accepted one");
	
	//read the handshake to know what and who it is
	TcpInitHandshake handshake;
	int status = recv(fd, &handshake, sizeof(handshake), 0);
	assume(status == sizeof(handshake),"Invalid size from handshake on incomming connection !");
	
	//now apply
	switch (handshake.goal)
	{
		case TCP_HANDSHAKE_COMMAND:
			cmdFd[handshake.rank] = fd;
			break;
		case TCP_HANDSHAKE_RDMA:
			rdmaChan[handshake.id * launcher.getWorldSize() + handshake.rank].fd = fd;
			break;
		default:
			DAQ_FATAL("Invalid handshake gaol !");
			break;
	}
}

/*******************  FUNCTION  *********************/
int DriverTCP::doConnect ( Launcher& launcher, int rank, TcpHandshakeGoal goal, int id )
{
	struct sockaddr_in sin;
	int fd;
	int status;
	
	/* Allocate a new socket */
	fd = socket( useIpV6 ? AF_INET6 : AF_INET , SOCK_STREAM, 0);
	assumeArg(fd>=0, "Error while calling socket() : %1").argStrErrno().end();
	
	/* Prepare connection parameters. */
	sin.sin_family = useIpV6 ? AF_INET6 : AF_INET;
	sin.sin_port = htons(getPort(rank));
	std::string ip = launcher.get("ip",rank);
	inet_aton(ip.c_str(), &sin.sin_addr);
	DAQ_DEBUG_ARG("tcp","Try to connect to %1:%2").arg(ip).arg(sin.sin_port).end();
	
	/* Connect to the remote host. */
	status = connect(fd, (struct sockaddr*) &sin, sizeof(sin));
	assumeArg(status == 0, "Error while calling connect() : %1").argStrErrno().end();
	
	//setup handshake
	TcpInitHandshake handshake;
	handshake.rank = launcher.getRank();
	handshake.id = id;
	handshake.goal = goal;
	
	//send into socket
	status = send(fd,&handshake,sizeof(handshake),0);
	assume(status == sizeof(handshake),"Failed to fully send the handshake when creating TCP connection (client side) !");
	
	return fd;
}

/*******************  FUNCTION  *********************/
void DriverTCP::makeNonBlocking(int fd)
{
	fcntl(fd, F_SETFL, O_NONBLOCK);
}

/*******************  FUNCTION  *********************/
int DriverTCP::getPort ( int rank )
{
	return portBase + rank % portRange;
}

/*******************  FUNCTION  *********************/
RemoteIOV DriverTCP::getRemoteIOV ( void* addr, size_t size )
{
	RemoteIOV ret;
	ret.addr = addr;
	ret.key = 0;
	return ret;
}

/*******************  FUNCTION  *********************/
uint64_t DriverTCP::getSegmentRemoteKey ( void* addr, size_t size )
{
	return 0;
}

/*******************  FUNCTION  *********************/
void DriverTCP::unregisterSegment ( void* addr, size_t size )
{
	//nothing
}

/*******************  FUNCTION  *********************/
void DriverTCP::startNoThreads ()
{
	hasNoThread = true;
	DAQ_FATAL("Single threaded mode not supported !");
}

/*******************  FUNCTION  *********************/
void DriverTCP::startThreads ()
{
	threadRunning = true;
	hasNoThread = false;
	
	this->cmdRecvThread = new std::thread[this->cmdThreadNumber];
	this->cmdSendThread = new std::thread[this->cmdThreadNumber];
	this->rdmaRecvThread = new std::thread[this->rdmaThreadNumber];
	this->rdmaSendThread = new std::thread[this->rdmaThreadNumber];
	
    for (int i = 0 ; i < this->cmdThreadNumber ; i++)
	{
		this->cmdRecvThread[i] = std::thread([this, i](){this->cmdRecvMain(i);});
		this->cmdSendThread[i] = std::thread([this, i](){this->cmdSendMain(i);});
	}
	
	for (int i = 0 ; i < this->rdmaThreadNumber ; i++)
	{
		this->rdmaRecvThread[i] = std::thread([this, i](){this->rdmaRecvMain(i);});
		this->rdmaSendThread[i] = std::thread([this, i](){this->rdmaSendMain(i);});
	}
}

/*******************  FUNCTION  *********************/
void DriverTCP::registerSegment ( void* addr, size_t size )
{
	//nothing
}

/*******************  FUNCTION  *********************/
void DriverTCP::postWaitRemoteRDMAWrite ( Unit* srcUnit, int rank, int unitId, void* ptr, size_t size, int uuid, const UnitCommand& postCommand )
{
	//safety reg
	{
		std::lock_guard<std::mutex> guard(lock);
		DAQ_DEBUG_ARG("tcp","rdmaRemoteWriteNotif NEW uuid=%1").arg(uuid).end();
		#ifndef NDEBUG
			assume(this->notifMap.find(uuid) == this->notifMap.end(),"uuid reuse");
		#endif
		this->notifMap[uuid] = postCommand;
	}
}

/*******************  FUNCTION  *********************/
void DriverTCP::updateNoThread ()
{
	this->rdmaRecvMain(0);
	this->rdmaSendMain(0);
	this->cmdRecvMain(0);
	this->cmdSendMain(0);
}

/*******************  FUNCTION  *********************/
void DriverTCP::sendRemoteCommand ( const UnitCommand& command )
{
	//define task
	UnitCommand cmd = command;
	std::function<void()> f = [=]()
	{
		UnitCommand cmdLocal = cmd;
		char * cp = (char *)&cmdLocal;
		int remaining = sizeof(cmdLocal);
		
		while (remaining)
		{
			int n_written = send(cmdFd[cmdLocal.dest.rank], cp, remaining, 0);
			if (n_written <= 0) {
				if (!threadRunning) {
					/* We have already terminated the running at this point.
					 * The receiver side may have already closed the socket,
					 * that is why we can't send anything. */
					break;
				} else {
					perror("sendRemoteCommand perror: ");
				}
			}
			remaining -= n_written;
			cp += n_written;
		}
	};
	
	//push task
	cmdSendChan[cmd.dest.rank % cmdThreadNumber] << f;
}

/*******************  FUNCTION  *********************/
void DriverTCP::postCommandRecv ()
{
	//nothing
}

/*******************  FUNCTION  *********************/
void DriverTCP::stop ()
{
	threadRunning = false;
	
	for (int i = 0 ; i < this->cmdThreadNumber ; i++)
	{
		this->cmdSendChan[i].close();
		this->cmdRecvThread[i].join();
		this->cmdSendThread[i].join();
	}
	
	for (int i = 0 ; i < this->rdmaThreadNumber ; i++)
	{
		this->rdmaSendChan[i].close();
		this->rdmaRecvThread[i].join();
		this->rdmaSendThread[i].join();
	}
}

/*******************  FUNCTION  *********************/
void DriverTCP::cmdSendMain ( int threadId )
{
	std::function<void()> task;
	while (task << cmdSendChan[threadId])
		task();
	DAQ_DEBUG("tcp","cmdSendChan closed");
}

/*******************  FUNCTION  *********************/
void DriverTCP::rdmaSendMain ( int threadId )
{
	std::function<void()> task;
	while (task << rdmaSendChan[threadId])
		task();
	DAQ_DEBUG("tcp","rdmaSendChan closed");
}

/*******************  FUNCTION  *********************/
void DriverTCP::cmdRecvMain ( int threadId )
{
	struct epoll_event eventCmd;
	struct epoll_event *eventsCmd;
	
	eventsCmd = (epoll_event*)calloc(transport->getWorldSize(), sizeof eventCmd);
	
	do {
		int n;
// 		char buf[MAX_LINE];
		UnitCommand command;
		
		n = epoll_wait(epollCmdFd[threadId], eventsCmd, transport->getWorldSize(), this->hasNoThread ? 5 : 1000 );
		for(int i = 0; i < n; i++) {
			if ((eventsCmd[i].events & EPOLLERR) || (eventsCmd[i].events & EPOLLHUP) ||
				(!(eventsCmd[i].events & EPOLLIN))) {
				/* An error has occured on this fd, or the socket is not
				ready for reading (why were we notified then?) */
				DAQ_ERROR_ARG("epoll error").argStrErrno().end();
				close(eventsCmd[i].data.fd);
			} else {
				ssize_t count;
				count = recv(eventsCmd[i].data.fd, (void *)&command, sizeof(command), 0);
				assume(count != -1, "Cmd Receive count = -1 occurred !");
				if (count > 0 )
				{
					assume(count==sizeof(command), "Cmd Receive count != sizeof(command) !");
					transport->sendCommand(command);
				}
			}
		}
	} while (threadRunning);
	
	free(eventsCmd);
	
	DAQ_DEBUG("tcp_channel","cmdRecvChan closed");
}

/*******************  FUNCTION  *********************/
TcpRdmaChannelHandler* DriverTCP::getRdmaHandler ( int fd )
{
	int cnt = rdmaChannels * transport->getWorldSize();
	for (int i = 0 ; i < cnt ; i++)
		if (rdmaChan[i].fd == fd)
			return &rdmaChan[i];
		
	DAQ_FATAL("FD not found !");
	return nullptr;
}

/*******************  FUNCTION  *********************/
void DriverTCP::rdmaWrite ( Unit* srcUnit, int rank, int unitId, void* src, void* dest, uint64_t destKey, size_t size, int uuid, const UnitCommand& postCommand )
{
	int chan;
	UnitCommand cmd = postCommand;
	
	do {
		std::lock_guard<std::mutex> guard(this->lock);
		chan = rank + rdmaChanRoundRobin * transport->getWorldSize();
		rdmaChanRoundRobin = (rdmaChanRoundRobin + 1) % rdmaChannels;
	} while(0);
	
	//define task
	std::function<void()> f = [=]() {
		DAQ_DEBUG_ARG("tcp","rdmaWrite(scrrank=%7, destrank=%1, unitId=%2, destKey=%3, size=%4, uuid=%5, type=%6, postCommand.args.pullGatherAck.eventId=%7)").arg(rank).arg(unitId).arg(destKey).arg(size).arg(uuid).arg(postCommand.type).arg(transport->getRank()).arg(postCommand.args.gatherAck.eventId).end();
		for(int i = 0 ; i < 2 ; i++) {
			char * cp;
			int remaining;

			TcpRdmaHeader headerLocal;
			headerLocal.size = size;
			headerLocal.uuid = uuid;
			headerLocal.addr = dest;
			#ifndef NDEBUG
				headerLocal.crc = crc16((uint8_t*)src,size);
			#endif
			
			switch(i) {
				//send header first
				case 0:
					cp = (char *)&headerLocal;
					remaining = sizeof(headerLocal);
					break;
				//then send the actual payload
				case 1:
					cp = (char *)src;
					remaining = headerLocal.size;
					break;
				default:
					perror("invalid iteration in rdmaWrite");
					break;
			}
			
			while (remaining) {
				int n_written = send(rdmaChan[chan].fd, cp, remaining, 0);
				assumeArg(n_written > 0 && n_written <= remaining,"DriverTCP::rdmaWrite perror: %1").argStrErrno().end();
				remaining -= n_written;
				cp += n_written;
			}
		}
		
		//send post command to sender
		transport->sendCommand(cmd);
	};
	
	//push task
	rdmaSendChan[chan % rdmaThreadNumber] << f;
}

/*******************  FUNCTION  *********************/
//From CEA/MPC
ssize_t safeRecv(int fd, void* buf, size_t count, int flags)
{
	/* vars */
	size_t tmp = 0, total = 0;
	int res = count;

	if( count == 0 )
	{
		return 1;
	}

	/* loop until read all */
	while (total < count) {
		tmp = recv(fd, (char *)buf + total, count - total,flags);

		/* check errors */
		if (tmp == 0) {
			res = total;
			break;
		} else if (tmp < 0) {
			/* on interuption continue to re-read */
			if (errno == EINTR) {
				continue;
			} else {
				DAQ_ERROR_ARG("Recv FAIL : %1").argStrErrno().end();
				res = -1;
				break;
			}
		}

		/* update size counter */
		total += tmp;
	};
	
	return res;
}

/*******************  FUNCTION  *********************/
void DriverTCP::rdmaRecvMain ( int threadId )
{
	struct epoll_event eventData;
	struct epoll_event *eventsData; //config.eventRailMaxDataSize
	int maxEvts = transport->getWorldSize() * rdmaChannels;
	
	eventsData = (epoll_event*)calloc(maxEvts, sizeof eventData);
	
	DAQ_DEBUG("tcp_recvmain","DriverTCP::rdmaRecvMain called.");
	
	do
	{
		int n;
		n = epoll_wait(epollRdmaFd[threadId], eventsData, maxEvts, this->hasNoThread ? 5 : 1000 ); 
		DAQ_DEBUG_ARG("tcp","epoll get %1").arg(n).end();
		for(int i = 0; i < n; i++) {
			if ((eventsData[i].events & EPOLLERR) || (eventsData[i].events & EPOLLHUP) ||
				(!(eventsData[i].events & EPOLLIN))) {
				// An error has occured on this fd, or the socket is not
				// ready for reading (why were we notified then?)
				DAQ_ERROR_ARG("epoll error : %1").argStrErrno().end();
				close(eventsData[i].data.fd);
				continue;
			} else {
				TcpRdmaChannelHandler *handler = getRdmaHandler(eventsData[i].data.fd);
				
				//receive header
				if (handler->state == TCP_RDMA_STATE_WAIT_HEADER) {
					//get it
					ssize_t status = safeRecv(eventsData[i].data.fd, (void *)&(handler->header), sizeof(handler->header), 0);
					assumeArg(status == sizeof(handler->header),"Invalid receive size : %1").arg(status).end();
					DAQ_DEBUG_ARG("tcp","get RDMA header %1").arg(handler->header.addr).end();
					
					//update
					handler->cursor = 0;
					handler->state = TCP_RDMA_STATE_WAIT_DATA;
				} else if (handler->state == TCP_RDMA_STATE_WAIT_DATA) {
					//get next part
					ssize_t expectedSize = handler->header.size - handler->cursor;
					DAQ_DEBUG_ARG("tcp","recv on addr : %1").arg(handler->header.addr).end();
					ssize_t recvSize = recv(eventsData[i].data.fd, (char*)handler->header.addr+handler->cursor, expectedSize, 0);
					assumeArg(recvSize > 0 && recvSize <= expectedSize,"Invalid return value : %1 : %2").arg(recvSize).argStrErrno().end();
					handler->cursor += recvSize;
					DAQ_DEBUG_ARG("tcp","get RDMA chunk %1 / %2").arg(handler->cursor).arg(handler->header.size).end();
					
					//finish
					if (recvSize == expectedSize){
						int uuid = handler->header.uuid;
						std::lock_guard<std::mutex> guard(this->lock);
						assume(this->notifMap.find(uuid) != this->notifMap.end(),"Fail to find uuid in notif map !");
						assume(this->notifMap[uuid].type != UNIT_CMD_INVALID,"Invalid command type !");
						
						#ifndef NDEBUG
							assume(handler->header.crc == crc16((uint8_t*)handler->header.addr,handler->header.size),"Invalid CRC16 checking in RDMA comm !");
						#endif
						
						transport->sendCommand(this->notifMap[uuid]);
						this->notifMap.erase(uuid);
						handler->state = TCP_RDMA_STATE_WAIT_HEADER;
						DAQ_DEBUG("tcp","RDMA finished");
					}
				} else {
					DAQ_ERROR("Unknown state");
				}
			}
		}
	} while (threadRunning);
	
	free(eventsData);
}

}

