/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include <cassert>
#include <common/Debug.hpp>
#include "DriverMPI.hpp"
#include <unistd.h>
#include <gochannels/GoChannelSelector.hpp>

/********************  MACROS  **********************/
#define DAQ_DRIVER_MPI_SLOT_CMD_TAG 0

/********************  NAMESPACE  *******************/
namespace DAQ
{

/*******************  FUNCTION  *********************/
DriverMPI::DriverMPI () 
           :DriverSlots<MPI_Request>("DriverMPI")
{
}

/*******************  FUNCTION  *********************/
DriverMPI::~DriverMPI ()
{
}

/*******************  FUNCTION  *********************/
void DriverMPI::doInitRequest ( MPI_Request & request )
{
	request = MPI_REQUEST_NULL;
}

/*******************  FUNCTION  *********************/
RemoteIOV DriverMPI::getRemoteIOV ( void* addr, size_t size )
{
	//we don't care
	RemoteIOV iov = {0,0};
	return iov;
}

/*******************  FUNCTION  *********************/
uint64_t DriverMPI::getSegmentRemoteKey ( void* addr, size_t size )
{
	return 0;
}

/*******************  FUNCTION  *********************/
void DriverMPI::unregisterSegment ( void* addr, size_t size )
{
	//nothing to de
}

/*******************  FUNCTION  *********************/
void DriverMPI::registerSegment ( void* addr, size_t size )
{
	//nothing to do
}

/*******************  FUNCTION  *********************/
void DriverMPI::doSendCommand ( UnitCommand& command, MPI_Request& request )
{
// 	fprintCommand(stdout, command);

	
	//post actions
	DAQ_DEBUG_ARG("mpi","%3 -> MPI_ISend(command,%1,%2)").arg(command.dest.rank).arg(DAQ_DRIVER_MPI_SLOT_CMD_TAG).arg(this->transport->getRank()).end();
	int status = MPI_Isend(&command,sizeof(command),MPI_CHAR,command.dest.rank,DAQ_DRIVER_MPI_SLOT_CMD_TAG,MPI_COMM_WORLD,&request);
	assumeArg(status == 0,"Failed to post MPI_Secv : %1").arg(status).end();
}

/*******************  FUNCTION  *********************/
void DriverMPI::doPostCommandRecv ( DAQ::UnitCommand& command, MPI_Request& request, int rank )
{
// 	fprintCommand(stdout, command);
	assert(rank == -1);
	DAQ_DEBUG_ARG("mpi","%2 -> MPI_IRecv(command,ANY_SOURCE,%1)").arg(DAQ_DRIVER_MPI_SLOT_CMD_TAG).arg(this->transport->getRank()).end();
	int status = MPI_Irecv(&command,sizeof(command),MPI_CHAR,MPI_ANY_SOURCE,DAQ_DRIVER_MPI_SLOT_CMD_TAG,MPI_COMM_WORLD,&request);
	assumeArg(status == 0,"Failed to post MPI_IRecv : %1").arg(status).end();
}

/*******************  FUNCTION  *********************/
void DriverMPI::doRdmaWrite ( int rank, int uuid, void* src,void * dest, uint64_t destKey, size_t size, MPI_Request& request )
{
	//port MPI request
	DAQ_DEBUG_ARG("mpi","%3 -> MPI_ISend(rdma,%1,%2)").arg(rank).arg(uuid).arg(this->transport->getRank()).end();
	int status = MPI_Isend(src,size,MPI_CHAR,rank,uuid+1,MPI_COMM_WORLD,&request);
	assumeArg(status == 0,"Failed to post MPI_Secv : %1").arg(status).end();
}

/*******************  FUNCTION  *********************/
void DriverMPI::doPostRdmaRecv ( int rank, int uuid, void* ptr, size_t size, MPI_Request& request )
{
	//post MPI request
	assert(ptr != nullptr);
	DAQ_DEBUG_ARG("mpi","%3 -> MPI_IRecv(rdma,%1,%2,%4)").arg(rank).arg(uuid).arg(this->transport->getRank()).arg(ptr).end();
	int status = MPI_Irecv(ptr,size,MPI_CHAR,rank,uuid+1,MPI_COMM_WORLD,&request);
	assumeArg(status == 0,"Failed to post MPI_Secv : %1").arg(status).end();
}

/*******************  FUNCTION  *********************/
int DriverMPI::doTest ( MPI_Request* requests, size_t count, bool useWait )
{
	int id = MPI_UNDEFINED;
	int flag = 1;

	//wait any of the requests, made progress all
	MPI_Status mpiStatus;
	if (useWait) {
		DAQ_DEBUG_ARG("mpiWait","%1 -> MPI_Waitany()").arg(this->transport->getRank()).end();
		int status = MPI_Waitany(count,requests,&id,&mpiStatus);
		assumeArg(status == 0,"Failed to MPI_Waitany : %1").arg(status).end();
	} else {
		DAQ_DEBUG_ARG("mpiWait","%1 -> MPI_Testany()").arg(this->transport->getRank()).end();
		int status = MPI_Testany(count,requests,&id, &flag,&mpiStatus);
		assumeArg(status == 0,"Failed to MPI_Testany : %1").arg(status).end();
	}
	
	//nothing
	if (flag == 0 || id == MPI_UNDEFINED)
		return -1;
	else
		return id;
}

/*******************  FUNCTION  *********************/
void DriverMPI::doRepublishCommandRecv ( DAQ::UnitCommand& command, MPI_Request& request, int rank )
{
	assert(rank == -1);
	this->doPostCommandRecv(command,request,rank);
}

}
