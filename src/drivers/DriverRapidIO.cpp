/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Sima Baymani - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/* IDEAS FOR THE FUTURE
 *
 * -- Allocating larger buffers
 *  - Map them so that they become contiguous, either by suggesting an address
 *  or remapping the mapping RIO gives us
 */

/********************  HEADERS  *********************/
#include "DriverRapidIO.hpp"
#include "common/Debug.hpp"

/* For handling the shell call to find out our local destination id */
#include <string>
#include <iostream>
#include <cstdio>
#include <memory>

/* For memset */
#include <string.h>

/* For error codes */
#include <errno.h>

/* for getpagesize() */
#include <unistd.h>

/********************  MACROS  **********************/
#define DAQ_DRIVER_RAPIDIO_DEST_ID_FILE "/sys/class/rio_mport/rio_mport0/device/port_destid"
#define DAQ_DRIVER_RAPIDIO_LOCAL_ID_COMMAND "cat %s 2>/dev/null"

/**
 * Helper macro to check RIO lib return status.
 * It will finish with abort() in case of failure.
 * @param funcName Define the name of the function which has been called.
 * @param status Define the output status to check.
**/
#define RIO_CHECK_STATUS(funcName, status) \
do { assumeArg(status == 0,"Failed: %1 ERR %2 errno %3").arg(funcName).arg(status).argStrErrno().end(); } while(0);

/* Currently, RIO Message buffers have to be 4KB large, the library only supports that */
#define DAQ_DRIVER_RAPIDIO_CM_MAX_BUF_SIZE (0x1000)

/* The allocated RIO Message buffer includes 20 bytes of header, used internally.
 * This has to be accounted for at each send/recv call */
#define DAQ_DRIVER_RAPIDIO_CM_HEADER_SIZE (20)
#define DAQ_DRIVER_RAPIDIO_CM_PAYLOAD_SIZE (DAQ_DRIVER_RAPIDIO_CM_MAX_BUF_SIZE - DAQ_DRIVER_RAPIDIO_CM_HEADER_SIZE)

/* Need this as otherwise we will wait forever in receive -> miss when we should stop
 * NOTE: Seems that a longer timeout is needed when scaling up.
 * */
#define DAQ_DRIVER_RAPIDIO_CM_RECV_TIMEOUT (3 * 1000) // few nodes: 1 second is enough

/* Due to limitations in the CMA (Contiguous Memory Allocator)
 * we can currently only allocate max 2MB per buffer */
#define DAQ_DRIVER_RAPIDIO_DMA_MAX_WIN_SIZE (2 * 1024 * 1024)
#define DAQ_DRIVER_RAPIDIO_DMA_MIN_WIN_SIZE (4096)
#define DAQ_DRIVER_RAPIDIO_DMA_DEF_WIN_SIZE (2 * 1024 * 1024)

/* When using reserved memory
 * */
#define DAQ_DRIVER_RAPIDIO_DMA_RES_MEM_ADDR (0x180000000)
#define DAQ_DRIVER_RAPIDIO_DMA_RES_MEM_SIZE (0x40000000)


/* Nice to have during development if sharing server or
 * if channels crash and you don't want to restart the server 
 * NOTE: channel 0 is management channel and should not be used */
#define DAQ_DRIVER_RAPIDIO_CM_CHAN_OFFSET (50)

/********************  NAMESPACE  *******************/
namespace DAQ
{

/*******************  FUNCTION  *********************/
DriverRapidIO::DriverRapidIO()
	:Driver("DriverRapidIO"),
	/* mport (RapidIO) devices on a host are numbered from 0 -> X
	 * For our cases, we have only one device per host == device 0 */
	mportId(0),
	localId(0),
	kbufMode(0),
	worldSize(0),
	clientSkt(nullptr),
	acceptThread(),
	cmdRecvThread(nullptr),
	cmdSendThread(nullptr),
	cmdRecvCont(false),
	cmdSendChan(nullptr),
	msgSent(0),
	launcher(nullptr),
	mportHnd(0),
	maxSge(32),
	mempool(nullptr),
	resMemBuf(nullptr),
	resMemTgtAddr(RIOMP_MAP_ANY_ADDR),
	resMemHandle(DAQ_DRIVER_RAPIDIO_DMA_RES_MEM_ADDR),
	resMemSize(DAQ_DRIVER_RAPIDIO_DMA_RES_MEM_SIZE),
	// set default values in case we can't get actual values
	maxSize(DAQ_DRIVER_RAPIDIO_DMA_MIN_WIN_SIZE),
	rdmaRecvThread(nullptr),
	rdmaSendThread(nullptr),
	rdmaRecvCont(false),
	rdmaSendChan(nullptr)
{

}

/*******************  FUNCTION  *********************/
DriverRapidIO::~DriverRapidIO()
{
	DAQ_DEBUG("rapidio", ">>> ~DriverRapidIO");
	DAQ_DEBUG("rapidio", "<<< ~DriverRapidIO");
}

/*******************  FUNCTION  *********************/
/* SEQUENCE
 * 1 - map your destid to your rank DONE
 * 2 - open outgoing connections
 * 3 - wait for incoming connections
 * 4 - exchange information with the other side
 *
 * TODO
 * - Computation of port when we have several processes on the same node (resource busy)
 *   -> loopback connection is no longer possible, functionality has been removed
 * */
void  DriverRapidIO::initialize(const DAQ::Config& config, DAQ::Launcher& launcher,
				int argc, char** argv)
{
	this->worldSize = launcher.getWorldSize();
	this->launcher = &launcher;
/*	DAQ_DEBUG_ARG("rapidio", "My rank: %1\n")
		.arg(launcher.getRank())
		.end();
		*/
	if(this->readLocalId())
	{
		DAQ_ERROR("Failed to read the local destination id\n");
		return;
	}
	DAQ_INFO_ARG("My local id: %1")
		.arg(this->localId)
		.end();

	if(system("hostname")) {
		DAQ_ERROR("Failed to read the hostname\n");
	}

	// register our local destination id in the database and wait for everyone else
	launcher.set("destId", std::to_string(this->localId));
	launcher.commit();
	launcher.barrier();


	this->initializeClientConnections();
	this->openServerSocket(launcher);

	// Use threaded approach to make sure the server is up before any clients try
	// to connect and that it can serve them properly
	this->acceptThread = std::thread([this, &launcher](){this->acceptIncomingConnections(launcher);});
	// to make sure we are accepting before anyone starts connecting
	launcher.barrier();
	
	this->openOutgoingConnections(launcher);
	this->acceptThread.join();
	
	launcher.barrier();
	this->outputMyInfo(launcher);

	// set up RapidIO memory
	this->initializeDMA();
	this->initializeMempool();
	launcher.barrier();

	//Don't create threads until we know that we're ready
	this->createThreads();
	launcher.barrier();
}

/*******************  FUNCTION  *********************/
void DriverRapidIO::initializeClientConnections()
{
	this->clientSkt = new struct RIOClientConnection[this->worldSize];
	memset(clientSkt, 0, sizeof(struct RIOClientConnection) * this->worldSize);
}

/*******************  FUNCTION  *********************/
void DriverRapidIO::initializeMailbox(riomp_mailbox_t& mbox)
{
	DAQ_DEBUG("rapidio", ">>> initializeMailbox");
	 //Create riomp_mailbox control structure
	int res = riomp_sock_mbox_create_handle(this->mportId, 0, &mbox);
/*	DAQ_DEBUG_ARG("rapidio", "riomp_sock_mbox_create_handle mport_id %1, mailbox 0x%2\n")
		.arg(this->mportId)
		.arg(mbox)
		.end();*/
	RIO_CHECK_STATUS("riomp_sock_mbox_create_handle", res);
	DAQ_DEBUG("rapidio", "<<< initializeMailbox");
}

/*******************  FUNCTION  *********************/
void DriverRapidIO::initializeDMA()
{
	DAQ_DEBUG("rapidio", ">>> initializeDMA");

	struct riomp_mgmt_mport_properties prop;
	int res = riomp_mgmt_mport_create_handle(this->mportId, 0, &(this->mportHnd));
	RIO_CHECK_STATUS("riomp_mgmt_mport_create_handle", res);

	// some IDT magic
	// Get information about our link
	if (!riomp_mgmt_query(this->mportHnd, &prop)) {
		riomp_mgmt_display_info(&prop);

		if (prop.flags & RIO_MPORT_DMA) {
			this->maxSge = prop.dma_max_sge;
			this->maxSize = prop.dma_max_size;
		}

		if (prop.link_speed == 0) {
			riomp_mgmt_mport_destroy_handle(&(this->mportHnd));
			// TODO should have a cleanup somewhere...
			DAQ_FATAL("SRIO link is down. Aborting.");
		}
	}
	else {
		DAQ_ERROR("Failed to obtain mport information. Using default configuration");
	}
	DAQ_DEBUG("rapidio", "<<< initializeDMA");
}

/*******************  FUNCTION  *********************/
/* We allocate all of reserved memory and map it up.
 * Then we allocate the already mapped memory chunk by chunk
 * upon request, instead of allocating and mapping each chunk.
 * Target addresses are calculated by using an offset for the mapped
 * memory.
 * */
void DriverRapidIO::initializeMempool()
{
	DAQ_DEBUG("rapidio", ">>> initializeMempool");

	int res = riomp_dma_ibwin_map(this->mportHnd, &(this->resMemTgtAddr),
			this->resMemSize, &(this->resMemHandle));

	/* TODO: clean up properly after we find an error
	 * riomp_mgmt_mport_destroy_handle(mport_hnd); */
	RIO_CHECK_STATUS("riomp_dma_ibwin_map", res);

	res = riomp_dma_map_memory(this->mportHnd, this->resMemSize, this->resMemHandle,
			(void **)&(this->resMemBuf));

	RIO_CHECK_STATUS("riomp_dma_map_memory", res);
/*	DAQ_DEBUG_ARG("rapidio", "mempool ibwin tgt_addr: %1 memory map: %2")
			.arg(tmpSegm.tgtAddr)
			.arg(tmpSegm.buf)
			.end();*/

	mp_create (&(this->mempool), this->resMemBuf , this->resMemSize);
	DAQ_DEBUG("rapidio", "<<< initializeMempool");
}

void DriverRapidIO::finalizeMempool()
{
	DAQ_DEBUG("rapidio", ">>> finalizeMempool");

	mp_destroy(this->mempool);

	int res = riomp_dma_unmap_memory(this->resMemSize, this->resMemBuf);
	RIO_CHECK_STATUS("riomp_dma_unmap_memory", res);

	res = riomp_dma_ibwin_free(this->mportHnd, &(this->resMemHandle));
	RIO_CHECK_STATUS("riomp_dma_ibwin_free", res);

	DAQ_DEBUG("rapidio", "<<< finalizeMempool");
}

/*******************  FUNCTION  *********************/
void DriverRapidIO::openServerSocket(DAQ::Launcher& launcher)
{
	DAQ_DEBUG("rio-conn", ">>> openServerSocket");

	this->initializeMailbox(this->serverSkt.mbox);

	/* Our server will listen here.
	 * However, we can't use channel 0 as it is used for internal
	 * RapidIO management. So use rank+1
	 * */
	this->serverSkt.chan = launcher.getRank() + DAQ_DRIVER_RAPIDIO_CM_CHAN_OFFSET;
	// Create an unbound socket structure
	int res = riomp_sock_socket(this->serverSkt.mbox, &(this->serverSkt.skt));
/*	DAQ_DEBUG_ARG("rapidio", "riomp_sock_socket mailbox 0x%1 socket 0x%2")
		.arg(this->serverSkt.mbox)
		.arg(this->serverSkt.skt)
		.end();*/
	RIO_CHECK_STATUS("riomp_sock_socket", res);

	/* Bind the listen channel to the opened MPORT device
	 * Let each server listen on channel <rank> */
	res = riomp_sock_bind(this->serverSkt.skt, this->serverSkt.chan);
/*	DAQ_DEBUG_ARG("rapidio", "riomp_sock_bind socket 0x%1 to channel %2")
		.arg(this->serverSkt.skt)
		.arg(this->serverSkt.chan)
		.end();*/
	RIO_CHECK_STATUS("riomp_sock_bind", res);

	res = riomp_sock_listen(this->serverSkt.skt);
/*	DAQ_DEBUG_ARG("rapidio", "riomp_sock_listen socket 0x%1")
		.arg(this->serverSkt.skt)
		.end();*/
	RIO_CHECK_STATUS("riomp_sock_listen", res);
	DAQ_DEBUG_ARG("rio-conn", "destId %1 listening on chan %2")
		.arg(this->localId)
		.arg(this->serverSkt.chan)
		.end();
	DAQ_DEBUG("rio-conn", "<<< openServerSocket");
}

/*******************  FUNCTION  *********************/
void DriverRapidIO::openOutgoingConnections(DAQ::Launcher& launcher)
{
	DAQ_DEBUG("rio-conn", ">>> openOutgoingConnections");
	usleep(1000 + launcher.getRank() * 500 * 1000);
	// Following the algorithm as described in the wiki
	// First, open my outgoing connections
	for(int i = 0; i < launcher.getRank(); ++i) {
		// TODO need to get IP of each rank
		int destId = std::stoi(launcher.get("destId", i));
		int chan = i + DAQ_DRIVER_RAPIDIO_CM_CHAN_OFFSET; // chan 0 is management channel
/*		DAQ_DEBUG_ARG("rapidio", "rank %1 has destid %2, listens on chan %3")
			.arg(i)
			.arg(destId)
			.arg(chan)
			.end();*/

		this->initializeMailbox(clientSkt[i].mbox);
		// Create an unbound socket structure
		int res = riomp_sock_socket(this->clientSkt[i].mbox, &(this->clientSkt[i].skt));
/*		DAQ_DEBUG_ARG("rapidio", "riomp_sock_socket mailbox 0x%1 socket 0x%2")
			.arg(this->serverSkt.mbox)
			.arg(this->serverSkt.skt)
			.end();*/
		RIO_CHECK_STATUS("riomp_sock_socket", res);

/*		DAQ_DEBUG_ARG("rapidio", "socket %1")
			.arg(this->clientSkt[i].skt)
			.end();*/
		DAQ_DEBUG_ARG("rio-conn", "Trying to connect to rank %1 destid %2 on chan %3")
			.arg(i)
			.arg(destId)
			.arg(chan)
			.end();
		res = riomp_sock_connect(this->clientSkt[i].skt, destId, chan, 0); //TODO SIMA: last param - retry
		// If the server is not up yet, let's wait a bit and then retry
		if(res == ECONNREFUSED) {
			DAQ_DEBUG("rio-conn", "Connection refused, retrying.");
			usleep(1 * 1000 * 1000);
			res = riomp_sock_connect(this->clientSkt[i].skt, destId, chan, 0); // TODO SIMA - last param, retry
		}
		RIO_CHECK_STATUS("riomp_sock_connect", res);
		DAQ_DEBUG_ARG("rio-conn", "Successfully connected to rank %1 destid %2")
			.arg(i)
			.arg(destId)
			.end();
		//remember who we are talking to
		this->clientSkt[i].rank = i;
		this->initializeCMBuffers(clientSkt[i]);
		//tell the other side who we are
		struct RIOAcceptAck ack = {launcher.getRank()};
		this->sendCMMsg(this->clientSkt[i], &ack, sizeof(ack));
		()this->recvCMMsg(this->clientSkt[i], &ack, sizeof(ack));
		if(ack.rank != i) {
			DAQ_ERROR_ARG("Acknowledgement failed for server at rank %1")
				.arg(i)
				.end();
		} else {
			DAQ_DEBUG_ARG("rio-conn", "Got ack from server at rank %1")
				.arg(ack.rank)
				.end();
		}
	}
	DAQ_DEBUG("rio-conn", "<<< openOutgoingConnections");
}

/*******************  FUNCTION  *********************/
void DriverRapidIO::acceptIncomingConnections(DAQ::Launcher& launcher)
{
	DAQ_DEBUG("rio-conn", ">>> acceptIncomingConnections");

	for(int i = launcher.getRank()+1; i< this->worldSize; ++i) {
		struct RIOClientConnection tmpSkt = {0,0,-1,0,0};
		// Initialize socket for accepted connection
		int res = riomp_sock_socket(this->serverSkt.mbox, &(tmpSkt.skt));
		RIO_CHECK_STATUS("riomp_sock_socket", res);
		DAQ_DEBUG_ARG("rio-conn", "Waiting on connect from client %1")
			.arg(i)
			.end();

		// wait for accept with timeout == 3 min
		res = riomp_sock_accept(serverSkt.skt, &(tmpSkt.skt), 3*60000, 0); //TODO SIMA - last param, retry
		RIO_CHECK_STATUS("riomp_sock_accept", res);
		DAQ_DEBUG_ARG("rio-conn", "Accepted client %1")
			.arg(i)
			.end();
		this->initializeCMBuffers(tmpSkt);
		// find out who is on the other end
		struct RIOAcceptAck ack = {0};
		struct RIOAcceptAck ack_srv = {launcher.getRank()};
		//TODO any host <-> network byte ordering needed?
		res = this->recvCMMsg(tmpSkt, &ack, sizeof(ack));
		this->sendCMMsg(tmpSkt, &ack_srv, sizeof(ack_srv));
		RIO_CHECK_STATUS("recvCMMsg->riomp_sock_receive", res);
		DAQ_DEBUG_ARG("rio-conn", "Accepted client %1 with rank %2")
			.arg(i)
			.arg(ack.rank)
			.end();
		tmpSkt.rank = ack.rank;
		memcpy(&clientSkt[ack.rank], &tmpSkt, sizeof(struct RIOClientConnection));
	}
	DAQ_DEBUG("rio-conn", "<<< acceptIncomingConnections");
}

/*******************  FUNCTION  *********************/
/* Read the local destination id and store it in our member variable
 * TODO should be wrapped in Helper::getLocalIP somehow?
 *
 * http://stackoverflow.com/questions/478898/how-to-execute-a-command-and-get-output-of-command-within-c-using-posix
 * */
int DriverRapidIO::readLocalId()
{
	DAQ_DEBUG("rapidio", ">>> readLocalId");
	char cmd[100];
	snprintf(cmd, sizeof(cmd), DAQ_DRIVER_RAPIDIO_LOCAL_ID_COMMAND,
			DAQ_DRIVER_RAPIDIO_DEST_ID_FILE);
	std::shared_ptr<FILE> pipe(popen(cmd, "r"), pclose);
	if (!pipe)
	{
		return 1;
	}
	char buffer[128];
	std::string result = "";
	while (!feof(pipe.get()))
	{
		if (fgets(buffer, 128, pipe.get()) != nullptr)
		{
			result += buffer;
		}
	}
	this->localId = std::stoi(result, 0, 16); // destId is stored as a hex value
/*	DAQ_DEBUG_ARG("rapidio", "LocalId is stored as %1, was read as %2")
		.arg(this->localId)
		.arg(result)
		.end();*/
	DAQ_DEBUG("rapidio", "<<< readLocalId");
	return 0;
}


/*******************  FUNCTION  *********************/
void  DriverRapidIO::initializeCMBuffers(struct RIOClientConnection & conn)
{
	DAQ_DEBUG("rapidio", ">>> initializeCMBuffers");
	if(!conn.msg_rx_buf) {
		conn.msg_rx_buf = (rapidio_mport_socket_msg *) malloc(sizeof(rapidio_mport_socket_msg));
	}

	if(!conn.msg_tx_buf) {
		int res = riomp_sock_request_send_buffer(conn.skt,
				&(conn.msg_tx_buf));

		RIO_CHECK_STATUS("riomp_sock_request_send_buffer", res);
		memset(conn.msg_tx_buf, 0, DAQ_DRIVER_RAPIDIO_CM_MAX_BUF_SIZE);
	}
	DAQ_DEBUG("rapidio", "<<< initializeCMBuffers");
}

/*******************  FUNCTION  *********************/
void  DriverRapidIO::sendCMMsg(struct RIOClientConnection & conn, const void * buf, size_t size )
{
	DAQ_DEBUG("rapidio", ">>> sendCMMsg");
	/* Place message into buffer with space reserved for msg_header */
	void * tmp_msg_tx_buf = (char *)conn.msg_tx_buf + DAQ_DRIVER_RAPIDIO_CM_HEADER_SIZE;
	size_t len = size>DAQ_DRIVER_RAPIDIO_CM_PAYLOAD_SIZE? DAQ_DRIVER_RAPIDIO_CM_PAYLOAD_SIZE : size;
	memcpy(tmp_msg_tx_buf, buf, len);

	int res = riomp_sock_send(conn.skt, conn.msg_tx_buf, sizeof(*conn.msg_tx_buf), 0); //TODO SIMA - last param, retry
	if(res == 16) {
		res = riomp_sock_send(conn.skt, conn.msg_tx_buf, sizeof(*conn.msg_tx_buf), 0); //TODO SIMA - last param, retry
	}
	memset(conn.msg_tx_buf, 0, DAQ_DRIVER_RAPIDIO_CM_MAX_BUF_SIZE);
	RIO_CHECK_STATUS("riomp_sock_send", res);
	this->msgSent++;
	DAQ_DEBUG("rapidio", "<<< sendCMMsg");
}

/*******************  FUNCTION  *********************/
int  DriverRapidIO::recvCMMsg(struct RIOClientConnection & conn, void * buf, size_t size)
{
	DAQ_DEBUG("rapidio", ">>> recvCMMsg");
	int res = riomp_sock_receive(conn.skt, &(conn.msg_rx_buf),
			DAQ_DRIVER_RAPIDIO_CM_RECV_TIMEOUT, 0); // TODO SIMA - last param, retry
	if(res !=  0) {
		/* If we time out, we may want to retry. Let the caller handle the errors*/
		return res;
	}

	// disregard CM message header
	void * tmp_msg_rx_buf =	(char *)conn.msg_rx_buf + DAQ_DRIVER_RAPIDIO_CM_HEADER_SIZE;
	size_t len = size>DAQ_DRIVER_RAPIDIO_CM_PAYLOAD_SIZE? DAQ_DRIVER_RAPIDIO_CM_PAYLOAD_SIZE : size;
	memcpy(buf, tmp_msg_rx_buf, len);
	memset(conn.msg_rx_buf, 0, DAQ_DRIVER_RAPIDIO_CM_MAX_BUF_SIZE);
	DAQ_DEBUG("rapidio", "<<< recvCMMsg");
	return res;
}

/*******************  FUNCTION  *********************/
void  DriverRapidIO::createThreads()
{
	DAQ_DEBUG("rapidio", ">>> createThreads");
	/* Receive is blocking, so we need one thread for each connection
	 * to handle the receives
	 * For send we will use a consumer thread and a GoChannel to put
	 * incoming tasks
	 * */
	this->cmdRecvThread = new std::thread[this->worldSize];
	this->cmdSendThread = new std::thread[this->worldSize];
	this->cmdSendChan = new GoChannel<std::function<void()>>[this->worldSize];
	this->rdmaRecvThread = new std::thread[this->worldSize];
	this->rdmaSendChan = new GoChannel<std::function<void()>>[this->worldSize];
	this->rdmaSendThread = new std::thread[this->worldSize];
	DAQ_DEBUG("rapidio", "<<< createThreads");
}

/*******************  FUNCTION  *********************/
void  DriverRapidIO::finalize()
{
	DAQ_DEBUG("rapidio", ">>> finalize");
	// Close client connections
	for(int i = 0; i<this->worldSize; ++i) {
		// Skip clientSkt[rank] since that's us and the connection is empty
		if( i != this->transport->getRank()) {
			this->closeClientConnection(i);
		}
	}

	// Close server socket
	int res = riomp_sock_close(&(this->serverSkt.skt));
	RIO_CHECK_STATUS("riomp_sock_close", res);
	this->serverSkt.skt = nullptr;

	if(this->serverSkt.mbox) {
		res = riomp_sock_mbox_destroy_handle(&(this->serverSkt.mbox));
		RIO_CHECK_STATUS("riomp_sock_mbox_destroy_handle", res);
		this->serverSkt.mbox = nullptr;
	}

	// Deallocate all memory that has not allready been deallocated
	// Need to have a temp iterator, since deallocateRdmaSegment will
	// erase the iterator already
	// TODO maybe there is a better solution?
	std::map<void *, RIODMASegment>::iterator it = allocations.begin();
	std::map<void *, RIODMASegment>::iterator tmpIt = allocations.begin();
	while(it != allocations.end()) {
		tmpIt = it++;
		this->deallocateRdmaSegment(tmpIt->first, tmpIt->second.askSize, tmpIt->second.write);
	}

	finalizeMempool();

	if(this->mportHnd) {
		res = riomp_mgmt_mport_destroy_handle(&(this->mportHnd));
		RIO_CHECK_STATUS("riomp_mgmt_mport_destroy_handle", res);
		this->mportHnd = nullptr;
	}

	DAQ_DEBUG_ARG("rio-conn", "Number of messages sent %1")
		.arg(this->msgSent)
		.end();

	delete[] clientSkt;
	delete[] cmdRecvThread;
	delete[] cmdSendThread;
	delete[] cmdSendChan;
	//TODO: remove the recv threads - they are not needed
	delete[] rdmaRecvThread;
	delete[] rdmaSendThread;
	delete[] rdmaSendChan;
	DAQ_DEBUG("rapidio", "<<< finalize");
}

/*******************  FUNCTION  *********************/
void DriverRapidIO::closeClientConnection(int conn)
{
	DAQ_DEBUG("rapidio", ">>> closeClientConnection");
	int res = 0;
	if(this->clientSkt[conn].skt) {

		if(this->clientSkt[conn].msg_tx_buf) {
			res = riomp_sock_release_send_buffer(this->clientSkt[conn].skt,
					this->clientSkt[conn].msg_tx_buf);
			RIO_CHECK_STATUS("riomp_sock_release_send_buffer", res);
			this->clientSkt[conn].msg_tx_buf = nullptr;
		}

		if(this->clientSkt[conn].msg_rx_buf) {
			res = riomp_sock_release_receive_buffer(this->clientSkt[conn].skt,
					this->clientSkt[conn].msg_rx_buf);
			RIO_CHECK_STATUS("riomp_sock_release_receive_buffer", res);
		}
		this->clientSkt[conn].msg_rx_buf = nullptr;

		res = riomp_sock_close(&(this->clientSkt[conn].skt));
		RIO_CHECK_STATUS("riomp_sock_close", res);
		this->clientSkt[conn].skt = nullptr;
	}


	if(this->clientSkt[conn].mbox) {
		res = riomp_sock_mbox_destroy_handle(&(this->clientSkt[conn].mbox));
		RIO_CHECK_STATUS("riomp_sock_mbox_destroy_handle", res);
		this->clientSkt[conn].mbox = nullptr;
	}
	DAQ_DEBUG("rapidio", "<<< closeClientConnection");
}


/*******************  FUNCTION  *********************/
void  DriverRapidIO::registerSegment(void * addr,size_t size)
{
//	DAQ_DEBUG("rapidio", ">>> registerSegment");
//	DAQ_DEBUG("rapidio", "<<< registerSegment");
}

/*******************  FUNCTION  *********************/
void DriverRapidIO::unregisterSegment(void * addr,size_t size)
{
//	DAQ_DEBUG("rapidio", ">>> unregisterSegment");
//	DAQ_DEBUG("rapidio", "<<< unregisterSegment");
}

/*******************  FUNCTION  *********************/
uint64_t DriverRapidIO::getSegmentRemoteKey(void* addr, size_t size)
{
//	DAQ_DEBUG("rapidio", ">>> getSegmentRemoteKey");
//	DAQ_DEBUG("rapidio", "<<< getSegmentRemoteKey");
	return 0;
}

/*******************  FUNCTION  *********************/
void DriverRapidIO::postWaitRemoteRDMAWrite(Unit* srcUnit, int rank, int unitId, void* ptr,
				size_t size, int uuid, const DAQ::UnitCommand& postCommand)
{
	DAQ_DEBUG("rapidio", ">>> postWaitRemoteRDMAWrite");
	DAQ_DEBUG_ARG("rapidio", "postWaitRemoteRDMAWrite srcUnit %1, rank %2, unitId %3, ptr %4, size %5, uuid %6")
		.arg(srcUnit)
		.arg(rank)
		.arg(unitId)
		.arg(ptr)
		.arg(size)
		.arg(uuid)
		.end();

	uint64_t guuid = this->getGlobalUUID(rank, uuid);
	{
		std::lock_guard<std::mutex> guard(rdmaPostCommandMapLock);
		DAQ_DEBUG_ARG("rapidio","postWaitRemoteRDMAWrite rdmaRemoteWriteNotif NEW uuid=%1")
			.arg(guuid)
			.end();
		/* Check if the request is already pending, if yes, the postCommand
		 * can be sent directly. Otherwise, put the postCommand in the map
		 * and wait for completion. */
		std::list<uint64_t>::iterator it = std::find(rdmaPendingList.begin(), rdmaPendingList.end(), guuid);
		if(it != rdmaPendingList.end()) {
			rdmaPendingList.erase(it);
			transport->sendCommand(postCommand);
		} else {
			assume(this->rdmaPostCommandMap.find(guuid) == this->rdmaPostCommandMap.end(),
					"Notification for uuid already exists!");
			this->rdmaPostCommandMap[guuid] = postCommand;
		}
	}
	DAQ_DEBUG("rapidio", "<<< postWaitRemoteRDMAWrite");

}

/*******************  FUNCTION  *********************/
/* 
 * \param src is the local transmit buffer, NOTE: this does not have to be a base address, i.e. it can be offset already!
 * \param dest is used to denote the offset within the (remote) target buffer
 * \param destKey remote target address
 *
 * TODO ask IDT if it's possible to offset remote address. Does the offset have to be
 * the same on source and target buffer? (kernel buffer operations seem to imply that)
 * */
void DriverRapidIO::rdmaWrite(Unit* srcUnit, int rank, int unitId, void* src, void* dest,
				uint64_t destKey, size_t size, int uuid,
				const UnitCommand& postCommand)
{
	DAQ_DEBUG("rapidio", ">>> rdmaWrite");
	int destId = std::stoi(launcher->get("destId", rank));
	uint32_t offset = (uint32_t)((uint64_t)dest); //(uint32_t)(((uint64_t)dest)<<32);

	DAQ_DEBUG_ARG("rapidio2", "rdmaWrite request to destId %1 src %2 destKey %3 offset %4 dest %5")
		.arg(destId)
		.arg(src)
		.arg((void *)destKey)
		.arg(offset)
		.arg(dest)
		.end();

	// Find local transmit buffer info
	RIODMASegment * srcSegm = findAllocatedSegment(src, size);

	DAQ_DEBUG_ARG("rapidio2", "rdmaWrite segment found local transmit buffer %1 srcSegm.write %2\n")
		.arg(srcSegm->buf)
		.arg(srcSegm->write)
		.end();

	// It is possible to use the inbound window as source buffer, but we don't.
	// Right now, let us differ between the incoming and outgoing buffers.
	assume(srcSegm->write == false,
			"Cannot use source segment: is a write buffer, i.e. an inbound window.");

	DAQ_DEBUG_ARG("rapidio", "rdmaWrite unitId %1 from rank %2 where uuid %3")
		.arg(unitId)
		.arg(rank)
		.arg(uuid)
		.end();

	uint64_t guuid = this->getGlobalUUID(this->transport->getRank(), uuid);
	DAQ_DEBUG_ARG("rapidio", "rdmaWrite creating task with guuid %1")
		.arg(guuid)
		.end();
	std::function<void()> task = [=]()
	{
		DAQ_DEBUG("rapidio", ">>> taskRdmaSend");
		// to notify the sender when we're done
		// TODO should this be done after the notification to the receiver?
		writeDMA(destId, destKey, offset, src, size);
		transport->sendCommand(postCommand); 

		// to notify the receiver about the incoming RDMA write operation
		UnitCommand notifyCommand;
		notifyCommand.type =  UNIT_CMD_DRIVER_NOTIFY_RDMA;
		notifyCommand.args.rdma.guuid = guuid;
		DAQ_DEBUG_ARG("rapidio", "taskRdmaSend guuid %1")
			.arg(notifyCommand.args.rdma.guuid)
			.end();
		transport->sendCommand(srcUnit, rank, unitId, notifyCommand);
		DAQ_DEBUG("rapidio", "<<< taskRdmaSend");
	};
	rdmaSendChan[rank] << task;
	DAQ_DEBUG("rapidio", "<<< rdmaWrite");
}

/*******************  FUNCTION  *********************/
/*
 * Note that offset IS used for the target buffer!
 * Note that offset IS used for the source buffer in the case of KERNEL space buffers!
 * TODO add separate offset parameter for kernel space buffers
 * Note that offset IS NOT used for the source buffer in the case of USER space buffers!
 * (User space buffers should already be offset)
 */
void DriverRapidIO::writeDMA(int destId, uint64_t targetAddress, uint32_t offset, void * src, size_t size)
{
	DAQ_DEBUG("rapidio", ">>> writeDMA");
	DAQ_DEBUG_ARG("rapidio2", "writeDMA destId %1 targetAddress %2 offset %3 src %4 size %5 kbufMode %6")
		.arg(destId)
		.arg(targetAddress)
		.arg(offset)
		.arg(src)
		.arg(size)
		.arg(this->kbufMode)
		.end();
	DAQ_DEBUG_ARG("rapidio2", "writeDMA *((size_t*)src) == %1")
		.arg(*((size_t *)src))
		.end();

	if(this->kbufMode) { //kernel space buffer
/*		DAQ_DEBUG("rapidio", "Writing from kernel allocated buffer");
		DAQ_DEBUG_ARG("rapidio", "offset %1 tgtAddr %2")
			.arg(offset)
			.arg(targetAddress)
			.end();*/
		//TODO: ask IDT - how do all these offsets work?
		// For target? For source? for both?
		DAQ_FATAL("Kernel buffers not supported yet!");
	} else { //user space buffer
		DAQ_DEBUG("rapidio", "Writing from user allocated buffer");
		int res = riomp_dma_write(this->mportHnd, destId,
				targetAddress + offset, // destination target address
				(char *)src, // local transmit buffer, already offset
				size,
				RIO_DIRECTIO_TYPE_NWRITE, RIO_DIRECTIO_TRANSFER_SYNC,
				nullptr);
		assumeArg(res == 0,"Failed: %1 ERR %2 errno %3 when writing to destid %d").arg("riomp_dma_write").arg(res).argStrErrno().arg(destId).end();
		//RIO_CHECK_STATUS("riomp_dma_write", res);

		/*
		 * usleep(10000);
		 * where did all of this come from?!
		char * tmp[16];
		res = riomp_dma_read(this->mportHnd, destId, targetAddress + offset,
				tmp, 16, RIO_DIRECTIO_TRANSFER_SYNC);
		RIO_CHECK_STATUS("riomp_dma_read", res);
		DAQ_DEBUG_ARG("rapidio2", "writeDMA reading back data: *((size_t*)tmp) == %1")
			.arg(*((size_t *)tmp))
			.end();
*/
	}
	DAQ_DEBUG("rapidio", "<<< writeDMA");
}

/*******************  FUNCTION  *********************/
void DriverRapidIO::postCommandRecv()
{

//	DAQ_DEBUG("rapidio", ">>> postCommandRecv");
//	DAQ_DEBUG("rapidio", "<<< postCommandRecv");
}

/*******************  FUNCTION  *********************/
void DriverRapidIO::sendRemoteCommand(const UnitCommand & command)
{
	DAQ_DEBUG("rapidio", ">>> sendRemoteCommand");
	DAQ_DEBUG_ARG("rapidio", "Sending command.src.rank %1 command.dest.rank %2")
		.arg(command.src.rank)
		.arg(command.dest.rank)
		.end();
	assumeArg(sizeof(UnitCommand) < DAQ_DRIVER_RAPIDIO_CM_MAX_BUF_SIZE ,
			"Failed: Command > MAX_BUF_SIZE(4KB).");
	// TODO Do we really need to copy this?
	UnitCommand cmd = command;
	std::function<void()> task = [=]()
	{
		this->sendCMMsg(clientSkt[cmd.dest.rank], &cmd, sizeof(UnitCommand));
		DAQ_DEBUG_ARG("rapidio", "Sending command %1 from %2 to %3")
			.arg(cmd.type)
			.arg(cmd.src.rank)
			.arg(cmd.dest.rank)
			.end();
	};

	cmdSendChan[command.dest.rank] << task;
	DAQ_DEBUG("rapidio", "<<< sendRemoteCommand");

}

/*******************  FUNCTION  *********************/
/*  When this unit wants someone to write to one of its buffers,
 *  it will take an address within that buffer and request the Remote IOV for it.
 *  This information is then sent to the remote endpoint from which this unit
 *  wants something. So the RemoteIOV is always for the RX buffer.
 *  \returns RemoteIOV.addr = offset within receive buffer
 *  \returns RemoteIOV.key = receive buffer target (physical) address
 * */
RemoteIOV DriverRapidIO::getRemoteIOV ( void* addr, size_t size )
{
	DAQ_DEBUG("rapidio", ">>> getRemoteIOV");
	DAQ_DEBUG_ARG("rapidio", "getRemoteIOV: Looking for match for src %1 size %2")
		.arg(addr)
		.arg(size)
		.end();
	RemoteIOV iov = {0,0};

	RIODMASegment * segm = findAllocatedSegment(addr, size);;
	// Calculate offset
	iov.addr = (void*)((char*)addr - (char*)segm->buf);
	iov.key = segm->tgtAddr; //NOTE: there is no notification if we write to a bad address on the receiver end!!
	DAQ_DEBUG_ARG("rapidio2", "getRemoteIOV: Found matching segment buf %1 tgtAddr %2 askSize %3")
		.arg(segm->buf)
		.arg((void*)segm->tgtAddr)
		.arg(segm->askSize)
		.end();
	DAQ_DEBUG_ARG("rapidio2", "getRemoteIOV: iov.key (target address) %1 iov.addr (offset) %2")
		.arg((void*)iov.key)
		.arg(iov.addr)
		.end();
	DAQ_DEBUG("rapidio", "<<< getRemoteIOV");
	return iov;
}

/*******************  FUNCTION  *********************/
void DriverRapidIO::startThreads(void )
{
	DAQ_DEBUG("rapidio", ">>> startThreads");

	this->cmdRecvCont = true;
	this->rdmaRecvCont = true;
	for (int i = 0 ; i < this->worldSize; i++) {
		// Skip clientSkt[rank] since that's us and the connection is empty
		if( i != this->transport->getRank()) {
			this->cmdRecvThread[i] = std::thread([this, i](){this->cmdRecvMain(i);});
			this->cmdSendThread[i] = std::thread([this, i](){this->cmdSendMain(i);});
			// We don't really need the thread for RDMA receive - the data is written
			// directly to our memory. But for the sake of consistency, we keep it like this.
			this->rdmaRecvThread[i] = std::thread([this, i](){this->rdmaRecvMain(i);});
			this->rdmaSendThread[i] = std::thread([this, i](){this->rdmaSendMain(i);});
		}
	}

	DAQ_DEBUG("rapidio", "<<< startThreads");
}

/*******************  FUNCTION  *********************/
void DriverRapidIO::cmdRecvMain(int conn)
{
	DAQ_DEBUG("rapidio", ">>> cmdRecvMain");
	while(this->cmdRecvCont) {
		struct UnitCommand cmd;
		DAQ_DEBUG_ARG("rapidio", "Receiving commands for connection %1")
			.arg(conn)
			.end();
		int res = this->recvCMMsg(clientSkt[conn], &cmd, sizeof(cmd));

		/* The timeout in recvCMMsg is to make sure that we return once in a while
		 * to have a look at this->cmdRecvCont, otherwise we would block forever there
		 * and not know we should exit. This way we get to have a look at the loop 
		 * variable and we can stop if needed. */
		if(res == ETIME) {
			DAQ_INFO_ARG("[rapidio] Receive timed out for connection with rank %1")
			.arg(conn)
			.end();
			continue;
		} else {
			assumeArg(res == 0,"Failed: %1 ERR %2 errno %3 when receiving from from rank %d").arg("recvCMMsg->riomp_sock_receive").arg(res).argStrErrno().arg(conn).end();
			//RIO_CHECK_STATUS("recvCMMsg->riomp_sock_receive",res);
		}
		DAQ_DEBUG_ARG("rapidio", "Received command of type %1 from rank %2 on connection %3")
			.arg(cmd.type)
			.arg(cmd.src.rank)
			.arg(conn)
			.end();

		// intercept if it is a RDMA notification. We'll send a matching
		// postCommand from this call branch
		if(cmd.type == UNIT_CMD_DRIVER_NOTIFY_RDMA) {
			DAQ_DEBUG_ARG("rapidio", "Received notification for guuid %1")
			.arg(cmd.args.rdma.guuid)
			.end();
			this->notifyRdmaFinish(cmd.args.rdma.guuid);
		}
		else {
			transport->sendCommand(cmd);
		}
	}
	DAQ_DEBUG("rapidio", "<<< cmdRecvMain");
}

/*******************  FUNCTION  *********************/
void DriverRapidIO::cmdSendMain(int conn)
{
	DAQ_DEBUG("rapidio", ">>> cmdSendMain");
	std::function<void()> task;
	while (task << cmdSendChan[conn])
	{
		task();
	}
	DAQ_DEBUG("rapidio", "<<< cmdSendMain");
}

/*******************  FUNCTION  *********************/
void DriverRapidIO::rdmaRecvMain(int conn)
{
	DAQ_DEBUG("rapidio", ">>> rdmaRecvMain");
// Keep this commented out for now as it does not do anything.
//	while(this->rdmaRecvCont) {
		// Need this sleep in order to not get bug with optimization flags
//		usleep(10);
		//Nothing really to do here?
		//The sender will write directly into our buffer
//	}
	DAQ_DEBUG("rapidio", "<<< rdmaRecvMain");
}

/*******************  FUNCTION  *********************/
void DriverRapidIO::rdmaSendMain(int conn)
{
	DAQ_DEBUG("rapidio", ">>> rdmaSendMain");
	std::function<void()> task;
	while (task << rdmaSendChan[conn])
	{
		task();
	}
	DAQ_DEBUG("rapidio", "<<< rdmaSendMain");
}

/*******************  FUNCTION  *********************/
uint64_t DriverRapidIO::getGlobalUUID(int rank,int uuid)
{
	DAQ_DEBUG("rapidio", ">>> getGlobalUUID");
	uint64_t fulltag = (uint64_t)((uint32_t)rank) + (((uint64_t)uuid) << 32);
	DAQ_DEBUG_ARG("rapidio", "GlobalUUID == %1")
		.arg(fulltag)
		.end();
	DAQ_DEBUG("rapidio", "<<< getGlobakUUID");
	return fulltag;
}

/*******************  FUNCTION  *********************/
void DriverRapidIO::notifyRdmaFinish(uint64_t guuid)
{
	DAQ_DEBUG("rapidio", ">>> notifyRdmaFinish");

	DAQ_DEBUG_ARG("rapidio", "notifyRdmaFinish: guuid == %1")
		.arg(guuid)
		.end();
	{
		/* Check if we already have a postCommand for this operation,
		 * if not, the request has come before the operation itself.
		 * Save it in the pending list for later processing.
		 */
		std::lock_guard<std::mutex> guard(rdmaPostCommandMapLock);
		std::map<uint64_t, UnitCommand>::iterator it =
			this->rdmaPostCommandMap.find(guuid);
		if( it == this->rdmaPostCommandMap.end()) {
			rdmaPendingList.push_back(guuid);
		} else {
			transport->sendCommand(it->second);
			this->rdmaPostCommandMap.erase(it);
		}
	}
	DAQ_DEBUG("rapidio", "<<< notifyRdmaFinish");
}

/*******************  FUNCTION  *********************/
void DriverRapidIO::stop()
{
	DAQ_DEBUG("rapidio", ">>> stop");
	this->cmdRecvCont = false;
	this->rdmaRecvCont = false;

	for (int i = 0 ; i < this->worldSize ; i++)
	{
		// Skip clientSkt[rank] since that's us and the thread was never started
		if( i != this->transport->getRank())
		{
			this->cmdSendChan[i].close();
			this->rdmaSendChan[i].close();
		}
	}
	for (int i = 0 ; i < this->worldSize ; i++)
	{
		// Skip clientSkt[rank] since that's us and the thread was never started
		if( i != this->transport->getRank())
		{
			this->cmdRecvThread[i].join();
			this->cmdSendThread[i].join();
			this->rdmaRecvThread[i].join();
			this->rdmaSendThread[i].join();
		}
	}
	/* Wait for everyone so that we can close the connections in a controlled manner
	 * else we will get ECONNRESET for some endpoints */
	this->launcher->barrier();
	DAQ_DEBUG("rapidio", "<<< stop");
}

/*******************  FUNCTION  *********************/
void DriverRapidIO::startNoThreads()
{
	DAQ_FATAL("Starting with no threads is not supported.");
}

/*******************  FUNCTION  *********************/
void DriverRapidIO::updateNoThread()
{

}

/*******************  FUNCTION  *********************/
void DriverRapidIO::main()
{

}

/*******************  FUNCTION  *********************/
 /* write = true -> allocate Rx buffer
  * write = false -> allocate Tx buffer
  * */
void * DriverRapidIO::allocateRdmaSegment(size_t size, bool write)
{
	DAQ_DEBUG("rapidio",">>> allocateRdmaSegment");
	void * tmpSegment = nullptr;
	if(write) {
		tmpSegment = this->allocDMARx(size);
	} else {
		tmpSegment = this->allocDMATx(size);
	}
	memset(tmpSegment, 0, size);
	DAQ_DEBUG("rapidio","<<< allocateRdmaSegment");
	return tmpSegment;
}

/*******************  FUNCTION  *********************/
void DriverRapidIO::deallocateRdmaSegment(void * ptr, size_t size, bool write)
{
	DAQ_DEBUG("rapidio",">>> deallocateRdmaSegment");
	void * tmpSegment = nullptr;
	if(write) {
		tmpSegment = this->deallocDMARx(ptr, size);
	} else {
		tmpSegment = this->deallocDMATx(ptr, size);
	}
	DAQ_DEBUG("rapidio","<<< deallocateRdmaSegment");
}

/*******************  FUNCTION  *********************/
/* For calls to riomp_dma_ibwin_map:
 * rio_base (parameter name in header file, tgtAddr in this code) is
 * BOTH an IN and an OUT parameter.
 * [IN] By setting rio_base to a known address, you request that your mapping be
 * done to that specific address. Otherwise RIO_MAP_ANY_ADDR can be used.
 * [OUT] The return value of rio_base is a valid 64 bit (physical) address to
 * where the mapping is.
 *
 * Rx == receive buffer, means that there will be DMA writes to it ->
 * RIODMASegment.write = true.
 */
void * DriverRapidIO::allocDMARx(size_t size)
{
	DAQ_DEBUG("rapidio",">>> allocDMARx");
/*	DAQ_DEBUG_ARG("rapidio", "Allocating RX for size %1")
		.arg(size)
		.end();*/

	/* RIO_MAP_ANY_ADDR = signal the RIO lib that we can be mapped wherever
	 * RIODMASegment = { askSize, allocSize, write, buf, tgtAddr } */
	RIODMASegment tmpSegm = { size, 0, true, 0, RIOMP_MAP_ANY_ADDR };

	// TODO: do we still need to do this? + should we check alloc sizes?
	size_t power = this->roundUpToPowerOfTwo(size);
	offset_t offset = 0;
	tmpSegm.allocSize = power;

	tmpSegm.buf = (void *)mp_alloc(this->mempool, tmpSegm.allocSize, &offset);
	assume(tmpSegm.buf != nullptr, "FATAL: mp_alloc returned nullptr");
	tmpSegm.tgtAddr = this->resMemTgtAddr + offset;

/*	DAQ_DEBUG_ARG("rapidio", "mempool ibwin tgt_addr: %1 memory map: %2")
			.arg(tmpSegm.tgtAddr)
			.arg(tmpSegm.buf)
			.end();*/

	allocations[tmpSegm.buf] = tmpSegm;
	DAQ_DEBUG_ARG("rapidio2", "ALLOCATED DMA RX buffer %1 tgtAddr %2 askSize %3 allocSize %4")
		.arg(allocations[tmpSegm.buf].buf)
		.arg((void*)allocations[tmpSegm.buf].tgtAddr)
		.arg(allocations[tmpSegm.buf].askSize)
		.arg(allocations[tmpSegm.buf].allocSize)
		.end();

	DAQ_DEBUG("rapidio", "<<< allocDMARx");
	return tmpSegm.buf;
}

/*******************  FUNCTION  *********************/
/* Tx == transmit buffer, means that we will not do DMA
 * writes to it -> RIODMASegment.write = false.
 */
void * DriverRapidIO::allocDMATx(size_t size)
{
	DAQ_DEBUG("rapidio", ">>> allocDMATx");

	uint32_t maxHwSize; /* max DMA transfer size supported by HW */

	// { askSize, allocSize, write, buf, tgtAddr }
	RIODMASegment tmpSegm = { size, 0 , false, 0, 0 };
	size_t power = this->roundUpToPowerOfTwo(size);
	tmpSegm.allocSize = power;

	if(this->kbufMode) {
		maxHwSize = this->maxSize;
	}
	else {
		maxHwSize = (this->maxSge == 0) ? DAQ_DRIVER_RAPIDIO_DMA_DEF_WIN_SIZE : (this->maxSge * getpagesize());
	}

	// calls are different depending on which mode we use
	if (this->kbufMode) {
		DAQ_FATAL("Kernel buffer mode NOT supported");
	}
	else {
//		DAQ_DEBUG("rapidio", "allocating user space buffer");
		tmpSegm.buf = malloc(tmpSegm.allocSize);
		//TODO check nullptr return value from malloc
	}

	allocations[tmpSegm.buf] = tmpSegm;
	DAQ_DEBUG_ARG("rapidio2", "ALLOCATED DMA TX buffer %1 askSize %2 allocSize %3 kbufMode %4")
		.arg(allocations[tmpSegm.buf].buf)
		.arg(allocations[tmpSegm.buf].askSize)
		.arg(allocations[tmpSegm.buf].allocSize)
		.arg(this->kbufMode)
		.end();
	DAQ_DEBUG("rapidio", "<<< allocDMATx");
	return tmpSegm.buf;
}

/*******************  FUNCTION  *********************/
/* Rx = incoming buffer, will get DMA writes -> RIODMASegment.write = true */
void * DriverRapidIO::deallocDMARx(void * ptr, size_t size)
{
	DAQ_DEBUG("rapidio", ">>> deallocDMARx");

	std::map<void *, RIODMASegment>::iterator it = allocations.find(ptr);

	if(it == allocations.end()) {
		DAQ_DEBUG_ARG("rapidio", "No such segment: %1")
			.arg(ptr)
			.end();
	}
	else {
		RIODMASegment tmpSegm = it->second;

		// double check values so we release the correct segment
		assume(tmpSegm.buf != nullptr, "Can't free, segment pointer is nullptr");
		assume(tmpSegm.write == true,
				"Can't free, wrong type of segment: expected Rx but got Tx");
		assume(tmpSegm.askSize == size, "Can't free, size mismatch");

		mp_dispose(this->mempool, (char *)ptr);
		allocations.erase(it);
	}

	DAQ_DEBUG("rapidio", "<<< deallocDMARx");
	// TODO what to return here?
	return ptr;
}

/*******************  FUNCTION  *********************/
/* Tx = outgoing buffer, no DMA write -> RIODMASegment.write = false */
void * DriverRapidIO::deallocDMATx(void * ptr, size_t size)
{
	DAQ_DEBUG("rapidio", ">>> deallocDMATx");

	std::map<void *, RIODMASegment>::iterator it = allocations.find(ptr);

	if(it == allocations.end()) {
		DAQ_DEBUG_ARG("rapidio", "No such segment: %1")
			.arg(ptr)
			.end();
	}
	else {
		RIODMASegment tmpSegm = it->second;
		// double check values so we release the correct segment
		assume(tmpSegm.buf != nullptr, "Can't free, segment pointer is nullptr.");
		assume(tmpSegm.write == false,
				"Can't free, wrong type of segment: expected Tx but got Rx");
		assume(tmpSegm.askSize == size, "Can't free, size mismatch");

		if(this->kbufMode) {
			DAQ_FATAL("Kernel buffer mode NOT supported");
		}
		else {
			/* User allocated buffer */
			free(tmpSegm.buf);
		}
		allocations.erase(it);
	}
	DAQ_DEBUG("rapidio", "<<< deallocDMATx");
	// TODO what to return here?
	return ptr;
}

/*******************  FUNCTION  *********************/
/* compute the next highest power of 2 of 32-bit v
 * http://graphics.stanford.edu/~seander/bithacks.html#RoundUpPowerOf2 */
size_t DriverRapidIO::roundUpToPowerOfTwo(size_t v)
{
	//we're assuming a 64 bit system...
	v--;
	v |= v >> 1;
	v |= v >> 2;
	v |= v >> 4;
	v |= v >> 8;
	v |= v >> 16;
	v |= v >> 32;
	v++;
	return v;
}

/*******************  FUNCTION  *********************/
/* compute the next highest power of 2 of 32-bit v
 * http://graphics.stanford.edu/~seander/bithacks.html#RoundUpPowerOf2 */
size_t DriverRapidIO::roundUpToMultiple4K(size_t size)
{

	if((size % 4096) != 0) {
		size = size + 4096 - (size % 4096);
	}

	return size;
}


/*******************  FUNCTION  *********************/
RIODMASegment * DriverRapidIO::findAllocatedSegment(void * addr, size_t size)
{

	/* The incoming address can be within a segment,
	 * need to find which segment it belongs to */
	RIODMASegment * segm = nullptr;
/*	DAQ_DEBUG_ARG("rapidio", "Starting search for RemoteIOV for address %1 and size %2")
		.arg(addr)
		.arg(size)
		.end();*/
	for (std::map<void *, RIODMASegment>::iterator it = allocations.begin();
			it != allocations.end() ; ++it) {

/*		DAQ_DEBUG_ARG("rapidio", "Looking at segment buf %1 size %2 tgtAddr %3")
			.arg(it->second.buf)
			.arg(it->second.askSize)
			.arg(it->second.tgtAddr)
			.end();*/

		if (it->second.buf <= addr && (char*)it->second.buf + it->second.askSize > addr) {
			if ((char*)addr+size > (char*)it->second.buf + it->second.askSize) {
				DAQ_WARNING("Caution, a segment from RapidIO does not completetly fit with the request which is larger.");
			}
			segm = &(it->second);
			break;
		}
	}
	assume(segm != nullptr, "FATAL: no matching segment was found!");
	return segm;
}

void DriverRapidIO::outputMyInfo(DAQ::Launcher& launcher)
{
	DAQ_INFO("----------- MY INFO -----------");
	DAQ_INFO_ARG("localId %1").arg(this->localId).end();
	DAQ_INFO_ARG("kbufMode %1").arg(this->kbufMode).end();

	DAQ_INFO("----------- MY RX SEGMENTS -----------");
	for (std::map<void *, RIODMASegment>::iterator it = allocations.begin();
			it != allocations.end() ; ++it) {
		if(it->second.write == true) {
			RIODMASegment segm = it->second;
			DAQ_INFO_ARG("Segment: tgtAddr %1 buf %2 askSize %4 allocSize %5 write %6").arg(segm.tgtAddr).arg(segm.buf).arg(segm.askSize).arg(segm.allocSize).arg(segm.write).end();
		}
	}
	DAQ_INFO("----------- MY TX SEGMENTS -----------");
	for (std::map<void *, RIODMASegment>::iterator it = allocations.begin();
			it != allocations.end() ; ++it) {
		if(it->second.write == false) {
			RIODMASegment segm = it->second;
			DAQ_INFO_ARG("Segment: tgtAddr %1 buf %2 askSize %3 allocSize %4 write %5").arg(segm.tgtAddr).arg(segm.buf).arg(segm.askSize).arg(segm.allocSize).arg(segm.write).end();
		}
	}
}

} // namespace DAQ
