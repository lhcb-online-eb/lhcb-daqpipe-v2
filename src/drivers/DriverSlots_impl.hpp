/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_DRIVER_SLOTS_IMPL_HPP
#define DAQ_DRIVER_SLOTS_IMPL_HPP

/********************  HEADERS  *********************/
#include "DriverSlots.hpp"
#include <unistd.h>

#include <iostream>

/********************  NAMESPACE  *******************/
namespace DAQ
{

/*******************  FUNCTION  *********************/
template <class T>
DriverSlots<T>::DriverSlots ( const std::string & name ) 
           :Driver(name)
{
	this->noAnySource = false;
	this->noAnySourceCmds = nullptr;
	this->noAnySourceReqs = nullptr;
	this->noRdmaRequest = false;
	this->onfly = 0;
	this->exitListener = false;
	this->hasNoThreads = false;
	this->noRdmaAck = false;
	for (int i = 0 ; i < DRIVER_SLOTS_MAX_ONFLY ; i++)
	{
		inUse[i] = false;
		taskType[i] = TASK_TYPE_NULL;
	}
	for (int i = 0 ; i < TASK_TYPE_COUNT ; i++)
		countType[i] = 0;
	
	//here we play with the condition to manage correct scheduling
	selector.subscibe(&chRdmaWrite,[this]() -> bool{return this->hasEmptySlots() && this->countType[TASK_TYPE_RDMA_SEND] < this->maxRdmaSend;});
	selector.subscibe(&chWaitRdmaWrite,[this]() -> bool{return this->hasEmptySlots() && this->countType[TASK_TYPE_RDMA_RECV] < this->maxRdmaRecv;});
	selector.subscibe(&chSendCommand,[this]() -> bool{return this->hasEmptySlots();});
	selector.subscibe(&chWakeUpSender);
	selector.subscibe(&chListen);
	

}

/*******************  FUNCTION  *********************/
template <class T>
DriverSlots<T>::~DriverSlots ()
{
	if (noAnySourceReqs != nullptr)
		delete [] noAnySourceReqs;
	if (noAnySourceCmds != nullptr)
		delete [] noAnySourceCmds;
}

/*******************  FUNCTION  *********************/
template <class T>
void DriverSlots<T>::setNoAnySource() 
{
	this->noAnySource = true;
	this->noAnySourceCmds = new UnitCommand[this->cmdRecver * nodes];
	this->noAnySourceReqs = new T[this->cmdRecver * nodes];
}

/*******************  FUNCTION  *********************/
template <class T>
void DriverSlots<T>::doCommandRegistration()
{
	if (this->noAnySource)
		this->registerSegment(this->noAnySourceCmds,sizeof(UnitCommand)*this->cmdRecver * nodes);
	this->registerSegment(&commands,sizeof(UnitCommand)*DRIVER_SLOTS_MAX_ONFLY);
}

/*******************  FUNCTION  *********************/
template <class T>
void DriverSlots<T>::doCommandDeRegistration()
{
	if (this->noAnySource)
		this->unregisterSegment(this->noAnySourceCmds,sizeof(UnitCommand)*this->cmdRecver * nodes);
	this->unregisterSegment(&commands,sizeof(UnitCommand)*DRIVER_SLOTS_MAX_ONFLY);	
}

/*******************  FUNCTION  *********************/
template <class T>
void DriverSlots<T>::initialize ( const Config& config, Launcher& launcher, int argc, char** argv )
{
	//load config
	this->maxOnfly = config.getAndSetConfigInt("drivers.slots","maxOnFly",16);
	this->cmdRecver = config.getAndSetConfigInt("drivers.slots","cmdRecver",2);
	this->maxRdmaRecv = config.getAndSetConfigInt("drivers.slots","maxRdmaRecv",(this->maxOnfly-this->cmdRecver)/2);
	this->maxRdmaSend = config.getAndSetConfigInt("drivers.slots","maxRdmaSend",(this->maxOnfly-this->cmdRecver)/2);
	this->listenerSleep = config.getAndSetConfigUInt64("drivers.slots","listenerSleep",100);
	this->useTracer = config.useTracer;
	this->rank = launcher.getRank();
	this->nodes = launcher.getWorldSize();
	
	//checking
	assumeArg(this->maxOnfly > 0 && this->maxOnfly <= DRIVER_SLOTS_MAX_ONFLY,"Invalid value for mpi.maxOnFly : %1").arg(this->maxOnfly).end();
	assumeArg(this->cmdRecver > 0 && this->cmdRecver <= this->maxOnfly,"Invalid value for mpi.cmdRecver : %1").arg(this->cmdRecver).end();
	assumeArg(this->maxRdmaRecv > 0 && this->maxRdmaRecv <= this->maxOnfly,"Invalid value for mpi.maxRdmaRecv : %1").arg(this->maxRdmaRecv).end();
	assumeArg(this->maxRdmaSend > 0 && this->maxRdmaSend <= this->maxOnfly,"Invalid value for mpi.maxRdmaSend : %1").arg(this->maxRdmaSend).end();
	
	//init requests
	for (int i = 0 ; i < DRIVER_SLOTS_MAX_ONFLY ; i++)
		doInitRequest(requests[i]);
	
	#ifdef HAVE_HTOPML 
		
		DynamicConfigHttpNode::registerParam("maxRdmaSend",maxRdmaSend, 1,maxOnfly, [this](long value){
			this->maxRdmaSend=value;
			std::cout<<value;
		});
		DynamicConfigHttpNode::registerParam("maxRdmaRecv",maxRdmaRecv,1,maxOnfly,[this](long value){
			this->maxRdmaRecv = value;
			std::cout<<value;
		});
	#endif //HAVE_HTOPML
}

/*******************  FUNCTION  *********************/
template <class T>
void DriverSlots<T>::startThreads ()
{
	//post initial recv
	this->postInitialRecvCmd();
	
	//at least one listen
	this->chListen << true;
	
    //create thread
	this->threadSender = std::thread([this]() {this->mainSender();});
}

/*******************  FUNCTION  *********************/
template <class T>
void DriverSlots<T>::setNoRDMARequest()
{
	this->noRdmaRequest = true;
}

/*******************  FUNCTION  *********************/
template <class T>
void DriverSlots<T>::postInitialRecvCmd()
{
	if (noAnySource) {
		for (int i = 0 ; i < cmdRecver ; i++)
			for (int n = 0 ; n < nodes ; n++)
				if (n != rank)
					this->doPostCommandRecv(noAnySourceCmds[i * nodes + n],noAnySourceReqs[i * nodes + n],n);
		//not needed but to ensure same semantic in the rest of the code.
		for (int i = 0 ; i < cmdRecver ; i++)
			getUnusedId(TASK_TYPE_CMD_RECV);
	} else {
		//post initial recv
		for (int i = 0 ; i < cmdRecver ; i++) {
			int id = getUnusedId(TASK_TYPE_CMD_RECV);
			assert(id >= 0);
			//tracing
			if (useTracer)
				tracer.startComm(&requests[id],sizeof(UnitCommand),true,false);
			this->doPostCommandRecv(commands[id],requests[id]);
		}
	}
}

/*******************  FUNCTION  *********************/
template <class T>
void DriverSlots<T>::startNoThreads()
{
	this->chSendCommand.disableWaitOnEmpty();
	this->chRdmaWrite.disableWaitOnEmpty();
	this->chWaitRdmaWrite.disableWaitOnEmpty();
	this->chWakeUpSender.disableWaitOnEmpty();
	this->chListen.disableWaitOnEmpty();
	this->hasNoThreads = true;
	
	this->selector.disableWaitOnEmpty();
	
	this->postInitialRecvCmd();
}

/*******************  FUNCTION  *********************/
template <class T>
void DriverSlots<T>::stop ()
{
    //close channels
	chSendCommand.close();
	chRdmaWrite.close();
	chWaitRdmaWrite.close();
	chWakeUpSender.close();
	chListen.close();

	//wait threads
	if (!this->hasNoThreads)
		this->threadSender.join();
}

/*******************  FUNCTION  *********************/
template <class T>
void DriverSlots<T>::finalize ()
{
	
}

/*******************  FUNCTION  *********************/
template <class T>
bool DriverSlots<T>::hasEmptySlots ()
{
	assert(onfly >= 0);
	return onfly < maxOnfly;
}

/*******************  FUNCTION  *********************/
template <class T>
void DriverSlots<T>::mainSender ()
{
	//if no thread ensure to wake up chListen channel
	chListen << true;
	
	//loop
	GoChannelBase * ch;
	while((ch = selector.select())) {
		if (ch == &chSendCommand) {
			runSendCommand(*chSendCommand);
			chSendCommand.popUnlock();
		} else if (ch == &chRdmaWrite) {
			runRdmaWrite(*chRdmaWrite);
			chRdmaWrite.popUnlock();
		} else if (ch == &chWaitRdmaWrite) {
			runWaitRdmaWrite(*chWaitRdmaWrite);
			chWaitRdmaWrite.popUnlock();
		} else if (ch == &chListen) {
			//read
			bool tmp;
			int id = -1;
			tmp << chListen;
			//push to wake up next
			if (((!chListen.isClosed() || (chListen.isClosed() && onfly > this->cmdRecver)) && !hasNoThreads))
				chListen << true;
			//do listen
			if (onfly > 0) {
				id = doTest(requests,maxOnfly,(onfly == maxOnfly));
				if (id != -1)
					markDone(id);
			}
			//sleep a little bit if nothing to wait
 			//if (id == -1 || onfly == 0)
 			//	usleep(this->listenerSleep);
		} else if (ch == &chWakeUpSender) {
			//this is just to be used when we reach onfly==maxOnFly, to ensure wakeup
			bool tmp;
			tmp << chWakeUpSender;
		}
	}
}

/*******************  FUNCTION  *********************/
template <class T>
void DriverSlots<T>::updateNoThread()
{
	this->mainSender();
}

/*******************  FUNCTION  *********************/
template <class T>
void DriverSlots<T>::markDone ( int id )
{
	//checks
	assert(id >= 0 && id < maxOnfly);
	assert(inUse[id]);
	assert(taskType[id] != TASK_TYPE_NULL);
	
	//info
	DAQ_DEBUG_ARG("slots","Finish op %1 on slot %2").arg(taskType[id]).arg(id).end();

	//finish in tracer
	if (useTracer && taskType[id] != TASK_TYPE_NULL)
		tracer.finishComm(&requests[id]);
	
	//apply switch
	bool reset = true;
	switch(taskType[id]) {
		case TASK_TYPE_CMD_RECV:
			//send to transport
			DAQ_DEBUG_ARG("slots","Forward recieved command : %1 -> %2 (%3)").arg(commands[id].src).arg(commands[id].dest).arg(commands[id].type).end();
			if (commands[id].type == UNIT_CMD_DRIVER_NOTIFY_RDMA) {
				this->notifyRdmaFinish(commands[id].args.rdma.guuid);
			} else {
				this->transport->sendCommand(commands[id]);
			}
			//tracer
			if (useTracer)
				tracer.startComm(&requests[id],sizeof(UnitCommand),true,false);
			//republish
			this->doRepublishCommandRecv(commands[id],requests[id]);
			reset = false;
			break;
		case TASK_TYPE_CMD_SEND:
			//nothing to do
			break;
		case TASK_TYPE_RDMA_SEND:
			if (noRdmaAck)
				sendRdmaAck(pendingAck[id].rank,pendingAck[id].uuid);
		case TASK_TYPE_RDMA_RECV:
			//send feedback to transport
			DAQ_DEBUG_ARG("slots","Forward feedback command[%4] : %1 -> %2 (%3)").arg(commands[id].src).arg(commands[id].dest).arg(commands[id].type).arg(id).end();
			this->transport->sendCommand(commands[id]);
			break;
		default:
			DAQ_FATAL_ARG("Invalid task type : %1").arg(taskType[id]).end();
	}
	
	//update
	if (reset) {
		this->countType[this->taskType[id]]--;
		assert(this->countType[this->taskType[id]] >= 0);
		this->taskType[id] = TASK_TYPE_NULL;
		this->inUse[id] = false;
		doInitRequest(this->requests[id]);
		this->commands[id].src = this->commands[id].dest = buildUnitId(-1,-1);
		if (this->onfly == this->maxOnfly)
			chWakeUpSender << true;
		this->onfly--;
	}
	
	//we always need at least cmd wait channels
	assert(onfly >= this->cmdRecver);
}

/*******************  FUNCTION  *********************/
template <class T>
void DriverSlots<T>::postCommandRecv ()
{
	DAQ_FATAL("Must never be call, must be removed");
}

/*******************  FUNCTION  *********************/
template <class T>
void DriverSlots<T>::sendRemoteCommand ( const UnitCommand& command )
{
	chSendCommand << command;
}

/*******************  FUNCTION  *********************/
template <class T>
void DriverSlots<T>::runSendCommand (const UnitCommand & command)
{
	DAQ_DEBUG("driver-slots","Do command sending");

	//get request
	int freeId = getUnusedId(TASK_TYPE_CMD_SEND);
	assume(freeId >= 0,"Invalid free id");
	commands[freeId] = command;
	
	//tracing
	if (useTracer)
		tracer.startComm(&requests[freeId],sizeof(UnitCommand),true,true);
	
	//post actions
	this->doSendCommand(commands[freeId],this->requests[freeId]);
}

/*******************  FUNCTION  *********************/
template <class T>
int DriverSlots<T>::getUnusedId ( DAQ::DriverSlotsTaskType taskType)
{
	int id = -1;
	assert(onfly <= maxOnfly);
	
	//search
	for (int i = 0 ; i < this->maxOnfly ; i++) {
		if (inUse[i] == false) {
			id = i;
			break;
		}
	}
	
	//update status
	if (id != -1) {
		DAQ_DEBUG_ARG("slots","Start op %1 on slot %2").arg(taskType).arg(id).end();
		assert(this->taskType[id] == TASK_TYPE_NULL);
		inUse[id] = true;
		this->taskType[id] = taskType;
		this->countType[taskType]++;
		this->onfly++;
	} else {
		assert(onfly == maxOnfly);
	}

	//ok
	return id;
}

/*******************  FUNCTION  *********************/
template <class T>
void DriverSlots<T>::noAnySourceCmdFinsished(T & request)
{
	assume(noAnySource,"Get a noAnySource request when noAnySource is not enabled !");
	int id = &request - noAnySourceReqs;
	assumeArg(id >= 0 && id < nodes * cmdRecver,"Invalid request, not in expected range : %1 : %2 not in %3").arg(id).arg(&request).arg(noAnySourceReqs).end();

	//check type
	if (noAnySourceCmds[id].type == UNIT_CMD_DRIVER_NOTIFY_RDMA) {

		this->notifyRdmaFinish(noAnySourceCmds[id].args.rdma.guuid);
	} else {
		this->transport->sendCommand(noAnySourceCmds[id]);

	}

	//republish
	this->doRepublishCommandRecv(noAnySourceCmds[id],noAnySourceReqs[id],id % nodes);

}

/*******************  FUNCTION  *********************/
template <class T>
void DriverSlots<T>::rdmaWriteAlreadyFinished(T & request)
{
	int id = &request - requests;
	assumeArg(id >= 0 && id < maxOnfly,"Invalid request, not in expected range : %1").arg(id).end();
	markDone(id);
}

/*******************  FUNCTION  *********************/
template <class T>
void DriverSlots<T>::rdmaWrite ( Unit* srcUnit, int rank, int unitId, void* src, void* dest, uint64_t destKey, size_t size, int uuid, const UnitCommand& postCommand )
{
	DriverSlotsRdmaWriteInfo infos;
	infos.srcUnit = srcUnit;
	infos.rank = rank;
	infos.unitId = unitId;
	infos.src = src;
	infos.dest = dest;
	infos.destKey = destKey;
	infos.size = size;
	infos.uuid = uuid;
	infos.postCommand = postCommand;
	
	//to be sure of the copy
	DAQ_DEBUG_ARG("slots","Get rdmaWrite( %1 -> %2 )").arg(postCommand.src).arg(postCommand.dest).end();
	
	//push
	chRdmaWrite << infos;
}

/*******************  FUNCTION  *********************/
template <class T>
void DriverSlots<T>::runRdmaWrite(const DriverSlotsRdmaWriteInfo & infos)
{
	//get request
	int freeId = getUnusedId(TASK_TYPE_RDMA_SEND);
	assume(freeId >= 0,"Invalid free id");
	commands[freeId] = infos.postCommand;
	
	//tracing
	if (useTracer)
		tracer.startComm(&requests[freeId],infos.size,false,true);
	
	//ack rdma
	pendingAck[freeId].rank = infos.rank;
	pendingAck[freeId].uuid = infos.uuid;	

	//post actions
	this->doRdmaWrite(infos.rank,infos.uuid,infos.src,infos.dest,infos.destKey,infos.size,requests[freeId]);
}

/*******************  FUNCTION  *********************/
template<class T>
uint64_t DriverSlots<T>::getGlobalUUID(int rank,int uuid)
{
	uint64_t fulltag = (uint64_t)((uint32_t)rank) + (((uint64_t)uuid) << 32);
	return fulltag;
}

/*******************  FUNCTION  *********************/
template<class T>
bool DriverSlots<T>::checkRdmaPending(int rank,int uuid)
{
	//compute
	uint64_t fulltag = getGlobalUUID(rank,uuid);
	std::lock_guard<std::mutex> lock(polling_mutex);

	//check if match with pending
	for (RDMAPendingUuidList::iterator it = rdmaPendingUuids.begin() ; it != rdmaPendingUuids.end() ; ++it) {
		DAQ_DEBUG_ARG("slots","Search in pending %1 == %2").arg(*it).arg(uuid).end();
		if (*it == fulltag) {
			/*if (it->ptr != ptr && it->ptr != 0)
				DAQ_WARNING_ARG("Don't get same pointer on pending RDMA recv (%1 != %2)").arg(it->ptr).arg(ptr).end();
			if (it->size > size)
				DAQ_WARNING_ARG("Don't get same size on pending RDMA recv (%1 != %2)").arg(it->size).arg(size).end();*/
			//send back directly to the caller
			DAQ_DEBUG_ARG("libfabric2","Match pending RDMA, call the slot function (%1)").arg(fulltag).end();
			rdmaPendingUuids.erase(it);
			return true;
		}
	}
	
	return false;
}

/*******************  FUNCTION  *********************/
template <class T>
void DriverSlots<T>::notifyRdmaFinish(int rank,int uuid)
{
	this->notifyRdmaFinish(getGlobalUUID(rank,uuid));
}

/*******************  FUNCTION  *********************/
template <class T>
void DriverSlots<T>::notifyRdmaFinish(uint64_t guuid)
{
polling_mutex.lock();
	auto it = rdmaPostCommandMap.find(guuid);
	if(it == rdmaPostCommandMap.end()) {
		DAQ_DEBUG_ARG("slots","Make GUUID %1 as pending").arg(guuid).end();
		rdmaPendingUuids.push_back(guuid);
	} else {
		DAQ_DEBUG_ARG("slots","Make GUUID %1 ack local command. Send it.").arg(guuid).end();
		transport->sendCommand(it->second);
		rdmaPostCommandMap.erase(it);
	}
polling_mutex.unlock();	
}

/*******************  FUNCTION  *********************/
template <class T>
void DriverSlots<T>::postWaitRemoteRDMAWrite ( Unit* srcUnit, int rank, int unitId, void* ptr, size_t size, int uuid, const UnitCommand& postCommand )
{
	if (noRdmaAck) {
		//if pending we can send the postCommand directly
		//otherwise we register to the wait map
		bool pending = checkRdmaPending(rank,uuid);
		if (pending) {
			transport->sendCommand(postCommand);
		} else {
			uint64_t guuid = getGlobalUUID(rank,uuid);
			
			polling_mutex.lock();
			assume(rdmaPostCommandMap.find(guuid) == rdmaPostCommandMap.end(),"UUID already in use !");
			rdmaPostCommandMap[guuid] = postCommand;
			polling_mutex.unlock();
		}
	} else {
		DriverSlotsWaitRdmaInfo infos;
		infos.srcUnit = srcUnit;
		infos.rank = rank;
		infos.unitId = unitId;
		infos.ptr = ptr;
		infos.size = size;
		infos.uuid = uuid;
		infos.postCommand = postCommand;
		
		//to be sure of the copy
		DAQ_DEBUG_ARG("slots","Get postWaitRemoteRDMAWrite( %1 -> %2 )").arg(postCommand.src).arg(postCommand.dest).end();
		assert(ptr != nullptr);
		
		chWaitRdmaWrite << infos;
	}
}

/*******************  FUNCTION  *********************/
template <class T>
void DriverSlots<T>::runWaitRdmaWrite(const DriverSlotsWaitRdmaInfo & infos)
{
	//get request
	int freeId = getUnusedId(TASK_TYPE_RDMA_RECV);
	assume(freeId >= 0,"Invalid free id");
	commands[freeId] = infos.postCommand;
	DAQ_DEBUG_ARG("slots","cmd[%3] %1 -> %2").arg(infos.postCommand.src).arg(infos.postCommand.dest).arg(freeId).end();
	
	//tracing
	if (useTracer)
		tracer.startComm(&requests[freeId],infos.size,false,false);
	
	//post actions
	this->doPostRdmaRecv(infos.rank,infos.uuid,infos.ptr,infos.size,requests[freeId]);
}

/*******************  FUNCTION  *********************/
inline std::ostream& operator<< ( std::ostream& out, DriverSlotsTaskType opType )
{
	static const char * names[] = {"NULL","CMD_RECV","CMD_SEND","RDMA_RECV","RDMA_SEND"};
	out << names[opType];
	return out;
}

/*******************  FUNCTION  *********************/
template <class T>
void DriverSlots<T>::setNoRdmaAck()
{
	this->noRdmaAck = true;
}

/*******************  FUNCTION  *********************/
template <class T>
void DriverSlots<T>::sendRdmaAck(int rank, uint64_t uuid)
{
	DAQ_DEBUG_ARG("slots","Send RDMA ack to %1 with uuid : %2").arg(rank).arg(uuid).end();
	UnitCommand command;
	command.type = UNIT_CMD_DRIVER_NOTIFY_RDMA;
	command.args.rdma.guuid = getGlobalUUID(this->rank,uuid);
	command.dest.rank = rank;
	command.dest.id = 0;
	command.src.rank = this->rank;
	command.src.id = 0;
	transport->sendCommand(command);
}

}

#endif //DAQ_DRIVER_SLOTS_IMPL_HPP
