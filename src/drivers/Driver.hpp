/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_DRIVER_HPP
#define DAQ_DRIVER_HPP

/********************  HEADERS  *********************/
#include "common/Config.hpp"
#include "launcher/Launcher.hpp"
#include "common/Throughput.hpp"
#include <string>
#include <stdint.h>
#include <cstdlib>

/********************  NAMESPACE  *******************/
namespace DAQ
{

/********************  MACROS  **********************/
#define HUGE_PAGE_SIZE (2*1024*1024)

/*********************  TYPES  **********************/
class Unit;
class Transport;
struct UnitCommand;

/********************  STRUCT  **********************/
/**
 * Short structured used to descrive a remote segment registered into the IB/OPA.. board
 * for RDMA.
**/
struct RemoteIOV
{
	/** Address of the segment, could be absolute or relative dending on the driver.**/
	void * addr;
	/** Key to identify the registered segment. To be used by the driver only.**/
	uint64_t key;
};

/*********************  CLASS  **********************/
/**
 * Class used to track the in/out/command bandwidth of a driver. In theroy is make
 * better statistics but need to be handled manually in each driver.
 * Currently I prefered to keep the accounting in RU.
 * @todo Currently unused
**/
class DriverThroughput
{
	public:
		DriverThroughput();
		void setup(int rank,int bandwidthDelay);
		void regRdmaIn(size_t size);
		void regRdmaOut(size_t size);
		void regCommand(size_t size);
	private:
		void printAndFlush();
	private:
		int rank;
		int bandwidthDelay;
		Throughput rdmaIn;
		Throughput rdmaOut;
		Throughput commands;
};

/*********************  CLASS  **********************/
/**
 * Basic definition of a driver in DAQPIPE. It mostly define the function to override and behave
 * as an interface.
 * @brief Interface of DAQPIPE network drivers.
**/
class Driver
{
	public:
		/**
		 * Constructor of the driver.
		 * @param name Define the name of the driver.
		**/
		Driver(const std::string & name){this->name = name;this->transport = nullptr;};
		/** Destructor of the driver **/
		virtual ~Driver(){};
		/** Used to initialize the driver.
		 * @param config Define the global configuration structure to be used by implementation.
		 * @param launcher Define the launcher used to request ranks and exchange addresses.
		 * @param argc Number of arguements transmitted to the program.
		 * @param argv List of arguments transmitted to the program.
		**/
		virtual void initialize(const Config & config,Launcher & launcher,int argc, char ** argv) = 0;
		/**
		 * Function used to close the drivers before exit.
		**/
		virtual void finalize() = 0;
		/**
		 * Function used to register a new allocated segment to the board for furture RDMA exchanes.
		 * @param addr The absolute address of the segment.
		 * @param size Size of the segment.
		**/
		virtual void registerSegment(void * addr,size_t size) = 0;
		/**
		 * Unregister a segment before freeing.
		 * @param addr The absolute address of the segment.
		 * @param size Size of the segment.
		**/
		virtual void unregisterSegment(void * addr,size_t size) = 0;
		/**
		 * Post notification wait and possibility requests in the driver to wait some incomming data.
		 * This function is asynchrous and return directly after posting the request.
		 * @param srcUnit The unit source of the call (to send back the notification command).
		 * @param rank The remote rank to wait for.
		 * @param unitId The remote unit ID to wait for.
		 * @param ptr Address of the segment to write on.
		 * @param size size of the segment to write on.
		 * @param uuid Uniq identifier of the communication. It must be uniq while the communication is pending.
		 * @param postCommand The notification command to send back to the caller unit when the data has been recieved.
		**/
		virtual void postWaitRemoteRDMAWrite ( Unit* srcUnit, int rank, int unitId, void* ptr, size_t size, int uuid, const DAQ::UnitCommand& postCommand ) = 0;
		/**
		 * Start to send some data to the remote unit.
		 * This function is asynchrous and return directly after posting the request.
		 * @param srcUnit The unit source of the call (to send back the notification command).
		 * @param rank The remote rank to wait for.
		 * @param unitId The remote unit ID to wait for.
		 * @param sec Address of the data to be sent.
		 * @param dest Address of the remote segment to write on (obained via RemoteIOV structure).
		 * @param destKey key of the remote segment to write on (obained via RemoteIOV structure).
		 * @param size Size of the remote segment.
		 * @param uuid Uniq identifier of the communication. It must be uniq while the communication is pending.
		 * @param postCommand The notification command to send back to the caller unit when the data has been send.
		**/
		virtual void rdmaWrite ( Unit* srcUnit,int rank, int unitId, void* src, void* dest, uint64_t destKey, size_t size, int uuid, const UnitCommand& postCommand ) = 0;
		/**
		 * If the driver need to post some buffers to recieve command it must do it here.
		**/
		virtual void postCommandRecv() = 0;
		/**
		 * Send a command, it uses the command informations to determine the remote target.
		 * This function is asynchrous and return directly after posting the request.
		 * @param command The command to be sent.
		**/
		virtual void sendRemoteCommand(const UnitCommand & command) = 0;
		/**
		 * Request the remote key of a registered segment to share with remote processes.
		 * @deprecated Now use getRemoteIOV in place.
		**/
		virtual uint64_t getSegmentRemoteKey ( void* addr, size_t size ) = 0;
		/**
		 * Request the address and key of a registered segment to be shared with remote process
		 * for future communications. The segment must have beed registered with registerSegment()
		 * before using this function.
		 * @param addr Address of the segment to request.
		 * @param size Size of the segment.
		**/
		virtual RemoteIOV getRemoteIOV(void * addr, size_t size) = 0;
		/**
		 * Start the driver threads if using the multi-thread mode for drivers.
		**/
		virtual void startThreads(){};
		/**
		 * Stop the driver for multi-thread and sequential mode.
		**/
		virtual void stop(){};
		/**
		 * Start the driver in sequential mode.
		**/
		virtual void startNoThreads() {};
		/**
		 * Update status of the thread in sequential mode. Caution yuo must return the CPU to the
		 * caller and not handling infinit loop here otherwise DAQPIPE while deadlock.
		**/
		virtual void updateNoThread() = 0;
		/**
		 * @return Return the name of the driver.
		**/
		const std::string & getName() const {return name;};
		/**
		 * Attach the transport layer to the driver to send-back the notification commands.
		**/
		void setTransport(Transport * transport) {this->transport = transport;};
		/**
		 * Allocate a segment to be registered in the driver for RDMA usage.
		 * Caution, it only do the allocate, you still need to make registration.
		 * This is to handle the spacial RapidIO case which does not provide register function but only allocate.
		**/
		virtual void * allocateRdmaSegment(size_t size, bool write) {return aligned_alloc(HUGE_PAGE_SIZE,size+HUGE_PAGE_SIZE - (size % HUGE_PAGE_SIZE));}; 
		/**
		 * Deallocate a segment used and resigtered for RDMA.
		 * Caution, you still need to make unregister before calling this method.
		**/
		virtual void deallocateRdmaSegment(void * ptr,size_t size,bool write) {free(ptr);};
	protected:
		/** Store the name of the driver. **/
		std::string name;
		/** Keep track of the transport layer to send back the notification commands.**/
		Transport * transport;
};

}

#endif //DAQ_DRIVER_HPP
