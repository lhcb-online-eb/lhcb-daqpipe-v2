#include"mempool.h"
typedef struct alloc_area {
    struct alloc_area* next;
    size_t size;
    char data[0];
} MCB, *MCB_P;

struct mempool { MCB_P mempool_start_p, free_list_p; pthread_mutex_t lock; };

// Create memory pool - This memory pool behaves like malloc
void mp_create (MPOOL_PP mp_handle, char* mempool_start_p, size_t size_in_bytes)
{
    *mp_handle = malloc (sizeof (MPOOL));
    assert (*mp_handle);
    int res = pthread_mutex_init (&(*mp_handle)->lock, NULL);
    assert (res == 0);
    (*mp_handle)->mempool_start_p = (*mp_handle)->free_list_p = (MCB_P)mempool_start_p;
    *(*mp_handle)->free_list_p = (MCB){NULL, size_in_bytes - sizeof (MCB)};
}

// Free memory pool
void mp_destroy (MPOOL_P mp_handle)
{
    int ret = pthread_mutex_destroy (&mp_handle->lock);
    assert (ret == 0);
    free (mp_handle);
}

// Allocates a block of len bytes of memory, returning a pointer to the beginning of the block
void* mp_alloc (MPOOL_P mp_handle, size_t len, offset_t* offset_from_start_p)
{
    MCB_P block_p, next_p, prev_p = NULL, data_p = NULL;
    int ret = pthread_mutex_lock (&mp_handle->lock);
    assert (ret == 0);
    for (block_p = mp_handle->free_list_p; block_p && len && !data_p; prev_p = block_p, block_p = block_p->next) {
        if ((sizeof (MCB) + len) < block_p->size) {
            next_p = (MCB_P)((size_t)block_p + len + sizeof (MCB));
            if (block_p == mp_handle->free_list_p)
                mp_handle->free_list_p = next_p;
            else
                prev_p->next = next_p;
            *next_p = (MCB){block_p->next, block_p->size - len - sizeof (MCB)};
            block_p->size = len;
        }

        else if (len <= block_p->size) {
            if (block_p->next && block_p == mp_handle->free_list_p)
                mp_handle->free_list_p = block_p->next;
            else if (block_p == mp_handle->free_list_p)
                mp_handle->free_list_p = NULL;
            else
                prev_p->next = block_p->next;
        }

        else continue;

        block_p->next = NULL;
        data_p = (void *)block_p->data;
        if (offset_from_start_p != NULL)
            *offset_from_start_p = (offset_t)block_p->data - (offset_t)mp_handle->mempool_start_p;
    }
    ret = pthread_mutex_unlock (&mp_handle->lock);
    assert (ret == 0);
    return data_p;
}

// Mark the given block as free by linking it to the free-list
static void linkBlock (MPOOL_P mp_handle, MCB_P block_p)
{
    if (block_p < mp_handle->free_list_p) {
        block_p->next = mp_handle->free_list_p;
        mp_handle->free_list_p = block_p;
    } else {
        MCB_P tmp_p = mp_handle->free_list_p;
        for (; tmp_p->next && tmp_p->next < block_p; tmp_p = tmp_p->next) ;
        if (tmp_p->next)
            block_p->next = tmp_p->next;
        tmp_p->next = block_p;
    }
}

// Merge block with adjacent free blocks
static void coalesce (MCB_P block_p)
{
    do if (((size_t)block_p + block_p->size + sizeof (MCB)) == (size_t)block_p->next) {
        block_p->size+= block_p->next->size + sizeof (MCB);
        block_p->next = block_p->next->next;
    } else {
        block_p = block_p->next;
    } while (block_p->next);
}

// A block of memory previously allocated by a call to mp_alloc() is deallocated,
// making it available again for further allocations.
void mp_dispose (MPOOL_P mp_handle, void* block_p)
{
    if (block_p == NULL) return ; // If ptr is NULL, no operation is performed
    int ret = pthread_mutex_lock (&mp_handle->lock);
    assert (ret == 0);
    if (mp_handle->free_list_p != NULL) {
        linkBlock (mp_handle, (MCB_P)block_p - 1);
        coalesce (mp_handle->free_list_p);
    } else
        mp_handle->free_list_p = (MCB_P)block_p - 1;
    ret = pthread_mutex_unlock(&mp_handle->lock);
    assert (ret == 0);
}
