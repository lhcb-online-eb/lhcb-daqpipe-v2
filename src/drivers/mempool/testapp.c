#include<time.h>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>

#include"mempool.h"

#define RAND() ((double)rand()/(double)RAND_MAX)
#define BUFS (3500)

char bigbuf[1024*1024];

int main(int argc, char *argv[])
{
    MPOOL_P mp;
    unsigned int size;
    unsigned int where;
    void *ptr[BUFS];
    unsigned int i;

    srand(time(NULL));
    mp_create (&mp, bigbuf, sizeof (bigbuf));

    for (i=0; i < BUFS; i++)
        ptr[i] = NULL;
    for (i=0; i < 30000; i++) {
        where = (int)(RAND() * BUFS);
        if (ptr[where] == NULL) {
            size = (int)(RAND() * 1024 + 1);
            ptr[where] = mp_alloc (mp, size, NULL);
            if (ptr[where] == NULL)
                printf ("mp_alloc returned NULL at iteration %d failed for size %d \n", i, size);
        } else {
            mp_dispose (mp, ptr[where]);
            ptr[where] = NULL;
        }
    }

    for (i=0; i < BUFS; i++) {
        if (ptr[i] != NULL) {
            mp_dispose (mp, ptr[i]);
            ptr[i] = NULL;
        }
    }

    mp_destroy (mp);
    mp = NULL;
    return 0;
}
