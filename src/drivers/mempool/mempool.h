#ifdef __cplusplus
extern "C" {
#endif

#ifndef MEMPOOL_H
#define MEMPOOL_H
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<pthread.h>

typedef size_t offset_t;

typedef struct mempool MPOOL, *MPOOL_P, **MPOOL_PP;

/**
 * @brief Create a memory pool.
 *
 * Call mp_create() before you do anything else.
 *
 * @param mp_handle Pointer-to-pointer to the memory pool to be instantiated,
 * @param mempool_start_p User-provided memory.
 * @param size_in_bytes Size of memory, in bytes.
 * @return Returns pointer to the new pool.
 */
void mp_create (MPOOL_PP mp_handle, char* mempool_start_p, size_t size_in_bytes);

/**
 * @brief Deallocate a memory pool.
 *
 * @param mp_handle Pointer to the memory pool which was allocated via mp_create().
 * @return Void.
 */
void mp_destroy (MPOOL_P mp_handle);

/**
 * @brief Allocate memory block.
 *
 * The mp_alloc() function allocates size bytes and returns a pointer to
 * the allocated memory.
 *
 * @param mp_handle Pointer to the memory pool.
 * @param len Size of the memory block, in bytes.
 * @param offset_from_start_p Retrieve offset from the mempool_start_p. Can be NULL.
 * @return On success, a pointer to the memory block allocated by the
 *         function. If the function failed to allocate the requested
 *         block of memory, a null pointer is returned.
 */
void* mp_alloc (MPOOL_P mp_handle, size_t len, offset_t* offset_from_start_p);

/**
 * @brief Deallocate space in memory.
 *
 * The mp_dispose() function frees the memory space pointed to by ptr.
 *
 * @param mp_handle Pointer to the memory pool
 * @param block_p Pointer to a memory block previously allocated with
 *            mp_alloc() to be deallocated.
 * @return Void.
 */
void mp_dispose (MPOOL_P mp_handle, void* block_p);

#endif/*MEMPOOL_H*/

#ifdef __cplusplus
}
#endif

