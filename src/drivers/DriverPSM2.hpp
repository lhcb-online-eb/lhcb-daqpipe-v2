/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_DRIVER_PSM2_HPP
#define DAQ_DRIVER_PSM2_HPP

/********************  HEADERS  *********************/
#include "DriverSlots.hpp"
#include <psm2.h>
#include <psm2_mq.h>

/********************  NAMESPACE  *******************/
namespace DAQ
{

/********************** STRUCT **********************/
struct Psm2Request
{
	bool active;
	psm2_mq_req_t req;
};

/*********************  CLASS  **********************/
class DriverPSM2 : public DriverSlots<Psm2Request>
{
	public:
		DriverPSM2();
		virtual ~DriverPSM2();
		virtual void initialize( const DAQ::Config& config, DAQ::Launcher& launcher, int argc, char** argv );
		virtual void finalize();
		virtual void registerSegment(void * addr,size_t size);
		virtual void unregisterSegment(void * addr,size_t size);
		virtual uint64_t getSegmentRemoteKey ( void* addr, size_t size );
		virtual RemoteIOV getRemoteIOV(void * addr, size_t size);
	protected:
		//TO export in outter claass by splitting slots/impl
		void initUuid(Launcher & launcher,psm2_uuid_t uuid);
		void setupConnections(Launcher & launcher);
		void doSendCommand ( UnitCommand& command, Psm2Request& request );
		void doPostCommandRecv ( DAQ::UnitCommand& command, Psm2Request& request, int rank );
		void doRdmaWrite ( int rank, int uuid, void* src,void * dest, uint64_t destKey, size_t size, Psm2Request& request );
		void doPostRdmaRecv ( int rank, int uuid, void* ptr, size_t size, Psm2Request& request );
		int doTest ( Psm2Request* requests, size_t count, bool useWait );
		void doRepublishCommandRecv(DAQ::UnitCommand& command, Psm2Request& request, int rank );
		void doInitRequest ( Psm2Request& request );
	private:
		psm2_ep_t endpoint;
		psm2_epid_t endpointId;
		psm2_epaddr_t * epaddr_array;
		psm2_mq_t mq;
};

}

#endif //DAQ_DRIVER_PSM2_HPP
