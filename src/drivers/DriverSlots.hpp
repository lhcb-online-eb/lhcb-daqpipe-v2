/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_DRIVER_SLOTS_HPP
#define DAQ_DRIVER_SLOTS_HPP

/********************  HEADERS  *********************/
#include "common/Config.hpp"
#include "Driver.hpp"
#include <thread>
#include <functional>
#include <gochannels/GoChannel.hpp>
#include <gochannels/GoChannelSelector.hpp>
#include <units/Command.hpp>
#include "CommTracer.hpp"
#include <list>
#include <mutex>
#ifdef HAVE_HTOPML //my code
	#include "htopml/DynamicConfigHttpNode.hpp"
#endif //HAVE_HTOPML

/********************  NAMESPACE  *******************/
namespace DAQ
{

/********************  MACROS  **********************/
#define DRIVER_SLOTS_MAX_ONFLY 256

/********************  ENUM  ************************/
enum DriverSlotsTaskType
{
	TASK_TYPE_NULL,
	TASK_TYPE_CMD_RECV,
	TASK_TYPE_CMD_SEND,
	TASK_TYPE_RDMA_RECV,
	TASK_TYPE_RDMA_SEND,
	TASK_TYPE_COUNT
};

/*********************  STRUCT  *********************/
struct DriverSlotsRdmaWriteInfo
{
	Unit* srcUnit;
	int rank;
	int unitId;
	void* src;
	void* dest;
	uint64_t destKey;
	size_t size;
	int uuid;
	UnitCommand postCommand;
};

/*********************  STRUCT  *********************/
struct DriverSlotsWaitRdmaInfo
{
	Unit* srcUnit;
	int rank;
	int unitId;
	void* ptr;
	size_t size;
	int uuid;
	UnitCommand postCommand;
};

/*********************  STRUCT  *********************/
struct DriverSlotsPendingAck
{
	int rank;
	uint64_t uuid;
};

/*******************  FUNCTION  *********************/
std::ostream & operator << (std::ostream & out,DriverSlotsTaskType opType);

/*********************  TYPES  **********************/
typedef std::map<uint64_t,UnitCommand> RDMAPostCommandMap;
typedef std::list<uint64_t> RDMAPendingUuidList;

/*********************  CLASS  **********************/
template <class T>
class DriverSlots : public Driver
{
	public:
		DriverSlots(const std::string & name);
		virtual ~DriverSlots();
		virtual void initialize( const DAQ::Config& config, DAQ::Launcher& launcher, int argc, char** argv );
		virtual void finalize();
		virtual void postWaitRemoteRDMAWrite ( Unit* srcUnit, int rank, int unitId, void* ptr, size_t size, int uuid, const DAQ::UnitCommand& postCommand );
		virtual void rdmaWrite ( Unit* srcUnit,int rank, int unitId, void* src, void* dest, uint64_t destKey, size_t size, int uuid, const UnitCommand& postCommand );
		virtual void postCommandRecv();
		virtual void sendRemoteCommand(const UnitCommand & command);
		virtual void startThreads ();
		virtual void stop ();
		virtual void startNoThreads ();
		virtual void updateNoThread ();
	protected:
		//TO export in outter claass by splitting slots/impl
		virtual void doInitRequest(T & request) = 0;
		virtual void doSendCommand ( UnitCommand& command, T& request ) = 0;
		virtual void doPostCommandRecv ( DAQ::UnitCommand& command, T& request, int rank = -1 ) = 0;
		virtual void doRdmaWrite ( int rank, int uuid, void* src,void * dest, uint64_t destKey, size_t size, T& request ) = 0;
		virtual void doPostRdmaRecv ( int rank, int uuid, void* ptr, size_t size, T& request ) = 0;
		virtual int doTest ( T* requests, size_t count, bool useWait ) = 0;
		virtual void doRepublishCommandRecv(DAQ::UnitCommand& command, T& request, int rank = -1 ) = 0;
	protected:
		void setNoAnySource();
		void noAnySourceCmdFinsished(T & request);
		void rdmaWriteAlreadyFinished(T & request);
		void mainSender();
		int getUnusedId(DriverSlotsTaskType taskType);
		bool hasEmptySlots();
		void markDone(int id);
		void postInitialRecvCmd();
		void runSendCommand(const UnitCommand & command);
		void runRdmaWrite(const DriverSlotsRdmaWriteInfo & infos);
		void runWaitRdmaWrite(const DriverSlotsWaitRdmaInfo & infos);
		void setNoRDMARequest();
		bool checkRdmaPending(int rank,int uuid);
		uint64_t getGlobalUUID(int rank,int uuid);
		void notifyRdmaFinish(int rank,int uuid);
		void notifyRdmaFinish(uint64_t guuid);
		void sendRdmaAck(int rank,uint64_t guuid);
		void setNoRdmaAck();
		void doCommandRegistration();
		void doCommandDeRegistration();
	protected:
		std::thread threadSender;
		std::thread threadListener;
		int onfly;
		int maxOnfly; 
		int maxRdmaRecv; 
		int maxRdmaSend;
		int cmdRecver;
		size_t listenerSleep;
		UnitCommand commands[DRIVER_SLOTS_MAX_ONFLY];
		T requests[DRIVER_SLOTS_MAX_ONFLY];
		bool inUse[DRIVER_SLOTS_MAX_ONFLY];
		DriverSlotsPendingAck pendingAck[DRIVER_SLOTS_MAX_ONFLY];
		DriverSlotsTaskType taskType[DRIVER_SLOTS_MAX_ONFLY];
		std::function<void()> callback[DRIVER_SLOTS_MAX_ONFLY];
		GoChannel<UnitCommand> chSendCommand;
		GoChannel<DriverSlotsRdmaWriteInfo> chRdmaWrite;
		GoChannel<DriverSlotsWaitRdmaInfo> chWaitRdmaWrite;
		GoChannel<bool> chWakeUpSender;
		GoChannel<bool> chListen;
		bool exitListener;
		int countType[TASK_TYPE_COUNT];
		CommTracer tracer;
		bool useTracer;
		int rank;
		bool hasNoThreads;
		GoChannelSelector selector;
		bool noAnySource;
		bool noRdmaAck;
		int nodes;
		UnitCommand * noAnySourceCmds;
		T * noAnySourceReqs;
		bool noRdmaRequest;
		RDMAPostCommandMap rdmaPostCommandMap;
		RDMAPendingUuidList rdmaPendingUuids;
	protected:
		std::mutex polling_mutex;
};

}

/********************  HEADERS  *********************/
#include "DriverSlots_impl.hpp"

#endif //DAQ_DRIVER_SLOTS_HPP
