/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include <algorithm>
#include <cassert>
#include <fstream>
#include <cstring>
#include <chrono>
#include <thread>
#include <unistd.h>
#include "CommTracer.hpp"
#include "../extern-deps/from-fftw/cycle.h"
#ifdef HAVE_HTOPML
	#include "common/Histogram.hpp"
	#include "htopml/HtopmlCommTracer.hpp"
	#include "htopml/HtopmlHistogram.hpp"
#endif //HAVE_HTOPML

/********************  NAMESPACE  *******************/
namespace DAQ
{

/*******************  FUNCTION  *********************/
CommTracer::CommTracer()
		: cursor(0) {
#ifdef HAVE_HTOPML
	HtopmlCommTraceHttpNode::registerTracer(this);
	HtopmlHistogramHttpNode::registerHistogram(&(this->histogram));
#endif //HAVE_HTOPML
}

/*******************  FUNCTION  *********************/
CommTracer::~CommTracer ()
{

}

/*******************  FUNCTION  *********************/
/**
 * Notify a new communication start.
 * @param id Can be any identifier as long as you use the same for finish. Most of the time
 * it would be the context (MPI/libfabric).
 * @param size Size of the communication in bytes.
 * @param isCommand Define if it is a command message of a data.
 * @param isSend Define if it is a send or a recieve.
**/
void CommTracer::startComm ( void* id, size_t size, bool isCommand, bool isSend )
{
	CommStats & comm = actives[id];
	comm.size = size;
	comm.start = getticks();
	comm.isCommand = isCommand;
	comm.isSend = isSend;
}

/*******************  FUNCTION  *********************/
/**
 * Mark the given communication as finished.
 * @param id Must be the same ID used in startComm, most of the time the context to track
 * the communication status.
**/
void CommTracer::finishComm ( void* id )
{
	CommMap::iterator it = actives.find(id);
	assert(it != actives.end());
	
	it->second.end = getticks();
	log[cursor] = it->second;
	
	cursor = (cursor + 1)%COMM_TRACKER_SIZE;

	if( !it->second.isCommand ) {
		double time = (it->second.end - it->second.start) / getTicksPerSecond();
		double bw = ( it->second.size * 8 / time ) / (1024 * 1024 * 1024);
		histogram.push(bw);
	}
}

/*******************  FUNCTION  *********************/
/**
 * Convet ticks in seconds.
 * @caution It will wait for 2 seconds on the first call.
**/
double getTicksPerSecond()
{
	static double prev = 0;
	
	//cache for htopml to not sleep
	if (prev != 0) {
		return prev;
	}
	
	ticks t0 = getticks();
	auto tt0 = std::chrono::high_resolution_clock::now();

	std::this_thread::sleep_for(std::chrono::seconds(2));

	ticks t1 = getticks();
	auto tt1 = std::chrono::high_resolution_clock::now();

	double const tick_diff = static_cast<double>(t1-t0);
	double const time_diff = std::chrono::duration<double>(tt1-tt0).count();

	prev = tick_diff / time_diff;
	return prev;
}

/*******************  FUNCTION  *********************/
/**
 * Save the log into the given file.
 * @param filename File to be used to store the log.
**/
void CommTracer::save ( const char* filename )
{
	std::ofstream file;
	file.open(filename);
	
	//search ref
	auto max_it = std::max_element(
			std::begin(log),
			std::end(log),
			[](CommStats const& l, CommStats const& r) {return l.start < r.start;});
	uint64_t ref = max_it->start;

	//fill
	Json::Value value;
	for (int i = 0 ; i < COMM_TRACKER_SIZE ; i++) {
		Json::Value & data = value["data"];
		if (log[cursor].size > 0) {
			Json::Value entry;
			entry["start"] = (Json::UInt64)((log[cursor].start-ref)/1000);
			entry["end"] = (Json::UInt64)((log[cursor].end-ref)/1000);
			entry["size"] = (Json::UInt64)(log[cursor].size);
			entry["isCommand"] = log[cursor].isCommand;
			entry["isSend"] = log[cursor].isSend;
			data.append(entry);
		}
		cursor = (cursor + 1)%COMM_TRACKER_SIZE;
	}
	
	//ticks to sec
	value["ticksPerSecond"] = getTicksPerSecond();
	
	//write
	Json::StyledWriter styledWriter;
	file << styledWriter.write(value);

	//close
	file.close();
}

/*******************  FUNCTION  *********************/
#ifdef HAVE_HTOPML
	/**
	 * Convert a tracer into JSON format, used to feed htopml.
	 * @param json The json state object.
	 * @param tracer The tracer to convert.
	**/
	void convertToJson(htopml::JsonState & json,const CommTracer & tracer)
	{
		json.openStruct();
		
			int cursor = tracer.cursor;
			
			//search ref
			uint64_t ref = tracer.log[0].start;
			for (int i = 0 ; i < COMM_TRACKER_SIZE ; i++)
				if (tracer.log[i].start < ref)
					ref = tracer.log[i].start;
			
			json.openFieldArray("data");
				for (int i = 0 ; i < COMM_TRACKER_SIZE ; i++) {
					if (tracer.log[cursor].size > 0) {
						json.printListSeparator();
						json.openStruct();
							json.printField("start",((tracer.log[cursor].start-ref)/1000));
							json.printField("end",((tracer.log[cursor].end-ref)/1000));
							json.printField("size",(tracer.log[cursor].size));
							json.printField("isCommand",tracer.log[cursor].isCommand);
							json.printField("isSend",tracer.log[cursor].isSend);
						json.closeStruct();
					}
					cursor = (cursor + 1)%COMM_TRACKER_SIZE;
				}
			json.closeFieldArray("data");
		
			//ticks to sec
			json.printField("ticksPerSecond",getTicksPerSecond());
		json.closeStruct();
	}
#endif //HAVE_HTOPML

}
