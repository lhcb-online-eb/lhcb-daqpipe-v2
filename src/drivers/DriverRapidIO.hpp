/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Sima Baymani - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_DRIVER_RAPIDIO_HPP
#define DAQ_DRIVER_RAPIDIO_HPP

/********************  HEADERS  *********************/
#include "Driver.hpp"
#include "units/Unit.hpp"

/* For RapidIO */
#include <rapidio_mport_sock.h>
#include <rapidio_mport_dma.h>
#include <rapidio_mport_mgmt.h>

#include <string>
#include <stdint.h>
#include <list>
#include <map>
#include <algorithm>

/* For thread handling */
#include <thread>

#include "drivers/mempool/mempool.h"

/********************  NAMESPACE  *******************/
namespace DAQ
{

/********************  STRUCT  **********************/
/**
 * Struct keeping internal information for a RapidIO server connection.
 */
struct RIOServerConnection
{
	riomp_mailbox_t mbox;
	riomp_sock_t skt;
	uint16_t chan; //where we're listening
};

/********************  STRUCT  **********************/
/**
 * Struct keeping internal information for a RapidIO client connection.
 */
struct RIOClientConnection
{
	riomp_mailbox_t mbox;
	riomp_sock_t skt;
	int rank; // remote rank
	rapidio_mport_socket_msg * msg_rx_buf; //TODO SIMA change name
	rapidio_mport_socket_msg * msg_tx_buf; //TODO SIMA change name
	uint64_t tgtAddr;
};

/********************  STRUCT  **********************/
/**
 * Used for first accept, to exchange rank information.
 */
struct RIOAcceptAck
{
	int rank;
};

/********************  STRUCT  **********************/
/**
 * Used to keep track of our allocations
 */
struct RIODMASegment
{
		size_t askSize;
		size_t allocSize; // have to alloc power of two
		bool write;
		void * buf;
		uint64_t tgtAddr;
};

//TODO Helper class for non-class functionality. Or static functions...


/*********************  CLASS  **********************/
class DriverRapidIO : public Driver
{
	public:
		DriverRapidIO();
		virtual ~DriverRapidIO();
		virtual void initialize( const DAQ::Config& config, DAQ::Launcher& launcher,
                                 int argc, char** argv );
		virtual void finalize();
		virtual void * allocateRdmaSegment(size_t size, bool write);
		virtual void deallocateRdmaSegment(void * ptr,size_t size,bool write);
		virtual void registerSegment(void * addr,size_t size);
		virtual void unregisterSegment(void * addr,size_t size);
		virtual uint64_t getSegmentRemoteKey ( void* addr, size_t size );
		virtual void postWaitRemoteRDMAWrite ( Unit* srcUnit, int rank, int unitId,
							void* ptr, size_t size, int uuid, 
							const DAQ::UnitCommand& postCommand );
		virtual void rdmaWrite ( Unit* srcUnit,int rank, int unitId, void* src,
					void* dest, uint64_t destKey, size_t size,
					int uuid, const UnitCommand& postCommand );
		virtual void postCommandRecv();
		virtual void sendRemoteCommand(const UnitCommand & command);
		virtual RemoteIOV getRemoteIOV ( void* addr, size_t size );
		virtual void startThreads ();
		virtual void stop ();
		virtual void startNoThreads ();
		virtual void updateNoThread ();
	private:
		void main();
		int readLocalId();
		void createThreads();
		void initializeClientConnections();
		void initializeCMBuffers(struct RIOClientConnection & conn);
		void initializeMailbox(riomp_mailbox_t& mbox);
		void initializeDMA();
		void initializeMempool();
		void finalizeMempool();
		void * allocDMARx(size_t size);
		void * allocDMATx(size_t size);
		void * deallocDMARx(void * ptr, size_t size);
		void * deallocDMATx(void * ptr, size_t size);
		void openServerSocket(DAQ::Launcher& launcher);
		void openOutgoingConnections(DAQ::Launcher& launcher);
		void acceptIncomingConnections(DAQ::Launcher& launcher);
		void closeClientConnection(int conn);
		void sendCMMsg(struct RIOClientConnection & conn, const void * buf, size_t size);
		int recvCMMsg(struct RIOClientConnection & conn, void * buf, size_t size);
		void writeDMA(int destId, uint64_t targetAddress, uint32_t offset, void * src,
				size_t size);
		void cmdRecvMain(int conn);
		void cmdSendMain(int conn);
		void rdmaRecvMain(int conn);
		void rdmaSendMain(int conn);
		void notifyRdmaFinish(uint64_t guuid);
		uint64_t getGlobalUUID(int rank, int uuid);
		size_t roundUpToPowerOfTwo(size_t size);
		size_t roundUpToMultiple4K(size_t size);
		RIODMASegment * findAllocatedSegment(void * addr, size_t size);
		void outputMyInfo(DAQ::Launcher& launcher);

	private:
		uint32_t mportId; // which RapidIO device to use, usually zero (only one device)
		uint16_t localId; // My local destination id on the RapidIO network
		bool kbufMode; // true = kernel buffers, false = user space buffers
		int worldSize;
		struct RIOServerConnection serverSkt;
		struct RIOClientConnection * clientSkt;
		std::thread acceptThread; // ptr or not?

		// For CM
		std::thread* cmdRecvThread;
		std::thread* cmdSendThread;
		bool cmdRecvCont;
		GoChannel<std::function<void()>> * cmdSendChan;
		int msgSent;
		DAQ::Launcher * launcher;

		// For RDMA
		riomp_mport_t mportHnd;
		/* These will be set when asking for the connection properties.
		 *
		 * (copy paste from DMA sample code)
		 * Max data block length that can be transferred by DMA channel
		 * by default configured for 32 scatter/gather entries of 4K.
		 * Size returned by mport driver should be when available.
		 * Shall less or eq. TEST_BUF_SIZE.
		 */
		uint32_t maxSge;  // default = 32;
		uint32_t maxSize; // default = 4096;

		// RDMA with reserved memory
		MPOOL_P mempool;
		// TODO: initialize mempool with void *
		char * resMemBuf; // Base address of the mapped reserved memory in the process virtual memory
		uint64_t resMemTgtAddr; // Base address of mapped reserved memory in the RapidIO driver memory
		uint64_t resMemHandle; // Base address of reserved memory
		size_t resMemSize; // Size of reserved memory

		std::map<void *, RIODMASegment> allocations;
		std::thread* rdmaRecvThread;
		std::thread * rdmaSendThread;
		bool rdmaRecvCont;
		GoChannel<std::function<void()>> * rdmaSendChan;
		std::map<uint64_t, UnitCommand> rdmaPostCommandMap;
		std::list<uint64_t> rdmaPendingList;
		std::mutex rdmaPostCommandMapLock;
};

}

#endif //DAQ_DRIVER_RAPIDIO_HPP
