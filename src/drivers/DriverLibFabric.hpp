/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_DRIVER_LIBFABRIC_HPP
#define DAQ_DRIVER_LIBFABRIC_HPP

/********************  HEADERS  *********************/
#include "DriverSlots.hpp"
#include <map>
#include <list>
//libfabric
#include <rdma/fabric.h>
#include <rdma/fi_domain.h>
#include <rdma/fi_endpoint.h>
#include <rdma/fi_cm.h>
#include <rdma/fi_errno.h>
//to get ip
#include <unistd.h>
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <ifaddrs.h>
#include <netinet/in.h> 
#include <arpa/inet.h>

/********************  NAMESPACE  *******************/
namespace DAQ
{

/********************  MACROS  **********************/
#define DAQ_LIBFABRIC_CMD_CNT 32
#define DAQ_LIBFABRIC_CMD_RECV 2

/********************  MACROS  **********************/
#define LIBFABRIC_VERSION FI_VERSION(1,1)
// #define LIBFABRIC_VERSION FI_VERSION(FI_MAJOR_VERSION,FI_MINOR_VERSION)
#define LIBFABRIC_CHECK_STATUS(call, status) assumeArg(status == 0,"Libfabric get failure on command %1 with status %2 : %3").arg(call).arg(status).arg(fi_strerror(-status)).end();
#define LIBFABRIC_EQ_FATAL_ERROR(call, eq) DAQ_FATAL_ARG("Libfabric get failure on command %1: \n%2").arg(call).arg(Helper::getEqErrString(eq,call)).end();
#define LIBFABRIC_CQ_FATAL_ERROR(call, cq) DAQ_FATAL_ARG("Libfabric get failure on command %1: \n%2").arg(call).arg(Helper::getCqErrString(cq,call)).end();
#define LIBFABRIC_CHECK_AND_GOTO(call, status,label) do {if (status != 0) { DAQ_ERROR_ARG("Libfabric get failure on command %1 with status %2 : %3").arg(call).arg(status).arg(fi_strerror(-status)).end(); goto label;} } while(0)

/********************  STRUCT  **********************/
struct LibFabricMyAddr
{
	char raw[16];
};

/********************  STRUCT  **********************/
typedef std::map<void*,UnitCommand> LibFabricRDMANotif;

/********************  ENUM  ************************/
enum LibFabricActType
{
	LIBFABRIC_ACTYPE_CMD_UNUSED,
	LIBFABRIC_ACTYPE_CMD_RECV,
	LIBFABRIC_ACTYPE_CMD_SEND,
	LIBFABRIC_ACTYPE_CMD_RDMA_WRITE,
	LIBFABRIC_ACTYPE_COUNT
};

/********************  STRUCT  **********************/
struct RDMAContext
{
	void * ptr;
	size_t size;
	fi_context * request;
	uint64_t tag;
};

/*********************  TYPES  **********************/
typedef std::map<uint64_t,RDMAContext> RDMAContextMap;
typedef std::list<RDMAContext> RDMAPendingList;
typedef std::list<fi_context*> PendingContext;

/********************  STRUCT  **********************/
struct LibFabricConfig
{
	fi_ep_type epType;
	std::string iface;
	int firstPort;
	int portRange;
	std::string providerName;
	int getServicePort(int rank);
	void loadConfig(const DAQ::Config& config, int rank);
};

/********************  STRUCT  **********************/
struct LibfabricAcceptAck
{
	int rank;
};

/********************  STRUCT  **********************/
struct LibFabricMSGEndpoint
{
	LibFabricMSGEndpoint();
	void setup( fid_domain* domain, fid_fabric* fabric, fi_info* fi );
	struct fid_cq *cq;
	struct fid_eq *eq;
    int ret;
	struct fid_ep *ep;
};

/*********************  STRUCT  *********************/
struct MemoryRegion
{
	void * ptr;
	size_t size;
	fid_mr * mr;
};

/*********************  TYPES  **********************/
typedef std::vector<MemoryRegion> LibfabricMemoryRegionVector;
typedef fi_context Request;
typedef std::map<Request*,bool> LibFabricRequestMap;

/*********************  CLASS  **********************/
struct Helper
{
	static std::string convertAddrToHex( const void* addr, int size );
	static void convertAddrFromHex( void* addr, int size, const std::string& value );
	static std::string getEqErrString(fid_eq *eq, const char *eq_str);
	static std::string getCqErrString( fid_cq* eq, const char* eq_str );
	static int waitForCompletion(fid_cq *cq, int num_completions);
	static std::string getHostIp(const std::string & hostname);
	static std::string getLocalIP(const std::string & iface);
	static std::string getRankIface(const DAQ::Config & Config, int rank);
};

/*********************  CLASS  **********************/
class DriverLibFabric : public DriverSlots<Request>
{
	public:
		DriverLibFabric();
		virtual ~DriverLibFabric();
		virtual void initialize( const DAQ::Config& config, DAQ::Launcher& launcher, int argc, char** argv );
		virtual void finalize();
		virtual void registerSegment(void * addr,size_t size);
		virtual void unregisterSegment(void * addr,size_t size);
		virtual uint64_t getSegmentRemoteKey ( void* addr, size_t size );
		virtual RemoteIOV getRemoteIOV ( void* addr, size_t size );
	protected:
		void initRDM( DAQ::Launcher& launcher, fi_info* fi );
		void initMSG( DAQ::Launcher& launcher, fi_info * hints, fi_info* fi );
		void initMSGListen( DAQ::Launcher& launcher, fi_info* fi );
		int initMSGWaitConnection( DAQ::Launcher& launcher );
		int initMSGGetConnectionAccept( DAQ::LibFabricMSGEndpoint& tmpEP, DAQ::Launcher& launcher, fi_info* fi );
		int initMSGConnect( DAQ::Launcher& launcher, int rank, fi_info* hints, fi_info* fi );
		virtual void doPostCommandRecv(UnitCommand& command, Request& request, int rank);
		virtual void doPostRdmaRecv(int rank, int uuid, void* ptr, size_t size, Request& request);
		virtual void doRdmaWrite(int rank, int uuid, void* src, void* dest, uint64_t destKey, size_t size, Request& request);
		virtual void doSendCommand(UnitCommand& command, Request& request);
		virtual int doTest(Request* requests, size_t count, bool wait);
		virtual int doWaitRequests(Request* requests, size_t size);
		virtual void doInitRequest(Request& request);
		virtual void doRepublishCommandRecv ( UnitCommand& command, Request& request, int rank);
	private:
		int doTestRDM(Request* requests, size_t count, bool wait);
		int doTestMSG(Request* requests, size_t count, bool wait);
		fid_mr * getFidMR(void * ptr,size_t size);
		MemoryRegion * getMR(void * ptr,size_t size);
		RemoteIOV getIOV(void * ptr, size_t size);
		void fillAddrVector ( Launcher &launcher );
		fi_context * getContext ( fi_cq_data_entry& comp );
		void testDebug( DAQ::Launcher& launcher );
		bool checkRdmaPending(int rank, int uuid, void* ptr, size_t size, Request& request);
	private:
		struct fid_fabric *fabric;
		struct fid_domain *domain;
		struct fid_cq *cq;
		struct fid_av *av;
		struct fid_ep *ep;
		struct fid_pep * listenEndpoint;
		fid_eq * listenQueue;
		LibFabricMyAddr myAddr;
		LibFabricMyAddr * allAddrs;
		fi_addr_t *directAddrs;
		size_t myAddrLen;
		LibfabricMemoryRegionVector segments;
		int mr_mode;
		int localRank;
		/** Context map to match RDMA ack to requests. **/
		RDMAContextMap rdmaContextMap;
		/** List of pending rdma ACK recieved before getting the recvRDMA posted (might not apppend but make code safer) **/
		RDMAPendingList rdmaPending;
		int worldSize;
		bool useRDM;
		LibFabricConfig lfConfig;
		LibfabricAcceptAck ackListen;
		LibfabricAcceptAck ackConnect;
		LibFabricMSGEndpoint * msgEndpoints;
		int nodes;
};

}

#endif //DAQ_DRIVER_LIBFABRIC_HPP
