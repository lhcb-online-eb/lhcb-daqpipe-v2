/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_MOCK_DRIVER_HPP
#define DAQ_MOCK_DRIVER_HPP

/********************  HEADERS  *********************/
#include <gmock/gmock.h>
#include "../Driver.hpp"

/********************  NAMESPACE  *******************/
namespace DAQ {

class MockDriver : public Driver {
	public:
		MockDriver() : Driver("mock"){};
		MOCK_METHOD4(initialize,
			void(const Config & config,Launcher & launcher,int argc, char ** argv));
		MOCK_METHOD0(finalize,
			void());
		MOCK_METHOD2(registerSegment,
			void(void * addr,size_t size));
		MOCK_METHOD2(unregisterSegment,
			void(void * addr,size_t size));
		MOCK_METHOD7(postWaitRemoteRDMAWrite,
			void(Unit* srcUnit, int rank, int unitId, void* ptr, size_t size, int uuid, const DAQ::UnitCommand& postCommand));
		MOCK_METHOD9(rdmaWrite,
			void(Unit* srcUnit,int rank, int unitId, void* src, void* dest, uint64_t destKey, size_t size, int uuid, const UnitCommand& postCommand));
		MOCK_METHOD0(postCommandRecv,
			void());
		MOCK_METHOD1(sendRemoteCommand,
			void(const UnitCommand & command));
		MOCK_METHOD2(getSegmentRemoteKey,
			uint64_t(void* addr, size_t size));
		MOCK_METHOD2(getRemoteIOV,
			RemoteIOV(void * addr, size_t size));
		MOCK_METHOD0(startThreads,
			void());
		MOCK_METHOD0(stop,
			void());
		MOCK_METHOD0(startNoThreads,
			void());
		MOCK_METHOD0(updateNoThread,
			void());
};

}  // namespace DAQ

#endif //DAQ_MOCK_DRIVER_HPP
