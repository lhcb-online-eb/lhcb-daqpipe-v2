/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include "DriverUDP.hpp"
#include "portability/TcpHelper.hpp"
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <cstdlib>
#include <sys/epoll.h>
#include <cstring>

/********************  MACROS  **********************/
#define PORTID_CMD_TCP 0
#define MAX_BUFFER_SIZE (100*1024)

/********************  NAMESPACE  *******************/
namespace DAQ
{

/*******************  FUNCTION  *********************/
DriverUDP::DriverUDP () : Driver("DriverUDP")
{

}

/*******************  FUNCTION  *********************/
DriverUDP::~DriverUDP ()
{
	delete [] udpRdmaSockets;
}

/*******************  FUNCTION  *********************/
void UdpConfig::loadFromConfig ( const Config& config )
{
	//load
	this->useIpV6          = config.getAndSetConfigBool("drivers.udp","ipv6",false);
	this->portBase         = config.getAndSetConfigInt("drivers.udp","portBase",2000);
	this->portRange        = config.getAndSetConfigInt("drivers.udp","portRange",512);
	this->cmdIface         = config.getAndSetConfigString("drivers.udp","cmdIface","em1");
	this->rdmaIface        = config.getAndSetConfigString("drivers.udp","rdmaIface","em1");
	this->rdmaChannels     = config.getAndSetConfigInt("drivers.udp","rdmaChannels",4);
	this->rdmaThreadNumber = config.getAndSetConfigInt("drivers.udp","rdmaThreads",4);
	this->cmdThreadNumber  = config.getAndSetConfigInt("drivers.udp","cmdThreads",4);
	this->packetSize       = config.getAndSetConfigInt("drivers.udp","packetSize",9000);
	this->delay            = config.getAndSetConfigUInt64("drivers.udp","delay",1000);
	this->kernelBuf        = config.getAndSetConfigUInt64("drivers.udp","kernelBuf",1677721);//16777216
	this->maxPackets       = config.eventRailMaxDataSize / this->packetSize + 100;
	
	//check
	assumeArg(this->packetSize <= MAX_BUFFER_SIZE,"UDP packet size must be smaller than %1").arg(MAX_BUFFER_SIZE).end();
}

/*******************  FUNCTION  *********************/
void DriverUDP::initialize ( const Config& config, Launcher& launcher, int argc, char** argv )
{
	//load config
	udpConfig.loadFromConfig(config);
	
	//setup some struct
	//this->rdmaSendChan = new GoChannel<std::function<void()>>[rdmaThreadNumber];
	this->cmdSendChan = new GoChannel<std::function<void()>>[udpConfig.cmdThreadNumber];
	
	//get local IP and share
	std::string cmdIp = TcpHelper::getLocalIP(this->udpConfig.cmdIface,this->udpConfig.useIpV6);
	launcher.set("cmdIp",cmdIp);
	std::string rdmaIp = TcpHelper::getLocalIP(this->udpConfig.rdmaIface,this->udpConfig.useIpV6);
	launcher.set("rdmaIp",rdmaIp);
	launcher.commit();
	
	//get port
	int port = getPort(launcher.getRank(),PORTID_CMD_TCP);
	
	//setup listening socket (TCP)
	DAQ_DEBUG_ARG("udp","Listen on port : %1:%2").arg(cmdIp).arg(port).end();
	int listenSocket = TcpHelper::makeListeningSocket(port,udpConfig.useIpV6);
	
	//setup UDP sockets
	udpRdmaSockets = new int[udpConfig.rdmaChannels];
	/*TODO*/
	for (int i = 0 ; i < udpConfig.rdmaChannels ; i++) {
		DAQ_DEBUG_ARG("udp","Open UDP port : %1 -> %2").arg(i).arg(getPort(launcher.getRank(),i+1)).end();
		udpRdmaSockets[i] = TcpHelper::makeUdpListen(udpConfig.rdmaIface,udpConfig.kernelBuf,getPort(launcher.getRank(),i+1));
	}
	
	//udp send socekts
	udpRdmaSendSockets = new int[udpConfig.rdmaThreadNumber];
	for (int i = 0 ; i < udpConfig.rdmaThreadNumber ; i++)
		assumeArg((udpRdmaSendSockets[i] = socket(AF_INET, SOCK_DGRAM, 0)) != -1,"Failed to create DGRAM socket : %1").argStrErrno().end();
	
	//sync
	launcher.barrier();
	
	//allocate sockets
	this->cmdFd = new int[launcher.getWorldSize()];
	for (int i = 0 ; i < launcher.getWorldSize() ; i++)
		cmdFd[i] = -1;
	
	//setup conections
	doConnectAll(launcher);
	doAcceptAll(launcher,listenSocket);
	
	//setup usage of epools to manage many connection per threads
	setupEpolls(launcher);
	
	//setup addresses
	for (int i = 0 ; i < launcher.getWorldSize() ; i++)
		addresses[i] = launcher.get("rdmaIp",i);

	//can close listening socket, useless now
	close(listenSocket);
}

/*******************  FUNCTION  *********************/
void DriverUDP::doConnectAll ( Launcher& launcher )
{
	//connect to the "rank" previous nodes
	for (int targetRank = 0 ; targetRank < launcher.getRank() ; targetRank++)
		cmdFd[targetRank] = doConnect(launcher,targetRank);
}

/*******************  FUNCTION  *********************/
int DriverUDP::doConnect ( Launcher& launcher, int rank)
{
	struct sockaddr_in sin;
	int fd;
	int status;
	
	/* Allocate a new socket */
	fd = socket( udpConfig.useIpV6 ? AF_INET6 : AF_INET , SOCK_STREAM, 0);
	assumeArg(fd>=0, "Error while calling socket() : %1").argStrErrno().end();
	
	/* Prepare connection parameters. */
	sin.sin_family = udpConfig.useIpV6 ? AF_INET6 : AF_INET;
	sin.sin_port = htons(getPort(rank,PORTID_CMD_TCP));
	std::string ip = launcher.get("cmdIp",rank);
	inet_aton(ip.c_str(), &sin.sin_addr);
	DAQ_DEBUG_ARG("tcp","Try to connect to %1:%2").arg(ip).arg(sin.sin_port).end();
	
	/* Connect to the remote host. */
	status = connect(fd, (struct sockaddr*) &sin, sizeof(sin));
	assumeArg(status == 0, "Error while calling connect() : %1").argStrErrno().end();
	
	//setup handshake
	UdpInitHandshake handshake;
	handshake.rank = launcher.getRank();
	
	//send into socket
	status = send(fd,&handshake,sizeof(handshake),0);
	assume(status == sizeof(handshake),"Failed to fully send the handshake when creating TCP connection (client side) !");
	
	return fd;
}

/*******************  FUNCTION  *********************/
void DriverUDP::doAcceptAll ( Launcher& launcher, int listenSocket )
{
	//wait (worldSize - rank - 1) connections for CommandChannal + the N rdma channels
	int cnt = (launcher.getWorldSize() - launcher.getRank() - 1);
	for (int i = 0 ; i < cnt ; i++)
		doAccept(launcher,listenSocket);
}

/*******************  FUNCTION  *********************/
void DriverUDP::doAccept ( Launcher &launcher, int listenSocket )
{
	//vars
	struct sockaddr_storage ss;
	socklen_t slen = sizeof(ss);
	
	//accept the incomming connection
	DAQ_DEBUG("tcp","Waiting for accept");
	int fd = accept(listenSocket, (struct sockaddr*)&ss, &slen);
	assumeArg(fd > 2,"Invalid file descriptor on accept : %1").argStrErrno().end();
	DAQ_DEBUG("tcp","Has accepted one");
	
	//read the handshake to know what and who it is
	UdpInitHandshake handshake;
	int status = recv(fd, &handshake, sizeof(handshake), 0);
	assume(status == sizeof(handshake),"Invalid size from handshake on incomming connection !");
	
	//now apply
	cmdFd[handshake.rank] = fd;
}

/*******************  FUNCTION  *********************/
void DriverUDP::finalize ()
{
	//vars
	int worldSize = transport->getWorldSize();
	int rank = transport->getRank();
	
	//close command sockets
	for (int targetRank = 0 ; targetRank < worldSize ; targetRank++)
		if (rank != targetRank)
			close(cmdFd[targetRank]);

	//close rdma udp port
	for (int i = 0 ; i < udpConfig.rdmaChannels ; i++)
		close(udpRdmaSockets[i]);
}

/*******************  FUNCTION  *********************/
int DriverUDP::getPort ( int rank, int id )
{
	return udpConfig.portBase + (((udpConfig.rdmaChannels + 1) * rank + id) % udpConfig.portRange);
}

/*******************  FUNCTION  *********************/
RemoteIOV DriverUDP::getRemoteIOV ( void* addr, size_t size )
{
	assert(checkRdmaReg(addr,size));
	RemoteIOV out;
	out.addr = addr;
	out.key = 0;
	return out;
}

/*******************  FUNCTION  *********************/
uint64_t DriverUDP::getSegmentRemoteKey ( void* addr, size_t size )
{
	return 0;
}

/*******************  FUNCTION  *********************/
void DriverUDP::postCommandRecv ()
{
	//nothing
}

/*******************  FUNCTION  *********************/
void DriverUDP::postWaitRemoteRDMAWrite ( Unit* srcUnit, int rank, int unitId, void* ptr, size_t size, int uuid, const UnitCommand& postCommand )
{
	//safety reg
	{
		uint64_t guuid = getGuuid(transport->getRank(),uuid);
		std::lock_guard<std::mutex> guard(lock);
		DAQ_DEBUG_ARG("tcp","rdmaRemoteWriteNotif NEW uuid=%1").arg(uuid).end();
		#ifndef NDEBUG
			assumeArg(this->notifMap.find(guuid) == this->notifMap.end(),"uuid reuse %1").arg(guuid).end();
		#endif
		this->notifMap[guuid] = postCommand;
	}
}

/*******************  FUNCTION  *********************/
void DriverUDP::rdmaWrite ( Unit* srcUnit, int rank, int unitId, void* src, void* dest, uint64_t destKey, size_t size, int uuid, const UnitCommand& postCommand )
{
	//setup
	UdpRdmaJob job;
	job.baseAddr = src;
	job.missingCnt = 0;
	job.remoteAddr = dest;
	job.size = size;
	job.targetRank = rank;
	job.guuid = getGuuid(rank,uuid);
	
	//packet number
	int cnt = job.size / udpConfig.packetSize;
	if (job.size % udpConfig.packetSize != 0)
		cnt++;
	job.blocks = cnt;
	
	//setup post
	{
		std::lock_guard<std::mutex> guard(lock);
		auto tmp = rdmaPostCommands.find(job.guuid);
		assumeArg(tmp == rdmaPostCommands.end(),"Already existing entry ! uuid = %1").arg(uuid).end();
		auto & it = rdmaPostCommands[job.guuid];
		it.ack = postCommand;
		it.job = job;
	}
	
	//push
	job >> rdmaChan;
}

/*******************  FUNCTION  *********************/
void DriverUDP::registerSegment ( void* addr, size_t size )
{
	UdpRdmaSeg seg = {
		(char*)addr,
		size
	};
	rdmaSegs.push_back(seg);
}

/*******************  FUNCTION  *********************/
bool DriverUDP::checkRdmaReg ( void* addr, size_t size )
{
	//DAQ_DEBUG_ARG("udp","addr = %1 , size = %2, end = %3").arg(addr).arg(size).arg(addr+size).end();
	for (auto & entry : rdmaSegs) {
		//DAQ_DEBUG_ARG("udp","Check = %1 , size = %2, end = %3").arg((void*)entry.addr).arg(entry.size).arg((void*)(entry.addr+entry.size)).end();
		if (addr >= entry.addr && (char*)addr + size <= entry.addr + entry.size)
			return true;
	}
	return false;
}

/*******************  FUNCTION  *********************/
void DriverUDP::sendRemoteCommand ( const UnitCommand& command )
{
	//define task
	UnitCommand cmd = command;
	std::function<void()> f = [=]()
	{
		UnitCommand cmdLocal = cmd;
		char * cp = (char *)&cmdLocal;
		int remaining = sizeof(cmdLocal);
		
		while (remaining)
		{
			int n_written = send(cmdFd[cmdLocal.dest.rank], cp, remaining, 0);
			if (n_written <= 0) {
				if (!threadRunning) {
					/* We have already terminated the running at this point.
					 * The receiver side may have already closed the socket,
					 * that is why we can't send anything. */
					break;
				} else {
					perror("sendRemoteCommand perror: ");
				}
			}
			remaining -= n_written;
			cp += n_written;
		}
	};
	
	//push task
	cmdSendChan[cmd.dest.rank % udpConfig.cmdThreadNumber] << f;
}

/*******************  FUNCTION  *********************/
void DriverUDP::unregisterSegment ( void* addr, size_t size )
{
	//nothing
}

/*******************  FUNCTION  *********************/
void DriverUDP::setupEpolls ( Launcher& launcher )
{
	//allocates
	epollCmdFd = new int[this->udpConfig.cmdThreadNumber];
	//setup cmd
	for (int i = 0 ; i < this->udpConfig.cmdThreadNumber ; i++)
	{
		epollCmdFd[i] = epoll_create1(0);
		assumeArg(epollCmdFd[i] > 0,"Error at epoll_create1() : %1").argStrErrno().end();
	}

	//fill
	struct epoll_event event;
	for (int i = 0 ; i < launcher.getWorldSize() ; i++)
	{
		//insert if not local (not setup => -1)
		if (cmdFd[i] != -1)
		{
			event.data.fd = cmdFd[i];
			event.events = EPOLLIN;
		
			//regs and check
			int status = epoll_ctl(epollCmdFd[i % udpConfig.cmdThreadNumber], EPOLL_CTL_ADD, cmdFd[i], &event);
			assumeArg(status >= 0,"Error at epoll_ctl() : %1").argStrErrno().end();
		}
	}
}

/*******************  FUNCTION  *********************/
void DriverUDP::startThreads ()
{
    threadRunning = true;
	
	this->cmdRecvThread = new std::thread[this->udpConfig.cmdThreadNumber];
	this->cmdSendThread = new std::thread[this->udpConfig.cmdThreadNumber];
	this->rdmaRecvThread = new std::thread[this->udpConfig.rdmaChannels];
	this->rdmaSendThread = new std::thread[this->udpConfig.rdmaThreadNumber];
	
    for (int i = 0 ; i < this->udpConfig.cmdThreadNumber ; i++)
	{
		this->cmdRecvThread[i] = std::thread([this, i](){this->cmdRecvMain(i);});
		this->cmdSendThread[i] = std::thread([this, i](){this->cmdSendMain(i);});
	}
	
	for (int i = 0 ; i < this->udpConfig.rdmaChannels ; i++)
		this->rdmaRecvThread[i] = std::thread([this, i](){this->rdmaRecvMain(i);});
	for (int i = 0 ; i < this->udpConfig.rdmaThreadNumber ; i++)
		this->rdmaSendThread[i] = std::thread([this, i](){this->rdmaSendMain(i);});
}

/*******************  FUNCTION  *********************/
void DriverUDP::cmdSendMain ( int threadId )
{
	std::function<void()> task;
	while (task << cmdSendChan[threadId])
		task();
	DAQ_DEBUG("tcp","cmdSendChan closed");
}


/*******************  FUNCTION  *********************/
void DriverUDP::cmdRecvMain ( int threadId )
{
	struct epoll_event eventCmd;
	struct epoll_event *eventsCmd;
	
	eventsCmd = (epoll_event*)calloc(transport->getWorldSize(), sizeof eventCmd);
	
	do {
		int n;
// 		char buf[MAX_LINE];
		UnitCommand command;
		
		n = epoll_wait(epollCmdFd[threadId], eventsCmd, transport->getWorldSize(), 1000 );
		for(int i = 0; i < n; i++) {
			if ((eventsCmd[i].events & EPOLLERR) || (eventsCmd[i].events & EPOLLHUP) ||
				(!(eventsCmd[i].events & EPOLLIN))) {
				/* An error has occured on this fd, or the socket is not
				ready for reading (why were we notified then?) */
				DAQ_ERROR_ARG("epoll error").argStrErrno().end();
				close(eventsCmd[i].data.fd);
			} else {
				ssize_t count;
				count = recv(eventsCmd[i].data.fd, (void *)&command, sizeof(command), 0);
				assume(count != -1, "Cmd Receive count = -1 occurred !");
				if (count > 0 )
				{
					assume(count==sizeof(command), "Cmd Receive count != sizeof(command) !");
					filterCommand(command);
				}
			}
		}
	} while (threadRunning);
	
	free(eventsCmd);
	
	DAQ_DEBUG("tcp_channel","cmdRecvChan closed");
}

/*******************  FUNCTION  *********************/
void DriverUDP::stop ()
{
    threadRunning = false;
	
	//cmd
	for (int i = 0 ; i < this->udpConfig.cmdThreadNumber ; i++)
	{
		this->cmdSendChan[i].close();
		this->cmdRecvThread[i].join();
		this->cmdSendThread[i].join();
	}
	
	//rdma send
	this->rdmaChan.close();
	for (int i = 0 ; i < this->udpConfig.rdmaThreadNumber ; i++)
		this->rdmaSendThread[i].join();
	
	//rdma recv
	sendClosePacket();
	for (int i = 0 ; i < this->udpConfig.rdmaChannels ; i++)
		this->rdmaRecvThread[i].join();
}

/*******************  FUNCTION  *********************/
void DriverUDP::startNoThreads ()
{
    startThreads();
}

/*******************  FUNCTION  *********************/
void DriverUDP::updateNoThread ()
{
	//nothing
}

/*******************  FUNCTION  *********************/
void DriverUDP::rdmaSendMain ( int threadId )
{
	UdpRdmaJob job;
	while (job << rdmaChan) {
		//send of resend
		if (job.missingCnt > 0) {
			for (int i = 0 ; i < job.missingCnt ; i++)
				this->sendRdmaMessage(job,threadId,job.missing[i]);
		} else {
			for (int i = 0 ; i < job.blocks ; i++)
				this->sendRdmaMessage(job,threadId,i);
		}
		
		DAQ_DEBUG_ARG("udp","Send ack for blocks : %1").arg(job.guuid).end();
		
		usleep(udpConfig.delay);
		
		//ack
		UnitCommand command;
		command.type = UNIT_CMD_DRIVER_UDP_ACK;
		command.src.rank = this->transport->getRank();
		command.src.id = 0;
		command.dest.rank = job.targetRank;
		command.dest.id = 0;
		command.args.udpAck.guuid = job.guuid;
		command.args.udpAck.blocks = job.blocks;
		this->transport->sendCommand(command);
	}
}

/*******************  FUNCTION  *********************/
void DriverUDP::filterCommand ( UnitCommand& command )
{
	if (command.type == UNIT_CMD_DRIVER_UDP_ACK) {
		//prep command
		UnitCommand cmd;
		cmd.src = command.dest;
		cmd.dest = command.src;
		cmd.args = command.args;
		
		//check status
		if (checkFull(command.args.udpAck.guuid,command.args.udpAck.blocks,cmd.args.udpResend)) {
			cmd.type = UNIT_CMD_DRIVER_UDP_ACK_FULL;
			cmd.args = command.args;
			UnitCommand localCmd;
			{
				std::lock_guard<std::mutex> guard(lock);
				auto it = notifMap.find(command.args.udpAck.guuid);
				assume(it != notifMap.end(),"Failed to fine ack command !");
				localCmd = it->second;
				notifMap.erase(it);
			}
			this->transport->sendCommand(localCmd);
		} else if (cmd.args.udpResend.missingCnt == 0) {
			while(!checkFull(command.args.udpAck.guuid,command.args.udpAck.blocks,cmd.args.udpResend) && cmd.args.udpResend.missingCnt == 0){};
			filterCommand(command);
		} else {
			cmd.type = UNIT_CMD_DRIVER_UDP_RESEND;
			cmd.args.udpResend.guuid = command.args.udpAck.guuid;
		}
		
		//send
		this->transport->sendCommand(cmd);
	} else if (command.type == UNIT_CMD_DRIVER_UDP_ACK_FULL) {
		DAQ_DEBUG_ARG("udp","Get ack full for uuid = %1").arg(command.args.udpAck.guuid).end();
		UnitCommand cmd;
		{
			std::lock_guard<std::mutex> guard(lock);
			auto it = rdmaPostCommands.find(command.args.udpAck.guuid);
			assumeArg(it != rdmaPostCommands.end(),"Failed to find ack command (%1) !").arg(command.args.udpAck.guuid).end();
			cmd = it->second.ack;
			rdmaPostCommands.erase(it);
		}
		this->transport->sendCommand(cmd);
	} else if (command.type == UNIT_CMD_DRIVER_UDP_RESEND) {
		UdpRdmaJob job;
		{
			std::lock_guard<std::mutex> guard(lock);
			auto it = rdmaPostCommands.find(command.args.udpResend.guuid);
			assumeArg(it != rdmaPostCommands.end(),"Failed to find ack command  (%1)!").arg(command.args.udpResend.guuid).end();
			job = it->second.job;
		}
		job.missingCnt = command.args.udpResend.missingCnt;
		memcpy(job.missing,command.args.udpResend.missing,sizeof(job.missing));
		job >> rdmaChan;
	} else {
		//retransimit to upper layer
		transport->sendCommand(command);
	}
}

/*******************  FUNCTION  *********************/
void DriverUDP::sendRdmaMessage ( const UdpRdmaJob& job, int threadId, int id )
{
	//mem
	char buffer[MAX_BUFFER_SIZE];
	assert(udpConfig.packetSize < sizeof(buffer));
	
	//setup parts
	UpdRdmaMsgHdr * header = (UpdRdmaMsgHdr *)buffer;
	char * data = (char*)(header+1);
	
	//offset
	size_t offset = udpConfig.packetSize * id;
	size_t size = job.size - offset;
	if (size > udpConfig.packetSize)
		size = udpConfig.packetSize;
	
	//setup header
	header->destAddr = (char*)job.remoteAddr + offset;
	header->id = id;
	header->size = size;
	header->guuid = job.guuid;
	header->srcRank = transport->getRank();
	
	//copy data
	memcpy(data,(char*)job.baseAddr + offset,size);
	
	//randomly select target port
	int port = getPort(job.targetRank,1+id % (udpConfig.rdmaChannels));
	
	//tfc
	struct sockaddr_in address;
	memset(&address, 0, sizeof address);
	address.sin_family = AF_INET;
	//address.sin_addr.s_addr = htonl(INADDR_ANY);
	address.sin_addr.s_addr = inet_addr(addresses[job.targetRank].c_str());
	address.sin_port = htons(port);//TODO port randomization
	
	//debug
// 	DAQ_DEBUG_ARG("udp","Send RDMA block : %1 -> %2 [%3], uuid = %4, id = %5, port = %6")
// 		.arg(job.baseAddr+offset)
// 		.arg(header->destAddr)
// 		.arg(header->size)
// 		.arg(header->uuid)
// 		.arg(header->id)
// 		.arg(getPort(job.targetRank,1))
// 		.end();
	
	//send to network
	sendto(udpRdmaSendSockets[threadId],buffer,size+sizeof(*header),0,(sockaddr*)&address,sizeof(address));
}

/*******************  FUNCTION  *********************/
void DriverUDP::sendClosePacket ()
{
	for (int i = 0 ; i < udpConfig.rdmaChannels ; i++) {
		struct sockaddr_in address;
		memset(&address, 0, sizeof address);
		address.sin_family = AF_INET;
		//address.sin_addr.s_addr = htonl(INADDR_ANY);
		address.sin_addr.s_addr = inet_addr(addresses[this->transport->getRank()].c_str());
		address.sin_port = htons(getPort(transport->getRank(),1+i));//TODO port randomization
		
		UpdRdmaMsgHdr header;
		header.size = 0;
		header.id = -1;
		header.guuid = -1;
		
		sendto(udpRdmaSendSockets[0],&header,sizeof(header),0,(sockaddr*)&address,sizeof(address));
	}
}

/*******************  FUNCTION  *********************/
void DriverUDP::rdmaRecvMain ( int threadId )
{
	//vars
	char buffer[MAX_BUFFER_SIZE];
	socklen_t addr_len;
	sockaddr address;
	
	DAQ_DEBUG_ARG("udp","Start rdma recv main for thread %1").arg(threadId).end();

	//loop until end
	while(threadRunning) {
		//fetch
		size_t size = recvfrom(udpRdmaSockets[threadId], buffer, sizeof(buffer), 0, &address, &addr_len);
		assumeArg(size >= 0,"UDP socket failure : %1").argStrErrno().end();
		
		//extract header
		UpdRdmaMsgHdr * header = (UpdRdmaMsgHdr *)buffer;
		char * data = (char*)(header+1);
		
		//if exit
		if (header->size == 0 && header->id == -1)
			break;
		
		//debug
// 		DAQ_DEBUG_ARG("udp","Recv RDMA block : -> %1 [%2], uuid = %3, id = %4")
// 			.arg(header->destAddr)
// 			.arg(header->size)
// 			.arg(header->uuid)
// 			.arg(header->id)
// 			.end();
		
		//copy to memory
		memcpy(header->destAddr,data,header->size);
		
		//mark recv
		markRecv(header->guuid,header->id);
	}
}

/*******************  FUNCTION  *********************/
void DriverUDP::markRecv ( uint64_t guuid, int id )
{
	//lock
	std::lock_guard<std::mutex> guard(stateMapLock);
	
	//search
	auto it = stateMap.find(guuid);

	//allocate if needed
	if (it == stateMap.end())
	{
		DAQ_DEBUG_ARG("udp","Make map entry for %1").arg(guuid).end();
		bool * ptr = new bool[udpConfig.maxPackets];
		for (int i = 0 ; i < udpConfig.maxPackets ; i++)
			ptr[i] = false;
		stateMap[guuid] = ptr;
		it = stateMap.find(guuid);
	}
	
	//mark done
	it->second[id] = true;
}

/*******************  FUNCTION  *********************/
bool DriverUDP::checkFull ( uint64_t guuid, int blocks, DAQ::ArgUdpResend& missing )
{
	//lock
	std::lock_guard<std::mutex> guard(stateMapLock);
	
	//check
	DAQ_DEBUG_ARG("udp","Check full for %1 [%2]").arg(guuid).arg(blocks).end();
	
	//search
	auto it = stateMap.find(guuid);
	if(it == stateMap.end())
	{
		missing.missingCnt = 1;
		missing.missing[0] = 0;
		return false;
	}
	
	//check
	missing.missingCnt = 0;
	for (int i = 0 ; i < blocks ; i++) {
		if (it->second[i] == false) {
			missing.missing[missing.missingCnt] = i;
			missing.missingCnt++;
		}
		if (missing.missingCnt >= DAQ_UDP_RDMA_MISSING)
			break;
	}
	
	//check if full or have missing
	if (missing.missingCnt == 0) {
		DAQ_DEBUG("udp","Is full");
		for (int i = 0 ; i < udpConfig.maxPackets ; i++)
			it->second[i] = false;
		return true;
	} else {
		DAQ_DEBUG_ARG("udp","Is not full (%1)").arg(guuid).end();
		return false;
	}
}

/*******************  FUNCTION  *********************/
uint64_t DriverUDP::getGuuid ( int rank, int uuid )
{
	return (uint64_t)((uint32_t)rank) + (((uint64_t)uuid) << 16);
}

}
