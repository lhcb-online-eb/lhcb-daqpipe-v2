/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include "Driver.hpp"
#include "common/Debug.hpp"

/********************  NAMESPACE  *******************/
namespace DAQ
{

/*******************  FUNCTION  *********************/
DriverThroughput::DriverThroughput ()
{
	this->bandwidthDelay = 0;
	this->rank = -1;
}

/*******************  FUNCTION  *********************/
void DriverThroughput::setup ( int rank, int bandwidthDelay )
{
	this->bandwidthDelay = bandwidthDelay;
	this->rank = rank;
}

/*******************  FUNCTION  *********************/
void DriverThroughput::regRdmaOut ( size_t size )
{
	this->rdmaOut.reg(1,size);

	if (commands.getElapsedTime() > bandwidthDelay)
		printAndFlush();
}

/*******************  FUNCTION  *********************/
void DriverThroughput::regRdmaIn ( size_t size )
{
	this->rdmaIn.reg(1,size);
	
	if (commands.getElapsedTime() > bandwidthDelay)
		printAndFlush();
}

/*******************  FUNCTION  *********************/
void DriverThroughput::regCommand ( size_t size )
{
	this->commands.reg(1,size);
	
	if (commands.getElapsedTime() > bandwidthDelay)
		printAndFlush();
}

/*******************  FUNCTION  *********************/
void DriverThroughput::printAndFlush ()
{
	DAQ_MESSAGE_ARG("[MON] %1 input : %2, output : %3 , commands : %4")
		.arg(rank)
		.arg(commands)
		.arg(rdmaIn)
		.arg(rdmaOut)
		.end();
		
	commands.restart();
	rdmaIn.restart();
	rdmaOut.restart();
}

}
