/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include <cassert>
#include <common/Debug.hpp>
#include "DriverPSM2.hpp"

/********************  MACROS  **********************/
#define DAQ_DRIVER_MPI_SLOT_CMD_TAG 0

/********************  NAMESPACE  *******************/
namespace DAQ
{

/*******************  FUNCTION  *********************/
DriverPSM2::DriverPSM2 () 
           :DriverSlots<Psm2Request>("DriverPSM2")
{
}

/*******************  FUNCTION  *********************/
DriverPSM2::~DriverPSM2 ()
{
}

/*******************  FUNCTION  *********************/
void DriverPSM2::initUuid(Launcher & launcher,psm2_uuid_t uuid)
{
	//generate uuid on master
	if (launcher.getRank() == 0)
	{
		//generate
		psm2_uuid_generate(uuid);
		
		//convert to string
		char buffer[128];
		sprintf(buffer,"%x-%x-%x-%x-%x-%x-%x-%x-%x-%x-%x-%x-%x-%x-%x-%x",
			uuid[0],uuid[1],uuid[2],uuid[3],
			uuid[4],uuid[5],uuid[6],uuid[7],
			uuid[8],uuid[9],uuid[10],uuid[11],
			uuid[12],uuid[13],uuid[14],uuid[15]);
			
		//debug
		DAQ_DEBUG_ARG("psm2","UUID : %1").arg(buffer).end();
		
		//share
		launcher.set("psm2-uuid",buffer);
		
		//sync with others
		launcher.commit();
		launcher.barrier();
	} else {
		//wait master
		launcher.barrier();
		
		//read
		std::string masterUuid = launcher.get("psm2-uuid",0);
		
		//convert
		sscanf(masterUuid.c_str(),"%x-%x-%x-%x-%x-%x-%x-%x-%x-%x-%x-%x-%x-%x-%x-%x",
			&uuid[0],&uuid[1],&uuid[2],&uuid[3],
			&uuid[4],&uuid[5],&uuid[6],&uuid[7],
			&uuid[8],&uuid[9],&uuid[10],&uuid[11],
			&uuid[12],&uuid[13],&uuid[14],&uuid[15]);
	}
}

/*******************  FUNCTION  *********************/
void DriverPSM2::initialize( const DAQ::Config& config, DAQ::Launcher& launcher, int argc, char** argv )
{
	//parent
	DriverSlots<Psm2Request>::initialize(config,launcher,argc,argv);
	
	//init PSM
	psm2_uuid_t uuid;
	int status;
	int ver_major = PSM2_VERNO_MAJOR;
	int ver_minor = PSM2_VERNO_MINOR;
	
	//init uuid
	initUuid(launcher,uuid);
	
	//init psm
	status = psm2_init(&ver_major,&ver_minor);
	assumeArg(status == PSM2_OK,"Failed to init PSM2 : %1").arg(status).end();
	
	//setup endpoint options
	struct psm2_ep_open_opts o;
	status = psm2_ep_open_opts_get_defaults(&o);
	assumeArg(status == PSM2_OK,"Failed to load default EP options : %1").arg(status).end();
	
	/* Attempt to open a PSM2 endpoint. This allocates hardware resources. */
	status = psm2_ep_open(uuid, &o, &endpoint, &endpointId);
	assumeArg(status == PSM2_OK,"Failed to open EP : %1").arg(status).end();
	
	//setup connections
	setupConnections(launcher);
	
	//setup mq
	status = psm2_mq_init(endpoint,PSM2_MQ_ORDERMASK_NONE,nullptr,0,&mq);
	assumeArg(status == PSM2_OK,"Failed to open MQ : %1").arg(status).end();
}

/*******************  FUNCTION  *********************/
void DriverPSM2::setupConnections(Launcher & launcher)
{
	//vars
	int status;
	
	//shareing endpoints
	char buffer[64];
	sprintf(buffer,"%lx",endpointId);
	
	//share
	launcher.set("psm2-ep",buffer);
	launcher.commit();
	launcher.barrier();
	
	//now interconnect
	int ranks = launcher.getWorldSize();
	psm2_epid_t * epid_array = new psm2_epid_t[ranks];
	int * epid_array_mask = new int[ranks];
	psm2_error_t * epid_connect_errors = new psm2_error_t[ranks];
	epaddr_array = new psm2_epaddr_t[ranks];
	
	//read
	for (int i = 0 ; i < ranks ; i++) {
		//get and convert
		std::string addr = launcher.get("psm2-ep",i);
		status = sscanf(addr.c_str(),"%lx",&epid_array[i]);
		assume(status == 1, "Fail to convert address !");
		
		//mask
		if (i == launcher.getRank())
			epid_array_mask[i] = 0;
		else
			epid_array_mask[i] = 1;
	}
	
	//connect
	status = psm2_ep_connect(endpoint,ranks,epid_array,epid_array_mask,epid_connect_errors,epaddr_array,0);
	assumeArg(status == PSM2_OK,"Failed to connect EP : %1").arg(status).end();
	
	//check connection errors
	for (int i = 0 ; i < ranks ; i++) {
		assumeArg(epid_array_mask[i] == 0 || epid_connect_errors[i] == PSM2_OK,"Failed to connect node %1").arg(i).end();
	}
}

/*******************  FUNCTION  *********************/
void DriverPSM2::finalize()
{
	DriverSlots<Psm2Request>::finalize();
	
	//close MQ
	int status = psm2_mq_finalize(mq);
	assumeArg(status == PSM2_OK,"Failed to close MQ : %1").arg(status).end();
	
	//close endpoint
	status = psm2_ep_close(endpoint,PSM2_EP_CLOSE_GRACEFUL,0);
	assumeArg(status == PSM2_OK,"Failed to close endpoint : %1").arg(status).end();
	
	//finalize psm
	status = psm2_finalize();
	assumeArg(status == PSM2_OK,"Failed to finalize : %1").arg(status).end();
}

/*******************  FUNCTION  *********************/
void DriverPSM2::doInitRequest ( Psm2Request & request )
{
	request.active = false;
	request.req = PSM2_MQ_REQINVALID;
}

/*******************  FUNCTION  *********************/
RemoteIOV DriverPSM2::getRemoteIOV ( void* addr, size_t size )
{
	//we don't care
	RemoteIOV iov = {0,0};
	return iov;
}

/*******************  FUNCTION  *********************/
uint64_t DriverPSM2::getSegmentRemoteKey ( void* addr, size_t size )
{
	return 0;
}

/*******************  FUNCTION  *********************/
void DriverPSM2::unregisterSegment ( void* addr, size_t size )
{
	//nothing to , we use only MQ, no RDMA
}

/*******************  FUNCTION  *********************/
void DriverPSM2::registerSegment ( void* addr, size_t size )
{
	//nothing to , we use only MQ, no RDMA
}

/*******************  FUNCTION  *********************/
void DriverPSM2::doSendCommand ( UnitCommand& command, Psm2Request& request )
{
	//post actions
	DAQ_DEBUG_ARG("psm2","%3 -> SendCommand(command,%1,%2)").arg(command.dest.rank).arg(DAQ_DRIVER_MPI_SLOT_CMD_TAG).arg(this->transport->getRank()).end();

	//send command
	int status = psm2_mq_isend(mq,epaddr_array[command.dest.rank],0,0,&command,sizeof(command),nullptr,&request.req);
	assumeArg(status == PSM2_OK,"Failed to send command : %1").arg(status).end();

	//mark request in use
	request.active = true;
}

/*******************  FUNCTION  *********************/
void DriverPSM2::doPostCommandRecv ( DAQ::UnitCommand& command, Psm2Request& request, int rank )
{
	//dbug
	assert(rank == -1);
	DAQ_DEBUG_ARG("psm2","%2 -> MPI_IRecv(command,ANY_SOURCE,%1)").arg(DAQ_DRIVER_MPI_SLOT_CMD_TAG).arg(this->transport->getRank()).end();
	
	//send command
	int status = psm2_mq_irecv(mq,0,(uint64_t)-1,0,&command,sizeof(command),nullptr,&request.req);
	assumeArg(status == PSM2_OK,"Failed to send command : %1").arg(status).end();
	
	//mark request in use
	request.active = true;
}

/*******************  FUNCTION  *********************/
void DriverPSM2::doRdmaWrite ( int rank, int uuid, void* src,void * dest, uint64_t destKey, size_t size, Psm2Request& request )
{
	//debuf
	DAQ_DEBUG_ARG("psm2","%3 -> MPI_ISend(rdma,%1,%2)").arg(rank).arg(uuid).arg(this->transport->getRank()).end();
	
		//send command
	int status = psm2_mq_isend(mq,epaddr_array[rank],0,uuid+1,src,size,nullptr,&request.req);
	assumeArg(status == PSM2_OK,"Failed to send command : %1").arg(status).end();

	//mark request in use
	request.active = true;
}

/*******************  FUNCTION  *********************/
void DriverPSM2::doPostRdmaRecv ( int rank, int uuid, void* ptr, size_t size, Psm2Request& request )
{
	//dbug
	assert(ptr != nullptr);
	DAQ_DEBUG_ARG("psm2","%3 -> MPI_IRecv(rdma,%1,%2,%4)").arg(rank).arg(uuid).arg(this->transport->getRank()).arg(ptr).end();
	
	//post recv command
	//int status = psm2_mq_irecv(mq,epaddr_array[rank],uuid+1,(uint64_t)-1,0,ptr,size,nullptr,&request.req);
	int status = psm2_mq_irecv(mq,uuid+1,(uint64_t)-1,0,ptr,size,nullptr,&request.req);
	assumeArg(status == PSM2_OK,"Failed to send command : %1").arg(status).end();
	
	//mark request in use
	request.active = true;
}

/*******************  FUNCTION  *********************/
int DriverPSM2::doTest ( Psm2Request* requests, size_t count, bool useWait )
{
	//vars
	int ret;
	
	//scan all
	do {
		//make progress
		psm2_poll(endpoint);
		
		//scan
		for (int i = 0 ; i < count ; i++) {
			//scan only active
			if (requests[i].active) {
				ret = psm2_mq_test(&requests[i].req,nullptr);
				if (ret == PSM2_OK) {
					doInitRequest(requests[i]);
					return i;
				} else {
					assume(ret == PSM2_MQ_INCOMPLETE,"Invalid status on mq_test !");
				}
			}
		}
	} while (useWait);
	
	return -1;
}

/*******************  FUNCTION  *********************/
void DriverPSM2::doRepublishCommandRecv ( DAQ::UnitCommand& command, Psm2Request& request, int rank )
{
	assert(rank == -1);
	this->doPostCommandRecv(command,request,rank);
}

}
