/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include <cstring>
#include "common/Debug.hpp"
#include "DriverLocal.hpp"

/********************  NAMESPACE  *******************/
namespace DAQ
{

/*******************  FUNCTION  *********************/
DriverLocal::DriverLocal()
		: Driver("DriverLocal"),
			hasNoThread(false),
			skipMemcpy(false),
			realisticMemcpyRate(0),
			memcpyRateTracking(0),
			memcpyToSkip(0) {
	this->tasks.setUniqReader();
#ifdef NDEBUG
	this->realisticMemcpyRate = 500;
#endif
}

/*******************  FUNCTION  *********************/
void DriverLocal::initialize ( const DAQ::Config& config, DAQ::Launcher& launcher, int argc, char** argv )
{
	this->skipMemcpy = config.getAndSetConfigBool("testing","skipMemcpy",skipMemcpy);
	this->realisticMemcpyRate = config.getAndSetConfigInt("testing","realisticMemcpyRate",this->realisticMemcpyRate);
	//0 disable
	if (this->realisticMemcpyRate == 0 || config.dataChecking) {
		this->memcpyToSkip = 0;
		this->memcpyRateTracking = this->memcpyToSkip;
	} else {
		//here we consider one node lost of EM and RU/BU shared for the rest
		if (launcher.getWorldSize() == 1)
			this->memcpyToSkip = 0;
		else
			this->memcpyToSkip = this->realisticMemcpyRate / (launcher.getWorldSize() - 1);
		this->memcpyRateTracking = this->memcpyToSkip;
	}
}

/*******************  FUNCTION  *********************/
void DriverLocal::startThreads ()
{
    this->thread = std::thread([this](){this->main();});
}

/*******************  FUNCTION  *********************/
void DriverLocal::startNoThreads ()
{
    this->tasks.disableWaitOnEmpty();
	this->hasNoThread = true;
}

/*******************  FUNCTION  *********************/
void DriverLocal::stop ()
{
	this->tasks.close();
	if (this->hasNoThread == false)
		this->thread.join();
}

/*******************  FUNCTION  *********************/
void DriverLocal::finalize ()
{
}

/*******************  FUNCTION  *********************/
void DriverLocal::postCommandRecv ()
{
	//nothing to do
}

/*******************  FUNCTION  *********************/
void DriverLocal::sendRemoteCommand ( const UnitCommand& command )
{
	//DAQ_FATAL("Must never append");
	assert(command.dest.rank == transport->getRank());
	Unit * unit = transport->getLocalUnit(command.dest.id);
	assert(unit != nullptr);

	DAQ_DEBUG_ARG("tmptcp","eventId %1 (from DriverLocal::sendRemoteCommand)").arg(command.args.pullGather.eventId).end();
	
	unit->pushCommand(command);
}

/*******************  FUNCTION  *********************/
void DriverLocal::registerSegment ( void* addr, size_t size )
{
	//nothing to do in our case
}

/*******************  FUNCTION  *********************/
void DriverLocal::unregisterSegment ( void* addr, size_t size )
{
	//nothing to do in our case
}

/*******************  FUNCTION  *********************/
uint64_t DriverLocal::getSegmentRemoteKey ( void* addr, size_t size )
{
	return 0;
}

/*******************  FUNCTION  *********************/
RemoteIOV DriverLocal::getRemoteIOV ( void* addr, size_t size )
{
	RemoteIOV ret = {addr,0};
	return ret;
}

/*******************  FUNCTION  *********************/
void DriverLocal::postWaitRemoteRDMAWrite ( Unit * unit, int rank, int unitId, void* ptr, size_t size, int uuid, const UnitCommand& postCommand )
{
	//checks
	DAQ_DEBUG_ARG("local","Register wait RDMA write on %1 into %2").arg((void*)ptr).arg(this).end();
	assert(postCommand.type != UNIT_CMD_INVALID);
	
	//safety reg
	{
		std::lock_guard<std::mutex> guard(lock);
		
		//check pending
		for (auto it = pendings.begin() ; it != pendings.end() ; ++it) {
			if (*it == ptr) {
				DAQ_DEBUG_ARG("local","Used pending queue %1").arg(ptr).end();
				this->transport->sendCommand(postCommand);
				pendings.erase(it);
				return;
			}
		}
		
		//register
		assumeArg(this->rdmaRemoteWriteNotif.find(ptr) == this->rdmaRemoteWriteNotif.end(),"RDMA notif already exist for given ptr = %1").arg(ptr).end();
		this->rdmaRemoteWriteNotif[ptr] = postCommand;
	}
}

/*******************  FUNCTION  *********************/
void DriverLocal::rdmaWrite ( Unit * unit,int rank, int unitId, void* src, void* dest, uint64_t destKey, size_t size, int uuid, const UnitCommand& postCommand )
{
	//errors
	assert(rank == transport->getRank());
	
	//debug
	DAQ_DEBUG_ARG("local","Have RDMA write %3 on %1 [%4] into %2").arg((void*)dest).arg(this).arg((void*)src).arg(size).end();
	UnitCommand cmd = postCommand;
	
	//define task
	std::function<void()> f = [=]() {
		//perform memcopy
 		if (!skipMemcpy) {
 			if (this->memcpyRateTracking == 0) {
				DAQ_DEBUG_ARG("local","Do memcpy(%1,%2,%3)").arg(dest).arg(src).arg(size).end();
				memcpy(dest,src,size);
 				this->memcpyRateTracking = this->memcpyToSkip;
 			} else {
				DAQ_DEBUG_ARG("local","Skip memcpy [%1]").arg(this->memcpyRateTracking).end();
 				this->memcpyRateTracking--;
 				assert(this->memcpyRateTracking >= 0);
 			}
 		}
		
		//send post command to sender
		transport->sendCommand(cmd);

		//send post command to target
		{
			std::lock_guard<std::mutex> guard(this->lock);
			
			//if not in map, reg to pending
			if (this->rdmaRemoteWriteNotif.find(dest) == this->rdmaRemoteWriteNotif.end()) {
				DAQ_DEBUG_ARG("local","Store in pending queue %1").arg(dest).end();
				pendings.push_back(dest);
			} else {
				assume(this->rdmaRemoteWriteNotif[dest].type != UNIT_CMD_INVALID,"Invalid type");
				transport->sendCommand(this->rdmaRemoteWriteNotif[dest]);
				this->rdmaRemoteWriteNotif.erase(dest);
			}
		}
	};
	
	//push task
	tasks << f;
}

/*******************  FUNCTION  *********************/
void DriverLocal::main ()
{
	std::function<void()> task;
	while (task << tasks)
		task();
}

/*******************  FUNCTION  *********************/
void DriverLocal::updateNoThread ()
{
	this->main();
}

}
