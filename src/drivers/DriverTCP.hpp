/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Balazs Voneki - CERN
                        Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_DRIVER_TCP_HPP
#define DAQ_DRIVER_TCP_HPP

/********************  HEADERS  *********************/
#include <map>
#include "units/Unit.hpp"
#include "Driver.hpp"
#include <thread>
#include <gochannels/GoChannel.hpp>
#include <functional>
#include <list>

/********************  NAMESPACE  *******************/
namespace DAQ
{

/********************  ENUM  ************************/
enum TcpHandshakeGoal
{
	TCP_HANDSHAKE_COMMAND,
	TCP_HANDSHAKE_RDMA
};

/********************  ENUM  ************************/
enum TcpRdmaState
{
	TCP_RDMA_STATE_WAIT_HEADER,
	TCP_RDMA_STATE_WAIT_DATA
};

/********************  STRUCT  **********************/
struct TcpInitHandshake
{
	int rank;
	int id;
	TcpHandshakeGoal goal;
};

/********************  STRUCT  **********************/
struct TcpRdmaHeader
{
	size_t size;
	int uuid;
	void * addr;
	uint16_t crc;
};

/********************  STRUCT  **********************/
struct TcpRdmaChannelHandler
{
	TcpRdmaChannelHandler();
	int fd;
	TcpRdmaState state;
	size_t cursor;
	TcpRdmaHeader header;
};

/*********************  TYPES  **********************/
typedef std::map<int,UnitCommand> TcpRdmaNotifMap;

/*********************  CLASS  **********************/
class DriverTCP : public Driver
{
	public:
		DriverTCP();
		virtual ~DriverTCP();
		virtual void initialize( const DAQ::Config& config, DAQ::Launcher& launcher, int argc, char** argv );
		virtual void finalize();
		virtual void registerSegment(void * addr,size_t size);
		virtual void unregisterSegment(void * addr,size_t size);
		virtual uint64_t getSegmentRemoteKey ( void* addr, size_t size );
		virtual void postWaitRemoteRDMAWrite ( Unit* srcUnit, int rank, int unitId, void* ptr, size_t size, int uuid, const DAQ::UnitCommand& postCommand );
		virtual void rdmaWrite ( Unit* srcUnit,int rank, int unitId, void* src, void* dest, uint64_t destKey, size_t size, int uuid, const UnitCommand& postCommand );
		virtual void postCommandRecv();
		virtual void sendRemoteCommand(const UnitCommand & command);
		virtual RemoteIOV getRemoteIOV ( void* addr, size_t size );
		virtual void startThreads ();
		virtual void stop ();
		virtual void startNoThreads ();
		virtual void updateNoThread ();
	private:
		int getPort(int rank);
		void makeNonBlocking(int fd);
		int doConnect( DAQ::Launcher& launcher, int rank, DAQ::TcpHandshakeGoal goal, int id );
		void doConnectAll(DAQ::Launcher& launcher);
		void doAccept( Launcher &launcher,int listenSocket );
		void doAcceptAll(Launcher &launcher,int listenSocket);
		void setupEpolls( DAQ::Launcher& launcher );
		TcpRdmaChannelHandler * getRdmaHandler(int fd);
		void rdmaRecvMain(int threadId);
		void rdmaSendMain(int threadId);
		void cmdRecvMain(int threadId);
		void cmdSendMain(int threadId);
	private:
		int portBase;
		int portRange;
		bool useIpV6;
		int rdmaChannels;
		int rdmaChanRoundRobin;
		int rdmaThreadNumber;
		int cmdThreadNumber;
		bool hasNoThread;
		std::string iface;
		int * cmdFd;
		int * epollRdmaFd;
		int * epollCmdFd;
		bool threadRunning;
		std::mutex lock;
		TcpRdmaNotifMap notifMap;
		TcpRdmaChannelHandler * rdmaChan;
		std::thread* rdmaRecvThread;
		std::thread* rdmaSendThread;
		std::thread* cmdRecvThread;
		std::thread* cmdSendThread;
		GoChannel<std::function<void()>> * rdmaSendChan;
		GoChannel<std::function<void()>> * cmdSendChan;
};

}

#endif //DAQ_DRIVER_TCP_HPP
