/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_TOPOLOGY_HPP
#define DAQ_TOPOLOGY_HPP

/********************  HEADERS  *********************/
#include "common/Config.hpp"
#include "transport/Transport.hpp"

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/********************  STRUCT  **********************/
struct Topology
{
	static void setupTopologyLocal(Transport & transport,const Config & config);
	static void setupTopologyMPI(Transport & transport,Config & config);
	static void setupCustomTopology(Transport & transport,Config & config);
	static void setupTopology( DAQ::Transport& transport, DAQ::Config& config, bool local );
};

}

#endif //DAQ_TOPOLOGY_HPP
