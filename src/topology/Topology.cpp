/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include "common/Debug.hpp"
#include "Topology.hpp"

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*******************  FUNCTION  *********************/
void Topology::setupTopologyLocal(Transport & transport,const Config & config)
{
	//check
	assume(config.subFarmSize * config.filterSubfarm == 0,"Local topology do not manage subfarams currently, please use 0 in the related config fields !");
	
	//setup global topology, all on local node
	//one manager on 0
	transport.setUnitType(0,0,DAQ::UNIT_EVENT_MANAGER);

	//two RU
	transport.setUnitType(0,1,DAQ::UNIT_READOUT_UNIT);
	//transport.setUnitType(0,2,DAQ::UNIT_READOUT_UNIT);

	//two BU
	transport.setUnitType(0,3,DAQ::UNIT_BUILDER_UNIT);
	//transport.setUnitType(0,4,DAQ::UNIT_BUILDER_UNIT);
	
	//profile
	transport.setUnitType(0,5,DAQ::UNIT_PROFILE_UNIT);
};

/*******************  FUNCTION  *********************/
void Topology::setupTopologyMPI(Transport & transport,Config & config)
{
	//min 
	int min = 2;
	if (config.farmMode == "farms")
		min += config.subFarmSize * config.filterSubfarm;
	
	//check
	assumeArg(transport.getWorldSize() >= min,"You need to launch at least %2 processes, you provide : %1")
		.arg(transport.getWorldSize())
		.arg(min)
		.end();
		
	//for testing purpose
	bool oneWay = config.getAndSetConfigBool("testing","oneWay",false);

	//ppn
	int ppn = config.localProcesses(transport.getWorldSize());
	if (config.getAndSetConfigBool("testing","EventManagerAlone",true) == false || ppn == transport.getWorldSize())
		ppn = 1;

	//setup EM on node 0
	transport.setUnitType(0,0,DAQ::UNIT_EVENT_MANAGER);//one manager on node 0
	transport.setUnitType(1,2,DAQ::UNIT_PROFILE_UNIT);//one profile on node 2
	
	//setup RU and BU on same nodes
	int rubu = transport.getWorldSize() - config.subFarmSize * config.filterSubfarm;
	for (int i = 1 ; i < rubu; i++) {
		if (i < ppn)
			transport.setUnitType(i,0,DAQ::UNIT_DUMMY);//dummy unit on EM node
		else if (oneWay) {
			if (i % 2 == 0)
				transport.setUnitType(i,0,DAQ::UNIT_READOUT_UNIT);//One RU per node
			else
				transport.setUnitType(i,0,DAQ::UNIT_BUILDER_UNIT);//One BU per node
		} else {
			transport.setUnitType(i,0,DAQ::UNIT_READOUT_UNIT);//One RU per node
			transport.setUnitType(i,1,DAQ::UNIT_BUILDER_UNIT);//One BU per node
		}
	}
	
	//setup sufarms
	if (config.farmMode == "farms") {
		for (int farm = 0 ; farm < config.filterSubfarm ; farm++) {
			
			for (int i = 0 ; i < config.subFarmSize ; i++) {
				int id = rubu + farm * config.subFarmSize + i;
				if (id < transport.getWorldSize())
					transport.setUnitType(id,0,DAQ::UNIT_FILTER_UNIT);
				if (i == 0)
					transport.setUnitType(id,1,DAQ::UNIT_FILTER_MANAGER);
			}
		}
	} else if (config.farmMode == "local") {
		config.subFarmSize = 1;
		config.filterSubfarm = rubu - 1;
		for (int i = 1 ; i < rubu; i++) {
			transport.setUnitType(i,3,DAQ::UNIT_FILTER_UNIT);//One RU per node
			transport.setUnitType(i,4,DAQ::UNIT_FILTER_MANAGER);//One BU per node
		}
	} else if (config.farmMode == "none" ) {
		//nothing to do
	} else {
		DAQ_FATAL_ARG("Invalid farm mode : %1").arg(config.farmMode).end();
	}
}

/*******************  FUNCTION  *********************/
void Topology::setupCustomTopology(Transport & transport,Config & config)
{
	for (int rank = 0 ; rank < transport.getWorldSize() ; rank++) {
		Json::Value & topology = config.getConfig("topology");
		assume(topology.isArray(),"The topology field must be an array !");
		Json::Value & localTopo = topology[rank];
		assume(localTopo.isArray(),"The topology childs must be an array !");
		assumeArg(localTopo.size() > 0,"You must have at least one unit per rank, currently get none on rank %1").arg(rank).end();
		for (int i = 0 ; i < (int)localTopo.size() ; i++) {
			std::string unit = localTopo[i].asString();
			if (unit == "EM")
				transport.setUnitType(rank,i,DAQ::UNIT_EVENT_MANAGER);
			else if (unit == "BU")
				transport.setUnitType(rank,i,DAQ::UNIT_BUILDER_UNIT);
			else if (unit == "RU")
				transport.setUnitType(rank,i,DAQ::UNIT_READOUT_UNIT);
			else if (unit == "FM")
				transport.setUnitType(rank,i,DAQ::UNIT_FILTER_MANAGER);
			else if (unit == "FU")
				transport.setUnitType(rank,i,DAQ::UNIT_FILTER_UNIT);
			else if (unit == "PU")
				transport.setUnitType(rank,i,DAQ::UNIT_PROFILE_UNIT);
			else
				DAQ_FATAL_ARG("Get invalid unit type in custom topology for rank %1 : %2").arg(rank).arg(unit).end();
		}
	}
}

/*******************  FUNCTION  *********************/
void Topology::setupTopology(Transport & transport,Config & config, bool local)
{
	bool custom = config.getAndSetConfigBool("testing","customTopology",false);
	
	if (custom) {
		setupCustomTopology(transport,config);
	} else {
		if (local)
			setupTopologyLocal(transport,config);
		else
			setupTopologyMPI(transport,config);
	}
}

}