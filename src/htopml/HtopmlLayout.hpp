/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_HTOPML_LAYOUT_HPP
#define DAQ_HTOPML_LAYOUT_HPP

/********************  HEADERS  *********************/
#include <htopml/JsonHttpNode.h>
#include <htopml/HtopmlHttpServer.h>
#include <transport/UnitType.hpp>


/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*********************  TYPES  **********************/
struct Layout
{
	UnitType * units;
	int nodes;
	int unitsPerNodes;
	bool * fail;
};

/*********************  CLASS  **********************/
class HtopmlLayoutHttpNode : public htopml::JsonHttpNode<Layout>
{
	public:
		HtopmlLayoutHttpNode(const std::string & addr);
		static void registerUnitArray(UnitType * units, int nodes, int unitsPerNodes, bool * deadNodes);
		
	private:
		static Layout layout;

};

/*******************  FUNCTION  *********************/
void convertToJson(htopml::JsonState & json,const Layout & value);

}

#endif //DAQ_HTOPML_LAYOUT_HPP