/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_HTOPML_COMM_TRACER_HPP
#define DAQ_HTOPML_COMM_TRACER_HPP

/********************  HEADERS  *********************/
#include <htopml/JsonHttpNode.h>
#include <htopml/HtopmlHttpServer.h>

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*********************  TYPES  **********************/
class CommTracer;
typedef CommTracer * CommTracerPtr;

/*********************  CLASS  **********************/
class HtopmlCommTraceHttpNode : public htopml::JsonHttpNode<CommTracerPtr>
{
	public:
		HtopmlCommTraceHttpNode(const std::string & addr);
		static void registerTracer(CommTracer * tracer);
	private:
		static CommTracerPtr tracer;
};

/*******************  FUNCTION  *********************/
void convertToJson(htopml::JsonState & json,const CommTracerPtr & value);

}

#endif //DAQ_HTOPML_COMM_TRACER_HPP
