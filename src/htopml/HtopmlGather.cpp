/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include <cassert>
#include "common/Debug.hpp"
#include "HtopmlGather.hpp"
#include "gather/PullGather.hpp"
#include <htopml/TemplatePageHttpNode.h>

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/********************  GLOBALS  *********************/
PullGatherPtrVector HtopmlGatherHttpNode::gathers;

/*******************  FUNCTION  *********************/
HtopmlGatherHttpNode::HtopmlGatherHttpNode ( const std::string& addr )
	:htopml::JsonHttpNode<PullGatherPtrVector>(addr,&gathers)
{

}

/*******************  FUNCTION  *********************/
void HtopmlGatherHttpNode::registerGather ( Gather* gather )
{
	assert(gather != nullptr);
	gathers.push_back(gather);
}

/*******************  FUNCTION  *********************/
void htopmlRegisterGatherPage ( htopml::HtopmlHttpServer & server )
{
	
	//setup dynamic nodes
	server.registerHttpNode(new HtopmlGatherHttpNode("/daqpipe/gather.json"),true);
	
	//mount some static files
	server.quickRegisterFile("/daqpipe/gather.css",HTOPML_PLUGIN_WWW "/gather.css",false);
	server.quickRegisterFile("/daqpipe/gather.js",HTOPML_PLUGIN_WWW "/gather.js",false);
	server.registerHttpNode(new htopml::TemplatePageHttpNode("/daqpipe/gather.html",HTOPML_PLUGIN_WWW "/gather.html",false));
	
	//register icon file 
	server.quickRegisterFile("/daqpipe/gather.png",HTOPML_PLUGIN_WWW "icons/gather.png",false);
	
	//setup icon
	std::string icon = "/daqpipe/gather.png";

	
	//add menu entry
	server.addMenuEntry("Pull gather","/daqpipe/gather.html",icon);
}

/*******************  FUNCTION  *********************/
void convertToJson ( htopml::JsonState& json, const PullGatherPtrVector& value )
{
	json.openArray();
	for (auto it : value)
		json.printValue(*it);
	json.closeArray();
}

/*********************  CONSTS  *********************/
HTOPML_REGISTER_MODULE(htopmlRegisterGatherPage);

}
