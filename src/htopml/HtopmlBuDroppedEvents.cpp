/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include <cassert>
#include "common/Debug.hpp"
#include "HtopmlBuDroppedEvents.hpp"
#include "units/ProfileUnit.hpp"
#include "transport/Transport.hpp"
#include <htopml/TemplatePageHttpNode.h>

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/********************  GLOBALS  *********************/
DroppedEvents HtopmlDropedEventsHttpNode::dropped = {
	nullptr,
	0,
	nullptr
};

/*******************  FUNCTION  *********************/
HtopmlDropedEventsHttpNode::HtopmlDropedEventsHttpNode ( const std::string& addr )
	:htopml::JsonHttpNode<DroppedEvents>(addr,&dropped)
{

}

/*******************  FUNCTION  *********************/
void HtopmlDropedEventsHttpNode::registerArray ( uint64_t* array, size_t size, uint64_t * total )
{
	HtopmlDropedEventsHttpNode::dropped.dropped = array;
	HtopmlDropedEventsHttpNode::dropped.size = size;
	HtopmlDropedEventsHttpNode::dropped.total = total;
}

/*******************  FUNCTION  *********************/
void htopmlRegisterDroppedPage(htopml::HtopmlHttpServer& server)
{
	
	//setup dynamic nodes
	server.registerHttpNode(new HtopmlDropedEventsHttpNode("/daqpipe/dropped.json"),true);
	
	//mount some static files
	server.quickRegisterFile("/daqpipe/dropped.css",HTOPML_PLUGIN_WWW "/dropped.css",false);
	server.quickRegisterFile("/daqpipe/dropped.js",HTOPML_PLUGIN_WWW "/dropped.js",false);
	server.registerHttpNode(new htopml::TemplatePageHttpNode("/daqpipe/dropped.html",HTOPML_PLUGIN_WWW "/dropped.html",false));
	//server.registerHttpNode(new MyHttpNode("/dapipe/dynamicConfig.json",true));

	//register icon file 
	server.quickRegisterFile("/daqpipe/dropped.png",HTOPML_PLUGIN_WWW "icons/dropped.png",false);
		
	//setup icon
	std::string icon = "/daqpipe/dropped.png";
	
	//add menu entry
	server.addMenuEntry("Dropped","/daqpipe/dropped.html",icon);
}

/*******************  FUNCTION  *********************/
void convertToJson ( htopml::JsonState& json, const DroppedEvents & filledPercentage )
{
	json.openStruct();
		json.printField("total",*(filledPercentage.total));
		json.printFieldArray("dropped",filledPercentage.dropped,filledPercentage.size);
	json.closeStruct();
}

/*********************  CONSTS  *********************/
HTOPML_REGISTER_MODULE(htopmlRegisterDroppedPage);

}
