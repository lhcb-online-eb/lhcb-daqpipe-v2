/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/

//#include "common/Debug.hpp"
#include "DynamicConfigHttpNode.hpp"
#include "htopml/mongoose.h"
#include <iostream>
#include <string>
#include "htopml/JsonState.h"
#include "htopml/TypeToJson.h"


using namespace std;

/********************  NAMESPACE  *******************/
namespace DAQ
{
	
//DynamicConfigHttpNode::registerParam();

ParamVec DynamicConfigHttpNode::params;
Transport * DynamicConfigHttpNode::transport;

/*******************  FUNCTION  *********************/
DynamicConfigHttpNode::DynamicConfigHttpNode (const std::string & path, bool strictPath)
:HttpNode(path,strictPath)
{

}

/*******************  FUNCTION  *********************/
DynamicConfigHttpNode::~DynamicConfigHttpNode()
{
}


/*******************  FUNCTION  *********************/
void DynamicConfigHttpNode::updateParam(int id, long int value)
{
	DynamicParam & cur = params[id];
	if(value >= cur.minValue && value<=cur.maxValue && value != cur.value )
	{
		cur.value = value;
		cur.callback(value);
	};

	
}

/*******************  FUNCTION  *********************/
void DynamicConfigHttpNode::registerTransport(Transport& transport)
{
	DynamicConfigHttpNode::transport = &transport;
}


/*******************  FUNCTION  *********************/
void DynamicConfigHttpNode:: onHttpRequest(htopml::HttpResponse & response, const htopml::HttpRequest & request)
{
	//fill the vector with the values from the HttpRequest
	int i =0;
	for(auto & it :params)
	{
 		command.type = UNIT_CMD_DYNAMIC_CONFIG;
 		command.args.dynamicConfig.id = i;
		i++;
		command.args.dynamicConfig.value = atol(request.getParam(it.name).c_str());
		Unit * unit = transport->getLocalUnit(0);
		for (int node = 0 ; node < transport->getWorldSize(); node++)
			transport->sendCommand(unit,node,0,command);
	};
	response.setHttpStatus(200);
	response.setMimeType("application/json");
	htopml::convertToJson(response.getStream(),params,true);
}

/*******************  FUNCTION  *********************/
void DynamicConfigHttpNode::registerParam(std::string nameParam, long defaultValue, long defaultMin, long defaultMax, std::function<void (long)> callback)
{
	
	DynamicParam param;
	param.name = nameParam;
	param.value = defaultValue; 
	param.minValue = defaultMin;
	param.maxValue = defaultMax; 
	param.callback = callback; 
	
	params.push_back(param);

}

/*******************  FUNCTION  *********************/
void convertToJson (htopml::JsonState & json, const DynamicParam & data)
{

	json.openStruct();
		json.printField("name", data.name);
		json.printField("value",data.value);
		json.printField("min",data.minValue);
		json.printField("max",data.maxValue);
	json.closeStruct();
	

	
}

/*******************  FUNCTION  *********************/
static void htopmlRegisterDynamicConfigHttpNode(htopml::HtopmlHttpServer& server)
{
	server.registerHttpNode(new DynamicConfigHttpNode("/daqpipe/dynamicConfig.json",true));
	server.quickRegisterFile("/daqpipe/bootstrap-slider/dist/bootstrap-slider.js",HTOPML_PLUGIN_WWW "/sliders/dist/bootstrap-slider.js",false);
	server.quickRegisterFile("/daqpipe/bootstrap-slider/dependencies/css/bootstrap.min.css",HTOPML_PLUGIN_WWW "/sliders/dependencies/css/bootstrap.min.css",false);
	server.quickRegisterFile("/daqpipe/bootstrap-slider/dist/css/bootstrap-slider.css",HTOPML_PLUGIN_WWW "/sliders/dist/css/bootstrap-slider.css",false);
	
}

/*********************  CONSTS  *********************/
HTOPML_REGISTER_MODULE(htopmlRegisterDynamicConfigHttpNode);

}
