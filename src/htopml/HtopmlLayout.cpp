/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include <cassert>
#include "common/Debug.hpp"
#include "HtopmlLayout.hpp"
#include <unistd.h>
#include <htopml/TemplatePageHttpNode.h>
#include <transport/Transport.hpp> 

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/********************  GLOBALS  *********************/
Layout HtopmlLayoutHttpNode::layout;

/*******************  FUNCTION  *********************/
HtopmlLayoutHttpNode::HtopmlLayoutHttpNode ( const std::string& addr )
	:htopml::JsonHttpNode<Layout>(addr,& layout)
{

}

/*******************  FUNCTION  *********************/
void HtopmlLayoutHttpNode::registerUnitArray ( UnitType * units, int nodes, int unitsPerNodes, bool * deadNodes)
{
	HtopmlLayoutHttpNode::layout.units = units;
	HtopmlLayoutHttpNode::layout.nodes = nodes;
	HtopmlLayoutHttpNode::layout.unitsPerNodes = unitsPerNodes;
	HtopmlLayoutHttpNode::layout.fail = deadNodes;

}

/*******************  FUNCTION  *********************/
void htopmlRegisterLayoutPage ( htopml::HtopmlHttpServer& server )
{

	
	//setup dynamic nodes
	server.registerHttpNode(new HtopmlLayoutHttpNode("/daqpipe/layout.json"),true);
	
	//mount some static files
	server.quickRegisterFile("/daqpipe/layout.css",HTOPML_PLUGIN_WWW "/layout.css",false);
	server.quickRegisterFile("/daqpipe/layout.js",HTOPML_PLUGIN_WWW "/layout.js",false);
	server.registerHttpNode(new htopml::TemplatePageHttpNode("/daqpipe/layout.html",HTOPML_PLUGIN_WWW "/layout.html",false));

	//register icon file 
	server.quickRegisterFile("/daqpipe/layout.png",HTOPML_PLUGIN_WWW "icons/layout.png",false);
	
	//setup icon
	std::string icon ="/daqpipe/layout.png";

	//add menu entry
	server.addMenuEntry("Layout","/daqpipe/layout.html",icon);
}

/*******************  FUNCTION  *********************/
/**
* Exports to JSON format a struct containing the name 
* of the host and an array with its units layout.
**/
void convertToJson ( htopml::JsonState& json, const Layout & value )
{
	const char * unitNames[UNIT_TYPE_COUNT] = {"EMPTY","EM","BU","RU","PU","CU","FU","FM"};
	//bool fail[value.nodes]
	char buffer[256];
	gethostname(buffer,256);
	json.openArray();
		for (int i = 0 ; i < value.nodes ; i++)
		{
			json.printListSeparator();
			json.openStruct();
				json.printField("host",buffer);
				json.openFieldArray("units");

				for (int j = 0 ; j < value.unitsPerNodes ; j++)
				{
					assume(value.units[i] < UNIT_TYPE_COUNT,"Invalid unit type.");
					json.printValue(unitNames[value.units[i*value.unitsPerNodes+j] ]);

				}
				json.closeFieldArray("units");
				json.printField("fail",value.fail[i]);
			json.closeStruct();
		}
	json.closeArray();
}

/*********************  CONSTS  *********************/
HTOPML_REGISTER_MODULE(htopmlRegisterLayoutPage);

}
