/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_HTOPML_DROPPED_EVENTS_HPP
#define DAQ_HTOPML_DROPPED_EVENTS_HPP

/********************  HEADERS  *********************/
#include <htopml/JsonHttpNode.h>
#include <htopml/HtopmlHttpServer.h>
//#include "transport/Transport.hpp"

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*********************  TYPES  **********************/
struct DroppedEvents
{
 	uint64_t * dropped;
 	size_t size;
	uint64_t * total;
}; 


/*********************  CLASS  **********************/
class HtopmlDropedEventsHttpNode: public htopml::JsonHttpNode<DroppedEvents>
{
	public:
		HtopmlDropedEventsHttpNode(const std::string & addr);
		static void registerArray(uint64_t* array, size_t size, uint64_t * total);
	private:
		static DroppedEvents dropped;
};

/*******************  FUNCTION  *********************/
void convertToJson(htopml::JsonState & json,const DroppedEvents & value);

}

#endif //DAQ_HTOPML_DROPPED_EVENTS_HPP
