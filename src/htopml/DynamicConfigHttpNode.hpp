/*****************************************************
             PROJECT  : htopml
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef HTOPML_DYNAMIC_CONFIG_HTTP_NODE_H
#define HTOPML_DYNAMIC_CONFIG_HTTP_NODE_H
 

 /********************  HEADERS  *********************/
#include <string>
#include <htopml/HtopmlHttpServer.h>
#include <map> 
#include <functional> 
#include "htopml/JsonState.h"
#include "htopml/HttpNode.h"
#include "htopml/HttpRequest.h"
#include "htopml/HttpResponse.h"
#include <htopml/JsonHttpNode.h>
#include "units/Command.hpp"
#include "units/Unit.hpp"
#include "transport/Transport.hpp"

/********************  NAMESPACE  *******************/
namespace DAQ
//namespace htopml
{


/*********************  STRUCT  *********************/
struct DynamicParam
{
	std::string name;
	long minValue; 
	long maxValue; 
	long value; 
	std::function<void (long)> callback;
};

/*********************** TYPES **********************/
typedef std::vector<DynamicParam> ParamVec;

/*********************  CLASS  **********************/

class DynamicConfigHttpNode : public htopml::HttpNode 
{
	public: 
		DynamicConfigHttpNode(const std::string & path,bool strictPath);
		virtual ~DynamicConfigHttpNode();
		virtual void onHttpRequest(htopml::HttpResponse & response, const htopml::HttpRequest & request);
		static void registerParam(std::string nameParam, long defaultValue, long defaultMin, long defaultMax, std::function<void (long)> callback);
		static void registerTransport(Transport & transport);
		static void updateParam(int id,long value);
	private:
		UnitCommand  command;
		static ParamVec params;
		static Transport * transport;
		static DynamicConfigHttpNode * node;
		
};

/*******************  FUNCTION  *********************/
void convertToJson (htopml::JsonState & json, const DAQ::DynamicParam & data);

}

#endif //HTOPML_DYNAMIC_CONFIG_HTTP_NODE_H

