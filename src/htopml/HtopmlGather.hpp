/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_HTOPML_GATHER_HPP
#define DAQ_HTOPML_GATHER_HPP

/********************  HEADERS  *********************/
#include <htopml/JsonHttpNode.h>
#include <htopml/HtopmlHttpServer.h>
#include <vector>

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*********************  TYPES  **********************/
class Gather;
typedef std::vector<Gather*> PullGatherPtrVector;

/*********************  CLASS  **********************/
class HtopmlGatherHttpNode : public htopml::JsonHttpNode<PullGatherPtrVector>
{
	public:
		HtopmlGatherHttpNode(const std::string & addr);
		static void registerGather(Gather * gather);
	private:
		static PullGatherPtrVector gathers;
};

/*******************  FUNCTION  *********************/
void convertToJson(htopml::JsonState & json,const PullGatherPtrVector & value);

}

#endif //DAQ_HTOPML_GATHER_HPP
