/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include <cassert>
#include "common/Debug.hpp"
#include "HtopmlFilledPercentage.hpp"
#include "units/ProfileUnit.hpp"
#include "transport/Transport.hpp"
#include <htopml/TemplatePageHttpNode.h>

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/********************  GLOBALS  *********************/
FilledPercentage HtopmlFilledPercentageHttpNode::filledPercentage = {
	nullptr,
	0
};

/*******************  FUNCTION  *********************/
HtopmlFilledPercentageHttpNode::HtopmlFilledPercentageHttpNode ( const std::string& addr )
	:htopml::JsonHttpNode<FilledPercentage>(addr,&filledPercentage)
{

}

/*******************  FUNCTION  *********************/
void HtopmlFilledPercentageHttpNode::registerFilledArray ( uint64_t* ruPercentage, size_t ruSize,uint64_t * buPercentage, size_t buSize )
{
	HtopmlFilledPercentageHttpNode::filledPercentage.ruPercentage = ruPercentage;
	HtopmlFilledPercentageHttpNode::filledPercentage.ruSize = ruSize;
	HtopmlFilledPercentageHttpNode::filledPercentage.buPercentage = buPercentage;
	HtopmlFilledPercentageHttpNode::filledPercentage.buSize = buSize;
}

/*******************  FUNCTION  *********************/
void htopmlRegisterFilledPercentagePage(htopml::HtopmlHttpServer& server)
{
	
	//setup dynamic nodes
	server.registerHttpNode(new HtopmlFilledPercentageHttpNode("/daqpipe/filledPercentage.json"),true);
	
	//mount some static files
	server.quickRegisterFile("/daqpipe/filledPercentage.css",HTOPML_PLUGIN_WWW "/filledPercentage.css",false);
	server.quickRegisterFile("/daqpipe/filledPercentage.js",HTOPML_PLUGIN_WWW "/filledPercentage.js",false);
	server.registerHttpNode(new htopml::TemplatePageHttpNode("/daqpipe/filledPercentage.html",HTOPML_PLUGIN_WWW "/filledPercentage.html",false));
	//server.registerHttpNode(new MyHttpNode("/dapipe/dynamicConfig.json",true));

	//register icon file 
	server.quickRegisterFile("/daqpipe/filledPercentage.png",HTOPML_PLUGIN_WWW "icons/filledPercentage.png",false);
		
	//setup icon
	std::string icon = "/daqpipe/filledPercentage.png";
	
	//add menu entry
	server.addMenuEntry("Buffering","/daqpipe/filledPercentage.html",icon);
}

/*******************  FUNCTION  *********************/
void convertToJson ( htopml::JsonState& json, const FilledPercentage & filledPercentage )
{
	json.openStruct();
		json.printFieldArray("ru",filledPercentage.ruPercentage,filledPercentage.ruSize);
		json.printFieldArray("bu",filledPercentage.buPercentage,filledPercentage.buSize);
	json.closeStruct();
}

/*********************  CONSTS  *********************/
HTOPML_REGISTER_MODULE(htopmlRegisterFilledPercentagePage);

}
