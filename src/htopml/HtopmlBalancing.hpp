/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_HTOPML_BALANCING_HPP
#define DAQ_HTOPML_BALANCING_HPP

/********************  HEADERS  *********************/
#include <htopml/JsonHttpNode.h>
#include <htopml/HtopmlHttpServer.h>
//#include "transport/Transport.hpp"

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*********************  TYPES  **********************/
struct Balance
 {
 	uint64_t * balance;
 	size_t size;
 }; 


/*********************  CLASS  **********************/
class HtopmlBalancingHttpNode: public htopml::JsonHttpNode<Balance>
{
	public:
		HtopmlBalancingHttpNode(const std::string & addr);
		static void registerUnitArray(uint64_t * balance, size_t size);
	private:
		static Balance balance;
};

/*******************  FUNCTION  *********************/
void convertToJson(htopml::JsonState & json,const Balance & value);

}

#endif //DAQ_HTOPML_BANDWIDTH_HPP
