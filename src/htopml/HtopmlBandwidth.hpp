/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_HTOPML_BANDWIDTH_HPP
#define DAQ_HTOPML_BANDWIDTH_HPP

/********************  HEADERS  *********************/
#include <htopml/JsonHttpNode.h>
#include <htopml/HtopmlHttpServer.h>

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*********************  TYPES  **********************/
class ProfileUnit;
typedef ProfileUnit * ProfileUnitPtr;

/*********************  CLASS  **********************/
class HtopmlBandwidthHttpNode : public htopml::JsonHttpNode<ProfileUnitPtr>
{
	public:
		HtopmlBandwidthHttpNode(const std::string & addr);
		static void registerUnit(ProfileUnit * unit);
	private:
		static ProfileUnitPtr unit;
};

/*******************  FUNCTION  *********************/
void convertToJson(htopml::JsonState & json,const ProfileUnitPtr & value);

}

#endif //DAQ_HTOPML_BANDWIDTH_HPP
