/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include <cassert>
#include "common/Debug.hpp"
#include "units/ProfileUnit.hpp"
#include "transport/Transport.hpp"
#include <htopml/TemplatePageHttpNode.h>
#include "htopml/ProcessHttpNode.h"
#include <htopml/HtopmlHttpServer.h>

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*******************  FUNCTION  *********************/
static void htopmlRegisterOpaStatsNode(htopml::HtopmlHttpServer& server)
{
	
	//setup dynamic nodes
	server.registerHttpNode(new htopml::SimpleProcessHttpNode("/daqpipe/opaStats.txt","hfi1stats","text/plain"));
	
	//mount some static files
	server.quickRegisterFile("/daqpipe/opaStats.css",HTOPML_PLUGIN_WWW "/opaStats.css",false);
	server.quickRegisterFile("/daqpipe/opaStats.js",HTOPML_PLUGIN_WWW "/opaStats.js",false);
	server.registerHttpNode(new htopml::TemplatePageHttpNode("/daqpipe/opaStats.html",HTOPML_PLUGIN_WWW "/opaStats.html",false));

	//register icon file 
	server.quickRegisterFile("/daqpipe/omnipath.png",HTOPML_PLUGIN_WWW "icons/omnipath.png",false);

	//setup icon
	std::string icon = "/daqpipe/omnipath.png";
// 	icon = "/linux/icons/memory.png";
// 	server.quickRegisterFile(icon,STATIC_DATA_PATH "/memory.png");
	
	//add menu entry
	server.addMenuEntry("OmniPath","/daqpipe/opaStats.html",icon);
}

/*********************  CONSTS  *********************/
HTOPML_REGISTER_MODULE(htopmlRegisterOpaStatsNode);

}
