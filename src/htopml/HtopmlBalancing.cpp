/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include <cassert>
#include "common/Debug.hpp"
#include "HtopmlBalancing.hpp"
#include "units/ProfileUnit.hpp"
#include "transport/Transport.hpp"
#include <htopml/TemplatePageHttpNode.h>

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/********************  GLOBALS  *********************/
Balance HtopmlBalancingHttpNode::balance = {
	nullptr,
	0
};

/*******************  FUNCTION  *********************/
HtopmlBalancingHttpNode::HtopmlBalancingHttpNode ( const std::string& addr )
	:htopml::JsonHttpNode<Balance>(addr,&balance)
{

}

/*******************  FUNCTION  *********************/
void HtopmlBalancingHttpNode::registerUnitArray ( uint64_t * balance, size_t size )
{
	HtopmlBalancingHttpNode::balance.balance = balance;
	HtopmlBalancingHttpNode::balance.size = size;
}

/*******************  FUNCTION  *********************/
void htopmlRegisterBalancePage(htopml::HtopmlHttpServer& server)
{
	
	//setup dynamic nodes
	server.registerHttpNode(new HtopmlBalancingHttpNode("/daqpipe/balance.json"),true);
	
	//mount some static files
	server.quickRegisterFile("/daqpipe/balance.css",HTOPML_PLUGIN_WWW "/balance.css",false);
	server.quickRegisterFile("/daqpipe/balance.js",HTOPML_PLUGIN_WWW "/balance.js",false);
	server.registerHttpNode(new htopml::TemplatePageHttpNode("/daqpipe/balance.html",HTOPML_PLUGIN_WWW "/balance.html",false));
	//server.registerHttpNode(new MyHttpNode("/dapipe/dynamicConfig.json",true));

	
	//register icon file 
	server.quickRegisterFile("/daqpipe/balance.png",HTOPML_PLUGIN_WWW "icons/balance.png",false);
	
	//setup icon
	std::string icon = "/daqpipe/balance.png";
	
	//add menu entry
	server.addMenuEntry("Balance","/daqpipe/balance.html",icon);
}

/*******************  FUNCTION  *********************/
void convertToJson ( htopml::JsonState& json, const Balance & balance )
{
	json.printArray(balance.balance,balance.size);
}

/*********************  CONSTS  *********************/
HTOPML_REGISTER_MODULE(htopmlRegisterBalancePage);

}
