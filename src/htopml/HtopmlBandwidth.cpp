/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include <cassert>
#include "common/Debug.hpp"
#include "HtopmlBandwidth.hpp"
#include "units/ProfileUnit.hpp"
#include <htopml/TemplatePageHttpNode.h>

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/********************  GLOBALS  *********************/
ProfileUnitPtr HtopmlBandwidthHttpNode::unit = nullptr;

/*******************  FUNCTION  *********************/
HtopmlBandwidthHttpNode::HtopmlBandwidthHttpNode ( const std::string& addr )
	:htopml::JsonHttpNode<ProfileUnitPtr>(addr,&unit)
{

}

/*******************  FUNCTION  *********************/
void HtopmlBandwidthHttpNode::registerUnit ( ProfileUnit* unit )
{
	HtopmlBandwidthHttpNode::unit = unit;
}

/*******************  FUNCTION  *********************/
void htopmlRegisterBandwidthPage(htopml::HtopmlHttpServer& server)
{
	
	//setup dynamic nodes
	server.registerHttpNode(new HtopmlBandwidthHttpNode("/daqpipe/bandwidth.json"),true);
	
	//mount some static files
	server.quickRegisterFile("/daqpipe/bandwidth.css",HTOPML_PLUGIN_WWW "/bandwidth.css",false);
	server.quickRegisterFile("/daqpipe/bandwidth.js",HTOPML_PLUGIN_WWW "/bandwidth.js",false);
	server.registerHttpNode(new htopml::TemplatePageHttpNode("/daqpipe/bandwidth.html",HTOPML_PLUGIN_WWW "/bandwidth.html",false));

	server.quickRegisterFile("/bandwidth1.js","/home/pribesme/projects/BandWidth/bandwidth1.js",false);
	server.registerHttpNode(new htopml::TemplatePageHttpNode("/bandwidth1.html","/home/pribesme/projects/BandWidth/bandwidth1.html",false));
	
	//register icon file 
	server.quickRegisterFile("/daqpipe/bandwidth.png",HTOPML_PLUGIN_WWW "icons/bandwidth.png",false);
	
	//setup icon
	std::string icon ="/daqpipe/bandwidth.png";
	
	//setup button images
	server.quickRegisterFile("/daqpipe/Less.png",HTOPML_PLUGIN_WWW "Less.png",false);
	server.quickRegisterFile("/daqpipe/LessSelected.png",HTOPML_PLUGIN_WWW "LessSelected.png",false);
	server.quickRegisterFile("/daqpipe/Plus.png",HTOPML_PLUGIN_WWW "Plus.png",false);
	server.quickRegisterFile("/daqpipe/PlusSelected.png",HTOPML_PLUGIN_WWW "PlusSelected.png",false);
	
	//add menu entry
	server.addMenuEntry("Bandwidth","/daqpipe/bandwidth.html",icon);
}

/*******************  FUNCTION  *********************/
void convertToJson ( htopml::JsonState& json, const ProfileUnitPtr& value )
{
	if (value != nullptr)
		convertToJson(json,*value);
}

/*********************  CONSTS  *********************/
HTOPML_REGISTER_MODULE(htopmlRegisterBandwidthPage);

}
