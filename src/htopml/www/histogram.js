/*****************************************************
 PROJECT  : lhcb-daqpipe
 VERSION  : 2.0.0
 DATE     : 01/2016
 AUTHOR   : Machen Jon - Intel
 LICENSE  : CeCILL-C
 *****************************************************/

    
/*******************  FUNCTION  *********************/
function printHistogram( data )
{

    var min = data.min;
    var max = data.max;
    var stepCount = data.stepCount;

    var range = ( max - min ) / stepCount;
    var categories = new Array( stepCount );

    for( var i = 0; i < stepCount; i++ ) {
      var start = min + (i * range);
      var stop = min + ( ( i + 1 ) * range );
      if( i > 0 ) {
      	start = start + 1;
      }
      categories[i] = Intl.NumberFormat( "en-US", { minimumFractionDigits : 2, maximumFractionDigits : 2 } ).format( start ) + 
	" - " + Intl.NumberFormat( "en-US", { minimumFractionDigits : 2, maximumFractionDigits : 2 } ).format( stop );
    }

    Chart = new Highcharts.Chart({
        chart: {
	    renderTo: 'histogram',
            type: 'column'
        },
        title: {
            text: 'DAQ-Pipe Bandwidth Benchmark'
        },
        subtitle: {
            text: 'Results'
        },
        xAxis: {
            categories: categories,
            crosshair: true,
	    labels : {
 		rotation : -90,
		y : 15,
		align: 'right'
	    }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Frequency'
            }
        },
        tooltip: {
            formatter: function() {
                return ''+
                  this.series.name +': '+ this.y +' (' + this.x + ' gbps)';
            }
        },
        plotOptions: {
            column: {
                pointPadding: 0,
                borderWidth: 0,
                groupPadding: 0,
                shadow: false
            }
        },
	legend: {
	    x : 70 
	},
        series: [{
                name: 'Throughput in Gbps',
                data: data.data
	}]
    });
}

/*******************  FUNCTION  *********************/
$(document).ready( function () {
    //fetch data
    $.getJSON("/daqpipe/histogram.json", printHistogram);
} );

