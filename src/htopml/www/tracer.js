/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/*********************  CLASS  **********************/
function BaseChart(elementId,data,command)
{
}

/*******************  FUNCTION  *********************/
BaseChart.prototype.init = function(elementId,data,command)
{
	//setup some common data
	this.trace = data;
	this.data = data.data;
	this.ticksPerSecond = data.ticksPerSecond;
	this.command = command;
	this.elementId = elementId;
	this.div = d3.select("#"+elementId);
	
	//defaults
	this.send = true;
	
	//margins
	this.xmargin = 10;
	this.ymargin = 10;
	this.axisMarginLeft = 70;
	this.axisMarginRight = 5;
	this.axisMarginTop = 5;
	this.axisMarginBottom = 20;
	
	//size
	var tmp = this.div.node().getBoundingClientRect();
	this.width = tmp.width - 2 * this.xmargin;
	this.height = 300 - 2 * this.ymargin;
	
	//prepare data
	this.setupGroupIds();
	this.computeBandwidth();
}

/*******************  FUNCTION  *********************/
BaseChart.prototype.accept = function(data)
{
	return this.command == data.isCommand && (this.send == data.isSend || this.send == undefined);
}

/*******************  FUNCTION  *********************/
BaseChart.prototype.toGbPs = function(value)
{
	return 8 * value * this.ticksPerSecond / 1000 / 1024 / 1024 / 1024;
}

/*******************  FUNCTION  *********************/
BaseChart.prototype.computeBandwidth = function()
{
	var data = this.data;
	for(id in data)
		data[id].bw = data[id].size/(data[id].end-data[id].start);
}

/*******************  FUNCTION  *********************/
BaseChart.prototype.collide = function(a,b)
{
	return (!(a.start > b.end || a.end < b.start) && (a.isCommand == b.isCommand) && (a.isSend == b.isSend || this.send == undefined));
}

/*******************  FUNCTION  *********************/
BaseChart.prototype.setupGroupIds = function()
{
	var data = this.data;
	
	//setup id
	for(id in data)
	{
		//count collision
		var notAvail = {};
		for (var i = 0 ; i < id ; i++)
			if (this.collide(data[id],data[i]))
				notAvail[data[i].group] = true;
		//select avail
		var g = 0;
		for (var i = 0 ; i < id ; i++)
		{
			if (notAvail[i] == undefined)
			{
				g = i;
				break;
			}
		}
		//setup
		data[id].group = g;
	}
}

/*********************  CLASS  **********************/
CommChart.prototype = new BaseChart();        // Here's where the inheritance occurs 
CommChart.prototype.constructor=CommChart;

/*********************  CLASS  **********************/
function CommChart(elementId,data,command)
{
	//init
	this.init(elementId,data,command);
	
	//draw
	this.prepareDrawElements();
	this.drawElements();
	this.drawData();
}

/*******************  FUNCTION  *********************/
CommChart.prototype.prepareDrawElements = function()
{
	var cur = this;
	
	//xmin/max
	this.xMax = d3.max(this.data, function(d) { return d.end;} );
	this.xMin = d3.min(this.data, function(d) { return d.start;} );
	
	//scale
	this.xScale = d3.scale.linear().domain([this.xMin,this.xMax]).range([cur.axisMarginLeft,cur.width-cur.axisMarginLeft - cur.axisMarginRight]);
	
	//y scale for commands
	this.yMax = d3.max(this.data, function(d) { return cur.accept(d) ? d.group : 0 ;} );
	this.yScale = d3.scale.ordinal().rangeRoundBands([cur.axisMarginTop, cur.height - cur.axisMarginBottom - cur.axisMarginTop], .4, 1)
		.domain(this.data.map(function (d) {return cur.accept(d)?d.group:0;}));
	
	//build axis
	this.xAxis = d3.svg.axis().scale(this.xScale).orient("bottom")
						.tickFormat(function(d) {return (d * 1000 / cur.ticksPerSecond).toFixed(2)+ " s";});
	this.yAxis = d3.svg.axis().scale(this.yScale).orient("left");
	
	//color scale
	var bwMin = d3.min(this.data, function(d) { return cur.accept(d)?d.bw:0;} );
	var bwMax = d3.max(this.data, function(d) { return cur.accept(d)?d.bw:0;} );
	this.colorScale = d3.scale.linear()  
			.domain([bwMin,bwMax])
			.range(['gray', 'green'])
}

/*******************  FUNCTION  *********************/
CommChart.prototype.updateData = function(data)
{
	var cur = this;
	
	//update
	this.trace = data;
	this.data = data.data;
	this.ticksPerSecond = data.ticksPerSecond;
	
	//prepare data
	this.setupGroupIds();
	this.computeBandwidth();
	
	//prepare draw again
	this.prepareDrawElements();
	
	//reshape axis
	this.svg.selectAll("g.y.axis")
		.call(this.yAxis);
	this.svg.selectAll("g.x.axis")
		.call(this.xAxis);
	
	//update data
	this.drawData();
}

/*******************  FUNCTION  *********************/
CommChart.prototype.updateSend = function(send)
{
	this.send = send;
	this.updateData(this.trace);
}

/*******************  FUNCTION  *********************/
CommChart.prototype.drawElements = function()
{
	var cur = this;
	
	//create SVG
	this.svg = this.div.append("svg")
						.attr("width", this.width)
						.attr("height",this.height);
	
	//x axis
	this.svg.append("g").attr("class", "x axis")
					.attr("transform", "translate(0,"+(cur.height-cur.axisMarginBottom)+")")
					.call(this.xAxis);
					
	//y axis
	this.svg.append("g").attr("class", "y axis")
					.attr("transform", "translate("+cur.axisMarginLeft+",0)")
					.call(this.yAxis);
					
	//tip
	this.tip = d3.tip()
		.attr('class', 'd3-tip')
		.offset([-10, 0])
		.html(function(d) {
			return "<strong>Bw:</strong> <span style='color:red'>" + cur.toGbPs(d.bw).toFixed(2) + "</span> <strong>Size:</strong> <span style='color:red'>" + d.size + "</span>";
		})
	this.svg.call(this.tip);
}

/*******************  FUNCTION  *********************/
CommChart.prototype.drawData = function()
{
	var cur = this;
	
	//group
	if (this.dataGroup == undefined)
		this.dataGroup = this.svg.append("g");
	
	//build commands
	this.dataRects = this.dataGroup.selectAll("rect")
							.data(this.data);

	//update
	this.dataRects.transition()
		.duration(2000)
			.attr("x", function (d) { return cur.xScale(d.start); })
			.attr("y", function (d) { return cur.yScale(d.group); })
			.attr("height", cur.yScale.rangeBand())
			.attr("width", function (d) { return cur.xScale(d.end)-cur.xScale(d.start); })
			.style("fill", function (d) { return cur.colorScale(d.bw);})
			.style("visibility",function(d) {return cur.accept(d)?'':'hidden'});

	//new
	this.dataRects.enter()
		.append("rect")
			.attr("x", function (d) { return cur.xScale(d.start); })
			.attr("y", function (d) { return cur.yScale(d.group); })
			.attr("height", cur.yScale.rangeBand())
			.attr("width", function (d) { return cur.xScale(d.end)-cur.xScale(d.start); })
			.style("fill", function (d) { return cur.colorScale(d.bw);})
			.on('mouseover', this.tip.show)
			.on('mouseout', this.tip.hide)
			.style("visibility",function(d) {return cur.accept(d)?'':'hidden'});
	
	//exit
	this.dataRects.exit()
		.transition()
		.duration(300)
			.attr("width", 0)
			.remove();
}

/*********************  CLASS  **********************/
BwChart.prototype = new BaseChart();        // Here's where the inheritance occurs 
BwChart.prototype.constructor=BwChart;

/*********************  CLASS  **********************/
function BwChart(elementId,data,command)
{
	//init
	this.init(elementId,data,command);
	
	//comptue bw
	this.buildAggregateBandwidth();
	
	//draw
	this.prepareDrawElements();
	this.drawElements();
	this.drawData();
}

/*******************  FUNCTION  *********************/
BwChart.prototype.prepareDrawElements = function()
{
	var cur = this;
	
	//xmin/max
	this.xMax = d3.max(this.data, function(d) { return d.end;} );
	this.xMin = d3.min(this.data, function(d) { return d.start;} );
	
	//color scale total bw
	var yMin = d3.min(this.ranges, function(d) { return d.bw;} );
	var yMax = d3.max(this.ranges, function(d) { return d.bw;} );
	this.yScale = d3.scale.linear().range([cur.axisMarginTop, cur.height - cur.axisMarginTop - cur.axisMarginBottom])
			.domain([yMax,yMin]);
	
	//scale
	this.xScale = d3.scale.linear().domain([this.xMin,this.xMax]).range([cur.axisMarginLeft,cur.width-cur.axisMarginLeft - cur.axisMarginRight]);
		
	//build axis
	this.xAxis = d3.svg.axis().scale(this.xScale).orient("bottom")
						.tickFormat(function(d) {return (d * 1000 / cur.ticksPerSecond).toFixed(2)+ " s";});
	this.yAxis = d3.svg.axis().scale(this.yScale).orient("left")
						.tickFormat(function(d) {return cur.toGbPs(d).toFixed(2)+ " Gb/s";});

	//color
	this.colorScale = d3.scale.linear()  
			.domain([yMin,yMax])
			.range(['gray', 'green'])
}

/*******************  FUNCTION  *********************/
BwChart.prototype.updateData = function(data)
{
	var cur = this;
	
	//update
	this.trace = data;
	this.data = data.data;
	this.ticksPerSecond = data.ticksPerSecond;
	
	//prepare data
	this.setupGroupIds();
	this.computeBandwidth();
	this.buildAggregateBandwidth();
	
	//prepare draw again
	this.prepareDrawElements();
	
	//reshape axis
	this.svg.selectAll("g.y.axis")
		.call(this.yAxis);
	this.svg.selectAll("g.x.axis")
		.call(this.xAxis);
	
	//update data
	this.drawData();
}

/*******************  FUNCTION  *********************/
BwChart.prototype.updateSend = function(send)
{
	this.send = send;
	this.updateData(this.trace);
}

/*******************  FUNCTION  *********************/
BwChart.prototype.drawElements = function()
{
	var cur = this;
	
	//create SVG
	this.svg = this.div.append("svg")
						.attr("width", this.width)
						.attr("height",this.height);
	
	//x axis
	this.svg.append("g").attr("class", "x axis")
					.attr("transform", "translate(0,"+(cur.height-cur.axisMarginBottom)+")")
					.call(this.xAxis);
					
	//y axis
	this.svg.append("g").attr("class", "y axis")
					.attr("transform", "translate("+cur.axisMarginLeft+",0)")
					.call(this.yAxis);
					
	//tip
	this.tip = d3.tip()
		.attr('class', 'd3-tip')
		.offset([-10, 0])
		.html(function(d) {
			return "<strong>Bw:</strong> <span style='color:red'>" + cur.toGbPs(d.bw).toFixed(2) + "</span>";
		})
	this.svg.call(this.tip);
}

/*******************  FUNCTION  *********************/
BwChart.prototype.drawData = function()
{
	var cur = this;
	
	//group
	if (this.dataGroup == undefined)
		this.dataGroup = this.svg.append("g");
	
	//build commands
	this.dataRects = this.dataGroup.selectAll("rect")
							.data(this.ranges);

	//setup line for mean bw
	if (this.meanLine == undefined)
		this.meanLine = this.svg.append("g")
							.append("line");
	
	//update
	this.meanLine
		.attr("x1", cur.xScale(0))
		.attr("y1", cur.yScale(this.mean))
		.attr("x2", cur.xScale(this.xMax))
		.attr("y2", cur.yScale(this.mean))
		.attr("stroke-width", 2)
		.attr("stroke-dasharray","5,5")
		.attr("stroke", "black");

	//update
	this.dataRects.transition()
		.duration(2000)
			.attr("x", function(d) { return cur.xScale(d.start); })
			.attr("y", function(d) { return cur.yScale(d.bw);})
			.attr("width", function(d) { return cur.xScale(d.end)-cur.xScale(d.start); })
			.attr("height", function(d) { return cur.yScale(0) - cur.yScale(d.bw);})
			.style("fill", function (d) { return cur.colorScale(d.bw);})

	//new
	this.dataRects.enter()
		.append("rect")
			.attr("class","totBwChart")
			.attr("x", function(d) { return cur.xScale(d.start); })
			.attr("y", function(d) { return cur.yScale(d.bw);})
			.attr("width", function(d) { return cur.xScale(d.end)-cur.xScale(d.start); })
			.attr("height", function(d) { return cur.yScale(0) - cur.yScale(d.bw);})
			.style("fill", function (d) { return cur.colorScale(d.bw);})
			.on('mouseover', cur.tip.show)
			.on('mouseout', cur.tip.hide);
	
	//exit
	this.dataRects.exit()
		.transition()
		.duration(300)
			.attr("width", 0)
			.remove();
}

/*******************  FUNCTION  *********************/
BwChart.prototype.buildAggregateBandwidth = function()
{
	var data = this.data;
	//first search all cut points for summary bw
	var cutPts = {};
	for (var id in data)
	{
		if (this.accept(data[id]))
		{
			cutPts[data[id].start] = true;
			cutPts[data[id].end] = true;
		}
	}
	//extract and sort
	var cutPtsArray = [];
	for (var id in cutPts)
		cutPtsArray.push(id);
	cutPtsArray = cutPtsArray.sort(function(a,b){return a-b;});
	//build ranges
	var ranges = [];
	var prev = 0;
	for (var id in cutPtsArray)
	{
		ranges.push({start:prev,end:cutPtsArray[id],size:0});
		prev = cutPtsArray[id];
		if (ranges[id].end - ranges[id].start < 0)
			debugger;
	}
	//now merge on ranges
	for (var id in ranges)
	{
		for (var i in data)
		{
			if (ranges[id].end - ranges[id].start < 0)
				debugger;
			if (this.accept(data[i]))
				if (ranges[id].start >= data[i].start && ranges[id].end <= data[i].end)
					ranges[id].size += (ranges[id].end - ranges[id].start) * data[i].bw;
		}
	}
	
	//compute bw
	for(id in ranges)
		ranges[id].bw = ranges[id].size/(ranges[id].end-ranges[id].start);
	
	//attach
	this.ranges = ranges;
	
	//compute mean
	this.mean = 0;
	var tot = 0;
	for (var id in ranges)
	{
		this.mean += ranges[id].size;
		tot += ranges[id].end - ranges[id].start;
	}
	this.mean = this.mean / tot;
}

/*******************  FUNCTION  *********************/
function autoRefresh()
{
	$.getJSON("/daqpipe/tracer.json", function(data){
		commandChart.updateData(data);
		rdmaChart.updateData(data);
		bwChart.updateData(data);
		setTimeout("autoRefresh()",2000);
	});
}

/*******************  FUNCTION  *********************/
var commandChart;
var rdmaChart;
var bwChart;
$(document).ready(function() {
	$.getJSON("/daqpipe/tracer.json", function(data){
		commandChart = new CommChart('chart-command',data,true);
		rdmaChart = new CommChart('chart-rdma',data,false);
		bwChart = new BwChart('chart-bandwidth',data,false);
	});
	
	d3.selectAll("input[name=selection]").on("change",function() {
		var send;
		//update config
		if (this.value == 'send')
			send = true;
		else if (this.value == 'recv')
			send = false;
		else if (this.value == 'both')
			send = undefined;
		//refresh
		commandChart.updateSend(send);
		rdmaChart.updateSend(send);
		bwChart.updateSend(send);
	});

	var hasAutoRefresh = false;
	d3.selectAll("button[name=autoRefresh]").on("click",function() {
		if (!hasAutoRefresh)
		{
			hasAutoRefresh = true;
			autoRefresh();
		}
	});
});
