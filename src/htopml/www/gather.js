/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/*******************  FUNCTION  *********************/
function printGather(data)
{
	var section = d3.select(".gathers");
	
	var state = section.selectAll("div")
		.data(data)
		.enter()
		.append('div')
		.attr('class','section');
	
	var title = state.append('h1')
		.text(function(d) {return "Credit "+d.creditId;});
	
	var ranks = state.selectAll('div')
		.data(function(d) { return d.state.ressources; })
		.enter()
		.append('div')
		.attr('class','daqpipe-gather-rank');
		
	var units = ranks.selectAll('div')
		.data(function(d) { return d; })
		.enter()
		.append('div')
		.attr('class','daqpipe-gather-unit');
	
	var ressource = units.selectAll('div')
		.data(function(d) { return d; })
		.enter()
		.append('div')
		.attr('class',function(d) { if (d) return 'daqpipe-gather-ressource active'; else return 'daqpipe-gather-ressource inactive';});
}

/*******************  FUNCTION  *********************/
$(function() {
	//fetch data
	$.getJSON( "/daqpipe/gather.json", printGather);
})