var chartRu = null; 
var chartBu = null;

function requestDataBu() {
    $.ajax({
        url: 'filledPercentage.json',
        contentType: 'application/json; charset=utf-8',
//         data: json,
//         dataType: 'json',
        success: function(point) {

            var i = 0;
            var series = [];
			var emptyPerc = []; 
             
            var categories = new Array();
            for ( i = 0 ; i < point.bu.length ; i++)
                categories[i] = "BU " + i;
            chartBu.xAxis[0].setCategories(categories, false);

            for ( i = 0 ; i < point.bu.length ; i++)
			{
                series[i]=point.bu[i];
				emptyPerc[i] = 100-point.bu[i];
			}
             
            chartBu.series[0].setData(emptyPerc);
            chartBu.series[1].setData(series);
            chartBu.redraw();
            //console.log(chart.xAxis[0].categories);
            //console.log(chart.series[0]);
            // call it again after one second
            setTimeout(requestDataBu, 1000);
            },
                type: "GET",
//          async: false,
//          error:function (xhr, status, error){
//          alert("Erreur de chargement du fichier '"+self.url+"' : "+xhr.responseText+" ("+status+" - "+error+")");
//          },
    });
}

function requestDataRu() {
    $.ajax({
        url: 'filledPercentage.json',
        contentType: 'application/json; charset=utf-8',
//         data: json,
//         dataType: 'json',
        success: function(point) {

            var i = 0;
            var series = [];
			var emptyPerc = []; 
             
            var categories = new Array();
            for ( i = 0 ; i < point.ru.length ; i++)
                categories[i] = "RU " + i;
            chartRu.xAxis[0].setCategories(categories, false);

            for ( i = 0 ; i < point.ru.length ; i++)
			{
                series[i]=point.ru[i];
				emptyPerc[i] = 100-point.ru[i];
			}
             
            chartRu.series[0].setData(emptyPerc);
            chartRu.series[1].setData(series);
            chartRu.redraw();
            //console.log(chart.xAxis[0].categories);
            //console.log(chart.series[0]);
            // call it again after one second
            setTimeout(requestDataRu, 1000);
            },
                type: "GET",
//          async: false,
//          error:function (xhr, status, error){
//          alert("Erreur de chargement du fichier '"+self.url+"' : "+xhr.responseText+" ("+status+" - "+error+")");
//          },
    });
}

$(document).ready(function() {
    chartRu = new Highcharts.Chart({
		chart: {
		renderTo: 'containerRu',
		type: 'bar',
		events:{
			load: requestDataRu
		}
		},
		title: {
		text: 'Filled percentage in Readout Units'
		},
		xAxis: {
		categories: ['0']
		},
		yAxis: {
		min: 0,
		max:100,
		title: {
			text: 'Filled percentage'
		}
		},
		legend: {
				reversed: true
		},
		plotOptions: {
			series: {
				stacking: 'normal'
			}
		},
		series: [{
		name: 'empty',
		color: 'black',
		data: [0]
		},{
		name: "filled",
		color: "green",
		data: [0]
		}]
	});    
	
	chartBu = new Highcharts.Chart({
		chart: {
		renderTo: 'containerBu',
		type: 'bar',
		events:{
			load: requestDataBu
		}
		},
		title: {
		text: 'Filled percentage in Builder Units'
		},
		xAxis: {
		categories: ['0']
		},
		yAxis: {
		min: 0,
		max:100,
		title: {
			text: 'Filled percentage'
		}
		},
		legend: {
				reversed: true
		},
		plotOptions: {
			series: {
				stacking: 'normal'
			}
		},
		series: [{
		name: 'empty',
		color: 'black',
		data: [0]
		},{
		name: "filled",
		color: "green",
		data: [0]
		}]
	});   
});
