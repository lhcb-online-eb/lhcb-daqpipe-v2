var chart2 = null;
var chart = null;
var lastPoint = null;
var overTime = [
	{
		date:0,
		pct05:0,
		pct25:0,
		pct50:0,
		pct75:0,
		pct95:0,
	},
	{
		date:1,
		pct05:3e+9,
		pct25:3e+9,
		pct50:3e+9,
		pct75:3e+9,
		pct95:3e+9,
	}
];
var numCursors = 0; 
var params = [];
var maxValues = []; 
var minValues = []; 
var changePlus = [];
var changeLess = [];

function updateOverTime(points)
{
  var t = overTime[overTime.length-1].date + 1;
  var p = points.sort();
  var min = p[0];
  var max = p[p.length-1];
  var q1 = p[Math.floor(1*p.length/4)];
  var q2 = p[Math.round(2*p.length/4)];
  var q3 = p[Math.ceil(3*p.length/4)];
  
  var state = {
    date:t,
    pct05:min,
    pct25:q1,
    pct50:q2,
    pct75:q3,
    pct95:max
  };
  overTime.push(state);
  
  overTime = overTime.slice(-100);
  return state;

}

//Request of the data for the first graph
function requestData() {
    $.ajax({
		url: 'bandwidth.json',
		contentType: 'application/json; charset=utf-8',
//         data: json,
//         dataType: 'json',
        success: function(point) {
          var i = 0;
          var state = updateOverTime(point);
			    var series = [];
			 
          var categories = new Array();
          for ( i = 0 ; i < point.length ; i++)
             categories[i] = "RU " + i;
          chart.xAxis[0].setCategories(categories, false);

			   for ( i = 0 ; i < point.length ; i++)
            series[i]=point[i];
			   
         chart.series[0].setData(series);
         //console.log(chart.xAxis[0].categories);
         //console.log(chart.series[0]);
			   chart.redraw();
          // call it again after one second
         setTimeout(requestData, 1000);
          },
            type: "GET",
//         async: false,
//         error:function (xhr, status, error){
// 		alert("Erreur de chargement du fichier '"+self.url+"' : "+xhr.responseText+" ("+status+" - "+error+")");
// 		},
    });
}


$(document).ready(function(){
  chart = new Highcharts.Chart({
    chart: {
      renderTo: 'container',
      type: 'column',
      animation: false,
      events:{
        load: requestData
      }
    },
    title: {
      text: 'Bandwidth'
    },
    xAxis: {
      categories: ['0']
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Bandwidth'
      },
      stackLabels: {
        enabled: true,
        style: {
          color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
        }
      }
    },
    tooltip: {
      formatter: function() {
        return ''+
          this.series.name +': '+ this.y +' ('+ Math.round(this.percentage) +'%)';
      }
    },
    plotOptions: {
      column: {
//        stacking: 'percent',
        borderWidth: 0,
        groupPadding: 0
      }
    },
    series: [{
      name: 'bw',
      color: 'green',
      data: [0]
    }]
  });
});


/*******************************************************************/
// Request of the data for the second graph

function requestData2() {
    $.ajax({
    url: 'bandwidth.json',
    contentType: 'application/json; charset=utf-8',
//         data: json,
//         dataType: 'json',
        success: function(point) {
      var state = updateOverTime(point);
      var series = chart2.series[0],
          shift = series.data.length > 50; 

      chart2.series[0].addPoint([state.date, state.pct50], false,shift);
      chart2.series[1].addPoint([state.date, state.pct05, state.pct95],false,shift);
      //console.log(state.pct50);

      chart2.redraw();
      // call it again after one second
      setTimeout(requestData2, 1000);
	  
	  
	  updateCursors();
      
        },
        type: "GET",
//         async: false,
//         error:function (xhr, status, error){
//    alert("Erreur de chargement du fichier '"+self.url+"' : "+xhr.responseText+" ("+status+" - "+error+")");
//    },
    });
}




$(document).ready(function(){
  buildCursors();
  chart2 = new Highcharts.Chart({
    chart: {
      renderTo: 'trend',
      events: {
        load: requestData2
      }
    }, 
    title: {
      text:'Total Bandwidth over time'
    },
        xAxis: {
            type: 'linear',
      data: [0]
      
        },

        yAxis: {
            min: 0,
            title: {
                text: null
            }
        },

        tooltip: {
            crosshairs: true,
            shared: true,
            valueSuffix: ''
        },
        legend: {
        },

        series: [{
            name: 'Average',
            data:  [0],
            zIndex: 1,
            marker: {
                fillColor: 'white',
                lineWidth: 2,
                lineColor: Highcharts.getOptions().colors[0]
            }
        }, {
            name: 'Range',
            type: 'arearange',
            data:  [[0,0,0]],
            lineWidth: 0,
            linkedTo: ':previous',
            color: "green",
            fillOpacity: 1,
            zIndex: 0
        }]
    });
});

/****************************************************/

function buildCursors(){
/**  
** Generate a cursor for each one of the parameters
** in http://localhost:8080/dapipe/dynamicConfig.json
**/ 

    $.ajax({
    url: '/daqpipe/dynamicConfig.json',
    contentType: 'application/json; charset=utf-8',
//         data: json,
//         dataType: 'json',
        success: function(data) {
			var i = 0;
			numCursors = data.length;
			//console.log(data);
			//console.log(numCursors);
			
			for(i=0;i<numCursors;i++)
			{
				var name = data[i].name;
				params.push(name);
				maxValues.push(data[i].max);
				minValues.push(data[i].min);
				changePlus.push(false);
				changeLess.push(false);

			
				d3.select("#cursors").append("input")
				.attr("id","c"+i)
				.attr("name",name)
				.attr("type","text")
				.attr("data-slider-min",data[i].min)
				.attr("data-slider-max",data[i].max)
				.attr("data-slider-value",data[i].value)
				.attr("data-slider-tooltip","hide");
				
				
				d3.select("#cursors")
				.append("span")
				.append("td")
				.attr("id","param"+i)
				.attr("style","padding-left:2em")
				.attr("width","200px")
				.html(data[i].name+":")
				.style("color","white");
				
				buttonLess = d3.select("#cursors")
				.append("span")
				.append("td")
				.attr("style","padding-left:2em")

				buttonLess.append("img")
				.attr("src","/daqpipe/Less.png")
				.attr("width", 30)
				.attr("height", 30)
				.attr("id","c"+i+"less")
				.attr("style","padding-right:2em")
				.attr("onmouseenter","setAttribute('src','/daqpipe/LessSelected.png')")
				.attr("onmouseout","setAttribute('src','/daqpipe/Less.png')");
				
				buttonPlus = d3.select("#cursors")
				.append("span")
				.append("td")
				.attr("style","padding-left:2em")

				buttonPlus.append("img")
				.attr("src","/daqpipe/Plus.png")
				.attr("width", 30)
				.attr("height", 30)
				.attr("id","c"+i+"plus")
				.attr("style","padding-right:2em")
				.attr("onmouseenter","setAttribute('src','/daqpipe/PlusSelected.png')")
				.attr("onmouseout","setAttribute('src','/daqpipe/Plus.png')");

				d3.select("#cursors").append("br");
				
			}
			
          },
            type: "GET"
    });
};



function updateCursors()
{
	var i = 0;
	var url = "/daqpipe/dynamicConfig.json?";
	for(i=0;i<numCursors;i++)
	{
	
		d3.select("#c"+i+"plus").on("click",function(event){
			i=this.id.replace(/[^0-9]/g, ''); 
			changePlus[i] = true;
		});
		
		d3.select("#c"+i+"less").on("click",function(event){
			i=this.id.replace(/[^0-9]/g, ''); 
			changeLess[i] = true;
		});
		
		var slider = $("#c"+i).bootstrapSlider();
		var value = slider.bootstrapSlider("getValue");
		
		if (changePlus[i]==true)
		{
			if (value< maxValues[i])
				value += 1;
			changePlus[i]=false;
			
		};
		
		if (changeLess[i]==true)
		{
			if (value> minValues[i])
				value -= 1;
			changeLess[i]=false;
			
		};
		
		slider.bootstrapSlider("setValue",value);
		console.log(value);
		url +=params[i]+"="+value+"&";
		
		d3.select("#param"+i).html(params[i]+":   "+value);
		
	};
	d3.select("#cursors").attr("action",url);
	var t = setTimeout("$('#cursors').submit();",10);
	
	$('#cursors').on('submit', function(event){
    event.preventDefault(); 
    $.ajax({
        url: $(this).attr('action'),
        type: $(this).attr('method'),
        data: $(this).serialize(),
        success: function(html) {
        }
    });
    //return false; 
});
	
}

