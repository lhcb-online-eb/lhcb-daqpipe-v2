/*****************************************************
 PROJECT  : lhcb-daqpipe
 VERSION  : 2.0.0
 DATE     : 01/2016
 AUTHOR   : Machen Jon - Intel
 LICENSE  : CeCILL-C
 *****************************************************/

/*******************  GLOBALS  *********************/
var chart = null;

/*******************  FUNCTION  *********************/

function requestData() {
    $.ajax({
        url: 'gather-histogram.json',
        contentType: 'application/json; charset=utf-8',
//         data: json,
//         dataType: 'json',
        success: function(data) {

            var i = 0;
            var series = [];
            var min = data.min;
            var max = data.max;
            var stepCount = data.stepCount;

            var range = ( max - min ) / stepCount;
            var categories = new Array( stepCount );

            for ( i = 0 ; i < data.data.length ; i++)
            {
                series[i]=data.data[i];
            }
               
            chart.series[0].setData(series);

             for( var i = 0; i < stepCount; i++ ) {
                var start = min + (i * range);
                var stop = min + ( ( i + 1 ) * range );
                if( i > 0 ) {
                    start = start + 1;
                }
                categories[i] = Intl.NumberFormat( "en-US", { minimumFractionDigits : 2, maximumFractionDigits : 2 } ).format( start ) + 
                " - " + Intl.NumberFormat( "en-US", { minimumFractionDigits : 2, maximumFractionDigits : 2 } ).format( stop );
                }
            chart.xAxis[0].setCategories(categories);
            chart.redraw();
            // call it again after one second
            setTimeout(requestData, 1000);
            },
                type: "GET",
//          async: false,
//          error:function (xhr, status, error){
//          alert("Erreur de chargement du fichier '"+self.url+"' : "+xhr.responseText+" ("+status+" - "+error+")");
//          },
    });
}

/*******************  FUNCTION  *********************/

$(document).ready( function () {

    chart = new Highcharts.Chart({
        chart: {
        renderTo: 'histogram',
            type: 'column',
        events:{
            load: requestData
        }
        },
        title: {
            text: 'DAQ-Pipe Time for each gather'
        },
        subtitle: {
            text: 'Results'
        },
        xAxis: {
            categories: [0],
            crosshair: true,
        labels : {
        rotation : -90,
        y : 15,
        align: 'right'
        },
        title: {
            text: 'Time (ms)'
        }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Gathers',
            }            
        },
        tooltip: {
            formatter: function() {
                return ''+
                  this.series.name +': '+ this.y +' (in ' + this.x + ' ms)';
            }
        },
        plotOptions: {
            column: {
                pointPadding: 0,
                borderWidth: 0,
                groupPadding: 0,
                shadow: false
            }
        },
    legend: {
        x : 70 
    },
        series: [{
               name: 'Gathers',
               data: [0]
    }]
    });

}); 

