var chart2 = null;
var chart = null;
var lastPoint = null;
var overTime = [
	{
		date:0,
		pct05:0,
		pct25:0,
		pct50:0,
		pct75:0,
		pct95:0,
	},
	{
		date:1,
		pct05:3e+9,
		pct25:3e+9,
		pct50:3e+9,
		pct75:3e+9,
		pct95:3e+9,
	}
];
var numCursors = 0; 
var params = [];
var maxValues = []; 
var minValues = []; 
var changePlus = [];
var changeLess = [];

function updateOverTime(points)
{
  var t = overTime[overTime.length-1].date + 1;
  var p = points.sort();
  var min = p[0];
  var max = p[p.length-1];
  var q1 = p[Math.floor(1*p.length/4)];
  var q2 = p[Math.round(2*p.length/4)];
  var q3 = p[Math.ceil(3*p.length/4)];
  
  var state = {
    date:t,
    pct05:min,
    pct25:q1,
    pct50:q2,
    pct75:q3,
    pct95:max
  };
  overTime.push(state);
  
  overTime = overTime.slice(-100);
  return state;

}

//Request of the data for the first graph
function requestData() {
    $.ajax({
		url: 'dropped.json',
		contentType: 'application/json; charset=utf-8',
//         data: json,
//         dataType: 'json',
        success: function(point) {
          var i = 0;
          var state = updateOverTime(point.dropped);
			    var series = [];
			 
          var categories = new Array();
          for ( i = 0 ; i < point.dropped.length ; i++)
             categories[i] = "RU " + i;
          chart.xAxis[0].setCategories(categories, false);

			   for ( i = 0 ; i < point.dropped.length ; i++)
            series[i]=point.dropped[i];
			   
         chart.series[0].setData(series);
         //console.log(chart.xAxis[0].categories);
         //console.log(chart.series[0]);
			   chart.redraw();
          // call it again after one second
         setTimeout(requestData, 1000);
          },
            type: "GET",
//         async: false,
//         error:function (xhr, status, error){
// 		alert("Erreur de chargement du fichier '"+self.url+"' : "+xhr.responseText+" ("+status+" - "+error+")");
// 		},
    });
}


$(document).ready(function(){
  chart = new Highcharts.Chart({
    chart: {
      renderTo: 'container',
      type: 'column',
      animation: false,
      events:{
        load: requestData
      }
    },
    title: {
      text: 'Dropped events'
    },
    xAxis: {
      categories: ['0']
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Dropped'
      },
      stackLabels: {
        enabled: true,
        style: {
          color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
        }
      }
    },
    tooltip: {
      formatter: function() {
        return ''+
          this.series.name +': '+ this.y +' ('+ Math.round(this.percentage) +'%)';
      }
    },
    plotOptions: {
      column: {
//        stacking: 'percent',
        borderWidth: 0,
        groupPadding: 0
      }
    },
    series: [{
      name: 'bw',
      color: 'green',
      data: [0]
    }]
  });
});


/*******************************************************************/
// Request of the data for the second graph

function requestData2() {
    $.ajax({
    url: 'dropped.json',
    contentType: 'application/json; charset=utf-8',
//         data: json,
//         dataType: 'json',
        success: function(point) {
      var state = updateOverTime(point.dropped);
      var series = chart2.series[0],
          shift = series.data.length > 50; 

      chart2.series[0].addPoint([state.date, state.pct50], false,shift);
      chart2.series[1].addPoint([state.date, state.pct05, state.pct95],false,shift);
      //console.log(state.pct50);

      chart2.redraw();
      // call it again after one second
      setTimeout(requestData2, 1000);
	  
	 
      
        },
        type: "GET",
//         async: false,
//         error:function (xhr, status, error){
//    alert("Erreur de chargement du fichier '"+self.url+"' : "+xhr.responseText+" ("+status+" - "+error+")");
//    },
    });
}




$(document).ready(function(){
  chart2 = new Highcharts.Chart({
    chart: {
      renderTo: 'trend',
      events: {
        load: requestData2
      }
    }, 
    title: {
      text:'Total dropped over time'
    },
        xAxis: {
            type: 'linear',
      data: [0]
      
        },

        yAxis: {
            min: 0,
            title: {
                text: null
            }
        },

        tooltip: {
            crosshairs: true,
            shared: true,
            valueSuffix: ''
        },
        legend: {
        },

        series: [{
            name: 'Average',
            data:  [0],
            zIndex: 1,
            marker: {
                fillColor: 'white',
                lineWidth: 2,
                lineColor: Highcharts.getOptions().colors[0]
            }
        }, {
            name: 'Range',
            type: 'arearange',
            data:  [[0,0,0]],
            lineWidth: 0,
            linkedTo: ':previous',
            color: "green",
            fillOpacity: 1,
            zIndex: 0
        }]
    });
});

