/*****************************************************
 PROJECT  : lhcb-daqpipe
 VERSION  : 2.0.0
 DATE     : 01/2016
 AUTHOR   : Machen Jon - Intel
 LICENSE  : CeCILL-C
 *****************************************************/

/*******************  FUNCTION  *********************/
function buildTable(data)
{
	
	//Values of the parameters
	
	// TxWords
	var regExp = /TxWords \s* [0-9]*/;
	var txWordsValue = regExp.exec(data);
	txWordsValue = txWordsValue[0].split(" "); 
	console.log(txWordsValue);
	txWordsValue = txWordsValue[txWordsValue.length-1];
	
	//TxPkt 
	var regExp = /TxPkt \s* [0-9]*/;
	var txPktValue = regExp.exec(data);
	txPktValue = txPktValue[0].split(" "); 
	console.log(txPktValue);
	txPktValue = txPktValue[txPktValue.length-1];
	
	//RxWords 
	var regExp = /RxWords \s* [0-9]*/;
	var rxWordsValue = regExp.exec(data);
	rxWordsValue  = rxWordsValue [0].split(" "); 
	console.log(rxWordsValue);
	rxWordsValue  = rxWordsValue [rxWordsValue .length-1];
	
	//RxPkt 
	var regExp = /RxPkt \s* [0-9]*/;
	var rxPktValue = regExp.exec(data);
	rxPktValue  = rxPktValue[0].split(" "); 
	console.log(rxPktValue);
	rxPktValue  = rxPktValue[rxPktValue.length-1];
	
	//KernIntr 
	var regExp = /KernIntr  \s* [0-9]*/;
	var kernIntrValue = regExp.exec(data);
	kernIntrValue  = kernIntrValue[0].split(" "); 
	console.log(kernIntrValue);	
	kernIntrValue  = kernIntrValue[kernIntrValue.length-1];
	
	//intr
	var regExp = /Intr  \s* [0-9]*/;
	var intrValue = regExp.exec(data);
	intrValue = intrValue[0].split(" "); 
	console.log(intrValue);
	intrValue = intrValue[intrValue.length-1];
	
	
	//Table 
	
	//Head
	d3.select("#tab")
	.attr("border","1" )
	.attr("align","center")
	.append("thead")
	.attr("id","head")
	
	d3.selectAll("#head")
	.append("th")
	.text("Parameter")
	.attr("width", "130")
	
	d3.selectAll("#head")
	.append("th")
	.text("Value")
	.attr("width", "130");
	
	// Body
	//TxWords
	d3.selectAll("#tab")
	.append("tbody")
	.attr("id","body")
	.append("tr")
	.attr("id","TxWords")
	.append("td")
	.text("TxWords")
	
	
	d3.selectAll("#TxWords")
	.append("td")
	.text(txWordsValue);
	
	//TxPkt
	d3.selectAll("#body")
	.append("tr")
	.attr("id","txpkt")
	.append("td")
	.text("TxPkt");
	
	d3.selectAll("#txpkt")
	.append("td")
	.text(txPktValue);
	
	//RxWords
	d3.selectAll("#body")
	.append("tr")
	.attr("id","RxWords")
	.append("td")
	.text("RxWords");
	
	d3.selectAll("#RxWords")
	.append("td")
	.text(rxWordsValue);
	
	//RxPkt
	d3.selectAll("#body")
	.append("tr")
	.attr("id","RxPkt")
	.append("td")
	.text("RxPkt");
	
	d3.selectAll("#RxPkt")
	.append("td")
	.text(rxPktValue);
	
	//KernIntr
	d3.selectAll("#body")
	.append("tr")
	.attr("id","KernIntr")
	.append("td")
	.text("KernIntr");
	
	d3.selectAll("#KernIntr")
	.append("td")
	.text(kernIntrValue);
	
	//Intr
	d3.selectAll("#body")
	.append("tr")
	.attr("id","intr")
	.append("td")
	.text("Intr");
	
	d3.selectAll("#intr")
	.append("td")
	.text(intrValue);
	
}; 


    
/*******************  FUNCTION  *********************/
$(document).ready( function () {
    //fetch data
	$.ajax({
		url: 'opaStats.txt',
		dataType: 'text',
//         data: json,
//         dataType: 'json',
        success: function(data) {
			$("#OpaText").html(data);
			buildTable(data);
		},
		type: "GET",
		
	});
});
