/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/** 
* Request of data from layout.json in order to build a
* table which shows the unit layout of the different 
* modules.  
**/
$(document).ready(function () {
	$.ajax({
		url: 'layout.json',
		contentType: 'application/json; charset=utf-8',
//         data: json,
//         dataType: 'json',
        success: function(data) {
      console.log(data);
      // create table 
      var table = d3.selectAll("#tab").append("tbody");
      
      // add rows 
      
      var row = table.selectAll("tbody tr")
      .data(data)
      .enter()
      .append("tr")
      .style("background-color", function(data){
      if(data.fail == true)
            return "red";      
      })
      .style("color", function(data){
      if(data.fail == true)
            return "black";
      });

      // create the columns 
      row.append("td").html(function(d,i){  
        return i;});
      
      row.append("td").html(function(d){
        var i; 
        var col = "";
        for (i = 0; i< d.units.length;i++)
        {
          if (d.units[i] != "EMPTY")
           col = col + "<span class= 'cell'>" + d.units[i] + "</span> ";
        }
      return col});
      
      row.append("td").html(function(d){return d.host});
  		},
  		type: "GET",  		
	});

});



