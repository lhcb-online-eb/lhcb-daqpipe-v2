var chart = null; 

function requestData() {
    $.ajax({
        url: 'balance.json',
        contentType: 'application/json; charset=utf-8',
//         data: json,
//         dataType: 'json',
        success: function(point) {

            var i = 0;
            var series = [];
             
            var categories = new Array();
            for ( i = 0 ; i < point.length ; i++)
                categories[i] = "BU " + i;
            chart.xAxis[0].setCategories(categories, false);

            for ( i = 0 ; i < point.length ; i++)
                series[i]=point[i];
               
            chart.series[0].setData(series);
            chart.redraw();
            //console.log(chart.xAxis[0].categories);
            //console.log(chart.series[0]);
            // call it again after one second
            setTimeout(requestData, 1000);
            },
                type: "GET",
//          async: false,
//          error:function (xhr, status, error){
//          alert("Erreur de chargement du fichier '"+self.url+"' : "+xhr.responseText+" ("+status+" - "+error+")");
//          },
    });
}

$(document).ready(function() {
    chart = new Highcharts.Chart({
    chart: {
      renderTo: 'container',
      type: 'column',
      animation: false,
      events:{
        load: requestData
      }
    },
    title: {
      text: 'Balance'
    },
    xAxis: {
      categories: ['0']
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Number of events'
      },
      stackLabels: {
        enabled: true,
        style: {
          color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
        }
      }
    },
    tooltip: {
      formatter: function() {
        return ''+
          this.series.name +': '+ this.y +' ('+ Math.round(this.percentage) +'%)';
      }
    },
    plotOptions: {
      column: {
//        stacking: 'percent',
        borderWidth: 0,
        groupPadding: 0
      }
    },
    series: [{
      name: 'bw',
      color: 'green',
      data: [0]
    }]
  });      
});
