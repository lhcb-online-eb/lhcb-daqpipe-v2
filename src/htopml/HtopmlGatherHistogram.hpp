/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_HTOPML_GATHER_HISTOGRAM_HPP
#define DAQ_HTOPML_GATHER_HISTOGRAM_HPP

/********************  HEADERS  *********************/
#include <htopml/JsonHttpNode.h>
#include <htopml/HtopmlHttpServer.h>
#include "common/Histogram.hpp"

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*********************  TYPES  **********************/
typedef Histogram * HistogramPtr;

/*********************  CLASS  **********************/
class HtopmlGatherHistogramHttpNode : public htopml::JsonHttpNode<HistogramPtr>
{
	public:
		HtopmlGatherHistogramHttpNode(const std::string & addr);
		static void Register(Histogram * histogramTimeGather);  
	private:
		static Histogram * histogramTimeGather ;
};

}

#endif //DAQ_HTOPML_GATHER_HPP
