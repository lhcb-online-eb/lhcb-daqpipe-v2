/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Machen Jon - Intel 
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_HTOPML_HISTOGRAM_HPP
#define DAQ_HTOPML_HISTOGRAM_HPP

/********************  HEADERS  *********************/
#include <htopml/JsonHttpNode.h>
#include <htopml/HtopmlHttpServer.h>
#include "HtopmlCommTracer.hpp"
#include "common/Histogram.hpp"
/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*********************  TYPES  **********************/
class Histogram;
typedef Histogram * HistogramPtr;

/*********************  CLASS  **********************/
class HtopmlHistogramHttpNode : public htopml::JsonHttpNode<HistogramPtr>
{
	public:
		HtopmlHistogramHttpNode(const std::string & addr);
		static void registerHistogram(DAQ::Histogram * histogram);
	private:
		static HistogramPtr histogram;
};

/*******************  FUNCTION  *********************/
void convertToJson(htopml::JsonState & json,const HistogramPtr & value);

}

#endif //DAQ_HTOPML_HISTOGRAM_HPP

