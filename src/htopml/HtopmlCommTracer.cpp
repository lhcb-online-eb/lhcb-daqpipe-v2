/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include <cassert>
#include "common/Debug.hpp"
#include "HtopmlCommTracer.hpp"
#include "drivers/CommTracer.hpp"
#include <htopml/TemplatePageHttpNode.h>

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/********************  GLOBALS  *********************/
CommTracerPtr HtopmlCommTraceHttpNode::tracer = nullptr;

/*******************  FUNCTION  *********************/
HtopmlCommTraceHttpNode::HtopmlCommTraceHttpNode ( const std::string& addr )
	:htopml::JsonHttpNode<CommTracerPtr>(addr,&tracer)
{

}

/*******************  FUNCTION  *********************/
void HtopmlCommTraceHttpNode::registerTracer ( CommTracer* tracer )
{
	HtopmlCommTraceHttpNode::tracer = tracer;
}

/*******************  FUNCTION  *********************/
void htopmlRegisterCommTracerPage ( htopml::HtopmlHttpServer& server )
{
// 	DAQ_INFO_ARG("Load PullGather page into htopml");
	
	//setup dynamic nodes
	server.registerHttpNode(new HtopmlCommTraceHttpNode("/daqpipe/tracer.json"),true);
	
	//mount some static files
	server.quickRegisterFile("/daqpipe/tracer.css",HTOPML_PLUGIN_WWW "/tracer.css",false);
	server.quickRegisterFile("/daqpipe/tracer.js",HTOPML_PLUGIN_WWW "/tracer.js",false);
	server.registerHttpNode(new htopml::TemplatePageHttpNode("/daqpipe/tracer.html",HTOPML_PLUGIN_WWW "/tracer.html",false));
	
	//register icon file 
	server.quickRegisterFile("/daqpipe/tracer.png",HTOPML_PLUGIN_WWW "icons/tracer.png",false);
	
	

	//setup icon
	std::string icon="/daqpipe/tracer.png";
// 	icon = "/linux/icons/memory.png";
// 	server.quickRegisterFile(icon,STATIC_DATA_PATH "/memory.png");
	
	//add menu entry
	server.addMenuEntry("Comm tracer","/daqpipe/tracer.html",icon);
}

/*******************  FUNCTION  *********************/
void convertToJson ( htopml::JsonState& json, const CommTracerPtr& value )
{
	if (value != nullptr)
		convertToJson(json,*value);
}

/*********************  CONSTS  *********************/
HTOPML_REGISTER_MODULE(htopmlRegisterCommTracerPage);

}
