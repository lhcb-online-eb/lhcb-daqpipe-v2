/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Machen Jon - Intel
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include <cassert>
#include "common/Debug.hpp"
#include "HtopmlHistogram.hpp"
#include "drivers/CommTracer.hpp"
#include <htopml/TemplatePageHttpNode.h>

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/********************  GLOBALS  *********************/
HistogramPtr HtopmlHistogramHttpNode::histogram = nullptr;

/*******************  FUNCTION  *********************/
HtopmlHistogramHttpNode::HtopmlHistogramHttpNode ( const std::string& addr )
	:htopml::JsonHttpNode<HistogramPtr>(addr,&histogram)
{

}

/*******************  FUNCTION  *********************/
void HtopmlHistogramHttpNode::registerHistogram ( Histogram* histogram )
{
	HtopmlHistogramHttpNode::histogram = histogram;
}

/*******************  FUNCTION  *********************/
void htopmlRegisterHistogramPage ( htopml::HtopmlHttpServer & server )
{
	
	//setup dynamic nodes
	server.registerHttpNode(new HtopmlHistogramHttpNode("/daqpipe/histogram.json"),true);
	
	//mount some static files
	server.quickRegisterFile("/daqpipe/histogram.css",HTOPML_PLUGIN_WWW "/histogram.css",false);
	server.quickRegisterFile("/daqpipe/histogram.js",HTOPML_PLUGIN_WWW "/histogram.js",false);
	server.registerHttpNode(new htopml::TemplatePageHttpNode("/daqpipe/histogram.html",HTOPML_PLUGIN_WWW "/histogram.html",false));
	
	
	//register icon file 
	server.quickRegisterFile("/daqpipe/histogram.png",HTOPML_PLUGIN_WWW "icons/histogram.png",false);
	
	
	//setup icon
	std::string icon = "/daqpipe/histogram.png";
	
	//add menu entry
	server.addMenuEntry("Histogram","/daqpipe/histogram.html",icon);
}

/*******************  FUNCTION  *********************/
void convertToJson ( htopml::JsonState& json, const HistogramPtr& value )
{
	if( value != nullptr ) {
            convertToJson(json, *value);
        }
}

/*********************  CONSTS  *********************/
HTOPML_REGISTER_MODULE(htopmlRegisterHistogramPage);

}
