/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_HTOPMLFILLED_PERCENTAGE_HPP
#define DAQ_HTOPMLFILLED_PERCENTAGE_HPP

/********************  HEADERS  *********************/
#include <htopml/JsonHttpNode.h>
#include <htopml/HtopmlHttpServer.h>
//#include "transport/Transport.hpp"

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*********************  TYPES  **********************/
struct FilledPercentage
{
 	uint64_t * ruPercentage;
 	size_t ruSize;
	uint64_t * buPercentage;
 	size_t buSize;
}; 


/*********************  CLASS  **********************/
class HtopmlFilledPercentageHttpNode: public htopml::JsonHttpNode<FilledPercentage>
{
	public:
		HtopmlFilledPercentageHttpNode(const std::string & addr);
		static void registerFilledArray(uint64_t * ruPercentage, size_t ruSize,uint64_t * buPercentage, size_t buSize);
	private:
		static FilledPercentage filledPercentage;
};

/*******************  FUNCTION  *********************/
void convertToJson(htopml::JsonState & json,const FilledPercentage & value);

}

#endif //DAQ_HTOPML_FILLED_PERCENTAGE_HPP
