/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include <cassert>
#include "common/Debug.hpp"
#include "HtopmlHistogram.hpp"
#include "HtopmlGatherHistogram.hpp"
#include "gather/PullGather.hpp"
#include <htopml/TemplatePageHttpNode.h>

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/********************  GLOBALS  *********************/
Histogram * HtopmlGatherHistogramHttpNode::histogramTimeGather = nullptr;

/*******************  FUNCTION  *********************/
HtopmlGatherHistogramHttpNode::HtopmlGatherHistogramHttpNode ( const std::string& addr )
	:htopml::JsonHttpNode<HistogramPtr>(addr,&histogramTimeGather)
{

}

/*******************  FUNCTION  *********************/
void HtopmlGatherHistogramHttpNode::Register(Histogram * histogram)
{
	HtopmlGatherHistogramHttpNode::histogramTimeGather = histogram; 
}

/*******************  FUNCTION  *********************/
void htopmlRegisterGatherHistogramPage ( htopml::HtopmlHttpServer & server )
{
	
	//setup dynamic nodes
	server.registerHttpNode(new HtopmlGatherHistogramHttpNode("/daqpipe/gather-histogram.json"),true);
	
	//mount some static files
	server.quickRegisterFile("/daqpipe/gather-histogram.css",HTOPML_PLUGIN_WWW "/gather-histogram.css",false);
	server.quickRegisterFile("/daqpipe/gather-histogram.js",HTOPML_PLUGIN_WWW "/gather-histogram.js",false);
	server.registerHttpNode(new htopml::TemplatePageHttpNode("/daqpipe/gather-histogram.html",HTOPML_PLUGIN_WWW "/gather-histogram.html",false));
	
	//register icon file 
	server.quickRegisterFile("/daqpipe/gatherHistogram.png",HTOPML_PLUGIN_WWW "icons/gatherHistogram.png",false);
	
	//setup icon
	std::string icon = "/daqpipe/gatherHistogram.png";
	
	//add menu entry
	server.addMenuEntry("Gather-histogram","/daqpipe/gather-histogram.html",icon);
}

/*********************  CONSTS  *********************/
HTOPML_REGISTER_MODULE(htopmlRegisterGatherHistogramPage);

}
