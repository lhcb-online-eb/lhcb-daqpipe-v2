/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include <mpi.h>
#include "common/Debug.hpp"
#include "LauncherMPI.hpp"

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*******************  FUNCTION  *********************/
LauncherMPI::LauncherMPI ( Config * config ) : Launcher(config)
{
	this->rank = -1;
	this->worldSize = 0;
}

/*******************  FUNCTION  *********************/
LauncherMPI::~LauncherMPI ()
{

}

/*******************  FUNCTION  *********************/
void LauncherMPI::initialize ( int argc, char ** argv )
{
	MPI_Init(&argc,&argv);
	MPI_Comm_rank(MPI_COMM_WORLD,&rank);
	MPI_Comm_size(MPI_COMM_WORLD,&worldSize);
}

/*******************  FUNCTION  *********************/
void LauncherMPI::finalize ()
{
	MPI_Finalize();
}

/*******************  FUNCTION  *********************/
int LauncherMPI::getRank ()
{
	return rank;
}

/*******************  FUNCTION  *********************/
int LauncherMPI::getWorldSize ()
{
	return worldSize;
}

/*******************  FUNCTION  *********************/
void LauncherMPI::barrier ()
{
	MPI_Barrier(MPI_COMM_WORLD);
}

/*******************  FUNCTION  *********************/
void LauncherMPI::commit ()
{
	//not support, might not be used by MPI driver
}

/*******************  FUNCTION  *********************/
std::string LauncherMPI::get ( const std::string& key, int rank )
{
	//not support, might not be used by MPI driver
	DAQ_FATAL("Not supported");
	return "undefined";
}

/*******************  FUNCTION  *********************/
void LauncherMPI::set ( const std::string& key, const std::string& value )
{
	//not support, might not be used by MPI driver
	DAQ_FATAL("Not supported");
}

}
