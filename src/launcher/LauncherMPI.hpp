/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_LAUNCHER_MPI_HPP
#define DAQ_LAUNCHER_MPI_HPP

/********************  HEADERS  *********************/
#include "Launcher.hpp"

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*********************  CLASS  **********************/
/**
 * MPI implementation of the launcher.
**/
class LauncherMPI : public Launcher
{
	public:
		LauncherMPI( DAQ::Config* config );
		virtual ~LauncherMPI();
		virtual void initialize ( int argc, char ** argv );
		virtual void finalize ();
		virtual int getWorldSize();
		virtual int getRank();
		virtual void barrier ();
		virtual void commit ();
		virtual std::string get ( const std::string& key, int rank );
		virtual void set ( const std::string& key, const std::string& value );
	private:
		/** Store the value as cache to avoid calling the MPI methode each time we need it. **/
		int rank;
		/** Store the value as cache to avoid calling the MPI methode each time we need it. **/
		int worldSize;
};

}

#endif //DAQ_LAUNCHER_MPI_HPP