/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_LAUNCHER_HPP
#define DAQ_LAUNCHER_HPP

/********************  HEADERS  *********************/
#include <string>
#include <common/Config.hpp>

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*********************  CLASS  **********************/
/**
 * Launcher is responsible of the initialisation step to start all the jobs
 * and make them connected. It can be deviated to support MPI or the hydra launcher
 * from MPICH to be used with hard codded fabric support.
**/
class Launcher
{
	public:
		/** Constructor, do nothing in this interface class. **/
		Launcher(Config * config){this->config = config;};
		/** Destructor to ensure good inheritance. **/
		virtual ~Launcher(){};
		/**
		 * Initialisation function which has to be called before any other
		 * functions of the class.
		 * @param argc Number of arguments provided to the main function.
		 * @param argc Arguments provided to the main function.
		**/
		virtual void initialize(int argc, char ** argv) = 0;
		/**
		 * Finish all the internal operations before exit.
		**/
		virtual void finalize() = 0;
		/**
		 * Return the number of processed launched by the current job. **/
		virtual int getWorldSize() = 0;
		/**
		 * Return the rank of the current process.
		**/
		virtual int getRank() = 0;
		/**
		 * Do a barrier to wait all processes to be there.
		**/
		virtual void barrier() = 0;
		/**
		 * Setup some key/values attached to units and shared to every one (mostly for hydra/PMI launcher).
		**/
		virtual void set(const std::string & key,const std::string & value) = 0;
		/**
		 * Read a key/value attached to the given unit.
		**/
		virtual std::string get(const std::string & key,int rank) = 0;
		/**
		 * Commit the values which have been set so that all can see them.
		**/
		virtual void commit() = 0;
	protected:
		Config * config;
};

}

#endif //DAQ_LAUNCHER_HPP