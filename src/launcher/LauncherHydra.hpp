/*****************************************************
             PROJECT  : lhcb-daqpipe
             DATE     : 12/2017
             AUTHOR   : Sébastien Valat - CERN
*****************************************************/

#ifndef HYDRA_LAUNCHER_HPP
#define HYDRA_LAUNCHER_HPP

/********************  HEADERS  *********************/
#include <string>
#include "Launcher.hpp"

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*********************  CLASS  **********************/
/**
 * Provide a wrapper to hide the low level functions from PMI (hydra).
**/
class LauncherHydra : public Launcher
{
	public:
		LauncherHydra(Config * config);
		virtual void initialize ( int argc, char** argv );
		virtual void finalize();
		virtual int getRank();
		virtual int getWorldSize();
		virtual void barrier();
		virtual void set(const std::string & key,const std::string & value);
		virtual std::string get(const std::string & key,int rank);
		virtual void commit();
	private:
		/** Store the KVS DB name to access keys/values. **/
		char kvsName[1024];
};

}

#endif //HYDRA_LAUNCHER_HPP
