/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include <common/Debug.hpp>
#include "LauncherFake.hpp"

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*******************  FUNCTION  *********************/
LauncherFake::LauncherFake ( Config* config, int nodes )
              : Launcher ( config )
{
	this->rank = config->launcherRank;
	this->nodes = nodes;
	assumeArg(rank < nodes && rank >= 0,"Invalid rank (%1), should be positive and lower than number of nodes (%2)").arg(rank).arg(nodes).end();
}

/*******************  FUNCTION  *********************/
LauncherFake::~LauncherFake ( void )
{

}

/*******************  FUNCTION  *********************/
void LauncherFake::initialize ( int argc, char ** argv )
{

}

/*******************  FUNCTION  *********************/
void LauncherFake::finalize ( void )
{

}

/*******************  FUNCTION  *********************/
int LauncherFake::getRank ( void )
{
	return rank;
}

/*******************  FUNCTION  *********************/
int LauncherFake::getWorldSize ( void )
{
	return nodes;
}

/*******************  FUNCTION  *********************/
void LauncherFake::barrier ( void )
{
	//nothing to do there is only one process
}

/*******************  FUNCTION  *********************/
void LauncherFake::commit ( void )
{
	//not support, might not be used by local driver
	//DAQ_FATAL("Not supported");
}

/*******************  FUNCTION  *********************/
std::string LauncherFake::get ( const std::string& key, int rank )
{
	//not support, might not be used by local driver
	DAQ_FATAL("Not supported");
	return "";
}

/*******************  FUNCTION  *********************/
void LauncherFake::set ( const std::string& key, const std::string& value )
{
	//not support, might not be used by local driver
	DAQ_FATAL("Not supported");
}

}
