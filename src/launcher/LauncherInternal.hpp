/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_LAUNCHER_INTERNAL_HPP
#define DAQ_LAUNCHER_INTERNAL_HPP

/********************  HEADERS  *********************/
#include "Launcher.hpp"
#include "../tools/internal-launcher/Client.hpp"

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*********************  CLASS  **********************/
/**
 * Dummy implementation of the launcher to work localy in a uniq process.
**/
class LauncherInternal : public Launcher
{
	public:
		LauncherInternal(Config * config);
		virtual ~LauncherInternal();
		virtual void initialize ( int argc, char ** argv );
		virtual void finalize ();
		virtual int getWorldSize();
		virtual int getRank();
		virtual void barrier ();
		virtual void commit ();
		virtual std::string get ( const std::string& key, int rank );
		virtual void set ( const std::string& key, const std::string& value );
	private:
		InternalLauncher::Client * client;
};

}

#endif //DAQ_LAUNCHER_INTERNAL_HPP