/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include <common/Debug.hpp>
#include "LauncherLocal.hpp"

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*******************  FUNCTION  *********************/
LauncherLocal::LauncherLocal ( Config* config )
              : Launcher ( config )
{

}

/*******************  FUNCTION  *********************/
void LauncherLocal::initialize ( int argc, char ** argv )
{

}

/*******************  FUNCTION  *********************/
void LauncherLocal::finalize ()
{

}

/*******************  FUNCTION  *********************/
int LauncherLocal::getRank ()
{
	return 0;
}

/*******************  FUNCTION  *********************/
int LauncherLocal::getWorldSize ()
{
	return 1;
}

/*******************  FUNCTION  *********************/
void LauncherLocal::barrier ()
{
	//nothing to do there is only one process
}

/*******************  FUNCTION  *********************/
void LauncherLocal::commit ()
{
	//not support, might not be used by local driver
	//DAQ_FATAL("Not supported");
}

/*******************  FUNCTION  *********************/
std::string LauncherLocal::get ( const std::string& key, int rank )
{
	//not support, might not be used by local driver
	DAQ_FATAL("Not supported");
	return "";
}

/*******************  FUNCTION  *********************/
void LauncherLocal::set ( const std::string& key, const std::string& value )
{
	//not support, might not be used by local driver
	DAQ_FATAL("Not supported");
}

}
