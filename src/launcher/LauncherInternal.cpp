/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include <common/Debug.hpp>
#include "LauncherInternal.hpp"

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*******************  FUNCTION  *********************/
LauncherInternal::LauncherInternal ( Config * config )
                 :Launcher(config)
{
	client = nullptr;
}

/*******************  FUNCTION  *********************/
LauncherInternal::~LauncherInternal ()
{
	if (client != nullptr)
		delete client;
}

/*******************  FUNCTION  *********************/
void LauncherInternal::initialize ( int argc, char ** argv )
{
	std::string server = config->getAndSetConfigString("launcher.internal","server","127.0.0.1");
	int port = config->getAndSetConfigInt("launcher.internal","port",8086);
	
	client = new InternalLauncher::Client(server,config->launcherRank,port);
}

/*******************  FUNCTION  *********************/
void LauncherInternal::finalize ()
{

}

/*******************  FUNCTION  *********************/
int LauncherInternal::getRank ()
{
	return client->getRank();
}

/*******************  FUNCTION  *********************/
int LauncherInternal::getWorldSize ()
{
	return client->getWorldSize();
}

/*******************  FUNCTION  *********************/
void LauncherInternal::barrier ()
{
	client->barrier();
}

/*******************  FUNCTION  *********************/
void LauncherInternal::commit ()
{
	
}

/*******************  FUNCTION  *********************/
std::string LauncherInternal::get ( const std::string& key, int rank )
{
	return client->get(key,rank);
}

/*******************  FUNCTION  *********************/
void LauncherInternal::set ( const std::string& key, const std::string& value )
{
	client->set(key,value);
}

}
