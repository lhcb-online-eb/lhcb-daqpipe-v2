/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_LAUNCHER_FAKE_HPP
#define DAQ_LAUNCHER_FAKE_HPP

/********************  HEADERS  *********************/
#include "Launcher.hpp"

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*********************  CLASS  **********************/
/**
 * Dummy implementation of the launcher to work localy in a uniq process.
**/
class LauncherFake : public Launcher
{
	public:
		LauncherFake(Config * config,int nodes);
		virtual ~LauncherFake(void);
		virtual void initialize ( int argc, char ** argv );
		virtual void finalize ( void );
		virtual int getWorldSize(void);
		virtual int getRank(void);
		virtual void barrier ( void );
		virtual void commit ( void );
		virtual std::string get ( const std::string& key, int rank );
		virtual void set ( const std::string& key, const std::string& value );
	private:
		int nodes;
		int rank;
};

}

#endif //DAQ_LAUNCHER_FAKE_HPP
