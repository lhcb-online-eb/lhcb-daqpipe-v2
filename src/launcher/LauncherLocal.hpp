/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_LAUNCHER_DUMMY_HPP
#define DAQ_LAUNCHER_DUMMY_HPP

/********************  HEADERS  *********************/
#include "Launcher.hpp"

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*********************  CLASS  **********************/
/**
 * Dummy implementation of the launcher to work localy in a uniq process.
**/
class LauncherLocal : public Launcher
{
	public:
		LauncherLocal(Config * config);
		virtual void initialize ( int argc, char ** argv );
		virtual void finalize ();
		virtual int getWorldSize();
		virtual int getRank();
		virtual void barrier ();
		virtual void commit ();
		virtual std::string get ( const std::string& key, int rank );
		virtual void set ( const std::string& key, const std::string& value );
};

}

#endif //DAQ_LAUNCHER_DUMMY_HPP