/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  INFO   ***********************/
/**
 * Restructuration of the original work of 
 * Daniel Campora / dcampora@cern.ch
**/

/********************  HEADERS  *********************/
#include "EventManager.hpp"
#include "common/Debug.hpp"

#ifdef HAVE_HTOPML
	#include <iostream>
#endif

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*******************  FUNCTION  *********************/
/**
 * Constructor of the event manager.
 * @param transport Provide the transport layer to be used for command sends.
 * @param config Provide access to the global config object.
**/
EventManager::EventManager ( DAQ::Transport* transport, const DAQ::Config* config ) 
             :Unit ( "EventManager" )
             ,waitAllBuForExit(transport,config,config->builderCredits,UNIT_BUILDER_UNIT)
{
	this->transport = transport;
	this->eventId = DAQ_FIRST_EVENT_ID;
	this->unitsReady = 0;
	this->runTimeout = config->runTimeout;
	this->builderCredits = config->builderCredits;
	this->mode = config->mode;
	this->commandChannel.setUniqReader();
	this->balanceDelay = config->balanceDelay; 
	this->eventsPerRank = new uint64_t [transport->countUnits(UNIT_BUILDER_UNIT)];
	memset(this->eventsPerRank,0,transport->countUnits(UNIT_BUILDER_UNIT) * sizeof(uint64_t));
	
	UnitCommand command;
	//ArgCreditBalance * eventsPerRank = new ArgCreditBalance[transport->countUnits(UNIT_BUILDER_UNIT)]; 

}

/*******************  FUNCTION  *********************/
/**
 * Destructor of the event manager. Used to free memory.
**/
EventManager::~EventManager ()
{
	delete [] eventsPerRank;
}

/*******************  FUNCTION  *********************/
/**
 * Main loop of event manager handling commands received from transport.
**/
void EventManager::main ()
{
	UnitCommand command;
	while (command << commandChannel) {
		DAQ_DEBUG_ARG("em","Get command %1").arg(command.type).end();
		switch(command.type) {
			////////////////////////// INIT STUFF /////////////////////
			case UNIT_CMD_START:
				DAQ_DEBUG("em","Get start command");
				timeoutChecker.start();
				balanceProfileTimer.start();  
				break;
			case UNIT_CMD_EXIT:
				DAQ_DEBUG("em","Get exit");
				transport->setExit();
				break;
			case UNIT_CMD_INIT_UNIT_READY:
				DAQ_DEBUG("em","Get unit ready");
				unitsReady++;
				if (unitsReady == transport->countUnits(UNIT_READOUT_UNIT) + transport->countUnits(UNIT_BUILDER_UNIT))
					initialSendAllCredit();
				break;
			//////////////////////////  CREDIT TRACKING ///////////////
			case UNIT_CMD_REG_CREDIT:
				
				eventsPerRank[transport->getGblIdOfUnit(command.src.rank,command.src.id)] ++; 
				
				if (balanceProfileTimer.getElapsedTime() > balanceDelay)
				{
					//notify balance every balanceDelay
					notifyBalancing(command.src);
					balanceProfileTimer.stop();
					balanceProfileTimer.start();
				}
				
				DAQ_DEBUG_ARG("em","Get credit from : %1").arg(command.src).end();
				if ((runTimeout == 0 || runTimeout > timeoutChecker.getElapsedTime()) && mode != MODE_PULL_NO_EM) {
					if (transport->isDead(command.src.rank) == false)
						sendCredit(command.src,eventId++,command.args.regCredit.creditId, command.args.regCredit.storageId);
				} else {
					DAQ_DEBUG_ARG("em","EM retain credit for %1 to prepeare exit").arg(command.src).end();
					//wait to get all credits in wait to send exit to everybody
					waitAllBuForExit.set(command.src,command.args.regCredit.creditId,true);
					if (waitAllBuForExit.hasAllRessources())
						this->sendExitToAll();
				}
				break;

			///////////////////////////// ERRORS ///////////////////////
			case UNIT_CMD_NODE_FAILED:
				//mark them for exit
				for (int i = 0 ; i < builderCredits ; i++)
					for (int j = 0 ;j < DAQ_MAX_UNITS_PER_PROCESS ; j++)
						if (transport->getUnitType(command.args.nodeFailed.rank,j) == UNIT_BUILDER_UNIT)
							waitAllBuForExit.set(UnitId(command.args.nodeFailed.rank,j),i,true);
				break;
			default:
				DAQ_FATAL_ARG("Invalid command on EventManager : %1").arg(command.type).end();
				break;
		}
	}
}

/*******************  FUNCTION  *********************/
/**
 * When all nodes are ready this can be used to send exit command to every nodes.
 * It then send the exit command to itself. The exit command is send to unit 0 of
 * each rank. This make a requirement to have a unit at this location. In last
 * ressort one can assign a dummy unit at this place to handle exit.
**/
void EventManager::sendExitToAll ()
{
	//setup
	UnitCommand command;
	command.type = UNIT_CMD_EXIT;
	
	//print
	DAQ_INFO("EM Send command to all units");
	
	//send to all except up
	for (int rank = 0 ; rank < transport->getWorldSize() ; rank++)
		if (rank != transport->getRank())
			transport->sendCommand(this,rank,0,command);
	
	//send to us
	transport->sendCommand(this,transport->getRank(),0,command);
}
/*******************  FUNCTION  *********************/
/**
 * Send a notifications to the profile unit to provide the balancing of events
 * over builder units. Caution it send all values one by one which might
 * consume network, try to make this rare enougth by using a time not too short
 * between two sends.
**/
void EventManager::notifyBalancing (UnitId & src)
{
	#ifdef HAVE_HTOPML
		UnitCommand command;
		command.type = UNIT_CMD_BALANCE_TRACKING;
		for(int i = 0; i< transport->countUnits(UNIT_BUILDER_UNIT);i++)
		{
			command.args.creditBalance.id = i;
			command.args.creditBalance.value = this->eventsPerRank[i];
			this->eventsPerRank[i] = 0;
			transport->sendCommand(this,UNIT_PROFILE_UNIT,command);
		}
	#endif //HAVE_HTOPML
}
/*******************  FUNCTION  *********************/
/**
 * Send all the initial credits to filter units. This is used on startup
 * to boostrap the communications.
**/
void EventManager::initialSendAllCredit ()
{
	if (this->mode == MODE_PULL_NO_EM) {
		sendPullNoEMStart();
	} else {
		//loop on all CREDIT times to made dispatch as round robin
		for (int creditId = 0 ; creditId < builderCredits ; creditId++)
			for (int unitId = 0 ; unitId < DAQ_MAX_UNITS_PER_PROCESS ; unitId++)
				for (int rank = 0 ; rank < transport->getWorldSize() ; rank++)
					if (transport->getUnitType(rank,unitId) == UNIT_BUILDER_UNIT)
						sendCredit(buildUnitId(rank,unitId),eventId++,creditId,creditId);
	}
}

/*******************  FUNCTION  *********************/
/**
 * Use for PULL NO EM protocol to make all the unit starting data transfers.
**/
void EventManager::sendPullNoEMStart ()
{
	//setup command
	UnitCommand command;
	command.type = UNIT_CMD_PULL_NO_EM_START;
	
	//send to all BU
	transport->sendCommand(this,UNIT_BUILDER_UNIT,command);
}

/*******************  FUNCTION  *********************/
/**
 * Send a credit affecting a given event to the given builder unit.
 * This function handle the PULL mode where the credit is sent to the builder unit
 * and PUSH mode where is send to all readout units.
 * @todo On PUSH mode, we need to avail making a all in one go but do balancing in place.
**/
void EventManager::sendCredit ( const UnitId & unitId, long eventId, int creditId, int storageId )
{
	//setup command
	UnitCommand command;
	command.type = UNIT_CMD_PULL_PUBLISH_CREDIT;
	ArgCreditAffection & args = command.args.creditAffect;
	args.eventId = eventId;
	args.buRank = unitId.rank;
	args.buUnitId = unitId.id;
	args.creditId = creditId;
	args.storageId = storageId;
	
	//send to BU or RU depending on the mode
	if (mode == MODE_PUSH) {
		DAQ_DEBUG_ARG("em","Send eventId to BU : %1").arg(unitId).end();
		command.type = UNIT_CMD_PUSH_REG_EVENT_ID;
		transport->sendCommand(this,unitId,command);
		//send to all readout units
		DAQ_DEBUG_ARG("em","Send credit to RU (target = %1)").arg(unitId).end();
		command.type = UNIT_CMD_PULL_PUBLISH_CREDIT;
		transport->sendCommand(this,UNIT_READOUT_UNIT,command);
	} else if (mode == MODE_PULL) {
		//send to BU
		DAQ_DEBUG_ARG("em","Send credit to BU %1 for eventId %2").arg(unitId).arg(eventId).end();
		transport->sendCommand(this,unitId,command);
	} else {
		assert(false);
	}
}

}
