/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  INFO   ***********************/
/**
 * Restructuration of the original work of 
 * Daniel Campora / dcampora@cern.ch
**/

#ifndef DAQ_BUILDER_UNIT_DATA_HPP
#define DAQ_BUILDER_UNIT_DATA_HPP

/********************  HEADERS  *********************/
#include <cstdlib>
#include "../transport/Transport.hpp"
#include "../datasource/DataSource.hpp"

/********************  MACROS  **********************/
#define DAQ_SKIP_ID 0

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*********************  STRUCT  *********************/
/**
 * Structure to organize the data storage in the builder unit.
**/
class BuilderUnitData
{
	public:
		BuilderUnitData( int eventSize, int credits, int storage, int units, DAQ::Transport* transport, const std::string& name = "unknown" );
		~BuilderUnitData();
		char * getCreditPtr(int creditId, int rail,int ruUnitId);
		char * getIdPtr(int id, int rail,int ruUnitId);
		char * getIdPtrNoCheck(int id, int rail,int ruUnitId);
		bool assign(int creditId);
		void assignSkip(int creditId);
		void unassign( int creditId, bool free = false);
		void free(int id);
		void setSameSegment(bool value);
		size_t getEventSize();
		int getCreditId(int creditId);
		int getFillPercentage(void);
		void enableSkip(void);
	private:
		/** Keep track of an event size **/
		size_t eventSize;
		/** Keep track of the total size of a credit & unit data **/
		size_t totalSize;
		/** Store pointers of the data segments **/
		#ifdef DAQ_DRIVER_RAPIDIO_MULTISEGMENT
			//TODO define and explain this magic number!
			char *data[64][DAQ_MAX_CREDIT][];
			#error TODO
		#else
			char ** data[DAQ_DATA_SOURCE_RAILS];
		#endif // DAQ_DRIVER_RAPIDIO_MULTISEGMENT
		/** Keep track of reserved segments **/
		bool * reserved;
		/** Keep track of mapping between credits and stored events **/
		int creditMapping[DAQ_MAX_CREDIT];
		/** Keep track of unit count **/
		int units;
		/** Keep track of number of stored fragments **/
		int stored;
		/** Keep track of credit count **/
		int credits;
		/** Enable usage of a uniq segment for testing (not for production) **/
		bool sameSegmentMode;
		/** Keep handler of transport layer **/
		Transport * transport;
		/** Enable one storage for data skiping (used by BU) **/
		bool skip;
};

}

#endif //DAQ_BUILDER_UNIT_DATA_HPP
