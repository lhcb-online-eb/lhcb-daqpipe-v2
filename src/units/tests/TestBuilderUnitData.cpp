/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include <cerrno>
#include <gtest/gtest.h>
#include "../Command.hpp"
#include "../BuilderUnitData.hpp"
#include "transport/tests/MockTransport.hpp"
#include "launcher/LauncherLocal.hpp"
#include "drivers/tests/MockDriver.hpp"

/***************** USING NAMESPACE ******************/
using namespace DAQ;
using namespace testing;

/*******************  FUNCTION  *********************/
TEST(BuilderUnitData,constructor)
{
	Config config;
	LauncherLocal launcher(&config);
	MockDriver driver;
	MockTransport transport("MockTransport",&config,&launcher,&driver);
	
	EXPECT_CALL(driver,registerSegment(_,2*1024*1024)).Times(8);
	EXPECT_CALL(driver,unregisterSegment(_,2*1024*1024)).Times(8);
	
	BuilderUnitData data( 1024*1024, 2, 4, 2, &transport );
}

/*******************  FUNCTION  *********************/
TEST(BuilderUnitData,assign)
{
	Config config;
	LauncherLocal launcher(&config);
	MockDriver driver;
	MockTransport transport("MockTransport",&config,&launcher,&driver);
	
	EXPECT_CALL(driver,registerSegment(_,2*1024*1024)).Times(8);
	EXPECT_CALL(driver,unregisterSegment(_,2*1024*1024)).Times(8);
	
	BuilderUnitData data( 1024*1024, 2, 4, 2, &transport );
	
	for (int i = 0 ; i < 2 ; i++) {
		EXPECT_TRUE(data.assign(i));
		EXPECT_EQ(i+1,data.getCreditId(i));
	}
}

/*******************  FUNCTION  *********************/
TEST(BuilderUnitData,unassign)
{
	Config config;
	LauncherLocal launcher(&config);
	MockDriver driver;
	MockTransport transport("MockTransport",&config,&launcher,&driver);
	
	EXPECT_CALL(driver,registerSegment(_,2*1024*1024)).Times(8);
	EXPECT_CALL(driver,unregisterSegment(_,2*1024*1024)).Times(8);
	
	BuilderUnitData data( 1024*1024, 2, 4, 2, &transport );
	
	EXPECT_TRUE(data.assign(0));
	EXPECT_EQ(1,data.getCreditId(0));
	data.unassign(0);
	
	EXPECT_TRUE(data.assign(0));
	EXPECT_EQ(2,data.getCreditId(0));
}

/*******************  FUNCTION  *********************/
TEST(BuilderUnitData,free)
{
	Config config;
	LauncherLocal launcher(&config);
	MockDriver driver;
	MockTransport transport("MockTransport",&config,&launcher,&driver);
	
	EXPECT_CALL(driver,registerSegment(_,2*1024*1024)).Times(8);
	EXPECT_CALL(driver,unregisterSegment(_,2*1024*1024)).Times(8);
	
	BuilderUnitData data( 1024*1024, 2, 4, 2, &transport );
	
	EXPECT_TRUE(data.assign(0));
	EXPECT_EQ(1,data.getCreditId(0));
	data.unassign(0);
	data.free(0);
	
	EXPECT_TRUE(data.assign(0));
	EXPECT_EQ(DAQ_SKIP_ID,data.getCreditId(0));
}

