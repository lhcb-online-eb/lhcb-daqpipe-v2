/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_MOCK_UNIT_HPP
#define DAQ_MOCK_UNIT_HPP

/********************  HEADERS  *********************/
#include <gmock/gmock.h>
#include "../Unit.hpp"

/********************  NAMESPACE  *******************/
namespace DAQ {

class MockUnit : public Unit {
	public:
		MockUnit() : Unit("mock"){};
		MOCK_METHOD1(pushCommand,
			void(const UnitCommand & command));
		MOCK_METHOD0(startThread,
			void());
		MOCK_METHOD0(stop,
			void());
		MOCK_METHOD0(main,
			void());
		MOCK_METHOD0(startNoThreads,
			void());
};

}  // namespace DAQ

#endif //DAQ_MOCK_UNIT_HPP
