/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include <cerrno>
#include <gtest/gtest.h>
#include "../FilterSendState.hpp"

/***************** USING NAMESPACE ******************/
using namespace DAQ;


/*******************  FUNCTION  *********************/
TEST(FilterSendState,constructor)
{
	FilterSendState state(10);
}

/*******************  FUNCTION  *********************/
TEST(FilterSendState,start)
{
	FilterSendState state(10);
	state.start();
	EXPECT_FALSE(state.isFinished());
}

/*******************  FUNCTION  *********************/
TEST(FilterSendState,partial)
{
	FilterSendState state(10);
	state.start();
	
	for (int unit = 0  ; unit < 9 ; unit++) {
		for (int rail = 0 ; rail < DAQ_DATA_SOURCE_RAILS ; rail++) {
			state.step(rail,unit,TARGET_DATA);
			state.step(rail,unit,TARGET_META);
		}
	}
	
	EXPECT_FALSE(state.isFinished());
}

/*******************  FUNCTION  *********************/
TEST(FilterSendState,finished)
{
	FilterSendState state(10);
	state.start();
	
	for (int unit = 0  ; unit < 10 ; unit++) {
		for (int rail = 0 ; rail < DAQ_DATA_SOURCE_RAILS ; rail++) {
			state.step(rail,unit,TARGET_DATA);
			state.step(rail,unit,TARGET_META);
		}
	}
	
	EXPECT_TRUE(state.isFinished());
}