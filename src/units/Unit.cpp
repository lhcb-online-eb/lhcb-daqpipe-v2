/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include "common/Debug.hpp"
#include "Unit.hpp"

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*******************  FUNCTION  *********************/
/**
 * Unit constructor.
 * @param name Name of the unit (eg. ReadoutUnit)
**/
Unit::Unit ( const std::string & name )
     :name(name)
{
	this->hasNoThread = false;
}

/*******************  FUNCTION  *********************/
/**
 * Function called by the transport layer when using non threaded units.
 * It mainly call the main (virtual) function of the unit.
**/
void Unit::updateNoThread ()
{
	assert(this->hasNoThread);
	this->main();
}

/*******************  FUNCTION  *********************/
/**
 * Return then unit name.
**/
const std::string& Unit::getName () const
{
	return this->name;
}

/*******************  FUNCTION  *********************/
/**
 * Called from the transport layer when a new command is recieved for the unit.
 * It push it to the GoChannel.
**/
void Unit::pushCommand ( const UnitCommand& command )
{
	DAQ_DEBUG_ARG("unit","Push command %1").arg(command.type).end();
	commandChannel << command;
}

/*******************  FUNCTION  *********************/
/**
 * Function called to start the unit when not using threaded mode.
 * (To be inherited)
**/
void Unit::startNoThreads ()
{
	this->commandChannel.disableWaitOnEmpty();
	this->hasNoThread = true;
}

/*******************  FUNCTION  *********************/
/**
 * Function called to start the unit when using threaded mode.
 * It mainly start the unit thread.
**/
void Unit::startThread()
{
	this->thread = std::thread([this](){this->main();});
}

/*******************  FUNCTION  *********************/
/**
 * Stop the unit. Mainly used to close the channel and wait the thread to exit.
**/
void Unit::stop()
{
	if (this->hasNoThread == false) {
		this->commandChannel.close();
		this->thread.join();
	}
}

}
