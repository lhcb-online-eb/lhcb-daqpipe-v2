/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_FILTER_MANAGER_HPP
#define DAQ_FILTER_MANAGER_HPP

/********************  HEADERS  *********************/
#include "Unit.hpp"
#include "common/TopoRessource.hpp"
#include <list>

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/********************  STRUCT  **********************/
/**
 * Keep track of the available slots in the filter units or builder unit to assign a job.
 * There will be a filter manager for every filter unit sub-farm.
**/
struct FilterUnitSlot
{
	/** Unit which is ready to get task **/
	UnitId unitId;
	/** Availabel cerdit id in the unit **/
	int creditId;
	/** Event ID to assign **/
	long eventId;
};

/*********************  TYPES  **********************/
/** List of slots for then pending list **/
typedef std::list<FilterUnitSlot> FilterMangerPeningList;
/** List of slots for the available slots **/
typedef std::list<FilterUnitSlot> FilterUnitSlotList;
	
/*********************  CLASS  **********************/
class FilterManager : public Unit
{
	public:
		FilterManager(Transport * transport,const Config * config);
		virtual ~FilterManager() = default;
		void main();
	private:
		void sendRequestToFilter();
		void removeFailed(UnitCommand & command);
	private:
		/** Keep track of the transport to send commands **/
		Transport * transport;
		/** List of slots available in the filter unit side to send the data to **/
		FilterUnitSlotList slots;
		/** List of data slots available for transfert in the builder unit side. **/
		FilterMangerPeningList pending;
};

}

#endif //DAQ_FILTER_MANAGER_HPP
