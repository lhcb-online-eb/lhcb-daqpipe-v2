/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_UNIT_HPP
#define DAQ_UNIT_HPP

/********************  HEADERS  *********************/
#include "Command.hpp"
#include <gochannels/GoChannel.hpp>
#include <thread>

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*********************  CLASS  **********************/
/**
 * Base claass to implement a unit. A unit mainly handle a state and react to
 * input commands comming from the transport layer.
**/
class Unit
{
	public:
		Unit(const std::string & name);
		virtual ~Unit() = default;
		virtual void pushCommand(const UnitCommand & command);
		const std::string & getName() const;
		virtual void startThread();
		virtual void stop();
		virtual void main() = 0;
		virtual void startNoThreads();
		void updateNoThread();
	private:
		//copy is forbidden
		Unit(const Unit & unit);
	protected:
		/** Name of the unit **/
		std::string name;
		/** 
		 * Go channel taking the input command to run them. This permit to 
		 * possibly run each unit in their own thread. But from testing it looks
		 * better to keep the unit main funciton called in the main thread.
		**/
		GoChannel<UnitCommand> commandChannel;
		/**
		 * Handler of the thread if used.
		**/
		std::thread thread;
		/** Keep track of thread usage **/
		bool hasNoThread;
};

}

#endif //NODE_HPP
