/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include <unistd.h>
#include "FilterUnitTCP.hpp"

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*******************  FUNCTION  *********************/
/**
 * Constructor of the filter unit.
 * @param transport Define the transport to be used to send and receive data.
 * @param config Define the global config object to be used.
**/
FilterUnitTCP::FilterUnitTCP ( Transport* transport, const Config* config )
	:Unit("FilterUnitTCP")
{
	//info
	DAQ_DEBUG("fu-tcp","Unit ready");
	
	this->config = config;
	this->transport = transport;
	this->commandChannel.setUniqReader();
	//TODO change
	credits = config->filterCredits;
	assumeArg(credits < DAQ_MAX_CREDIT,"Invalid credit number, limited to %1").arg(DAQ_MAX_CREDIT).end();
}

/*******************  FUNCTION  *********************/
FilterUnitTCP::~FilterUnitTCP()
{
	delete fu;
}

/*******************  FUNCTION  *********************/
void FilterUnitTCP::setupFilterManagerAndThreads ( void )
{
	//find the related manager
	UnitId cur = UnitId(transport->getRank(), transport->getUnitId(this));
	bool found = false;
	int fuId = -1;//-1 as we cound the local one at start time
	while (cur.rank >= 0 && !found)
	{
		DAQ_INFO_ARG("%1").arg(cur).end();
		//check
		if (transport->getUnitType(cur.rank,cur.id) == UNIT_FILTER_MANAGER)
		{
			manager = cur;
			found = true;
			break;
		} else if (transport->getUnitType(cur.rank,cur.id) == DAQ::UNIT_FILTER_UNIT) {
			fuId++;
		}
		
		//move
		cur.id--;
		if (cur.id < 0)
		{
			cur.id = DAQ_MAX_UNITS_PER_PROCESS - 1;
			cur.rank--;
		}
	}
	assume(found,"Fail to found the related FilterManager, check your topology !");
	
	DAQ_DEBUG_ARG("fu-tcp","Current fuid : %1").arg(fuId).end();
	
	//setup connector
	this->fu = new DAQFU::FuBuSide(config,transport->countUnits(DAQ::UNIT_READOUT_UNIT),false);
	this->fu->startLocalThread(fuId);
}

/*******************  FUNCTION  *********************/
/**
 * Main loop of the unit to handle the incomming commands. It need to loop
**/
void FilterUnitTCP::main()
{
	UnitCommand command;
	while(command << commandChannel) {
		switch(command.type) {
			//////////////////////// INIT STUFF ////////////////////////
			case UNIT_CMD_START:
				this->setupFilterManagerAndThreads();
				for (int i = 0 ; i < credits ; i++)
					this->registerCreditToManager(i);
				break;
			case UNIT_CMD_NODE_FAILED:
				//TODO
				DAQ_WARNING("Node failure not handled !");
				break;
			case UNIT_CMD_FILTER_FETCH_REQUEST:
				DAQ_DEBUG_ARG("fu","Get fetch request on %6 from FilterManager (%1) for BuilderUnit (%2:%3:%4), eventId %5 on credit %6")
					.arg(command.src)
					.arg(command.args.filterRequest.buRank)
					.arg(command.args.filterRequest.buUnitId)
					.arg(command.args.filterRequest.buStorageId)
					.arg(command.args.filterRequest.fuCreditId)
					.arg(command.args.filterRequest.eventId)
					.arg(command.dest)
					.end();
				assert(command.args.filterRequest.fuCreditId < credits);
				sendData(command);
				break;
			//////////////////////////// END ///////////////////////////
			default:
				DAQ_FATAL_ARG("Get unexpected command in FilterUnitTCP : %1").arg(command.type).end();
				break;
		}
	}
}

/*******************  FUNCTION  *********************/
void FilterUnitTCP::sendData ( UnitCommand command )
{
	//check
	assume(command.args.filterRequest.buRank == transport->getRank(),"Invalid rank, should be same than current !");
	assume(transport->getUnitType(command.args.filterRequest.buRank,command.args.filterRequest.buUnitId) == UNIT_BUILDER_UNIT,"Invalid unit type !");
	
	//get data handler
	BuilderUnit * bu = dynamic_cast<BuilderUnit*>(transport->getLocalUnit(command.args.filterRequest.buUnitId));
	assert(bu != NULL);
	BuilderUnitData & meta = bu->getData(true);
	BuilderUnitData & data = bu->getData(false);
	
	//extract 
	int credit = command.args.filterRequest.fuCreditId;
	size_t eventId = command.args.filterRequest.eventId;
	int storageId = command.args.filterRequest.buStorageId;
	
	//setup handler
	size_t messages = 2 * transport->countUnits(DAQ::UNIT_READOUT_UNIT) * DAQ_DATA_SOURCE_RAILS;
	fu->setCallback(credit,messages,[this,command,credit](){
		UnitCommand cmd;
		cmd.type = UNIT_CMD_FILTER_FREE_BU_STORAGE;
		cmd.args = command.args;
		DAQ_DEBUG_ARG("fu-tcp","Send ack to %1").arg(UnitId(command.args.filterRequest.buRank,command.args.filterRequest.buUnitId)).end();
		transport->sendCommand(this,command.args.filterRequest.buRank,command.args.filterRequest.buUnitId,cmd);
		//meta.free(storageId);
		//data.free(storageId);
		//request new
		DAQ_DEBUG("fu-tcp","Event fetch finished");
		registerCreditToManager(credit);
	});
	
	//push data
	int units = transport->countUnits(DAQ::UNIT_READOUT_UNIT);
	for (int unit = 0 ; unit < units ; unit++) {
		for (int rail = 0 ; rail < DAQ_DATA_SOURCE_RAILS ; rail++) {
			fu->sendData(credit,eventId,true,unit,rail,meta.getIdPtr(storageId,rail,unit),meta.getEventSize());
			fu->sendData(credit,eventId,false,unit,rail,data.getIdPtr(storageId,rail,unit),data.getEventSize());
		}
	}
}

/*******************  FUNCTION  *********************/
/**
 * When a credit has been finished for transfert, we register it again
 * in the filter manager for later use.
 * @param creditId ID of the credit to register to the filter manager.
**/
void FilterUnitTCP::registerCreditToManager ( int creditId )
{
	//prepare command
	UnitCommand command;
	command.type = UNIT_CMD_FILTER_REGISTER_CREDIT;
	command.args.filterReg.creditId = creditId;

	//reg
	transport->sendCommand(this,manager,command);
}

}
