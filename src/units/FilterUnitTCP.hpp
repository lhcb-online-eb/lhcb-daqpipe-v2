/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_FILTER_UNIT_TCP_HPP
#define DAQ_FILTER_UNIT_TCP_HPP

/********************  HEADERS  *********************/
#include "Unit.hpp"
#include "BuilderUnit.hpp"
#include <../tools/filter-units/BuSide.hpp>

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*********************  CLASS  **********************/
/**
 * The filter unit is the endpoint for the data. It receive the full events
 * from the builder unit, still represented as fragments, then it convert them
 * to finally flushed the data into files. The unit is mainly drived by the closer
 * filter manager.
**/
class FilterUnitTCP : public Unit
{
	public:
		FilterUnitTCP(Transport * transport,const Config * config);
		virtual ~FilterUnitTCP();
		void main();
	protected:
		void setupFilterManagerAndThreads(void);
		void registerCreditToManager(int creditId);
		void sendData(UnitCommand command);
	private:
		/** Keep track of the transport layer to send requets and ack to the filter manager. **/
		Transport * transport;
		/** Keep track of config **/
		const Config * config;
		/** Define how many event to fetch at the same time just like the builder unit credits **/
		int credits;
		/** keep track of the ID of the filter manager of this unit. **/
		UnitId manager;
		/** Connector to filter unit **/
		DAQFU::FuBuSide * fu;
		//mutex to protect fu
		std::mutex mutex;
};

}

#endif //DAQ_FILTER_UNIT_TCP_HPP
