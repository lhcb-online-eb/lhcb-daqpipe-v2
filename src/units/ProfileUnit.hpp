/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_PROFILE_UNIT_HPP
#define DAQ_PROFILE_UNIT_HPP

/********************  HEADERS  *********************/
#include "Unit.hpp"
#include "common/TopoRessource.hpp"

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*********************  CLASS  **********************/
/**
 * The profiling unit permit to aggregate some distributed parameters in one process
 * to be rendered by htopml. For better efficiecy this unit need to be
 * placed with a readout or builder unit to be able to print the communications
 * scheduling at the same time than other datas. But this is not a strong requirement.
**/
class ProfileUnit : public Unit
{
	public:
		ProfileUnit(Transport * transport,const Config * config);
		virtual ~ProfileUnit() = default;
		void main();
		#ifdef HAVE_HTOPML
			friend void convertToJson(htopml::JsonState & json,const ProfileUnit & value);
		#endif //HAVE_HTOPML
		void printTotBandwidth(void);
	private:
		/**	Keep track of the bandwidth of each units **/
		TopoRessource<float> bandwidth;
		/** Keep track of the transport layer **/
		Transport * transport;
		/** Load balancing array to store number of event managed by each builder unit **/
		uint64_t * balancingArray;
		/** Keep track of thebuffer filling of each readout units **/
		uint64_t * ruFilledPercentageArray;
		/** Keep track of thebuffer filling of each readout units **/
		uint64_t * buFilledPercentageArray;
		/** BU dropped events **/
		uint64_t * buDroppedEvents;
		/** total dropped **/
		uint64_t totalDropped;
		/** counter to know when to print **/
		unsigned int cntRecv;
		/** number of readout unit **/
		unsigned int cntRU;
};

}

#endif //DAQ_PROFILE_UNIT_HPP
