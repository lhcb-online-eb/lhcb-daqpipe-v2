/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  INFO   ***********************/
/**
 * Restructuration of the original work of 
 * Daniel Campora / dcampora@cern.ch
**/

#ifndef DAQ_FILTER_SEND_STATE_HPP
#define DAQ_FILTER_SEND_STATE_HPP

/********************  HEADERS  *********************/
#include "transport/Transport.hpp"
#include "Unit.hpp"

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*********************  CLASS  **********************/
/**
 * To be used into the builder unit to track the progression of data sending
 * to the filter units. Caution, it only track the state and do not do the
 * send itself, the work is dont by the parent unit (BuilderUnit).
**/
class FilterSendState
{
	public:
		FilterSendState( int readoutUnitNb);
		~FilterSendState();
		void start();
		void step(int rail,int unit,GatherTarget dataType);
		bool isFinished();
	private:
		/** Array to keep track of which data has been sent **/
		bool * data[DAQ_DATA_SOURCE_RAILS];
		/** Array to keep track of which meta data has been sent **/
		bool * meta[DAQ_DATA_SOURCE_RAILS];
		/** To know if state is finished or not **/
		bool finished;
		/** Keep track of readout unit number to know how many segments to send **/
		int readoutUnitNb;
		/** Keep track of the current number of segment sent to know when finish. **/
		int cnt;
};

}

#endif //DAQ_FILTER_SEND_STATE_HPP
