/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include <cassert>
#include "BuilderUnitData.hpp"
#include "common/Debug.hpp"

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*******************  FUNCTION  *********************/
/**
 * Constructor of the builder unit data storage component.
 * @param eventSize Define the size of a fragment (could be data of meta)
 * @param credits Define the number of credit to be stored.
 * @param units Define the number of readout unit to consider for the data aggregation.
 * @param transport Define the transport to be used.
 * @param storage Will store storeage * credits events.
**/
BuilderUnitData::BuilderUnitData ( int eventSize, int credits, int storage, int units, Transport * transport, const std::string & name )
{
	//errors
	assert(eventSize > 0);
	assert(storage >= credits);
	assert(credits < DAQ_MAX_CREDIT);
	
	//check
	assumeArg(credits <= DAQ_MAX_CREDIT,"Too much credit, max is %1, request is %2")
		.arg(DAQ_MAX_CREDIT)
		.arg(credits)
		.end();
	#ifdef DAQ_DRIVER_RAPIDIO_MULTISEGMENT
		// See BuilderUnit.hpp - magic number that needs to be explained
		assume(units <= 64, "Too many units, max is 64");
	#endif// DAQ_DRIVER_RAPIDIO_MULTISEGMENT

	//calc size
	this->units = units;

	#ifdef DAQ_DRIVER_RAPIDIO_MULTISEGMENT
		size_t perCreditSize = eventSize; //multisegment case
	#else
		size_t perCreditSize = eventSize * units; //normal case
	#endif //DAQ_DRIVER_RAPIDIO_MULTISEGMENT

	//setup
	this->eventSize = eventSize;
	this->totalSize = perCreditSize;
	this->credits = credits;
	this->sameSegmentMode = false;
	this->transport = transport;
	this->stored = storage;
	this->skip = false;
	
	//reserved slots
	this->reserved = new bool[stored];
	for (int i = 0 ; i < stored ; i++)
		this->reserved[i] = false;
	this->reserved[0] = true;

	//credit mapping
	for (int i = 0 ; i < credits ; i++)
		this->creditMapping[i] = -1;
	
	//init pointers
	#ifdef DAQ_DRIVER_RAPIDIO_MULTISEGMENT
		for(int u = 0; u < this->units; u++)
			for (int c = 0 ; c < credits ; c++)
				for (int rail = 0 ; rail < DAQ_DATA_SOURCE_RAILS ; rail++)
					data[u][c][rail] = new (transport->allocateRdmaSegment(perCreditSize,true)) char[perCreditSize];
	#else
		for (int rail = 0 ; rail < DAQ_DATA_SOURCE_RAILS ; rail++) {
			data[rail] =  new char*[stored];
			for (int s = 0 ; s < stored ; s++) {
				data[rail][s] = new (transport->allocateRdmaSegment(perCreditSize,true)) char[perCreditSize];
				transport->registerSegment(data[rail][s],perCreditSize);
			}
		}
	#endif // DAQ_DRIVER_RAPIDIO_MULTISEGMENT
	
	//print memory usage
	size_t mem = perCreditSize * stored * DAQ_DATA_SOURCE_RAILS;
	DAQ_DEBUG_ARG("mem","Using %1 of memory for %2").argUnit1024(mem).arg(name).end();
}	

/*******************  FUNCTION  *********************/
/**
 * Desructor of the storage, it mainly free the memory.
**/
BuilderUnitData::~BuilderUnitData ()
{
	delete [] this->reserved;

	#ifdef DAQ_DRIVER_RAPIDIO_MULTISEGMENT
		size_t perCreditSize = eventSize;
		for(int u = 0; u < this->units; u++) {
			for (int i = 0 ; i < credits ; i++) {
				for (int j = 0 ; j < DAQ_DATA_SOURCE_RAILS ; j++) {
					transport->unregisterSegment(data[u][i][j],perCreditSize);
					transport->deallocateRdmaSegment(data[u][i][j],perCreditSize,true);
				}
			}
		}
		
	#else
		size_t perCreditSize = eventSize * units;
		for (int r = 0 ; r < DAQ_DATA_SOURCE_RAILS ; r++) {
			for (int s = 0 ; s < stored ; s++) {
				transport->unregisterSegment(data[r][s],perCreditSize);
				transport->deallocateRdmaSegment(data[r][s],perCreditSize,true);
			}
			delete [] this->data[r];
		}
	#endif // DAQ_DRIVER_RAPIDIO_MULTISEGMENT
}

/*******************  FUNCTION  *********************/
/**
 * Get pointer for of the fragment.
 * @param id define the id we consider.
 * @param rail Define the rail to be used.
 * @param ruUnitId Define the readout unit ID to consider.
**/
char* BuilderUnitData::getIdPtr ( int id, int rail, int ruUnitId )
{
	//check
	assert(id < this->stored);
	assume(this->reserved[id],"Data ID is not reserved");
	
	//if same segment mode, shrinked on 0 IDs
	if (sameSegmentMode)
		rail = id = ruUnitId = 0;
	
	assert(ruUnitId >= 0 && ruUnitId < units);
	#ifdef DAQ_DRIVER_RAPIDIO_MULTISEGMENT
		return data[ruUnitId][creditId][rail];
	#else
		return data[rail][id] + ruUnitId * eventSize;
	#endif // DAQ_DRIVER_RAPIDIO_MULTISEGMENT
}

/*******************  FUNCTION  *********************/
/**
 * Get pointer for of the fragment.
 * @param id define the id we consider.
 * @param rail Define the rail to be used.
 * @param ruUnitId Define the readout unit ID to consider.
**/
char* BuilderUnitData::getIdPtrNoCheck ( int id, int rail, int ruUnitId )
{
	//check
	assert(id < this->stored);
	
	//if same segment mode, shrinked on 0 IDs
	if (sameSegmentMode)
		rail = id = ruUnitId = 0;
	
	assert(ruUnitId >= 0 && ruUnitId < units);
	#ifdef DAQ_DRIVER_RAPIDIO_MULTISEGMENT
		return data[ruUnitId][creditId][rail];
	#else
		return data[rail][id] + ruUnitId * eventSize;
	#endif // DAQ_DRIVER_RAPIDIO_MULTISEGMENT
}

/*******************  FUNCTION  *********************/
/**
 * Get pointer for of the fragment.
 * @param id define the id we consider.
 * @param rail Define the rail to be used.
 * @param ruUnitId Define the readout unit ID to consider.
**/
char* BuilderUnitData::getCreditPtr ( int creditId, int rail, int ruUnitId )
{
	//checks
	assert(creditId < DAQ_MAX_CREDIT);
	assume(creditMapping[creditId] != -1,"Credit not mapped");
	assume(this->reserved[creditMapping[creditId]],"Credit not reserved");
	
	//id
	int id = creditMapping[creditId];

	return this->getIdPtr(id,rail,ruUnitId);
}

/*******************  FUNCTION  *********************/
/**
 * Assign a storage location to credit.
 * @param creditId Define the credit for which to assign
 * @return True if assignement is done, false if not storage room is found.
**/
bool BuilderUnitData::assign(int creditId)
{
	//checks
	assert(creditId < DAQ_MAX_CREDIT);
	assert(creditMapping[creditId] == -1);
	
	//offset
	int startOffset = 0;
	if (skip)
		startOffset = 1;
	
	//search free, we skip i=0 which is used for backpressure
	int id = -1;
	for (int i = startOffset ; i < stored ; i++) {
		if (reserved[i] == false) {
			id = i;
			break;
		}
	}
	
	//mark reserved
	if (id == -1) {
		return false;
	} else {
		creditMapping[creditId] = id;
		reserved[id] = true;
		return true;
	}
}

/*******************  FUNCTION  *********************/
void BuilderUnitData::assignSkip(int creditId)
{
	creditMapping[creditId] = DAQ_SKIP_ID;
}

/*******************  FUNCTION  *********************/
/**
 * Free the storage location assigned to the give credit.
 * @param creditId Define the credit we want to unassign.
**/
void BuilderUnitData::unassign(int creditId, bool doFree)
{
	//checks
	assert(creditId < DAQ_MAX_CREDIT);
	assert(creditMapping[creditId] != -1);
	
	int id = creditMapping[creditId];
	creditMapping[creditId] = -1;
	
	if (doFree)
		this->free(id);
}

/*******************  FUNCTION  *********************/
void BuilderUnitData::free(int id)
{
	//trivia
	if (id == DAQ_SKIP_ID && skip)
		return;
	
	//checks
	assert(id < stored);
	assert(reserved[id]);
	
	reserved[id] = false;
}

/*******************  FUNCTION  *********************/
/**
 * Enable or disable same segment mode.
 * @param value The new state.
**/
void BuilderUnitData::setSameSegment(bool value)
{
	this->sameSegmentMode = value;
}

/*******************  FUNCTION  *********************/
/**
 * Return the size of an event.
**/
size_t BuilderUnitData::getEventSize ()
{
	return eventSize;
}

/*******************  FUNCTION  *********************/
int BuilderUnitData::getCreditId ( int creditId )
{
	//checks
	assert(creditId < DAQ_MAX_CREDIT);
	assert(creditMapping[creditId] != -1);
	assert(reserved[creditMapping[creditId]]);
	
	return creditMapping[creditId];
}

/*******************  FUNCTION  *********************/
int BuilderUnitData::getFillPercentage ( void )
{
	int cnt = 0;
	for (int i = 0 ; i < stored ; i++)
		if (reserved[i])
			cnt++;
	return (100*cnt)/stored;
}

/*******************  FUNCTION  *********************/
void BuilderUnitData::enableSkip ( void )
{
	this->skip = true;
}

}
