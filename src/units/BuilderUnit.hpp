/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  INFO   ***********************/
/**
 * Restructuration of the original work of 
 * Daniel Campora / dcampora@cern.ch
**/

#ifndef DAQ_BUILDER_UNIT_HPP
#define DAQ_BUILDER_UNIT_HPP

/********************  HEADERS  *********************/
#include "../transport/Transport.hpp"
#include "Unit.hpp"
#include "common/TopoRessource.hpp"
#include "common/Throughput.hpp"
#include "ReadoutUnit.hpp"
#include "gather/PullGather.hpp"
#include "gather/PushGather.hpp"
#include "FilterSendState.hpp"
#include "common/Histogram.hpp" 
#include "BuilderUnitData.hpp"

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*********************  CLASS  **********************/
/**
 * Implement a builder unit to aggregate the data coming from the readout unit.
 * It then send the data to the filter unit.
 * The builder unit handle credits to know how many event to fetch at the same time.
 * It receive the credit assignement from the filter manager and from the filter manager.
**/
class BuilderUnit : public Unit
{
	public:
		BuilderUnit( DAQ::Transport* transport, const DAQ::Config* config );
		virtual ~BuilderUnit();
		void main();
		BuilderUnitData & getData(bool meta);
	protected:
		void sendCreditToEM( int creditId, int storageId );
		void startGather(const UnitCommand & command);
		void progressGather(const UnitCommand & command);
		void sendLocalAddressesToRU();
		PullGather * buildPullGather( const DAQ::Config* config, int credit );
		void pullGatherFinished ( const DAQ::ArgGatherAck& args );
		void pullDataUsageFinished ( const DAQ::ArgGatherAck& args, bool doFree );
		void pushGatherFinished ( const DAQ::ArgGatherAck& args );
		void pushDataUsageFinished ( const DAQ::ArgGatherAck& args, bool doFree );
		void gatherStartNextPull( int creditId );
		void sendFilterRequest(const DAQ::ArgGatherAck& args);
		void setupFilterManager();
		void pushToFilter( const DAQ::UnitCommand& command );
		void filterPushFinised ( const DAQ::UnitCommand& command );
		void updateGatherTiming(int creditId);
		void assign(int creditId);
		void checkFillProfiling(void);
	private:
		/** Keep reack of the transport layer. **/
		Transport * transport;
		/** Store the remote addresses (readout unit) of data fragments **/
		TopoRessource<RemoteIOV> ruDataBuffers;
		/** Store the remote addresses (readout unit) of meta data fragments **/
		TopoRessource<RemoteIOV> ruMetaBuffers;
		/** Store the recived meta-data fragments **/
		BuilderUnitData meta;
		/** store the recived data fragments **/
		BuilderUnitData data;
		/** 
		 * Filter state permiting to track the transfert progression of fragments 
		 * for each credit when sending to the filter units.
		**/
		FilterSendState ** filterStates;
		/**
		 * Keep track of the state when transfering the fragments from the readout units.
		 * Get one per credit.
		**/
		Gather * gather[DAQ_MAX_CREDIT];
		/** Track transfert timings for each credit to detect timeout **/
		Timer timeForGather [DAQ_MAX_CREDIT]; 
		/** Timer for htopml buffer filling report **/
		Timer timeForFillReport;
		/** Build histograme of the time to complete a gather optation. **/
		Histogram histogramTimeGather; 
		/** Number of credits to be used **/
		int credits;
		/** Event data size **/
		size_t eventDataSize;
		/** Event meta data size **/
		size_t eventMetaSize;
		/** Type of gather operation in use **/
		std::string gatherType;
		/** Enable of disable usage of event manager **/
		bool useEM;
		/** Keep track of next event ID to use when running without event manager **/
		size_t nextEventId;
		/** Check timeout to exit when running without event manager**/
		Timer timeoutChecker;
		/** Delay febore exiting when runnign without event manager **/
		int runTimeout;
		/** Running mode (PULL, PUSH, PULL_NO_EM) **/
		Mode mode;
		/** Enable usage of filter units **/
		bool hasFilter;
		/** Keep location of the filter manager **/
		UnitId filterManager;
		/** To know when we finish to send to the filter unit for each credit **/
		int * filterStatus;
		/** Keep track of readout unit count **/
		int readoutUnits;
		/** Enable usage of filter farm **/
		bool hasFarm;
		/** used to skip the 100 first gather in histogram **/
		long numGathers; 
		/** Used to keep track of long gather to not put them in the histogram **/
		long sumGathers;
		/** Stored event in meta/data **/
		size_t stored;
		/** If use filter TCP **/
		bool filterTCP;
		/** Count dropped events **/
		size_t dropped;
};

}

#endif //DAQ_BUILDER_UNIT_HPP
