/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include "FilterSendState.hpp"
#include <cstring>

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*******************  FUNCTION  *********************/
/**
 * Constructor of the filter send state.
 * @param unitNb Numner of readout unit to know how many segments to send.
**/
FilterSendState::FilterSendState ( int unitNb )
{
	this->readoutUnitNb = unitNb;
	this->finished = true;
	this->cnt = -1;
	
	for (int i = 0 ; i < DAQ_DATA_SOURCE_RAILS ; i++)
	{
		this->data[i] = new bool[unitNb];
		this->meta[i] = new bool[unitNb];
	}
}

/*******************  FUNCTION  *********************/
/**
 * Destructor of the filter send state, free the memory.
**/
FilterSendState::~FilterSendState ()
{
	for (int i = 0 ; i < DAQ_DATA_SOURCE_RAILS ; i++)
	{
		delete [] this->data[i];
		delete [] this->meta[i];
	}
}

/*******************  FUNCTION  *********************/
/**
 * Start a new transmission by reseting the internal states.
**/
void FilterSendState::start ()
{
	assume(this->finished,"Invalid status try to start a FilterSendState which is running !");
	for (int i = 0 ; i < DAQ_DATA_SOURCE_RAILS ; i++)
	{
		memset(this->data[i],0,sizeof(bool) * readoutUnitNb);
		memset(this->meta[i],0,sizeof(bool) * readoutUnitNb);
	}
	this->finished = false;
	this->cnt = 0;
}

/*******************  FUNCTION  *********************/
/**
 * Check if is finished.
**/
bool FilterSendState::isFinished ()
{
	return finished;
}

/*******************  FUNCTION  *********************/
/**
 * Mark the given step as done.
 * @param rail Define the rail which has been sent.
 * @param unitId Define the unit id.
 * @param dataType Define if it is DATA or META_DATA.
**/
void FilterSendState::step ( int rail, int unitId, GatherTarget dataType )
{
	//errors
	assert(rail >= 0 && rail <= DAQ_DATA_SOURCE_RAILS);
	assert(unitId >= 0 && unitId < readoutUnitNb);

	//check
	bool * cur = nullptr;
	switch(dataType)
	{
		case TARGET_META:
			cur = &meta[rail][unitId];
			break;
		case TARGET_DATA:
			cur = &data[rail][unitId];
			break;
	}
	
	//check & update
	assert(*cur == false);
	*cur = true;
	
	//check finished
	assert(cnt < DAQ_DATA_SOURCE_RAILS * readoutUnitNb * 2);
	cnt++;
	if (cnt == DAQ_DATA_SOURCE_RAILS * readoutUnitNb * 2)
	{
		this->finished = true;
// 		UnitCommand command;
// 		command.type = UNIT_CMD_FILTER_FINISHED;
// 		command.args.filterFinish.id = creditId;
// 		parentUnit->pushCommand(command);
	}
}

}
