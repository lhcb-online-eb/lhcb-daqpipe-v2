/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  INFO   ***********************/
/**
 * Restructuration of the original work of 
 * Daniel Campora / dcampora@cern.ch
**/

#ifndef DAQ_READOUT_UNIT_HPP
#define DAQ_READOUT_UNIT_HPP

/********************  HEADERS  *********************/
#include "Unit.hpp"
#include "common/TopoRessource.hpp"
#include "common/Throughput.hpp"
#include <datasource/DummyDataSource.hpp>
#include <list>

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*********************  TYPES  **********************/
/**
 * Used to store the pending request commands if the requested event is not yet
 * available in the data sources.
**/
typedef std::list<UnitCommand> PendingCmdList;

/*********************  CLASS  **********************/
/**
 * Define a readout unit. The readout unit is responsible of reading the data
 * from the input card (DataSource) and to answer the BuilderUnit requests by
 * sending the requested events via RDMA.
**/
class ReadoutUnit : public Unit
{
	public:
		ReadoutUnit( DAQ::Transport* transport, const DAQ::Config* config, bool isDumpConfig );
		virtual ~ReadoutUnit();
		void main();
	protected:
		void sendLocalAddressesToBU();
		void doPullRequestWrite(const UnitCommand & command);
		void doPushRequestWrite(const UnitCommand & command);
		void onWriteFinished(const UnitCommand & command);
		void flushPending();
		void updateProfile();
		void sendData(UnitId dest, const ArgPullGather & arg);
		void notifyPercentage(); 
	private:
		/** Keep track of the remoteIOV of data buffers for PUSH mode **/
		TopoRessource<RemoteIOV> buDataBuffers;
		/** Keep track of the remoteIOV of meta data buffers for PUSH mode **/
		TopoRessource<RemoteIOV> buMetaBuffers;
		/** Keep track of the transport layer **/
		Transport * transport;
		/** Maximum size of an event data fragment **/
		size_t eventMaxDataSize;
		/** Datasource to be used **/
		DataSource * dataSource;
		/** 
		 * Keep track of how many fragments have been sent for the given
		 * event (event id is the key). When full we can mark the event done
		 * in the data source.
		**/
		std::map<uint64_t,int> stateCounter;
		/** Do not send the meta data for testing effect on bandwdith **/
		bool skipMeta;
		/** 
		 * Keep track of the througput of the unit to be printed every 
		 * 2 seconds
		**/
		Throughput bw;
		/** Print the bandwidth every X seconds **/
		int bandwidthDelay;
		/**
		 * Used to store the pending request commands if the requested event is not yet
		 * available in the data sources.
		**/
		PendingCmdList pending;
		/** Define if we are in PULL or PUSH mode **/
		Mode mode;
		/** Keep track of the filling of the datasource for monitoring in htopml **/
		uint64_t filledPercentage;
		/** To know which nodes are local to not account the local communictions **/
		bool * localMap;
		/** Permit to skiip the N first output considered as warm up. **/
		int warmup;
		/** send profiler **/
		bool enableProfile;
		/** keep local bw **/
		bool keepLocalBw;
};


}

#endif //DAQ_READOUT_UNIT_HPP
