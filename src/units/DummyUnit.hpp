/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_DUMMY_UNIT_HPP
#define DAQ_DUMMY_UNIT_HPP

/********************  HEADERS  *********************/
#include "Unit.hpp"

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*********************  CLASS  **********************/
/**
 * The dummy unit is a unit doing nothing, it is required when we want to put
 * nothing in one process as the EXIT method require to have a unit to recieve
 * the message.
 * @todo we can avoid this by making a special check in transport::sendCommand to not check
 * the unit availability if the messages is an exit message.
**/
class DummyUnit : public Unit
{
	public:
		DummyUnit();
		virtual ~DummyUnit();
		void main();
};


}

#endif //DAQ_DUMMY_UNIT_HPP
