/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_FILTER_UNIT_HPP
#define DAQ_FILTER_UNIT_HPP

/********************  HEADERS  *********************/
#include "Unit.hpp"
#include "BuilderUnit.hpp"
#include "gather/FilterFetcher.hpp"
#include "writer/Writer.hpp"

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*********************  CLASS  **********************/
/**
 * The filter unit is the endpoint for the data. It receive the full events
 * from the builder unit, still represented as fragments, then it convert them
 * to finally flushed the data into files. The unit is mainly drived by the closer
 * filter manager.
**/
class FilterUnit : public Unit
{
	public:
		FilterUnit(Transport * transport,const Config * config);
		virtual ~FilterUnit();
		void main();
	protected:
		void fetchFinished ( UnitCommand command );
		void registerCreditToManager(int creditId);
	private:
		/** Keep track of the transport layer to send requets and ack to the filter manager. **/
		Transport * transport;
		/** Data storage **/
		BuilderUnitData data;
		/** Meta data storage **/
		BuilderUnitData meta;
		/** Writer responsible of the data conversion and file writing. **/
		Writer writer;
		/** Define how many event to fetch at the same time just like the builder unit credits **/
		int credits;
		/** keep track of the ID of the filter manager of this unit. **/
		UnitId manager;
		/** Enable of disable file writing. **/
		bool writeFile;
		/** Object tracking the state of the data transfert from the builder unit **/
		FilterFetcher * fetcher[DAQ_MAX_CREDIT];
};

}

#endif //DAQ_FILTER_UNIT_HPP
