/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include "FilterManager.hpp"

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*******************  FUNCTION  *********************/
/**
 * Constructor of the FilterManager.
 * @param transport Define the transport to be used to send the commands.
 * @param config Define the global config object to be used.
**/
FilterManager::FilterManager ( Transport* transport, const Config* config )
	:Unit("FilterManager")
{
	this->transport = transport;
	this->commandChannel.setUniqReader();
}

/*******************  FUNCTION  *********************/
/**
 * Main loop to accept credits from filter units and get data ready from
 * builder unit.
**/
void FilterManager::main()
{
	UnitCommand command;
	while(command << commandChannel) {
		switch(command.type) {
			//////////////////////// INIT STUFF ////////////////////////
			case UNIT_CMD_START:
				break;
			//////////////////////// SCHEDULING ////////////////////////
			case UNIT_CMD_FILTER_REGISTER_CREDIT:
				{
					FilterUnitSlot slot;
					slot.unitId = command.src;
					slot.creditId = command.args.filterReg.creditId;
					if (transport->isDead(slot.unitId.rank) == false)
					{
						this->slots.push_back(slot);
						DAQ_DEBUG_ARG("fm","Filter manager recive credit from FilterUnit : %1").arg(slot.creditId).end();
						if (pending.empty() == false)
							this->sendRequestToFilter();
					} else {
						DAQ_DEBUG_ARG("fm","Ignore reg credit linked to dead node");
					}
				}
				break;
			case UNIT_CMD_FILTER_FETCH_REQUEST:
				{
					FilterUnitSlot slot;
					slot.unitId = command.src;
					slot.creditId = command.args.filterRequest.buStorageId;
					slot.eventId = command.args.filterRequest.eventId;
					
					if (transport->isDead(slot.unitId.rank) == false)
					{
						this->pending.push_back(slot);
						DAQ_DEBUG_ARG("fm","Filter manager recive request from BuilderUnit : %1").arg(slot.creditId).end();
						if (slots.empty() == false)
							this->sendRequestToFilter();
					} else {
						DAQ_DEBUG_ARG("fm","Ignore fetch linked to dead node");
					}
				}
				break;
			case UNIT_CMD_NODE_FAILED:
				removeFailed(command);
				break;
			//////////////////////////// END ///////////////////////////
			default:
				DAQ_FATAL_ARG("Get unexpected command in FilterManager : %1").arg(command.type).end();
				break;
		}
	}
}

/*******************  FUNCTION  *********************/
/**
 * Loop in the available slots to find matching.
 * It search an available slot in filter unit and match it to pending data in
 * builder unit. It then send the request to the filter unit.
**/
void FilterManager::sendRequestToFilter ()
{
	while (pending.empty() == false && slots.empty() == false)
	{
		//extract
		FilterUnitSlot & bu = pending.front();
		FilterUnitSlot & fu = slots.front();
		
		//creaft command
		UnitCommand command;
		command.type = UNIT_CMD_FILTER_FETCH_REQUEST;
		command.args.filterRequest.buStorageId = bu.creditId;
		command.args.filterRequest.buRank     = bu.unitId.rank;
		command.args.filterRequest.buUnitId   = bu.unitId.id;
		command.args.filterRequest.fuCreditId = fu.creditId;
		command.args.filterRequest.eventId    = bu.eventId;
		DAQ_DEBUG_ARG("fm","Links BU (%1) to FU (%2)").arg(bu.unitId).arg(fu.unitId).end();
		
		//remove
		pending.erase(pending.begin());
		slots.erase(slots.begin());
		
		//send
		transport->sendCommand(this,fu.unitId,command);
	}
}

/*******************  FUNCTION  *********************/
/**
 * Manage failed nodes.
**/
void FilterManager::removeFailed ( UnitCommand& command )
{
	DAQ_DEBUG_ARG("fm","Remove reference to failed node : %1").arg(command.args.nodeFailed.rank).end();
	for (FilterUnitSlotList::iterator it = slots.begin() ; it != slots.end() ; ++it)
		if (it->unitId.rank == command.args.nodeFailed.rank)
			it = slots.erase(it);
		
	for (FilterMangerPeningList::iterator it = pending.begin() ; it != pending.end() ; ++it)
		if (it->unitId.rank == command.args.nodeFailed.rank)
			it = pending.erase(it);
}

}
