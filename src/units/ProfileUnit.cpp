/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include "ProfileUnit.hpp"
#ifdef HAVE_HTOPML
	#include <htopml/HtopmlBandwidth.hpp>
	#include <htopml/HtopmlBalancing.hpp>
	#include <htopml/HtopmlFilledPercentage.hpp>
	#include <htopml/HtopmlBuDroppedEvents.hpp>
#endif //HAVE_HTOPML

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*******************  FUNCTION  *********************/
/**
 * Constructor of the profiling unit
 * @param transport define the transport layer to get access to the units layout.
 * @param config Define the global configuration to be used.
**/
ProfileUnit::ProfileUnit(Transport* transport, const Config* config)
	: Unit("ProfileUnit")
	, bandwidth(transport,config,1,UNIT_READOUT_UNIT)
{
	//vars
	this->cntRecv = 0;
	this->transport = transport;
	this->commandChannel.setUniqReader();
	this->cntRU = transport->countUnits(UNIT_READOUT_UNIT);
	
	//bw blancing
	this->balancingArray = new uint64_t [transport->countUnits(UNIT_BUILDER_UNIT)];
	memset(this->balancingArray,0,transport->countUnits(UNIT_BUILDER_UNIT) * sizeof(uint64_t));

	//ru fill
	this->ruFilledPercentageArray = new uint64_t  [transport->countUnits(UNIT_READOUT_UNIT)];
	memset(this->ruFilledPercentageArray,0,transport->countUnits(UNIT_READOUT_UNIT) * sizeof(uint64_t));
	
	//ru fill
	this->buFilledPercentageArray = new uint64_t  [transport->countUnits(UNIT_BUILDER_UNIT)];
	memset(this->buFilledPercentageArray,0,transport->countUnits(UNIT_BUILDER_UNIT) * sizeof(uint64_t));
	
	//ru fill
	this->buDroppedEvents = new uint64_t  [transport->countUnits(UNIT_BUILDER_UNIT)];
	memset(this->buDroppedEvents,0,transport->countUnits(UNIT_BUILDER_UNIT) * sizeof(uint64_t));
	this->totalDropped = 0;
	
	//reg
	#ifdef HAVE_HTOPML
		HtopmlBandwidthHttpNode::registerUnit(this);
		HtopmlBalancingHttpNode::registerUnitArray(this->balancingArray , transport->countUnits(UNIT_BUILDER_UNIT));
		HtopmlFilledPercentageHttpNode::registerFilledArray(this->ruFilledPercentageArray,transport->countUnits(UNIT_READOUT_UNIT),this->buFilledPercentageArray,transport->countUnits(UNIT_BUILDER_UNIT));
		HtopmlDropedEventsHttpNode::registerArray(this->buDroppedEvents,transport->countUnits(UNIT_BUILDER_UNIT),&(this->totalDropped));
	#endif //HAVE_HTOPML
}

/*******************  FUNCTION  *********************/
/**
 * Main function of the unit, need to run indefinitly in threaded mode and need
 * to exit after each command for non threaded mode.
**/
void ProfileUnit::main()
{
	UnitCommand command;
	while(command << commandChannel) {
		switch(command.type) {
			//////////////////////// INIT STUFF ////////////////////////
			case UNIT_CMD_START:
				break;
			case UNIT_CMD_PROFILE:
				do {
					ArgProfile & profile = command.args.profile;
					this->bandwidth.get(command.src.rank,command.src.id,0) = profile.bandwidth;
					if ((++cntRecv) %  cntRU == 0)
						printTotBandwidth();
				} while(false);
				break;
			case UNIT_CMD_NODE_FAILED:
				//TODO
				break;
			//////////////////////// EXPORT BALANCE TRACKING ////////////////////////
			case UNIT_CMD_BALANCE_TRACKING:

				this->balancingArray[command.args.creditBalance.id] = command.args.creditBalance.value;
				break; 
			///////////////////// EXPORT FILLED PERCENTAGE /////////////////////////
			case UNIT_CMD_RU_FILLED_PERCENTAGE:
				ruFilledPercentageArray[command.args.filledPercentage.id] = command.args.filledPercentage.percentage;
				break;
			case UNIT_CMD_BU_FILLED_PERCENTAGE:
				buFilledPercentageArray[command.args.filledPercentage.id] = command.args.filledPercentage.percentage;
				break;
			case UNIT_CMD_BU_DROPPED_EVENTS:
				buDroppedEvents[command.args.filledPercentage.id] = command.args.filledPercentage.percentage;
				totalDropped += command.args.filledPercentage.percentage;
				break;
			//////////////////////////// END ///////////////////////////
			default:
				DAQ_FATAL_ARG("Get unexpected command in ReadoutUnit : %1").arg(command.type).end();
				break;
		}
	}
}

/*******************  FUNCTION  *********************/
void ProfileUnit::printTotBandwidth ( void )
{
	//vars
	float tot = 0.0;
	int cnt = 0;
	
	//sum all
	for (int rank = 0 ; rank < transport->getWorldSize() ; rank++) {
		for (int unitId = 0 ; unitId < DAQ_MAX_UNITS_PER_PROCESS ; unitId++) {
			if (transport->getUnitType(rank,unitId) == UNIT_READOUT_UNIT)
			{
				tot += bandwidth.get(rank,unitId,0);
				cnt++;
			}
		}
	}
	
	//print tot and average
	DAQ_MESSAGE_ARG("[SUM] total : %1, average : %2").argUnit1000(tot,"b/s").argUnit1000(tot/(float)cnt,"b/s").end();
}

/*******************  FUNCTION  *********************/
#ifdef HAVE_HTOPML
	/**
	 * Convert the profiling unit to JSON to be used by htopml.
	**/
	void convertToJson(htopml::JsonState & json,const ProfileUnit & value)
	{
		json.openArray();
			for (int rank = 0 ; rank < value.transport->getWorldSize() ; rank++) {
				for (int unitId = 0 ; unitId < DAQ_MAX_UNITS_PER_PROCESS ; unitId++) {
					if (value.transport->getUnitType(rank,unitId) == UNIT_READOUT_UNIT)
						json.printValue(value.bandwidth.get(rank,unitId,0));
				}
			}
		json.closeArray();
	}
#endif //HAVE_HTOPML

}
