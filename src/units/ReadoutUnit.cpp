/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  INFO   ***********************/
/**
 * Restructuration of the original work of 
 * Daniel Campora / dcampora@cern.ch
**/

/********************  HEADERS  *********************/
#include "ReadoutUnit.hpp"
#include "common/Debug.hpp"
#include <gather/Gather.hpp>
#include "datasource/Amc40DataSource.hpp"
#ifdef HAVE_PCIE40
	#include "datasource/Pcie40DataSource.hpp"
#endif //HAVE_PCIE40

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*******************  FUNCTION  *********************/
/**
 * Constructor of the readout unit.
 * @param transport Define the transport layer to be used.
 * @param config Define the global config object.
 * @param isDumpConfig To be enabled when the unit is biuld just to register all 
 *        the fields in the config JSON file. Effectivly when dump is enabled we 
 *        build all the units for fake in the master node (rank == 0) to have all 
 *        the local settings registered in the config file. Without this method
 *        the dumped file might contain only the event manager options as it is the
 *        only unit running in node 0.
**/
ReadoutUnit::ReadoutUnit ( DAQ::Transport* transport, const DAQ::Config* config, bool isDumpConfig ) 
            :Unit ( "ReadoutUnit")
            ,buDataBuffers(transport,config,config->builderStored * DAQ_DATA_SOURCE_RAILS,UNIT_BUILDER_UNIT)
			,buMetaBuffers(transport,config,config->builderStored * DAQ_DATA_SOURCE_RAILS,UNIT_BUILDER_UNIT)
{
	//setup
	this->transport = transport;
	this->bandwidthDelay = config->bandwidthDelay;
	this->eventMaxDataSize = config->eventRailMaxDataSize;
	this->skipMeta = config->skipMeta;
	this->commandChannel.setUniqReader();
	this->localMap = config->localMap;
	this->warmup = config->getAndSetConfigInt("testing","warmup",0);
	this->enableProfile = config->getAndSetConfigBool("testing","profile",false);
	this->mode = config->mode;
	this->keepLocalBw = config->keepLocalBw;
	
	//data source
	if (isDumpConfig) 
	{
		this->dataSource = nullptr;
	} else if (config->dataSource == "dummy") {
		this->dataSource = new DummyDataSource(config,transport);
	} else if (config->dataSource == "amc40") {
		this->dataSource = new Amc40DataSource(config,transport);
	} else if (config->dataSource == "pcie40") {
		#ifdef HAVE_PCIE40
			this->dataSource = new Pcie40DataSource(config,transport);
		#else	
			DAQ_FATAL("PCIE40 support not compiled, cannot use this datasource !");
		#endif
	} else {
		DAQ_FATAL_ARG("Invalid data source type : %1, supporte 'dummy' or 'amc40'").arg(config->dataSource).end();
	}
	
	//
	if (this->dataSource != nullptr)
		this->filledPercentage = dataSource->getFilledPercentage();
	else
		this->filledPercentage = 0;
	
	//register
	if (dataSource != nullptr)
		dataSource->registerSegments(transport);
}

/*******************  FUNCTION  *********************/
/**
 * Readout unit destructor to free the data source 
**/
ReadoutUnit::~ReadoutUnit ()
{
	if (this->dataSource != nullptr) {
		delete this->dataSource;
		this->dataSource = nullptr;
	}
}

/*******************  FUNCTION  *********************/
/**
 * Main method of readout unit. It take the next command from the go channel
 * and execute it. This function need to exit in non threaded mode and loop
 * until closure of the go channel in threaded mode.
**/
void ReadoutUnit::main ()
{
	UnitCommand command;
	while(command << commandChannel) {
		switch(command.type) {
			//////////////////////// INIT STUFF ////////////////////////
			case UNIT_CMD_START:
				DAQ_DEBUG("ru","Get start command");
				this->dataSource->setId(transport->getGblIdOfUnit(this));
				sendLocalAddressesToBU();
				bw.start();
				break;
			case UNIT_CMD_EXIT:
				DAQ_DEBUG("ru","Get exit");
				transport->setExit();
				break;
			case UNIT_CMD_INIT_REG_BU_BUFFER:
				do {
					const ArgRegBuffer & args = command.args.regBuffer;
					for (int i = 0 ; i < DAQ_DATA_SOURCE_RAILS ; i++) {
						DAQ_DEBUG_ARG("ru","Get BU buffer for %1 : %2/%3").arg(command.src).arg(args.meta[i].addr).arg(args.meta[i].key).end();
						DAQ_DEBUG_ARG("ru","Get BU buffer for %1 : %2/%3").arg(command.src).arg(args.data[i].addr).arg(args.data[i].key).end();
						buDataBuffers.set(command.src,i+DAQ_DATA_SOURCE_RAILS * args.creditId,args.data[i]);
						buMetaBuffers.set(command.src,i+DAQ_DATA_SOURCE_RAILS * args.creditId,args.meta[i]);
					}
					
					if (buDataBuffers.hasAllRessources() && buMetaBuffers.hasAllRessources()) {
						DAQ_DEBUG("ru","ReadoutUnit send READY");
						UnitCommand cmd;
						cmd.type = UNIT_CMD_INIT_UNIT_READY;
						transport->sendCommand(this,UNIT_EVENT_MANAGER,cmd);
					}
				} while (false);
				break;
			//////////////////////// PULL STUFF ////////////////////////
			case UNIT_CMD_PULL_REQUEST_WRITE:
				DAQ_DEBUG("ru","Get request write");
				doPullRequestWrite(command);
				break;
			case UNIT_CMD_INIT_UNIT_READY:
				break;
			/////////////////////// PUSH STUFF /////////////////////////
			case UNIT_CMD_PULL_PUBLISH_CREDIT:
				doPushRequestWrite(command);
				break;
			////////////////////// COMMON STUFF ////////////////////////
			case UNIT_CMD_PUSH_WRITE_FINISHED:
			case UNIT_CMD_PULL_WRITE_FINISHED:
				DAQ_DEBUG_ARG("ru","Get pull finished %1").arg(command.args.pullGather.eventId).end();
				onWriteFinished(command);
				break;
			//////////////////////// ERROR STUFF ////////////////////////
			case UNIT_CMD_NODE_FAILED:
				//TODO
				break;
			default:
				DAQ_FATAL_ARG("Get unexpected command in ReadoutUnit : %1").arg(command.type).end();
				break;
		};
		
		//check if some previously tasks can be done dur to new events available
		//in the data source.
		flushPending();
	}
	
	flushPending();
}

/*******************  FUNCTION  *********************/
/**
 * Function to be called when a RDMA write is finished and the data can be considered
 * as sent. It permits to check if the event is finished (all 4 fragments sent) ot not.
 * If finished it will mark it done in the data source and account it contriubtion to
 * bandwidth.
 * @param command The recvied acknowledgment command which must be UNIT_CMD_PUSH_WRITE_FINISHED or UNIT_CMD_PULL_WRITE_FINISHED 
**/
void ReadoutUnit::onWriteFinished ( const UnitCommand& command )
{
	//checks
	assert(command.type == UNIT_CMD_PUSH_WRITE_FINISHED || command.type == UNIT_CMD_PULL_WRITE_FINISHED);
	
	int eventId = command.args.pullGather.eventId;
	assumeArg(stateCounter.find(eventId) != stateCounter.end(),"Can't find counter for eventId %1").arg(eventId).end();
	stateCounter[eventId]++;
	int cnt = 2;
	if (skipMeta)
		cnt = 1;
	assert(stateCounter[eventId] <= DAQ_DATA_SOURCE_RAILS * cnt);
	if (stateCounter[eventId] == DAQ_DATA_SOURCE_RAILS * cnt) {
		DAQ_DEBUG_ARG("ru","eventId %1 (from ReadoutUnit::onWriteFinished)").arg(command.args.pullGather.eventId).end();
		
		// update bw
		// only update for external node transfers - we don't want to count the
		// speed for same node transfers (will be faster)
		if (!localMap[command.src.rank] || !localMap[command.dest.rank] || this->keepLocalBw)
			bw.reg(DAQ_DATA_SOURCE_RAILS * cnt,dataSource->getTotalSize(eventId));
		
		//cleanup event
		dataSource->ackSent(eventId);
		stateCounter.erase(eventId);
		
		//if elapsed need print
		if (bw.getElapsedTime() > bandwidthDelay) {
			if (warmup == 0)
				DAQ_MESSAGE_ARG("[MON] RU %2 rates : %1").arg(bw).arg(transport->getRank()).end();
			else
				warmup--;
			updateProfile();
			bw.restart();
			// Send the percentage of filled buffers to PU 
			notifyPercentage();  

		}
	}
}

/*******************  FUNCTION  *********************/
/**
 * Used when htopml is enabled, it send the bandwidth of the unit to the
 * global profiling unit.
**/
void ReadoutUnit::updateProfile()
{
	bool status = this->enableProfile;
	#ifdef HAVE_HTOPML
		status = true;
	#endif //HAVE_HTOPML
	if (status)
	{
		UnitCommand command;
		command.type = UNIT_CMD_PROFILE;
		command.args.profile.bandwidth = bw.getBw();
		transport->sendCommand(this,UNIT_PROFILE_UNIT,command);
	}
}

/*******************  FUNCTION  *********************/
/**
 * register the local buffer into the remote builder units at startup
**/
void ReadoutUnit::sendLocalAddressesToBU ()
{
	//setup command
	UnitCommand command;
	ArgRegBuffer & args = command.args.regBuffer;
	command.type = UNIT_CMD_INIT_REG_RU_BUFFER;
	args.creditId = -1;
	//TODO
		
	//send to all BU
	transport->sendCommand(this,UNIT_BUILDER_UNIT,command);
}

/*******************  FUNCTION  *********************/
/**
 * Flush the pending requests waiting their event to be available in the data source.
**/
void ReadoutUnit::flushPending ()
{
	PendingCmdList::iterator it = pending.begin();
	while( it != pending.end()) {
		ArgPullGather & args = it->args.pullGather;
		if (dataSource->IsEventAvailable(args.eventId)) {
			DAQ_DEBUG_ARG("ru","Flush pending request for %1").arg(args.eventId).end();
			doPullRequestWrite(*it);
			it = pending.erase(it);
		} else {
			++it;
		}
	}
}

/*******************  FUNCTION  *********************/
/**
 * Excute the PULL requeste by sending rdma write command for the requested event.
**/
void ReadoutUnit::doPullRequestWrite ( const UnitCommand& command )
{
	//errors
	assert(command.type == UNIT_CMD_PULL_REQUEST_WRITE);
	
	const ArgPullGather & args = command.args.pullGather;
	
	//check if can send next, otherwise push again in queue (might be better to build a pending queue: TODO)
	if (dataSource->IsEventAvailable(args.eventId) == false) {
		pending.push_back(command);
		DAQ_DEBUG_ARG("ru","Re-insert event request for %1").arg(args.eventId).end();
		return;
	}
	
	this->sendData(command.src,args);
}

/*******************  FUNCTION  *********************/
/**
 * Excute the PUSH requeste by sending rdma write command for the requested event.
**/
void ReadoutUnit::doPushRequestWrite ( const UnitCommand& command )
{
	//errors
	assert(command.type == UNIT_CMD_PULL_PUBLISH_CREDIT);
	
	//extract some infos
	int credit = command.args.creditAffect.creditId;
	int eventId = command.args.creditAffect.eventId;
	int storageId = command.args.creditAffect.storageId;
	UnitId dest;
	dest.rank = command.args.creditAffect.buRank;
	dest.id = command.args.creditAffect.buUnitId;
	
	//build ArgPullGather
	ArgPullGather arg;
	arg.creditId = credit;
	arg.eventId = eventId;
	
	//setup datas
	for (int i = 0 ; i < DAQ_DATA_SOURCE_RAILS ; i++) {
		arg.dataIov[i] = buDataBuffers.get(dest,i+DAQ_DATA_SOURCE_RAILS * storageId);
		arg.metaIov[i] = buMetaBuffers.get(dest,i+DAQ_DATA_SOURCE_RAILS * storageId);
		//int creditId,int worldSize,int targetRank, int targetUnitId, int rail, DAQ::GatherTarget targetVar )
		arg.uuidData[i] = Gather::getUUID(credit,transport->getWorldSize(),this->transport->getRank(),this->transport->getUnitId(this),i,TARGET_DATA);
		arg.uuidMeta[i] = Gather::getUUID(credit,transport->getWorldSize(),this->transport->getRank(),this->transport->getUnitId(this),i,TARGET_META);
	}
	
	//do send
	sendData(dest,arg);
}

/*******************  FUNCTION  *********************/
/**
 * Send fill percentage to the profiling unit if htopml is enabled.
**/
void ReadoutUnit::notifyPercentage()
{
	#ifdef HAVE_HTOPML
		UnitCommand command;
		command.type = UNIT_CMD_RU_FILLED_PERCENTAGE;
		command.args.filledPercentage.id = transport->getGblIdOfUnit(this);
		command.args.filledPercentage.percentage = this->filledPercentage;
		transport->sendCommand ( this, UNIT_PROFILE_UNIT, command );
	#endif //HAVE_HTOPML
}

/*******************  FUNCTION  *********************/
/**
 * Effectively send the requested data via RMDA write to the remote builder unit.
 * @param dest Define the unit id of the remote builder unit.
 * @param args Define the addresses to be used to make the RMDA write.
**/
void ReadoutUnit::sendData ( UnitId dest, const ArgPullGather& args )
{
	//setup post command for notification of the end of write action
	UnitCommand postCommand;
	postCommand.type = (mode == MODE_PULL) ? UNIT_CMD_PULL_WRITE_FINISHED : UNIT_CMD_PUSH_WRITE_FINISHED;
	postCommand.args.pullGather = args;
	postCommand.src = dest;
	postCommand.dest.rank = transport->getRank();
	postCommand.dest.rank = transport->getUnitId(this);
	
	//dummy data source
	BucketInfo bucket;
	dataSource->getBucket(bucket,args.eventId);
	
	//extract addresses
	long eventId = args.eventId;
	stateCounter[eventId] = 0;
	
	DAQ_DEBUG_ARG("ru","Get a write request for eventId = %1").arg(eventId).end();
	
	//loop on sources
	for (int rail = 0 ; rail < DAQ_DATA_SOURCE_RAILS ; rail++) {
		DAQ_DEBUG_ARG("ru","Send %1 uuid.meta[0] = %2").arg((void*)bucket.metaFragments[rail].base).arg(args.uuidMeta[0]).end();
		
		if (skipMeta == false) {
			//post to transport (meta)
			transport->rdmaWrite(
				this,
				dest.rank,
				dest.id,
				bucket.metaFragments[rail].base,
				args.metaIov[rail].addr,
				args.metaIov[rail].key,
				bucket.metaFragments[rail].size,
				args.uuidMeta[rail],
				postCommand
			);
		}
		
		//DAQ_DEBUG_ARG("RU","Send data : %1").arg(bucket.dataFragments[rail].base+4).end();
		
		//post to transport (data)
		transport->rdmaWrite(
			this,
			dest.rank,
			dest.id,
			bucket.dataFragments[rail].base,
			args.dataIov[rail].addr,
			args.dataIov[rail].key,
			bucket.dataFragments[rail].size,
			args.uuidData[rail],
			postCommand
		);
	}
}

}
