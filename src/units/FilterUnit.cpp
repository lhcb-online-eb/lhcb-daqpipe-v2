/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include "FilterUnit.hpp"

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*******************  FUNCTION  *********************/
/**
 * Constructor of the filter unit.
 * @param transport Define the transport to be used to send and receive data.
 * @param config Define the global config object to be used.
**/
FilterUnit::FilterUnit ( Transport* transport, const Config* config )
	:Unit("FilterUnit"),
	data(config->eventRailMaxDataSize,config->filterCredits,config->builderStored,transport->countUnits(UNIT_READOUT_UNIT),transport,"RU/data"),
	meta(config->getDefaultMetaDataSize(),config->filterCredits,config->builderStored,transport->countUnits(UNIT_READOUT_UNIT),transport,"RU/meta"),
	writer(*config,transport->countUnits(UNIT_READOUT_UNIT),&meta,&data)
{
	//info
	DAQ_DEBUG("fu","Unit ready");
	
	this->transport = transport;
	this->commandChannel.setUniqReader();
	//TODO change
	credits = config->filterCredits;
	assumeArg(credits < DAQ_MAX_CREDIT,"Invalid credit number, limited to %1").arg(DAQ_MAX_CREDIT).end();
	
	//find the related manager
	int rank = transport->getRank();
	bool found = false;
	while (rank >= 0 && !found)
	{
		for (int j = 0 ; j < DAQ_MAX_UNITS_PER_PROCESS ; j++)
		{
			if (transport->getUnitType(rank,j) == UNIT_FILTER_MANAGER)
			{
				manager = UnitId(rank,j);
				found = true;
				break;
			}
		}
		rank--;
	}
	assume(found,"Fail to found the related FilterManager, check your topology !");
	
	//setup fetcher
	for (int i = 0 ; i < credits ; i++)
		fetcher[i] = new FilterFetcher(transport,this,config,&data,&meta,transport->countUnits(UNIT_READOUT_UNIT),i);
	
	//if need to write file
	this->writeFile = config->getAndSetConfigBool("general","writeFile",false);
}

/*******************  FUNCTION  *********************/
/**
 * Destroy the fetcher states.
**/
FilterUnit::~FilterUnit()
{
	for (int i = 0 ; i < credits ; i++)
		delete fetcher[i];
}

/*******************  FUNCTION  *********************/
/**
 * Main loop of the unit to handle the incomming commands. It need to loop
**/
void FilterUnit::main()
{
	UnitCommand command;
	while(command << commandChannel) {
		switch(command.type) {
			//////////////////////// INIT STUFF ////////////////////////
			case UNIT_CMD_START:
				for (int i = 0 ; i < credits ; i++)
					this->registerCreditToManager(i);
				break;
			case UNIT_CMD_NODE_FAILED:
				//TODO
				DAQ_WARNING("Node failure not handled !");
				break;
			case UNIT_CMD_FILTER_FETCH_REQUEST:
				DAQ_DEBUG_ARG("fu","Get fetch request on %6 from FilterManager (%1) for BuilderUnit (%2:%3:%4), eventId %5 on credit %6")
					.arg(command.src)
					.arg(command.args.filterRequest.buRank)
					.arg(command.args.filterRequest.buUnitId)
					.arg(command.args.filterRequest.buStorageId)
					.arg(command.args.filterRequest.fuCreditId)
					.arg(command.args.filterRequest.eventId)
					.arg(command.dest)
					.end();
				assert(command.args.filterRequest.fuCreditId < credits);
				meta.assign(command.args.filterRequest.fuCreditId);
				data.assign(command.args.filterRequest.fuCreditId);
				fetcher[command.args.filterRequest.fuCreditId]->start(UnitId(command.args.filterRequest.buRank,command.args.filterRequest.buUnitId),command.args.filterRequest.buStorageId,command.args.filterRequest.eventId);
				break;
			case UNIT_CMD_FILTER_FETCH_FINISHED:
				DAQ_DEBUG("fu","Fetch finished");
				fetchFinished(command);
				break;
			//////////////////////////// END ///////////////////////////
			default:
				DAQ_FATAL_ARG("Get unexpected command in FilterUnit : %1").arg(command.type).end();
				break;
		}
	}
}

/*******************  FUNCTION  *********************/
/**
 * When a fetch is finished it send the event to the file writer
 * for convertion. If file writing is disabled it direclty send the credit
 * to the filter manager to get another event.
**/
void FilterUnit::fetchFinished ( UnitCommand command )
{
	ArgFilterFetchDataReq & args = command.args.filterDataRequest;
	int credit = args.fuCreditId;
	if (fetcher[credit]->stepFinish(args.uuid)) {
		
		//assignement
		int idMeta = meta.getCreditId(credit);
		int idData = data.getCreditId(credit);
		assume(idMeta == idData,"Invalid IDs");
		meta.unassign(credit);
		data.unassign(credit);
		
		if (writeFile) {
			//save to file
			writer.flush(idMeta,[=]{
				//request new
				DAQ_DEBUG("fu","Event fetch finished");
			});
		}
		
		//free
		meta.free(idMeta);
		data.free(idData);
		
		//request new
		DAQ_DEBUG("fu","Event fetch finished");
		registerCreditToManager(credit);
	}
}

/*******************  FUNCTION  *********************/
/**
 * When a credit has been finished for transfert, we register it again
 * in the filter manager for later use.
 * @param creditId ID of the credit to register to the filter manager.
**/
void FilterUnit::registerCreditToManager ( int creditId )
{
	//prepare command
	UnitCommand command;
	command.type = UNIT_CMD_FILTER_REGISTER_CREDIT;
	command.args.filterReg.creditId = creditId;

	//reg
	transport->sendCommand(this,manager,command);
}

}
