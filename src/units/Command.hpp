/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_COMMAND_HPP
#define DAQ_COMMAND_HPP

/********************  HEADERS  *********************/
#include <cassert>
#include <stdint.h>
#include "transport/Transport.hpp"
#include "datasource/DataSource.hpp"
//from jsoncpp
#ifdef HAVE_JSON_CPP
	#include <json/json.h>
#endif //HAVE_JSON_CPP

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/********************  MACROS  **********************/
/**
 * Defint he maximum size of command arguments. This is
 * used to define the size of a command. it depends on the AllArgs
 * union.
**/
#define CMD_ARG_MAX_SIZE sizeof(AllArgs)
/**
 * For UDP protocole we can send up to X missing packets to resend.
 * This must be keep small to not increase the size of arguments as
 * it impact the size of command if larger than other type of arguments.
**/
#define DAQ_UDP_RDMA_MISSING 8

/*********************  TYPES  **********************/
/**
 * List of command type used in DAQPIPE.
**/
enum UnitCommandType
{
	//to detect bad initialization
	UNIT_CMD_INVALID,
	//for init phase
	UNIT_CMD_START,
	UNIT_CMD_INIT_REG_BU_BUFFER,
	UNIT_CMD_INIT_REG_RU_BUFFER,
	UNIT_CMD_INIT_UNIT_READY,
	//Credit tracking
	UNIT_CMD_REG_CREDIT,
	UNIT_CMD_PULL_NO_EM_START,
	//Pull mode
	UNIT_CMD_PULL_PUBLISH_CREDIT,
	UNIT_CMD_PULL_REQUEST_WRITE,
	UNIT_CMD_PULL_WRITE_FINISHED,
	UNIT_CMD_PULL_GATHER_FINISHED,
	//finish
	UNIT_CMD_EXIT,
	UNIT_CMD_STOP_THREAD,	
	//push
	UNIT_CMD_PUSH_REG_EVENT_ID,
	UNIT_CMD_PUSH_GATHER_FINISHED,
	UNIT_CMD_PUSH_WRITE_FINISHED,
	//filter units
	UNIT_CMD_FILTER_REGISTER_CREDIT,
	UNIT_CMD_FILTER_FETCH_REQUEST,
	UNIT_CMD_FILTER_FETCH_FINISHED,
	UNIT_CMD_FILTER_FINISHED,
	UNIT_CMD_FILTER_FREE_BU_STORAGE,
	//for test
	UNIT_PING_PONG,
	//for ProfileUnit
	UNIT_CMD_PROFILE,
	UNIT_CMD_BALANCE_TRACKING, 
	UNIT_CMD_RU_FILLED_PERCENTAGE,
	UNIT_CMD_BU_FILLED_PERCENTAGE,
	UNIT_CMD_BU_DROPPED_EVENTS,
	//for dynamic configuration 
	UNIT_CMD_DYNAMIC_CONFIG, 
	//failure
	UNIT_CMD_NODE_FAILED,
	// RDMA notification
	UNIT_CMD_DRIVER_NOTIFY_RDMA,
	//UDP driver
	UNIT_CMD_DRIVER_UDP_ACK,
	UNIT_CMD_DRIVER_UDP_ACK_FULL,
	UNIT_CMD_DRIVER_UDP_RESEND,
	//keep this as last
	UNIT_CMD_COUNT
};

/********************  ENUM  ************************/
/**
 * Define the gather target considered (data or meta data)
**/
enum GatherTarget
{
	/** Target is data **/
	TARGET_DATA,
	/** Target is meta data **/
	TARGET_META
};

/********************  STRUCT  **********************/
/**
 * Used to notivy when a RDMA is finished (UNIT_CMD_DRIVER_NOTIFY_RDMA).
**/
struct ArgRdmaNotify
{
	uint64_t guuid;
};

/********************  STRUCT  **********************/
/**
 * Used in message for profiling unit to provide the bandwidth of the sender node.
 * (UNIT_CMD_PROFILE)
**/
struct ArgProfile
{
	float bandwidth;
};

/********************  STRUCT  **********************/
/**
 * This is used for ping pong unit test, not used into DAQPIPE.
 * Used when message type is UNIT_PING_PONG.
**/
struct ArgPingPong
{
	int id;
	bool write;
};

/********************  STRUCT  **********************/
/**
 * Used to register remote buffers between RU/BU.
 * Used with type UNIT_CMD_INIT_REG_RU_BUFFER.
**/
struct ArgRegBuffer
{
	/** Credit ID of segments to register **/
	int creditId;
	/** Data adresses and keys **/
	RemoteIOV data[DAQ_DATA_SOURCE_RAILS];
	/** Meta data adresses and keys **/
	RemoteIOV meta[DAQ_DATA_SOURCE_RAILS];
};

/********************  STRUCT  **********************/
/**
 * Used to ack that a gather operation if finished.
 * Used with UNIT_CMD_FILTER_FETCH_FINISHED, UNIT_CMD_PULL_GATHER_FINISHED, UNIT_CMD_PUSH_GATHER_FINISHED
**/
struct ArgGatherAck
{
	uint64_t eventId;
	int creditId;
	int uuid;
	int rail;
	GatherTarget target;
};

/********************  STRUCT  **********************/
/** To be used to ack in RU that a data/meta is sent.
 * Used with type UNIT_CMD_PULL_WRITE_FINISHED.
**/
struct ArgPullGather
{
	RemoteIOV dataIov[DAQ_DATA_SOURCE_RAILS];
	RemoteIOV metaIov[DAQ_DATA_SOURCE_RAILS];
	long eventId;
	int creditId;
	int uuidData[DAQ_DATA_SOURCE_RAILS];
	int uuidMeta[DAQ_DATA_SOURCE_RAILS];
};

/********************  STRUCT  **********************/
/**
 * Register a credit between BU/EM and FM/FU.
 * Used with UNIT_CMD_REG_CREDIT.
**/
struct ArgRegCredit
{
	int creditId;
	int storageId;
};


/********************  STRUCT  **********************/
/**
 * Used to transmit the number of events assigned to each credit. Sent from
 * the BU to the PU. Used with UNIT_CMD_BALANCE_TRACKING.
**/
struct ArgCreditBalance
{
	int id;
	uint64_t value; 
};

/********************  STRUCT  **********************/
/**
 * Used between the FilterUnit and FilterManger to assigned the credits for
 * the transfert. Used with UNIT_CMD_FILTER_REGISTER_CREDIT.
**/
struct ArgCreditAffection
{
	long eventId;
	int buRank;
	int buUnitId;
	int creditId;
	int storageId;
};

/********************  STRUCT  **********************/
/**
 * Used when a node failed to give his rank. Used with UNIT_CMD_NODE_FAILED.
**/
struct ArgNodeFailed
{
	int rank;
};

/********************  STRUCT  **********************/
/**
 * Transmit the credit registration from filter unit to filter manager.
 * Used with UNIT_CMD_FILTER_REGISTER_CREDIT.
**/
struct ArgFilterReg
{
	/** Filter credit to register **/
	int creditId;
};

/********************  STRUCT  **********************/
/**
 * Credit assignation from the filter manager to filter unit and from the builder 
 * unit to filter manager. Used with UNIT_CMD_FILTER_FETCH_REQUEST.
**/
struct ArgFilterFetchReq
{
	/** Credit to be used on builder unit side **/
	int buStorageId;
	/** Credit to be used on filter unit side **/
	int fuCreditId;
	/** Rank of the builder unit **/
	int buRank;
	/** builder unit id **/
	int buUnitId;
	/** Event ID to be assigned **/
	long eventId;
};

/********************  STRUCT  **********************/
/**
 * Request data fragemnts by the filter unit to the builde unit.
 * To be used with UNIT_CMD_FILTER_FETCH_REQUEST.
**/
struct ArgFilterFetchDataReq
{
	/** Filter unit credit to be used **/
	int fuCreditId;
	/** Builder unit credit to be used **/
	int buStorageId;
	/** Readout unit id data to transfert fragments **/
	int unitId;
	/** Selected rail **/
	int rail;
	/** If it is DATA or META **/
	GatherTarget target;
	/** Where to write the data on the reciever side **/
	RemoteIOV iov;
	/** Unit ID to use to notify end of the transaction on the reciever side. **/
	int uuid;
};

/********************  STRUCT  **********************/
/**
 * On the filter unit side ack the recieving of a collision.
**/
struct ArgFilterFinish
{
	int id;
};

/********************  STRUCT  **********************/
/**
 * Used to update dynamic parameteres values.
 * Used with UNIT_CMD_DYNAMIC_CONFIG.
**/
struct ArgDyncamicConfig
{
	/** ID of the parameter to setup. **/
	int id; 
	/** New value of the parameters **/
	long int value; 
};

/********************  STRUCT  **********************/
/**
 * Notify the profiling unit of the buffer filling in readout units.
 * Used with UNIT_CMD_FILLED_PERCENTAGE.
**/
struct ArgFilledPercentage
{
	/** Unit ID **/
	int id;
	/** Buffer filling percentage **/
	size_t percentage;
};

/********************  STRUCT  **********************/
/**
 * Used in UDP driver to ack reception of a given block (when all paquets have been sent).
 * Used with UNIT_CMD_DRIVER_UDP_ACK.
**/
struct ArgUdpAck
{
	/** Uniq ID to identify the RDMA request **/
	uint64_t guuid;
	/** Number of blocks sent **/
	int blocks;
};

/********************  STRUCT  **********************/
/**
 * Request the sending of missing paquets.
**/
struct ArgUdpResend
{
	/**Unit ID identifying the RDMA request. **/
	uint64_t guuid;
	/** Number of missing fragments **/
	int missingCnt;
	/** IDs of missing fragments. **/
	int missing[DAQ_UDP_RDMA_MISSING];
};

/*********************  TYPES  **********************/
/**
 * Aggregate all the arguments type to compute the max size.
 * Caution, this struct is not used except to compute the arg size in UnitCommand.
**/
union AllArgs
{
	ArgRegBuffer regBuffer;
	ArgPullGather pullGather;
	ArgGatherAck gatherAck;
	ArgRegCredit regCredit;
	ArgCreditAffection creditAffect;
	ArgPingPong pingPong;
	ArgProfile profile;
	ArgNodeFailed nodeFailed;
	ArgFilterReg filterReg;
	ArgFilterFinish filterFinish;
	ArgFilterFetchReq filterRequest;
	ArgFilterFetchDataReq filterDataRequest;
	ArgRdmaNotify rdma;
	ArgCreditBalance creditBalance; 
	ArgDyncamicConfig dynamicConfig; 
	ArgFilledPercentage filledPercentage;
	ArgUdpAck udpAck;
	ArgUdpResend udpResend;
};

/********************  STRUCT  **********************/
/**
 * Common srtucture to transport commands between units.
 * It used a union to store various type of parameters. To know which one 
 * to be used we need to refer to the "type" enum parameter.
**/
struct UnitCommand
{
	//keep as first to avoid alignment problems
	/**
	 * Contain arguments to be transfered by the command.
	**/
	union {
		char raw[CMD_ARG_MAX_SIZE];
		ArgRegBuffer regBuffer;
		ArgPullGather pullGather;
		ArgRegCredit regCredit;
		ArgCreditAffection creditAffect;
		ArgGatherAck gatherAck;
		ArgPingPong pingPong;
		ArgProfile profile;
		ArgNodeFailed nodeFailed;
		ArgFilterReg filterReg;
		ArgFilterFinish filterFinish;
		ArgFilterFetchReq filterRequest;
		ArgFilterFetchDataReq filterDataRequest;
		ArgRdmaNotify rdma;
		ArgCreditBalance creditBalance;
		ArgDyncamicConfig dynamicConfig; 
		ArgFilledPercentage filledPercentage;
		ArgUdpAck udpAck;
		ArgUdpResend udpResend;
	} args;
	/** Identify the sender (rank and id) **/
	UnitId src;
	/** Identify the receiver (rank and id) **/
	UnitId dest;
	/** Define type of command to know how to interpret args. **/
	UnitCommandType type;
	//accessors
	template <class T> void setArg(const T & value);
	template <class T> void loadArg(T & value) const;
	template <class T> T & castArg();
	template <class T> const T & castArg() const;
};

/*******************  FUNCTION  *********************/
/**
 * Set an arguments and automatically used the type to know how to copy it.
 * @param value Define the value to setup.
**/
template <class T>
void UnitCommand::setArg(const T & value) 
{
	assert(sizeof(T) <= CMD_ARG_MAX_SIZE);
	*(T*)(this->args.raw) = value;
}

/*******************  FUNCTION  *********************/
/**
 * Load the argument value depending on the type we want to read.
 * @param value Reference the the local variable in which to read the value.
**/
template <class T>
void UnitCommand::loadArg(T & value) const
{
	assert(sizeof(T) <= CMD_ARG_MAX_SIZE);
	value = *(T*)(this->args.raw);
}

/*******************  FUNCTION  *********************/
/**
 * Cast the argument to the given type.
**/
template <class T>
T & UnitCommand::castArg()
{
	assert(sizeof(T) <= CMD_ARG_MAX_SIZE);
	return *(T*)(this->args.raw);
}

/*******************  FUNCTION  *********************/
/**
 * Cast the argument to the given type.
**/
template <class T>
const T & UnitCommand::castArg() const
{
	assert(sizeof(T) <= CMD_ARG_MAX_SIZE);
	return *(T*)(this->args.raw);
}

/*******************  FUNCTION  *********************/
//from jsoncpp
void fillJson(Json::Value & json,const UnitCommand & command);
void fillJson(Json::Value& json, const UnitId& unitId);
void fillJson(Json::Value& json, const ArgRegBuffer & regBuffer);
void fillJson(Json::Value& json, const ArgGatherAck & arg);
void fillJson(Json::Value& json, const ArgPullGather & arg);
void fillJson(Json::Value& json, const ArgRegCredit & arg);
void fillJson(Json::Value& json, const ArgCreditAffection & arg);
void fillJson(Json::Value& json, const ArgPingPong & arg);
void fillJson(Json::Value& json, const RemoteIOV & iov);
void fprintCommand(FILE *f, const UnitCommand& command);

/*******************  FUNCTION  *********************/
std::ostream & operator << (std::ostream & out,const UnitCommand & command);
std::ostream & operator << (std::ostream & out,const UnitCommandType & type);
std::ostream & operator << (std::ostream & out,const GatherTarget & type);

}

#endif //COMMAND_HPP
