/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include "Command.hpp"
#include <cstdio>
#include <sstream>

/********************  NAMESPACE  *******************/
namespace DAQ
{

/********************  GLOBALS  *********************/
/**
 * Convert target type to string
**/
static const char * cstGatherTargetToStr[] = {
	"DATA",
	"META"
};

/********************  GLOBALS  *********************/
/**
 * Convert command type to string
**/
const char * cstCommandTypeString[UNIT_CMD_COUNT+1] = {
	"UNIT_CMD_INVALID",
	
	"UNIT_CMD_START",
	"UNIT_CMD_INIT_REG_BU_BUFFER",
	"UNIT_CMD_INIT_REG_RU_BUFFER",
	"UNIT_CMD_INIT_UNIT_READY",
	
	"UNIT_CMD_REG_CREDIT",
	"UNIT_CMD_PULL_NO_EM_START",
	
	"UNIT_CMD_PULL_PUBLISH_CREDIT",
	"UNIT_CMD_PULL_REQUEST_WRITE",
	"UNIT_CMD_PULL_WRITE_FINISHED",
	"UNIT_CMD_PULL_GATHER_FINISHED",
	
	"UNIT_CMD_EXIT",
	"UNIT_CMD_STOP_THREAD",
	
	"UNIT_CMD_PUSH_REG_EVENT_ID",
	"UNIT_CMD_PUSH_GATHER_FINISHED",
	"UNIT_CMD_PUSH_WRITE_FINISHED",
	
	"UNIT_CMD_FILTER_REGISTER_CREDIT",
	"UNIT_CMD_FILTER_FETCH_REQUEST",
	"UNIT_CMD_FILTER_FETCH_FINISHED",
	"UNIT_CMD_FILTER_FINISHED",
	"UNIT_CMD_FILTER_FREE_BU_STORAGE",

	"UNIT_CMD_BALANCE_TRACKING", 
	
	"UNIT_PING_PONG",

	"UNIT_PROFILE",
	
	"UNIT_CMD_DYNAMIC_CONFIG", 
	
	"UNIT_CMD_RU_FILLED_PERCENTAGE",
	"UNIT_CMD_BU_FILLED_PERCENTAGE",
	"UNIT_CMD_BU_DROPPED_EVENTS",
	
	"UNIT_CMD_NODE_FAILED",
	
	"UNIT_CMD_DRIVER_NOTIFY_RDMA",
	
	"UNIT_CMD_DRIVER_UDP_ACK",
	"UNIT_CMD_DRIVER_UDP_ACK_FULL",
	"UNIT_CMD_DRIVER_UDP_RESEND",

	"UNIT_COUNT",
};

/*******************  FUNCTION  *********************/
/**
 * Convert argument to json for debugging
**/
void fillJson ( Json::Value& json, const ArgPullGather& arg )
{
	for (int i = 0 ; i < DAQ_DATA_SOURCE_RAILS ; i++) {
		Json::Value dataIov;
		fillJson(dataIov,arg.dataIov[i]);
		Json::Value metaIov;
		fillJson(metaIov,arg.metaIov[i]);
		json["dataIov"].append(dataIov);
		json["metaIov"].append(metaIov);
		json["uuidData"].append(arg.uuidData[i]);
		json["uuidMeta"].append(arg.uuidMeta[i]);
	}
	
	json["eventId"] = (Json::LargestInt)arg.eventId;
	json["creditId"] = arg. creditId;
}

/*******************  FUNCTION  *********************/
/**
 * Convert IOV to json for debugging
**/
void fillJson ( Json::Value& json, const RemoteIOV& iov )
{
	json["addr"] = iov.addr;
	json["key"] = (Json::UInt64)iov.key;
}

/*******************  FUNCTION  *********************/
/**
 * Convert Unit ID to json for debugging
**/
void fillJson ( Json::Value& json, const UnitId& unitId )
{
	json["rank"] = unitId.rank;
	json["id"] = unitId.id;
}

/*******************  FUNCTION  *********************/
/**
 * Convert argument to json for debugging.
**/
void fillJson ( Json::Value& json, const ArgRegBuffer& regBuffer )
{
	json["creditId"] = regBuffer.creditId;
	for (int i = 0 ; i < DAQ_DATA_SOURCE_RAILS ; i++) {
		Json::Value dataIov;
		fillJson(dataIov,regBuffer.data[i]);
		Json::Value metaIov;
		fillJson(dataIov,regBuffer.meta[i]);
		json["data"].append(dataIov);
		json["meta"].append(metaIov);
	}
}

/*******************  FUNCTION  *********************/
/**
 * Convert argument to json for debugging.
**/
void fillJson ( Json::Value& json, const ArgGatherAck& arg )
{
	json["eventId"] = (Json::UInt64)arg.eventId;
	json["creditId"] = arg.creditId;
	json["uuid"] = arg.uuid;
	json["rail"] = arg.rail;
	json["target"] = cstGatherTargetToStr[arg.target];
}

/*******************  FUNCTION  *********************/
/**
 * Convert argument to json for debugging.
**/
void fillJson ( Json::Value& json, const ArgRegCredit& arg )
{
	json["creditId"] = arg.creditId;
	json["storageId"] = arg.storageId;
}

/*******************  FUNCTION  *********************/
/**
 * Convert argument to json for debugging.
**/
void fillJson ( Json::Value& json, const ArgCreditAffection& arg )
{
	json["eventId"] = (Json::LargestInt)arg.eventId;
	json["buRank"] = arg.buRank;
	json["buUnitId"] = arg.buUnitId;
	json["creditId"] = arg.creditId;
}

/*******************  FUNCTION  *********************/
/**
 * Convert argument to json for debugging.
**/
void fillJson ( Json::Value& json, const ArgPingPong& arg )
{
	json["id"] = arg.id;
}

/*******************  FUNCTION  *********************/
/**
 * Convert argument to json for debugging.
**/
void fillJson (Json::Value& json, const ArgCreditBalance& arg)
{
	json["id"] = arg.id; 

}
/*******************  FUNCTION  *********************/
/**
 * Convert argument to json for debugging.
**/
void fillJson ( Json::Value& json, const UnitCommand& command )
{
	json["type"] = cstCommandTypeString[command.type];
	fillJson(json["src"], command.src);
	fillJson(json["dest"], command.dest);
	switch (command.type) {
		case UNIT_CMD_INIT_REG_BU_BUFFER:
		case UNIT_CMD_INIT_REG_RU_BUFFER:
			fillJson(json["args.regBuffer"],command.args.regBuffer);
			break;
		case UNIT_CMD_PULL_REQUEST_WRITE:
			fillJson(json["args.pullGather"],command.args.pullGather);
			break;
		case UNIT_CMD_REG_CREDIT:
			fillJson(json["args.regCredit"],command.args.regCredit);
			break;
		case UNIT_CMD_PULL_PUBLISH_CREDIT:
			fillJson(json["args.creditAffect"],command.args.creditAffect);
			break;
		case UNIT_CMD_PULL_WRITE_FINISHED:
			fillJson(json["args.pullGather"],command.args.pullGather);
		case UNIT_CMD_PULL_GATHER_FINISHED:
			fillJson(json["args.gatherAck"],command.args.gatherAck);
			break;
		case UNIT_CMD_BALANCE_TRACKING: //my code
			fillJson(json["args.creditBalance"],command.args.creditBalance);
			break; 
		case UNIT_PING_PONG:
			fillJson(json["args.pingPong"],command.args.pingPong);
			break;
		case UNIT_CMD_BU_DROPPED_EVENTS:
		case UNIT_CMD_START:
		case UNIT_CMD_INIT_UNIT_READY:
		case UNIT_CMD_PULL_NO_EM_START:
		case UNIT_CMD_EXIT:
		case UNIT_CMD_STOP_THREAD:
		case UNIT_CMD_PUSH_REG_EVENT_ID:
		case UNIT_CMD_PUSH_GATHER_FINISHED:
		case UNIT_CMD_FILTER_FETCH_FINISHED:
		case UNIT_CMD_INVALID:
		case UNIT_CMD_FILTER_FINISHED:
		case UNIT_CMD_FILTER_FREE_BU_STORAGE:
		case UNIT_CMD_DRIVER_NOTIFY_RDMA:
		case UNIT_CMD_PROFILE:
		case UNIT_CMD_PUSH_WRITE_FINISHED:
		case UNIT_CMD_NODE_FAILED:
		case UNIT_CMD_FILTER_FETCH_REQUEST:
		case UNIT_CMD_FILTER_REGISTER_CREDIT:
		case UNIT_CMD_COUNT:
		case UNIT_CMD_DYNAMIC_CONFIG:
		case UNIT_CMD_RU_FILLED_PERCENTAGE:
		case UNIT_CMD_BU_FILLED_PERCENTAGE:
		case UNIT_CMD_DRIVER_UDP_ACK:
			//TODO
		case UNIT_CMD_DRIVER_UDP_ACK_FULL:
			//TODO
		case UNIT_CMD_DRIVER_UDP_RESEND:
			//TODO
			break;
	}
	
	//raw
	char tmp[3*CMD_ARG_MAX_SIZE+1];
	tmp[3*CMD_ARG_MAX_SIZE] = '\0';
	for(size_t i=0; i< CMD_ARG_MAX_SIZE; i++)
		sprintf(tmp+3*i,"%02x ",(unsigned char)command.args.raw[i]);
	json["args.raw"] = tmp;
}

/********************  GLOBALS  *********************/
/**
 * Convert to stream.
**/
std::ostream& operator<< ( std::ostream& out, const UnitCommandType& type )
{
	assert(type >= 0 && type < UNIT_CMD_COUNT);
	out << cstCommandTypeString[type];
	return out;
}

/*******************  FUNCTION  *********************/
/**
 * Convert to stream.
**/
std::ostream& operator<< ( std::ostream& out, const GatherTarget& type )
{
	out << cstGatherTargetToStr[type];
	return out;
}

/*******************  FUNCTION  *********************/
/**
 * Convert to stream.
**/
std::ostream& operator<< ( std::ostream& out, const UnitCommand& command )
{
	Json::Value json;
	fillJson(json,command);
	out << json;
	return out;
}

/*******************  FUNCTION  *********************/
/**
 * Convert to stream.
**/
void fprintCommand(FILE *f, const UnitCommand& command)
{
// 	std::stringstream buffer;
// 	buffer << command;
// 	fprintf(f,buffer.str().c_str());
	
	fprintf(f,"raw = '");
	for(size_t i=0; i<CMD_ARG_MAX_SIZE; i++)
		fprintf(f,"%2x ",(unsigned char)command.args.raw[i]);
	fprintf(f,"'\n");
	
	if (command.type == UNIT_PING_PONG) {
		fprintf(f,"\tid = %d,\n", command.args.pingPong.id);
		fprintf(f,"\twrite = %d,\n", (int)command.args.pingPong.write);
	}
	
	if (command.type == UNIT_CMD_INIT_REG_BU_BUFFER || command.type == UNIT_CMD_INIT_REG_RU_BUFFER) {
		fprintf(f,"regBuffer = {\n");
		fprintf(f,"\tcreditId = %d,\n", command.args.regBuffer.creditId);
		/*fprintf(f,"\tdataBuffer = %#lx,\n", (long unsigned int)command.args.regBuffer.dataBuffer);
		fprintf(f,"\tmetaBuffer = %#lx,\n", (long unsigned int)command.args.regBuffer.metaBuffer);
		fprintf(f,"\tdataKey = %u,\n", (unsigned int)command.args.regBuffer.dataKey);
		fprintf(f,"\tmetaKey = %u,\n", (unsigned int)command.args.regBuffer.metaKey);*/
		fprintf(f,"},\n");
	}
	
	
	if (command.type == UNIT_CMD_PULL_GATHER_FINISHED || command.type == UNIT_CMD_PULL_REQUEST_WRITE ) {
		fprintf(f,"pullGather = {\n");
		
		fprintf(f,"\tdataIov = {{\n");
		fprintf(f,"\t\t\taddr = %#lx,\n", (long unsigned int)command.args.pullGather.dataIov[0].addr);
		fprintf(f,"\t\t\tkey = %d,\n", (int)command.args.pullGather.dataIov[0].key);
		fprintf(f,"\t\t}, {\n");
		fprintf(f,"\t\t\taddr = %#lx,\n", (long unsigned int)command.args.pullGather.dataIov[1].addr);
		fprintf(f,"\t\t\tkey = %d,\n", (int)command.args.pullGather.dataIov[1].key);
		fprintf(f,"\t\t}},\n");
		
		fprintf(f,"\tmetaIov = {{\n");
		fprintf(f,"\t\t\taddr = %#lx,\n", (long unsigned int)command.args.pullGather.metaIov[0].addr);
		fprintf(f,"\t\t\tkey = %d,\n", (int)command.args.pullGather.metaIov[0].key);
		fprintf(f,"\t\t}, {\n");
		fprintf(f,"\t\t\taddr = %#lx,\n", (long unsigned int)command.args.pullGather.metaIov[1].addr);
		fprintf(f,"\t\t\tkey = %d,\n", (int)command.args.pullGather.metaIov[1].key);
		fprintf(f,"\t\t}},\n");
		
		fprintf(f,"\teventId = %ld,\n", command.args.pullGather.eventId);
		fprintf(f,"\tcreditId = %d,\n", command.args.pullGather.creditId);
		fprintf(f,"\tuuidData = {%d, %d}\n", command.args.pullGather.uuidData[0], command.args.pullGather.uuidData[1]);
		fprintf(f,"\tuuidMeta = {%d, %d}\n", command.args.pullGather.uuidMeta[0], command.args.pullGather.uuidMeta[1]);
		fprintf(f,"},\n");
	}
	
	if (command.type == UNIT_CMD_REG_CREDIT) {
		fprintf(f,"regCredit = {\n");
		fprintf(f,"\tid = %d,\n", command.args.regCredit.creditId);
		fprintf(f,"},\n");
	}
	
	if (command.type == UNIT_CMD_PULL_PUBLISH_CREDIT) {
		fprintf(f,"creditAffect = {\n");
		fprintf(f,"\teventId = %u,\n", (unsigned int)command.args.creditAffect.eventId);
		fprintf(f,"\tbuRank = %d,\n", command.args.creditAffect.buRank);
		fprintf(f,"\tbuUnitId = %d,\n", command.args.creditAffect.buUnitId);
		fprintf(f,"\tcreditId = %d,\n", command.args.creditAffect.creditId);
		fprintf(f,"},\n");
	}
	
	if (command.type == UNIT_CMD_PULL_WRITE_FINISHED || command.type == UNIT_CMD_PULL_GATHER_FINISHED) {
		fprintf(f,"gatherAck = {\n");
		fprintf(f,"\teventId = %u,\n", (unsigned int)command.args.gatherAck.eventId);
		fprintf(f,"\tcreditId = %d,\n", command.args.gatherAck.creditId);
		fprintf(f,"\tuuid = %d,\n", command.args.gatherAck.uuid);
		fprintf(f,"\trail = %d,\n", command.args.gatherAck.rail);
		fprintf(f,"\ttarget = %d (TARGET_DATA, TARGET_META),\n", command.args.gatherAck.target);
		fprintf(f,"},\n");
	}
	
	fprintf(f,"src = {\n");
	fprintf(f,"\trank = %d,\n", command.src.rank);
	fprintf(f,"\tid = %d,\n", command.src.id);
	fprintf(f,"},\n");
	
	fprintf(f,"dest = {\n");
	fprintf(f,"\trank = %d,\n", command.dest.rank);
	fprintf(f,"\tid = %d,\n", command.dest.id);
	fprintf(f,"},\n");
	
	fprintf(f,"type = %s,\n", cstCommandTypeString[command.type]);
	
	fprintf(f,"\n");
}

}
