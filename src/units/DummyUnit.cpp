/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  INFO   ***********************/
/**
 * Restructuration of the original work of 
 * Daniel Campora / dcampora@cern.ch
**/

/********************  HEADERS  *********************/
#include "DummyUnit.hpp"
#include "common/Debug.hpp"

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*******************  FUNCTION  *********************/
/**
 * Constructor of the dummy unit.
**/
DummyUnit::DummyUnit () 
            :Unit ( "DummyUnit")
{
}

/*******************  FUNCTION  *********************/
/**
 * Destructor of the dummy unit.
**/
DummyUnit::~DummyUnit ()
{
}

/*******************  FUNCTION  *********************/
/**
 * Main function of dummy unit, read the commands and do nothing.
**/
void DummyUnit::main ()
{
	UnitCommand command;
	while(command << commandChannel) {
	}
}

}
