/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  INFO   ***********************/
/**
 * Restructuration of the original work of 
 * Daniel Campora / dcampora@cern.ch
**/

/********************  HEADERS  *********************/
#include <iostream>
#include <math.h>  

#include "BuilderUnit.hpp"
#include "ReadoutUnit.hpp"
#include "common/Debug.hpp"
#include "gather/PullGatherSimple.hpp"
#include "gather/PullGatherSched.hpp"
#ifdef HAVE_HTOPML
 	#include "htopml/HtopmlGather.hpp"
	#include "htopml/HtopmlGatherHistogram.hpp"
#endif //HAVE_HTOPML

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*******************  FUNCTION  *********************/
/**
 * Constructor of the builder unit. It allocate the gather states, data storage
 * and register the memory to the driver layer for RDMA operations.
 * @param transport Define the transport layer to be used for communications.
 * @param config Define the config global component.
**/
BuilderUnit::BuilderUnit( DAQ::Transport* transport, const DAQ::Config* config ) 
            : Unit("BuilderUnit")
            , ruDataBuffers(transport,config,DAQ_DATA_SOURCE_RAILS,UNIT_READOUT_UNIT)
            , ruMetaBuffers(transport,config,DAQ_DATA_SOURCE_RAILS,UNIT_READOUT_UNIT)
            , meta(config->getDefaultMetaDataSize(),config->builderCredits,config->builderStored,transport->countUnits(UNIT_READOUT_UNIT),transport,"BU/meta")
            , data(config->eventRailMaxDataSize,config->builderCredits,config->builderStored,transport->countUnits(UNIT_READOUT_UNIT),transport,"BU/data")
            , histogramTimeGather(0,0.001,100)
{
	//setup
	this->transport = transport;
	this->credits = config->builderCredits;
	this->eventDataSize = config->eventRailMaxDataSize;
	this->eventMetaSize = config->getDefaultMetaDataSize();
	this->gatherType = config->getAndSetConfigString("general","gatherType","sched");
	this->useEM = (config->mode != MODE_PULL_NO_EM);
	this->runTimeout = config->runTimeout;
	this->mode = config->mode;
	this->commandChannel.setUniqReader();
	this->hasFilter = (config->filterSubfarm != 0);
	this->readoutUnits = transport->countUnits(UNIT_READOUT_UNIT);
	this->hasFarm = (config->farmMode != "none");
	this->numGathers = 0; 
	this->sumGathers = 0; 
	this->nextEventId = -1;
	this->stored = config->builderStored;
	
	//enable skip
	meta.enableSkip();
	data.enableSkip();
	
	//check if filter TCP
	std::string filterType = config->getAndSetConfigString("filter","type","local");
	this->filterTCP = (filterType == "tcp");
	
	//allocate
	this->filterStates = new FilterSendState*[config->builderStored];
	this->filterStatus = new int[config->builderStored];
	
	//same segment mode
	bool sameSegmentMode = config->getAndSetConfigBool("testing","sameSegment",false);
	this->meta.setSameSegment(sameSegmentMode);
	this->data.setSameSegment(sameSegmentMode);
	
	//setup gather manager
	for (int credit = 0 ; credit < config->builderCredits ; credit++) {
		switch(mode) {
			case MODE_PULL:
			case MODE_PULL_NO_EM:
				gather[credit] = buildPullGather(config,credit);
				break;
			case MODE_PUSH:
				gather[credit] = new PushGather(config,this,transport,credit,&meta,&data);
				break;
			default:
				DAQ_FATAL("Invalid mode !");
				break;
		}
	}
	
	//filter states
	for (size_t credit = 0 ; credit < config->builderStored ; credit++)
		filterStates[credit] = new FilterSendState(readoutUnits);
	
	if (mode == MODE_PUSH) {
		for (int credit = 0 ; credit < config->builderCredits ; credit++){ 
			meta.assign(credit);
			data.assign(credit);
		}
	}

	#ifdef HAVE_HTOPML
		HtopmlGatherHistogramHttpNode::Register(&this->histogramTimeGather);
	#endif
}

/*******************  FUNCTION  *********************/
/**
 * Destructor of the builder unit. Mainly free dynamic sub-objects.
**/
BuilderUnit::~BuilderUnit ()
{
	for (int credit = 0 ; credit < this->credits ; credit++)
		delete gather[credit];
	for (size_t credit = 0 ; credit < this->stored ; credit++)
		delete filterStates[credit];
	delete [] filterStates;
	delete [] filterStatus;
}

/*******************  FUNCTION  *********************/
/**
 * Build the selected pull gather tracking state depending of the gather type
 * to be used.
 * @param config Define the config object
 * @param credit Define for which credit to build the gather tracking state.
**/
PullGather* BuilderUnit::buildPullGather ( const Config * config, int credit)
{
	PullGather * gather = nullptr;
	
	//build
	if (this->gatherType == "sched") {
		gather = new PullGatherSched(config,this,transport,credit,&meta,&data);
	} else if (this->gatherType == "simple") {
		gather = new PullGatherSimple(config,this,transport,credit,&meta,&data);
	} else {
		DAQ_FATAL_ARG("Invalid gather type for builder unit (bu.gatherType) : %1").arg(this->gatherType).end();
	}
		
	return gather;
}

/*******************  FUNCTION  *********************/
/**
 * Assign the filter manager to be used by the builder unit.
**/
void BuilderUnit::setupFilterManager ()
{
	int fms = transport->countUnits(UNIT_FILTER_MANAGER);
	int bus = transport->countUnits(UNIT_BUILDER_UNIT);
	int cur = transport->getGblIdOfUnit(this);
	int index = (cur*fms)/bus;
	
	filterManager = transport->getIndexedUnitId(UNIT_FILTER_MANAGER,index);
	assert(transport->getUnitType(filterManager.rank,filterManager.id) == UNIT_FILTER_MANAGER);
	DAQ_DEBUG_ARG("bu","FilterManager is %1").arg(filterManager).end();
}

/*******************  FUNCTION  *********************/
/**
 * Maint loop to handle incomming commands.
**/
void BuilderUnit::main ()
{
	
	UnitCommand command;
	while (command << commandChannel) {
		//apply
		switch(command.type) {
			//////////////////////////// SOME INIT WORK //////////////////////
			case UNIT_CMD_START:
				DAQ_DEBUG("bu","Get start command");
				timeoutChecker.start();
				timeForFillReport.start();
				sendLocalAddressesToRU();
				if (hasFarm)
					setupFilterManager();
				#ifdef HAVE_HTOPML
					for (int credit = 0 ; credit < credits ; credit++)
						HtopmlGatherHttpNode::registerGather(gather[credit]);
				#endif //HAVE_HTOPML
				break;
			case UNIT_CMD_EXIT:
				DAQ_DEBUG("bu","Get exit");
				transport->setExit();
				break;
			case UNIT_CMD_INIT_REG_RU_BUFFER:
				do {
					const ArgRegBuffer & args = command.args.regBuffer;
					
					for (int i = 0 ;i < DAQ_DATA_SOURCE_RAILS ; i++) {
						DAQ_DEBUG_ARG("bu","Get RU buffer for %1 : %2/%3").arg(command.src).arg(args.meta[i].addr).arg(args.meta[i].key).end();
						DAQ_DEBUG_ARG("bu","Get RU buffer for %1 : %2/%3").arg(command.src).arg(args.data[i].addr).arg(args.data[i].key).end();
						ruDataBuffers.set(command.src,i,args.data[i]);
						ruMetaBuffers.set(command.src,i,args.meta[i]);
					}
					
					if (ruDataBuffers.hasAllRessources() && ruMetaBuffers.hasAllRessources()) {
						DAQ_DEBUG("bu","ReadoutUnit send READY");
						UnitCommand cmd;
						cmd.type = UNIT_CMD_INIT_UNIT_READY;
						transport->sendCommand(this,UNIT_EVENT_MANAGER,cmd);
					}
				} while (false);
				break;
			/////////////////////////// PULL NO EM /////////////////////////
			case UNIT_CMD_PULL_NO_EM_START:
				DAQ_DEBUG("bu","No EM start received");
				this->nextEventId = this->transport->getGblIdOfUnit(this);
				for (int i = 0 ; i < credits ; i++)
					this->gatherStartNextPull(i);
				break;
			/////////////////////////// PULL EXCHANGES /////////////////////
			case UNIT_CMD_PULL_PUBLISH_CREDIT:
				DAQ_DEBUG("bu","Get credit, restart gather");
				this->assign(command.args.creditAffect.creditId);
				this->startGather(command);
				this->timeForGather[command.args.creditAffect.creditId].start(); 
				break;
			case UNIT_CMD_PULL_WRITE_FINISHED:
				DAQ_DEBUG("bu","Pull gather write finished");
				this->progressGather(command);
				break;
			case UNIT_CMD_PULL_GATHER_FINISHED: 
				updateGatherTiming(command.args.gatherAck.creditId);
				this->histogramTimeGather.push(this->timeForGather[command.args.gatherAck.creditId].getElapsedTime());
				this->pullGatherFinished(command.args.gatherAck);
				break;
			//////////////////////////// PUSH EXCHANGES ////////////////////
			case UNIT_CMD_PUSH_REG_EVENT_ID:
				DAQ_DEBUG("bu","Get credit, restart gather");
				this->startGather(command);
				this->timeForGather[command.args.creditAffect.creditId].start(); 
				break;
			case UNIT_CMD_PUSH_WRITE_FINISHED:
				DAQ_DEBUG("bu","Push gather write finished");
				this->progressGather(command);
				break;
			case UNIT_CMD_PUSH_GATHER_FINISHED:
				// adds new value to histogram
				this->histogramTimeGather.push(this->timeForGather[command.args.gatherAck.creditId].getElapsedTime());
				this->pushGatherFinished(command.args.gatherAck);
				break;
			//////////////////////////// FILTER UNITS //////////////////////
			case UNIT_CMD_FILTER_FETCH_REQUEST:
				pushToFilter(command);
				break;
			case UNIT_CMD_FILTER_FETCH_FINISHED:
				filterPushFinised(command);
				break;
			case UNIT_CMD_FILTER_FREE_BU_STORAGE:
				meta.free(command.args.filterRequest.buStorageId);
				data.free(command.args.filterRequest.buStorageId);
				break;
			////////////////////////////// ERRROR //////////////////////////
			case UNIT_CMD_NODE_FAILED:
				for (int i = 0 ; i < credits ; i++)
					this->gather[i]->markNodeFailure(command.args.nodeFailed.rank);
				break;
			default:
				DAQ_FATAL_ARG("Get invalid command : %1").arg(command.type).end();
				break;
		}
	}
}

/*******************  FUNCTION  *********************/
void BuilderUnit::checkFillProfiling ( void )
{
	#ifdef HAVE_HTOPML
		if (timeForFillReport.getElapsedTime() > 2)
		{
			//buffer filling
			UnitCommand command;
			command.type = UNIT_CMD_BU_FILLED_PERCENTAGE;
			command.args.filledPercentage.id = transport->getGblIdOfUnit(this);
			command.args.filledPercentage.percentage = (meta.getFillPercentage() + data.getFillPercentage())/2;
			transport->sendCommand ( this, UNIT_PROFILE_UNIT, command );
			
			//dropped
			command.type = UNIT_CMD_BU_DROPPED_EVENTS;
			command.args.filledPercentage.id = transport->getGblIdOfUnit(this);
			command.args.filledPercentage.percentage = dropped;
			this->dropped = 0;
			transport->sendCommand ( this, UNIT_PROFILE_UNIT, command );

			timeForFillReport.stop();
			timeForFillReport.start();
		}
	#endif //HAVE_HTOPML
}

/*******************  FUNCTION  *********************/
void BuilderUnit::assign ( int creditId )
{
	//assign
	bool m = this->meta.assign(creditId);
	bool d = this->data.assign(creditId);
	
	//check
	//assume(m,"Failed to assigne id to credit, increase storage or check bandwidth between BU and FU");
	//assume(d,"Failed to assigne id to credit, increase storage or check bandwidth between BU and FU");
	
	//if not, assign to skip mem
	if (!m || !d)
	{
		static size_t cnt = 0;
		if ((cnt++)%1000 == 0)
			DAQ_WARNING_ARG("Has to skip data in BU due to backpressure : %1 events lost !").arg(cnt).end();
		this->meta.assignSkip(creditId);
		this->data.assignSkip(creditId);
		dropped++;
	}
	
	//check
	assert(data.getCreditId(creditId) == meta.getCreditId(creditId));
	
	//notify
	checkFillProfiling();
}

/*******************  FUNCTION  *********************/
BuilderUnitData& BuilderUnit::getData ( bool meta )
{
	if (meta)
		return this->meta;
	else
		return this->data;
}

/*******************  FUNCTION  *********************/
/**
 * Keep track of timings of each gather operation to build an histogram for monitoring.
 * @param creditId Define the credit which just finished.
**/
void BuilderUnit::updateGatherTiming(int creditId)
{
	float t = this->timeForGather[creditId].getElapsedTime();
	sumGathers += t;
	numGathers++; 
	if(numGathers>100 && t < 5.0 * sumGathers / (float)numGathers)
	{
		this->histogramTimeGather.push(1000*t);
	}
}

/*******************  FUNCTION  *********************/
/**
 * Function to be called when receiving a command data has been sent to filter unit
 * It move the the next step and if finished send the credit to event manager.
 * @param command The recived command with args.
**/
void BuilderUnit::filterPushFinised ( const UnitCommand & command )
{
	//trivial
	if (filterTCP)
		return;
	
	int id = command.args.filterDataRequest.buStorageId;
	assert(filterStatus[id] >= 0 && filterStatus[id] <= readoutUnits * 2 * DAQ_DATA_SOURCE_RAILS);
	
	//incre
	filterStates[id]->step(command.args.filterDataRequest.rail,command.args.filterDataRequest.unitId,command.args.filterDataRequest.target);
	
	//finished
	if (filterStates[id]->isFinished())
	{
		DAQ_DEBUG_ARG("bu","Filter state finish %1").arg(id).end();
		/*ArgGatherAck args;
		args.creditId = command.args.filterDataRequest.buStorageId;
		pushDataUsageFinished(args);*/
		meta.free(id);
		data.free(id);
	}
}

/*******************  FUNCTION  *********************/
/**
 * On request push the requested data to the filter unit (via rdma)
 * @param command The recived command with args.
**/
void BuilderUnit::pushToFilter ( const UnitCommand& command )
{
	DAQ_DEBUG_ARG("bu","Get fetch request from FilterUnit (%1) for credit %2, unit %3")
		.arg(command.src)
		.arg(command.args.filterDataRequest.buStorageId)
		.arg(command.args.filterDataRequest.unitId)
		.end();
	const ArgFilterFetchDataReq & args = command.args.filterDataRequest;
	
	void * ptr = nullptr;
	size_t size = 0;
	switch(args.target)
	{
		case TARGET_DATA:
			ptr = data.getIdPtr(args.buStorageId,args.rail,args.unitId);
			//TODO : use the size from meta-data to not send the max size
			size = data.getEventSize();
			break;
		case TARGET_META:
			ptr = meta.getIdPtr(args.buStorageId,args.rail,args.unitId);
			size = meta.getEventSize();
			break;
	}
	
	UnitCommand postCommand;
	postCommand.type =  UNIT_CMD_FILTER_FETCH_FINISHED;
	postCommand.args.filterDataRequest = args;
	
	DAQ_DEBUG_ARG("bu","Write to filter unit on ptr = %1, size = %2, uuid = %3, type = %4").arg(args.iov.addr).arg(size).arg(args.uuid).arg(args.target).end();
	transport->rdmaWrite(this,command.src.rank,command.src.id,ptr,args.iov.addr,args.iov.key,size,args.uuid,postCommand);
}

/*******************  FUNCTION  *********************/
/**
 * Start a gathering operatoin for the given credit.
 * @param creditId Define which credit to start.
**/
void BuilderUnit::gatherStartNextPull(int creditId)
{
	//check
	assert(creditId >= 0 && creditId < this->credits);
	
	//assign
	assume(meta.assign(creditId),"Failed to assign location to credit !");
	assume(data.assign(creditId),"Failed to assign location to credit !");
	
	//use gather manager to start a new state
	gather[creditId]->start(this->nextEventId);
	
	//update next
	this->nextEventId += this->transport->countUnits(UNIT_BUILDER_UNIT);
}

/*******************  FUNCTION  *********************/
/**
 * Send a request to the filter manager so he assigned a filter unit
 * and start to fetch data.
 * @param args Parameters recived from the command.
**/
void BuilderUnit::sendFilterRequest ( const ArgGatherAck& args )
{
	//errors
	assert(args.creditId >= 0 && args.creditId < credits);
	
	//get id
	size_t id = meta.getCreditId(args.creditId);
	
	//if SKIP, do not send
	if (id == DAQ_SKIP_ID)
	{
		meta.free(id);
		data.free(id);
		return;
	}
	
	//status
	this->filterStatus[id] = 0;
	
	//start
	assert(id >= 0 && id < stored);
	if (!filterTCP)
		filterStates[id]->start();
	
	//debug
	DAQ_DEBUG_ARG("bu","Send fetch request to FilterManager %1 for credit %2")
		.arg(filterManager)
		.arg(args.creditId)
		.end();
	
	UnitCommand command;
	command.type = UNIT_CMD_FILTER_FETCH_REQUEST;
	command.args.filterRequest.buStorageId = id;
	command.args.filterRequest.eventId = args.eventId;
	transport->sendCommand(this,filterManager,command);
}

/*******************  FUNCTION  *********************/
/**
 * Called when the gather operation is finished from the readout unit for a given credit.
 * @param args Arguments recived from the command. For PUSH mode. . Depending on the 
 * availability of filter unit it register to the filter manager of event manager.
**/
void BuilderUnit::pushGatherFinished ( const ArgGatherAck& args )
{
	if (hasFilter) {
		DAQ_DEBUG("bu","pushGatherFinished");
		sendFilterRequest(args);
		pushDataUsageFinished(args,false);
	} else {
		pushDataUsageFinished(args,true);
	}
}

/*******************  FUNCTION  *********************/
/**
 * When all the transfers has been done for the given credit
 * we send it to the event manager to get an assignement to another event ID.
**/
void BuilderUnit::pushDataUsageFinished ( const ArgGatherAck& args, bool doFree )
{
	size_t creditId = args.creditId;
	DAQ_DEBUG_ARG("bu","Push gather finished on slot %1").arg(creditId).end();
	assert(creditId >= 0 && creditId < stored);
	
	//unassign previous
	meta.unassign(creditId,doFree);
	data.unassign(creditId,doFree);
	
	//assign to get an unsed id
	assume(meta.assign(creditId),"Failed to assign location to credit !");
	assume(data.assign(creditId),"Failed to assign location to credit !");
	
	//get id
	int id = meta.getCreditId(creditId);
	
	//publish credit
	sendCreditToEM(creditId,id);
}

/*******************  FUNCTION  *********************/
/**
 * Called when the gather operation is finished from the readout unit for a given credit.
 * @param args Arguments recived from the command. For PULL mode. Depending on the 
 * availability of filter unit it register to the filter manager of event manager.
**/
void BuilderUnit::pullGatherFinished ( const ArgGatherAck& args )
{
	if (hasFilter) {
		sendFilterRequest(args);
		pullDataUsageFinished(args,false);
	} else {
		pullDataUsageFinished(args,true);
	}
}

/*******************  FUNCTION  *********************/
/**
 * In PULL mode register to the event manager to request a new event or compute
 * directly the new event if in PULL_NO_EM mode.
 * @param args Arguments recived from the command.
**/
void BuilderUnit::pullDataUsageFinished ( const ArgGatherAck& args, bool doFree )
{
	bool timeout = false;
	int creditId = args.creditId;
	if (runTimeout != 0 && runTimeout < timeoutChecker.getElapsedTime())
		timeout = true;
	
	//unassign
	meta.unassign(creditId,doFree);
	data.unassign(creditId,doFree);
	
	if (useEM || timeout) {
		DAQ_DEBUG_ARG("bu","Pull gather finished on slot %1").arg(creditId).end();
		assert(creditId >= 0 && creditId < DAQ_MAX_CREDIT);
		sendCreditToEM(creditId,-1);
	} else {
		this->gatherStartNextPull(creditId);
	}
}

/*******************  FUNCTION  *********************/
/**
 * On statup send the local addresses to readout unit to be latter used by RDMA operations.
 * @todo Not used now as we send the addresses on every request commands.
**/
void BuilderUnit::sendLocalAddressesToRU ()
{
	for (int rank = 0 ; rank < transport->getWorldSize() ; rank++) {
		for (int unitId = 0 ; unitId < DAQ_MAX_UNITS_PER_PROCESS ; unitId++) {
			if (transport->getUnitType(rank,unitId) == UNIT_READOUT_UNIT) {
				for (size_t credit = 0 ; credit < this->stored ; credit++) {
					//setup command
					UnitCommand command;
					command.type = UNIT_CMD_INIT_REG_BU_BUFFER;
					ArgRegBuffer & args = command.args.regBuffer;
					args.creditId = credit;
					int ruId = transport->getGblIdOfUnit(rank,unitId);
					for (int i = 0 ; i < DAQ_DATA_SOURCE_RAILS ; i++)
					{
						args.data[i] = transport->getRemoteIOV(rank,data.getIdPtrNoCheck(credit,i,ruId),eventDataSize);
						args.meta[i] = transport->getRemoteIOV(rank,meta.getIdPtrNoCheck(credit,i,ruId),1);
					}
					
					//send to all RU
					transport->sendCommand(this,rank,unitId,command);
				}
			}
		}
	}
}

/*******************  FUNCTION  *********************/
/**
 * Start a gather operaton to fetch the fragments from readout units.
**/
void BuilderUnit::startGather ( const UnitCommand& command )
{
	//extract
	const ArgCreditAffection & args = command.args.creditAffect;
	int creditId = args.creditId;
	int eventId = args.eventId;
	
	//check
	assert(creditId >= 0 && creditId < this->credits);
	
	//use gather manager to start a new state
	gather[creditId]->start(eventId);
}

/*******************  FUNCTION  *********************/
/**
 * When a segment has been recived make progress in the gather state and
 * make the next request.
**/
void BuilderUnit::progressGather ( const UnitCommand& command )
{
	//error
	assert(command.type == UNIT_CMD_PULL_WRITE_FINISHED || command.type == UNIT_CMD_PUSH_WRITE_FINISHED);
	
	//extract
	const ArgGatherAck & args = command.args.gatherAck;
	int creditId = args.creditId;
	assert(creditId >= 0 && creditId < credits);
	
	//update
	if (gather[creditId]->onChunkFinished(command)) {
		for (int i = 0 ; i < credits ; i++)
			gather[i]->checkTimeout();
	}
}

/*******************  FUNCTION  *********************/
/**
 * Send the given credit to event manager to get an assignement.
**/
void BuilderUnit::sendCreditToEM ( int creditId, int storageId )
{
	//setup
	UnitCommand command;
	command.type = UNIT_CMD_REG_CREDIT;
	ArgRegCredit & args = command.args.regCredit;
	args.creditId = creditId;
	
	//ok send ig
	DAQ_DEBUG("bu","Send credit");
	transport->sendCommand(this,UNIT_EVENT_MANAGER,command);
}

}
