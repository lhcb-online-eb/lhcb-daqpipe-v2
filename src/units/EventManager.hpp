/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  INFO   ***********************/
/**
 * Restructuration of the original work of 
 * Daniel Campora / dcampora@cern.ch
**/

#ifndef DAQ_EVENT_MANAGER_HPP
#define DAQ_EVENT_MANAGER_HPP

/********************  HEADERS  *********************/
#include "Unit.hpp"
#include "common/Timer.hpp"
#include "common/TopoRessource.hpp"
#include "transport/Transport.hpp"

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*********************  CLASS  **********************/
/**
 * The event manager is responsible of the dispatch of the data fetching from
 * readout unit to builder unit. It get free credits from the builder unit and
 * and assign an event to it. Then it respond to the builder unit which then
 * start to fetch the data from the readout units.
**/
class EventManager : public Unit
{
	public:
		EventManager( DAQ::Transport* transport, const DAQ::Config* config );
		virtual ~EventManager();
		void main();
	private:
		void initialSendAllCredit();
		void sendExitToAll();
		void sendCredit( const UnitId & unitId, long int eventId, int creditId, int storageId );
		void sendPullNoEMStart ();
		void notifyBalancing(UnitId & src);
	private:
		/** Next event ID to assign **/
		long eventId;
		/** How many units ready to know when to start **/
		int unitsReady;
		/** Timer to check for timeout and make clean exit if reached **/
		Timer timeoutChecker;
		/** To make clean exit, keep track of which unit are in clean state **/
		TopoRessource<bool> waitAllBuForExit;
		/** Keep track of transport layer to send commands **/
		Transport * transport;
		/** For how long to run before clean exit, 0 for no limit **/
		int runTimeout;
		/** How many credits we need to assign to builder unit at startup **/
		int builderCredits;
		/** Running mode to use  (PULL, PUSH, PULL_NO_EM) **/
		Mode mode;
		/** timer between two send of the balance profiling to profile unit **/
		Timer balanceProfileTimer;
		/** Keep track of the number events assigned per rank to track balancing. **/
		uint64_t * eventsPerRank;
		/** Send balance infos every X seconds **/
		float balanceDelay; 
};

}

#endif //DAQ_EVENT_MANAGER_HPP
