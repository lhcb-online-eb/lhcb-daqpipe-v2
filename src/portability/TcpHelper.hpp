/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_TCP_HELPER_HPP
#define DAQ_TCP_HELPER_HPP

/********************  HEADERS  *********************/
#include <string>
#include <cstdio>

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/********************  STRUCT  **********************/
struct TcpHelper
{
	static std::string getHostIp(const std::string & hostname);
	static std::string getLocalIP( const std::string& iface, bool useIpV6 = false );
	static void safeFWrite(FILE * fp,void * base, size_t size);
	static bool safeSend(int fd, void* base, size_t size, bool retErr = false);
	static bool safeRecv(int fd,void * base, size_t size, bool retErr = false);
	static int makeListeningSocket(int port,bool ipv6 = false);
	static int makeUdpListen ( std::string& iface, size_t bufferSize, int port );
};

};

#endif //DAQ_TCP_HELPER_HPP
