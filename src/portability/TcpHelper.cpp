/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include "TcpHelper.hpp"
#include <common/Debug.hpp>
//std
#include <cstring>
#include <cassert>
//unix
#include <unistd.h>
//to get ip
#include <unistd.h>
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <ifaddrs.h>
#include <netinet/in.h> 
#include <arpa/inet.h>


/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*******************  FUNCTION  *********************/
std::string TcpHelper::getHostIp ( const std::string& hostname )
{
	struct hostent *he;
	struct in_addr **addr_list;
	
	//get entry
	he = gethostbyname( hostname.c_str() );
	assumeArg(he != nullptr,"Get error on gethostbyname : %1")
		.arg(hstrerror(h_errno))
		.end();

	//extract addr
	addr_list = (struct in_addr **) he->h_addr_list;
	
	//convert first one
	char buffer[256];
	strcpy(buffer , inet_ntoa(*addr_list[0]) );

	return buffer;
}

/*******************  FUNCTION  *********************/
std::string TcpHelper::getLocalIP ( const std::string& iface, bool useIpV6 )
{
	//locals
	struct ifaddrs * ifAddrStruct=nullptr;
	struct ifaddrs * ifa=nullptr;
	void * tmpAddrPtr=nullptr;
	
	//if iface is empty, search with hostname
	if (iface.empty()) {
		char hostname[1024];
		gethostname(hostname,sizeof(hostname));
		std::string ip = getHostIp(hostname);
		DAQ_DEBUG_ARG("tcp","Get ip for %1 = %2").arg(hostname).arg(ip).end();
		return ip;
	}

	//get addresses
	getifaddrs(&ifAddrStruct);

	//loop to seatch
	for (ifa = ifAddrStruct; ifa != nullptr; ifa = ifa->ifa_next) {
		//if no address
		if (!ifa->ifa_addr) {
			continue;
		}
		
		//check if requested one
		if (iface == ifa->ifa_name)
		{
			//check type
			if (ifa->ifa_addr->sa_family == AF_INET && useIpV6 == false) { // check it is IP4
				// is a valid IP4 Address
				tmpAddrPtr=&((struct sockaddr_in *)ifa->ifa_addr)->sin_addr;
				char addressBuffer[INET_ADDRSTRLEN];
				inet_ntop(AF_INET, tmpAddrPtr, addressBuffer, INET_ADDRSTRLEN);
				DAQ_DEBUG_ARG("tcp","Get ip for %1 = %2").arg(iface).arg(addressBuffer).end();
				return addressBuffer; 
			} else if (ifa->ifa_addr->sa_family == AF_INET6 && useIpV6 == true) { // check it is IP6
				// is a valid IP6 Address
				tmpAddrPtr=&((struct sockaddr_in6 *)ifa->ifa_addr)->sin6_addr;
				char addressBuffer[INET6_ADDRSTRLEN];
				inet_ntop(AF_INET6, tmpAddrPtr, addressBuffer, INET6_ADDRSTRLEN);
				DAQ_DEBUG_ARG("tcp","Get ip for %1 = %2").arg(iface).arg(addressBuffer).end();
				return addressBuffer;
			}
		}
	}
	
	//error not found
	DAQ_FATAL_ARG("Fail to find address of interface %1").arg(iface).end();
	return "";
}

/*******************  FUNCTION  *********************/
bool TcpHelper::safeSend ( int fd, void* base, size_t size, bool retErr )
{
	//check
	assert(fd != -1);
	assert(base != nullptr);
	assert(size > 0);
	
	//send
	char * ptr = (char*)base;
	size_t cursor = 0;
	while (cursor < size) {
		size_t s = size - cursor;
		if (s > 13*1024)
			s = 13*1024;
		ssize_t res = send(fd,ptr+cursor,s,0);
		if (res < 0)
		{
			if (retErr)
				return false;
			else
				assumeArg(res > 0,"Failed to send data, get error : %1").argStrErrno().end();
		}
		cursor += res;
	}
	
	return true;
}

/*******************  FUNCTION  *********************/
void TcpHelper::safeFWrite ( FILE * fp, void* base, size_t size )
{
	//check
	assert(fp != nullptr);
	assert(base != nullptr);
	assert(size > 0);
	
	//send
	char * ptr = (char*)base;
	size_t cursor = 0;
	while (cursor < size) {
		size_t res = fwrite(ptr+cursor,1,size - cursor,fp);
		//TODO check errno
		assumeArg(res > 0,"Failed to send data, get error : %1").argStrErrno().end();
		cursor += res;
	}
}


/*******************  FUNCTION  *********************/
bool TcpHelper::safeRecv ( int fd, void* base, size_t size, bool retErr )
{
	//check
	assert(fd != -1);
	assert(base != nullptr);
	assert(size > 0);
	
	//send
	char * ptr = (char*)base;
	size_t cursor = 0;
	while (cursor < size) {
		ssize_t res = recv(fd,ptr+cursor,size - cursor,0);
		if (res < 0)
		{
			if (retErr)
				return false;
			else
				assumeArg(res != -1,"Failed to send data, get error : %1").argStrErrno().end();
		}
		if (res == 0)
			break;
		else
			cursor += res;
	}
	
	return cursor == size;
}

/*******************  FUNCTION  *********************/
int TcpHelper::makeListeningSocket(int port,bool ipv6)
{
	struct sockaddr_in sin;
	int listener;
	int bindReturnValue, listenReturnValue;
	
	/* Prepare connection parameters. */
	sin.sin_family = ipv6 ? AF_INET6 : AF_INET;
	sin.sin_addr.s_addr = 0;
	sin.sin_port = htons(port);
	
	/* Allocate a new socket */
	listener = socket( ipv6 ? AF_INET6 : AF_INET , SOCK_STREAM, 0);
	assumeArg(listener >= 0, "Fail to open socket : %1 !").argStrErrno().end();
	/* Make it non-blocking */
	//makeNonBlocking(listener);
	
	/* This setting ensures that the socket will be reusable immediately after the program is shut down.
	   (Without this, we would need to wait TIME_WAIT seconds to let the server socket be available
	    again. You can check your OS's TIME_WAIT value by: cat /proc/sys/net/ipv4/tcp_fin_timeout) */
	int yes = 1;
	if (setsockopt(listener, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1) {
		perror("setsockopt");
		exit(1);
	}
	
	//do binding
	bindReturnValue = bind(listener, (struct sockaddr*)&sin, sizeof(sin));
	assumeArg( bindReturnValue == 0, "Error while calling bind(): %1 port: %2").argStrErrno().arg(port).end();
	
	//now listen
	listenReturnValue = listen(listener, 22);
	assumeArg( listenReturnValue == 0, "Error while calling listen(): %1 port: %2").argStrErrno().arg(port).end();
	
	return listener;
}

/*******************  FUNCTION  *********************/
int TcpHelper::makeUdpListen ( std::string & iface, size_t bufferSize, int port )
{
	struct sockaddr_in address;
	int sock;
	
	DAQ_DEBUG_ARG("amc40","Start binding on port %1:%2").arg(iface).arg(port).end();
	
	//get iface address
	std::string addr = TcpHelper::getLocalIP(iface,false);

	//create socket
	assumeArg((sock = socket(AF_INET, SOCK_DGRAM, 0)) != -1,"Failed to create DGRAM socket : %1").argStrErrno().end();

	//read recv buffer size (for debug)
	int rcvbuf;
	socklen_t rcvbuf_len = sizeof(rcvbuf);
	if (getsockopt(sock, SOL_SOCKET, SO_RCVBUF, &rcvbuf, &rcvbuf_len) == -1) {
		::close(sock);
		DAQ_FATAL_ARG("Failed to get socket options (getsockopt) : %1").argStrErrno().end();
	}
	DAQ_DEBUG_ARG("amc40n","SO_RCVBUF = %1").arg(rcvbuf).end();

	//set recv buffer size
	rcvbuf = bufferSize;
	if (setsockopt(sock, SOL_SOCKET, SO_RCVBUF, &rcvbuf, rcvbuf_len) == -1) {
		::close(sock);
		DAQ_FATAL_ARG("Failed to set socket options (setsockopt) : %1").argStrErrno().end();
	}
	DAQ_DEBUG_ARG("amc40n","SO_RCVBUF = %1").arg(rcvbuf).end();

	//setup address
	memset(&address, 0, sizeof address);
	address.sin_family = AF_INET;
	//address.sin_addr.s_addr = htonl(INADDR_ANY);
	address.sin_addr.s_addr = inet_addr(addr.c_str());
	address.sin_port = htons(port);

	//bind
	if ((bind(sock, (struct sockaddr *) &address, sizeof(address))) == -1) {
		::close(sock);
		DAQ_FATAL_ARG("Fail to bind on port %1:%2 : %3").arg(addr).arg(port).argStrErrno().end();
	}
	
	//return
	return sock;
}

};
