/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_HWLOC_HPP
#define DAQ_HWLOC_HPP

/********************  HEADERS  *********************/
#include <string>

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*******************  FUNCTION  *********************/
void pinProcessNearNetDevice(const std::string & deviceName);

}

#endif //DAQ_HWLOC_HPP
