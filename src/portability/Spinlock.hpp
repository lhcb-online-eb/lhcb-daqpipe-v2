/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_SPINLOCK_HPP
#define DAQ_SPINLOCK_HPP

/********************  INFO   ***********************/
/**
 * This file is imported from project DAQ developped by Sebastien Valat.
 * at exascale lab / university of versailles
**/

/********************  HEADERS  *********************/
//must be on top
#include "config.h"

/*********************  TYPES  **********************/
#if defined(DAQ_PORTABILITY_SPINLOCK_PTHREAD)
	//pthread mode
	#include "SpinlockPthread.hpp"

	//map types to generic names
	namespace DAQ
	{
		typedef SpinlockPthread Spinlock;
	}
#elif defined(DAQ_PORTABILITY_SPINLOCK_DUMMY)
	//dummy mode (not thread safe, only for quik portability)
	#include "LockDummy.hpp"
	
	//show some warning
	#warning Caution, you are using the DUMMY mutex implementation, DAQ will not be thread-safe !

	//map types to generic names
	namespace DAQ
	{
		typedef SpinlockDummy Spinlock;
	};
#else
	//not found, fail to compile
	#error "No available implementation for mutex, please check definition of one of DAQ_PORTABILITY_SPINLOCK_* macro in config.h or PORTABILITY_SPINLOCK given to cmake."
#endif

#endif //DAQ_SPINLOCK_HPP
