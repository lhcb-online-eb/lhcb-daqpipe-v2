/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include "config.h"
#include "Hwloc.hpp"
//internal
#include <common/Debug.hpp>
//optional
#ifdef HAVE_HWLOC
	#include <hwloc.h>
#endif //HAVE_HWLOC

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*******************  FUNCTION  *********************/
void pinProcessNearNetDevice ( const std::string& deviceName )
{
	#ifndef HAVE_HWLOC
		if (deviceName.empty() == false)
			DAQ_WARNING("Caution, this was not compiled with hwloc, threads will not be binded");
	#else
		hwloc_topology_t topology;
		
		//nothing to do
		if (deviceName.empty())
			return;
		
		//init
		hwloc_topology_init(&topology);
		
		//enable IO devices
		hwloc_topology_set_flags(topology,HWLOC_TOPOLOGY_FLAG_IO_DEVICES);
		
		//load topology
		hwloc_topology_load(topology);
		
		//loop on bards to print
		hwloc_obj_t obj = nullptr;
		do {
			obj = hwloc_get_next_osdev(topology,obj);
			if (obj != nullptr) {
				const char * name = obj->name;
				DAQ_DEBUG_ARG("binding","Available device : %1").arg(name).end();
				if (name != nullptr && strcmp(name,deviceName.c_str()) == 0)
					break;
			}
		} while (obj != nullptr);
		
		//fail to find
		assumeArg(obj != nullptr,"Fail to find device '%1' to bind threads close to it").arg(deviceName).end();
		
		//pin
		DAQ_INFO_ARG("Binding process close to device '%1'").arg(deviceName).end();
		hwloc_obj_t ancestor = hwloc_get_non_io_ancestor_obj(topology, obj);
		hwloc_set_cpubind(topology, ancestor->cpuset, HWLOC_CPUBIND_PROCESS),
		
		//clean
		hwloc_topology_destroy(topology);
	#endif //HAVE_HWLOC
}

}
