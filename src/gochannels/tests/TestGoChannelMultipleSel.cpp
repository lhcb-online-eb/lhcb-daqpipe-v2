/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#undef NDEBUG

/********************  HEADERS  *********************/
#include <cstdlib>
#include <cassert>
#include <thread>
#include <algorithm>
#include <iostream>
#include <unistd.h>
#include "../GoChannelSelector.hpp"
#include "../GoChannel.hpp"

/********************  NAMESPACE  *******************/
using namespace DAQ;
using namespace std;

/********************  GLOBALS  *********************/
const long REPEAT = 10000000;
bool finished = false;

/*******************  FUNCTION  *********************/
void timeout()
{
	int cnt = 50;
	while (cnt-- > 0 && !finished)
		sleep(1);
	if (!finished)
	{
		cerr << "Timout, likely to be deadlock !" << endl;
		exit(1);
	}
}

/*******************  FUNCTION  *********************/
void client(GoChannel<long> * channela,GoChannel<long> * channelb)
{
	long value;
	long sum = 0;
	long cnt = 0;
	GoChannelSelector sel;
	
	sel.subscibe(channela);
	sel.subscibe(channelb);
	
	GoChannel<long> * channel;
	while(sel.select(channel))
	{
		*channel >> value;
		sum += value;
		cnt++;
	}
	assert(cnt == REPEAT*2);
	assert(sum == REPEAT*30+REPEAT*20);
}

/*******************  FUNCTION  *********************/
int main()
{
	GoChannel<long> channela;
	GoChannel<long> channelb;
	thread t(timeout);
	thread clientThread(client,&channela,&channelb);
	for (long i = 0 ; i < REPEAT ; i++)
	{
		channela << 30l;
		channelb << 20l;
	}
	channela.close();
	channelb.close();
	clientThread.join();
	finished = true;
	t.join();
	return EXIT_SUCCESS;
}
