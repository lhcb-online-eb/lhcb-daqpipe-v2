/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#undef NDEBUG

/********************  HEADERS  *********************/
#include <cstdlib>
#include <cassert>
#include <thread>
#include <algorithm>
#include <unistd.h>
#include <iostream>
#include "../GoChannelSelector.hpp"
#include "../GoChannel.hpp"

/********************  NAMESPACE  *******************/
using namespace DAQ;
using namespace std;

/********************  GLOBALS  *********************/
const long REPEAT = 10000000;
bool finished = false;

/*******************  FUNCTION  *********************/
void timeout()
{
	int cnt = 50;
	while (cnt-- > 0 && !finished)
		sleep(1);
	if (!finished)
	{
		cerr << "Timout, likely to be deadlock !" << endl;
		exit(1);
	}
}

/*******************  FUNCTION  *********************/
void client(GoChannel<long> * channel)
{
	long value;
	long sum = 0;
	long cnt = 0;
	GoChannelSelector sel;
	sel.subscibe(channel);
	while(sel.select(channel))
	{
		*channel >> value;
		sum += value;
		cnt++;
	}
	assert(cnt == REPEAT*2);
	assert(sum == REPEAT*30+REPEAT*20);
}

/*******************  FUNCTION  *********************/
int main()
{
	GoChannel<long> channel;
	thread t(timeout);
	thread clientThread(client,&channel);
	for (long i = 0 ; i < REPEAT ; i++)
	{
		channel << 30l;
		channel << 20l;
	}
	channel.close();
	clientThread.join();
	finished = true;
	t.join();
	return EXIT_SUCCESS;
}
