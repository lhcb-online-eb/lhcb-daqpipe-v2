/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#undef NDEBUG

/********************  HEADERS  *********************/
#include <cstdlib>
#include <cassert>
#include <thread>
#include <algorithm>
#include <iostream>
#include <unistd.h>
#include "../GoChannelSelector.hpp"
#include "../GoChannel.hpp"

/********************  NAMESPACE  *******************/
using namespace DAQ;
using namespace std;

/********************  GLOBALS  *********************/
const long REPEAT = 10000000;
bool finished = false;

/********************  STRUCT  **********************/
struct Test
{
	long a;
	long b;
};

/*******************  FUNCTION  *********************/
void timeout()
{
	int cnt = 50;
	while (cnt-- > 0 && !finished)
		sleep(1);
	if (!finished)
	{
		cerr << "Timout, likely to be deadlock !" << endl;
		exit(1);
	}
}

/*******************  FUNCTION  *********************/
void client(GoChannel<Test> * channel)
{
	Test value;
	long cnt = 0;
	while(*channel >> value)
	{
		assert(value.a == cnt);
		assert(value.b == cnt+1);
		cnt++;
	}
	assert(cnt == REPEAT);
}

/*******************  FUNCTION  *********************/
int main()
{
	GoChannel<Test> channel;
	channel.setUniqReader();
	thread t(timeout);
	thread clientThread(client,&channel);
	for (long i = 0 ; i < REPEAT ; i++)
	{
		Test t{i,i+1};
		channel << t;
	}
	channel.close();
	clientThread.join();
	finished = true;
	t.join();
	return EXIT_SUCCESS;
}
