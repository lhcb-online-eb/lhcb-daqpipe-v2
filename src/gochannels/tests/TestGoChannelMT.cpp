/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#undef NDEBUG

/********************  HEADERS  *********************/
#include <cstdlib>
#include <cassert>
#include <thread>
#include <algorithm>
#include <iostream>
#include <unistd.h>
#include "../GoChannelSelector.hpp"
#include "../GoChannel.hpp"

/********************  NAMESPACE  *******************/
using namespace DAQ;
using namespace std;

/*********************  CONSTS  *********************/
const unsigned long LOOPS = 1000000;
const unsigned long MAX_THREADS = 1024;
unsigned long THREADS = 16;
unsigned long REPEAT = 1;
bool finished = false;

/*******************  FUNCTION  *********************/
void timeout()
{
	int cnt = 50 * REPEAT;
	while (cnt-- > 0 && !finished)
		sleep(1);
	if (!finished)
	{
		cerr << "Timout, likely to be deadlock !" << endl;
		exit(1);
	}
}

/*******************  FUNCTION  *********************/
void client(GoChannel<unsigned long> * channela,GoChannel<float> * channelb)
{
	unsigned long valuea;
	float valueb;
	unsigned long suma = 0;
	float sumb = 0;
	unsigned long cnta = 0;
	unsigned long cntb = 0;
	
	GoChannelSelector sel;
	
	sel.subscibe(channela);
	sel.subscibe(channelb);
	
	GoChannelBase * channel;
	while((channel = sel.select()))
	{
		if (channel == channela)
		{
			*channela >> valuea;
			suma += valuea;
			cnta ++;
		} else if (channel == channelb) {
			*channelb >> valueb;
			sumb += valueb;
			cntb++;
		}
	}
	
	float refb = 0;
	for (unsigned long th = 0 ; th < THREADS ; th++)
		for (unsigned long lo = 0 ; lo < LOOPS ; lo++)
			for (unsigned long rep = 0 ; rep < REPEAT ; rep++)
				refb+=16.0;
	
	assert(cnta == THREADS*LOOPS*REPEAT);
	assert(cntb == THREADS*LOOPS*REPEAT);
	assert(suma == THREADS*LOOPS*REPEAT*30l);
	assert(sumb == refb);
}

/*******************  FUNCTION  *********************/
void sendera(GoChannel<unsigned long> * channel)
{
	for (unsigned long j = 0 ; j < REPEAT ; j++)
		for (unsigned long i = 0 ; i < LOOPS ; i++)
			*channel << 30lu;
}

/*******************  FUNCTION  *********************/
void senderb(GoChannel<float> * channel)
{
	for (unsigned long j = 0 ; j < REPEAT ; j++)
		for (unsigned long i = 0 ; i < LOOPS ; i++)
			*channel << 16.0f;
}

/*******************  FUNCTION  *********************/
int main(int argc,char ** argv)
{
	//args
	if (argc == 3)
	{
		THREADS = atol(argv[1]);
		REPEAT = atol(argv[2]);
		assert(THREADS >= 1 && THREADS < MAX_THREADS);
		assert(REPEAT >= 1);
	}
	
	GoChannel<unsigned long> channela;
	GoChannel<float> channelb;
	thread t(timeout);
	thread clientThread(client,&channela,&channelb);
	thread * sender[MAX_THREADS][2];
	for (unsigned long i = 0 ; i < THREADS ; i++)
	{
		sender[i][0] = new thread(sendera,&channela);
		sender[i][1] = new thread(senderb,&channelb);
	}
	for (unsigned long i = 0 ; i < THREADS ; i++)
	{
		sender[i][0]->join();
		sender[i][1]->join();
	}
	channela.close();
	channelb.close();
	clientThread.join();
	finished = true;
	t.join();
	return EXIT_SUCCESS;
}
