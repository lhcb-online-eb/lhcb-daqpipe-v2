/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include <cstdlib>
#include <cassert>
#include <algorithm>
#include "GoChannelBase.hpp"
#include "GoChannelSelector.hpp"
#include <common/Debug.hpp>

/********************  NAMESPACE  *******************/
namespace DAQ 
{

/*******************  FUNCTION  *********************/
static bool goChannelSelectorDefaultAcceptor()
{
	return true;
}

/*******************  FUNCTION  *********************/
GoChannelSelector::GoChannelSelector ()
{
	this->waitOnEmpty = true;
	this->waiting = 0;
}

/*******************  FUNCTION  *********************/
GoChannelSelector::~GoChannelSelector ()
{
	std::lock_guard<std::mutex> guard(lock);
	if (this->waitOnEmpty)
		for (auto entry : channels)
			entry.channel->unsubscibe(this);
}

/*******************  FUNCTION  *********************/
void GoChannelSelector::subscibe ( GoChannelBase* channel, std::function< bool() > acceptor )
{
	std::lock_guard<std::mutex> guard(lock);
	assert(channel != nullptr);
	ChannelSelectorEntry entry;
	entry.channel = channel;
	entry.acceptor = acceptor;
	channel->setUniqReader();
	this->channels.push_back(entry);
	if (this->waitOnEmpty)
		channel->subscibe(this);
}

/*******************  FUNCTION  *********************/
void GoChannelSelector::subscibe ( GoChannelBase* channel )
{
	this->subscibe(channel,goChannelSelectorDefaultAcceptor);
}

/*******************  FUNCTION  *********************/
void GoChannelSelector::unsubscibe ( GoChannelBase* channel )
{
	bool needNotify = false;
	
	{
		std::lock_guard<std::mutex> guard(lock);
		auto it = channels.begin();
		while (it != channels.end())
			if (it->channel == channel)
				break;
			else
				++it;
		if (it != channels.end()) {
			this->channels.erase(it);
			if (this->waitOnEmpty)
				channel->unsubscibe(this);
		}
		if (channels.empty())
			needNotify = true;
	}
	
	if (needNotify)
		notify(nullptr);
}

/*******************  FUNCTION  *********************/
bool GoChannelSelector::notify ( GoChannelBase* base )
{
	int waiting;
	{
		std::lock_guard<std::mutex> guard(lock);
		waiting = this->waiting;
	}
	if (waiting > 0) {
		cond.notify_one();
		return true;
	} else {
		return false;
	}
}

/*******************  FUNCTION  *********************/
GoChannelBase* GoChannelSelector::select ()
{
	std::unique_lock<std::mutex> lk(lock);
	GoChannelBase * ret = nullptr;
	
	while (ret == nullptr)
	{
		//none to wait
		if (channels.empty()) {
			ret = nullptr;
			break;
		}
		
		//search none empty
		bool retry;
		do {
			retry = false;
			for (auto & entry : channels) {
				if (entry.acceptor()) {
					if (entry.channel->emptyTryLock(retry) == false) {
						ret = entry.channel;
						break;
					}
				}
			}
		} while(retry && ret == nullptr);

		if (ret == nullptr)
		{
			if (!waitOnEmpty) {
				break;
			} else if (channels.empty() == false) {
				//otherwise need to wait some
				waiting++;
				cond.wait(lk);
				waiting--;
			}
		}
	}
	
	lk.unlock();
	return ret;
}

/*******************  FUNCTION  *********************/
void GoChannelSelector::disableWaitOnEmpty ()
{
	assert(waiting == 0);
	this->waitOnEmpty = false;
}

}
