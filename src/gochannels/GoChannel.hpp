/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_GO_CHANN_HPP
#define DAQ_GO_CHANN_HPP

/********************  HEADERS  *********************/
#include "config.h"
#include "GoChannelBase.hpp"
#include <mutex>
#include <condition_variable>
//switches opa vs std
#ifdef HAVE_OPENPA
	extern "C" {
		#include <opa_queue.h>
		#include <opa_primitives.h>
	}
#else //HAVE_OPENPA
	#include <queue>
#endif //HAVE_OPENPA

/********************  NAMESPACE  *******************/
namespace DAQ 
{

/*********************  CLASS  **********************/
template <class T>
class GoChannel : public GoChannelBase
{
	public:
		#ifdef HAVE_OPENPA
			struct QueueEntry
			{
				OPA_Queue_element_hdr_t queueHeader;
				T value;
			};
		#else //HAVE_OPENPA
			typedef std::queue<T> Queue;
		#endif //HAVE_OPENPA
	public:
		GoChannel();
		void push(const T & data);
		bool pop(T & data);
		void popUnlock();
		bool popUnlock( T& data );
		bool empty();
		bool emptyLock();
		bool emptyTryLock(bool & retry);
		void close();
		bool isClosed();
		void disableWaitOnEmpty();
		T & operator * ();
		virtual void setUniqReader();
	public:
		template <class U> friend bool operator << (U & data, GoChannel<U> & channel);
		template <class U> friend void operator << (GoChannel<U> & channel, const U & data);
		template <class U> friend bool operator >> (GoChannel<U> & channel, U & data);
		template <class U> friend void operator >> (const U & data,GoChannel<U> & channel);
	private:
		#ifdef HAVE_OPENPA
			OPA_Queue_info_t opaQueue;
			OPA_Queue_info_t allocated;
			QueueEntry * unlockedEntry;
			OPA_int_t count;
			bool uniqReader;
		#else //HAVE_OPENPA
			Queue queue;
		#endif //HAVE_OPENPA
		std::mutex lock;
		bool locked;
		int closeDelay;
		std::condition_variable cond;
		int hasLocalWait;
		bool waitOnEmpty;
};

}

/********************  HEADERS  *********************/
#ifdef HAVE_OPENPA
#include "GoChannel_impl_opa.hpp"
#else
#include "GoChannel_impl_std.hpp"
#endif

#endif //DAQ_GO_CHANN_HPP
