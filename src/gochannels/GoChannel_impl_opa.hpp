/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_GO_CHANN_IMPL_HPP
#define DAQ_GO_CHANN_IMPL_HPP

/********************  HEADERS  *********************/
#include <common/Debug.hpp>
#include "GoChannel.hpp"

/********************  NAMESPACE  *******************/
namespace DAQ 
{

/*******************  FUNCTION  *********************/
template <class T>
GoChannel<T>::GoChannel()
{
	this->locked = false;
	this->closeDelay = -1;
	this->hasLocalWait = 0;
	this->waitOnEmpty = true;
	//opa specific
	this->unlockedEntry = nullptr;
	this->uniqReader = false;
	OPA_store_int(&this->count,0);
	OPA_Queue_init(&opaQueue);
	OPA_Queue_init(&allocated);
}

/*******************  FUNCTION  *********************/
template <class T>
void GoChannel<T>::setUniqReader()
{
	this->uniqReader = true;
}

/*******************  FUNCTION  *********************/
template <class T>
bool GoChannel<T>::isClosed()
{
	return this->closeDelay != -1;
}

/*******************  FUNCTION  *********************/
template <class T>
void GoChannel<T>::push(const T & data)
{
	bool needNotify = false;
	
	{
		//taks lock
		//std::lock_guard<std::mutex> guard(lock);
		
		if (closeDelay == -1) {
			QueueEntry * entry = new QueueEntry;
			entry->value = data;
			OPA_Queue_header_init(&entry->queueHeader);
			OPA_Queue_enqueue(&opaQueue, entry, QueueEntry, queueHeader);
			OPA_incr_int(&count);
			if (hasLocalWait > 0)
				cond.notify_one();
			else
				needNotify = true;
		} else {
			DAQ_WARNING("Caution, you push some messages after closing the channel !");
		}
	}
	
	if (needNotify)
		this->notify();
}

/*******************  FUNCTION  *********************/
template <class T>
bool GoChannel<T>::pop(T & data)
{
	bool needNotify = false;
	bool ret = false;
	
	if (!uniqReader) {
		//taks lock
		std::unique_lock<std::mutex> lk(lock);
		
		if (closeDelay != 0) {
			//wait some
			if (OPA_Queue_is_empty(&opaQueue)) {
				if (waitOnEmpty) {
					hasLocalWait++;
					cond.wait(lk);
					hasLocalWait--;
				}
			}
			
			//if not empty
			if (OPA_Queue_is_empty(&opaQueue) == false) {
				QueueEntry * entry;
				OPA_Queue_dequeue(&opaQueue, entry, QueueEntry, queueHeader);
				OPA_decr_int(&count);
				data = entry->value;
				delete entry;
				if (closeDelay > 0)
					closeDelay--;
				//assert(queue.size() == closeDelay || closeDelay == -1);
				if (closeDelay == 0)
					needNotify = true;
				ret = true;
			}
		}
	} else {
		if (closeDelay != 0) {
			//wait some
			if (OPA_Queue_is_empty(&opaQueue)) {
				if (waitOnEmpty) {
					std::unique_lock<std::mutex> lk(lock);
					hasLocalWait++;
					cond.wait(lk);
					hasLocalWait--;
				}
			}
			
			//if not empty
			if (OPA_Queue_is_empty(&opaQueue) == false) {
				QueueEntry * entry;
				OPA_Queue_dequeue(&opaQueue, entry, QueueEntry, queueHeader);
				OPA_decr_int(&count);
				data = entry->value;
				delete entry;
				if (closeDelay > 0)
					closeDelay--;
				//assert(queue.size() == closeDelay || closeDelay == -1);
				if (closeDelay == 0)
					needNotify = true;
				ret = true;
			}
		}
	}
	
	if (needNotify)
		notifyClose();
	return ret;
}

/*******************  FUNCTION  *********************/
template <class T>
bool GoChannel<T>::emptyLock()
{
	//check
	assume(locked == false,"Invalid lock status, must be unlocked");
	
	//lock
	if (!uniqReader)
		lock.lock();
	locked = true;
	
	//check
	//assert(closeDelay == -1 || queue.size() == closeDelay);
	if (OPA_Queue_is_empty(&opaQueue)) {
		//unlock
		locked = false;
		if (!uniqReader)
			lock.unlock();
		return true;
	} else {
		//keep lock
		return false;
	}
}

/*******************  FUNCTION  *********************/
template <class T>
bool GoChannel<T>::emptyTryLock(bool & retry)
{
	//check
	assume(locked == false,"Invalid lock status, must be unlocked");
	
	//lock
	if (!uniqReader)
	{
		if (lock.try_lock() == false) {
			retry = true;
			return true;
		}
	}
	
	//mark as locked
	locked = true;
	
	//check
	//assert(closeDelay == -1 || queue.size() == closeDelay);
	if (OPA_Queue_is_empty(&opaQueue)) {
		//unlock
		locked = false;
		if (!uniqReader)
			lock.unlock();
		return true;
	} else {
		//keep lock
		return false;
	}
}

/*******************  FUNCTION  *********************/
template <class T>
bool GoChannel<T>::popUnlock(T & data)
{
	//check
	assume(locked == true,"Invalid lock status, must be locked by previous call to emptyLock or emptyTryLock");
	bool ret = true;
	bool needNotify = false;
	
	//closed
	if (closeDelay == 0) {
		ret = false;
	} else {
		QueueEntry * entry;
		OPA_Queue_dequeue(&opaQueue, entry, QueueEntry, queueHeader);
		OPA_decr_int(&count);
		data = entry->value;
		delete entry;
		if (closeDelay != -1)
			closeDelay--;
		assert(OPA_load_acquire_int(&count) == closeDelay || closeDelay == -1);
		if (closeDelay == 0)
			needNotify = true;
	}
	
	//unlock
	locked = false;
	if (!uniqReader)
		lock.unlock();
	
	if (needNotify) {
		if (hasLocalWait)
			cond.notify_all();
		notifyClose();
	}
	
	return ret;
}

/*******************  FUNCTION  *********************/
template <class T>
void GoChannel<T>::popUnlock()
{
	//check
	assume(locked == true,"Invalid lock status, must be locked by previous call to emptyLock or emptyTryLock");
	bool needNotify = false;
	
	//closed
	if (closeDelay == 0) {
		//nothing
	} else {
		delete unlockedEntry;
		unlockedEntry = nullptr;
		if (closeDelay != -1)
			closeDelay--;
		assert(OPA_load_acquire_int(&count) == closeDelay || closeDelay == -1);
		if (closeDelay == 0)
			needNotify = true;
	}
	
	//unlock
	locked = false;
	if (!uniqReader)
		lock.unlock();
	
	if (needNotify) {
		if (hasLocalWait)
			cond.notify_all();
		notifyClose();
	}
}

/*******************  FUNCTION  *********************/
template <class T>
T & GoChannel<T>::operator*()
{
	assume(locked,"You must take the lock before using * operator and manually pop after !");
	assume(OPA_Queue_is_empty(&opaQueue) == false,"Cannot be used on empty GoChannel, you need to check before !");
	if (unlockedEntry == nullptr)
	{
		OPA_Queue_dequeue(&opaQueue, unlockedEntry, QueueEntry, queueHeader);
		OPA_decr_int(&count);
	}
	return unlockedEntry->value;
}

/*******************  FUNCTION  *********************/
template <class T>
void GoChannel<T>::close()
{
	bool needNotify = false;
	
	{
		std::lock_guard<std::mutex> guard(lock);
		
		this->closeDelay = OPA_load_acquire_int(&count);
		if (this->closeDelay == 0)
			needNotify = true;
	}
	
	if (needNotify) {
		if (hasLocalWait)
			cond.notify_all();
		notifyClose();
	}
}

/*******************  FUNCTION  *********************/
template <class T>
bool GoChannel<T>::empty()
{
	std::lock_guard<std::mutex> guard(lock);
	return OPA_Queue_is_empty(&opaQueue);
}

/*******************  FUNCTION  *********************/
template <class T>
void GoChannel<T>::disableWaitOnEmpty()
{
	assert(hasLocalWait == 0);
	this->waitOnEmpty = false;
}

/*******************  FUNCTION  *********************/
template <class T>
bool operator << (T & data, GoChannel<T> & channel)
{
	if (channel.locked)
		return channel.popUnlock(data);
	else
		return channel.pop(data);
}

/*******************  FUNCTION  *********************/
template <class T>
bool operator >> (GoChannel<T> & channel,T & data)
{
	if (channel.locked)
		return channel.popUnlock(data);
	else
		return channel.pop(data);
}

/*******************  FUNCTION  *********************/
template <class T>
void operator << (GoChannel<T> & channel,const T & data)
{
	channel.push(data);
}

/*******************  FUNCTION  *********************/
template <class T>
void operator >> (const T & data,GoChannel<T> & channel)
{
	channel.push(data);
}

}

#endif //DAQ_GO_CHANN_IMPL_HPP
