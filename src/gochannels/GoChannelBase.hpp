/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_GO_CHANN_BASE_HPP
#define DAQ_GO_CHANN_BASE_HPP

/********************  HEADERS  *********************/
#include <vector>
#include <portability/Spinlock.hpp>

/********************  NAMESPACE  *******************/
namespace DAQ 
{

/*********************  TYPES  **********************/
class GoChannelSelector;

/*********************  CLASS  **********************/
class GoChannelBase
{
	public:
		GoChannelBase();
		~GoChannelBase();
		void notifyClose();
		void notify();
		void subscibe(GoChannelSelector * selector);
		void unsubscibe(GoChannelSelector * selector);
		virtual bool empty() = 0;
		virtual bool emptyLock() = 0;
		virtual bool emptyTryLock(bool & retry) = 0;
		virtual void setUniqReader() = 0;
	private:
		GoChannelSelector * selector;
		Spinlock selLock;
};

}

#endif //DAQ_GO_CHANN_BASE_HPP
