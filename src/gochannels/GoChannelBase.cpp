/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include <cstdlib>
#include <cassert>
#include <algorithm>
#include "GoChannelSelector.hpp"
#include "GoChannelBase.hpp"
#include <common/Debug.hpp>
#include "common/TakeLock.hpp"

/********************  NAMESPACE  *******************/
namespace DAQ 
{

/*******************  FUNCTION  *********************/
GoChannelBase::GoChannelBase ()
{
	this->selector = nullptr;
}

/*******************  FUNCTION  *********************/
GoChannelBase::~GoChannelBase ()
{
	DAQ_START_CRITICAL(selLock)
		if (selector != nullptr)
			selector->unsubscibe(this);
	DAQ_END_CRITICAL
}

/*******************  FUNCTION  *********************/
void GoChannelBase::subscibe ( GoChannelSelector* selector )
{
	DAQ_START_CRITICAL(selLock)
		assert(selector != nullptr);
		assert(this->selector == nullptr);
		this->selector = selector;
	DAQ_END_CRITICAL
}

/*******************  FUNCTION  *********************/
void GoChannelBase::unsubscibe ( GoChannelSelector* selector )
{
	DAQ_START_CRITICAL(selLock)
		assert(selector == this->selector);
		this->selector = nullptr;
	DAQ_END_CRITICAL
}

/*******************  FUNCTION  *********************/
void GoChannelBase::notify ()
{
	DAQ_START_CRITICAL(selLock)
		if (selector != nullptr)
			selector->notify(this);
	DAQ_END_CRITICAL
}

/*******************  FUNCTION  *********************/
void GoChannelBase::notifyClose ()
{
	DAQ_START_CRITICAL(selLock)
		if (selector != nullptr) {
			selector->unsubscibe(this);
			this->selector = nullptr;
		}
	DAQ_END_CRITICAL
}

}
