/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_GO_CHANN_SELECTOR_HPP
#define DAQ_GO_CHANN_SELECTOR_HPP

/********************  HEADERS  *********************/
#include <vector>
#include <mutex>
#include <condition_variable>
#include <functional>

/********************  NAMESPACE  *******************/
namespace DAQ 
{

/*********************  TYPES  **********************/
class GoChannelBase;

/********************  STRUCT  **********************/
struct ChannelSelectorEntry 
{
	GoChannelBase * channel;
	std::function<bool()> acceptor;
};

/*********************  TYPES  **********************/
typedef std::vector<ChannelSelectorEntry> GoChannelBaseVector;

/*********************  CLASS  **********************/
class GoChannelSelector
{
	public:
		GoChannelSelector();
		~GoChannelSelector();
		void subscibe(GoChannelBase * channel);
		void subscibe(GoChannelBase * channel,std::function<bool()> acceptor);
		void unsubscibe(GoChannelBase * channel);
		GoChannelBase * select();
		template <class T> bool select(T & v);
		bool notify(GoChannelBase * base);
		void disableWaitOnEmpty();
	private:
		GoChannelBaseVector channels;
		int waiting;
		std::mutex lock;
		std::condition_variable cond;
		bool waitOnEmpty;
};

template <class T>
bool GoChannelSelector::select(T & v)
{
	v = dynamic_cast<T>(this->select());
	return v != nullptr;
}

}

#endif //DAQ_GO_CHANN_SELECTOR_HPP
