######################################################
# - Try to find IDT RapidIO libraries
# Once done this will define
#  RAPIDIO_FOUND - System has RapidIO installed
#  RAPIDIO_INCLUDE_DIRS - The RapidIO include directories
#  RAPIDIO_LIBRARIES - The libraries needed to use RapidIO
#  RAPIDIO_DEFINITIONS - Compiler switches required for using RapidIO
#
#  NOTE: After an update of the user space libraries (RRMAP) there are
#	 libraries outside of /opt/rapidio/rapidio_sw/common that are
#	 needed, therefore the prefix has to be /opt/rapidio/rapidio_sw
######################################################
set(RAPIDIO_PREFIX "" CACHE STRING "Help cmake to find IDT RapidIO libraries in your system.")

message(STATUS "${RAPIDIO_PREFIX}")

# Common include dir and lib
######################################################
find_path(RAPIDIO_COMMON_INCLUDE_DIR rapidio_mport_sock.h
	HINTS ${RAPIDIO_PREFIX}/common/include)

message(STATUS "${RAPIDIO_COMMON_INCLUDE_DIR}")
######################################################
find_library(RAPIDIO_COMMON_LIBRARY NAMES mport
	HINTS ${RAPIDIO_PREFIX}/common/libs_so ${RAPIDIO_PREFIX}/common/libs_a)

# Specific for Fabric Management Daemon parts (outside of common...)
######################################################
find_path(RAPIDIO_CT_INCLUDE_DIR ct.h
	HINTS ${RAPIDIO_PREFIX}/fabric_management/libct/inc)
find_path(RAPIDIO_DID_INCLUDE_DIR did.h
	HINTS ${RAPIDIO_PREFIX}/fabric_management/libdid/inc)
find_path(RAPIDIO_RIO_INCLUDE_DIR rio_ecosystem.h
	HINTS ${RAPIDIO_PREFIX}/fabric_management/librio/inc)


message(STATUS "${RAPIDIO_CT_INCLUDE_DIR}")
message(STATUS "${RAPIDIO_DID_INCLUDE_DIR}")
######################################################
find_library(RAPIDIO_CT_LIBRARY NAMES ct
	HINTS ${RAPIDIO_PREFIX}/fabric_management/libct)
find_library(RAPIDIO_DID_LIBRARY NAMES did
	HINTS ${RAPIDIO_PREFIX}/fabric_management/libdid)
find_library(RAPIDIO_RIO_LIBRARY NAMES rio
	HINTS ${RAPIDIO_PREFIX}/fabric_management/librio)


######################################################
set(RAPIDIO_INCLUDE_DIRS ${RAPIDIO_COMMON_INCLUDE_DIR} ${RAPIDIO_CT_INCLUDE_DIR} ${RAPIDIO_DID_INCLUDE_DIR} ${RAPIDIO_RIO_INCLUDE_DIR})
set(RAPIDIO_LIBRARIES ${RAPIDIO_COMMON_LIBRARY} ${RAPIDIO_CT_LIBRARY} ${RAPIDIO_DID_LIBRARY} ${RAPIDIO_RIO_LIBRARY})

message(STATUS "${RAPIDIO_INCLUDE_DIRS}")
message(STATUS "${RAPIDIO_LIBRARIES}")

######################################################
include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set RAPIDIO_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(rapidio  DEFAULT_MSG
	RAPIDIO_LIBRARY RAPIDIO_INCLUDE_DIR)

######################################################
mark_as_advanced(RAPIDIO_INCLUDE_DIR RAPIDIO_LIBRARY )
