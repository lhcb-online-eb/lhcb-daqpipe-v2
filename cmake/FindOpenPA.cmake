######################################################
# - Try to find openpa (https://trac.mpich.org/projects/openpa/wiki)
# Once done this will define
#  OPENPA_FOUND - System has openpa
#  OPENPA_INCLUDE_DIRS - The openpa include directories
#  OPENPA_LIBRARIES - The libraries needed to use openpa
#  OPENPA_DEFINITIONS - Compiler switches required for using openpa

######################################################
set(OPENPA_PREFIX "" CACHE STRING "Help cmake to find openpa library (https://trac.mpich.org/projects/openpa/wiki) into your system.")

######################################################
find_path(OPENPA_INCLUDE_DIR opa_queue.h
	HINTS ${OPENPA_PREFIX}/include)

######################################################
find_library(OPENPA_LIBRARY NAMES opa
	HINTS ${OPENPA_PREFIX}/lib)

######################################################
set(OPENPA_LIBRARIES ${OPENPA_LIBRARY} )
set(OPENPA_INCLUDE_DIRS ${OPENPA_INCLUDE_DIR} )

######################################################
include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set OPENPA_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(OpenPA  DEFAULT_MSG
	OPENPA_LIBRARY OPENPA_INCLUDE_DIR)

######################################################
mark_as_advanced(OPENPA_INCLUDE_DIR OPENPA_LIBRARY )