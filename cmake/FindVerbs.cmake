######################################################
# - Try to find Intel OPA PSM libraries
# Once done this will define
#  VERBS_FOUND - System has RapidIO installed
#  VERBS_INCLUDE_DIRS - The RapidIO include directories
#  VERBS_LIBRARIES - The libraries needed to use RapidIO
#  VERBS_DEFINITIONS - Compiler switches required for using RapidIO

######################################################
set(VERBS_PREFIX "/usr/lib64/" CACHE STRING "Help cmake to find Verbs InfiniBand libraries in your system.")

message(STATUS "${VERBS_PREFIX}")

######################################################
find_path(VERBS_INCLUDE_DIR verbs.h 
	PATH_SUFFIXES "infiniband"
	HINTS ${VERBS_PREFIX}/include
)

find_path(RDMA_INCLUDE_DIR rdma_cma.h 
	PATH_SUFFIXES "rdma"
	HINTS ${VERBS_PREFIX}/include	
)

message(STATUS "${VERBS_INCLUDE_DIR}")
######################################################
find_library(VERBS_LIBRARY_RDMACM NAMES rdmacm)
find_library(VERBS_LIBRARY_VERBS NAMES ibverbs)
set(VERBS_LIBRARY ${VERBS_LIBRARY_RDMACM} ${VERBS_LIBRARY_VERBS})

######################################################
set(VERBS_LIBRARIES ${VERBS_LIBRARY} )
set(VERBS_INCLUDE_DIRS ${VERBS_INCLUDE_DIR} )

######################################################
include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set VERBS_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(verbs  DEFAULT_MSG
	VERBS_LIBRARY VERBS_INCLUDE_DIR RDMA_INCLUDE_DIR)

######################################################
mark_as_advanced(VERBS_INCLUDE_DIR RDMA_INCLUDE_DIR VERBS_LIBRARY )
