######################################################
# - Try to find Intel OPA PSM libraries
# Once done this will define
#  PSM2_FOUND - System has RapidIO installed
#  PSM2_INCLUDE_DIRS - The RapidIO include directories
#  PSM2_LIBRARIES - The libraries needed to use RapidIO
#  PSM2_DEFINITIONS - Compiler switches required for using RapidIO

######################################################
set(PSM2_PREFIX "" CACHE STRING "Help cmake to find IDT RapidIO libraries in your system.")

message(STATUS "${PSM2_PREFIX}")

######################################################
find_path(PSM2_INCLUDE_DIR psm2.h
	HINTS ${PSM2_PREFIX}/include)

message(STATUS "${PSM2_INCLUDE_DIR}")
######################################################
find_library(PSM2_LIBRARY NAMES psm2)

######################################################
set(PSM2_LIBRARIES ${PSM2_LIBRARY} )
set(PSM2_INCLUDE_DIRS ${PSM2_INCLUDE_DIR} )

######################################################
include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set PSM2_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(psm2  DEFAULT_MSG
	PSM2_LIBRARY PSM2_INCLUDE_DIR)

######################################################
mark_as_advanced(PSM2_INCLUDE_DIR PSM2_LIBRARY )
