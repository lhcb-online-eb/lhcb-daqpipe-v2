######################################################
# - Try to find jsoncpp (https://github.com/open-source-parsers/jsoncpp/)
# Once done this will define
#  JSON_CPP_FOUND - System has jsoncpp
#  JSON_CPP_INCLUDE_DIRS - The jsoncpp include directories
#  JSON_CPP_LIBRARIES - The libraries needed to use jsoncpp
#  JSON_CPP_DEFINITIONS - Compiler switches required for using jsoncpp

######################################################
set(JSON_CPP_PREFIX "" CACHE STRING "Help cmake to find JsonCPP library (https://github.com/open-source-parsers/jsoncpp) into your system.")

######################################################
find_path(JSON_CPP_INCLUDE_DIR json/json.h
	HINTS ${JSON_CPP_PREFIX}/include
	PATH_SUFFIXES jsoncpp)

######################################################
find_library(JSON_CPP_LIBRARY NAMES jsoncpp
	HINTS ${JSON_CPP_PREFIX}/lib)

######################################################
set(JSON_CPP_LIBRARIES ${JSON_CPP_LIBRARY} )
set(JSON_CPP_INCLUDE_DIRS ${JSON_CPP_INCLUDE_DIR} )

######################################################
include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set JSON_CPP_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(jsoncpp  DEFAULT_MSG
	JSON_CPP_LIBRARY JSON_CPP_INCLUDE_DIR)

######################################################
mark_as_advanced(JSON_CPP_INCLUDE_DIR JSON_CPP_LIBRARY )