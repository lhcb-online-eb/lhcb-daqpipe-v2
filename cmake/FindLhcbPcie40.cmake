######################################################
# - Try to find LHCb PCIE40 libraries
# Once done this will define
#  LHCB_PCIE40_FOUND - System has LHCb PCIE40 installed
#  LHCB_PCIE40_INCLUDE_DIRS - The LHCb PCIE40 include directories
#  LHCB_PCIE40_LIBRARIES - The libraries needed to use LHCb PCIE40
#  LHCB_PCIE40_DEFINITIONS - Compiler switches required for using LHCb PCIE40

######################################################
set(LHCB_PCIE40_PREFIX "/usr/" CACHE STRING "Help cmake to find PCIe40 libraries in your system.")

message(STATUS "${LHCB_PCIE40_PREFIX}")

######################################################
find_path(LHCB_PCIE40_INCLUDE_DIR lhcb/pcie40/daq.h
	HINTS ${LHCB_PCIE40_PREFIX}/include
)

message(STATUS "${LHCB_PCIE40_INCLUDE_DIR}")

######################################################
find_library(LHCB_PCIE40_LIBRARY NAMES pcie40_daq
             HINTS ${LHCB_PCIE40_PREFIX}/lib64)

######################################################
set(LHCB_PCIE40_LIBRARIES ${LHCB_PCIE40_LIBRARY} )
set(LHCB_PCIE40_INCLUDE_DIRS ${LHCB_PCIE40_INCLUDE_DIR} )

######################################################
include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set LHCB_PCIE40_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(LHCB_PCIE40  DEFAULT_MSG
	LHCB_PCIE40_LIBRARY LHCB_PCIE40_INCLUDE_DIR)

######################################################
mark_as_advanced(LHCB_PCIE40_INCLUDE_DIR LHCB_PCIE40_LIBRARY )
