Change log
==========

master
------

 * Every config entry can be setup as per rank value if setup as array.
 * Add random distribution of real sized instead of grouped approach.
 * Support of hardware rails switch in config file for the PCIe40.

2.4.0 - 21/12/2017
------------------

 * Decouple credit and storage cells in builder unit to send to filter without blocking
 * Many patches in RapidIO driver, mainly suppress multi-segment mode
 * Fix crash in PUSH mode
 * C++11 improvement from Matteo
 * Provide new daqpipe-sync micro benchmark
 * Implement bandwidth imbalance support to be like final system.
 * Optionally report global bandwidth (testing.profile).
 * Add backpressure handling (trash data) into builder unit.
 * Add TCP filter unit benchmark outside of daqpipe and plus FilterUnitTCP in.
 * Add BU buffer usage in htopml plugin
 * Add event dropped chart as htopml plugin

2.3.0 - 26/05/2017
------------------

 * Implementation of UDP driver
 * Valid support of AMC40 in daqpipe
 * Provide a scan script for daqpipe
 * Add shift option to one-to-one benchmark
 * Add some micro-benchmarks with timings
 * Adding one-to-many micro benchmark
 * Add a network topology viewer
 * Aligned driver allocations on huge pages
 * Ubenchmarks now take in account local communications
 * Implementation of a minimal verison of daqpipe in MPI (mini-daqpipe)
 * Run no units on the node hosting the event manager
 * Bandwidth Gb/s is computed with 1000 instead of 1024
 * Improve doxygen coverage in code.

2.2.0 - 02/12/2016
------------------

 * Bugfix bandwidth measurement when using multiple processes on same node.
 * Add support of monitor to extract some data for external analysis
 * Add an internal launcher to also be able to run without hydra
 * Add IB/verbs driver thanks to Rafal
 * Add htopml pludings thanks to Patricia
 * Add timings
 * New all-to-all & mpiTimings micro benchmarks

2.1.0 - 15/09/2016
------------------

 * Validate portability on MacOSX (thanks to Matteo)
 * Support of custom topology from config file
 * Suppport for rapidio networks
 * Support for multiple OPA cards per node (but use of only one per process)
 * Basic support of filter farm
 * Support dumping data to file in filter unit.
 * Support of AMC40 input.
 * Support of PSM2 driver
 * Support of RapidIO driver
 * Support for multiple card per node with libfabric.

2.0.0 - 10/06/2016
------------------

 * First version since re-implementation
 * Provide EventManager, BuilderUnit and ReadoutUnit
 * Multi communication scheduling
 * Multiple drivers : TCP, Libfabric and MPI
 * Basic profiling with htopml
 * Benchmarking script
