/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  GLOBALS  *********************/
var child_process = require('child_process');
var fs = require('fs');
var clone = require('clone');
var util = require('util');
var Job = require('./Job');

/*********************  CLASS  **********************/
function JobDaqpipeV1(config,sharedData,mode,param,value)
{
	//locals
	this.mode = mode;
	this.param = param;
	this.value = value;
	
	var runCfg = clone(config.daqpipe.default.v1);
	runCfg[param] = value;
	
	//job
	this.jobInit(config,{
		title: "lhcb-daqpipe-v1 " + mode + " " + param + " " + value,
		chartTitle: "lhcb-daqpipe-v1 " + mode + " " + param,
		ofile: "/lhcb-daqpipe-v1/"+mode+"-std/"+param+"-"+value,
		regexp: /\[MON\]: BUMaster: [a-zA-z0-9-_.]+, t [0-9.]+: EB rate ([0-9]+\.[0-9]+) Gb\/s/,
		ppn: 2,
		nodes: runCfg.nodes,
		wrapper: (mode == 'mpi'?'mpirun':'hydra'),
		cmd: [
			"./lhcb-daqpipe-v1/build-"+mode+"/eb_app",
			"-f",
			runCfg.freq,
			"-s",
			runCfg.size,
			"-c",
			runCfg.credits,
			"-t",
			config.daqpipe.general.time
		]
	},value,sharedData);
}

/*********************  CLASS  **********************/
//inheritance
util.inherits(JobDaqpipeV1, Job);

module.exports = JobDaqpipeV1;