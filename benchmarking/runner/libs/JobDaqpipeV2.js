/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  GLOBALS  *********************/
var child_process = require('child_process');
var fs = require('fs');
var clone = require('clone');
var util = require('util');
var Job = require('./Job');

/*********************  CLASS  **********************/
function JobDaqpipeV2(config,sharedData,mode,variant,param,value)
{
	//locals
	this.mode = mode;
	this.param = param;
	this.variant = variant;
	
	//local config
	var runCfg = clone(config.daqpipe.default.v2);
	runCfg[param] = value;
	
	//ppn
	var ppn = 1;
	if (variant == 'one-way-2')
		ppn = 2;
	
	//cmd
	var cmd = [
		"./lhcb-daqpipe-v2/build-"+this.mode+"/src/daqpipe",
		"-c",
		runCfg.credits,
		"-t",
		config.daqpipe.general.time
	];
	
	//config files
	if (fs.existsSync("./lhcb-daqpipe-v2/benchmarking/runner/configs/config-v2.json"))
		cmd = cmd.concat(["-f","./lhcb-daqpipe-v2/benchmarking/runner/configs/config-v2.json"]);
	if (fs.existsSync("./lhcb-daqpipe-v2/benchmarking/runner/configs/config-v2-"+this.mode+".json"))
		cmd = cmd.concat(["-f","./lhcb-daqpipe-v2/benchmarking/runner/configs/config-v2-"+this.mode+".json"]);
	if (fs.existsSync("./lhcb-daqpipe-v2/benchmarking/runner/configs/config-v2-"+this.variant+".json"))
		cmd = cmd.concat(["-f","./lhcb-daqpipe-v2/benchmarking/runner/configs/config-v2-"+this.variant+".json"]);
	cmd = cmd.concat(["-f","config-specific.json"]);
	
	//job
	this.jobInit(config,{
		title: "lhcb-daqpipe-v2 " + mode + " " + variant + " " + param + " " + value,
		chartTitle: "lhcb-daqpipe-v2 " + mode + " " + variant + " " + param,
		ofile: "/lhcb-daqpipe-v2/"+mode+"-"+variant+"/"+param+"-"+value,
		regexp: /\[[0-9]+\] \[MON\] RU [0-9]+ rates : [0-9.]+ Km\/s , ([0-9.]+) Gb\/s/,
		wrapper: (mode == 'mpi'?'mpirun':'hydra'),
		ppn: ppn,
		nodes: runCfg.nodes,
		cmd: cmd
	},value,sharedData);
}

/*********************  CLASS  **********************/
//inheritance
util.inherits(JobDaqpipeV2, Job);

/*******************  FUNCTION  *********************/
JobDaqpipeV2.prototype.prepareRun = function()
{
	//config
	var config = clone(this.config.daqpipe.default.v1);
	config[this.param] = this.value;
	
	//config file
	configFile = {
		"general": {
			"eventRailMaxDataSize" : config.size * 1024,
			"dataSizeRandomRadio": config.random_ratio
		},
		"drivers": {
			"slots" : {
				"maxOnFly" : config.channels
			}
		},
		"gather": {
			"sched": {
				"parallel": config.parallel
			}
		},
	};
	fs.writeFileSync('config-specific.json',JSON.stringify(configFile));
}

/********************  GLOBALS  *********************/
module.exports = JobDaqpipeV2;
