/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  GLOBALS  *********************/
var child_process = require('child_process');
var fs = require('fs');
var jso = require('json-override');

/*********************  CLASS  **********************/
function Job()
{
}

/*******************  FUNCTION  *********************/
Job.prototype.jobInit = function(config,runConfig,value,sharedData)
{
	//setup run config
	this.runConfig = jso({
		title: "noTitle",
		chartTitle: "noChartTitle",
		ofile: "no-file",
		ppn: 1,
		nodes: 1,
		cmd: ['ls'],
		wrapper: 'mpirun',
		env:{}
	},runConfig,true);
	this.runConfig.regexp = runConfig.regexp;
	
	//store
	this.config = config;
	this.value = value;
	this.localData = {};
	this.dataPoints = [];
	this.output = {
		stdout: "",
		stderr: "",
		merged: "",
		data: sharedData
	};
}

/*******************  FUNCTION  *********************/
Job.prototype.getTitle = function()
{
	return this.runConfig.title;
}

/*******************  FUNCTION  *********************/
Job.prototype.getChartTitle = function()
{
	return this.runConfig.chartTitle;
}

/*******************  FUNCTION  *********************/
Job.prototype.getOutput = function()
{
	return this.output;
}

/*******************  FUNCTION  *********************/
Job.prototype.getChartData = function()
{
	return this.output.data;
}

/*******************  FUNCTION  *********************/
Job.prototype.getLocalData = function()
{
	return this.localData;
}

/*******************  FUNCTION  *********************/
Job.prototype.setLocalData = function(data)
{
	this.localData = data;
}

/*******************  FUNCTION  *********************/
Job.prototype.prepareRun = function()
{
	
}

/*******************  FUNCTION  *********************/
Job.prototype.loadExistingData = function()
{
	var ofile = this.config.runtime.dirname+"/"+this.runConfig.ofile+".txt";
	var content = fs.readFileSync(ofile).toString().split('\n');
	for (var i in content)
		this.onReadLine(content[i]+"\n");
	this.setupData(false);
}

/*******************  FUNCTION  *********************/
Job.prototype.onReadLine = function(data)
{
	this.output.stdout+=data;
	this.output.merged+=data;
	var check = this.runConfig.regexp.exec(data.toString());
	if (check)
		this.dataPoints.push(+check[1]);
}

/*******************  FUNCTION  *********************/
Job.prototype.setupData = function(runned)
{
	if (this.dataPoints.length > 0)
	{
		this.output.data[this.value] = [
			Math.min.apply(null, this.dataPoints),
			this.dataPoints.reduce(function(a, b){return a+b;}) / this.dataPoints.length,
			Math.max.apply(null, this.dataPoints)
		];
		if (runned)
			console.log(("       ✔ "+this.output.data[this.value].join(' , ')).green)
		else
			console.log(("       • "+this.output.data[this.value].join(' , ')).blue)
	}
}

/*******************  FUNCTION  *********************/
Job.prototype.getCmd = function()
{
	var cmd = [
		this.runConfig.nodes,
		this.runConfig.ppn,
		this.config.runtime.hostfile
	];
	cmd = cmd.concat(this.runConfig.cmd);
	return cmd;
}

/*******************  FUNCTION  *********************/
Job.prototype.getCmdString = function()
{
	var cmd = this.getCmd();
	return "./"+this.config.runtime.dirname+"/runner/scripts/"+this.runConfig.wrapper+".sh "+cmd.join(' ');
}

/*******************  FUNCTION  *********************/
Job.prototype.getPipeCmdString = function()
{
	var cmd = this.getCmd();
	var ofile = this.config.runtime.dirname+"/"+this.runConfig.ofile;
	return "./"+this.config.runtime.dirname+"/runner/scripts/"+this.runConfig.wrapper+".sh "+cmd.join(' ')+" | tee "+ofile+".txt";
}

/*******************  FUNCTION  *********************/
Job.prototype.onFinished = function()
{
	//for stream
}

/*******************  FUNCTION  *********************/
Job.prototype.run = function(callback)
{
	//vars
	var self = this;
	var ofile = this.config.runtime.dirname+"/"+this.runConfig.ofile;

	//check if exist
	if (fs.existsSync(ofile+".txt"))
	{
		this.loadExistingData();
		callback("skiped");
		return;
	}
	
	//prepare run (for gen config file in V2)
	this.prepareRun();
	
	//build cmd
	cmd = this.getCmd();
	
	//debug
	if (this.config.runtime.verbose)
		console.log("     => "+this.getCmdString());
	
	//var env
	env = jso(process.env,+this.runConfig.env);

	//run
	var task = child_process.spawn("./"+this.config.runtime.dirname+"/runner/scripts/"+this.runConfig.wrapper+".sh", cmd,{env:env});

	//capture output
	task.stdout.on('data', (data) => {
		self.onReadLine(data);
		if (this.config.runtime.verbose)
			console.log(data.toString().replace(/\n$/,''));
	});

	//capture err
	task.stderr.on('data', (data) => {
		self.output.stderr+=data;
		self.output.merged+=data;
		if (this.config.runtime.verbose)
			console.log(data.toString().replace(/\n$/,''));
	});

	//on finish
	task.on('close', (code) => {
		fs.writeFileSync(ofile+".txt",self.output.merged);
		
		if (self.output.stderr != "")
			fs.writeFileSync(ofile+".err.txt",self.output.stderr);
		
		//console.log(self.output.data);
		self.setupData(true);
		self.onFinished();
		
		if (code == 0)
			callback();
		else
			callback("Execution error");
	});
};

//export
module.exports = Job;
