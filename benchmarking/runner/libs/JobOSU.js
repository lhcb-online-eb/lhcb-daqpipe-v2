/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  GLOBALS  *********************/
var child_process = require('child_process');
var fs = require('fs');
var util = require('util');
var Job = require('./Job');

/*********************  CLASS  **********************/
function JobOSU(config,bibw)
{
	this.bibw = bibw;
	var exec = (bibw?"osu_bibw":"osu_bw");
	this.jobInit(config,{
		title: exec,
		chartTitle: exec,
		ofile: "osu/"+exec,
		regexp: /[0-9]+ +([0-9.]+)/,
		ppn: 1,
		nodes: 2,
		cmd: ["./usr/libexec/osu-micro-benchmarks/mpi/pt2pt/"+exec],
	},1,{});
}

/*********************  CLASS  **********************/
//inheritance
util.inherits(JobOSU, Job);

/********************  GLOBALS  *********************/
module.exports = JobOSU;