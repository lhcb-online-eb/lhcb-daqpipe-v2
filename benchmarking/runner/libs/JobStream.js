/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  GLOBALS  *********************/
var child_process = require('child_process');
var fs = require('fs');
var shell = require('shelljs');
var util = require('util');
var Job = require('./Job');

/*********************  CLASS  **********************/
function JobStream(config,globalData,topo,threads)
{
	//locals
	this.threads = threads;
	this.topo = topo;
	
	//topo
	if (this.topo == null)
		this.topo = this.getTopo();
	
	//exec
	var exec = [ "./usr/bin/stream" ];
	if (this.threads > 1)
		exec = [ "./usr/bin/stream-omp" ];
	if (this.threads <= this.topo.perParent)
		exec = [ "./usr/bin/hwloc-bind",this.topo.mode+":0",exec[0] ];
	
	//job
	this.jobInit(config,{
		title: "Stream "+this.threads,
		chartTitle: "Stream",
		ofile: "stream/"+this.threads,
		regexp: /Copy: +([0-9]+\.[0-9])/,
		ppn: 1,
		nodes: 1,
		cmd: exec,
		env: {'OMP_NUM_THREADS':threads}
	},threads,globalData);
}

/*********************  CLASS  **********************/
//inheritance
util.inherits(JobStream, Job);

/*******************  FUNCTION  *********************/
JobStream.prototype.getTopo = function()
{
	//extract number of threads
	var threads=shell.exec("./usr/bin/stream-omp | grep 'Threads counted' | cut -d ' ' -f 6").output;
	var sockets=shell.exec("./usr/bin/hwloc-ls | grep -c Socket").output;
	var packages=shell.exec("./usr/bin/hwloc-ls | grep -c Package").output;
	var mode;
	var parents;
	
	if (sockets == 0)
	{
		parents = packages;
		mode = "package";
	} else {
		parents = sockets;
		mode = "socket";
	}

	return {
		threads:threads,
		mode:mode,
		perParent:threads/parents
	};
}

/*******************  FUNCTION  *********************/
JobStream.prototype.onFinished = function()
{
	fs.writeFileSync(this.config.runtime.dirname+'/stream/summary.txt',this.genDataFile());
}

/*******************  FUNCTION  *********************/
JobStream.prototype.genDataFile = function()
{
	var out = "#Threads	MemBw\n";
	for (var i in this.output.data)
		out += i+"\t"+this.output.data[i]+"\n";
	return out;
}

module.exports = JobStream;
