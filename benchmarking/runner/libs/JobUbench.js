/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  GLOBALS  *********************/
var child_process = require('child_process');
var fs = require('fs');
var clone = require('clone');
var util = require('util');
var Job = require('./Job');

/*********************  CLASS  **********************/
function JobUbench(config,sharedData,benchmarkName,param,value)
{
	//locals
	this.benchmarkName = benchmarkName;
	this.param = param;
	
	var runCfg = clone(config.ubench.default);
	runCfg[param] = value;

	//job
	this.jobInit(config,{
		title: benchmarkName + " " + param + " " + value,
		chartTitle: benchmarkName + " " + param,
		ofile: "/ubenchmarks/"+benchmarkName+"/"+param+"-"+value,
		regexp: /Rank [0-9]+ as rate : ([0-9]+\.[0-9]+) Gb\/s/,
		ppn: 1,
		nodes: runCfg.nodes,
		cmd: [
			"./lhcb-daqpipe-v2/build-mpi/benchmarking/ubenchmarks/"+this.benchmarkName,
			"-s",
			runCfg.size*1024,
			"-o",
			runCfg.onfly,
			'-r',
			runCfg.repeat,
			'-b',
			runCfg.buffers
		]
	},value,sharedData);
}

/*********************  CLASS  **********************/
//inheritance
util.inherits(JobUbench, Job);

//export
module.exports = JobUbench;