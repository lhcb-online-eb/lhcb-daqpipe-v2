#!/bin/bash
######################################################
#            PROJECT  : lhcb-daqpipe                 #
#            VERSION  : 2.5.0-dev                    #
#            DATE     : 12/2017                      #
#            AUTHOR   : Valat Sébastien - CERN       #
#            LICENSE  : CeCILL-C                     #
######################################################

######################################################
#vars
#DIR=$HOME/lhcb-daqpipe-bench
DIR=$PWD
bold=$(tput bold)
normal=$(tput sgr0)

######################################################
#setup
set -e
set -x

######################################################
#create dir
mkdir -p $DIR

######################################################
function fatal()
{
	echo "$@" 1>&2
	exit 1
}

######################################################
function run()
{
	step=$(echo "$@" | sed -e 's/ /-/g')
	mkdir -p $DIR/steps
	if [ ! -f $DIR/steps/$step ]; then
		eval "$@"
		touch $DIR/steps/$step
	fi
}

######################################################
#clone
function step_clone()
{
	git clone https://gitlab.cern.ch/svalat/lhcb-daqpipe-v2.git
	git clone https://gitlab.cern.ch/dcampora/lhcb-daqpipe.git
}

######################################################
#download deps
function step_down_deps()
{
	mkdir -p $DIR/deps
	cd $DIR/deps
	wget -c http://www.mpich.org/static/downloads/3.1/hydra-3.1.tar.gz
	wget -c https://github.com/ofiwg/libfabric/releases/download/v1.3.0/libfabric-1.3.0.tar.bz2
	wget -c http://ftp.osuosl.org/pub/blfs/conglomeration/cmake/cmake-3.3.2.tar.gz
	wget -c http://mvapich.cse.ohio-state.edu/download/mvapich/osu-micro-benchmarks-5.1.tar.gz
	wget -c http://www.open-mpi.org/software/hwloc/v1.11/downloads/hwloc-1.11.2.tar.bz2
	wget -c http://nodejs.org/dist/v5.3.0/node-v5.3.0-linux-x64.tar.gz
	wget -c http://www.cs.virginia.edu/stream/FTP/Code/stream.c
	wget -c https://github.com/svalat/htopml/archive/master.zip -O htopml-master.zip
	wget -c https://nodejs.org/dist/v5.9.0/node-v5.9.0.tar.gz
}

######################################################
#setup osu
function setup_osu()
{
	cd $DIR/deps
	tar -xvf osu-micro-benchmarks-5.1.tar.gz
	cd osu-micro-benchmarks-5.1
	./configure --prefix=$DIR/usr
	make
	make install
}

######################################################
#seutp node
function setup_node()
{
	cd $DIR/deps
	tar -xvf node-v5.9.0.tar.gz
	cd node-v5.9.0
	./configure --prefix=$DIR/usr
	make -j8
	make install
}

######################################################
#seutp cmake
function setup_cmake()
{
	cd $DIR/deps
	tar -xvf cmake-3.3.2.tar.gz
	cd cmake-3.3.2
	mkdir -p build
	cd build
	../configure --prefix=$DIR/usr
	make -j8
	make install
}

######################################################
#build libfabric
function setup_libfabric()
{
	cd $DIR/deps
	tar -xvf libfabric-1.3.0.tar.bz2
	cd libfabric-1.3.0
	#./autogen.sh
	./configure --prefix=$DIR/usr
	make -j8
	make install
}

######################################################
#build hydra
function setup_hydra()
{
	cd $DIR/deps
	tar -xvf hydra-3.1.tar.gz
	cd hydra-3.1
	#seems not need anymore
	#patch -p1 < ../../lhcb-daqpipe-v2/extern-deps/hydra-3.1.4.patch
	#./autogen.sh
	./configure --prefix=$DIR/usr --program-prefix=hydra-
	make -j8
	make install
}

######################################################
#build hydra
function setup_hwloc()
{
	cd $DIR/deps
	tar -xvf hwloc-1.11.2.tar.bz2
	cd hwloc-1.11.2
	#seems not need anymore
	#patch -p1 < ../../lhcb-daqpipe-v2/extern-deps/hydra-3.1.4.patch
	#./autogen.sh
	./configure --prefix=$DIR/usr
	make -j8
	make install
}

######################################################
#build hydra
function setup_htopml()
{
	cd $DIR/deps
	unzip htopml-master.zip
	cd htopml-master
	#seems not need anymore
	#patch -p1 < ../../lhcb-daqpipe-v2/extern-deps/hydra-3.1.4.patch
	#./autogen.sh
	mkdir -p build
	cd build
	../configure --prefix=$DIR/usr
	make -j8
	make install
}

######################################################
function setup_stream()
{
	cd $DIR/deps
	gcc -O stream.c -o $DIR/usr/bin/stream
	gcc -fopenmp -O stream.c -o $DIR/usr/bin/stream-omp
}

######################################################
#setup env
function setup_env()
{
	#export paths
	export PATH=$DIR/usr/bin:$PATH
	export LD_LIBRARY_PATH=$DIR/usr/lib:$DIR/usr/lib64:$LD_LIBRARY_PATH
}

######################################################
#build daqpipe-v1
function build_daqpipe()
{
	#vars
	version=$1
	mode=$2
	debug=$3
	build=build-${mode}${debug}
	#fix
	if [ "$version" = "-v1" ]; then
		version=''
		libfabric='libfabric'
	else
		libfabric="LIBFABRIC"
	fi
	#options
	case $mode in
		mpi)
			options="-DTRANSPORT=MPI"
			;;
		libfabric)
			options="-DTRANSPORT=$libfabric -DHYDRA_PREFIX=$DIR/usr -DLIBFABRIC_PREFIX=$DIR/usr -DHYDRA_MPIEXEC_NAME=hydra-mpiexec"
			;;
		*)
			fatal "Invalid mode $mode"
			;;
	esac
	#debug
	if [ "$debug" = "-debug" ]; then
		options="$options -DCMAKE_BUILD_TYPE=Debug"
	else
		options="$options -DCMAKE_BUILD_TYPE=Release"
	fi
	#setup
	cd $DIR/lhcb-daqpipe${version}
	mkdir -p $build
	cd $build
	cmake .. $options -DCMAKE_C_COMPILER=gcc -DCMAKE_CXX_COMPILER=g++ -DHTOPML_PREFIX=$DIR/usr
	make -j8
	#make test
}

######################################################
run step_clone
run step_down_deps
run setup_cmake
run setup_libfabric
run setup_hydra
run setup_hwloc
run setup_osu
run setup_stream
run setup_env
run setup_htopml
run setup_node
run build_daqpipe '-v1' mpi -debug
run build_daqpipe '-v1' mpi
run build_daqpipe '-v1' libfabric -debug
run build_daqpipe '-v1' libfabric
run build_daqpipe '-v2' mpi -debug
run build_daqpipe '-v2' mpi
run build_daqpipe '-v2' libfabric -debug
run build_daqpipe '-v2' libfabric

######################################################
set +x

######################################################
#gen run
echo "#!/bin/bash" > ./run.sh
#echo "echo 'Enter a description : '" >> ./run.sh
#echo "read line" >> ./run.sh
echo "source env.sh" >> ./run.sh
echo "./usr/bin/node ./lhcb-daqpipe-v2/benchmarking/runner/index.js -c \$PWD/config.json \"\$@\"" >> ./run.sh
chmod a+x run.sh

######################################################
if [ ! -f config.json ]; then
	cp ./lhcb-daqpipe-v2/benchmarking/runner/config.example.json ./config.json
fi

######################################################
echo
echo "==================================================================="
echo "${bold}Now setup your env to be ready to run : ${normal}"
echo "export PATH=$DIR/usr/bin:$DIR/usr/libexec/osu-micro-benchmarks/mpi/pt2pt:\$PATH"
echo "export LD_LIBRARY_PATH=$DIR/usr/lib:$DIR/usr/lib64:\$LD_LIBRARY_PATH"
echo ""
echo "${bold}You can now run daqpipe V1 : ${normal}"
echo "cd lhcb-daqpipe/build-mpi-debug/ && mpirun -hostfile ~/hosts --map-by ppr:2:node -np 32 ./eb_app -f 1000000"
echo "cd lhcb-daqpipe/build-libfabric-debug/ && hydra-mpiexec -hostfile ~/hosts --ppn 2 -np 32 ./eb_app -f 1000000"
echo ""
echo "${bold}You can now run daqpipe V2 : ${normal}"
echo "cd lhcb-daqpipe-v2/build-mpi-debug/src && mpirun -hostfile ~/hosts --map-by ppr:1:node -np 16 ./daqpipe"
echo "cd lhcb-daqpipe-v2/build-libfabric-debug/src && hydra-mpiexec -hostfile ~/hosts --ppn 2 -np 16 ./daqpipe"
echo ""
echo "${bold}Edit config gile${normal}"
echo "nano ./config.json"
echo ""
echo "${bold}If all is fine you can run the benchsuite : ${normal}"
echo "./run.sh -d \"SHORT RUN DESCRIPTION TEXT\""
echo "===================================================================="

######################################################

echo > env.sh
echo "export PATH=$DIR/usr/bin:$DIR/usr/libexec/osu-micro-benchmarks/mpi/pt2pt:\$PATH" >> env.sh
echo "export LD_LIBRARY_PATH=$DIR/usr/lib:$DIR/usr/lib64:\$LD_LIBRARY_PATH" >> env.sh

