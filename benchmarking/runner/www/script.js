var socket = io();
var rootInfos = {};
var cur = {progress:0};
var active = {progress:0};

socket.on('init', function(infos){
	rootInfos = infos;
	cur = infos;
	active = infos;
    //console.log(infos);
	var p = Math.round(100*infos.progress/infos.count);
	$('#progress')
		.text(p+" %")
		.css('width',p+"%");
	$('#title').text(infos.title);
	$('#chart-title').text(infos.taskTitle);
	$('#cmd').text(infos.cmd);
	$('#console').text(infos.output);
	
	d3.select("#menu").selectAll("li")
		.data(infos.jobs)
			.enter()
				.append("li")
					.append('a')
						.text(function(d) { return d.chartTitle; })
						.on("click", function(d){ 
							moveToId(d.id);
						});
	
	d3.select('#jobs').selectAll('li')
		.data(infos.status)
			.enter()
				.append('li')
					.text(function(d) { return d.value;})
					.attr('class', function(d) { return d.status; })
					.on("click", function(d){ 
						moveToId(d.id);
					});
	
	updateChart(false);
});

function moveToId(id)
{
	$.getJSON( "/ajax/job/"+id, function( data ) {
		console.log(data);
		cur = data;
		updateCurInfo(data,true);
	});
}

function updateCurInfo(infos,all)
{
	var p = Math.round(100*infos.progress/infos.count);
	$('#progress')
		.text(p+" %")
		.css('width',p+"%");
	$('#title').text(infos.title);
	$('#chart-title').text(infos.taskTitle);
	
	if (all)
	{
		$('#cmd').text(infos.cmd);
		$('#console').text(infos.output);
		cur = infos;
	}
	
	var sel = d3.select('#jobs').selectAll('li')
		.data(infos.status)
	
	sel.enter()
			.append('li')
				.text(function(d) { return d.value;})
				.attr('class', function(d) { return d.status; })
				.on("click", function(d){ 
					moveToId(d.id);
				});
	
	sel.exit().remove();
	
	sel.text(function(d) { return d.value;})
		.attr('class', function(d) { return d.status; });
		
	updateChart(false);
}

socket.on('new', function(infos){
	//console.log(infos);
	if (active.chartTitle == cur.chartTitle) 
		updateCurInfo(infos,(active.progress == cur.progress));
	active = infos;
});

function updateConsole()
{
	console.log(cur.progress);
	$.get( "/ajax/console/"+cur.progress, function( data ) {
		$( "#console" ).html( data );
		setTimeout(updateConsole,1000);
	});
}

function updateChart(timeout)
{
	$.get( "/ajax/data/"+cur.progress, function( data ) {
		console.log(data);
		var chart = $('#chart').highcharts();
		var aver = [];
		var minMax = [];
		for (var i in data)
		{
			aver.push([i,data[i][1]]);
			minMax.push([i,data[i][0],data[i][2]]);
		}
		
		chart.series[0].setData(aver);
		chart.series[1].setData(minMax);
		if (timeout)
			setTimeout(updateChart,1000);
	});
}

function createChart()
{
	var ranges = [
		[1246406400000, 14.3, 27.7],
		[1246492800000, 14.5, 27.8],
	],
	averages = [
		[1246406400000, 21.5],
		[1246492800000, 22.1],
	
	];

    $('#chart').highcharts({
		title: {
			text: 'Parameter scan'
		},

		xAxis: {
			title: {
				text: "Value"
			}
		},

		yAxis: {
			title: {
				text: null
			}
		},

		tooltip: {
			crosshairs: true,
			shared: true,
		},

		legend: {
		},

		series: [{
			name: 'Temperature',
			data: averages,
			zIndex: 1,
			marker: {
				fillColor: 'white',
				lineWidth: 2,
				lineColor: Highcharts.getOptions().colors[0]
			}
		}, {
			name: 'Range',
			data: ranges,
			type: 'arearange',
			lineWidth: 0,
			linkedTo: ':previous',
			color: Highcharts.getOptions().colors[0],
			fillOpacity: 0.3,
			zIndex: 0
		}]
	});
}

$( document ).ready(function() {
	createChart();
	updateConsole();
	updateChart(false);
});
