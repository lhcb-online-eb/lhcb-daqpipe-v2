#!/bin/bash

#args
nodes=$1
ppn=$2
hostfile=$3
shift 3

echo $PWD

#cmd
#mpirun -np $(($nodes*$ppn)) --ppn $ppn --hostfile $hostfile "$@"
mpirun -np $(($nodes*$ppn)) --map-by ppr:$ppn:node --hostfile $hostfile "$@" 
exit $?
