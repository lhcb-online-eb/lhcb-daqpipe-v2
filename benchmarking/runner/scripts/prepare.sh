#!/bin/bash

######################################################
#setup root dir
DIR=$1
DIRNAME=$1
DESC="$2"
BENCHDIR="$PWD"
mkdir -p $DIR


######################################################
function saveScript()
{
	#save scripts used
	mkdir -p $DIR/runner
	cp $BENCHDIR/lhcb-daqpipe-v2/benchmarking/runner/* $DIR/runner
	cp -r $BENCHDIR/lhcb-daqpipe-v2/benchmarking/runner/scripts $DIR/runner/
	#cp -r ./lhcb-daqpipe-v2/bechmarking/runner $DIR

	#setup webreport dir
	cp -r ./lhcb-daqpipe-v2/benchmarking/webreport $DIR
}

######################################################
function askNote()
{
	echo "$DESC" > ${DIR}/infos/note.txt
}

######################################################
function export_meta_info()
{
	#date
	date > ${DIR}/infos/date.txt
	#git info
	cd lhcb-daqpipe/
	git rev-parse HEAD > ../${DIR}/infos/v1-git-ref.txt
	git diff > ../${DIR}/infos/v1-git-diff.patch
	#for v2
	cd ../lhcb-daqpipe-v2/
	git rev-parse HEAD > ../${DIR}/infos/v2-git-ref.txt
	git diff > ../${DIR}/infos/v2-git-diff.patch
	cd ..
	#hosts
	cp ./hosts ${DIR}/infos/hosts
	#ifconfig
	ifconfig -a > ${DIR}/infos/ifconfig.txt
	#cpuinfo
	cat /proc/cpuinfo > ${DIR}/infos/cpuinfo.txt
	#pci
	lspci > ${DIR}/infos/lspci.txt
	#topo
	lstopo --of svg > ${DIR}/infos/cpu.svg
}

######################################################
function get_versions()
{
	file=${DIR}/infos/versions.txt
	echo > $file
	fi_info --version >> $file || echo "No libfabric installed"
	echo "Redhat release: $(cat /etc/redhat-release)" >> $file || echo "Failed to find redhat version"
	echo "Kernel: $(uname -r)" >> $file
	echo "OFED: $(ofed_info | head -n 1)" >> $file || echo "No ofed found"
	ofed_info > $DIRNAME/infos/ofed_info.txt || echo "No ofed found"
	echo "gcc: $(gcc --version | head -n 1)" >> $file || echo "No GCC found"
	echo "MPI: $(mpirun --version | head -n 1)" >> $file || echo "No MPI found"
}

######################################################
saveScript
mkdir -p $DIR/infos
askNote
export_meta_info
get_versions
