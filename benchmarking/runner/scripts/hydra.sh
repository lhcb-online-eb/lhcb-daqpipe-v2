#!/bin/bash

#args
nodes=$1
ppn=$2
hostfile=$3
shift 3

#cmd
hydra-mpiexec -np $(($nodes*$ppn)) --ppn $ppn --hostfile $hostfile "$@"
exit $?
