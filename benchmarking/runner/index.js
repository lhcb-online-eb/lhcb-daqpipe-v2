/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  GLOBALS  *********************/
var fs = require('fs');
var os = require('os');
var Args = require('arg-parser');
var Batch = require('batch');
var colors = require('colors');
var dateFormat = require('dateformat');
var child_process = require('child_process');
var async = require('async');
var JobOSU = require('./libs/JobOSU');
var JobStream = require('./libs/JobStream');
var JobUbench = require('./libs/JobUbench');
var JobDaqpipeV1 = require('./libs/JobDaqpipeV1');
var JobDaqpipeV2 = require('./libs/JobDaqpipeV2');
var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

/*******************  FUNCTION  *********************/
function loadConfigFile(file)
{
	if (fs.existsSync(file)) 
	{ 
		var content = fs.readFileSync(file);
		var config = JSON.parse(content);
		return config;
	} else {
		console.error("No config file ~/.homelinux.json, using default values !".red);
		process.abort();
	}
}

/*******************  FUNCTION  *********************/
function checkFileExist(filename,errMsg)
{
	if (!fs.existsSync(filename))
	{
		console.error(errMsg.red);
		process.abort();
	}
}

/*******************  FUNCTION  *********************/
function createDirIfNot(dir)
{
	if (!fs.existsSync(dir))
		fs.mkdirSync(dir);
}

/****************************************************/
//parse args
args = new Args('lhcb-daqpipe-bench-runner', '2.0.0', 'Runnter for the LHCb DAQPIPE benchmark suite');
args.add({ name: 'config', desc: 'Config file', switches: [ '-c', '--config'], value: 'file', required: true });
args.add({ name: 'hostfile', desc: 'Hostfile for MPI/hydra', switches: [ '-H', '--hostfile'], value: 'file' });
args.add({ name: 'desc', desc: 'Run description', switches: [ '-d', '--desc'], value: 'desc' , required: true});
args.add({ name: 'verbose', desc: 'Use verbose mode', switches: [ '-V', '--verbose']});
args.add({ name: 'name', desc: 'Benchmark name', switches: [ '-n', '--name'], value:'name'});

if (!args.parse())
	process.exit(1);

/****************************************************/
//default values
if (args.params.hostfile == undefined)
	args.params.hostfile = process.cwd()+"/hosts";
if (args.params.name == undefined)
	args.params.name = '';

/****************************************************/
//load config
var config = loadConfigFile(args.params.config);
config.runtime = {
	hostfile: args.params.hostfile,
	dirname: "benchmark-"+os.hostname()+args.params.name+"-all-"+dateFormat(new Date(),"isoDate"),
	desc: args.params.desc,
	verbose: args.params.verbose
};

/****************************************************/
//do some checking
checkFileExist("lhcb-daqpipe","You must setup lhcb-daqipe in current directory");
checkFileExist("lhcb-daqpipe/build-mpi","You must setup lhcb-daqipe/build-mpi");
checkFileExist("lhcb-daqpipe/build-libfabric","You must setup lhcb-daqipe/build-libfabric");
checkFileExist("lhcb-daqpipe-v2","You must setup lhcb-daqipe in current directory");
checkFileExist("lhcb-daqpipe-v2/build-mpi","You must setup lhcb-daqipe/build-mpi");
checkFileExist("lhcb-daqpipe-v2/build-libfabric","You must setup lhcb-daqipe/build-libfabric");
checkFileExist(config.runtime.hostfile,"You must setup "+config.runtime.hostfile+" for mpi and hydra or provide one with -H/--hostfile");

/****************************************************/
//info
console.log(("Will use directory : "+config.runtime.dirname).yellow)
child_process.execSync("./lhcb-daqpipe-v2/benchmarking/runner/scripts/prepare.sh "+config.runtime.dirname+" \""+config.runtime.desc+'"');
fs.writeFileSync(config.runtime.dirname+"/infos/config.json",JSON.stringify(config,null,'\t'));

/****************************************************/
var runs = {
	active:null,
	previous:null,
	all: []
};
var jobs = [];

/****************************************************/
//OSU
if (config.benchmarks.osu == true)
{
	createDirIfNot(config.runtime.dirname+"/osu");
	jobs.push(new JobOSU(config,false));
	jobs.push(new JobOSU(config,true));
}

/****************************************************/
//stream
if (config.benchmarks.stream == true)
{
	createDirIfNot(config.runtime.dirname+"/stream");
	var streamGblData = {};
	var stream1 = new JobStream(config,streamGblData,null,1);
	var streamTopo = stream1.getTopo();
	for (var i = 1 ; i < streamTopo.threads ; i++)
		jobs.push(new JobStream(config,streamGblData,streamTopo,i));
}

/****************************************************/
//simple
if (config.benchmarks.ubench == true)
{
	var benchs = ['one-to-one','many-to-one','mpiRoundRobinGather','mpiRoundRobinGatherWithCmd','mpiRoundRobinGatherWithCmdMeta'];
	var params = ['size','onfly','nodes','buffers'];
	createDirIfNot(config.runtime.dirname+"/ubenchmarks");
	for (var i in benchs)
	{
		createDirIfNot(config.runtime.dirname+"/ubenchmarks/"+benchs[i]);
		for (var j in params)
		{
			var benchGblData = {};
			var values  = config.ubench.scan[params[j]];
			for (var k in values)
			{
				jobs.push(new JobUbench(config,benchGblData,benchs[i],params[j],values[k]));
			}
		}
	}
}

/****************************************************/
//v1
if (config.benchmarks.v1 == true)
{
	var params = ['size','credits','freq'];
	var modes = config.daqpipe.scan.modes;
	createDirIfNot(config.runtime.dirname+"/lhcb-daqpipe-v1");
	for (var m in modes )
	{
		createDirIfNot(config.runtime.dirname+"/lhcb-daqpipe-v1/"+modes[m]+"-std");
		for (var j in params)
		{
			var benchGblData = {};
			var values  = config.daqpipe.scan[params[j]];
			for (var k in values)
			{
				jobs.push(new JobDaqpipeV1(config,benchGblData,modes[m],params[j],values[k]));
			}
		}
	}
}

/****************************************************/
//v2
if (config.benchmarks.v2 == true)
{
	var params = ['size','credits','channels','parallel','random_ratio'];
	var modes = config.daqpipe.scan.modes;
	var variants = config.daqpipe.scan.variants;
	createDirIfNot(config.runtime.dirname+"/lhcb-daqpipe-v2");
	for (var m in modes )
	{
		for (var v in variants)
		{
			createDirIfNot(config.runtime.dirname+"/lhcb-daqpipe-v2/"+modes[m]+"-"+variants[v]);
			for (var j in params)
			{
				var benchGblData = {};
				var values  = config.daqpipe.scan[params[j]];
				for (var k in values)
				{
					jobs.push(new JobDaqpipeV2(config,benchGblData,modes[m],variants[v],params[j],values[k]));
				}
			}
		}
	}
}

/****************************************************/
//regs pages
app.use(express.static(__dirname+'/www'));
app.use('/bower', express.static(__dirname + '/bower_components'));

app.get('/ajax/console/:id', function (req, res) {
	res.send(jobs[req.params.id].getOutput().merged);
});

app.get('/ajax/cmd/:id', function (req, res) {
	res.send(jobs[req.params.id].getCmdString());
});

app.get('/ajax/data/:id', function (req, res) {
	res.send(jobs[req.params.id].getChartData());
});

app.get('/ajax/job/:id', function (req, res) {
	var job = jobs[req.params.id];
	res.send(JSON.stringify({
		cmd: job.getCmdString(),
		progress: req.params.id,
		count: job.length,
		title: job.getChartTitle(),
		taskTitle: job.getTitle(),
		output: job.getOutput().merged,
		status: getJobStatus(job.getChartTitle())
	}));
});

/****************************************************/
//setup web interface
http.listen(8080, "localhost", function(){
	console.log('Listening on *:8080'.yellow);
});

/****************************************************/
function getJobList()
{
	var out = [];
	
	var lst = {};
	for (var i in jobs)
		lst[jobs[i].getChartTitle()] = i;
	
	for (var i in lst)
		out.push({id:lst[i],chartTitle:i});

	return out;
}

/****************************************************/
function getJobStatus(chartTitle)
{
	var out = [];
	for (var i in jobs)
	{
		if (jobs[i].getChartTitle() == chartTitle)
		{
			out.push({
				title:jobs[i].getTitle(),
				chartTitle: jobs[i].getChartTitle(),
				id:i,
				status:jobs[i].status,
				value:jobs[i].value
			});
		}
	}
	
	return out;
}

/****************************************************/
//socket io
var cnt = -1;
io.on('connection', function(socket){
	socket.emit('init', {
		title: runs.active.getChartTitle(),
		taskTitle: runs.active.getTitle(),
		cmd: runs.active.getCmdString(),
		progress: cnt,
		count: jobs.length,
		jobs: getJobList(),
		output: runs.active.getOutput().merged,
		status: getJobStatus(runs.active.getChartTitle())
	});
});

/****************************************************/
var out = "#!/bin/bash\n";
for(var i in jobs)
{
	out += jobs[i].getPipeCmdString() + "\n";
}
fs.writeFileSync("cmd.sh",out);

/****************************************************/
async.eachSeries(jobs,function(job,callback) {
	//incr
	cnt++;
	//reg
	runs.previous = runs.active;
	runs.active = job;
	runs.all.push(job);
	//before run
	console.log((" ➤ Start job "+cnt + "/"+jobs.length+" "+job.getTitle()).magenta);
	//emit
	job.status = 'running';
	io.emit('new',{
		cmd: runs.active.getCmdString(),
		progress: cnt,
		count: jobs.length,
		title: runs.active.getChartTitle(),
		taskTitle: runs.active.getTitle(),
		output: runs.active.getOutput().merged,
		status: getJobStatus(runs.active.getChartTitle())
	});
	//run
	job.run(function(err) {
		if (err == "skiped") {
			console.log(("       • Already done").blue);
			job.status = 'runned';
		} else if (err) {
			console.log(("       ✘ Error : "+err).red);
			job.status = 'error';
		} else {
			console.log(("       ✔ Finished").green);
			job.status = 'success';
		}
		callback();
	});
},function(err) {
	if (err) {
		console.error(("Job as error : "+err).red);
	} else {
		console.log("Job finished".yellow);
	}
});
