# -*- coding: UTF-8 -*-
import os
from rio_plot_common import *

log_dir = 'daqpipe-normal-logs'
logs = os.listdir(log_dir)
plt.rcParams.update({'font.size': 26})

# TODO a common file for both python scripts?
# Right now this is hardcoded and assumed to be correct between the prep script
# and the plot script
eventRailMaxDataSize = [
		('64k', 64*1024), 
		('128k', 128*1024), 
		('256k', 256*1024), 
		('512k', 512*1024), 
		('680k', 680*1024), 
		('1M', 1024*1024), 
		]
parallell = [
		('p1', 1), 
		('p2', 2), 
		('p3', 3), 
		('p4', 4), 
		('p5', 5), 
		('p6', 6)
		]
credits = [
		("c1",1),
		("c2",2),
		("c3",3),
		("c4",4),
		]


##################################################################
## PLOT FOR VARYING CREDITS
mean_speeds = []
std_dev = []
std_err = []
buf_sizes_labels = []
for t in credits:
	print t
	buf_sizes_labels.append(t[0])
	# last size (1M) is the one we will use here
	ermds = eventRailMaxDataSize[len(eventRailMaxDataSize)-1]

	with open(log_dir + '/normal_' + t[0] + '_' + ermds[0] + '_np3.log') as log:
		lines = log.read().splitlines()
	gather_mean(lines, mean_speeds, std_dev, std_err)

print "VARYING CREDITS"
print "MEAN:", mean_speeds
print "STD DEV: ", std_dev
print "STD ERR:", std_err

if len(mean_speeds) != 0:
	filename = "normal_plot_mean_speed_varying_credits.png"
	fig = plot(credits, buf_sizes_labels, mean_speeds, std_dev, std_err, "Number of credits", "Mean speed per iteration (Gbps)", "2 units", filename, "Varying number of credits (simultaneously handled events)", 'seagreen', 'o', None)
	fig.savefig(filename, bbox_inches = 'tight')
	fig.clf()
else:
	print "WARNING: Lacking values for varying credit sizes!"

##################################################################
## PLOT FOR VARYING SIZES AND VARYING NODES
mean_speeds = []
std_dev = []
std_err = []
bbuf_sizes_labels = []
for t in eventRailMaxDataSize[:-1]:
	print t
	buf_sizes_labels.append(t[0])
	with open(log_dir + '/normal_s' + t[0] + '_c4_np4.log') as log:
		lines = log.read().splitlines()
	gather_mean(lines, mean_speeds, std_dev, std_err)

print "VARYING SIZES AND NODES"
print "MEAN:", mean_speeds
print "STD DEV: ", std_dev
print "STD ERR:", std_err

filename = "normal_plot_mean_speed_varying_sizes_and_nodes.png"
fig = None
if len(mean_speeds) != 0:
	fig = plot(eventRailMaxDataSize[:-1], buf_sizes_labels, mean_speeds, std_dev, std_err, "Buffer size", "Mean speed per iteration (Gbps)", "3 units", filename, "Varying buffer sizes", 'cornflowerblue', 'v', None)
else:
	print "WARNING: Lacking values for varying sizes NP4!"

mean_speeds = []
std_dev = []
std_err = []
bbuf_sizes_labels = []
for t in eventRailMaxDataSize[:-1]:
	print t
	buf_sizes_labels.append(t[0])
	with open(log_dir + '/normal_s' + t[0] + '_c4_np3.log') as log:
		lines = log.read().splitlines()
	gather_mean(lines, mean_speeds, std_dev, std_err)

print "VARYING SIZES AND NODES 2"
print "MEAN:", mean_speeds
print "STD DEV: ", std_dev
print "STD ERR:", std_err

if len(mean_speeds) != 0:
	fig = plot(eventRailMaxDataSize[:-1], buf_sizes_labels, mean_speeds, std_dev, std_err, "Buffer size", "Mean speed per iteration (Gbps)", "2 units", filename, "Varying sizes", 'seagreen', 'o', fig)
	fig.savefig(filename, bbox_inches = 'tight')
	fig.clf()
else:
	print "WARNING: Lacking values for varying sizes NP3!"

##################################################################
## PLOT FOR VARYING PARALLELL
mean_speeds = []
std_dev = []
std_err = []
bbuf_sizes_labels = []
for t in parallell:
	print t
	buf_sizes_labels.append(t[0])
	# last size (2M) is the one we will use here
	ermds = eventRailMaxDataSize[len(eventRailMaxDataSize)-1]

	with open(log_dir + '/' + 'normal_' + t[0] + '_' + ermds[0] + '_c4_np3.log') as log:
		lines = log.read().splitlines()
	gather_mean(lines, mean_speeds, std_dev, std_err)

print "VARYING PARALLEL"
print "MEAN:", mean_speeds
print "STD DEV: ", std_dev
print "STD ERR:", std_err

if len(mean_speeds) != 0:
	filename = "normal_plot_mean_speed_varying_parallell.png"
	fig = plot(parallell, buf_sizes_labels, mean_speeds, std_dev, std_err, "Parallell value", "Mean speed per iteration (Gbps)", "2 units", filename, "Varying parallell parameter (outgoing requests per credit)", 'seagreen', 'o', None)
	fig.savefig(filename, bbox_inches = 'tight')
	fig.clf()
else:
	print "WARNING: Lacking values for varying credit sizes!"

