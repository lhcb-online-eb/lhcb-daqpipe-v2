# -*- coding: UTF-8 -*-
# Ask Séb about -t, what would be good values. A: 3-4 mins would be good
# What is parallell when not set? A: default parallell == 2
# What does parallell do? A: number of outgoing requests per credit

# DAQPIPE Parameters:
#	eventRailMaxDataSize
#		(max size of BU buffer which is a RX buf)
#		eventRailMaxDataSize x units <= 2M
#		units = nodes - 1, more precisely the number of readout units 
#		but in the implementation the number of builder units are the same as the number readout units.
#		-> nodes = 3 -> units == 2 -> eventRailMaxDataSize = 1M
# 		-> nodes = 4 -> units == 3 -> eventRailMaxDataSize = 682*1024 = 698368
#	parallell 
#		(TODO describe this) Do this with only the largest buffer size
#		Number of outgoig requests per credit per builder unit
#
#	credits
#		1-4 OK, >4 -> "Device or resource busy" (too many allocations)
#
#	ruStoredEvents
#		Decides the size of the transmission buffer. How many (parts of events) we should be able to store from
#		the PCIe cards. Not a parameter per se, just needs to be large enough otherwise we don't have enough to feed
#		the builder units:
#		ruStoredEvents >= credits x units
#		Max credits = 4, max units = 3 -> 12
#		We can use the default 128, it is enough
#		Some experimentation on 10GbE shows that 4 x credits x units gives slightly higher results, so
#		this parameter is now calculated for each configuration

#Template configuration file with everything
#{
#	"testing":{
#		"useDriverThreads":true
#	},
#	"general":{
#		"credits": 4,
#		"eventRailMaxDataSize":2097152,
#		"ruStoredEvents":32,
#		"gatherType": "sched"
#	},
#	"gather":{
#		"sched":{
#			"parallell":4
#			}
#		}
#}

import os

g_eventRailMaxDataSize = [
		('64k', 64*1024),	# 0
		('128k', 128*1024),	# 1
		('256k', 256*1024),	# 2
		('512k', 512*1024), 	# 3
		('680k', 680*1024), 	# 4
		('1M', 1024*1024), 	# 5
		('2M', 2*1024*1024), 	# 6
		('4M', 4*1024*1024), 	# 7
		# We run out of memory with 8M on 16 nodes.
		#('8M', 8*1024*1024), 	# 8
		]
g_parallell = [
		('p1', 1), # 0
		('p2', 2), # 2
		('p3', 3), # 3
		('p4', 4), # 4
		('p5', 5), # 5
		('p6', 6), # 6
		('p7', 7), # 7
		('p8', 8), # 8
		('p9', 9), # 9
		('p10', 10), # 10
		('p11', 11), # 11
		('p12', 12), # 12
		('p13', 13), # 13
		('p14', 14), # 14
		('p15', 15), # 15
		('p16', 16), # 16
		]
g_credits = [
		("c1",1), # 0
		("c2",2), # 1
		("c3",3), # 2
		("c4",4), # 3
		]
g_units = 16;
g_config_dir = 'daqpipe-normal-configs'

###############################################
# WHERE EVERYTHING HAPPENS
def main():
	print "Creating configuration files"
	create_config_dir()
	create_config_scan_credits()
	create_config_scan_size()
	create_config_scan_parallell_c4()
	create_config_scan_parallell_c1()
	create_config_scan_parallell_c2()


###############################################
## CREATE CONFIG DIR
def create_config_dir():
	print "Creating config directory"
	global g_config_dir
	if not os.path.exists(g_config_dir):
		    os.mkdir(g_config_dir)

################################################
## CREATE CONFIG FILES: VARYING CREDITS
def create_config_scan_credits():
	print "Creating configs for credits scan"
	global g_eventRailMaxDataSize, g_credits, g_units, g_config_dir
	for c in g_credits:
		# 1M is the one we will use here, default parallel = 2
		ermds = g_eventRailMaxDataSize[5]
		credits = c[1]

		cfg = open(g_config_dir + '/' + 'normal_' + c[0] + '_' + ermds[0] + '_p2.cfg', 'w')
		cfg.write('{\n')
		cfg.write('	"testing":{\n')
		cfg.write('		"useDriverThreads":true\n')
		cfg.write('	},\n')
		cfg.write('	"general":{\n')
		cfg.write('		"credits":' + str(credits) + ',\n')
		cfg.write('		"eventRailMaxDataSize":' + str(ermds[1]) + ',\n')
		cfg.write('		"ruStoredEvents":' + str(4*g_units*credits) +'\n')
		cfg.write('	}\n')
		cfg.write('}\n')
		cfg.close()

##############################################
## CREATE CONFIG FILES: VARYING SIZES
## Default credits = 4, parallel = 2
def create_config_scan_size():
	print "Creating configs for size scan"
	global g_eventRailMaxDataSize, g_units, g_config_dir
	for ermds in g_eventRailMaxDataSize:
		credits = 4
		cfg = open(g_config_dir + '/' + 'normal_s' + ermds[0] + '_c4_p2.cfg', 'w')
		cfg.write('{\n')
		cfg.write('	"testing":{\n')
		cfg.write('		"useDriverThreads":true\n')
		cfg.write('	},\n')
		cfg.write('	"general":{\n')
		cfg.write('		"credits":' + str(credits) + ',\n')
		cfg.write('		"eventRailMaxDataSize":' + str(ermds[1]) + ',\n')
		cfg.write('		"ruStoredEvents":' + str(4 * g_units * credits)+ '\n')
		cfg.write('	}\n')
		cfg.write('}\n')
		cfg.close()

##############################################
## CREATE CONFIG FILES: VARYING PARALLELL PARAMETER
def create_config_scan_parallell_c4():
	print "Creating configs for parallel-c4 scan"
	global g_eventRailMaxDataSize, g_parallell, g_credits, g_units, g_config_dir
	for p in g_parallell:
		# 1M is the one we will use here
		ermds = g_eventRailMaxDataSize[5]
		credits = 4

		cfg = open(g_config_dir + '/' + 'normal_' + p[0] + '_' + ermds[0] + '_c4.cfg', 'w')
		cfg.write('{\n')
		cfg.write('	"testing":{\n')
		cfg.write('		"useDriverThreads":true\n')
		cfg.write('	},\n')
		cfg.write('	"general":{\n')
		cfg.write('		"credits":' + str(credits) + ',\n')
		cfg.write('		"eventRailMaxDataSize":' + str(ermds[1]) + ',\n')
		cfg.write('		"ruStoredEvents":' + str(4 * g_units * credits)+ ',\n')
		cfg.write('		"gatherType":"sched"\n')
		cfg.write('	},\n')
		cfg.write('	"gather":{\n')
		cfg.write('		"sched":{\n')
		cfg.write('			"parallell":' + str(p[1]) + '\n')
		cfg.write('		}\n')
		cfg.write('	}\n')
		cfg.write('}\n')
		cfg.close()

##############################################
## CREATE CONFIG FILES: VARYING PARALLELL PARAMETER WITH CREDIT 1
def create_config_scan_parallell_c1():
	print "Creating configs for parallel-c1 scan"
	global g_eventRailMaxDataSize, g_parallell, g_credits, g_units, g_config_dir
        for p in g_parallell:
                # 1M is the one we will use here
                ermds = g_eventRailMaxDataSize[5]
                credits = 1

                cfg = open(g_config_dir + '/' + 'normal_' + p[0] + '_' + ermds[0] + '_c1.cfg', 'w')
                cfg.write('{\n')
                cfg.write('     "testing":{\n')
                cfg.write('             "useDriverThreads":true\n')
                cfg.write('     },\n')
                cfg.write('     "general":{\n')
                cfg.write('             "runTimeout":120,\n')
                cfg.write('             "credits":' + str(credits) + ',\n')
                cfg.write('             "eventRailMaxDataSize":' + str(ermds[1]) + ',\n')
                cfg.write('             "ruStoredEvents":' + str(4 * g_units * credits)+ ',\n')
                cfg.write('             "gatherType":"sched"\n')
                cfg.write('     },\n')
                cfg.write('     "gather":{\n')
                cfg.write('             "sched":{\n')
                cfg.write('                     "parallell":' + str(p[1]) + '\n')
                cfg.write('             }\n')
                cfg.write('     }\n')
                cfg.write('}\n')
                cfg.close()

##############################################
## CREATE CONFIG FILES: VARYING PARALLELL PARAMETER WITH CREDIT 2
def create_config_scan_parallell_c2():
	print "Creating configs for parallel-c1 scan"
	global g_eventRailMaxDataSize, g_parallell, g_credits, g_units, g_config_dir
        for p in g_parallell:
                # 1M is the one we will use here
                ermds = g_eventRailMaxDataSize[5]
                credits = 2

                cfg = open(g_config_dir + '/' + 'normal_' + p[0] + '_' + ermds[0] + '_c2.cfg', 'w')
                cfg.write('{\n')
                cfg.write('     "testing":{\n')
                cfg.write('             "useDriverThreads":true\n')
                cfg.write('     },\n')
                cfg.write('     "general":{\n')
                cfg.write('             "runTimeout":120,\n')
                cfg.write('             "credits":' + str(credits) + ',\n')
                cfg.write('             "eventRailMaxDataSize":' + str(ermds[1]) + ',\n')
                cfg.write('             "ruStoredEvents":' + str(4 * g_units * credits)+ ',\n')
                cfg.write('             "gatherType":"sched"\n')
                cfg.write('     },\n')
                cfg.write('     "gather":{\n')
                cfg.write('             "sched":{\n')
                cfg.write('                     "parallell":' + str(p[1]) + '\n')
                cfg.write('             }\n')
                cfg.write('     }\n')
                cfg.write('}\n')
                cfg.close()


# Need to have this here to be able to define the main function at the top
# http://stackoverflow.com/questions/3754240/declare-function-at-end-of-file-in-python
if __name__ == '__main__':
	main()

