# -*- coding: UTF-8 -*-
import numpy as np
import matplotlib.pyplot as plt

plt.rcParams.update({'font.size': 26})

# thx kalexopo
def string_between(string, before, after):
#	print string
#	print string.split(before)
#	print string.split(after)
	temp = string.split(before)[1]
	temp2 = temp.split(after)[0]
	# In case we don't find what we're looking for in the splits,
	# signal that nothing was found
	if temp == temp2:
		return ''
	return temp2

def plot(what, buf_sizes_labels, mean_speeds, std_dev, std_err, xlabel, ylabel, label, filename, title, color, marker, fig):
	x = np.array(range(len(what)))
	plt.xticks(x, buf_sizes_labels, rotation = '35')
	if fig is None:
		fig = plt.figure(2, figsize=(15,8))
		fig.suptitle(title)
	ax = fig.add_subplot(111)
	ax.set_xlabel(xlabel)
	ax.set_ylabel(ylabel)
	ax.set_xticks(x, buf_sizes_labels)
	ax.set_ylim([0,11])
	# Used this instead of ax.[x|y]axis.grid() since the grid failed to appear on some plots
	ax.grid(True, which='both')
	plt.xticks(x, buf_sizes_labels, rotation = '35')
	ax.errorbar(x=x, y=mean_speeds, yerr=std_err, elinewidth=2, capthick=2, capsize=8, linewidth='4',linestyle='-', marker=marker, markersize=15, color=color, label=label)
	ax.legend(loc='best', fancybox=True, fontsize='small')
	return fig

def gather_mean(lines, mean_speed, std_dev, std_err):
	speeds = []
	for line in lines:
		if 'rates' in line:
			#print line
			tmp_gb = string_between(line, 'msg/s , ', ' Gb/s').strip()
			# Depending on the speed, DAQPIPE will print output in Gb or Mb
			if tmp_gb == '':
				tmp_gb = float(string_between(line, 'm/s , ', ' Mb/s').strip())/1000
			else:
				tmp_gb = float(tmp_gb)
			speeds.append(tmp_gb)
	mean_speed.append(np.mean(speeds))
	stdev = np.std(speeds, ddof=1) 
	std_dev.append(stdev)
	#From http://www.radford.edu/~biol-web/stats/standarderrorcalc.pdf
	std_err.append(stdev/np.sqrt(len(speeds)))

