#!/bin/bash
CONF_DIR="./daqpipe-normal-configs"
LOG_DIR_BASE="./daqpipe-eth-normal-logs"
timestamp=$(date +"%Y%m%d_%H%M%S")
LOG_DIR="${LOG_DIR_BASE}_${timestamp}"

# Create timestamped log dir
mkdir $LOG_DIR

# Start with generating all the files
python normal_prep.py

# Then run ALL configs in scale
for np in 4 8 16
do
	for cfg in ${CONF_DIR}/*.cfg
	do
		name=${cfg##*/}
		base=${name%.cfg}
		# FLAG --map-by ppr:2:node: http://stackoverflow.com/questions/28216897/syntax-of-the-map-by-option-in-openmpi-mpirun-v1-8
		# This was recommended by Sébastien to use, in combination with testing.oneWay in the configuration file. That way
		# there will be one process for sending and one for receiving on each node.
		# This becomes a problem for RapidIO, since we can't have loop back sockets with RIO CM,
		# so we have to use ONE PROCESS PER NODE
		# FLAG -mca plm_rsh_no_tree_spawn 1: http://www.cfd-online.com/Forums/openfoam-installation/164956-host-key-verification-failed-upgrades-openmpi-openfoam-2-3-1-a.html
		# Needed to disable omnidirectional launching, which requires omnidirectional SSH keys.
		CMD_ETH="mpirun -mca plm_rsh_no_tree_spawn 1 --map-by ppr:1:node -np ${np} -hostfile ~/eth-hosts ../../build-mpi/src/daqpipe -f ${cfg} -t 240 > ${LOG_DIR}/${base}_np${np}.log 2>&1"
		echo "${CMD_ETH}"
		eval "${CMD_ETH}"
	done
done

# Now we're ready for plotting
#python normal_plot.py

