# -*- coding: UTF-8 -*-
# TODO change script to handle log files for -np3 and -np4 in the same directory

import os
from rio_plot_common import *

log_dir_np3 = 'daqpipe-multisegm-logs-np3'
log_dir_np4 = 'daqpipe-multisegm-logs-np4'
logs_np3 = os.listdir(log_dir_np3)
logs_np4 = os.listdir(log_dir_np4)

# TODO a common file for both prep and plot scripts?
# Right now this is hardcoded and assumed to be correct between the prep script
# and the plot script
eventRailMaxDataSize = [
		('64k', 64*1024), 
		('128k', 128*1024), 
		('256k', 256*1024), 
		('512k', 512*1024), 
		('1M', 1024*1024), 
		('2M', 2*1024*1024)
		]
parallell = [
		('p1', 1), 
		('p2', 2), 
		('p3', 3), 
		('p4', 4), 
		('p5', 5), 
		('p6', 6)
		]

##################################################################
## PLOT FOR VARYING SIZES
filename = "multisegm_plot_mean_speed_varying_size_cmp.png"

## First for -np3
mean_speeds = []
std_dev = []
std_err = []
buf_sizes_labels = []
for t in eventRailMaxDataSize:
	#print t
	buf_sizes_labels.append(t[0])
	with open(log_dir_np3 + '/multisegm_' + t[0] + '.log') as log:
		lines = log.read().splitlines()
	gather_mean(lines, mean_speeds, std_dev, std_err)

print "VARYING SIZES -np3"
print "MEAN:", mean_speeds
print "STD DEV: ", std_dev
print "STD ERR:", std_err

# plot -np3
fig = plot(eventRailMaxDataSize, buf_sizes_labels, mean_speeds, std_dev, std_err, "Buffer size", "Mean speed per iteration (Gbps)", "2 units", filename, "Multisegment DAQPIPE, varying sizes", 'seagreen', 'o', None)

# Then for -np4
mean_speeds = []
std_dev = []
std_err = []
buf_sizes_labels = []
for t in eventRailMaxDataSize:
	#print t
	buf_sizes_labels.append(t[0])
	with open(log_dir_np4 + '/multisegm_' + t[0] + '.log') as log:
		lines = log.read().splitlines()
	gather_mean(lines, mean_speeds, std_dev, std_err)

print "VARYING SIZES -np4"
print "MEAN:", mean_speeds
print "STD DEV: ", std_dev
print "STD ERR:", std_err

# add -np4 to the plot
fig = plot(eventRailMaxDataSize, buf_sizes_labels, mean_speeds, std_dev, std_err, "Buffer size", "Mean speed per iteration (Gbps)", "3 units", filename, "Multisegment DAQPIPE, varying sizes", 'cornflowerblue', 'v', fig)

# save our file and clear the plotting
fig.savefig(filename, bbox_inches = 'tight')
fig.clf()


##################################################################
## PLOT FOR VARYING PARALLELL
filename = "multisegm_plot_mean_speed_varying_parallell_cmp.png"

# First for -np3
mean_speeds = []
std_dev = []
std_err = []
buf_sizes_labels = []
for t in parallell:
	#print t
	buf_sizes_labels.append(t[0])
	# last size (2M) is the one we will use here
	ermds = eventRailMaxDataSize[len(eventRailMaxDataSize)-1]

	with open(log_dir_np3 + '/' + 'multisegm_' + t[0] + '_' + ermds[0] + '.log') as log:
		lines = log.read().splitlines()
	gather_mean(lines, mean_speeds, std_dev, std_err)

print "VARYING PARALLELL -np3"
print "MEAN:", mean_speeds
print "STD DEV: ", std_dev
print "STD ERR:", std_err

# plot -np3
fig = plot(parallell, buf_sizes_labels, mean_speeds, std_dev, std_err, "Parallell value", "Mean speed per iteration (Gbps)", "2 units", filename , "Multisegment DAQPIPE, varying parallell value", 'seagreen', 'o', None)

# Now do -np4
mean_speeds = []
std_dev = []
std_err = []
buf_sizes_labels = []
for t in parallell:
	#print t
	buf_sizes_labels.append(t[0])
	# last size (2M) is the one we will use here
	ermds = eventRailMaxDataSize[len(eventRailMaxDataSize)-1]

	with open(log_dir_np4 + '/' + 'multisegm_' + t[0] + '_' + ermds[0] + '.log') as log:
		lines = log.read().splitlines()
	gather_mean(lines, mean_speeds, std_dev, std_err)

print "VARYING PARALLELL -np4"
print "MEAN:", mean_speeds
print "STD DEV: ", std_dev
print "STD ERR:", std_err

# add -np4 to the plot
fig = plot(parallell, buf_sizes_labels, mean_speeds, std_dev, std_err, "Parallell value", "Mean speed per iteration (Gbps)", "3 units", filename , "Multisegment DAQPIPE, varying parallell value", 'cornflowerblue', 'v', fig)

# save file and clear the plot
fig.savefig(filename, bbox_inches = 'tight')
fig.clf()


