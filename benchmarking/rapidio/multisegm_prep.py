# -*- coding: UTF-8 -*-
# TODO ask Séb about -t, what would be good values. A: 3-4 mins would be good
# TODO ask Sébastien for calculation formula for number of allocations. Still not clear.
# TODO create configs for "normal" version of daqpipe
# TODO what is parallell when not set?
# TODO what does parallell do?

# SCRIPT FOR SRC WITH PATCH FOR MULTILPLE SEGMENTS
# Limitations:
# 	rails == 1, number of PCIe cards connected to a readout unit (not entirely true as there is only one physical board but it feeds into two rails)
# 	credits == 1, because BU allocates credits x units number of segments
#			Otherwise, credits is the number of events handled at the same time by the SW
#
# Parameters:
#	eventRailMaxDataSize
#		(max size of BU buffer which is a RX buf)
#	parallell 
#		(TODO describe this) Do this with only the largest buffer size

#Template configuration file with everything
#{
#	"testing":{
#		"useDriverThreads":true
#	},
#	"general":{
#		"eventRailMaxDataSize":2097152,
#		"ruStoredEvents":32,
#		"gatherType": "sched"
#	},
#	"gather":{
#		"sched":{
#			"parallell":4
#			}
#		}
#}

import os

eventRailMaxDataSize = [
		('64k', 64*1024), 
		('128k', 128*1024), 
		('256k', 256*1024), 
		('512k', 512*1024), 
		('1M', 1024*1024), 
		('2M', 2*1024*1024)
		]
parallell = [
		('p1', 1), 
		('p2', 2), 
		('p3', 3), 
		('p4', 4), 
		('p5', 5), 
		('p6', 6)
		]

###############################################
## CREATE CONFIG DIR
config_dir = 'daqpipe-multisegm-configs'
if not os.path.exists(config_dir):
	    os.mkdir(config_dir)

###############################################
## CREATE CONFIG FILES: VARYING SIZES
for t in eventRailMaxDataSize:
	cfg = open(config_dir + '/' + 'multisegm_' + t[0] + '.cfg', 'w')
	cfg.write('{\n')
	cfg.write('	"testing":{\n')
	cfg.write('		"useDriverThreads":true\n')
	cfg.write('	},\n')
	cfg.write('	"general":{\n')
	cfg.write('		"eventRailMaxDataSize":' + str(t[1]) + ',\n')
	cfg.write('		"ruStoredEvents":32\n')
	cfg.write('	}\n')
	cfg.write('}\n')
	cfg.close()

##############################################
## CREATE CONFIG FILES: VARYING PARALLELL PARAMETER
for t in parallell:
	# last size (2M) is the one we will use here
	ermds = eventRailMaxDataSize[len(eventRailMaxDataSize)-1]

	cfg = open(config_dir + '/' + 'multisegm_' + t[0] + '_' + ermds[0] + '.cfg', 'w')
	cfg.write('{\n')
	cfg.write('	"testing":{\n')
	cfg.write('		"useDriverThreads":true\n')
	cfg.write('	},\n')
	cfg.write('	"general":{\n')
	cfg.write('		"eventRailMaxDataSize":' + str(ermds[1]) + ',\n')
	cfg.write('		"ruStoredEvents":32,\n')
	cfg.write('		"gatherType":"sched"\n')
	cfg.write('	},\n')
	cfg.write('	"gather":{\n')
	cfg.write('		"sched":{\n')
	cfg.write('			"parallell":' + str(t[1]) + '\n')
	cfg.write('		}\n')
	cfg.write('	}\n')
	cfg.write('}\n')
	cfg.close()

##############################################
# CREATE LOG DIRS
#
# TODO: change so that we can handle all files in the same directory
log_dir_np3 = 'daqpipe-multisegm-logs-np3'
if not os.path.exists(log_dir_np3):
	    os.mkdir(log_dir_np3)

log_dir_np4 = 'daqpipe-multisegm-logs-np4'
if not os.path.exists(log_dir_np4):
	    os.mkdir(log_dir_np4)

#############################################
## GENERATE HOSTS FILE
hosts_filename = 'daqpipe_multisegm_hosts'
hosts_quad = [
		'cd1001532-6m001828ts-a',
		'cd1001532-6m001828ts-b',
		'cd1001532-6m001828ts-c',
		'cd1001532-6m001828ts-d'
		]
hosts_file = open(hosts_filename, 'w')
for n in hosts_quad:
	hosts_file.write(n + '\n')

