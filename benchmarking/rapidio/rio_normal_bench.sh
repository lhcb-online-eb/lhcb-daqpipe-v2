#!/bin/bash
CONF_DIR="./daqpipe-normal-configs"
LOG_DIR_BASE="./daqpipe-rio-normal-logs"
timestamp=$(date +"%Y%m%d_%H%M%S")
LOG_DIR="${LOG_DIR_BASE}_${timestamp}"

# Create timestamped log dir
mkdir $LOG_DIR

# Start with generating all the files
python normal_prep.py

# Then run ALL configs in scale
for np in 4 8 12 16
do
	for cfg in ${CONF_DIR}/*.cfg
	do
		name=${cfg##*/}
		base=${name%.cfg}
		# Can only run one process per node since we can't have loop back sockets with RIO CM
		CMD_RIO="dp-mpiexec --ppn 1 -np ${np} -f ~/rio-hosts ../../build-rio/src/daqpipe -f ${cfg}  > ${LOG_DIR}/${base}_np${np}.log -t 120 2>&1"
		echo "${CMD_RIO}"
		eval "${CMD_RIO}"
	done
done

# Now we're ready for plotting
#python normal_plot.py

