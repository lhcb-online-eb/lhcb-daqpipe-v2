#!/bin/bash

# ABOUT THIS FILE
# This file can be used as-is, and it will do everything needed.
# It is also possible to run the preparation and logging scripts separately.
# Especially if you already have some results, you can rerun the plotting script whenever needed.
#
# TODO: change so that we can vary the number of nodes and change the file name accordingly
# instead of what we do now, put them in separate directories.
# TODO: do a function that runs the benchmark runs on a set of parameters, instead of the copy-pasta
# it is now...

# Start with generating all the files and dirs
python multisegm_prep.py

CONF_DIR="./daqpipe-multisegm-configs"
LOG_DIR_NP3="./daqpipe-multisegm-logs-np3"
LOG_DIR_NP4="./daqpipe-multisegm-logs-np4"
# Then run all the configs for -np3
for cfg in ${CONF_DIR}/*.cfg
do
	name=${cfg##*/}
        base=${name%.cfg}
	CMD="/opt/hydra/bin/daqpipempiexec -np 3 -f daqpipe_multisegm_hosts ../../build-rio/src/daqpipe -f ${cfg} -t 60 > ${LOG_DIR_NP3}/${base}.log 2>&1"
	echo ${CMD}
	eval ${CMD}
done

# Then run all the configs for -np4
for cfg in ${CONF_DIR}/*.cfg
do
	name=${cfg##*/}
        base=${name%.cfg}
	CMD="/opt/hydra/bin/daqpipempiexec -np 4 -f daqpipe_multisegm_hosts ../../build-rio/src/daqpipe -f ${cfg} -t 60 > ${LOG_DIR_NP4}/${base}.log 2>&1"
	echo ${CMD}
	eval ${CMD}
done

# Now we're ready for plotting
python multisegm_plot_node_cmp.py

