/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include <mpi.h>
#include <getopt.h>
#include <cstdlib>
#include <cstdio>

/********************  GLOBALS  *********************/
size_t MESSAGE_SIZE = 1024*1024;//1 MB
size_t BUFFER_SIZE = 1024*1024*256;//256 MB
size_t REPEAT_FACTOR = 20;

/*******************  FUNCTION  *********************/
void parseArgs(int argc, char ** argv)
{
	int c;
	while ((c = getopt(argc,argv,"m:b:r:h")) != -1)
	{
		switch(c)
		{
			case 'h':
				fprintf(stderr,"%s [-m {MESSAGE_SIZE}] [-b {BUFFER_SIZE}] [-r REPEAT_FACTOR] [-h]\n",argv[0]);
				exit(EXIT_SUCCESS);
				break;
			case 'b':
				BUFFER_SIZE = atol(optarg);
				break;
			case 'm':
				MESSAGE_SIZE = atol(optarg);
				break;
			case 'r':
				REPEAT_FACTOR = atol(optarg);
				break;
			default:
				fprintf(stderr,"Invalid options : %c\n",c);
				abort();
		}
	}
}

/*******************  FUNCTION  *********************/
int main(int argc, char ** argv)
{
	//setup MPI
	MPI_Init(&argc,&argv);
	
	//parse
	parseArgs(argc,argv);
	
	//get info
	int nodes;
	int rank;
	MPI_Comm_rank(MPI_COMM_WORLD,&rank);
	MPI_Comm_size(MPI_COMM_WORLD,&nodes);
	
	//check
	if (nodes != 2) {
		fprintf(stderr,"Need to run on exactly 2 tasks !\n");
		return EXIT_FAILURE;
	}
	if (BUFFER_SIZE % MESSAGE_SIZE != 0) {
		fprintf(stderr,"BUFFER_SIZE need to be a multiple of MESSAGE_SIZE ! ");
		return EXIT_FAILURE;
	}
	
	//print header
	if (rank == 0)
	{
		printf("---------------------------------------\n");
		printf("MESSAGE_SIZE  : %lu\n",MESSAGE_SIZE);
		printf("BUFFER_SIZE   : %lu\n",BUFFER_SIZE);
		printf("REPEAT_FACTOR : %lu\n",REPEAT_FACTOR);
		printf("---------------------------------------\n");
	}
	
	//compute repeat
	size_t repeat = 4 * (BUFFER_SIZE / MESSAGE_SIZE);
	
	//run warmup
	if (rank == 0) {
		char * message = new char[MESSAGE_SIZE];
		MPI_Status status;
		for (size_t i = 0 ; i < repeat ; i++)
			MPI_Recv(message,MESSAGE_SIZE,MPI_CHAR,1,0,MPI_COMM_WORLD,&status);
	} else {
		char * buffer = new char[BUFFER_SIZE];
		for (size_t i = 0 ; i < repeat ; i++)
			MPI_Send(buffer + (i*MESSAGE_SIZE)%BUFFER_SIZE,MESSAGE_SIZE,MPI_CHAR,0,0,MPI_COMM_WORLD);
	}
	
	//compute repeat
	repeat = REPEAT_FACTOR * (BUFFER_SIZE / MESSAGE_SIZE);
	
	//start clock
	double t0 = MPI_Wtime();
	
	//run
	if (rank == 0) {
		char * message = new char[MESSAGE_SIZE];
		MPI_Status status;
		for (size_t i = 0 ; i < repeat ; i++)
			MPI_Recv(message,MESSAGE_SIZE,MPI_CHAR,1,0,MPI_COMM_WORLD,&status);
	} else {
		char * buffer = new char[BUFFER_SIZE];
		for (size_t i = 0 ; i < repeat ; i++)
			MPI_Send(buffer + (i*MESSAGE_SIZE)%BUFFER_SIZE,MESSAGE_SIZE,MPI_CHAR,0,0,MPI_COMM_WORLD);
	}
	
	//stop clock
	double t1 = MPI_Wtime();
	
	//print
	if (rank == 0)
		printf("Bandwidth : %f Gb/s\n",(double)(8*MESSAGE_SIZE * repeat) / (t1 - t0) / 1000.0 / 1000.0 / 1000.0);
	
	//finish MPI
	MPI_Finalize();
	
	//finish
	return EXIT_SUCCESS;
}
