/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef UBENCH_COMMON_HPP
#define UBENCH_COMMON_HPP

/********************  HEADERS  *********************/
#include <mpi.h>
#include <cstdio>
#include <cstdlib>
#include <unistd.h>
#include <cassert>
#include <ctime>
#include <string>
#include <sys/mman.h>
#include <getopt.h>
#include <malloc.h>
#include <cstring>

#ifdef __APPLE__
#include <mach/clock.h>
#include <mach/mach.h>
#endif

/********************  MACROS  **********************/
//This can be removed, it is unused now
#define CREDITS 2
//rank of master process
#define MPI_MASTER 0
//Max number of onfly communications (it is used to allocate a static request array)
#define MAX_ONFLY 512
//Size of a command in bytes.
#define CMD_SIZE 64
//Must be 1 or 0, 0 disable usage of multiple segments. More stress for the board if 1, for 0
//the board can optimize the traffic by detecting the reuse but it is not reallistic.
#define SELECTOR 1
//sizes
#define MEM_SIZE (BUFFER_SIZE+OFFSET)
#define META_SIZE (BUFFER_SIZE/100)
#define META_MEM_SIZE (META_SIZE+OFFSET)

/********************  GLOBALS  *********************/
size_t BUFFER_SIZE = 10UL*1024UL*1024UL;
size_t REPEAT = 10000;
int ONFLY = 1;
int BUFFER_COUNT = 100;
int OFFSET = 0;
int SHIFT = 0;
int GROUPS = 4;

/********************  STRUCT  **********************/
struct RequestArray
{
	RequestArray(void);
	void isend(void * buffer,size_t size,int target,int tag);
	void irecv( void* buffer, size_t size, int tag, int source );
	int getId(void);
	void waitAll(void);
	MPI_Request requests[MAX_ONFLY];
	bool available[MAX_ONFLY];
};

/*******************  FUNCTION  *********************/
RequestArray::RequestArray ( void )
{
	for (int i = 0 ; i < ONFLY ; i++)
	{
		requests[i] = MPI_REQUEST_NULL;
		available[i] = true;
	}
}

/*******************  FUNCTION  *********************/
int RequestArray::getId ( void )
{
	//find empty
	int id = -1;
	for (int i = 0 ; i < ONFLY ; i++)
		if (available[i])
		{
			id = i;
			break;
		}
	
	//if not avail
	if (id == -1)
	{
		MPI_Status status;
		int ret = MPI_Waitany(ONFLY,requests,&id,&status);
		assert(ret == 0);
		assert(id >= 0 && id < ONFLY);
		available[id] = true;
		requests[id] = MPI_REQUEST_NULL;
	}
	
	return id;
}

/*******************  FUNCTION  *********************/
void RequestArray::isend ( void* buffer, size_t size, int target, int tag )
{
	//get a free id
	int id = getId();
	
	//push send
	MPI_Isend(buffer,size,MPI_CHAR,target,tag,MPI_COMM_WORLD,&requests[id]);
	available[id] = false;
}

/*******************  FUNCTION  *********************/
void RequestArray::irecv ( void* buffer, size_t size, int tag = MPI_ANY_TAG, int source = MPI_ANY_SOURCE )
{
	//get a free id
	int id = getId();
	
	//push send
	MPI_Irecv(buffer,size,MPI_CHAR,source,tag,MPI_COMM_WORLD,&requests[id]);
	available[id] = false;
}

/*******************  FUNCTION  *********************/
void RequestArray::waitAll ( void )
{
	//printf("wait all\n");
	MPI_Status status[ONFLY];
	int ret = MPI_Waitall(ONFLY,requests,status);
	assert(ret == 0);
	
	for (int id = 0 ; id < ONFLY ; id++)
	{
		available[id] = true;
		requests[id] = MPI_REQUEST_NULL;
	}
}

/*******************  FUNCTION  *********************/
std::string getHostname()
{
	char buffer[1024];
	gethostname(buffer,sizeof(buffer));
	return buffer;
}

/*******************  FUNCTION  *********************/
void printAlignment(void * a = NULL, void * b = NULL)
{
	return;
	if (a != NULL)
		printf("Alignment 4K = %lu, 2M = %lu\n",(size_t)a%4096UL,(size_t)a%(2UL*1024UL*1024UL));
	if (b != NULL)
		printf("Alignment 4K = %lu, 2M = %lu\n",(size_t)b%4096UL,(size_t)b%(2UL*1024UL*1024UL));
}

/*******************  FUNCTION  *********************/
void parseArgs(int argc, char ** argv)
{
	int c;
	while ((c = getopt(argc,argv,"c:s:o:b:r:S:g:h")) != -1)
	{
		switch(c)
		{
			case 'h':
				fprintf(stderr,"%s [-s {SIZE}] [-c {onfly}] [-b {BUFFERS}] [-o {OFFSET}] [-r {REPEAT}] [-S {SHIFT}] [-g {GROUPS}]\n",argv[0]);
				exit(EXIT_SUCCESS);
				break;
			case 's':
				BUFFER_SIZE = atol(optarg);
				break;
			case 'c':
				ONFLY = atoi(optarg);
				break;
			case 'b':
				BUFFER_COUNT = atoi(optarg);
				break;
			case 'o':
				OFFSET = atoi(optarg);
				break;
			case 'r':
				REPEAT = atoi(optarg);
				break;
			case 'S':
				SHIFT = atoi(optarg);
				break;
			case 'g':
				GROUPS = atoi(optarg);
				break;
			default:
				fprintf(stderr,"Invalid options : %c\n",c);
				abort();
		}
	}

	//addapt repeat
	REPEAT *= ((10UL*1024UL*1024UL)/BUFFER_SIZE);
}

/*******************  FUNCTION  *********************/
char * alloc(size_t size)
{
	return (char*)memalign(2*1024*1024,size);
	//return (char*)mmap(NULL,size,PROT_READ|PROT_WRITE,MAP_ANON|MAP_PRIVATE,0,0);
}

/*******************  FUNCTION  *********************/
void dealloc(char * ptr,size_t size)
{
	//munmap(ptr,size);
	free(ptr);
}

/*******************  FUNCTION  *********************/
int countLocalNodes(int rank,int nodes)
{
	char buffer[128];
	gethostname(buffer,sizeof(buffer));
	
	char * rbuffer = new char[128 * nodes];
	memset(rbuffer,0,128 * nodes);
	MPI_Gather(buffer, 128, MPI_CHAR,rbuffer, 128, MPI_CHAR,0,MPI_COMM_WORLD);
	
	int max = 0;
	if (rank == 0 )
	{
		std::string tmp;
		int cnt = 1;
		for (int i = 0 ; i < nodes ; i++)
		{
			if (tmp == rbuffer+128*i)
			{
				cnt++;
			} else {
				tmp = rbuffer+128*i;
				if (cnt > max)
					max = cnt;
				cnt = 1;
			}
		}
		if (cnt > max)
			max = cnt;
	
		delete [] rbuffer;
	}
	
	MPI_Bcast(&max, 1,MPI_INT, 0,MPI_COMM_WORLD );
	
	return max;
}

/*******************  FUNCTION  *********************/
void runPrep(int rank,int nodes);
void runClean(int rank,int nodes);
void runUnit( int rank, int nodes, size_t repeat );
size_t memSize(int rank,int nodes,int local);
extern const char * name;

/*******************  FUNCTION  *********************/
int main(int argc, char ** argv)
{
	//setup MPI
	MPI_Init(&argc,&argv);
	
	//args
	parseArgs(argc,argv);
	
	//get info
	int nodes;
	int rank;
	MPI_Comm_rank(MPI_COMM_WORLD,&rank);
	MPI_Comm_size(MPI_COMM_WORLD,&nodes);
	
	//print
	if (rank == MPI_MASTER)
	{
		printf("Try %s on %d processes\n",name,nodes);
		printf("Use : \n  - buffer size : %lu\n  - repeat : %lu\n  - credits : %d\n  - onfly requests : %d\n  - buffers : %d\n  - offset : %d\n  - shift : %d\n  - groups : %d\n",BUFFER_SIZE,REPEAT,CREDITS,ONFLY,BUFFER_COUNT,OFFSET,SHIFT,GROUPS);
		if (nodes < 2)
		{
			fprintf(stderr,"You need a world size larger than 1\n");
			MPI_Abort(MPI_COMM_WORLD,1);
		}
		if (nodes % 2 == 1)
		{
			fprintf(stderr,"You need a world size multiple of 2\n");
			MPI_Abort(MPI_COMM_WORLD,1);
		}
	}
	
	//wait all
	MPI_Barrier(MPI_COMM_WORLD);
	
	//get start time
	struct timespec tstart, tend;
	
	runPrep(rank,nodes);
	runUnit(rank,nodes,256);
	
	MPI_Barrier(MPI_COMM_WORLD);
	
#ifndef __APPLE__
  clock_gettime(CLOCK_REALTIME, &tstart);
#else
  clock_serv_t cclock;
  mach_timespec_t mts;
  host_get_clock_service(mach_host_self(), CALENDAR_CLOCK, &cclock);
  clock_get_time(cclock, &mts);
  mach_port_deallocate(mach_task_self(), cclock);
  tstart.tv_sec = mts.tv_sec;
  tstart.tv_nsec = mts.tv_nsec;
#endif
// 	double tStart = MPI_Wtime();

	//run
	runUnit(rank,nodes,REPEAT);
	
// 	double tEnd = MPI_Wtime();
	
	//get end time
#ifndef __APPLE__
  clock_gettime(CLOCK_REALTIME, &tend);
#else
  host_get_clock_service(mach_host_self(), CALENDAR_CLOCK, &cclock);
  clock_get_time(cclock, &mts);
  mach_port_deallocate(mach_task_self(), cclock);
  tend.tv_sec = mts.tv_sec;
  tend.tv_nsec = mts.tv_nsec;
#endif
  
	runClean(rank,nodes);
	
	int local = countLocalNodes(rank,nodes);
	if (rank == 0)
		printf("Local nodes : %d\n",local);
	
	//get elapsed
	long long int timeDiff = (tend.tv_sec - tstart.tv_sec) * 1000000000 + (tend.tv_nsec - tstart.tv_nsec);
	float elapsed = timeDiff / 1000000000.0f;
// 	float elapsed = tEnd - tStart;
	
	//print rate
	float bw = (float)((float)(8*memSize(rank,nodes,local))/(float)(1000UL*1000UL*1000UL))/elapsed;
	printf("Rank %d as rate : %f Gb/s\n",rank,bw);
	
	//compute min/mean/max
	float * all = new float[nodes];
	MPI_Gather(&bw,1,MPI_FLOAT,all,1,MPI_FLOAT,0,MPI_COMM_WORLD);
	
	//compute
	if (rank == 0)
	{
		float min = all[0];
		float mean = 0;
		float max = 0;
		for (int i = 0 ; i < nodes ; i++) {
			if (all[i] < min)
				min = all[i];
			if (all[i] > max)
				max = all[i];
			mean += all[i];
		}
		printf("Stats : min = %f , average = %f , max = %f\n",min,mean/(float)nodes,max);
	}
	
	//finish MPI
	MPI_Finalize();
	
	return EXIT_SUCCESS;
}

#endif //UBENCH_COMMON_HPP
