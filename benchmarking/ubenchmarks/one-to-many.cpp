/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include "common.hpp"

/********************  GLOBALS  *********************/
static char ** buffer = NULL;
const char * name = "one-to-many";

/*******************  FUNCTION  *********************/
void runPrep(int rank,int nodes)
{
	//allocate
	buffer = new char*[BUFFER_COUNT];
	for (int i = 0 ; i < BUFFER_COUNT ; i++) {
		buffer[i] = alloc(MEM_SIZE);
		printAlignment(buffer[i]);
		for (size_t j = 0 ; j < MEM_SIZE ; j++)
			buffer[i][j] = (char)j;
	}
}

/*******************  FUNCTION  *********************/
void runClean(int rank,int nodes)
{
	//free mem
	for (int i = 0 ; i < BUFFER_COUNT ; i++)
		dealloc(buffer[i],MEM_SIZE);
	delete [] buffer;
}

/*******************  FUNCTION  *********************/
int groupNodes(int rank, int nodes)
{
	int base = rank - rank % GROUPS;
	if (nodes - base >= GROUPS)
		return GROUPS;
	else
		return nodes - base;
}

/*******************  FUNCTION  *********************/
void sendUnit(int rank,int nodes, size_t repeat)
{
	//info
	//printf("Rank %d on %s is SEND\n",rank,getHostname().c_str());
	//printf("Group nodes on %d : %d\n",rank,groupNodes(rank,nodes));
	
	RequestArray req;
	
	//loops on steps
	for (size_t i = 0 ; i < repeat ; i++)
		for (int id = 1 ; id < groupNodes(rank,nodes) ; id++)
			req.isend(buffer[SELECTOR*i%BUFFER_COUNT]+OFFSET,BUFFER_SIZE,rank+id,0);
	req.waitAll();
}

/*******************  FUNCTION  *********************/
void recvUnit(int rank,int nodes, size_t repeat)
{
	//info
	//printf("Rank %d on %s is RECV\n",rank,getHostname().c_str());

	RequestArray req;
	
	//need to receive all
	for (size_t i = 0 ; i < repeat ; i++)
		req.irecv(buffer[SELECTOR*i%BUFFER_COUNT]+OFFSET,BUFFER_SIZE);
	req.waitAll();
}

/*******************  FUNCTION  *********************/
void runUnit(int rank, int nodes, size_t repeat)
{
	if (rank % GROUPS == 0)
		sendUnit(rank,nodes,repeat);
	else
		recvUnit(rank,nodes,repeat);
}

/*******************  FUNCTION  *********************/
size_t memSize(int rank,int nodes,int local)
{
	if (rank % GROUPS == 0)
		return BUFFER_SIZE*(long)REPEAT*(groupNodes(rank,nodes)-1);
	else
		return BUFFER_SIZE*(long)REPEAT;
}
