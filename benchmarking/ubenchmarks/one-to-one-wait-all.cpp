/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include "common.hpp"

/********************  GLOBALS  *********************/
static char ** buffer = NULL;
const char * name = "one-to-one-wait-all";

/*******************  FUNCTION  *********************/
void runPrep(int rank,int nodes)
{
	//allocate
	buffer = new char*[BUFFER_COUNT];
	for (int i = 0 ; i < BUFFER_COUNT ; i++) {
		buffer[i] = alloc(MEM_SIZE);
		printAlignment(buffer[i]);
		for (size_t j = 0 ; j < MEM_SIZE ; j++)
			buffer[i][j] = (char)j;
	}
}

/*******************  FUNCTION  *********************/
void runClean(int rank,int nodes)
{
	//free mem
	for (int i = 0 ; i < BUFFER_COUNT ; i++)
		dealloc(buffer[i],MEM_SIZE);
	delete [] buffer;
}

/*******************  FUNCTION  *********************/
void runReadoutUnit(int rank,int nodes,size_t repeat)
{
	//info
	//printf("Rank %d on %s is RU\n",rank,getHostname().c_str());
	
	RequestArray req;
	
	//loops on steps
	for (size_t i = 0 ; i < repeat ; i++){
		if (i % ONFLY == 0)
			req.waitAll();
		req.isend(buffer[(SELECTOR*i)%BUFFER_COUNT]+OFFSET,BUFFER_SIZE,(rank-1 + SHIFT)%nodes,i);
	}
	req.waitAll();
}

/*******************  FUNCTION  *********************/
void runBuilderUnit(int rank,int nodes,size_t repeat)
{
	//info
	//printf("Rank %d on %s is BU\n",rank,getHostname().c_str());

	RequestArray req;
	
	//need to receive all
	for (size_t i = 0 ; i < repeat ; i++) {
		if (i % ONFLY == 0)
			req.waitAll();
		req.irecv(buffer[(SELECTOR*i)%BUFFER_COUNT]+OFFSET,BUFFER_SIZE,i);
	}
	req.waitAll();
}

/*******************  FUNCTION  *********************/
void runUnit(int rank, int nodes,size_t repeat)
{
	if (rank % 2 == 0)
		runBuilderUnit(rank,nodes,repeat);
	else
		runReadoutUnit(rank,nodes,repeat);
}

/*******************  FUNCTION  *********************/
size_t memSize(int rank,int nodes,int local)
{
	return BUFFER_SIZE*(long)REPEAT;
}
