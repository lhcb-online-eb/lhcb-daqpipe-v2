/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#undef NDEBUG

/********************  HEADERS  *********************/
#include <mpi.h>
#include <cstdio>
#include <cstdlib>
#include <unistd.h>
#include <cassert>
#include <ctime>
#include <string>
#include "../../extern-deps/from-fftw/cycle.h"

#ifdef __APPLE__
#include <mach/clock.h>
#include <mach/mach.h>
#endif

/********************  MACROS  **********************/
#define BUFFER_SIZE (1UL*1024UL*1024UL)
#define REPEAT 2000
#define CREDITS 2
#define MPI_MASTER 0
#define ONFLY 64

/********************  STRUCT  **********************/
struct RequestArray
{
	RequestArray(void);
	void gather(void * sendBuffer,int sendSize,void * recvBuffer,int recvSize, int root);
	void isend(void * buffer,size_t size,int target,int tag);
	void irecv(void * buffer,size_t size,int tag, int source);
	void test(void);
	int getId(void);
	void waitAll(void);
	MPI_Request requests[ONFLY];
	bool available[ONFLY];
	MPI_Comm coms[ONFLY];
};

/*******************  FUNCTION  *********************/
void RequestArray::isend ( void* buffer, size_t size, int target, int tag )
{
	//get a free id
	int id = getId();
	
	//push send
	MPI_Isend(buffer,size,MPI_CHAR,target,tag,MPI_COMM_WORLD,&requests[id]);
	available[id] = false;
}

/*******************  FUNCTION  *********************/
void RequestArray::irecv ( void* buffer, size_t size, int tag = MPI_ANY_TAG, int source = MPI_ANY_SOURCE )
{
	//get a free id
	int id = getId();
	
	//push send
	MPI_Irecv(buffer,size,MPI_CHAR,source,tag,MPI_COMM_WORLD,&requests[id]);
	available[id] = false;
}


/*******************  FUNCTION  *********************/
RequestArray::RequestArray ( void )
{
	for (int i = 0 ; i < ONFLY ; i++)
	{
		requests[i] = MPI_REQUEST_NULL;
		available[i] = true;
		MPI_Comm_dup(MPI_COMM_WORLD,&coms[i]);
	}
}

/*******************  FUNCTION  *********************/
int RequestArray::getId ( void )
{
	//find empty
	int id = -1;
	for (int i = 0 ; i < ONFLY ; i++)
		if (available[i])
		{
			id = i;
			break;
		}
	
	//if not avail
	if (id == -1)
	{
		MPI_Status status;
		int ret = MPI_Waitany(ONFLY,requests,&id,&status);
		assert(ret == 0);
		assert(id >= 0 && id < ONFLY);
		available[id] = true;
		requests[id] = MPI_REQUEST_NULL;
	}
	
	return id;
}

/*******************  FUNCTION  *********************/
void RequestArray::test ( void )
{
	int id;
	int flag;
	MPI_Status status;
	int ret = MPI_Testany(ONFLY,requests,&id,&flag,&status);
	assert(ret == 0);
	if (flag && id != MPI_UNDEFINED)
	{
		assert(id >= 0 && id < ONFLY);
		available[id] = true;
		requests[id] = MPI_REQUEST_NULL;
	}
}

/*******************  FUNCTION  *********************/
void RequestArray::gather ( void* sendBuffer, int sendSize, void* recvBuffer, int recvSize, int root )
{
	//get a free id
	int id = getId();
	
	//push send
	#ifdef MPI_Igather
		MPI_Igather(sendBuffer,sendSize,MPI_CHAR,recvBuffer,recvSize,MPI_CHAR,root,coms[id],&requests[id]);
	#else //MPI_Igather
		fprintf(stderr,"MPI_Igather is not supported by you MPI version, please check for MPI 3.0 compatibility.\n");
		abort();
	#endif //MPI_Igather
	available[id] = false;
}

/*******************  FUNCTION  *********************/
void RequestArray::waitAll ( void )
{
	MPI_Status status[ONFLY];
	int ret = MPI_Waitall(ONFLY,requests,status);
	assert(ret == 0);
}

/*******************  FUNCTION  *********************/
std::string getHostname()
{
	char buffer[1024];
	gethostname(buffer,sizeof(buffer));
	return buffer;
}

/*******************  FUNCTION  *********************/
unsigned long long diff( struct timespec & tstart, struct timespec & tend)
{
	return (tend.tv_sec - tstart.tv_sec) * 1000000000 + (tend.tv_nsec - tstart.tv_nsec);
}

/*******************  FUNCTION  *********************/
void add(struct timespec & tstart, unsigned long long deltaT)
{
	tstart.tv_nsec += deltaT;
	tstart.tv_sec += tstart.tv_nsec / 1000000000;
	tstart.tv_nsec = tstart.tv_nsec % 1000000000;
}

/*******************  FUNCTION  *********************/
bool less(struct timespec & tcur, struct timespec & tref)
{
	return tcur.tv_sec < tref.tv_sec || (tcur.tv_sec == tref.tv_sec && tcur.tv_nsec < tref.tv_nsec);
}

/*******************  FUNCTION  *********************/
bool wait(struct timespec & tref)
{
	struct timespec tcur;
	clock_gettime(CLOCK_REALTIME, &tcur);
	return less(tcur,tref);
}

/*******************  FUNCTION  *********************/
int main(int argc, char ** argv)
{
	//setup MPI
	MPI_Init(&argc,&argv);

	ticks manualDeltaT = atol(argv[1]);
	
	//get info
	int nodes;
	int rank;
	MPI_Comm_rank(MPI_COMM_WORLD,&rank);
	MPI_Comm_size(MPI_COMM_WORLD,&nodes);
	
	//print
	if (rank == MPI_MASTER)
	{
		printf("Try round robin gather on %d nodes\n",nodes);
		printf("Use : \n  - buffer size : %lu\n  - repeat : %d\n  - credits : %d  - onfly requests : %d\n",BUFFER_SIZE,REPEAT,CREDITS,ONFLY);
	}
	
	//wait all
	MPI_Barrier(MPI_COMM_WORLD);
	
	//setup
	RequestArray req;
	char * sendBuffer = new char[BUFFER_SIZE];
	char * recvBuffer = new char[BUFFER_SIZE * nodes];
	
	//measure ping pong time
	MPI_Status request;
	ticks deltaT;
	if (rank == 0) {
		struct timespec tt0,tt1;
		clock_gettime(CLOCK_REALTIME, &tt0);
		ticks t0 = getticks();
		for (int i = 0 ; i < REPEAT ; i ++) {
			MPI_Send(sendBuffer,BUFFER_SIZE,MPI_CHAR,1,0,MPI_COMM_WORLD);
			MPI_Recv(recvBuffer,BUFFER_SIZE,MPI_CHAR,1,0,MPI_COMM_WORLD,&request);
		}
		ticks t1 = getticks();
		clock_gettime(CLOCK_REALTIME, &tt1);
		deltaT = (t1-t0) / (2UL * REPEAT);
		deltaT = diff(tt0,tt1) / (2UL*REPEAT);
		printf("  - delta  : %llu us\n",deltaT);
		printf("  - manual : %llu us\n",manualDeltaT);
	} else if (rank == 1) {
		for (int i = 0 ; i < REPEAT ; i ++) {
			MPI_Recv(recvBuffer,BUFFER_SIZE,MPI_CHAR,0,0,MPI_COMM_WORLD,&request);
			MPI_Send(sendBuffer,BUFFER_SIZE,MPI_CHAR,0,0,MPI_COMM_WORLD);
		}
	}

	//share
	MPI_Bcast(&deltaT,sizeof(deltaT),MPI_CHAR,0,MPI_COMM_WORLD);
	MPI_Barrier(MPI_COMM_WORLD);
	
	//get start time
	struct timespec tstart, tend;

	//force conection
	for (int j = 0 ; j < nodes ; j+=2) {
		if (rank % 2 == 0) {
			req.irecv(recvBuffer,BUFFER_SIZE,0);
		} else {
			req.isend(sendBuffer,BUFFER_SIZE,(j+rank-1)%nodes,0);
		}
	}

	//barrier
	MPI_Barrier(MPI_COMM_WORLD);

	struct timespec tref;
	clock_gettime(CLOCK_REALTIME, &tref);
	add(tref,deltaT*1000);	
	MPI_Bcast(&tref,sizeof(tref),MPI_CHAR,0,MPI_COMM_WORLD);

	while (wait(tref));

#ifndef __APPLE__
	clock_gettime(CLOCK_REALTIME, &tstart);
#else
	clock_serv_t cclock;
	mach_timespec_t mts;
	host_get_clock_service(mach_host_self(), CALENDAR_CLOCK, &cclock);
	clock_get_time(cclock, &mts);
	mach_port_deallocate(mach_task_self(), cclock);
	tstart.tv_sec = mts.tv_sec;
	tstart.tv_nsec = mts.tv_nsec;
#endif

	ticks next = getticks() ;
	
	//loop
	for (int i = 0 ; i < REPEAT ; i++) {
		for (int j = 0 ; j < nodes ; j+=2) {
			if (rank % 2 == 0) {
				req.irecv(recvBuffer,BUFFER_SIZE,j);//,(2*nodes+rank + 1 - j)%nodes);
			} else {
				req.isend(sendBuffer,BUFFER_SIZE,(j+rank-1)%nodes,j);
			}
				next += manualDeltaT;//*deltaT;//-deltaT/20;
				add(tref,manualDeltaT);
				while (wait(tref))
					req.test();
				/*while (getticks() < next) {
					req.test();
				}*/
			//}
		}
	}
	printf("waitall\n");
	req.waitAll();
	
	//get end time
#ifndef __APPLE__
	clock_gettime(CLOCK_REALTIME, &tend);
#else
	host_get_clock_service(mach_host_self(), CALENDAR_CLOCK, &cclock);
	clock_get_time(cclock, &mts);
	mach_port_deallocate(mach_task_self(), cclock);
	tend.tv_sec = mts.tv_sec;
	tend.tv_nsec = mts.tv_nsec;
#endif
	
	//free mem
	delete [] sendBuffer;
	delete [] recvBuffer;
	
	//get elapsed
	long long int timeDiff = (tend.tv_sec - tstart.tv_sec) * 1000000000 + (tend.tv_nsec - tstart.tv_nsec);
	float elapsed = timeDiff / 1000000000.0f;
	
	//print rate
	float rate = (float)((8*BUFFER_SIZE*(long)REPEAT*nodes/2)/(1024UL*1024UL*1024UL))/elapsed;
	printf("Rank %d as rate : %f Gb/s\n",rank,rate);
	
	//wait all
	MPI_Barrier(MPI_COMM_WORLD);
	
	//share
	float * rates = new float[nodes];
	MPI_Gather(&rate,1,MPI_FLOAT,rates,1,MPI_FLOAT,0,MPI_COMM_WORLD);
	
	//compute
	float min = rates[0];
	float max = rates[0];
	float sum = 0;
	for (int i = 0 ; i < nodes ; i++) {
		if (rates[i] < min)
			min = rates[i];
		if (rates[i] > max)
			max = rates[i];
		sum += rates[i];
	}
	
	delete [] rates;
	
	//printf
	if (rank == 0)
		printf("summary : %f\t%f\t%f\n",min,sum/(float)nodes,max);
	
	//finish MPI
	MPI_Finalize();
	
	return EXIT_SUCCESS;
}
