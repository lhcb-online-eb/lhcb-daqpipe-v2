#!/bin/bash

nodes=64

###############################################################

mkdir one-to-one-$nodes
for tmp in `seq 0 2 $((2*$nodes))`
do
	salloc -p hipri -N $nodes -n $((2*$nodes)) mpirun  ./one-to-one -s 10485760 -r 100 -S $tmp | tee one-to-one-$nodes/out-$tmp.txt
	cat one-to-one-$nodes/out-$tmp.txt | grep Stats >> one-to-one-$nodes/all.txt
done

###############################################################

mkdir all-to-all
for tmp in 2 4 8 16 24 32 48 64
do
	salloc -p hipri -N $tmp -n $tmp mpirun ./mpiAllToAll -r 2000 | tee all-to-all/out-$tmp.txt
	cat all-to-all/out-$tmp.txt | grep Stats >> all-to-all/all.txt
done

###############################################################

mkdir daq-sync
for tmp in 2 4 8 16 24 32 48 64
do
	salloc -p hipri -N $tmp -n $tmp mpirun ./daqpipe-sync -r 100 | tee daq-sync/out-$tmp.txt
	cat daq-sync/out-$tmp.txt | grep Stats >> daq-sync/all.txt
done
