/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include "common.hpp"

/********************  GLOBALS  *********************/
static char ** buffer = NULL;
static char ** meta;
const char * name = "many-gather-cmd-meta";

/*******************  FUNCTION  *********************/
void runPrep(int rank,int nodes)
{
	//allocate
	buffer = new char*[BUFFER_COUNT];
	meta = new char*[BUFFER_COUNT];
	for (int i = 0 ; i < BUFFER_COUNT ; i++) {
		buffer[i] = alloc(MEM_SIZE);
		meta[i] = alloc(META_MEM_SIZE);
		printAlignment(buffer[i],meta[i]);
		for (size_t j = 0 ; j < MEM_SIZE ; j++)
			buffer[i][j] = (char)j;
		for (size_t j = 0 ; j < META_MEM_SIZE ; j++)
			meta[i][j] = (char)j;
	}
}

/*******************  FUNCTION  *********************/
void runClean(int rank,int nodes)
{
	//free mem
	for (int i = 0 ; i < BUFFER_COUNT ; i++) {
		dealloc(meta[i],META_MEM_SIZE);
		dealloc(buffer[i],MEM_SIZE);
	}
	delete [] buffer;
	delete [] meta;
}

/*******************  FUNCTION  *********************/
void runReadoutUnit(int rank,int nodes, size_t repeat )
{
	//info
	//printf("Rank %d on %s is RU\n",rank,getHostname().c_str());
	
	//allocate memory
	char cmd[CMD_SIZE];
	RequestArray req;
	
	//loops on steps
	for (size_t i = 0 ; i < repeat ; i++)
		for (int j = 1 ; j < nodes ; j += 2)
		{
			int k = j;//(j + rank) % nodes;
			req.irecv(cmd,CMD_SIZE,0);
			req.isend(buffer[(SELECTOR*(i+j))%BUFFER_COUNT]+OFFSET,META_SIZE,k,1);
			req.isend(buffer[(SELECTOR*(i+j))%BUFFER_COUNT]+OFFSET,BUFFER_SIZE,k,2);
		}
	req.waitAll();
}

/*******************  FUNCTION  *********************/
void runBuilderUnit(int rank,int nodes, size_t repeat )
{
	//info
	//printf("Rank %d on %s is BU\n",rank,getHostname().c_str());

	//allocate memory
	char cmd[CMD_SIZE];
	RequestArray req;
	
	//need to receive all
	for (size_t i = 0 ; i < repeat * nodes/2 ; i++)
	{
		req.isend(cmd,CMD_SIZE,(i*2)%nodes,0);
		req.irecv(meta[(SELECTOR*i)%BUFFER_COUNT]+OFFSET,META_SIZE,1);
		req.irecv(buffer[(SELECTOR*i)%BUFFER_COUNT]+OFFSET,BUFFER_SIZE,2);
	}
	req.waitAll();
}

/*******************  FUNCTION  *********************/
void runUnit(int rank, int nodes, size_t repeat )
{
	if (rank % 2 == 0)
		runReadoutUnit(rank,nodes,repeat);
	else
		runBuilderUnit(rank,nodes,repeat);
}

/*******************  FUNCTION  *********************/
size_t memSize(int rank,int nodes,int local)
{
	return ((BUFFER_SIZE+META_SIZE)*(long)REPEAT*(nodes-local))/2;
}
