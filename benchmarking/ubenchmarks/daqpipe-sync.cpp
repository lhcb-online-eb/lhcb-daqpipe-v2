/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include "common.hpp"

/********************  GLOBALS  *********************/
static char ** buffer = NULL;
const char * name = "daqpipe-sync";
MPI_Comm comm;

/*******************  FUNCTION  *********************/
void runPrep(int rank,int nodes)
{
	//allocate
	buffer = new char*[BUFFER_COUNT];
	for (int i = 0 ; i < BUFFER_COUNT ; i++) {
		buffer[i] = alloc(MEM_SIZE);
		printAlignment(buffer[i]);
		for (size_t j = 0 ; j < MEM_SIZE ; j++)
			buffer[i][j] = (char)j;
	}
}

/*******************  FUNCTION  *********************/
void runClean(int rank,int nodes)
{
	//free mem
	for (int i = 0 ; i < BUFFER_COUNT ; i++)
		dealloc(buffer[i],MEM_SIZE);
	delete [] buffer;
}

/*******************  FUNCTION  *********************/
void runReadoutUnit(int rank,int nodes, size_t repeat)
{
	int start = rank - rank % 2;
	int cnt = 0;
	for (int i = 0 ; i < repeat ; i++)
	{
		for (int step = 0 ; step < nodes / 2 ; step++)
		{
			//track
			MPI_Request requests[4];
			
			//target node
			int target = (start + step*2)%nodes;
			
			//send 2 data
			MPI_Isend(buffer[SELECTOR*(cnt++) % BUFFER_COUNT],BUFFER_SIZE,MPI_CHAR,target,0,MPI_COMM_WORLD,&requests[0]);
			MPI_Isend(buffer[SELECTOR*(cnt++) % BUFFER_COUNT],BUFFER_SIZE,MPI_CHAR,target,1,MPI_COMM_WORLD,&requests[1]);
			
			//send meta data
			MPI_Isend(buffer[SELECTOR*(cnt++) % BUFFER_COUNT],10*1024,MPI_CHAR,target,2,MPI_COMM_WORLD,&requests[2]);
			MPI_Isend(buffer[SELECTOR*(cnt++) % BUFFER_COUNT],10*1024,MPI_CHAR,target,3,MPI_COMM_WORLD,&requests[3]);
			
			//wait
			MPI_Status status[4];
			//printf("waitall\n");
			MPI_Waitall(4,requests,status);
			
			//barrier on RUs
			//printf("barrier\n");
			MPI_Barrier(comm);
		}
	}
}

/*******************  FUNCTION  *********************/
void runBuilderUnit(int rank,int nodes, size_t repeat)
{
	int cnt = 0;
	for (int i = 0 ; i < repeat ; i++)
	{
		for (int step = 0 ; step < nodes / 2 ; step++)
		{
			//track
			MPI_Request requests[4];
			
			//send 2 data
			MPI_Irecv(buffer[SELECTOR*(cnt++) % BUFFER_COUNT],BUFFER_SIZE,MPI_CHAR,MPI_ANY_SOURCE,0,MPI_COMM_WORLD,&requests[0]);
			MPI_Irecv(buffer[SELECTOR*(cnt++) % BUFFER_COUNT],BUFFER_SIZE,MPI_CHAR,MPI_ANY_SOURCE,1,MPI_COMM_WORLD,&requests[1]);
			
			//send meta data
			MPI_Irecv(buffer[SELECTOR*(cnt++) % BUFFER_COUNT],10*1024,MPI_CHAR,MPI_ANY_SOURCE,2,MPI_COMM_WORLD,&requests[2]);
			MPI_Irecv(buffer[SELECTOR*(cnt++) % BUFFER_COUNT],10*1024,MPI_CHAR,MPI_ANY_SOURCE,3,MPI_COMM_WORLD,&requests[3]);
			
			//wait
			MPI_Status status[4];
			MPI_Waitall(4,requests,status);
		}
	}
}

/*******************  FUNCTION  *********************/
void runUnit(int rank, int nodes, size_t repeat)
{
	//create comm
	MPI_Comm_split(MPI_COMM_WORLD,rank%2,0,&comm);
	
	if (rank % 2 == 0)
		runBuilderUnit(rank,nodes,repeat);
	else
		runReadoutUnit(rank,nodes,repeat);
}

/*******************  FUNCTION  *********************/
size_t memSize(int rank,int nodes,int local)
{
	return (2*BUFFER_SIZE + 2*10*1024)*(long)REPEAT*(nodes/2);
}
