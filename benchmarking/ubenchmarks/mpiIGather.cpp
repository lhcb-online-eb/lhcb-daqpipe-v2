/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#undef NDEBUG

/********************  HEADERS  *********************/
#include <mpi.h>
#include <cstdio>
#include <cstdlib>
#include <unistd.h>
#include <cassert>
#include <ctime>
#include <string>

#ifdef __APPLE__
#include <mach/clock.h>
#include <mach/mach.h>
#endif

/********************  MACROS  **********************/
#define BUFFER_SIZE (10UL*1024UL*1024UL)
#define REPEAT 10000
#define CREDITS 2
#define MPI_MASTER 0
#define ONFLY 1

/********************  STRUCT  **********************/
struct RequestArray
{
	RequestArray(void);
	void gather(void * sendBuffer,int sendSize,void * recvBuffer,int recvSize, int root);
	int getId(void);
	void waitAll(void);
	MPI_Request requests[ONFLY];
	bool available[ONFLY];
	MPI_Comm coms[ONFLY];
};

/*******************  FUNCTION  *********************/
RequestArray::RequestArray ( void )
{
	for (int i = 0 ; i < ONFLY ; i++)
	{
		requests[i] = MPI_REQUEST_NULL;
		available[i] = true;
		MPI_Comm_dup(MPI_COMM_WORLD,&coms[i]);
	}
}

/*******************  FUNCTION  *********************/
int RequestArray::getId ( void )
{
	//find empty
	int id = -1;
	for (int i = 0 ; i < ONFLY ; i++)
		if (available[i])
		{
			id = i;
			break;
		}
	
	//if not avail
	if (id == -1)
	{
		MPI_Status status;
		int ret = MPI_Waitany(ONFLY,requests,&id,&status);
		assert(ret == 0);
		assert(id >= 0 && id < ONFLY);
		available[id] = true;
		requests[id] = MPI_REQUEST_NULL;
	}
	
	return id;
}

/*******************  FUNCTION  *********************/
void RequestArray::gather ( void* sendBuffer, int sendSize, void* recvBuffer, int recvSize, int root )
{
	//get a free id
	int id = getId();
	
	//push send
	#ifdef MPI_Igather
		MPI_Igather(sendBuffer,sendSize,MPI_CHAR,recvBuffer,recvSize,MPI_CHAR,root,coms[id],&requests[id]);
	#else //MPI_Igather
		fprintf(stderr,"MPI_Igather is not supported by you MPI version, please check for MPI 3.0 compatibility.\n");
		abort();
	#endif //MPI_Igather
	available[id] = false;
}

/*******************  FUNCTION  *********************/
void RequestArray::waitAll ( void )
{
	MPI_Status status[ONFLY];
	int ret = MPI_Waitall(ONFLY,requests,status);
	assert(ret == 0);
}

/*******************  FUNCTION  *********************/
std::string getHostname()
{
	char buffer[1024];
	gethostname(buffer,sizeof(buffer));
	return buffer;
}

/*******************  FUNCTION  *********************/
int main(int argc, char ** argv)
{
	//setup MPI
	MPI_Init(&argc,&argv);
	
	//get info
	int nodes;
	int rank;
	MPI_Comm_rank(MPI_COMM_WORLD,&rank);
	MPI_Comm_size(MPI_COMM_WORLD,&nodes);
	
	//print
	if (rank == MPI_MASTER)
	{
		printf("Try round robin gather on %d nodes\n",nodes);
		printf("Use : \n  - buffer size : %lu\n  - repeat : %d\n  - credits : %d  - onfly requests : %d\n",BUFFER_SIZE,REPEAT,CREDITS,ONFLY);
	}
	
	//wait all
	MPI_Barrier(MPI_COMM_WORLD);
	
	//setup
	RequestArray req;
	char * sendBuffer = new char[BUFFER_SIZE];
	char * recvBuffer = new char[BUFFER_SIZE * nodes];
	
	//get start time
	struct timespec tstart, tend;

#ifndef __APPLE__
  clock_gettime(CLOCK_REALTIME, &tstart);
#else
  clock_serv_t cclock;
  mach_timespec_t mts;
  host_get_clock_service(mach_host_self(), CALENDAR_CLOCK, &cclock);
  clock_get_time(cclock, &mts);
  mach_port_deallocate(mach_task_self(), cclock);
  tstart.tv_sec = mts.tv_sec;
  tstart.tv_nsec = mts.tv_nsec;
#endif
	
	//loop
	for (int i = 0 ; i < REPEAT ; i++)
		for (int j = 0 ; j < nodes ; j++)
			req.gather(sendBuffer,BUFFER_SIZE,recvBuffer,BUFFER_SIZE * nodes,j);
	req.waitAll();
	
	//get end time
#ifndef __APPLE__
  clock_gettime(CLOCK_REALTIME, &tend);
#else
  host_get_clock_service(mach_host_self(), CALENDAR_CLOCK, &cclock);
  clock_get_time(cclock, &mts);
  mach_port_deallocate(mach_task_self(), cclock);
  tend.tv_sec = mts.tv_sec;
  tend.tv_nsec = mts.tv_nsec;
#endif
	
	//free mem
	delete [] sendBuffer;
	delete [] recvBuffer;
	
	//get elapsed
	long long int timeDiff = (tend.tv_sec - tstart.tv_sec) * 1000000000 + (tend.tv_nsec - tstart.tv_nsec);
	float elapsed = timeDiff / 1000000000.0f;
	
	//print rate
	printf("Rank %d as rate : %f Gb/s\n",rank,(float)((8*BUFFER_SIZE*(long)REPEAT*nodes/2)/(1024UL*1024UL*1024UL))/elapsed);
	
	//finish MPI
	MPI_Finalize();
	
	return EXIT_SUCCESS;
}
