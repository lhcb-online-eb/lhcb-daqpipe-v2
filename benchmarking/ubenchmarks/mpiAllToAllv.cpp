/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include <cstring>
#include "common.hpp"

/********************  GLOBALS  *********************/
static char * inBuffer = NULL;
static char * outBuffer = NULL;
const char * name = "all-to-allv";

/********************  STRUCT  **********************/
struct DummySize
{
	size_t cnt;
	size_t bw;
	size_t from;
	size_t to;
};


/*******************  CONSTS  ***********************/
#define cstRealSizesCnt 7
static DummySize cstRealSizes[cstRealSizesCnt] = {
	{52,55},//Velo
	{68,70},//UT
	{144,46},//SCIFI
	{65,71},//Rich1
	{36,73},//Rich2
	{52,90},//Calo
	{28,92},//muron
};

/*******************  FUNCTION  *********************/
void runPrep(int rank,int nodes)
{
	//allocate memory
	inBuffer = alloc(BUFFER_SIZE*nodes);
	outBuffer = alloc(BUFFER_SIZE*nodes);
	
	//setuyp
	memset(outBuffer,0,BUFFER_SIZE);
}

/*******************  FUNCTION  *********************/
void runClean(int rank,int nodes)
{
	//free mem
	dealloc(inBuffer,MEM_SIZE*nodes);
	dealloc(outBuffer,MEM_SIZE* nodes);
}

/*******************  FUNCTION  *********************/
int getNodeSize(int rank, int nodes) {
	//compute sum and max and prepare from to
	size_t max = 0;
	size_t cnt = 0;
	size_t size = 0;
	for (int i = 0 ; i < cstRealSizesCnt ; i++)
	{
		if (cstRealSizes[i].bw > max)
			max = cstRealSizes[i].bw;
		cstRealSizes[i].from = cnt;
		cnt += cstRealSizes[i].cnt;
		cstRealSizes[i].to = cnt;
	}
	
	//compute position
	size_t pos = (cnt * rank) / nodes;
	
	//extract size
	for (int i = 0 ; i < cstRealSizesCnt ; i++)
		if (pos >= cstRealSizes[i].from && pos < cstRealSizes[i].to)
			size = (100*cstRealSizes[i].bw);
		
	//ret
	return ((size / max) * BUFFER_SIZE) / 100;
}

/*******************  FUNCTION  *********************/
void runUnit( int rank, int nodes, size_t repeat )
{
	//info
	//printf("Rank %d on %s is started\n",rank,getHostname().c_str());

	//select size for this rank
	size_t size = getNodeSize(rank,nodes);

	//build arrays
	int * inputSize = new int[nodes];
	int * outputSize = new int[nodes];
	int * offset = new int[nodes];

	//fill
	for (int i = 0 ; i < nodes; i ++) {
		outputSize[i] = getNodeSize(i,nodes);
		inputSize[i] = size;
		offset[i] = BUFFER_SIZE * i;
	}
	
	//need to receive all
	for (size_t i = 0 ; i < repeat / 100 ; i++)
		MPI_Alltoallv(inBuffer, inputSize, offset, MPI_CHAR,
					outBuffer, outputSize, offset, MPI_CHAR,
					MPI_COMM_WORLD);

	//clear
	delete inputSize;
	delete outputSize;
	delete offset;
}

/*******************  FUNCTION  *********************/
size_t memSize(int rank,int nodes,int local)
{
	return (getNodeSize(rank,nodes)*(long)REPEAT*(nodes - local)) / (long)100;
}
