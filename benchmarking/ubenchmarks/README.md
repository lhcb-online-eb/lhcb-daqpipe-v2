Micro-benchmarks
================

This directory provide some simple micro-benchmarks to evaluate some properties of the way to
echange the data. It replay roughly the semantic from DAQPIPE but not strictly.

mpiRoundRobinGather
-------------------

Simple benchmark where all RU send as many data as possible to BU. In their side BUs read as most
data as they can without taking care from where it come and where it as to be stored.

You can choose to run the RU/BU in the same process or not :

	./mpiRoundRobinGather --grouped
	./mpiRoundRobinGather --single

mpiGather
---------

Try to use NODES number of MPI_IGather to check the related performance, it suppose that
RU and BU are hosted in the same process. This one require the MPI 3.0 feature MPI_IGather and
only be compiled if available.
