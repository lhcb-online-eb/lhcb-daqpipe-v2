/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include <cstring>
#include "common.hpp"

/********************  GLOBALS  *********************/
static char * inBuffer = NULL;
static char * outBuffer = NULL;
const char * name = "all-to-all";

/*******************  FUNCTION  *********************/
void runPrep(int rank,int nodes)
{
	//allocate memory
	inBuffer = alloc(BUFFER_SIZE*nodes);
	outBuffer = alloc(BUFFER_SIZE*nodes);
	
	//setuyp
	memset(outBuffer,0,BUFFER_SIZE);
}

/*******************  FUNCTION  *********************/
void runClean(int rank,int nodes)
{
	//free mem
	dealloc(inBuffer,MEM_SIZE*nodes);
	dealloc(outBuffer,MEM_SIZE* nodes);
}

/*******************  FUNCTION  *********************/
void runUnit( int rank, int nodes, size_t repeat )
{
	//info
	//printf("Rank %d on %s is started\n",rank,getHostname().c_str());
	
	//need to receive all
	for (size_t i = 0 ; i < repeat / 100 ; i++)
		MPI_Alltoall(inBuffer, BUFFER_SIZE, MPI_CHAR,
					outBuffer, BUFFER_SIZE, MPI_CHAR,
					MPI_COMM_WORLD);
}

/*******************  FUNCTION  *********************/
size_t memSize(int rank,int nodes,int local)
{
	return (BUFFER_SIZE*(long)REPEAT*(nodes - local)) / (long)100;
}
