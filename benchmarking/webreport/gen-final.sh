#!/bin/bash

set -x
set -e

function gen()
{
	input=$1
	output=$2

	rm -rfv final/$output
	cp -r ~/Projects/lhcb-daqpipe-v2/benchmarking/webreport/dist final/$output

	mkdir -p final/$output/scripts/
	node ~/Projects/lhcb-daqpipe-v2/benchmarking/webreport/gen-all.js "$input" > "final/$output/scripts/daqpipe-bench.js"

	rm -rfvd $input/webreport
	cp -r ~/Projects/lhcb-daqpipe-v2/benchmarking/webreport $input
	rm -rfvd $input/webreport/node_modules $input/webreport/bower_compo*
	tar -cvjf final/$output/$input.tar.bz2 $input
}

gen benchmark-broadwell2.broadwell2.net-opa-all-2016-02-25 opa-2016-02-25
gen benchmark-broadwell2.broadwell2.net-dapl-ch64-all-2016-02-26 ib-edr-2016-02-26
