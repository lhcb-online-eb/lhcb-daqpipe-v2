'use strict';

describe('Controller: PatchCtrl', function () {

  // load the controller's module
  beforeEach(module('webreportApp'));

  var PatchCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PatchCtrl = $controller('PatchCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(PatchCtrl.awesomeThings.length).toBe(3);
  });
});
