'use strict';

describe('Controller: OsuCtrl', function () {

  // load the controller's module
  beforeEach(module('webreportApp'));

  var OsuCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    OsuCtrl = $controller('OsuCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(OsuCtrl.awesomeThings.length).toBe(3);
  });
});
