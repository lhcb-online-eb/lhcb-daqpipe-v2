'use strict';

describe('Controller: IfconfigCtrl', function () {

  // load the controller's module
  beforeEach(module('webreportApp'));

  var IfconfigCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    IfconfigCtrl = $controller('IfconfigCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(IfconfigCtrl.awesomeThings.length).toBe(3);
  });
});
