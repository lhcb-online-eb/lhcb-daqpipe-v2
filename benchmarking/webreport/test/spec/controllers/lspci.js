'use strict';

describe('Controller: LspciCtrl', function () {

  // load the controller's module
  beforeEach(module('webreportApp'));

  var LspciCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    LspciCtrl = $controller('LspciCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(LspciCtrl.awesomeThings.length).toBe(3);
  });
});
