'use strict';

describe('Controller: RunsCtrl', function () {

  // load the controller's module
  beforeEach(module('webreportApp'));

  var RunsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    RunsCtrl = $controller('RunsCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(RunsCtrl.awesomeThings.length).toBe(3);
  });
});
