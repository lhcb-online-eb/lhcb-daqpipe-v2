var fs = require('fs');
var find = require('find');

/*******************  FUNCTION  *********************/
var path = process.argv[2];
if (path == undefined)
{
	console.error("You need to provide root path of benchmark results as parameter");
	process.exit(1);
}

/*******************  FUNCTION  *********************/
function extractCpuInfos()
{
	var cpuinfo = fs.readFileSync(path+'/infos/cpuinfo.txt').toString();
	cpuinfo = cpuinfo.split('\n');
	var info = {};
	var gbl = [];
	
	//loop on entries
	for (var i in cpuinfo)
	{
		if (cpuinfo[i] == '')
		{
			gbl.push(info);
			info = {};
		} else {
			var infos = cpuinfo[i].split(':');
			info[infos[0].trim()] = infos[1];
		}
	}
	
	//remove last empty one
	gbl.pop();
	
	//return
	return gbl;
}

/*******************  FUNCTION  *********************/
function getVersions()
{
	var files = fs.readFileSync(path+'/infos/versions.txt').toString();
	var lines = files.split('\n');
	var final = {};
	for (var i in lines)
	{
		var tmp = lines[i].split(':');
		final[tmp[0]] = tmp[1];
	}
	return final;
}

/*******************  FUNCTION  *********************/
function genInfos()
{
	var final = {};
	final.hwlocls = fs.readFileSync(path+'/infos/cpu.svg')
		.toString()
		.replace('<?xml version="1.0" encoding="UTF-8"?>\n',"");
	final.ifconfig = fs.readFileSync(path+'/infos/ifconfig.txt').toString();
	final.lspci = fs.readFileSync(path+'/infos/lspci.txt').toString();
	final.cpuinfo = extractCpuInfos();
	final.patchv1 = fs.readFileSync(path+'/infos/v1-git-diff.patch').toString();
	final.patchv2 = fs.readFileSync(path+'/infos/v2-git-diff.patch').toString();
	final.infos = {};
	final.infos.date = fs.readFileSync(path+'/infos/date.txt').toString();
	final.infos.gitref = fs.readFileSync(path+'/infos/v1-git-ref.txt').toString()+"\n"+fs.readFileSync(path+'/infos/v2-git-ref.txt').toString();
	final.infos.hosts = fs.readFileSync(path+'/infos/hosts').toString();
	final.infos.note = fs.readFileSync(path+'/infos/note.txt').toString();
	final.versions = getVersions();
	final.configs = {
		'config.json':fs.readFileSync(path+'/infos/config.json').toString()
	};
	return final;
}

/*******************  FUNCTION  *********************/
function loadData(version,file)
{
	var col = 7;
	var search = 'BUMaster:';
	if (version == 'v2')
	{
		col = 9;
		search = '[MON]';
	}
	var file = fs.readFileSync(file).toString();
	file = file.split("\n");
	var ret = [];
	var check = /^-?[0-9]+(\.[0-9]+)?$/;
	for (var i in file)
	{
		if (file[i].indexOf(search) != -1)
		{
			var value = file[i].split(' ')[col];
			if (check.test(value))
				ret.push(value);
		}
	}
	return ret;
}

/*******************  FUNCTION  *********************/
function genData(callback)
{
	var data = {};
	var regexp = new RegExp("lhcb-daqpipe-(v[1-2])/([a-zA-Z]+)-([a-zA-Z0-9-]+)/([a-z]+)-([0-9]+).txt");
	find.file(path, function(files) {
		for (var i in files)
		{
			var args = regexp.exec(files[i]);
			if (args != null)
			{
				version=args[1];
				mode=args[2];
				variant=args[3];
				param=args[4];
				value=args[5];

				if (data[version] == undefined)
					data[version] = {};
				if (data[version][mode] == undefined)
					data[version][mode] = {};
				if (data[version][mode][variant] == undefined)
					data[version][mode][variant] = {};
				if (data[version][mode][variant][param] == undefined)
					data[version][mode][variant][param] = {};
				if (data[version][mode][variant][param][value] == undefined)
					data[version][mode][variant][param][value] = {};
				data[version][mode][variant][param][value] = loadData(version,files[i]);
			}
		}
		callback(data);
		//console.log(JSON.stringify(data));
		//console.log(JSON.stringify(data,null,'\t'));
	});
	
	return data;
}

/*******************  FUNCTION  *********************/
function loadUbenchData(file)
{
	var search = 'rate';
	var file = fs.readFileSync(file).toString();
	file = file.split("\n");
	var ret = [];
	for (var i in file)
	{
		if (file[i].indexOf(search) != -1)
		{
			var value = file[i].split(' ')[5];
			ret.push(value);
		}
	}

	return ret;
}

/*******************  FUNCTION  *********************/
function genUbenchData(callback)
{
	var data = {};
	var regexp = new RegExp("ubenchmarks/([A-Za-z-]+)/([A-Za-z]+)-([0-9]+).txt");
	find.file(path+"/ubenchmarks/", function(files) {
		for (var i in files)
		{
			var args = regexp.exec(files[i]);
			if (args != null)
			{
				ppn="ppn-1";
				mode=args[1];
				param=args[2];
				value=args[3];

				if (data[ppn] == undefined)
					data[ppn] = {};
				if (data[ppn][mode] == undefined)
					data[ppn][mode] = {};
				if (data[ppn][mode][param] == undefined)
					data[ppn][mode][param] = {};
				if (data[ppn][mode][param][value] == undefined)
					data[ppn][mode][param][value] = {};
				data[ppn][mode][param][value] = loadUbenchData(files[i]);
			}
		}
		callback(data);
		//console.log(JSON.stringify(data));
		//console.log(JSON.stringify(data,null,'\t'));
	});
	
	return data;
}

/*******************  FUNCTION  *********************/
function genOSU()
{
	var osu = {
		"osu_bw": {},
		"osu_bibw": {}
	};
	
	for (var i in osu)
	{
		var out = {};
		var data = fs.readFileSync(path+'/osu/'+i+'.txt').toString().split('\n');
		for (var j in data)
		{
			if (data[j][0] != '#')
			{
				var d = data[j].replace(/ +/,' ');
				d = d.split(' ');
				osu[i][d[0]] = d[1];
			}
		}
	}
	
	return osu;
}

/*******************  FUNCTION  *********************/
function genBEff()
{
	var data = fs.readFileSync(path+'/b_eff/b_eff.plot').toString().split('\n');
	var out = {};
	var cur = '';
	
	for (var j in data)
	{
		if (data[j][0] == '#')
		{
			cur = data[j].slice(2);
			out[cur] = {};
		} else {
			var tmp = data[j].split(' ').filter(function(v) {return v != '';});
			out[cur][tmp[0]] = tmp.slice(1);
		}
	}
	
	return out;
}

/*******************  FUNCTION  *********************/
function genStream()
{
	var data = fs.readFileSync(path+'/stream/summary.txt').toString();
	data = data.split('\n');
	var out = {};
	for (var i in data)
	{
		if (data[i] != '' && data[i][0] != '#')
		{
			var d = data[i].split('\t');
			out[d[0]] = d[1].split(',')[0];
		}
	}
	return out;
}

/*******************  FUNCTION  *********************/
function genErrors(callback)
{
	var data = {};
	find.file(path, function(files) {
		for (var i in files)
		{
			var paths = files[i].split('/');
			var fname = paths.pop();
			var dir = paths.pop();
			if (/err.txt$/.test(fname))
				data[dir] = fs.readFileSync(files[i]).toString();
		}
		callback(data);
	});
}

/*******************  FUNCTION  *********************/
var all = {
	osu: genOSU(),
	infos: genInfos(),
	stream: genStream(),
	//beff: genBEff()
};

genData(function(data){
	all.data = data;
	
	genUbenchData(function(udata) {
		all.ubenchmark = udata;
		
		genErrors(function(data) {
			all.errors = data;
			console.log("daqpipeBenchmark = "+JSON.stringify(all,null,'\t')+";");
			console.log("bench = daqpipeBenchmark.infos;");
			console.log("benchdata = daqpipeBenchmark.data;");
		});
	});
});
