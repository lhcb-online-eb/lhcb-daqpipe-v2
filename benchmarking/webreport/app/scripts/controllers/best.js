'use strict';

/**
 * @ngdoc function
 * @name webreportApp.controller:BestCtrl
 * @description
 * # BestCtrl
 * Controller of the webreportApp
 */
angular.module('webreportApp')
  .controller('BestCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
