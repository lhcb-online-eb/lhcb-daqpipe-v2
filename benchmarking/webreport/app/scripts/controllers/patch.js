'use strict';

/**
 * @ngdoc function
 * @name webreportApp.controller:PatchCtrl
 * @description
 * # PatchCtrl
 * Controller of the webreportApp
 */
angular.module('webreportApp')
  .controller('PatchCtrl', function ($scope) {
    $scope.patchv1 = bench.patchv1;
	$scope.patchv2 = bench.patchv2;
  });
