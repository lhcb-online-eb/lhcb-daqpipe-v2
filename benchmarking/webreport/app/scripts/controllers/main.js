'use strict';

/**
 * @ngdoc function
 * @name webreportApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the webreportApp
 */
var module = angular.module('webreportApp');

module.controller('MainCtrl', function ($scope) {
	$scope.cpuinfo = bench.cpuinfo[bench.cpuinfo.length-1];
	$scope.infos = bench.infos;
	$scope.lspci = bench.lspci;	
	$scope.configs = bench.configs;
	$scope.versions = bench.versions;
});

module.directive('hwloclstopo', function() {
    return {
        restrict: 'E',
        template: bench.hwlocls,
        replace: true
    };
});