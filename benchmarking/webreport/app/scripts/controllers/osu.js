'use strict';

function genOSUData(select,data)
{
	var d = [];
	for (var i in data[select])
		d.push(parseFloat(data[select][i]*8/1024));
	return {
		name: select,
		data: d
	}
}

function getOSUChart(data)
{
	var cats = [];
	for (var i in data['osu_bw'])
		cats.push(i.toString());
	
	var ret = {
		 options: {
			chart: {
			},
// 			subtitle: {
// 				text: 'Source: <a href="https://en.wikipedia.org/wiki/World_population">Wikipedia.org</a>'
// 			},
			xAxis: {
				title: {
					text: "Size (bytes)",
				}
			},
			yAxis: {
				min: 0,
				title: {
					text: 'Bandwidth (Gb/s)',
					align: 'high'
				},
				labels: {
					overflow: 'justify'
				}
			}
		},
		//The below properties are watched separately for changes.

		//Series object (optional) - a list of series using normal Highcharts series options.
		series: [genOSUData('osu_bw',data),genOSUData('osu_bibw',data)],
		//Title configuration (optional)
		title: {
			text: "OSU benchmark"
		},
		//Boolean to control showing loading status on chart (optional)
		//Could be a string if you want to show specific loading text.
		loading: false,
		//Configuration for the xAxis (optional). Currently only one x axis can be dynamically controlled.
		//properties currentMin and currentMax provided 2-way binding to the chart's maximum and minimum
		xAxis: {
			categories: cats
			//title: {text: "Mode"}
		},
		//Whether to use Highstocks instead of Highcharts (optional). Defaults to false.
		useHighStocks: false,
		//size (optional) if left out the chart will default to size of the div or something sensible.
		size: {
// 		width: 400,
		height: 600
		},
		//function (optional)
		func: function (chart) {
		//setup some logic for the chart
		}
	};
	
	console.log(JSON.stringify(ret.series));
	
	return ret;
}

/**
 * @ngdoc function
 * @name webreportApp.controller:OsuCtrl
 * @description
 * # OsuCtrl
 * Controller of the webreportApp
 */
angular.module('webreportApp')
	.controller('OsuCtrl', function ($scope) {
		$scope.osuChart = getOSUChart(daqpipeBenchmark.osu);
	});
