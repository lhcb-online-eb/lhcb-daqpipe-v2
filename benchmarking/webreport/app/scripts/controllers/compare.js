'use strict';

function getMedianMax(data,version,mode,variant)
{
	var max = 0;
	for (var param in data[version][mode][variant])
	{
		if (param != 'nodes')
		{
			for (var value in data[version][mode][variant][param])
			{
				var sorted = data[version][mode][variant][param][value].sort(function(a,b) { return a - b;});
				var q2 = Math.floor(sorted.length/2);
				var median = sorted[q2];
				if (median > max)
					max = parseFloat(median);
			}
		}
	}
	return max;
}

function genCompareSeries(data)
{
	var out = {
		mpi:{},
		libfabric:{}
	}
	
	for (var version in data)
		for (var mode in data[version])
			for (var variant in data[version][mode])
				out[mode][version+" "+variant] = getMedianMax(data,version,mode,variant);

	var cats = genCompareSeriesCategories(data);
	var final = {
		mpi:[],
		libfabric:[]
	};

	for (var i in cats)
	{
		if (out['mpi'][cats[i]] == undefined)
			final['mpi'].push(0);
		else
			final['mpi'].push(out['mpi'][cats[i]]);
		if (out['libfabric'][cats[i]] == undefined)
			final['libfabric'].push(0);
		else
			final['libfabric'].push(out['libfabric'][cats[i]]);
	}
	
	var ret = [{
			name: 'mpi',
			data: final.mpi
		}, {
			name: 'libfabric',
			data: final.libfabric
		}];
		
	return ret;
}

function genCompareSeriesCategories(data)
{
	var cat = {};
	for (var version in data)
	{
		for (var mode in data[version])
		{
			for (var variant in data[version][mode])
				cat[version+" "+variant] = true;
		}
	}
	
	var cats = [];
	for (var i in cat)
		cats.push(i);
	return cats;
}

function getCompareChart(data)
{
	var ret = {
		 options: {
			chart: {
				type: 'bar'
			},
// 			subtitle: {
// 				text: 'Source: <a href="https://en.wikipedia.org/wiki/World_population">Wikipedia.org</a>'
// 			},
			xAxis: {
				categories: genCompareSeriesCategories(data),
				title: {
					text: null
				}
			},
			yAxis: {
				min: 0,
				title: {
					text: 'Bandwidth (Gb/s)',
					align: 'high'
				},
				labels: {
					overflow: 'justify'
				}
			},
			plotOptions: {
				bar: {
					dataLabels: {
						enabled: true
					}
				}
			},
			legend: {
				layout: 'vertical',
				align: 'right',
				verticalAlign: 'top',
				x: -40,
				y: 80,
				floating: true,
				borderWidth: 1,
				backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
				shadow: true
			},
			credits: {
				enabled: false
			},
		},
		//The below properties are watched separately for changes.

		//Series object (optional) - a list of series using normal Highcharts series options.
		series: genCompareSeries(data),
		//Title configuration (optional)
		title: {
			text: "Run entries"
		},
		//Boolean to control showing loading status on chart (optional)
		//Could be a string if you want to show specific loading text.
		loading: false,
		//Configuration for the xAxis (optional). Currently only one x axis can be dynamically controlled.
		//properties currentMin and currentMax provided 2-way binding to the chart's maximum and minimum
		xAxis: {
			title: {text: "Mode"}
		},
		//Whether to use Highstocks instead of Highcharts (optional). Defaults to false.
		useHighStocks: false,
		//size (optional) if left out the chart will default to size of the div or something sensible.
		size: {
// 		width: 400,
		height: 600
		},
		//function (optional)
		func: function (chart) {
		//setup some logic for the chart
		}
	};
	
	return ret;
}

/**
 * @ngdoc function
 * @name webreportApp.controller:CompareCtrl
 * @description
 * # CompareCtrl
 * Controller of the webreportApp
 */
angular.module('webreportApp')
	.controller('CompareCtrl', function ($scope) {
		$scope.compareChart = getCompareChart(benchdata);
	});
