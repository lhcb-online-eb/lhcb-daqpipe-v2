'use strict';

function getUBenchDataChart(title,param,data)
{
	var ret = {
		 options: {
			chart: {
				type: 'boxplot'
			},

			legend: {
				enabled: false
			},

			xAxis: {
				categories: genValues(data.mpiRoundRobinGather),
			},

			yAxis: {
				title: {
					text: 'Bandwidth (Gb/s)'
				},
				plotLines: [{
					value: 932,
					color: 'red',
					width: 1,
					label: {
						text: 'Theoretical mean: 932',
						align: 'center',
						style: {
							color: 'gray'
						}
					}
				}],
				min:0
			},
			legend: {
				layout: 'horizontal',
				align: 'center',
				verticalAlign: 'bottom',
// 				x: 0,
// 				y: 100,
				borderWidth: 1,
				backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
				shadow: true
			},
		},
		//The below properties are watched separately for changes.

		//Series object (optional) - a list of series using normal Highcharts series options.
		series:  [
			{
				name: 'one-to-one',
				data: genMinMaxMedianData(data["one-to-one"]),
				tooltip: {
					headerFormat: '<em>Value: {point.key}</em><br/>'
				}
			},
			{
				name: 'many-to-one',
				data: genMinMaxMedianData2(data["many-to-one"]),
				tooltip: {
					headerFormat: '<em>Value: {point.key}</em><br/>'
				}
			},
			{
				name: 'mpiRoundRobinGather',
				data: genMinMaxMedianData(data.mpiRoundRobinGather),
				tooltip: {
					headerFormat: '<em>Value: {point.key}</em><br/>'
				}
			},
			{
				name: 'mpiRoundRobinGatherCmd',
				data: genMinMaxMedianData(data.mpiRoundRobinGatherWithCmd),
				tooltip: {
					headerFormat: '<em>Value: {point.key}</em><br/>'
				}
			},
			{
				name: 'mpiRoundRobinGatherCmdMeta',
				data: genMinMaxMedianData(data.mpiRoundRobinGatherWithCmdMeta),
				tooltip: {
					headerFormat: '<em>Value: {point.key}</em><br/>'
				}
			}
		],
		//Title configuration (optional)
		title: {
			text: title
		},
		//Boolean to control showing loading status on chart (optional)
		//Could be a string if you want to show specific loading text.
		loading: false,
		//Configuration for the xAxis (optional). Currently only one x axis can be dynamically controlled.
		//properties currentMin and currentMax provided 2-way binding to the chart's maximum and minimum
		xAxis: {
			title: {text: param},
			gridLineWidth: 1,
		},
		//Whether to use Highstocks instead of Highcharts (optional). Defaults to false.
		useHighStocks: false,
		//size (optional) if left out the chart will default to size of the div or something sensible.
		size: {
// 		width: 400,
// 		height: 300
		},
		//function (optional)
		func: function (chart) {
		//setup some logic for the chart
		}
	};
	
	return ret;
}

function getBestCompare(data,ppn,param,mode)
{
	var best = 0;
	var d = genMinMaxMedianData(extractData(data,ppn,param)[mode]);
	console.log(d);
	for (var i in d)
	{
		if (d[i][2] - best > 0)
			best = d[i][2];
		if (mode == 'many-to-one' && d[i][4] - best > 0)
			best = d[i][4];
	}
	
	console.log("===>"+best);
	return best;
}

function getBestCompare2(data,ppn,param,mode)
{
	var best = getBestCompare(data,ppn,'size',mode);
	var tmp = getBestCompare(data,ppn,'onfly',mode);
	if (tmp > best)
		return tmp;
	else
		return best;
}

function genCompareSeries2(data)
{
	var ret = [
		{
			name: "one-to-one",
			data: [
				+getBestCompare2(data,'ppn-1','size','one-to-one'),
				//+getBestCompare2(data,'ppn-100','size','one-to-one')
			]
		},
		{
			name: "many-to-one",
			data: [
				+getBestCompare2(data,'ppn-1','size','many-to-one'),
				//+getBestCompare2(data,'ppn-100','size','mpiRoundRobinGather')
			]
		},
		{
			name: "mpiRoundRobinGather",
			data: [
				+getBestCompare2(data,'ppn-1','size','mpiRoundRobinGather'),
				//+getBestCompare2(data,'ppn-100','size','mpiRoundRobinGather')
			]
		},
		{
			name: "mpiRoundRobinGatherWithCmd",
			data: [
				+getBestCompare2(data,'ppn-1','size','mpiRoundRobinGatherWithCmd'),
				//+getBestCompare2(data,'ppn-100','size','mpiRoundRobinGatherWithCmd')
			]
		},
		{
			name: "mpiRoundRobinGatherWithCmdMeta",
			data: [
				+getBestCompare2(data,'ppn-1','size','mpiRoundRobinGatherWithCmdMeta'),
				//+getBestCompare2(data,'ppn-100','size','mpiRoundRobinGatherWithCmdMeta')
			]
		}
	];
	console.log(ret);
	return ret;
}

function getCompareChart2(data)
{
	var ret = {
		 options: {
			chart: {
				type: 'bar'
			},
// 			subtitle: {
// 				text: 'Source: <a href="https://en.wikipedia.org/wiki/World_population">Wikipedia.org</a>'
// 			},
			xAxis: {
				categories: ['ppn-1'],
				title: {
					text: "Processes"
				}
			},
			yAxis: {
				min: 0,
				title: {
					text: 'Bandwidth (Gb/s)',
					align: 'high'
				},
				labels: {
					overflow: 'justify'
				}
			},
			plotOptions: {
				bar: {
					dataLabels: {
						enabled: true
					}
				}
			},
			legend: {
				layout: 'horizontal',
				align: 'center',
				verticalAlign: 'bottom',
				floating: false,
				borderWidth: 1,
				backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
				shadow: true
			},
			credits: {
				enabled: false
			},
		},
		//The below properties are watched separately for changes.

		//Series object (optional) - a list of series using normal Highcharts series options.
		series: genCompareSeries2(data),
		//Title configuration (optional)
		title: {
			text: "Best performance"
		},
		//Boolean to control showing loading status on chart (optional)
		//Could be a string if you want to show specific loading text.
		loading: false,
		//Configuration for the xAxis (optional). Currently only one x axis can be dynamically controlled.
		//properties currentMin and currentMax provided 2-way binding to the chart's maximum and minimum
		xAxis: {
			title: {text: "Processes per node"}
		},
		//Whether to use Highstocks instead of Highcharts (optional). Defaults to false.
		useHighStocks: false,
		//size (optional) if left out the chart will default to size of the div or something sensible.
		size: {
// 		width: 400,
		height: 600
		},
		//function (optional)
		func: function (chart) {
		//setup some logic for the chart
		}
	};
	
	return ret;
}

function extractData(data,ppn,param)
{
	if (data[ppn] == undefined || data[ppn]["mpiRoundRobinGather"][param] == undefined)
		return {mpiRoundRobinGather:{},mpiRoundRobinGatherWithCmd:{},mpiRoundRobinGatherWithCmdMeta:{}};
	else
		return {
			mpiRoundRobinGather: data[ppn]["mpiRoundRobinGather"][param],
			mpiRoundRobinGatherWithCmd: data[ppn]["mpiRoundRobinGatherWithCmd"][param],
			mpiRoundRobinGatherWithCmdMeta: data[ppn]["mpiRoundRobinGatherWithCmdMeta"][param],
			'one-to-one': data[ppn]["one-to-one"][param],
			'many-to-one': data[ppn]["many-to-one"][param]
		};
}


/**
 * @ngdoc function
 * @name webreportApp.controller:UbenchmarkCtrl
 * @description
 * # UbenchmarkCtrl
 * Controller of the webreportApp
 */
angular.module('webreportApp')
	.controller('UbenchmarkCtrl', function ($scope) {
		$scope.compareChart = getCompareChart2(daqpipeBenchmark.ubenchmark);
		$scope.chartSize1 = getUBenchDataChart('1 buffer, size','Communictions size (Kb)',extractData(daqpipeBenchmark.ubenchmark,'ppn-1','size'));
// 		$scope.chartSize2 = getUBenchDataChart('100 buffers, size','Communictions size (Kb)',extractData(daqpipeBenchmark.ubenchmark,'ppn-100','size'));
		$scope.chartOnfly1 = getUBenchDataChart('1 buffer, onfly','On fly communictions',extractData(daqpipeBenchmark.ubenchmark,'ppn-1','onfly'));
// 		$scope.chartOnfly2 = getUBenchDataChart('100 buffers, onfly','On fly communictions',extractData(daqpipeBenchmark.ubenchmark,'ppn-100','onfly'));
		$scope.chartNodes1 = getUBenchDataChart('1 buffer, nodes','Nodes',extractData(daqpipeBenchmark.ubenchmark,'ppn-1','nodes'));
// 		$scope.chartNodes2 = getUBenchDataChart('100 buffers, nodes','Nodes',extractData(daqpipeBenchmark.ubenchmark,'ppn-100','nodes'));
	});
