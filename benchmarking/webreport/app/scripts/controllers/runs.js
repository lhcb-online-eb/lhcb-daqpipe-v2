'use strict';


function genTxtData(data)
{
	var minMax = genMinMaxMedianData(data)
	var final = "";
	var cnt = 0;
	for (var i in data)
		final += i+"\t"+ minMax[cnt++].join("\t")+"\n";
	return final;
}

/**
 * @ngdoc function
 * @name webreportApp.controller:RunsCtrl
 * @description
 * # RunsCtrl
 * Controller of the webreportApp
 */
angular.module('webreportApp')
	.controller('RunsCtrl', function ($scope) {
		$scope.benchdata = benchdata;
		$scope.modes = getModes(benchdata);
		$scope.selected = genSelected(benchdata);

		$scope.redraw = function()
		{
			$scope.txtNodes = genTxtData(getSelectedData(this.benchdata,this.selected,'nodes'));
			$scope.chartNodes = getDataChart('Scalability over nodes','Nodes',getSelectedData(this.benchdata,this.selected,'nodes'));
			$scope.txtSize = genTxtData(getSelectedData(this.benchdata,this.selected,'size'));
			$scope.chartSize = getDataChart('Size parameter','Size (KB)',getSelectedData(this.benchdata,this.selected,'size'));
			$scope.txtCredits = genTxtData(getSelectedData(this.benchdata,this.selected,'c'));
			$scope.chartCredits = getDataChart('Credits parameter','Credits',getSelectedData(this.benchdata,this.selected,'c'));
			$scope.txtFreq = genTxtData(getSelectedData(this.benchdata,this.selected,'freq'));
			$scope.chartFreq = getDataChart('Freqency parameter','Frequency',getSelectedData(this.benchdata,this.selected,'freq'));
			$scope.txtChan = genTxtData(getSelectedData(this.benchdata,this.selected,'channels'));
			$scope.chartChan = getDataChart('Channels parameter','Channels',getSelectedData(this.benchdata,this.selected,'channels'));
			$scope.txtRandom = genTxtData(getSelectedData(this.benchdata,this.selected,'random'));
			$scope.chartChan = getDataChart('Random ratio parameter','Random ratio',getSelectedData(this.benchdata,this.selected,'random'));
		}
		
		$scope.redraw($scope);
	});
