'use strict';

/**
 * @ngdoc function
 * @name webreportApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the webreportApp
 */
angular.module('webreportApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
