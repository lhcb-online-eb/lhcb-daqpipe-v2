'use strict';

/**
 * @ngdoc function
 * @name webreportApp.controller:ErrorsCtrl
 * @description
 * # ErrorsCtrl
 * Controller of the webreportApp
 */
angular.module('webreportApp')
	.controller('ErrorsCtrl', function ($scope) {
		var err = "";
		for (var i in daqpipeBenchmark.errors)
		{
			if (err != '')
				err += "\n\n======================================================\n\n";
			err += daqpipeBenchmark.errors[i];
		}
		$scope.err = err;
	});
