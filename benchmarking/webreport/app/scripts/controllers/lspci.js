'use strict';

/**
 * @ngdoc function
 * @name webreportApp.controller:LspciCtrl
 * @description
 * # LspciCtrl
 * Controller of the webreportApp
 */
angular.module('webreportApp')
	.controller('LspciCtrl', function ($scope) {
		$scope.lspci2 = bench.lspci;
	});
