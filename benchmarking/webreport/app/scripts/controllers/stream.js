'use strict';

function genStreamData(data)
{
	var d = [];
	for (var i in data)
		d.push(parseFloat(data[i]*8/1024));
	return {
		name: i,
		data: d
	}
}

function genStreamCsvData(data)
{
	var out = [ "#threads\tcopy_bw(Mb/s)" ];
	for (var i in data)
		out.push(i+"\t"+data[i]);
	return out.join('\n');
}

function getStreamChart(data)
{
	var cats = [];
	for (var i in data['osu-bw'])
		cats.push(i.toString());
	
	var ret = {
		 options: {
			chart: {
			},
			xAxis: {
				title: {
					text: "Threads",
				}
			},
			yAxis: {
				min: 0,
				title: {
					text: 'Bandwidth (Gb/s)',
					align: 'high'
				},
				labels: {
					overflow: 'justify'
				}
			}
		},
		//The below properties are watched separately for changes.

		//Series object (optional) - a list of series using normal Highcharts series options.
		series: [genStreamData(data)],
		//Title configuration (optional)
		title: {
			text: "Stream benchmark"
		},
		//Boolean to control showing loading status on chart (optional)
		//Could be a string if you want to show specific loading text.
		loading: false,
		//Configuration for the xAxis (optional). Currently only one x axis can be dynamically controlled.
		//properties currentMin and currentMax provided 2-way binding to the chart's maximum and minimum
// 		xAxis: {
// 			categories: cats
			//title: {text: "Mode"}
// 		},
		//Whether to use Highstocks instead of Highcharts (optional). Defaults to false.
		useHighStocks: false,
		//size (optional) if left out the chart will default to size of the div or something sensible.
		size: {
// 		width: 400,
// 		height: 900
		},
		//function (optional)
		func: function (chart) {
		//setup some logic for the chart
		}
	};
	
	console.log(JSON.stringify(ret.series));
	
	return ret;
}

/**
 * @ngdoc function
 * @name webreportApp.controller:StreamCtrl
 * @description
 * # StreamCtrl
 * Controller of the webreportApp
 */
angular.module('webreportApp')
	.controller('StreamCtrl', function ($scope) {
		$scope.streamData = genStreamCsvData(daqpipeBenchmark.stream);
		$scope.chartStream = getStreamChart(daqpipeBenchmark.stream);
	});
