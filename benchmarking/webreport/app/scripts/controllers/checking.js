'use strict';

function genDataEntries(data)
{
	var out = [];
	for (var version in data)
	{
		for (var mode in data[version])
		{
			for (var variant in data[version][mode])
			{
				for (var entry in data[version][mode][variant])
				{
					for (var value in data[version][mode][variant][entry])
						out.push(data[version][mode][variant][entry][value].length);
				}
			}
		}
	}
	
	return out;
}

function genDataEntriesCategories(data)
{
	var cat = [];
	for (var version in data)
	{
		for (var mode in data[version])
		{
			for (var variant in data[version][mode])
			{
				for (var entry in data[version][mode][variant])
				{
					for (var value in data[version][mode][variant][entry])
						cat.push(version+" "+mode+" "+variant+" "+entry+" "+value);
				}
			}
		}
	}
	
	return cat;
}

function getCheckChart(data)
{
	var ret = {
		 options: {
			chart: {
				type: 'bar'
			},
			title: {
				text: 'Historic World Population by Region'
			},
			subtitle: {
				text: 'Source: <a href="https://en.wikipedia.org/wiki/World_population">Wikipedia.org</a>'
			},
			xAxis: {
				categories: genDataEntriesCategories(data),
				title: {
					text: null
				}
			},
			yAxis: {
				min: 0,
				title: {
					text: 'Population (millions)',
					align: 'high'
				},
				labels: {
					overflow: 'justify'
				}
			},
			plotOptions: {
				bar: {
					dataLabels: {
						enabled: true
					}
				}
			},
			legend: {
				layout: 'vertical',
				align: 'right',
				verticalAlign: 'top',
				x: -40,
				y: 80,
				floating: true,
				borderWidth: 1,
				backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
				shadow: true
			},
			credits: {
				enabled: false
			},
		},
		//The below properties are watched separately for changes.

		//Series object (optional) - a list of series using normal Highcharts series options.
		series: [{
			name: 'Entries',
			data: genDataEntries(data)
		}],
		//Title configuration (optional)
		title: {
			text: "Run entries"
		},
		//Boolean to control showing loading status on chart (optional)
		//Could be a string if you want to show specific loading text.
		loading: false,
		//Configuration for the xAxis (optional). Currently only one x axis can be dynamically controlled.
		//properties currentMin and currentMax provided 2-way binding to the chart's maximum and minimum
		xAxis: {
			title: {text: "Mode"}
		},
		//Whether to use Highstocks instead of Highcharts (optional). Defaults to false.
		useHighStocks: false,
		//size (optional) if left out the chart will default to size of the div or something sensible.
		size: {
// 		width: 400,
		height: 4000
		},
		//function (optional)
		func: function (chart) {
		//setup some logic for the chart
		}
	};
	
	return ret;
}

/**
 * @ngdoc function
 * @name webreportApp.controller:CheckingCtrl
 * @description
 * # CheckingCtrl
 * Controller of the webreportApp
 */
angular.module('webreportApp')
	.controller('CheckingCtrl', function ($scope) {
		$scope.dataEntries = getCheckChart(benchdata);
	});
