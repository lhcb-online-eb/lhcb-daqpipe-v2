'use strict';

/**
 * @ngdoc overview
 * @name webreportApp
 * @description
 * # webreportApp
 *
 * Main module of the application.
 */
angular
  .module('webreportApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
	'highcharts-ng'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
      .when('/lspci', {
        templateUrl: 'views/lspci.html',
        controller: 'LspciCtrl',
        controllerAs: 'lspci'
      })
      .when('/ifconfig', {
        templateUrl: 'views/ifconfig.html',
        controller: 'IfconfigCtrl',
        controllerAs: 'ifconfig'
      })
      .when('/runs', {
        templateUrl: 'views/runs.html',
        controller: 'RunsCtrl',
        controllerAs: 'runs'
      })
      .when('/compare', {
        templateUrl: 'views/compare.html',
        controller: 'CompareCtrl',
        controllerAs: 'compare'
      })
      .when('/ubenchmark', {
        templateUrl: 'views/ubenchmark.html',
        controller: 'UbenchmarkCtrl',
        controllerAs: 'ubenchmark'
      })
      .when('/OSU', {
        templateUrl: 'views/osu.html',
        controller: 'OsuCtrl',
        controllerAs: 'OSU'
      })
      .when('/patch', {
        templateUrl: 'views/patch.html',
        controller: 'PatchCtrl',
        controllerAs: 'patch'
      })
      .when('/checking', {
        templateUrl: 'views/checking.html',
        controller: 'CheckingCtrl',
        controllerAs: 'checking'
      })
      .when('/best', {
        templateUrl: 'views/best.html',
        controller: 'BestCtrl',
        controllerAs: 'best'
      })
      .when('/stream', {
        templateUrl: 'views/stream.html',
        controller: 'StreamCtrl',
        controllerAs: 'stream'
      })
      .when('/errors', {
        templateUrl: 'views/errors.html',
        controller: 'ErrorsCtrl',
        controllerAs: 'errors'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
