function genMinMaxMedianData(data)
{
	var values = genValues(data);
	
	//reorder based on int
	var tmp = {};
	for (var i in data)
	{
		var cp = parseInt(i);
		tmp[cp] = data[i];
	}
	
	var out = [];
	for (var i in tmp)
	{
		var lst = tmp[i];
		if (lst.length == 0)
			lst.push(0);
		//min max
		var min = Math.min.apply(null, lst);
		var max = Math.max.apply(null, lst);
		//quartils
		var sorted = lst.sort(function(a,b) { return a - b;});
		var q1 = Math.floor(sorted.length/4);
		var q2 = Math.floor(sorted.length/2);
		var q3 = Math.floor((3*sorted.length)/4);
		out.push([min,sorted[q1],sorted[q2],sorted[q3],max]);
	}
	return out;
}

function genMinMaxMedianData2(data)
{
	var values = genValues(data);
	
	//reorder based on int
	var tmp = {};
	for (var i in data)
	{
		var cp = parseInt(i);
		tmp[cp] = data[i];
	}
	
	var out = [];
	for (var i in tmp)
	{
		var lst = tmp[i];
		if (lst.length == 0)
			lst.push(0);
		//min max
		var min = Math.min.apply(null, lst);
		var max = Math.max.apply(null, lst);
		out.push([max,max,max,max,max]);
	}
	return out;
}

function genValues(data)
{
	var values = [];
	for (var i in data)
		values.push(i);
	return values.sort(function(a,b) { return a - b;});
} 


function getDataChart(title,param,data)
{
	var ret = {
		 options: {
			chart: {
				type: 'boxplot'
			},

			legend: {
				enabled: false
			},

			xAxis: {
				categories: genValues(data),
			},

			yAxis: {
				title: {
					text: 'Bandwidth (Gb/s)'
				},
				plotLines: [{
					value: 932,
					color: 'red',
					width: 1,
					label: {
						text: 'Theoretical mean: 932',
						align: 'center',
						style: {
							color: 'gray'
						}
					}
				}],
				min:0
			}
		},
		//The below properties are watched separately for changes.

		//Series object (optional) - a list of series using normal Highcharts series options.
		series:  [{
			name: 'Observations',
			data: genMinMaxMedianData(data),
			tooltip: {
				headerFormat: '<em>Value: {point.key}</em><br/>'
			}
		}],
		//Title configuration (optional)
		title: {
			text: title
		},
		//Boolean to control showing loading status on chart (optional)
		//Could be a string if you want to show specific loading text.
		loading: false,
		//Configuration for the xAxis (optional). Currently only one x axis can be dynamically controlled.
		//properties currentMin and currentMax provided 2-way binding to the chart's maximum and minimum
		xAxis: {
			title: {text: param}
		},
		//Whether to use Highstocks instead of Highcharts (optional). Defaults to false.
		useHighStocks: false,
		//size (optional) if left out the chart will default to size of the div or something sensible.
		size: {
// 		width: 400,
// 		height: 300
		},
		//function (optional)
		func: function (chart) {
		//setup some logic for the chart
		}
	};
	
	return ret;
}

function genSelected(data)
{
	var modes = getModes(data);
	var selected = modes[0];
	return selected;
}

function getSelectedData(data,selected,sel)
{
// 	return data.v1.libfabric.std.c;
	var tmp = data[selected.version][selected.mode][selected.variant];
	if (tmp == undefined)
		return [];
	else if (tmp[sel] == undefined)
		return [];
	else
		return tmp[sel];
}

function getModes(data)
{
	var modes = [];
	
	for (var version in data)
	{
		for (var mode in data[version])
		{
			for (var variant in data[version][mode])
			{
				modes.push({
					id: modes.length,
					version: version,
					mode: mode,
					variant: variant
				});
			}
		}
	}
	
	return modes;
}
