######################################################
#            PROJECT  : lhcb-daqpipe                 #
#            VERSION  : 2.5.0-dev                    #
#            DATE     : 12/2017                      #
#            AUTHOR   : Valat Sébastien - CERN       #
#            LICENSE  : CeCILL-C                     #
######################################################

######################################################
find_package(MPI QUIET)

######################################################
#check for openmp for some parallel tests
find_package(OpenMP QUIET)
if (OPENMP_FOUND)
	set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
	set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
endif()

######################################################
if (MPI_FOUND)
	set(CMAKE_CXX_COMPILER ${MPI_CXX_COMPILER})
	add_executable(mini-daqpipe mini-daqpipe.cpp)
	add_executable(mini-daqpipe-mt mini-daqpipe-mt.cpp)
endif (MPI_FOUND)
