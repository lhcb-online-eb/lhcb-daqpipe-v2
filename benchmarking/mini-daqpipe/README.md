Mini-DAQPIPE
============

This application is a simplified version of DAQPIPE written in MPI. It mostly keep the DAQPIPE pattern
by dropping the multi-driver and multi-protocol support. It implement the PULL protocol only with barrel 
shifting.

With this implementation a process is either a readout unit or a builder unit. Node 0 (process 0) host an
event manager. Process 1 does nothing.

The ideal run condition is to use two process per nodes.

With OpenMPI :

```sh
	mpirun --map-by ppr:2:node -np 32 ./min-daqpipe
```

With Mpich :

```
	mpirun --ppn 2 -np 32 ./mini-daqpipe
```

Options:
--------

Mini-daqpipe support the following options : 

 * `-s {SIZE}` : Setup the data messages size in bytes (default is 1 MB).
 * `-c {CREDITS}` : Number of credits to be used in the builder unit (default is 1).
 * `-p {PARALLEL}`: Number of nodes to communicate with (default is 8).
 * `-t {RUNTIME}` : Time to run before exiting (default is 10).
 * `-r {RAND_PERCENT}` : Use random sizes which is max minus the given percentage (default is 0)
 * `-w {WARMUP}` : Skip the N first outputs to warmup (default is 0)
 * `-e` : Disable usage of event manager.

Caution
-------

The credit (`-c`) support is currently implemented by using threads which might be wrong with MPI without calling
MPI_Init_threads. But when calling it the performance drop. It seems to work like this with thread but
to be checked.
