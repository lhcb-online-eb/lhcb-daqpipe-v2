/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#undef NDEBUG

/********************  HEADERS  *********************/
//std
#include <cstdlib>
#include <getopt.h>
#include <cassert>
#include <iostream>
#include <cstring>
#include <iomanip>
//c++11
#include <thread>
//unix
#include <unistd.h>
//mpi
#include <mpi.h>

/*******************  NAMESPACE  ********************/
using namespace std;

/*********************  TYPES  **********************/
/** RANK of event manganer **/
#define EVENT_MANAGER_RANK 0
/** Number of rails (data/meta segments) to consider **/
#define RAILS 2
/** Maximum size of a data segment **/
#define DATA_SIZE (gblArgs.size)
/** Maximum size of a meta data segment (1/100 of the data size) */
#define META_DATA_SIZE (gblArgs.size/100)
/** Maximum numer of onfly communications **/
#define MAX_ONFLY 4096
/** Maximum number of credits **/
#define MAX_CREDIT 128
/** Number of event stored into the readout unit buffer **/
#define RU_STORED 256
/** Delay between two print of the bandwidth (in seconds) **/
#define BW_DELAY 2

/********************  ENUM  ************************/
/** List of special MPI tags in use **/
enum Tags
{
	/** Request to be sent to the event manager to get an event id to fetch **/
	TAG_EM_REQUEST,
	/** Answer from event manager containing the id of the event to fetch **/
	TAG_EM_EVENT_ASSIGN,
	/** Data request from the builder unit to the readout unit. **/
	TAG_RU_REQUEST,
	/** Exit message to stop running **/
	TAG_EXIT,
	/** Base free id for the data/meta-data messages. **/
	TAG_BASE
};

/********************  STRUCT  **********************/
/**
 * Option handled by the program
**/
struct Args
{
	/** Number of credits, number of event to fetch at the same time in the builder unit **/
	int credits;
	/** Number of nodes to communicate with at the same time in the builder unit **/
	int parallel;
	/** Size of a data segement **/
	size_t size;
	/** Time to run **/
	int time;
	/** Number of messages to send at the same time in the readout unit **/
	int psend;
	/** Use random size. Percentage. **/
	int rand;
	/** Skip the N first output to be considered as warm up **/
	int warmup;
	/** Enable usage of event manager, or disable it **/
	bool eventManager;
	/** Count number of processes per node **/
	int ppn;
};

/********************  STRUCT  **********************/
/**
 * Request to be sent by the builder unit to the readout unit to request the data.
**/
struct ReadoutRequest
{
	/** Event ID to send **/
	int event;
	/** Tags to use to identify the meta data of each rail **/
	int metaTag[RAILS];
	/** Tags to use to identify the data of each rail **/
	int dataTag[RAILS];
};

/********************  GLOBALS  *********************/
/** Map of boolean saying if the given rank is localated on the same node than the current one **/
static bool * gblIsLocal = NULL;
/** Global arguments of the program **/
static Args gblArgs = {
	1,
	8,
	1024*1024,
	10,
	32,
	0,
	0,
	true,
	2
};

/********************  STRUCT  **********************/
/**
 * Handle many MPI request with a limit, automatically manage the assignement to the free
 * requests and wait if there is not anymore rooms.
**/
class RequestArray
{
	public:
		RequestArray(int cnt);
		void isend(void * buffer,size_t size,int target,int tag);
		void irecv( void* buffer, size_t size, int tag, int source );
		int getId(void);
		void waitAll(void);
		bool testAll(void);
		size_t testany(void);
	private:
		/** List of MPI requets to track **/
		MPI_Request requests[MAX_ONFLY];
		/** Is the MPI requets available **/
		bool available[MAX_ONFLY];
		/** Keep track of the remote rank assigned to the request **/
		int rank[MAX_ONFLY];
		/** Keep track of the size as some MPI implementation seems to not work when using MPI_Get_count **/
		int msgSize[MAX_ONFLY];
		/** Max number of active request. Should be below MAX_ONFLY. **/
		int cnt;
};

/*******************  FUNCTION  *********************/
/**
 * Constructor. It init the MPI requests to NULL.
**/
RequestArray::RequestArray ( int cnt )
{
	assert(cnt <= MAX_ONFLY);
	this->cnt = cnt;
	for (int i = 0 ; i < MAX_ONFLY ; i++) {
		requests[i] = MPI_REQUEST_NULL;
		available[i] = true;
	}
}

/*******************  FUNCTION  *********************/
/**
 * Get a free ID of wait one request to finish.
**/
int RequestArray::getId ( void )
{
	//find empty
	int id = -1;
	for (int i = 0 ; i < cnt ; i++) {
		if (available[i]) {
			id = i;
			break;
		}
	}
	
	//if not avail
	if (id == -1) {
		MPI_Status status;
		int ret = MPI_Waitany(cnt,requests,&id,&status);
		assert(ret == 0);
		assert(id >= 0 && id < cnt);
		available[id] = true;
		requests[id] = MPI_REQUEST_NULL;
	}
	
	return id;
}

/*******************  FUNCTION  *********************/
/**
 * Perform a MPI_Isend request.
**/
void RequestArray::isend ( void* buffer, size_t size, int target, int tag )
{
	//get a free id
	int id = getId();
	
	//push send
	MPI_Isend(buffer,size,MPI_CHAR,target,tag,MPI_COMM_WORLD,&requests[id]);
	available[id] = false;
	rank[id] = target;
	msgSize[id] = size;
}

/*******************  FUNCTION  *********************/
/**
 * Perform a MPI_Irecv request.
**/
void RequestArray::irecv ( void* buffer, size_t size, int tag = MPI_ANY_TAG, int source = MPI_ANY_SOURCE )
{
	//get a free id
	int id = getId();
	
	//push send
	MPI_Irecv(buffer,size,MPI_CHAR,source,tag,MPI_COMM_WORLD,&requests[id]);
	available[id] = false;
	rank[id] = source;
	msgSize[id] = size;
}

/*******************  FUNCTION  *********************/
/**
 * Wait all the request to finish.
**/
void RequestArray::waitAll ( void )
{
	//printf("wait all\n");
	MPI_Status status[MAX_ONFLY];
	int ret = MPI_Waitall(cnt,requests,status);
	assert(ret == 0);
	
	for (int id = 0 ; id < cnt ; id++) {
		available[id] = true;
		requests[id] = MPI_REQUEST_NULL;
	}
}

/*******************  FUNCTION  *********************/
/**
 * Test is all the request are finished.
 * @return Return true if all are finished, false otherwise.
**/
bool RequestArray::testAll()
{
	//printf("wait all\n");
	MPI_Status status[MAX_ONFLY];
	int flag;
	int ret = MPI_Testall(cnt,requests,&flag,status);
	assert(ret == 0);
	
	if (flag) {
		for (int id = 0 ; id < cnt ; id++) {
			available[id] = true;
			requests[id] = MPI_REQUEST_NULL;
		}
		return true;
	} else {
		return false;
	}
}

/*******************  FUNCTION  *********************/
/**
 * Test is one of the request finish. It it finish it return its size
 * if the communication append with a remote node. Otherwise it return 0.
**/
size_t RequestArray::testany ( void )
{
	MPI_Status status;
	int index;
	int flag;
	MPI_Testany(cnt,requests,&index,&flag,&status);
	if (flag && index != MPI_UNDEFINED){
		available[index] = true;
		requests[index] = MPI_REQUEST_NULL;
		
		if (gblIsLocal[rank[index]])
			return 0;
		else
			return msgSize[index];
	} else {
		return 0;
	}
}

/******************* FUNCTION *********************/
/**
 * Build the local map to know which ranks are local to the compute node or not.
 * @param worldSize Define the number of rank in use.
**/
bool * buildLocalMap(int worldSize)
{
	char buffer[2048];
	bool * map = new bool[worldSize];
	gethostname(buffer,sizeof(buffer));
	char * all = new char[2048 * worldSize];
	MPI_Allgather(buffer, 2048, MPI_CHAR, all, 2048, MPI_CHAR, MPI_COMM_WORLD);
	
	//build local map
	for (int rank = 0 ; rank < worldSize ; rank++) {
		map[rank] = (strcmp(all+(rank*2048),buffer) == 0);
	}
	
	delete [] all;
	
	return map;
}

/*******************  FUNCTION  *********************/
/**
 * Parse the arguments.
**/
void parseArgs(int argc, char ** argv)
{
	int c;
	while ((c = getopt(argc,argv,"s:c:p:t:r:w:eh")) != -1) {
		switch(c) {
			case 'h':
				fprintf(stderr,"%s [-s {SIZE}] [-c {CREDITS}] [-p {PARALLEL}] [-t {RUNTIME}] [-r {RAND_PERCENT}] [-w {WARMUP}] [-e]\n",argv[0]);
				MPI_Finalize();
				exit(0);
				break;
			case 's':
				gblArgs.size = atol(optarg);
				assert(gblArgs.size > 100);
				break;
			case 'c':
				gblArgs.credits = atoi(optarg);
				assert(gblArgs.credits >= 1);
				assert(gblArgs.credits <= MAX_CREDIT);
				if (gblArgs.credits > 1)
					printf("Caution, credits use threads which might hurt MPI\n");
				break;
			case 'p':
				gblArgs.parallel = atoi(optarg);
				assert(gblArgs.parallel > 0);
				break;
			case 't':
				gblArgs.time = atoi(optarg);
				break;
			case 'r':
				gblArgs.rand = atoi(optarg);
				assert(gblArgs.rand >= 0);
				break;
			case 'w':
				gblArgs.warmup = atoi(optarg);
				break;
			case 'e':
				gblArgs.eventManager = false;
				break;
		}
	}
}

/*******************  FUNCTION  *********************/
/**
 * Run the event manger which assign the event to the builder units.
**/
void eventManager(int worldSize)
{
	//state
	int nextEvent = 0;
	double tstart = MPI_Wtime();
	double tcur = tstart;
	int activeBU = (worldSize/2-1) * gblArgs.credits;
	bool doExit = false;
	int exitCode = 42;
	
	//loop
	while (activeBU > 0) {
		//wait a request
		char command[64];
		MPI_Status status;
		MPI_Recv(command, 64, MPI_CHAR, MPI_ANY_SOURCE, TAG_EM_REQUEST,MPI_COMM_WORLD, &status);
		
		//build assignement
		int event = nextEvent++;
		int source = status.MPI_SOURCE;
		
		//send assignement or exit
		if (doExit) {
			MPI_Send(&exitCode,1,MPI_INT,source,TAG_EXIT,MPI_COMM_WORLD);
			activeBU--;
		}
		//cout << "Send assign : " << event << " => " << source << endl;
		MPI_Send(&event,1,MPI_INTEGER,source,TAG_EM_EVENT_ASSIGN,MPI_COMM_WORLD);
		
		//check timout
		tcur = MPI_Wtime();
		if (tcur - tstart > gblArgs.time)
			doExit = true;
	}
	
	//send exit to RUs
	for (int i = 2 ; i < worldSize ; i++)
		if (i % 2 == 0)
			MPI_Send(&exitCode,1,MPI_INT,i,TAG_EXIT,MPI_COMM_WORLD);
}

/*******************  FUNCTION  *********************/
/**
 * Run the readout unit which send the data to the builder unit.
**/
void readoutUnit(int worldSize, int rank)
{
	//allocate
	char * meta[RAILS];
	char * data[RAILS];
	for (int i = 0 ; i < RAILS ; i++) {
		meta[i] = new char[RU_STORED * META_DATA_SIZE];
		data[i] = new char[RU_STORED * DATA_SIZE];
		
		memset(meta[i],0,RU_STORED * META_DATA_SIZE);
		memset(data[i],0,RU_STORED * DATA_SIZE);
	}
	
	double tref = MPI_Wtime();
	size_t transmit = 0;
	bool has = true;
	ReadoutRequest readoutRequest;
	MPI_Status status;
	MPI_Request request;
	int flag;
	
	RequestArray reqArr(gblArgs.psend);
	
	//post recv of exit msg
	int exitCode;
	MPI_Request exitRequest;
	MPI_Irecv(&exitCode,1,MPI_INT,0,TAG_EXIT,MPI_COMM_WORLD,&exitRequest);

	//loop
	while (true) {
		//check exit request
		MPI_Test(&exitRequest,&flag,&status);
		if (flag)
			break;
		
		//timing
		double tcur = MPI_Wtime();
		if (tcur - tref > BW_DELAY) {
			if (gblArgs.warmup > 0)
				gblArgs.warmup--;
			else
				printf("[%03d] Bandwidth %0.2lf Gb/s\n",rank,(((double)(transmit)*8.0/(1000.0*1000.0*1000.0*(tcur-tref)))) );
			tref = tcur;
			transmit = 0;
		}
		
		//wait a request
		if (has) {
			MPI_Irecv(&readoutRequest,sizeof(readoutRequest),MPI_CHAR,MPI_ANY_SOURCE,TAG_RU_REQUEST,MPI_COMM_WORLD,&request);
			has = false;
		}
		
		//check
		MPI_Test(&request, &flag, &status);
		
		//check
		if (flag) {	
			//mark to report
			has = true;
			
			//extract
			int id = readoutRequest.event % RU_STORED;
			
			//cout << rank << " get req from " << status.MPI_SOURCE << endl;
		
			//send
			for (int i = 0 ; i < RAILS ; i++) {
				size_t size;
				if (gblArgs.rand == 0)
					size = DATA_SIZE;
				else
					size = DATA_SIZE - rand() % (gblArgs.rand*DATA_SIZE/100);
				//cout << request.metaTag[i] << " " << request.dataTag[i] << endl;
				reqArr.isend(meta[i]+id * META_DATA_SIZE, META_DATA_SIZE, status.MPI_SOURCE, readoutRequest.metaTag[i]);
				reqArr.isend(data[i]+id * DATA_SIZE, size, status.MPI_SOURCE, readoutRequest.dataTag[i]);
			}
		}
		
		//test
		transmit += reqArr.testany();
	}
}

/*******************  FUNCTION  *********************/
/**
 * Implement the builder unit. There is one call to this function for each credit in a dedicated thread.
**/
void builderUnitLoop(int worldSize,int rank,int credit)
{
	//there is as many reader as node - 1 (EventManger)
	int readers = worldSize / 2 - 1;
	int event = 0;

	//allocate buffers
	char * meta[RAILS];
	char * data[RAILS];
	for (int i = 0 ; i < RAILS ; i++) {
		data[i] = new char[DATA_SIZE * readers];
		meta[i] = new char[META_DATA_SIZE * readers];
		
		memset(data[i],0,DATA_SIZE * readers);
		memset(meta[i],0,META_DATA_SIZE * readers);
	}
	
	//Request array
	RequestArray req(gblArgs.parallel*RAILS*2);
	
	//post recv of exit msg
	int exitCode;
	MPI_Request exitRequest;
	MPI_Irecv(&exitCode,1,MPI_INT,0,TAG_EXIT,MPI_COMM_WORLD,&exitRequest);
	
	while (true) {
		MPI_Status status;
		
		//get event id from EventManger
		if (gblArgs.eventManager)
		{
			//send a request
			char command[64];
			MPI_Send(command,64,MPI_CHAR,EVENT_MANAGER_RANK,TAG_EM_REQUEST,MPI_COMM_WORLD);
			
			//wait an answer
			MPI_Recv(&event,1,MPI_INTEGER,EVENT_MANAGER_RANK,TAG_EM_EVENT_ASSIGN,MPI_COMM_WORLD,&status);
			//cout << "Get assign " << event << " => " << rank << endl;
		} else {
			event++;
		}
		
		//check exit request
		int flag;
		MPI_Test(&exitRequest,&flag,&status);
		if (flag)
			break;
		
		//fetch for every reader
		for (int r = 0 ; r < readers ; r++) {
			//get rank from id
			int reader = (r + rank)% readers;
			int targetRank = 2 + 2 * reader;
			
			//prepare req
			ReadoutRequest request;
			request.event = event;
			
			//post recv
			for (int rail = 0 ; rail < RAILS ; rail++) {
				//meta
				int id = req.getId();
				request.metaTag[rail] = TAG_BASE + credit * (gblArgs.parallel*RAILS*2) + id;
				req.irecv(meta[rail]+META_DATA_SIZE * reader,META_DATA_SIZE,request.metaTag[rail],targetRank);

				//data
				id = req.getId();
				request.dataTag[rail] = TAG_BASE + credit * (gblArgs.parallel*RAILS*2) + id;
				req.irecv(data[rail]+DATA_SIZE * reader,DATA_SIZE,request.dataTag[rail],targetRank);
				
				//cout << "=> " << request.metaTag[rail] << " " << request.dataTag[rail] << endl;
			}
			
			//send request
			//cout << "Send request (" << rank << " => " << targetRank << ")" << endl;
			MPI_Send(&request,sizeof(request),MPI_CHAR,targetRank,TAG_RU_REQUEST,MPI_COMM_WORLD);
		}

		//wait all
		//req.waitAll();
		while (!req.testAll()) {
			MPI_Test(&exitRequest,&flag,&status);
			if (flag)
				return;
		}
		
		//cout << "event " << event <<" done !" << endl;
	}
}

/*******************  FUNCTION  *********************/
/**
 * Start the builder units with one thread for each.
**/
void builderUnit(int worldSize, int rank)
{
	std::thread th[MAX_CREDIT];
	for (int c = 0 ; c < gblArgs.credits ; c++) {
		th[c] = std::thread([=]{
			builderUnitLoop(worldSize,rank,c);
		});
	}
	for (int c = 0 ; c < gblArgs.credits ; c++) {
		th[c].join();
	}
}

/*******************  FUNCTION  *********************/
/**
 * Print an header summurizing the run parameters.
**/
void printHeader(int worldSize)
{
	cout << "========== Mini-DAQPIPE =========" << endl;
	cout << "credits  : " << gblArgs.credits << endl;
	cout << "parallel : " << gblArgs.parallel << endl;
	cout << "size     : " << gblArgs.size << endl;
	cout << "time     : " << gblArgs.time << endl;
	cout << "psend    : " << gblArgs.psend << endl;
	cout << "rand     : " << gblArgs.rand << "%" << endl;
	cout << "warmup   : " << gblArgs.warmup << endl;
	cout << "em       : " << gblArgs.eventManager << endl;
	cout << "np       : " << worldSize << endl;
	cout << "ppn      : " << gblArgs.ppn << endl;
	cout << "=================================" << endl;
}

/*******************  FUNCTION  *********************/
int countProcessPerNode(int worldSize)
{
	int cnt = 0;
	for (int i = 0 ; i < worldSize ; i++)
		if (gblIsLocal[i])
			cnt++;
	return cnt;
}

/*******************  FUNCTION  *********************/
int main(int argc, char ** argv)
{
	//setup MPI
	//int provided;
	/*int status = MPI_Init_thread(&argc,&argv,MPI_THREAD_MULTIPLE ,&provided);
	if(status != 0 || provided != MPI_THREAD_MULTIPLE) {
		cerr << "This program requires MPI_THREAD_MULTIPLE from MPI !" << endl;
		return EXIT_FAILURE;
	}*/
	MPI_Init(&argc,&argv);
	
	//get info
	int worldSize;
	int rank;
	MPI_Comm_rank(MPI_COMM_WORLD,&rank);
	MPI_Comm_size(MPI_COMM_WORLD,&worldSize);
	
	//args
	parseArgs(argc,argv);
	assert(gblArgs.parallel < MAX_ONFLY);
	
	//detect local processes
	gblIsLocal = buildLocalMap(worldSize);
	gblArgs.ppn = countProcessPerNode(worldSize);
	
	//print header
	if (rank == 0)
		printHeader(worldSize);
	
	//safety exit in case of deadlock
	std::thread watchDog = std::thread([]{
		sleep(gblArgs.time * 2);
		fprintf(stderr,"Watch dog exit !");
		int flag;
		MPI_Finalized(&flag) ;
		if (!flag)
			MPI_Abort(MPI_COMM_WORLD,1);
	});
	watchDog.detach();
	
	//run
	if (rank == EVENT_MANAGER_RANK) {
		eventManager(worldSize);
	} else if (rank < gblArgs.ppn) {
		//nothing
	} else if (rank % 2 == 0) {
		readoutUnit(worldSize,rank);
	} else {
		builderUnit(worldSize, rank);
	}
	
	//finish MPI
	MPI_Finalize();
	
	return EXIT_SUCCESS;
}
