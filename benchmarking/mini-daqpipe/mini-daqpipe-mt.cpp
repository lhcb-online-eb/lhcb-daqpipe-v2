/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#undef NDEBUG

/********************  HEADERS  *********************/
#include <mpi.h>
#include <cstdlib>
#include <getopt.h>
#include <cassert>
#include <iostream>
#include <thread>
#include <cstring>

/*******************  NAMESPACE  ********************/
using namespace std;

/*********************  TYPES  **********************/
#define EVENT_MANAGER_RANK 0
#define RAILS 2
#define DATA_SIZE (gblArgs.size)
#define META_DATA_SIZE (gblArgs.size/100)
#define MAX_ONFLY 4096
#define RU_STORED 256
#define BW_DELAY 2

/********************  ENUM  ************************/
enum Tags
{
	TAG_EM_REQUEST,
	TAG_EM_EVENT_ASSIN,
	TAG_RU_REQUEST,
	TAG_BASE
};

/********************  STRUCT  **********************/
struct Args
{
	int credits;
	int parallel;
	size_t size;
	int time;
};

/********************  STRUCT  **********************/
struct ReadoutRequest
{
	int event;
	int metaTag[RAILS];
	int dataTag[RAILS];
};

/********************  GLOBALS  *********************/
static Args gblArgs = {
	1,
	8,
	1024*1024,
	10
};

/********************  STRUCT  **********************/
class RequestArray
{
	public:
		RequestArray(int cnt);
		void isend(void * buffer,size_t size,int target,int tag);
		void irecv( void* buffer, size_t size, int tag, int source );
		int getId(void);
		void waitAll(void);
	private:
		MPI_Request requests[MAX_ONFLY];
		bool available[MAX_ONFLY];
		int cnt;
};

/*******************  FUNCTION  *********************/
RequestArray::RequestArray ( int cnt )
{
	this->cnt = cnt;
	for (int i = 0 ; i < MAX_ONFLY ; i++)
	{
		requests[i] = MPI_REQUEST_NULL;
		available[i] = true;
	}
}

/*******************  FUNCTION  *********************/
int RequestArray::getId ( void )
{
	//find empty
	int id = -1;
	for (int i = 0 ; i < cnt ; i++)
		if (available[i])
		{
			id = i;
			break;
		}
	
	//if not avail
	if (id == -1)
	{
		MPI_Status status;
		int ret = MPI_Waitany(cnt,requests,&id,&status);
		assert(ret == 0);
		assert(id >= 0 && id < cnt);
		available[id] = true;
		requests[id] = MPI_REQUEST_NULL;
	}
	
	return id;
}

/*******************  FUNCTION  *********************/
void RequestArray::isend ( void* buffer, size_t size, int target, int tag )
{
	//get a free id
	int id = getId();
	
	//push send
	MPI_Isend(buffer,size,MPI_CHAR,target,tag,MPI_COMM_WORLD,&requests[id]);
	available[id] = false;
}

/*******************  FUNCTION  *********************/
void RequestArray::irecv ( void* buffer, size_t size, int tag = MPI_ANY_TAG, int source = MPI_ANY_SOURCE )
{
	//get a free id
	int id = getId();
	
	//push send
	MPI_Irecv(buffer,size,MPI_CHAR,source,tag,MPI_COMM_WORLD,&requests[id]);
	available[id] = false;
}

/*******************  FUNCTION  *********************/
void RequestArray::waitAll ( void )
{
	//printf("wait all\n");
	MPI_Status status[MAX_ONFLY];
	int ret = MPI_Waitall(cnt,requests,status);
	assert(ret == 0);
	
	for (int id = 0 ; id < cnt ; id++)
	{
		available[id] = true;
		requests[id] = MPI_REQUEST_NULL;
	}
}

/*******************  FUNCTION  *********************/
void parseArgs(int argc, char ** argv)
{
	int c;
	while ((c = getopt(argc,argv,"s:c:p:t:h")) != -1)
	{
		switch(c)
		{
			case 'h':
				fprintf(stderr,"%s [-s {SIZE}] [-c {CREDITS}] [-p {PARALLEL}] [-t {RUNTIME}]\n",argv[0]);
				break;
			case 's':
				gblArgs.size = atol(optarg);
				break;
			case 'c':
				gblArgs.credits = atoi(optarg);
				break;
			case 'p':
				gblArgs.parallel = atoi(optarg);
				break;
			case 't':
				gblArgs.time = atoi(optarg);
				break;
		}
	}
}

/*******************  FUNCTION  *********************/
void eventManager(int nodes)
{
	//state
	int nextEvent = 0;
	
	//loop
	while (true) {
		//wait a request
		char command[64];
		MPI_Status status;
		MPI_Recv(command, 64, MPI_CHAR, MPI_ANY_SOURCE, TAG_EM_REQUEST,MPI_COMM_WORLD, &status);
		
		//build assignement
		int event = nextEvent++;
		int source = status.MPI_SOURCE;
		
		//send assignement
		//cout << "Send assign : " << event << " => " << source << endl;
		MPI_Send(&event,1,MPI_INTEGER,source,TAG_EM_EVENT_ASSIN,MPI_COMM_WORLD);
	}
}

/*******************  FUNCTION  *********************/
void readoutUnit(int nodes, int rank)
{
	//allocate
	char * meta[RAILS];
	char * data[RAILS];
	for (int i = 0 ; i < RAILS ; i++) {
		meta[i] = new char[RU_STORED * META_DATA_SIZE];
		data[i] = new char[RU_STORED * DATA_SIZE];
		
		memset(meta[i],0,RU_STORED * META_DATA_SIZE);
		memset(data[i],0,RU_STORED * DATA_SIZE);
	}
	
	double tref = MPI_Wtime();
	size_t transmit = 0;
	
	//loop
	while (true) {
		//timing
		double tcur = MPI_Wtime();
		if (tcur - tref > BW_DELAY) {
			cout << "[" << rank << "] Bandwidth " << (((double)(transmit)*8.0/(1024.0*1024.0*1024.0*(tcur-tref)))) << "Gb/s" << endl;
			tref = tcur;
			transmit = 0;
		}
		
		//wait a request
		ReadoutRequest request;
		MPI_Status status;
		MPI_Recv(&request,sizeof(request),MPI_CHAR,MPI_ANY_SOURCE,TAG_RU_REQUEST,MPI_COMM_WORLD,&status);
		int id = request.event % RU_STORED;
		//cout << rank << " get req from " << status.MPI_SOURCE << endl;
		
		//send
		#pragma omp task firstprivate(request,status,id) shared(transmit)
		{
			MPI_Request req[RAILS*2];
			
			for (int i = 0 ; i < RAILS ; i++) {
				//cout << request.metaTag[i] << " " << request.dataTag[i] << endl;
				MPI_Isend(meta[i]+id * META_DATA_SIZE, META_DATA_SIZE, MPI_CHAR, status.MPI_SOURCE, request.metaTag[i],MPI_COMM_WORLD, &req[i]);
				MPI_Isend(data[i]+id * DATA_SIZE, DATA_SIZE, MPI_CHAR, status.MPI_SOURCE, request.dataTag[i],MPI_COMM_WORLD, &req[RAILS+i]);
			}
			
			MPI_Waitall(RAILS*2,req,NULL);
			
			#pragma omp atomic
			transmit += RAILS * (META_DATA_SIZE + DATA_SIZE);
		}
	}
}

/*******************  FUNCTION  *********************/
void builderUnitLoop(int nodes,int rank,int credit)
{
	//there is as many reader as node - 1 (EventManger)
	int readers = nodes / 2 - 1;

	//allocate buffers
	char * meta[RAILS];
	char * data[RAILS];
	for (int i = 0 ; i < RAILS ; i++) {
		data[i] = new char[DATA_SIZE * readers];
		meta[i] = new char[META_DATA_SIZE * readers];
		
		memset(data[i],0,DATA_SIZE * readers);
		memset(meta[i],0,META_DATA_SIZE * readers);
	}
	
	//Request array
	RequestArray req(gblArgs.parallel*RAILS*2);
	
	while (true)
	{
		//send a request
		char command[64];
		MPI_Send(command,64,MPI_CHAR,EVENT_MANAGER_RANK,TAG_EM_REQUEST,MPI_COMM_WORLD);
		
		//wait an answer
		int event;
		MPI_Recv(&event,1,MPI_INTEGER,EVENT_MANAGER_RANK,TAG_EM_EVENT_ASSIN,MPI_COMM_WORLD,NULL);
		//cout << "Get assign " << event << " => " << rank << endl;
		
		//fetch for every reader
		for (int r = 0 ; r < readers ; r++)
		{
			//get rank from id
			int reader = (r + rank)% readers;
			int targetRank = 2 + 2 * reader;
			
			//prepare req
			ReadoutRequest request;
			request.event = event;
			
			//post recv
			for (int rail = 0 ; rail < RAILS ; rail++)
			{
				//meta
				int id = req.getId();
				request.metaTag[rail] = TAG_BASE + credit * (gblArgs.parallel*RAILS*2) + id;
				req.irecv(meta[rail]+META_DATA_SIZE * reader,META_DATA_SIZE,request.metaTag[rail],targetRank);

				//data
				id = req.getId();
				request.dataTag[rail] = TAG_BASE + credit * (gblArgs.parallel*RAILS*2) + id;
				req.irecv(data[rail]+DATA_SIZE * reader,DATA_SIZE,request.dataTag[rail],targetRank);
				
				//cout << "=> " << request.metaTag[rail] << " " << request.dataTag[rail] << endl;
			}
			
			//send request
			//cout << "Send request (" << rank << " => " << targetRank << ")" << endl;
			MPI_Send(&request,sizeof(request),MPI_CHAR,targetRank,TAG_RU_REQUEST,MPI_COMM_WORLD);
		}

		//wait all
		req.waitAll();
		
		//cout << "event " << event <<" done !" << endl;
	}
}

/*******************  FUNCTION  *********************/
void builderUnit(int nodes, int rank)
{
	for (int c = 0 ; c < gblArgs.credits ; c++)
	{
		std::thread th = std::thread([=]{
			builderUnitLoop(nodes,rank,c);
		});
		th.join();
	}
}

/*******************  FUNCTION  *********************/
int main(int argc, char ** argv)
{
	//setup MPI
	int provided;
	int status = MPI_Init_thread(&argc,&argv,MPI_THREAD_MULTIPLE ,&provided);
	if(status != 0 || provided != MPI_THREAD_MULTIPLE) {
		cerr << "This program requires MPI_THREAD_MULTIPLE from MPI !" << endl;
		return EXIT_FAILURE;
	}
	
	//get info
	int nodes;
	int rank;
	MPI_Comm_rank(MPI_COMM_WORLD,&rank);
	MPI_Comm_size(MPI_COMM_WORLD,&nodes);
	
	//args
	parseArgs(argc,argv);
	assert(gblArgs.parallel < MAX_ONFLY);
	
	//run
	if (rank == EVENT_MANAGER_RANK) {
		eventManager(nodes);
	} else if (rank == 1) {
		//nothing
	} else if (rank % 2 == 0) {
		readoutUnit(nodes,rank);
	} else {
		builderUnit(nodes, rank);
	}
	
	//finish MPI
	MPI_Finalize();
	
	return EXIT_SUCCESS;
}
