b\_eff
======

This is from https://fs.hlrs.de/projects/par/mpi//b_eff/, you can get the related paper at
http://icc.dur.ac.uk/~tt/b_eff.pdf.

Thanks to Rolf Rabenseifner & Alice E. Koniges for their work.
