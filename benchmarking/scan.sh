#!/bin/bash

MODE=$1
mode=$2
np=$3

DIR=out4-$MODE-$mode-$np$4
mkdir -p $DIR
cp $0 $DIR/


if [ -z "$3" ]; then
        echo "USAGE : $0 {barrel|random} {PUSH|PULL} {16} [extra-dir-name]"
        exit 1
fi

function genconf()
{
cat > $DIR/config-scan.json << EOF
{
        "binding" : 
        {
                "netDevice" : ""
        },
        "debug" : 
        {
                "show" : null
        },
        "drivers" : 
        {
                "libfabric" : 
                {
                        "ep type" : "RDM",
                        "first port" : 4444,
                        "iface" : "ib0",
                        "port range" : 1024,
                        "provider name" : ""
                },
                "slots" : 
                {
                        "cmdRecver" : 4,
                        "listenerSleep" : 100,
                        "maxOnFly" : 256,
                        "maxRdmaRecv" : 128,
                        "maxRdmaSend" : $rdma
                }
        },
        "gather" : 
        {
                "sched" : 
                {
                        "parallel" : $parallel,
                        "policy" : "$MODE"
                }
        },
        "general" : 
        {
                "bandwidthDelay" : 2,
                "collisionSize" : 100,
                "credits" : $credit,
                "dataSizeRandomRatio" : 0,
                "eventRailMaxDataSize" : $size,
                "gatherType" : "sched",
                "mode" : "${mode}",
                "ruStoredEvents" : 4096,
                "runTimeout" : 5
        },
        "testing" : 
        {
                "warmup": 2,
                "dataChecking" : false,
                "oneWay" : $oneway,
                "realisticMemcpyRate" : 500,
                "sameSegment" : false,
                "skipMemcpy" : false,
                "skipMeta" : false,
                "useDriverThreads" : false,
                "useLocalDriverThreads" : false,
                "useTracer" : false,
                "useUnitThreads" : false
        }
}

EOF
}

function getMean()
{
        echo $(cat $fname | grep MON | cut -d ' ' -f 10 | xargs echo | sed -e 's/ /+/g' | bc -l)/$(grep -c MON $fname) | bc -l | xargs printf "%.2f\n" | sed -e "s/,/./g"
}

function getMax()
{
        cat $fname | grep MON | cut -d ' ' -f 10 | sort -n | tail -n 1
}

echo "#credit	parallel	size	rdma	ppn	Mean	Max" >> $DIR/stats.log

for ppn in 2
do
	if [ $ppn -eq 1 ]; then
			oneway=false
	else
			oneway=true
	fi

	for rdma in 128 4 8 32 58 64
	#`seq 1 32` 42 48 50 54 58 64 68 70 76 80 86 96 116 128
	do
		for parallel in 1 2 4 8
		do
			#for size in 32768 65536 131072 262144 524288 1048576 2097152 4194304
			for size in 524288 1048576
			do
				for credit in 1 2 4 8 10 12 16 20
				do
					fname=$DIR/out-${credit}-${parallel}-${size}-${rdma}-${ppn}.log
					echo --------------------------------------------------------------------------
					echo
					echo "credit=${credit} parallel=${parallel} size=${size} rdma=${rdma} ppn=${ppn}"
					echo
					if [ -f $fname ]; then continue; fi
					genconf
					#hydra-mpiexec -np 8 --ppn 1 --hostfile ~/hosts2 
					set -x
					salloc -N $np -n $(($ppn*$np)) -p hipri mpirun ./daqpipe  -f $DIR/config-scan.json -c $credit -t 15 | tee $fname
					set +x
					cp $DIR/config-scan.json $fname.json
					echo "$credit	$parallel	$size	$rdma	$ppn	$(getMean)	$(getMax)" >> $DIR/stats.log
				done
			done
		done
	done
done

