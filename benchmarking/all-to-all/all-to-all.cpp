/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include <mpi.h>
#include <cstdlib>
#include <getopt.h>
#include <cstring>
#include <cassert>
#include "../../extern-deps/from-fftw/cycle.h"

/********************  GLOBALS  *********************/
#define MAX_ONFLY 512

/********************  STRUCT  **********************/
struct RequestArray
{
	RequestArray(int max);
	void isend(void * buffer,size_t size,int target,int tag);
	void irecv( void* buffer, size_t size, int tag, int source );
	int getId(bool recv);
	void waitAll(void);
	MPI_Request requests[MAX_ONFLY*2];
	bool available[MAX_ONFLY*2];
	int ONFLY;
};

/*******************  FUNCTION  *********************/
RequestArray::RequestArray ( int max )
{
	this->ONFLY = max;
	for (int i = 0 ; i < MAX_ONFLY*2 ; i++)
	{
		requests[i] = MPI_REQUEST_NULL;
		available[i] = true;
	}
}

/*******************  FUNCTION  *********************/
int RequestArray::getId ( bool recv )
{
	//start
	int start = 0;
	int cnt = ONFLY;
	if (recv)
	{
		start = MAX_ONFLY;
		cnt = MAX_ONFLY*2;
	}
	
	//find empty
	int id = -1;
	for (int i = start ; i < cnt ; i++)
		if (available[i])
		{
			id = i;
			break;
		}
	
	//if not avail
	if (id == -1)
	{
		MPI_Status status;
		int ret;
		do {
			ret = MPI_Waitany(MAX_ONFLY*2,requests,&id,&status);
		} while (ret >= MAX_ONFLY);
		assert(ret == 0);
		assert(id >= 0 && id < 2*MAX_ONFLY);
		available[id] = true;
		requests[id] = MPI_REQUEST_NULL;
	}
	
	return id;
}

/*******************  FUNCTION  *********************/
void RequestArray::isend ( void* buffer, size_t size, int target, int tag )
{
	//get a free id
	int id = getId(false);
	
	//push send
	MPI_Isend(buffer,size,MPI_CHAR,target,tag,MPI_COMM_WORLD,&requests[id]);
	available[id] = false;
}

/*******************  FUNCTION  *********************/
void RequestArray::irecv ( void* buffer, size_t size, int tag = MPI_ANY_TAG, int source = MPI_ANY_SOURCE )
{
	//get a free id
	int id = getId(true);
	
	//push send
	MPI_Irecv(buffer,size,MPI_CHAR,source,tag,MPI_COMM_WORLD,&requests[id]);
	available[id] = false;
}

/*******************  FUNCTION  *********************/
void RequestArray::waitAll ( void )
{
	//printf("wait all\n");
	MPI_Status status[MAX_ONFLY*2];
	int ret = MPI_Waitall(MAX_ONFLY*2,requests,status);
	assert(ret == 0);
	
	for (int id = 0 ; id < MAX_ONFLY*2 ; id++)
	{
		available[id] = true;
		requests[id] = MPI_REQUEST_NULL;
	}
}

/*******************  FUNCTION  *********************/
void parallelAllToAll(void * buffIn,void * bufOut,size_t size, int rank,int nodes,int parallel)
{
	//setup request
	RequestArray req(parallel);
	
	//post all recv
	for (size_t i = 0 ; i < nodes ; i++)
		req.irecv((char*)bufOut+i*size,size,MPI_ANY_TAG,i);
	
	//post send per packet
	for (int i = 0 ; i < nodes ; i++)
	{
		int k = (i + rank) % nodes;
		req.isend((char*)buffIn+k*size,size,k,k);
		printf("%d k = %d\n",rank,k);
	}
	
	//waitall
	printf("waitall\n");
	req.waitAll();
}

/*******************  FUNCTION  *********************/
int main(int argc, char ** argv)
{
	//vars
	size_t repeat = 100;
	size_t size = 1024*1024;
	int parallel = 0;
	
	//mpi
	MPI_Init(&argc,&argv);
	
	//get info
	int nodes;
	int rank;
	MPI_Comm_rank(MPI_COMM_WORLD,&rank);
	MPI_Comm_size(MPI_COMM_WORLD,&nodes);
	
	//sync
	int c;
	while ((c = getopt(argc,argv,"hp:r:s:")) != -1)
	{
		switch(c)
		{
			case 'h':
				fprintf(stderr,"%s -r {SIZE}\n",argv[0]);
				exit(EXIT_SUCCESS);
				break;
			case 'r':
				repeat = atol(optarg);
				break;
			case 's':
				size = atol(optarg);
				break;
			case 'p':
				parallel = atoi(optarg);
				break;
		}
	}
	
	//print
	if (rank == 0)
	{
		printf("#SIZE : %lu\n",size);
		printf("#REPEAT : %lu\n",repeat);
		printf("#PARALLEL : %d\n",parallel);
	}
	
	//alloc mem
	ticks * results = new ticks[repeat];
	char * buffer = new char[size*nodes];
	char * finalBuffer = new char[nodes * size];
	
	//init
	memset(results,0,repeat);
	memset(buffer,0,size);
	memset(finalBuffer,0,size * nodes);
	
	//repeat
	for (size_t i = 0 ; i < repeat ; i++)
	{
		MPI_Barrier(MPI_COMM_WORLD);
		ticks t0 = getticks();
		if (parallel == 0)
			MPI_Alltoall(buffer, size, MPI_CHAR,finalBuffer, size, MPI_CHAR,MPI_COMM_WORLD);
		else
			parallelAllToAll(buffer,finalBuffer,size,rank,nodes,parallel);
		ticks t1 = getticks();
		results[i] = t1-t0;
	}
	
	//print
	if (parallel == 0 || (parallel > 0 && rank % 2 == 0))
		for (size_t i = 0 ; i < repeat ; i++)
			printf("%llu\n",results[i]);
	
	//free mem
	delete results;
	delete buffer;
	delete finalBuffer;
	
	//exit
	MPI_Finalize();
	return EXIT_SUCCESS;
}
