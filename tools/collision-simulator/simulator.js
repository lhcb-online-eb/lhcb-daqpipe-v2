/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

//This file provide a link collision simulator based on real topology.

/****************************************************/
var argparse = require('argparse').ArgumentParser;
var fs = require('fs');

/****************************************************/
var parser = new argparse({
	version: '2.5.0-dev',
	addHelp:true,
	description: 'Link collision simulator'
});
parser.addArgument(
	[ '-t', '--topology' ],
	{ 
		help: 'Topolology file to load',
		required: true
	});
parser.addArgument(
	[ '-n', '--nodes' ],
	{ 
		help: 'Number of nodes to simulate',
		required: true
	});
parser.addArgument(
	[ '-s', '--sched' ],
	{ 
		help: 'Scheduling to use',
		required: true,
		choices: [ "barrel", "xor", "random", "custom" ]
	});
parser.addArgument(
	[ '-S', '--step' ],
	{ 
		help: 'Simulate only given step',
		type: "int"
	});
parser.addArgument(
	[ '-N', '--node-per-switch' ],
	{ 
		help: 'Number of nodes per switch to keep',
		type: "int",
		defaultValue: 1000
	});
parser.addArgument(
	[ '-c', '--custom-sched' ],
	{ help: 'Scheduling file to load' });
var args = parser.parseArgs();
// console.dir(args);

/****************************************************/
var linkSize = 5;
var switchSize = 60;
var rootSwitches;
var nodeCnt = args["nodes"];

/****************************************************/
function checkCustom(cust,nodes)
{
	var cnt = {};
	//build map
	for (var i = 0 ; i < nodes ; i++)
	{
		cnt[i] = {};
		for (var j = 0 ; j < nodes ; j++)
			cnt[i][j] = 0;
	}

	//count
	for (var phase = 0 ; phase < nodes ; phase++)
	{
		for (var i = 0 ; i < nodes ; i++)	
		{
			var j = cust[phase][i];
			if (j == undefined)
			{
				printf("ERROR, invalide value in custom map");
				process.exit(1);
			}
			if (j >= nodes)
			{
				console.log("One step go out of scope (phase="+phase+" node="+i+" to="+j);
				process.exit(1);
			}
			cnt[i][j] += 1;
			if (cnt[i][j] > 1)
			{
				console.log("One step is made two times (phase="+phase+" node="+i+" to="+j);
				process.exit(1);
			}
		}
	}

	for (var i = 0 ; i < nodes ; i++)
	{
		for (var j = 0 ; j < nodes ; j++)
		{
			if (cnt[i][j] != 1)
			{
				console.log("There is some missing send in custom sched ! "+cnt[i][j]);
				process.exit(1);
			}
		}
	}
	
	//echo ok
	console.log("Custom sched ok");
}

/****************************************************/
//loads
var topo = JSON.parse(fs.readFileSync(args["topology"]));
var customSched = [];
if (args["custom_sched"] != undefined)
{
	customSched = JSON.parse(fs.readFileSync(args["custom_sched"]));
	checkCustom(customSched,nodeCnt);
}

/****************************************************/
//load
if (topo.spine == undefined)
{
	rootSwitches = {
		237: true,
		234: true,
		235: true,
		244: true,
		242: true,
		226: true,
		238: true,
		231: true,
		239: true,
		247: true,
		
	};
} else {
	rootSwitches = topo.spine;
}

/****************************************************/
if (topo.spine2 == undefined)
{
	rootSwitches2 = topo.spine2 = {};
	hasSwitch2 = false;
} else {
	rootSwitches2 = topo.spine2;
	hasSwitch2 = true;
}


function isNode(name)
{
	return ((name.substring(0,4) == "node" || name.substring(11,15) == "hfi1" || name.substring(0,3) == "nxt" || name.substring(0,3)=="skl" || name.substring(0,2)=="ru" || name.substring(0,2)=="bu") && name.substr(0,14) != "bu-c2e46-10-01");
}

/****************************************************/
function getRemotePort(obj, lid)
{
	if (obj.type == 'node')
		return 1;
	for (var i in obj.ports)
		if (obj.ports[i].lid == lid)
			return i;
	console.log('error');
	return 0;
}

/****************************************************/
function portCoord(device, port)
{
	if (device.type == 'switch') 
	{
		return {
			x: device.id *linkSize * switchSize + port * linkSize,
			y: device.level * 200 + 25
		}
	} else if (device.type == 'node') {
		return {
			x: device.ports[port].link.src.device.id  *linkSize * switchSize + device.ports[port].link.src.port * linkSize,
			y: device.level * 200 + 25
		}
	} else {
		alert('error');
	}
}

/****************************************************/
function linkCoord(link)
{
	var p1 = portCoord(link.src.device,link.src.port);
	var p2 = portCoord(link.dest.device,link.dest.port);
	var res = {
		x1:p1.x,
		y1:p1.y,
		x2:p2.x,
		y2:p2.y
	}
	return res;
}

/****************************************************/
function resetLinkRouting(links)
{
	for (var i in links)
	{
		links[i].show = false;
		links[i].usedUp = 0;
		links[i].usedDown = 0;
		links[i].moveUp = undefined;
		links[i].moveDown = undefined;
	}
}

/****************************************************/
function enableLinks(links,start,end)
{
	//find node link
	var startLink = null;
	for (var i in links)
	{
		if (links[i].dest.device.lid == start.lid)
		{
			startLink = links[i];
			break;
		}
	}

	//color it
	startLink.show = true;
	startLink.usedUp ++;

	//jump to switch
	var cur = startLink.src.device;
	var curLevel = cur.level;
	var cnt = 0;
	while (cur.lid != end.lid && cnt++ < 10)
	{
		//select port
		//console.log(cur.lid);
		var port = topo.routes[cur.lid][end.lid];
		var c = cur;
		//console.log(end.lid);
		var link = cur.ports[port].link;
		if (link.src.device.lid == cur.lid)
			cur = link.dest.device;
		else
			cur = link.src.device;
		link.show = true;
		if (cur.level < curLevel)
			link.usedUp ++;
		else
			link.usedDown ++;
		if ((link.usedUp > 1 && cur.level < curLevel) || (link.usedDown > 1 && cur.level > curLevel))
		{
			if (cur.level < curLevel)
				link.moveUp = {lid:end.lid, sw:c};
			else
				link.moveDown = {lid:end.lid, sw:c, dest:cur};
			//console.log("Collide "+start.id + " => " + end.id +" ; "+start.name+" => "+end.name);
			//console.log("   with "+(cur.level < curLevel ? link.whichUp : link.whichDown));
		} else {
			if (cur.level < curLevel)
				link.whichUp = start.id + " up=> " + end.id +" ; "+start.name+" => "+end.name;
			else
				link.whichDown = start.id + " do=> " + end.id +" ; "+start.name+" => "+end.name;
		}
		var curLevel = cur.level;
	}
}

/****************************************************/
function barrel(nodes, node, step)
{
	return (node-step+3*nodes)%nodes
}

/****************************************************/
function xor(nodes,node,step)
{
	return (node ^ step) %nodes;
}

/****************************************************/
function custom(nodes,node,step)
{
	if (customSched[step] == undefined)
		return undefined;
	return customSched[step][node];
}

/****************************************************/
var gblLinks;
var gblNodes;
var gblCleanNodes;

/****************************************************/
function calcCollisions(nodes,step,func)
{
	//clear
	resetLinkRouting(gblLinks);
	
	//run
	for (var i = 0 ; i < nodes ; i++)
	{
		var nodeStart = gblCleanNodes[i].node;
		var isEnd = func(nodes,i,step);//(i-delta+3*nodes)%nodes;
// 		var isEnd = func(nodes,gblCleanNodes[i].id,step);//(i-delta+3*nodes)%nodes;
		if (isEnd == undefined)
			continue;
		var nodeEnd = gblCleanNodes[isEnd].node;
// 		for (var j in gblCleanNodes)
// 		{
// 			if (isEnd == gblCleanNodes[j].id)
// 			{
// 				nodeEnd = gblCleanNodes[j].node;
// 				break;
// 			}
// 		}
		//console.log(isEnd + " => "+nodeEnd.name );
		enableLinks(gblLinks,nodeStart,nodeEnd);
	}

	//count collisions
	var colLinks = 0;
	var maxCol = 0;
	var mean = 0;
	var meanCnt = 0;
	for (var i in gblLinks)
	{
		var link = gblLinks[i];
		if (link.usedUp > maxCol)
			maxCol = link.usedUp;
		if (link.usedUp > 1)
			colLinks++;
		if (link.usedDown > maxCol)
			maxCol = link.usedDown;
		if (link.usedDown > 1)
			colLinks++;
		meanCnt += 2;
		mean += link.usedDown + link.usedUp;
		
	}

	//console.log("Cols ["+step+"] : "+maxCol +" ; " + colLinks);
	return {cols:colLinks,maxCol:maxCol,mean:mean/meanCnt};
}

/****************************************************/
function isAvail(step,comm)
{
	if (comm == null)
		return false;
	for (var i in step)
		if (i == comm.start ||step[i] == comm.target)
			return false;
	return true;
}

/****************************************************/
function genSchedule(nodes,steps)
{
	//gen list
	/*var lst = {};
	var id = 0;
	for (var step = 1 ; step < steps ; step++)
		for (var node = 0 ; node < nodes ; node++)
			lst[id++] = {start:node,target:barrel(nodes,node,step)};

	var sched = [];
	var step = {};
	var found = true;
	var count = 0;
	while (id > 0)
	{
		//search one which fit
		found = false;
		var selected = null;
		var c = 0;
		while(c < 100*10)
		{
			var i = Math.floor(Math.random() * id);
			var sel;
			var cnt = 0;
			for (var j in lst)
			{
				if (cnt == i)
				{
					sel = j;
					break;
				}
				cnt++;
			}
				
			if (isAvail(step,lst[sel]))
			{
				selected = lst[sel];
				lst[sel] = undefined;
				found = true;
				id--;
				break;
			}
			c++;
		}

		if (found)
			step[selected.start] = selected.target;
		count++;
		if (count == nodes || found == false)
		{
			sched.push(step);
			step = {};
			count = 0;
		}
	}

	return sched;*/
}

/****************************************************/
var algo = null;
if (args["sched"] == "barrel") {
	algo = barrel;
} else if (args["sched"] == "xor") {
	algo = xor;
} else if (args["sched"] == "random") {
	algo = randomSched;
} else if (args["sched"] == "custom") {
	algo = custom;
	//customSched = genSchedule(nodeCnt,nodeCnt);
}

/****************************************************/
function run()
{
	//numer switches to give ID
	var id = [0,0,0];
	var switches = [];
	for (var i in topo.links.switch)
	{
		topo.links.switch[i].type = 'switch';
		base = 0;
		if (hasSwitch2)
			base = 1;
		if (rootSwitches2[i]) {
			topo.links.switch[i].level = 0;
			topo.links.switch[i].id = id[0];
			id[0]++;
		} else if (rootSwitches[i]) {
			topo.links.switch[i].level = base;
			topo.links.switch[i].id = id[base];
			id[base]++;
		} else {
			topo.links.switch[i].level = base+1;
			topo.links.switch[i].id = id[base+1];
			id[base+1]++;
		}
		switches.push(topo.links.switch[i]);
	}

	//create nodes
	var nodes = {};
	var nodeList  = [];
	for (var i in switches)
		nodes[switches[i].lid] = switches[i];
	for (var i in switches)
	{
		var cnt = 0;
		var m = args["node_per_switch"];
		for (var p in switches[i].ports)
		{
			var port = switches[i].ports[p];
			if (nodes[port.lid] == undefined)
			{
				nodes[port.lid] = {type:"node", level: base+1.8,name: port.name, lid: port.lid, ports:{1:{}}};
				if (isNode(port.name) && cnt < m)
				{
					console.log("select : "+cnt+" : "+port.name);
					nodeList.push(nodes[port.lid]);
					cnt++;
				}
			}
		}
	}
	
	//build links
	var links = [];
	for (var i in switches)
	{
		for (var p in switches[i].ports)
		{
			if (switches[i].ports[p].link == undefined)
			{
				var link = {};
				link.src = {device:switches[i],port:p};
				var destLid = switches[i].ports[p].lid;
				var destPort = getRemotePort(nodes[destLid],switches[i].lid);
				//console.log(destPort);
				destPort = switches[i].ports[p].remotePort;
				link.dest = {device:nodes[destLid],port:destPort};
				//console.log(link);

				switches[i].ports[p].link = link;
				//console.log(destLid + " => " +destPort);
				if (nodes[destLid] == undefined)
					nodes[destLid] = {};
				if (nodes[destLid].ports[destPort] == undefined)
					nodes[destLid].ports[destPort] = {};
				nodes[destLid].ports[destPort].link = link;
				
				links.push(link);
			} else {
				//console.log("already done");
			}
		}
	}

	//clean node names
// 	var cleanNodes = [];
// 	for (var i = 0 ; i < 800 ; i++)
// 	{
// 		var name;
// 		if (i >= 100)
// 			name = "node"+i;
// //			name = "nxt0"+i;
// 		else if (i >= 10)
// 			name = "node"+i;
// //			name = "nxt00"+i;
// 		else
// 			name = "node"+i;
// //			name = "nxt000"+i;
// 		for (var j in nodes)
// 		{
// 			if (nodes[j].name.split(" ")[0] == name)
// 			{
// 				cleanNodes.push({id:i,node:nodes[j]});
// 				nodes[j].id = i;
// 			}
// 		}
// 	}
	
	//make contiguous
 	cleanNodes = [];
 	var c = 0;
 	for (var i in nodeList)
 	{
		//console.log(nodeList[i].name+"=>"+nodeList[i].name.substring(11,15));
 		if (isNode(nodeList[i].name))
		{
			//console.log(nodeList[i].name+"ok");
 			cleanNodes.push({id:c++, node:nodeList[i]});
		}
 	}

/*	var hostfile = [ "nxt0219" , "nxt0217" , "nxt0226" , "nxt0220" , "nxt0218" , "nxt0215" , "nxt0113" , "nxt0225" , "nxt0114" , "nxt0216" ,
 "nxt0118" , "nxt0117" , "nxt0123" , "nxt0122" , "nxt0115" , "nxt0121" , "nxt0119" , "nxt0124" , "nxt0116" , "nxt0120" , "nxt0311" ,
"nxt0307" , "nxt0314" , "nxt0304" , "nxt0357" , "nxt0356" , "nxt0360" , "nxt0359" , "nxt0312" , "nxt0303" , "nxt0310" , "nxt0358" ,
"nxt0355" , "nxt0305" , "nxt0308" , "nxt0309" ,
"nxt0306" , "nxt0313" , "nxt0323" ,
 "nxt0321" ,  "nxt0329" , "nxt0322" , "nxt0330" , "nxt0324" , "nxt0103" , "nxt0107" , "nxt0105" , "nxt0106" , "nxt0108" , "nxt0104" , 
"nxt0101" , "nxt0237" , "nxt0241" ,"nxt0242" , "nxt0244" , "nxt0240" , "nxt0236" , "nxt0231" , "nxt0239" , "nxt0229" , "nxt0230" , 
"nxt0232" , "nxt0227" , "nxt0243" , 
"nxt0235" , "nxt0234" , "nxt0238" , "nxt0228" , "nxt0233" , "nxt0211" , "nxt0213" , "nxt0209"
	];
	cleanNodes = [];
       	var c = 0;
	for (var j in hostfile){
	       	for (var i in nodeList)
       		{
	               	if (nodeList[i].name.substring(0,7) == hostfile[j])
                	       	cleanNodes.push({id:c++, node:nodeList[i]});
       		}
	}
	console.log(cleanNodes);*/

	gblNodes = nodes;
	gblCleanNodes = cleanNodes;
	gblLinks = links;

	console.log("#nodes\tstep\tconflicts\tmax\tmean/step\tmean2/steps");
// 	for (var j = 0 ; j < nodeCnt ; j++) {

	var nodes = nodeCnt;
	var steps = nodeCnt;
	
	var sum = 0;
	var confl = [];
	var max = 0;
	var mean = 0;
	var mean2 = 0;
	var stepStart = 0;
	var stepEnd = steps;
	if (args["step"] != undefined)
	{
		stepStart = args["step"];
		stepEnd = args["step"]+1;
	} else {
		args["step"] = -1;
	}
	for (var i = stepStart ; i < stepEnd ; i++)
	{
		var c = calcCollisions(nodes,i,algo);
		confl.push(c);
		sum += c.cols;
		if (c.maxCol > max)
			max = c.maxCol;
		mean += c.maxCol;
		mean2 += c.mean;
	}
	//console.log("=============== " + sum +" ======================");
	console.log(nodeCnt+"\t"+args["step"]+"\t"+sum+"\t"+max+"\t"+mean/steps+"\t"+mean2/steps);
// 	}
	
/*	var algo = custom;
	
	var sum = 0;
	var confl2 = [];
	for (var i = 0 ; i < steps ; i++)
	{
		var c = calcCollisions(nodes,i,algo);
		confl2.push(c);
		sum += c;
	}
	console.log("=============== " + sum +" ======================");
	
	calcCollisions(nodes,18,algo);*/

// 	var sum = 0;
// 	for (var i = 0 ; i < 64 ; i++)
// 		sum += calcCollisions(64,i,algo);
// 	var sum3 = sum;
// 	console.log("=============== " + sum +" ======================");
// 	
// 	sum = 0;
// 	for (var i = 0 ; i < 64 ; i++)
// 	{
// 		calcCollisions(64,i,algo);
// 		rebalanceRouting(gblLinks);
// // 		applyBalancing();
// // 		sum += calcCollisions(64,i,algo);
// // 		rebalanceRoutingDown(gblLinks);
// 	}
// 	var sum2 = sum;
// 	console.log("=============== " + sum +" ======================");
// 	applyBalancing();
// 	sum = 0;
// 	for (var i = 0 ; i < 64 ; i++)
// 		sum += calcCollisions(64,i,algo);
// 	console.log("=============== " + sum +" ====================== "+ sum2 + " === "+sum3);
// 	
// 	calcCollisions(64,18,algo);
// 	rebalanceRouting(gblLinks);
// 	applyBalancing();
// 	console.log("---------------------------");
// 	calcCollisions(64,18,algo);
// 	rebalanceRoutingDown(gblLinks);
// 	applyBalancing();
// 	console.log("---------------------------");
// 	calcCollisions(64,18,custom);
}

run();

