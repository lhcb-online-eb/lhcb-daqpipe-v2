#!/usr/bin/env python3
######################################################
#            PROJECT  : lhcb-daqpipe                 #
#            VERSION  : 2.5.0-dev                    #
#            DATE     : 12/2017                      #
#            AUTHOR   : Niko Neufled  - CERN         #
#            LICENSE  : CeCILL-C                     #
######################################################

import sys
import json
from math import log10

def printSchedFatTree(schedule):
    global radix, N
    i = int(log10(radix)) + 1 
    j = int(log10(N)) + 1
    format="{0:%dd},{1:%dd} " % (i,j)
    for s in range(radix):
        for n in range(N):
            print(format.format(s, n), end="")
    print("")
    print("-" * (((radix // 2) * radix * (i + j + 2)) - 1))
    for phase in schedule:
        for dest in phase:
            print(format.format(dest[0], dest[1]), end="")
        print("")


radix = 36
if len(sys.argv) == 2:
    radix = int(sys.argv[1])

if radix % 2 == 1:
    radix = radix + 1
    print("Setting radix to %d", radix)

N = radix // 2

def conflictFree(schedule):
    return true    

# need a name for this topology

#for ss in range(N):
	#for sl in range(N):
		#for ds in range(N):
			#for dl in range(N):
				#print("(", (ds + dl + ss) % N, ",", (dl + sl) % N, ") ", end="")
		#print("")

# fat tree (maximum)

sFatTree = []
for ss in range(2*N):
    for sl in range(N):
        phaseSched = []
        for ds in range(2*N):
            for dl in range(N):
                phaseSched.append(((ds + dl + ss) % (2 * N), (dl + sl) % N))
        sFatTree.append(phaseSched)

# fat tree addresses (IPv4)
# each leave switch needs a subnet of roundup(ld(N) + 2), where two cover the
# network and broadcast address
# spine switches a

#printSchedFatTree(sFatTree)

sched = [];
for phase in sFatTree:
    tmp = {}
    source = 0
    for dest in phase:
        tmp[source] = N * dest[0] + dest[1]
        source = source + 1
    sched.append(tmp)

file = open("sched.json","w") 
file.write(json.dumps(sched,indent=4))
