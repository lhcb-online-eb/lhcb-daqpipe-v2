/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/****************************************************/
var argparse = require('argparse').ArgumentParser;

/****************************************************/
var parser = new argparse({
	version: '2.5.0-dev',
	addHelp:true,
	description: 'Fat-tree generator'
});
parser.addArgument(
	[ '-s', '--spine' ],
	{ 
		help: 'Number of spine switch',
		defaultValue: 18
	});
parser.addArgument(
	[ '-l', '--leaf' ],
	{ 
		help: 'Number of leaf switch',
		defaultValue: 29
	});
parser.addArgument(
	[ '-n', '--nodes-per-leaf' ],
	{ 
		help: 'Number of node per leaf switch',
		defaultValue: 18
	});
parser.addArgument(
	[ '-p', '--ports' ],
	{ 
		help: 'Number ports on switch',
		defaultValue: 36
	});
parser.addArgument(
	[ '-P', '--print-examples' ],
	{ 
		help: 'Number ports on switch'
	});
var args = parser.parseArgs();
//console.dir(args);

/****************************************************/
//if print examples
if (args["print_examples"] != undefined)
{
	console.log("Solutions:");
	console.log("IB full : -s 18 -l 29 -n 18 -p 36");
	console.log("OPA full : -s 12 -l 22 -n 24 -p 48");
	process.exit(0);
}

/****************************************************/
function genNodeName(id)
{
	if (id >= 100)
		return "node"+id;
	else if (id >= 10)
		return "node0"+id;
	else
		return "node00"+id;
}

/****************************************************/
var topo = {
	"links": {
		"switch": {}
	},
	"routes" : {},
	"spine": {}
};

/****************************************************/
//top switches
var topSwitches = [];
// var spine = 18;//18 | 3 | 12 (48)
// var leaf = 29;//29 | 4 | 22 (48)
// var nodeLeaf = 18;//18 | 24 (48)
// var switchPorts = 36;//36 | 48
var spine = args["spine"];
var leaf = args["leaf"];
var nodeLeaf = args["nodes_per_leaf"];
var switchPorts = args["ports"];
var upLinks = (switchPorts-nodeLeaf);
var nodeStart = upLinks + 1;
var leafSpine = upLinks / spine;

//setup top switches
for (var i = 0 ; i < spine ; i++)
{
	topSwitches.push(i);
	topo.spine[i] = true;
	topo.links.switch[i] = {
		"name": "top switch "+i,
		"lid": i,
		"ports":{}
	}
}

//nodes
var nodeId = 1;
var lid = 100;
var leafs = [];
var nodes = {};
var aid = 0;
for (var sw = 0 ; sw < leaf ; sw++)
{
	//create swtich
	var s = topo.links.switch[lid] = {
		"name": "leaf switch "+lid,
		"lid": lid,
		"ports":{}
	}
	leafs.push(s);
	lid++;

	//up links
	for(var i in topSwitches)
	{
		for (var j = 0 ; j < leafSpine ; j++)
		{
			var srcPort = 1+leafSpine*i+j;
			var destPort = 1+ sw * leafSpine+j;
			s.ports[srcPort] = {
				"name": "switch",
				"lid": topSwitches[i],
				"remotePort": destPort
			}
			topo.links.switch[topSwitches[i]].ports[destPort] = {
				"name": "switch",
				"lid": lid-1,
				"remotePort": srcPort
			}
		}
	}

	//create nodes
	for (var i = 0 ; i < nodeLeaf ; i++)
	{
		s.ports[nodeStart+i] = {
			"name": genNodeName(nodeId),
			"lid": lid,
			"remotePort": "1"
		}
		nodes[lid] = {switch:s, aid:aid, port:nodeStart+i,lid:lid};
		aid++;
		nodeId++;
		lid++;
	}
}

//routes for top switches
for (var i in topSwitches)
{
	for (var lid in nodes)
	{
		if (topo.routes[topSwitches[i]] == undefined)
			topo.routes[topSwitches[i]] = {};
		topo.routes[topSwitches[i]][lid] = 1+ (Math.floor(nodes[lid].aid / nodeLeaf)) * leafSpine + nodes[lid].aid % leafSpine;
	}
}

//routes for leaf switches
for (var i in leafs)
{
	for (var lid in nodes)
	{
		if (topo.routes[leafs[i].lid] == undefined)
			topo.routes[leafs[i].lid] = {};
		topo.routes[leafs[i].lid][lid] = 1+ nodes[lid].aid % upLinks;
	}
}

//routes for nodes in leaf switches
for (var i in nodes)
{
	topo.routes[nodes[i].switch.lid][nodes[i].lid] = nodes[i].port;
}

console.log(JSON.stringify(topo,null,"\t"));

