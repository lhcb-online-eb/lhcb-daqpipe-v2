/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#undef NDEBUG
//std
#include <fstream>
#include <iostream>
#include <cassert>
#include <map>
#include <cstring>
#include <list>
#include <vector>
//jsoncpp
#include <json/json.h>

/*******************  NAMESPACE  ********************/
using namespace std;

/********************  MACROS  **********************/
#define MAX_NODES 512
#define MAX_PORTS 49

/*********************  TYPES  **********************/
enum DeviceType
{
	NODE,
	SWITCH,
	UNDEF
};

/********************  STRUCT  **********************/
struct Link;

/********************  STRUCT  **********************/
struct Port
{
	Port(void);
	std::string name;
	int lid;
	Link * link;
	int remotePort;
};

/********************  STRUCT  **********************/
struct Device
{
	Device(void);
	DeviceType type;
	Port ports[MAX_PORTS];
	int level;
	int lid;
	int id;
	std::string name;
};

/********************  STRUCT  **********************/
struct LinkTarget
{
	Device * device;
	int port;
};

/********************  STRUCT  **********************/
struct Link
{
	LinkTarget src;
	LinkTarget dest;
	bool show;
	int usedUp;
	int usedDown;
};

/********************  STRUCT  **********************/
struct Step
{
	int next[512];
};

/********************  STRUCT  **********************/
struct Comm
{
	int start;
	int target;
	int id;
};

/*******************  FUNCTION  *********************/
std::string numToString(int value)
{
	char buf[64];
	sprintf(buf,"%d",value);
	return buf;
}

/*******************  FUNCTION  *********************/
Port::Port ( void )
{
	this->lid = -1;
	this->link = NULL;
}

/*******************  FUNCTION  *********************/
Device::Device ( void )
{
	this->id = -1;
	this->lid = -1;
	this->type = UNDEF;
	for (int i = 0 ; i < MAX_PORTS ; i++)
	{
		this->ports[i].lid = -1;
		this->ports[i].link = NULL;
	}
}

/*******************  FUNCTION  *********************/
void loadTopoJson(Json::Value & tmpObj,const std::string & filename)
{
	//setup json file reader
	Json::Reader reader;
	
	//setup stream and open file
	std::ifstream file(filename.c_str(), std::ifstream::binary);
	assert(file.is_open());
	
	//skip
	char buf[32];
	file.read(buf,7);

	//parse
	bool status = reader.parse(file, tmpObj);
	assert(status);
}

/*******************  FUNCTION  *********************/
int getRemotePort(Device & device,int lid)
{
	if (device.type == NODE)
		return 1;
	for (int i = 0 ; i < MAX_PORTS ; i++)
		if (device.ports[i].lid == lid)
			return i;
	
	cerr << "Error ! " << endl;
	return 0;
}

/*******************  FUNCTION  *********************/
bool isAvail(Step & step,Comm * comm)
{
	if (comm == NULL)
		return false;
	for (int i = 0 ; i < MAX_NODES ; i++)
		if ((i == comm->start && step.next[i] != -1) || step.next[i] == comm->target)
			return false;
	return true;
}

/*******************  FUNCTION  *********************/
bool enableLinks(std::map<int,std::map<int,int> > & routing, std::list<Link*> & links,Device & start,Device & end,int delta = 1)
{
	//vars
	bool collide = false;
	
	//find node link
	Link * startLink = NULL;
	for (auto & it : links) {
		if (it->dest.device->lid == start.lid) {
			startLink = it;
			break;
		}
	}
	
	//color it
	startLink->show = true;
	startLink->usedUp += delta;
	
	//jump to switch
	Device * cur = startLink->src.device;
	int curLevel = cur->level;
	int cnt = 0;
	while (cur->lid != end.lid && cnt++ < 10) {
		//select port
		//cout << topo["routes"][numToString(cur->lid)][numToString(end.lid)].asString() << endl;
		//int port = atoi(topo["routes"][numToString(cur->lid)][numToString(end.lid)].asString().c_str());
		int port = routing[cur->lid][end.lid];
		Link& link = *(cur->ports[port].link);
		
		if (link.src.device->lid == cur->lid)
			cur = link.dest.device;
		else
			cur = link.src.device;
		link.show = true;
		if ((link.usedUp > 0 && cur->level < curLevel) || (link.usedDown > 0 && cur->level > curLevel))
		{
			collide = true;
		}
		if (cur->level < curLevel)
			link.usedUp +=delta;
		else
			link.usedDown +=delta;
		curLevel = cur->level;
	}
	
	//return
	return collide;
}

/*******************  FUNCTION  *********************/
void resetLinkRouting(std::list<Link*> & links)
{
	for (auto link : links) {
		link->show = false;
		link->usedDown = 0;
		link->usedUp = 0;
	}
}

/*******************  FUNCTION  *********************/
int barrel(int nodes,int node,int step)
{
	return (node - step+3*nodes)%nodes;
}

/*******************  FUNCTION  *********************/
void initStep(Step & step)
{
	for (int i = 0 ; i < MAX_NODES ; i++)
		step.next[i] = -1;
}

/*******************  FUNCTION  *********************/
int main(void)
{
	//load topo
	Json::Value topo;
	loadTopoJson(topo,"/home/svalat/Projects/lhcb-daqpipe-v2/tools/net-topo-viewer/topo2.js");
	
	//vars
	int nodeCnt = 64;
	int steps = nodeCnt;
	
	//build root switches
	std::map<int,bool> rootSwitches;
	if (topo.isMember("spine")) {
		Json::Value & s = topo["spine"];
		for( auto it = s.begin() ; it != s.end() ; it++ ) {
			rootSwitches[atoi(it.key().asString().c_str())] = (*it).asBool();
		}
	} else {
		rootSwitches[(7)] = true;
		rootSwitches[(5)] = true;
		rootSwitches[(4)] = true;
	}
	
	//setup switch levels and id
	int idLevel[2] = {0,0};
	std::list<Device> switches;
	Json::Value & s = topo["links"]["switch"];
	for( auto it = s.begin() ; it != s.end() ; it++ ) {
		Device dev;
		dev.type = SWITCH;
		dev.name = (*it)["name"].asString();
		dev.lid = (*it)["lid"].asInt();
		if (rootSwitches[dev.lid]) {
			dev.level = 0;
			dev.id = idLevel[0]++;
		} else {
			dev.level = 1;
			dev.id = idLevel[1]++;
		}
		for (int i = 0 ; i < MAX_PORTS ; i++) {
			//cout << (*it["ports"]) << endl;
			if ((*it)["ports"].isMember(numToString(i))) {
				Json::Value & tmp = (*it)["ports"][numToString(i)];
				dev.ports[i].name = tmp["name"].asString();
				dev.ports[i].lid = atoi(tmp["lid"].asString().c_str());
				dev.ports[i].remotePort = atoi(tmp["remotePort"].asString().c_str());
				dev.ports[i].link = NULL;
			} else {
				dev.ports[i].lid = -1;
				dev.ports[i].link = NULL;
			}
		}
		switches.push_back(dev);
	}
	
	//create nodes
	std::map<int,Device *> nodes;
	std::list<Device*> nodeList;
	for (auto & sw : switches)
		nodes[sw.lid] = &sw;
	for (auto & sw : switches) {
		for (int i = 0 ; i < MAX_PORTS ; i++) {
			Port p = sw.ports[i];
			if (p.lid != -1 && nodes.find(p.lid) == nodes.end()) {
				Device * dev = new Device;
				dev->type = NODE;
				dev->level = 3;
				dev->name = p.name;
				dev->lid = p.lid;
				nodeList.push_back(dev);
				nodes[p.lid] = dev;
			}
		}
	}
	
	//build links
	std::list<Link*> links;
	for (auto & sw : switches) {
		for (int i = 0 ; i < MAX_PORTS ; i++) {
			if (sw.ports[i].link == NULL && sw.ports[i].lid != -1) {
				Link * link = new Link;
				link->src.device = &sw;
				link->src.port = i;
				int destLid = sw.ports[i].lid;
				//int destPort = getRemotePort(nodes[destLid],sw.lid);
				int destPort = sw.ports[i].remotePort;
				link->dest.device = nodes[destLid];
				link->dest.port = destPort;
				
				sw.ports[i].link = link;
				nodes[destLid]->ports[destPort].link = link;
				
				links.push_back(link);
			}
		}
	}
	
	//clean node names
	std::vector<Device *> cleanNodes;
	for (int i = 0 ; i < 800 ; i++) {
		char name[64];
		sprintf(name,"node%03d",i);
		for (auto n : nodeList) {
			if (strncmp(n->name.c_str(),name,7) == 0) {
				cleanNodes.push_back(n);
				n->id = i;
			}
		}
	}
	
	//gen list
	std::map<int,Comm> lst;
	int id = 0;
	for (int step = 0 ; step < steps ; step++) {
		for (int node = 0 ; node < nodeCnt ; node++) {
			Comm & c = lst[id];
			c.id = id;
			c.start = node;
			c.target = barrel(nodeCnt,node,step);
			id++;
		}
	}
	
	//routing table
	std::map<int,std::map<int,int> > routing;
	Json::Value & ss = topo["routes"];
	for( auto it = ss.begin() ; it != ss.end() ; it++ ) {
		Json::Value & t = *it;
		for( auto it2 = t.begin() ; it2 != t.end() ; it2++ ) {
			routing[atoi(it.key().asString().c_str())][atoi(it2.key().asString().c_str())] = (*it2).asInt();
		}
	}
	
	//build sched
	int cnt = 0;
	Step step;
	initStep(step);
	std::list<Step> sched;
	while (id > 0) {
		//reset links
		resetLinkRouting(links);
		
		//replay
		for (int i = 0 ; i < MAX_NODES ; i++) {
			if (step.next[i] != -1) {
				Device * nodeStart = cleanNodes[i];
				Device * nodeEnd = cleanNodes[step.next[i]];
				if (enableLinks(routing,links,*nodeStart,*nodeEnd,1) == true)
					cerr << "Error ! " << endl;
			}
		}
		
		//get candidates
		std::vector<Comm> candidates;
		for (auto it : lst) {
			if (isAvail(step,&it.second)) {
				Device * nodeStart = cleanNodes[it.second.start];
				Device * nodeEnd = cleanNodes[it.second.target];
				if (enableLinks(routing,links,*nodeStart,*nodeEnd,0) == false)
					candidates.push_back(it.second);
				//enableLinks(routing,links,*nodeStart,*nodeEnd,-1);
			}
		}
		
		//if has something or not
		if (candidates.empty())
		{
			sched.push_back(step);
			initStep(step);
			cout << "Done step " << sched.size() << " (" << cnt << ")" << endl;
			cnt = 0;
		} else {
			//randomly select one
			int sel = (int)((rand()/(double)RAND_MAX) * candidates.size());
			Comm & selected = candidates[sel];
			
			//apply
			step.next[selected.start] = selected.target;
			
			//remove in list
			lst.erase(selected.id);
			id--;
			cnt++;
		}
	}
}
