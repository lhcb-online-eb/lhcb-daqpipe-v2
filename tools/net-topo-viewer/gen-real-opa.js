/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

//Use input from opaextractlink command

var fs = require('fs')

var input = fs.readFileSync("topo-opa.txt").toString()

var lines = input.split("\n")
var topo = {switch:{}}

var first = true
for (var line in lines)
{
	if (first)
	{
		first = false;
		continue;
	}
	//split
	var infos = lines[line].split(";")
	//extract
	var node = infos[10];
	var nodePort = infos[11];
	var sw = infos[12];
	var swPort = infos[13]
	//store
	if (topo.switch[sw] == undefined)
	{
		topo.switch[sw] = {
			"name": sw,
			"lid": sw,
			"ports": {
			}
		}
	}
	topo.switch[sw].ports[swPort] = {
		"name": node,
		"lid": node,
		"remotePort": nodePort
	}
	if (node.indexOf("hfi") == -1)
	{
		if (topo.switch[node] == undefined)
		{
			topo.switch[node] = {
				"name": node,
				"lid": node,
				"ports": {
				}
			}
		}
		topo.switch[node].ports[nodePort] = {
			"name": sw,
			"lid": sw,
			"remotePort": swPort
		}
	}
}

//det spine swithces
var spine = {};
for (var i in topo.switch)
{
	var res = true;
	for (var j in topo.switch[i].ports)
		if (topo.switch[i].ports[j].name.indexOf("hfi") != -1)
			res = false;
	if (res == true)
		spine[topo.switch[i].lid] = true
}

var final = {
	spine:spine,
	links:topo,
	routes:{}
};

console.log("topo = "+JSON.stringify(final,null,"\t")+";");