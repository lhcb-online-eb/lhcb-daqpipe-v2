/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

//need to generate two files : route.txt and topo.txt :
//ibnetdiscover > topo.txt
//saquery LFTR > route.txt
//
//there is also an alternative to generate route.txt but nedd to uncomment the end of this file
//to use it.

var fs = require("fs");

var input = fs.readFileSync("topo.txt").toString();

var inRoutes = fs.readFileSync("route.txt").toString();

var lines = input.split("\n");
var cur = null;
var s = null;
var topo = {switch:{}};
//                  Switch	36     "S-e41d2b13660"		# "MF0;rack1-switch2:MSB7700/U1" enhanced port 0 lid 91 lmc 0
//                  Switch	36 "S-e41d2d0300bfaf60"		# "SwitchIB Mellanox Technologies" base port 0 lid 4 lmc 0
//                  Switch	36 "S-e41d2d0300b59300"		# "MF0;rack1-switch1:MSB7700/U1" enhanced port 0 lid 1 lmc 0
//                  Switch  37 "S-ec0d9a0300f09ac0"         # "SwitchIB Mellanox Technologies" base port 0 lid 100 lmc 0
var switchRegexp = /Switch  [0-9]+ "[A-Za-z0-9-]+" +# "(.+)" [a-z]+ port [0-9]+ lid ([0-9]+) lmc [0-9]+/;
//                  [1]     "S-ec0d9a0300235060"[27]                # "SwitchIB Mellanox Technologies" lid 8 4xEDR
var portRegexp = /\[([0-9]+)\] +"[0-9A-Za-z-]+"\[([0-9]+)\](\([a-z0-9]+\))? ? +# "(.+)" lid ([0-9]+) 4x[EF]DR/;

for (var l in lines)
{
	//check switch entry
	var tmp = switchRegexp.exec(lines[l]);
	if (tmp){
		if (cur != null) {
			topo.switch[cur.lid] = {
				name: cur.name,
				lid: cur.lid,
				ports: s
			};
		}
		cur = {
			name:tmp[1],
			lid:tmp[2]
		};
		//console.log(tmp[1] + "=>"+tmp[2]);
		s = {};
	}

	var tmp = portRegexp.exec(lines[l]);
	if (tmp) {
		s[tmp[1]] = {
			name: tmp[4],
			lid: tmp[5],
			remotePort: tmp[2]
		}
	}
}

topo.switch[cur.lid] = {
				name: cur.name,
				lid: cur.lid,
				ports: s
			};

//------------------------------------------------------------
//using saquery LFTR > route.txt
var lidRegexp=/LID........................([0-9]+)/;
var routeRegexp=/([0-9]+)	([0-9]+)/;
lines = inRoutes.split("\n");
var cur = null;
var routes = {};
for (var l in lines)
{
	var tmp = lidRegexp.exec(lines[l]);
	if (tmp){
		cur = tmp[1];
		if (routes[cur] == undefined)
			routes[cur]= {};
	}
	var tmp = routeRegexp.exec(lines[l]);
	if (tmp) {
		routes[cur][tmp[1]] = tmp[2];
	}
}

//------------------------------------------------------------
//using other method 
/*lines = inRoutes.split("\n");
var cur = null;
var routes = {};
var buf = {};
var swLid = undefined;
for (var l in lines)
{
	if (lines[l].substr(0,7) == "Unicast")
	{
		buf = {};
	} else if (lines[l].substr(0,2) == "0x") {
		var lid = parseInt(lines[l].substr(0,6),16);
		var port = parseInt(lines[l].substr(7,3));
		if (port == 0)
		{
			swLid = lid;
			routes[swLid] = buf;
		} else {
			buf[lid] = port;
		}
	}
}*/

//detect spine
spines = {};
for (var i in topo.switch)
{
	var cnt = 0;
	var sw = topo.switch[i];

	for (var port in sw.ports)
		if (sw.ports[port].name.indexOf("HCA") != -1)
			cnt=cnt+1;
	if (cnt <= 2)
		spines[sw.lid] = true;	
}

var final = {
	spine: spines,
	links:topo,
	routes:routes
};

console.log("topo = "+JSON.stringify(final,null,"\t")+";");
