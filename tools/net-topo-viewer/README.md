Network Topology Viewer
=======================

This directory provide a simple tool to display the topology of an IB network.
You can load the topology by using the IB dump tools :

TODO

Generate real topology
----------------------

After dumping the IB information into topo.txt and route.txt just call the gen-real.js script.

```sh
	node gen-real.js > topo.js
```

Generate fat tree topology
--------------------------

You can use the gen-fat-tree.js script to generate a fat tree topology, you can change
the parameter into the script (spine, leaf and nodeLeaf).

```sh
	node gen-fat-tree.js > topo2.js
```

Displayer
---------

You just have to open the index.html file into your brower to get it. You might
change the topology file you load by modifying the file in the html file (topo.js or topo2.js).
