#!/bin/bash

cat > getnodes.cpp << EOF
#include <unistd.h>
#include <cstdio>
#include <cstdlib>

int main(int argc, char** argv) 
{
	char buffer[1024];
	gethostname(buffer,sizeof(buffer));
	printf("%s\n",buffer);
	return EXIT_SUCCESS;
}
EOF

mpicxx getnodes.cpp -o getnodes
nodes=$(mpirun --map-by ppr:1:node ./getnodes)

cat numbered.txt | while read line
do
	if [ ! -z "$(echo $nodes | grep $line)" ]; then
		echo $line
	fi
done
