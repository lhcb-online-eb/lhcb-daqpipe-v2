topo = {
 "spine": {
  "OmniPth00117501fa000404 S207A": true, 
  "OmniPth00117501fa000404 S207B": true, 
  "OmniPth00117501fa000404 S205B": true, 
  "OmniPth00117501fa000404 S208A": true, 
  "OmniPth00117501fa000404 L119B": true, 
  "OmniPth00117501fa000404 S203A": true, 
  "OmniPth00117501fa000404 S203B": true, 
  "OmniPth00117501fa000404 S201B": true, 
  "OmniPth00117501fa000404 S201A": true, 
  "OmniPth00117501fa000404 L114A": true, 
  "OmniPth00117501fa000404 L113B": true, 
  "OmniPth00117501fa000404 L113A": true, 
  "OmniPth00117501fa000404 L114B": true, 
  "OmniPth00117501fa000404 S208B": true, 
  "OmniPth00117501fa000404 L119A": true, 
  "OmniPth00117501fa000404 S205A": true, 
  "OmniPth00117501fa000404 S206B": true, 
  "OmniPth00117501fa000404 S206A": true, 
  "OmniPth00117501fa000404 S204A": true, 
  "OmniPth00117501fa000404 S204B": true, 
  "OmniPth00117501fa000404 S202B": true, 
  "OmniPth00117501fa000404 S202A": true, 
  "OmniPth00117501fa000404 L117B": true
 }, 
 "routes": {}, 
 "links": {
  "switch": {
   "OmniPth00117501fa000404 S207A": {
    "lid": "OmniPth00117501fa000404 S207A", 
    "name": "OmniPth00117501fa000404 S207A", 
    "ports": {
     "43": {
      "lid": "OmniPth00117501fa000404 L116A", 
      "remotePort": "30", 
      "name": "OmniPth00117501fa000404 L116A"
     }, 
     "24": {
      "lid": "OmniPth00117501fa000404 L113A", 
      "remotePort": "25", 
      "name": "OmniPth00117501fa000404 L113A"
     }, 
     "25": {
      "lid": "OmniPth00117501fa000404 L102A", 
      "remotePort": "48", 
      "name": "OmniPth00117501fa000404 L102A"
     }, 
     "27": {
      "lid": "OmniPth00117501fa000404 L106A", 
      "remotePort": "46", 
      "name": "OmniPth00117501fa000404 L106A"
     }, 
     "20": {
      "lid": "OmniPth00117501fa000404 L117A", 
      "remotePort": "29", 
      "name": "OmniPth00117501fa000404 L117A"
     }, 
     "21": {
      "lid": "OmniPth00117501fa000404 L119A", 
      "remotePort": "28", 
      "name": "OmniPth00117501fa000404 L119A"
     }, 
     "48": {
      "lid": "OmniPth00117501fa000404 L114A", 
      "remotePort": "25", 
      "name": "OmniPth00117501fa000404 L114A"
     }, 
     "44": {
      "lid": "OmniPth00117501fa000404 L118A", 
      "remotePort": "29", 
      "name": "OmniPth00117501fa000404 L118A"
     }, 
     "29": {
      "lid": "OmniPth00117501fa000404 L110A", 
      "remotePort": "44", 
      "name": "OmniPth00117501fa000404 L110A"
     }, 
     "1": {
      "lid": "OmniPth00117501fa000404 L101A", 
      "remotePort": "48", 
      "name": "OmniPth00117501fa000404 L101A"
     }, 
     "3": {
      "lid": "OmniPth00117501fa000404 L105A", 
      "remotePort": "46", 
      "name": "OmniPth00117501fa000404 L105A"
     }, 
     "5": {
      "lid": "OmniPth00117501fa000404 L109A", 
      "remotePort": "44", 
      "name": "OmniPth00117501fa000404 L109A"
     }, 
     "7": {
      "lid": "OmniPth00117501fa000404 L101B", 
      "remotePort": "31", 
      "name": "OmniPth00117501fa000404 L101B"
     }, 
     "6": {
      "lid": "OmniPth00117501fa000404 L111A", 
      "remotePort": "43", 
      "name": "OmniPth00117501fa000404 L111A"
     }, 
     "9": {
      "lid": "OmniPth00117501fa000404 L105B", 
      "remotePort": "33", 
      "name": "OmniPth00117501fa000404 L105B"
     }, 
     "39": {
      "lid": "OmniPth00117501fa000404 L118B", 
      "remotePort": "39", 
      "name": "OmniPth00117501fa000404 L118B"
     }, 
     "38": {
      "lid": "OmniPth00117501fa000404 L116B", 
      "remotePort": "38", 
      "name": "OmniPth00117501fa000404 L116B"
     }, 
     "11": {
      "lid": "OmniPth00117501fa000404 L109B", 
      "remotePort": "35", 
      "name": "OmniPth00117501fa000404 L109B"
     }, 
     "13": {
      "lid": "OmniPth00117501fa000404 L113B", 
      "remotePort": "37", 
      "name": "OmniPth00117501fa000404 L113B"
     }, 
     "12": {
      "lid": "OmniPth00117501fa000404 L111B", 
      "remotePort": "36", 
      "name": "OmniPth00117501fa000404 L111B"
     }, 
     "15": {
      "lid": "OmniPth00117501fa000404 L117B", 
      "remotePort": "39", 
      "name": "OmniPth00117501fa000404 L117B"
     }, 
     "14": {
      "lid": "OmniPth00117501fa000404 L115B", 
      "remotePort": "38", 
      "name": "OmniPth00117501fa000404 L115B"
     }, 
     "16": {
      "lid": "OmniPth00117501fa000404 L119B", 
      "remotePort": "40", 
      "name": "OmniPth00117501fa000404 L119B"
     }, 
     "19": {
      "lid": "OmniPth00117501fa000404 L115A", 
      "remotePort": "30", 
      "name": "OmniPth00117501fa000404 L115A"
     }, 
     "31": {
      "lid": "OmniPth00117501fa000404 L102B", 
      "remotePort": "31", 
      "name": "OmniPth00117501fa000404 L102B"
     }, 
     "30": {
      "lid": "OmniPth00117501fa000404 L112A", 
      "remotePort": "43", 
      "name": "OmniPth00117501fa000404 L112A"
     }, 
     "37": {
      "lid": "OmniPth00117501fa000404 L114B", 
      "remotePort": "37", 
      "name": "OmniPth00117501fa000404 L114B"
     }, 
     "36": {
      "lid": "OmniPth00117501fa000404 L112B", 
      "remotePort": "36", 
      "name": "OmniPth00117501fa000404 L112B"
     }, 
     "35": {
      "lid": "OmniPth00117501fa000404 L110B", 
      "remotePort": "35", 
      "name": "OmniPth00117501fa000404 L110B"
     }, 
     "33": {
      "lid": "OmniPth00117501fa000404 L106B", 
      "remotePort": "33", 
      "name": "OmniPth00117501fa000404 L106B"
     }
    }
   }, 
   "OmniPth00117501fa000404 S207B": {
    "lid": "OmniPth00117501fa000404 S207B", 
    "name": "OmniPth00117501fa000404 S207B", 
    "ports": {
     "29": {
      "lid": "OmniPth00117501fa000404 L117B", 
      "remotePort": "44", 
      "name": "OmniPth00117501fa000404 L117B"
     }, 
     "24": {
      "lid": "OmniPth00117501fa000404 L102B", 
      "remotePort": "25", 
      "name": "OmniPth00117501fa000404 L102B"
     }, 
     "25": {
      "lid": "OmniPth00117501fa000404 L113B", 
      "remotePort": "48", 
      "name": "OmniPth00117501fa000404 L113B"
     }, 
     "20": {
      "lid": "OmniPth00117501fa000404 L110B", 
      "remotePort": "29", 
      "name": "OmniPth00117501fa000404 L110B"
     }, 
     "48": {
      "lid": "OmniPth00117501fa000404 L101B", 
      "remotePort": "25", 
      "name": "OmniPth00117501fa000404 L101B"
     }, 
     "46": {
      "lid": "OmniPth00117501fa000404 L105B", 
      "remotePort": "27", 
      "name": "OmniPth00117501fa000404 L105B"
     }, 
     "44": {
      "lid": "OmniPth00117501fa000404 L109B", 
      "remotePort": "29", 
      "name": "OmniPth00117501fa000404 L109B"
     }, 
     "28": {
      "lid": "OmniPth00117501fa000404 L119B", 
      "remotePort": "45", 
      "name": "OmniPth00117501fa000404 L119B"
     }, 
     "43": {
      "lid": "OmniPth00117501fa000404 L111B", 
      "remotePort": "30", 
      "name": "OmniPth00117501fa000404 L111B"
     }, 
     "41": {
      "lid": "OmniPth00117501fa000404 L105A", 
      "remotePort": "41", 
      "name": "OmniPth00117501fa000404 L105A"
     }, 
     "1": {
      "lid": "OmniPth00117501fa000404 L114B", 
      "remotePort": "48", 
      "name": "OmniPth00117501fa000404 L114B"
     }, 
     "5": {
      "lid": "OmniPth00117501fa000404 L118B", 
      "remotePort": "44", 
      "name": "OmniPth00117501fa000404 L118B"
     }, 
     "7": {
      "lid": "OmniPth00117501fa000404 L102A", 
      "remotePort": "31", 
      "name": "OmniPth00117501fa000404 L102A"
     }, 
     "6": {
      "lid": "OmniPth00117501fa000404 L116B", 
      "remotePort": "43", 
      "name": "OmniPth00117501fa000404 L116B"
     }, 
     "39": {
      "lid": "OmniPth00117501fa000404 L109A", 
      "remotePort": "39", 
      "name": "OmniPth00117501fa000404 L109A"
     }, 
     "12": {
      "lid": "OmniPth00117501fa000404 L116A", 
      "remotePort": "36", 
      "name": "OmniPth00117501fa000404 L116A"
     }, 
     "14": {
      "lid": "OmniPth00117501fa000404 L112A", 
      "remotePort": "38", 
      "name": "OmniPth00117501fa000404 L112A"
     }, 
     "11": {
      "lid": "OmniPth00117501fa000404 L118A", 
      "remotePort": "35", 
      "name": "OmniPth00117501fa000404 L118A"
     }, 
     "13": {
      "lid": "OmniPth00117501fa000404 L114A", 
      "remotePort": "37", 
      "name": "OmniPth00117501fa000404 L114A"
     }, 
     "38": {
      "lid": "OmniPth00117501fa000404 L111A", 
      "remotePort": "38", 
      "name": "OmniPth00117501fa000404 L111A"
     }, 
     "15": {
      "lid": "OmniPth00117501fa000404 L110A", 
      "remotePort": "39", 
      "name": "OmniPth00117501fa000404 L110A"
     }, 
     "22": {
      "lid": "OmniPth00117501fa000404 L106B", 
      "remotePort": "27", 
      "name": "OmniPth00117501fa000404 L106B"
     }, 
     "17": {
      "lid": "OmniPth00117501fa000404 L106A", 
      "remotePort": "41", 
      "name": "OmniPth00117501fa000404 L106A"
     }, 
     "19": {
      "lid": "OmniPth00117501fa000404 L112B", 
      "remotePort": "30", 
      "name": "OmniPth00117501fa000404 L112B"
     }, 
     "31": {
      "lid": "OmniPth00117501fa000404 L101A", 
      "remotePort": "31", 
      "name": "OmniPth00117501fa000404 L101A"
     }, 
     "30": {
      "lid": "OmniPth00117501fa000404 L115B", 
      "remotePort": "43", 
      "name": "OmniPth00117501fa000404 L115B"
     }, 
     "37": {
      "lid": "OmniPth00117501fa000404 L113A", 
      "remotePort": "37", 
      "name": "OmniPth00117501fa000404 L113A"
     }, 
     "36": {
      "lid": "OmniPth00117501fa000404 L115A", 
      "remotePort": "36", 
      "name": "OmniPth00117501fa000404 L115A"
     }, 
     "35": {
      "lid": "OmniPth00117501fa000404 L117A", 
      "remotePort": "35", 
      "name": "OmniPth00117501fa000404 L117A"
     }, 
     "34": {
      "lid": "OmniPth00117501fa000404 L119A", 
      "remotePort": "34", 
      "name": "OmniPth00117501fa000404 L119A"
     }
    }
   }, 
   "OmniPth00117501fa000404 S205B": {
    "lid": "OmniPth00117501fa000404 S205B", 
    "name": "OmniPth00117501fa000404 S205B", 
    "ports": {
     "25": {
      "lid": "OmniPth00117501fa000404 L109B", 
      "remotePort": "48", 
      "name": "OmniPth00117501fa000404 L109B"
     }, 
     "26": {
      "lid": "OmniPth00117501fa000404 L119B", 
      "remotePort": "47", 
      "name": "OmniPth00117501fa000404 L119B"
     }, 
     "27": {
      "lid": "OmniPth00117501fa000404 L117B", 
      "remotePort": "46", 
      "name": "OmniPth00117501fa000404 L117B"
     }, 
     "20": {
      "lid": "OmniPth00117501fa000404 L106B", 
      "remotePort": "29", 
      "name": "OmniPth00117501fa000404 L106B"
     }, 
     "22": {
      "lid": "OmniPth00117501fa000404 L102B", 
      "remotePort": "27", 
      "name": "OmniPth00117501fa000404 L102B"
     }, 
     "46": {
      "lid": "OmniPth00117501fa000404 L101B", 
      "remotePort": "27", 
      "name": "OmniPth00117501fa000404 L101B"
     }, 
     "44": {
      "lid": "OmniPth00117501fa000404 L105B", 
      "remotePort": "29", 
      "name": "OmniPth00117501fa000404 L105B"
     }, 
     "28": {
      "lid": "OmniPth00117501fa000404 L115B", 
      "remotePort": "45", 
      "name": "OmniPth00117501fa000404 L115B"
     }, 
     "29": {
      "lid": "OmniPth00117501fa000404 L113B", 
      "remotePort": "44", 
      "name": "OmniPth00117501fa000404 L113B"
     }, 
     "41": {
      "lid": "OmniPth00117501fa000404 L101A", 
      "remotePort": "41", 
      "name": "OmniPth00117501fa000404 L101A"
     }, 
     "1": {
      "lid": "OmniPth00117501fa000404 L110B", 
      "remotePort": "48", 
      "name": "OmniPth00117501fa000404 L110B"
     }, 
     "3": {
      "lid": "OmniPth00117501fa000404 L118B", 
      "remotePort": "46", 
      "name": "OmniPth00117501fa000404 L118B"
     }, 
     "5": {
      "lid": "OmniPth00117501fa000404 L114B", 
      "remotePort": "44", 
      "name": "OmniPth00117501fa000404 L114B"
     }, 
     "4": {
      "lid": "OmniPth00117501fa000404 L116B", 
      "remotePort": "45", 
      "name": "OmniPth00117501fa000404 L116B"
     }, 
     "6": {
      "lid": "OmniPth00117501fa000404 L112B", 
      "remotePort": "43", 
      "name": "OmniPth00117501fa000404 L112B"
     }, 
     "9": {
      "lid": "OmniPth00117501fa000404 L118A", 
      "remotePort": "33", 
      "name": "OmniPth00117501fa000404 L118A"
     }, 
     "13": {
      "lid": "OmniPth00117501fa000404 L110A", 
      "remotePort": "37", 
      "name": "OmniPth00117501fa000404 L110A"
     }, 
     "11": {
      "lid": "OmniPth00117501fa000404 L114A", 
      "remotePort": "35", 
      "name": "OmniPth00117501fa000404 L114A"
     }, 
     "10": {
      "lid": "OmniPth00117501fa000404 L116A", 
      "remotePort": "34", 
      "name": "OmniPth00117501fa000404 L116A"
     }, 
     "39": {
      "lid": "OmniPth00117501fa000404 L105A", 
      "remotePort": "39", 
      "name": "OmniPth00117501fa000404 L105A"
     }, 
     "12": {
      "lid": "OmniPth00117501fa000404 L112A", 
      "remotePort": "36", 
      "name": "OmniPth00117501fa000404 L112A"
     }, 
     "15": {
      "lid": "OmniPth00117501fa000404 L106A", 
      "remotePort": "39", 
      "name": "OmniPth00117501fa000404 L106A"
     }, 
     "17": {
      "lid": "OmniPth00117501fa000404 L102A", 
      "remotePort": "41", 
      "name": "OmniPth00117501fa000404 L102A"
     }, 
     "33": {
      "lid": "OmniPth00117501fa000404 L117A", 
      "remotePort": "33", 
      "name": "OmniPth00117501fa000404 L117A"
     }, 
     "32": {
      "lid": "OmniPth00117501fa000404 L119A", 
      "remotePort": "32", 
      "name": "OmniPth00117501fa000404 L119A"
     }, 
     "30": {
      "lid": "OmniPth00117501fa000404 L111B", 
      "remotePort": "43", 
      "name": "OmniPth00117501fa000404 L111B"
     }, 
     "37": {
      "lid": "OmniPth00117501fa000404 L109A", 
      "remotePort": "37", 
      "name": "OmniPth00117501fa000404 L109A"
     }, 
     "36": {
      "lid": "OmniPth00117501fa000404 L111A", 
      "remotePort": "36", 
      "name": "OmniPth00117501fa000404 L111A"
     }, 
     "35": {
      "lid": "OmniPth00117501fa000404 L113A", 
      "remotePort": "35", 
      "name": "OmniPth00117501fa000404 L113A"
     }, 
     "34": {
      "lid": "OmniPth00117501fa000404 L115A", 
      "remotePort": "34", 
      "name": "OmniPth00117501fa000404 L115A"
     }
    }
   }, 
   "OmniPth00117501fa000404 S208A": {
    "lid": "OmniPth00117501fa000404 S208A", 
    "name": "OmniPth00117501fa000404 S208A", 
    "ports": {
     "42": {
      "lid": "OmniPth00117501fa000404 L102B", 
      "remotePort": "42", 
      "name": "OmniPth00117501fa000404 L102B"
     }, 
     "29": {
      "lid": "OmniPth00117501fa000404 L112A", 
      "remotePort": "44", 
      "name": "OmniPth00117501fa000404 L112A"
     }, 
     "24": {
      "lid": "OmniPth00117501fa000404 L115A", 
      "remotePort": "25", 
      "name": "OmniPth00117501fa000404 L115A"
     }, 
     "26": {
      "lid": "OmniPth00117501fa000404 L106A", 
      "remotePort": "47", 
      "name": "OmniPth00117501fa000404 L106A"
     }, 
     "20": {
      "lid": "OmniPth00117501fa000404 L119A", 
      "remotePort": "29", 
      "name": "OmniPth00117501fa000404 L119A"
     }, 
     "48": {
      "lid": "OmniPth00117501fa000404 L116A", 
      "remotePort": "25", 
      "name": "OmniPth00117501fa000404 L116A"
     }, 
     "23": {
      "lid": "OmniPth00117501fa000404 L101A", 
      "remotePort": "26", 
      "name": "OmniPth00117501fa000404 L101A"
     }, 
     "47": {
      "lid": "OmniPth00117501fa000404 L102A", 
      "remotePort": "26", 
      "name": "OmniPth00117501fa000404 L102A"
     }, 
     "28": {
      "lid": "OmniPth00117501fa000404 L110A", 
      "remotePort": "45", 
      "name": "OmniPth00117501fa000404 L110A"
     }, 
     "43": {
      "lid": "OmniPth00117501fa000404 L118A", 
      "remotePort": "30", 
      "name": "OmniPth00117501fa000404 L118A"
     }, 
     "2": {
      "lid": "OmniPth00117501fa000404 L105A", 
      "remotePort": "47", 
      "name": "OmniPth00117501fa000404 L105A"
     }, 
     "5": {
      "lid": "OmniPth00117501fa000404 L111A", 
      "remotePort": "44", 
      "name": "OmniPth00117501fa000404 L111A"
     }, 
     "4": {
      "lid": "OmniPth00117501fa000404 L109A", 
      "remotePort": "45", 
      "name": "OmniPth00117501fa000404 L109A"
     }, 
     "6": {
      "lid": "OmniPth00117501fa000404 L113A", 
      "remotePort": "43", 
      "name": "OmniPth00117501fa000404 L113A"
     }, 
     "8": {
      "lid": "OmniPth00117501fa000404 L105B", 
      "remotePort": "32", 
      "name": "OmniPth00117501fa000404 L105B"
     }, 
     "38": {
      "lid": "OmniPth00117501fa000404 L118B", 
      "remotePort": "38", 
      "name": "OmniPth00117501fa000404 L118B"
     }, 
     "11": {
      "lid": "OmniPth00117501fa000404 L111B", 
      "remotePort": "35", 
      "name": "OmniPth00117501fa000404 L111B"
     }, 
     "10": {
      "lid": "OmniPth00117501fa000404 L109B", 
      "remotePort": "34", 
      "name": "OmniPth00117501fa000404 L109B"
     }, 
     "13": {
      "lid": "OmniPth00117501fa000404 L115B", 
      "remotePort": "37", 
      "name": "OmniPth00117501fa000404 L115B"
     }, 
     "12": {
      "lid": "OmniPth00117501fa000404 L113B", 
      "remotePort": "36", 
      "name": "OmniPth00117501fa000404 L113B"
     }, 
     "15": {
      "lid": "OmniPth00117501fa000404 L119B", 
      "remotePort": "39", 
      "name": "OmniPth00117501fa000404 L119B"
     }, 
     "14": {
      "lid": "OmniPth00117501fa000404 L117B", 
      "remotePort": "38", 
      "name": "OmniPth00117501fa000404 L117B"
     }, 
     "19": {
      "lid": "OmniPth00117501fa000404 L117A", 
      "remotePort": "30", 
      "name": "OmniPth00117501fa000404 L117A"
     }, 
     "18": {
      "lid": "OmniPth00117501fa000404 L101B", 
      "remotePort": "42", 
      "name": "OmniPth00117501fa000404 L101B"
     }, 
     "30": {
      "lid": "OmniPth00117501fa000404 L114A", 
      "remotePort": "43", 
      "name": "OmniPth00117501fa000404 L114A"
     }, 
     "37": {
      "lid": "OmniPth00117501fa000404 L116B", 
      "remotePort": "37", 
      "name": "OmniPth00117501fa000404 L116B"
     }, 
     "36": {
      "lid": "OmniPth00117501fa000404 L114B", 
      "remotePort": "36", 
      "name": "OmniPth00117501fa000404 L114B"
     }, 
     "35": {
      "lid": "OmniPth00117501fa000404 L112B", 
      "remotePort": "35", 
      "name": "OmniPth00117501fa000404 L112B"
     }, 
     "34": {
      "lid": "OmniPth00117501fa000404 L110B", 
      "remotePort": "34", 
      "name": "OmniPth00117501fa000404 L110B"
     }, 
     "32": {
      "lid": "OmniPth00117501fa000404 L106B", 
      "remotePort": "32", 
      "name": "OmniPth00117501fa000404 L106B"
     }
    }
   }, 
   "OmniPth00117501fa000404 S205A": {
    "lid": "OmniPth00117501fa000404 S205A", 
    "name": "OmniPth00117501fa000404 S205A", 
    "ports": {
     "22": {
      "lid": "OmniPth00117501fa000404 L117A", 
      "remotePort": "27", 
      "name": "OmniPth00117501fa000404 L117A"
     }, 
     "29": {
      "lid": "OmniPth00117501fa000404 L106A", 
      "remotePort": "44", 
      "name": "OmniPth00117501fa000404 L106A"
     }, 
     "24": {
      "lid": "OmniPth00117501fa000404 L109A", 
      "remotePort": "25", 
      "name": "OmniPth00117501fa000404 L109A"
     }, 
     "27": {
      "lid": "OmniPth00117501fa000404 L102A", 
      "remotePort": "46", 
      "name": "OmniPth00117501fa000404 L102A"
     }, 
     "20": {
      "lid": "OmniPth00117501fa000404 L113A", 
      "remotePort": "29", 
      "name": "OmniPth00117501fa000404 L113A"
     }, 
     "21": {
      "lid": "OmniPth00117501fa000404 L115A", 
      "remotePort": "28", 
      "name": "OmniPth00117501fa000404 L115A"
     }, 
     "48": {
      "lid": "OmniPth00117501fa000404 L110A", 
      "remotePort": "25", 
      "name": "OmniPth00117501fa000404 L110A"
     }, 
     "23": {
      "lid": "OmniPth00117501fa000404 L119A", 
      "remotePort": "26", 
      "name": "OmniPth00117501fa000404 L119A"
     }, 
     "46": {
      "lid": "OmniPth00117501fa000404 L118A", 
      "remotePort": "27", 
      "name": "OmniPth00117501fa000404 L118A"
     }, 
     "44": {
      "lid": "OmniPth00117501fa000404 L114A", 
      "remotePort": "29", 
      "name": "OmniPth00117501fa000404 L114A"
     }, 
     "45": {
      "lid": "OmniPth00117501fa000404 L116A", 
      "remotePort": "28", 
      "name": "OmniPth00117501fa000404 L116A"
     }, 
     "43": {
      "lid": "OmniPth00117501fa000404 L112A", 
      "remotePort": "30", 
      "name": "OmniPth00117501fa000404 L112A"
     }, 
     "40": {
      "lid": "OmniPth00117501fa000404 L116B", 
      "remotePort": "40", 
      "name": "OmniPth00117501fa000404 L116B"
     }, 
     "41": {
      "lid": "OmniPth00117501fa000404 L118B", 
      "remotePort": "41", 
      "name": "OmniPth00117501fa000404 L118B"
     }, 
     "3": {
      "lid": "OmniPth00117501fa000404 L101A", 
      "remotePort": "46", 
      "name": "OmniPth00117501fa000404 L101A"
     }, 
     "5": {
      "lid": "OmniPth00117501fa000404 L105A", 
      "remotePort": "44", 
      "name": "OmniPth00117501fa000404 L105A"
     }, 
     "9": {
      "lid": "OmniPth00117501fa000404 L101B", 
      "remotePort": "33", 
      "name": "OmniPth00117501fa000404 L101B"
     }, 
     "13": {
      "lid": "OmniPth00117501fa000404 L109B", 
      "remotePort": "37", 
      "name": "OmniPth00117501fa000404 L109B"
     }, 
     "11": {
      "lid": "OmniPth00117501fa000404 L105B", 
      "remotePort": "35", 
      "name": "OmniPth00117501fa000404 L105B"
     }, 
     "39": {
      "lid": "OmniPth00117501fa000404 L114B", 
      "remotePort": "39", 
      "name": "OmniPth00117501fa000404 L114B"
     }, 
     "38": {
      "lid": "OmniPth00117501fa000404 L112B", 
      "remotePort": "38", 
      "name": "OmniPth00117501fa000404 L112B"
     }, 
     "15": {
      "lid": "OmniPth00117501fa000404 L113B", 
      "remotePort": "39", 
      "name": "OmniPth00117501fa000404 L113B"
     }, 
     "14": {
      "lid": "OmniPth00117501fa000404 L111B", 
      "remotePort": "38", 
      "name": "OmniPth00117501fa000404 L111B"
     }, 
     "17": {
      "lid": "OmniPth00117501fa000404 L117B", 
      "remotePort": "41", 
      "name": "OmniPth00117501fa000404 L117B"
     }, 
     "16": {
      "lid": "OmniPth00117501fa000404 L115B", 
      "remotePort": "40", 
      "name": "OmniPth00117501fa000404 L115B"
     }, 
     "19": {
      "lid": "OmniPth00117501fa000404 L111A", 
      "remotePort": "30", 
      "name": "OmniPth00117501fa000404 L111A"
     }, 
     "18": {
      "lid": "OmniPth00117501fa000404 L119B", 
      "remotePort": "42", 
      "name": "OmniPth00117501fa000404 L119B"
     }, 
     "37": {
      "lid": "OmniPth00117501fa000404 L110B", 
      "remotePort": "37", 
      "name": "OmniPth00117501fa000404 L110B"
     }, 
     "35": {
      "lid": "OmniPth00117501fa000404 L106B", 
      "remotePort": "35", 
      "name": "OmniPth00117501fa000404 L106B"
     }, 
     "33": {
      "lid": "OmniPth00117501fa000404 L102B", 
      "remotePort": "33", 
      "name": "OmniPth00117501fa000404 L102B"
     }
    }
   }, 
   "OmniPth00117501fa000404 L119B": {
    "lid": "OmniPth00117501fa000404 L119B", 
    "name": "OmniPth00117501fa000404 L119B", 
    "ports": {
     "25": {
      "lid": "OmniPth00117501fa000404 S204B", 
      "remotePort": "48", 
      "name": "OmniPth00117501fa000404 S204B"
     }, 
     "26": {
      "lid": "OmniPth00117501fa000404 S203B", 
      "remotePort": "47", 
      "name": "OmniPth00117501fa000404 S203B"
     }, 
     "27": {
      "lid": "OmniPth00117501fa000404 S202B", 
      "remotePort": "46", 
      "name": "OmniPth00117501fa000404 S202B"
     }, 
     "33": {
      "lid": "OmniPth00117501fa000404 S202A", 
      "remotePort": "9", 
      "name": "OmniPth00117501fa000404 S202A"
     }, 
     "32": {
      "lid": "OmniPth00117501fa000404 S203A", 
      "remotePort": "8", 
      "name": "OmniPth00117501fa000404 S203A"
     }, 
     "31": {
      "lid": "OmniPth00117501fa000404 S204A", 
      "remotePort": "7", 
      "name": "OmniPth00117501fa000404 S204A"
     }, 
     "42": {
      "lid": "OmniPth00117501fa000404 S205A", 
      "remotePort": "18", 
      "name": "OmniPth00117501fa000404 S205A"
     }, 
     "28": {
      "lid": "OmniPth00117501fa000404 S201B", 
      "remotePort": "45", 
      "name": "OmniPth00117501fa000404 S201B"
     }, 
     "39": {
      "lid": "OmniPth00117501fa000404 S208A", 
      "remotePort": "15", 
      "name": "OmniPth00117501fa000404 S208A"
     }, 
     "40": {
      "lid": "OmniPth00117501fa000404 S207A", 
      "remotePort": "16", 
      "name": "OmniPth00117501fa000404 S207A"
     }, 
     "34": {
      "lid": "OmniPth00117501fa000404 S201A", 
      "remotePort": "10", 
      "name": "OmniPth00117501fa000404 S201A"
     }, 
     "46": {
      "lid": "OmniPth00117501fa000404 S206B", 
      "remotePort": "27", 
      "name": "OmniPth00117501fa000404 S206B"
     }, 
     "47": {
      "lid": "OmniPth00117501fa000404 S205B", 
      "remotePort": "26", 
      "name": "OmniPth00117501fa000404 S205B"
     }, 
     "45": {
      "lid": "OmniPth00117501fa000404 S207B", 
      "remotePort": "28", 
      "name": "OmniPth00117501fa000404 S207B"
     }, 
     "41": {
      "lid": "OmniPth00117501fa000404 S206A", 
      "remotePort": "17", 
      "name": "OmniPth00117501fa000404 S206A"
     }, 
     "44": {
      "lid": "OmniPth00117501fa000404 S208B", 
      "remotePort": "29", 
      "name": "OmniPth00117501fa000404 S208B"
     }
    }
   }, 
   "OmniPth00117501fa000404 L109A": {
    "lid": "OmniPth00117501fa000404 L109A", 
    "name": "OmniPth00117501fa000404 L109A", 
    "ports": {
     "43": {
      "lid": "OmniPth00117501fa000404 S206A", 
      "remotePort": "6", 
      "name": "OmniPth00117501fa000404 S206A"
     }, 
     "25": {
      "lid": "OmniPth00117501fa000404 S205A", 
      "remotePort": "24", 
      "name": "OmniPth00117501fa000404 S205A"
     }, 
     "27": {
      "lid": "OmniPth00117501fa000404 S201A", 
      "remotePort": "22", 
      "name": "OmniPth00117501fa000404 S201A"
     }, 
     "44": {
      "lid": "OmniPth00117501fa000404 S207A", 
      "remotePort": "5", 
      "name": "OmniPth00117501fa000404 S207A"
     }, 
     "45": {
      "lid": "OmniPth00117501fa000404 S208A", 
      "remotePort": "4", 
      "name": "OmniPth00117501fa000404 S208A"
     }, 
     "28": {
      "lid": "OmniPth00117501fa000404 S202A", 
      "remotePort": "21", 
      "name": "OmniPth00117501fa000404 S202A"
     }, 
     "29": {
      "lid": "OmniPth00117501fa000404 S203A", 
      "remotePort": "20", 
      "name": "OmniPth00117501fa000404 S203A"
     }, 
     "40": {
      "lid": "OmniPth00117501fa000404 S208B", 
      "remotePort": "40", 
      "name": "OmniPth00117501fa000404 S208B"
     }, 
     "1": {
      "lid": "node259 hfi1_0", 
      "remotePort": "1", 
      "name": "node259 hfi1_0"
     }, 
     "2": {
      "lid": "node269 hfi1_0", 
      "remotePort": "1", 
      "name": "node269 hfi1_0"
     }, 
     "5": {
      "lid": "node263 hfi1_0", 
      "remotePort": "1", 
      "name": "node263 hfi1_0"
     }, 
     "7": {
      "lid": "node267 hfi1_0", 
      "remotePort": "1", 
      "name": "node267 hfi1_0"
     }, 
     "9": {
      "lid": "node083 hfi1_0", 
      "remotePort": "1", 
      "name": "node083 hfi1_0"
     }, 
     "8": {
      "lid": "node265 hfi1_0", 
      "remotePort": "1", 
      "name": "node265 hfi1_0"
     }, 
     "39": {
      "lid": "OmniPth00117501fa000404 S207B", 
      "remotePort": "39", 
      "name": "OmniPth00117501fa000404 S207B"
     }, 
     "38": {
      "lid": "OmniPth00117501fa000404 S206B", 
      "remotePort": "38", 
      "name": "OmniPth00117501fa000404 S206B"
     }, 
     "11": {
      "lid": "node101 hfi1_0", 
      "remotePort": "1", 
      "name": "node101 hfi1_0"
     }, 
     "10": {
      "lid": "node082 hfi1_0", 
      "remotePort": "1", 
      "name": "node082 hfi1_0"
     }, 
     "13": {
      "lid": "node100 hfi1_0", 
      "remotePort": "1", 
      "name": "node100 hfi1_0"
     }, 
     "12": {
      "lid": "node084 hfi1_0", 
      "remotePort": "1", 
      "name": "node084 hfi1_0"
     }, 
     "15": {
      "lid": "node098 hfi1_0", 
      "remotePort": "1", 
      "name": "node098 hfi1_0"
     }, 
     "14": {
      "lid": "node102 hfi1_0", 
      "remotePort": "1", 
      "name": "node102 hfi1_0"
     }, 
     "16": {
      "lid": "node097 hfi1_0", 
      "remotePort": "1", 
      "name": "node097 hfi1_0"
     }, 
     "33": {
      "lid": "OmniPth00117501fa000404 S201B", 
      "remotePort": "33", 
      "name": "OmniPth00117501fa000404 S201B"
     }, 
     "30": {
      "lid": "OmniPth00117501fa000404 S204A", 
      "remotePort": "19", 
      "name": "OmniPth00117501fa000404 S204A"
     }, 
     "37": {
      "lid": "OmniPth00117501fa000404 S205B", 
      "remotePort": "37", 
      "name": "OmniPth00117501fa000404 S205B"
     }, 
     "36": {
      "lid": "OmniPth00117501fa000404 S204B", 
      "remotePort": "36", 
      "name": "OmniPth00117501fa000404 S204B"
     }, 
     "35": {
      "lid": "OmniPth00117501fa000404 S203B", 
      "remotePort": "35", 
      "name": "OmniPth00117501fa000404 S203B"
     }, 
     "34": {
      "lid": "OmniPth00117501fa000404 S202B", 
      "remotePort": "34", 
      "name": "OmniPth00117501fa000404 S202B"
     }
    }
   }, 
   "OmniPth00117501fa000404 L109B": {
    "lid": "OmniPth00117501fa000404 L109B", 
    "name": "OmniPth00117501fa000404 L109B", 
    "ports": {
     "10": {
      "lid": "node081 hfi1_0", 
      "remotePort": "1", 
      "name": "node081 hfi1_0"
     }, 
     "13": {
      "lid": "login001 hfi1_0", 
      "remotePort": "1", 
      "name": "login001 hfi1_0"
     }, 
     "12": {
      "lid": "node079 hfi1_0", 
      "remotePort": "1", 
      "name": "node079 hfi1_0"
     }, 
     "43": {
      "lid": "OmniPth00117501fa000404 S204B", 
      "remotePort": "30", 
      "name": "OmniPth00117501fa000404 S204B"
     }, 
     "48": {
      "lid": "OmniPth00117501fa000404 S205B", 
      "remotePort": "25", 
      "name": "OmniPth00117501fa000404 S205B"
     }, 
     "38": {
      "lid": "OmniPth00117501fa000404 S204A", 
      "remotePort": "14", 
      "name": "OmniPth00117501fa000404 S204A"
     }, 
     "46": {
      "lid": "OmniPth00117501fa000404 S201B", 
      "remotePort": "27", 
      "name": "OmniPth00117501fa000404 S201B"
     }, 
     "39": {
      "lid": "OmniPth00117501fa000404 S203A", 
      "remotePort": "15", 
      "name": "OmniPth00117501fa000404 S203A"
     }, 
     "44": {
      "lid": "OmniPth00117501fa000404 S203B", 
      "remotePort": "29", 
      "name": "OmniPth00117501fa000404 S203B"
     }, 
     "45": {
      "lid": "OmniPth00117501fa000404 S202B", 
      "remotePort": "28", 
      "name": "OmniPth00117501fa000404 S202B"
     }, 
     "37": {
      "lid": "OmniPth00117501fa000404 S205A", 
      "remotePort": "13", 
      "name": "OmniPth00117501fa000404 S205A"
     }, 
     "29": {
      "lid": "OmniPth00117501fa000404 S207B", 
      "remotePort": "44", 
      "name": "OmniPth00117501fa000404 S207B"
     }, 
     "35": {
      "lid": "OmniPth00117501fa000404 S207A", 
      "remotePort": "11", 
      "name": "OmniPth00117501fa000404 S207A"
     }, 
     "34": {
      "lid": "OmniPth00117501fa000404 S208A", 
      "remotePort": "10", 
      "name": "OmniPth00117501fa000404 S208A"
     }, 
     "36": {
      "lid": "OmniPth00117501fa000404 S206A", 
      "remotePort": "12", 
      "name": "OmniPth00117501fa000404 S206A"
     }, 
     "40": {
      "lid": "OmniPth00117501fa000404 S202A", 
      "remotePort": "16", 
      "name": "OmniPth00117501fa000404 S202A"
     }, 
     "7": {
      "lid": "node099 hfi1_0", 
      "remotePort": "1", 
      "name": "node099 hfi1_0"
     }, 
     "30": {
      "lid": "OmniPth00117501fa000404 S206B", 
      "remotePort": "43", 
      "name": "OmniPth00117501fa000404 S206B"
     }, 
     "9": {
      "lid": "node080 hfi1_0", 
      "remotePort": "1", 
      "name": "node080 hfi1_0"
     }, 
     "41": {
      "lid": "OmniPth00117501fa000404 S201A", 
      "remotePort": "17", 
      "name": "OmniPth00117501fa000404 S201A"
     }, 
     "28": {
      "lid": "OmniPth00117501fa000404 S208B", 
      "remotePort": "45", 
      "name": "OmniPth00117501fa000404 S208B"
     }
    }
   }, 
   "i220": {
    "lid": "i220", 
    "name": "i220", 
    "ports": {
     "42": {
      "lid": "zenith-mds1 hfi1_0", 
      "remotePort": "1", 
      "name": "zenith-mds1 hfi1_0"
     }, 
     "43": {
      "lid": "zenith-oss2 hfi1_0", 
      "remotePort": "1", 
      "name": "zenith-oss2 hfi1_0"
     }, 
     "40": {
      "lid": "zenith-oss1 hfi1_0", 
      "remotePort": "1", 
      "name": "zenith-oss1 hfi1_0"
     }, 
     "41": {
      "lid": "zenith-mds2 hfi1_0", 
      "remotePort": "1", 
      "name": "zenith-mds2 hfi1_0"
     }, 
     "5": {
      "lid": "OmniPth00117501fa000404 L111B", 
      "remotePort": "1", 
      "name": "OmniPth00117501fa000404 L111B"
     }, 
     "6": {
      "lid": "OmniPth00117501fa000404 L111A", 
      "remotePort": "4", 
      "name": "OmniPth00117501fa000404 L111A"
     }
    }
   }, 
   "OmniPth00117501fa000404 S201B": {
    "lid": "OmniPth00117501fa000404 S201B", 
    "name": "OmniPth00117501fa000404 S201B", 
    "ports": {
     "24": {
      "lid": "OmniPth00117501fa000404 L114B", 
      "remotePort": "25", 
      "name": "OmniPth00117501fa000404 L114B"
     }, 
     "25": {
      "lid": "OmniPth00117501fa000404 L101B", 
      "remotePort": "48", 
      "name": "OmniPth00117501fa000404 L101B"
     }, 
     "26": {
      "lid": "OmniPth00117501fa000404 L111B", 
      "remotePort": "47", 
      "name": "OmniPth00117501fa000404 L111B"
     }, 
     "27": {
      "lid": "OmniPth00117501fa000404 L109B", 
      "remotePort": "46", 
      "name": "OmniPth00117501fa000404 L109B"
     }, 
     "22": {
      "lid": "OmniPth00117501fa000404 L118B", 
      "remotePort": "27", 
      "name": "OmniPth00117501fa000404 L118B"
     }, 
     "23": {
      "lid": "OmniPth00117501fa000404 L116B", 
      "remotePort": "26", 
      "name": "OmniPth00117501fa000404 L116B"
     }, 
     "46": {
      "lid": "OmniPth00117501fa000404 L117B", 
      "remotePort": "27", 
      "name": "OmniPth00117501fa000404 L117B"
     }, 
     "47": {
      "lid": "OmniPth00117501fa000404 L115B", 
      "remotePort": "26", 
      "name": "OmniPth00117501fa000404 L115B"
     }, 
     "45": {
      "lid": "OmniPth00117501fa000404 L119B", 
      "remotePort": "28", 
      "name": "OmniPth00117501fa000404 L119B"
     }, 
     "42": {
      "lid": "OmniPth00117501fa000404 L115A", 
      "remotePort": "42", 
      "name": "OmniPth00117501fa000404 L115A"
     }, 
     "29": {
      "lid": "OmniPth00117501fa000404 L105B", 
      "remotePort": "44", 
      "name": "OmniPth00117501fa000404 L105B"
     }, 
     "40": {
      "lid": "OmniPth00117501fa000404 L119A", 
      "remotePort": "40", 
      "name": "OmniPth00117501fa000404 L119A"
     }, 
     "41": {
      "lid": "OmniPth00117501fa000404 L117A", 
      "remotePort": "41", 
      "name": "OmniPth00117501fa000404 L117A"
     }, 
     "1": {
      "lid": "OmniPth00117501fa000404 L102B", 
      "remotePort": "48", 
      "name": "OmniPth00117501fa000404 L102B"
     }, 
     "3": {
      "lid": "OmniPth00117501fa000404 L110B", 
      "remotePort": "46", 
      "name": "OmniPth00117501fa000404 L110B"
     }, 
     "2": {
      "lid": "OmniPth00117501fa000404 L112B", 
      "remotePort": "47", 
      "name": "OmniPth00117501fa000404 L112B"
     }, 
     "5": {
      "lid": "OmniPth00117501fa000404 L106B", 
      "remotePort": "44", 
      "name": "OmniPth00117501fa000404 L106B"
     }, 
     "7": {
      "lid": "OmniPth00117501fa000404 L114A", 
      "remotePort": "31", 
      "name": "OmniPth00117501fa000404 L114A"
     }, 
     "9": {
      "lid": "OmniPth00117501fa000404 L110A", 
      "remotePort": "33", 
      "name": "OmniPth00117501fa000404 L110A"
     }, 
     "8": {
      "lid": "OmniPth00117501fa000404 L112A", 
      "remotePort": "32", 
      "name": "OmniPth00117501fa000404 L112A"
     }, 
     "11": {
      "lid": "OmniPth00117501fa000404 L106A", 
      "remotePort": "35", 
      "name": "OmniPth00117501fa000404 L106A"
     }, 
     "13": {
      "lid": "OmniPth00117501fa000404 L102A", 
      "remotePort": "37", 
      "name": "OmniPth00117501fa000404 L102A"
     }, 
     "48": {
      "lid": "OmniPth00117501fa000404 L113B", 
      "remotePort": "25", 
      "name": "OmniPth00117501fa000404 L113B"
     }, 
     "17": {
      "lid": "OmniPth00117501fa000404 L118A", 
      "remotePort": "41", 
      "name": "OmniPth00117501fa000404 L118A"
     }, 
     "33": {
      "lid": "OmniPth00117501fa000404 L109A", 
      "remotePort": "33", 
      "name": "OmniPth00117501fa000404 L109A"
     }, 
     "32": {
      "lid": "OmniPth00117501fa000404 L111A", 
      "remotePort": "32", 
      "name": "OmniPth00117501fa000404 L111A"
     }, 
     "31": {
      "lid": "OmniPth00117501fa000404 L113A", 
      "remotePort": "31", 
      "name": "OmniPth00117501fa000404 L113A"
     }, 
     "37": {
      "lid": "OmniPth00117501fa000404 L101A", 
      "remotePort": "37", 
      "name": "OmniPth00117501fa000404 L101A"
     }, 
     "35": {
      "lid": "OmniPth00117501fa000404 L105A", 
      "remotePort": "35", 
      "name": "OmniPth00117501fa000404 L105A"
     }, 
     "18": {
      "lid": "OmniPth00117501fa000404 L116A", 
      "remotePort": "42", 
      "name": "OmniPth00117501fa000404 L116A"
     }
    }
   }, 
   "OmniPth00117501fa000404 S201A": {
    "lid": "OmniPth00117501fa000404 S201A", 
    "name": "OmniPth00117501fa000404 S201A", 
    "ports": {
     "24": {
      "lid": "OmniPth00117501fa000404 L101A", 
      "remotePort": "25", 
      "name": "OmniPth00117501fa000404 L101A"
     }, 
     "25": {
      "lid": "OmniPth00117501fa000404 L114A", 
      "remotePort": "48", 
      "name": "OmniPth00117501fa000404 L114A"
     }, 
     "26": {
      "lid": "OmniPth00117501fa000404 L116A", 
      "remotePort": "47", 
      "name": "OmniPth00117501fa000404 L116A"
     }, 
     "27": {
      "lid": "OmniPth00117501fa000404 L118A", 
      "remotePort": "46", 
      "name": "OmniPth00117501fa000404 L118A"
     }, 
     "20": {
      "lid": "OmniPth00117501fa000404 L105A", 
      "remotePort": "29", 
      "name": "OmniPth00117501fa000404 L105A"
     }, 
     "22": {
      "lid": "OmniPth00117501fa000404 L109A", 
      "remotePort": "27", 
      "name": "OmniPth00117501fa000404 L109A"
     }, 
     "23": {
      "lid": "OmniPth00117501fa000404 L111A", 
      "remotePort": "26", 
      "name": "OmniPth00117501fa000404 L111A"
     }, 
     "46": {
      "lid": "OmniPth00117501fa000404 L110A", 
      "remotePort": "27", 
      "name": "OmniPth00117501fa000404 L110A"
     }, 
     "47": {
      "lid": "OmniPth00117501fa000404 L112A", 
      "remotePort": "26", 
      "name": "OmniPth00117501fa000404 L112A"
     }, 
     "44": {
      "lid": "OmniPth00117501fa000404 L106A", 
      "remotePort": "29", 
      "name": "OmniPth00117501fa000404 L106A"
     }, 
     "42": {
      "lid": "OmniPth00117501fa000404 L112B", 
      "remotePort": "42", 
      "name": "OmniPth00117501fa000404 L112B"
     }, 
     "41": {
      "lid": "OmniPth00117501fa000404 L110B", 
      "remotePort": "41", 
      "name": "OmniPth00117501fa000404 L110B"
     }, 
     "1": {
      "lid": "OmniPth00117501fa000404 L113A", 
      "remotePort": "48", 
      "name": "OmniPth00117501fa000404 L113A"
     }, 
     "3": {
      "lid": "OmniPth00117501fa000404 L117A", 
      "remotePort": "46", 
      "name": "OmniPth00117501fa000404 L117A"
     }, 
     "2": {
      "lid": "OmniPth00117501fa000404 L115A", 
      "remotePort": "47", 
      "name": "OmniPth00117501fa000404 L115A"
     }, 
     "4": {
      "lid": "OmniPth00117501fa000404 L119A", 
      "remotePort": "45", 
      "name": "OmniPth00117501fa000404 L119A"
     }, 
     "7": {
      "lid": "OmniPth00117501fa000404 L113B", 
      "remotePort": "31", 
      "name": "OmniPth00117501fa000404 L113B"
     }, 
     "9": {
      "lid": "OmniPth00117501fa000404 L117B", 
      "remotePort": "33", 
      "name": "OmniPth00117501fa000404 L117B"
     }, 
     "8": {
      "lid": "OmniPth00117501fa000404 L115B", 
      "remotePort": "32", 
      "name": "OmniPth00117501fa000404 L115B"
     }, 
     "39": {
      "lid": "OmniPth00117501fa000404 L106B", 
      "remotePort": "39", 
      "name": "OmniPth00117501fa000404 L106B"
     }, 
     "10": {
      "lid": "OmniPth00117501fa000404 L119B", 
      "remotePort": "34", 
      "name": "OmniPth00117501fa000404 L119B"
     }, 
     "13": {
      "lid": "OmniPth00117501fa000404 L101B", 
      "remotePort": "37", 
      "name": "OmniPth00117501fa000404 L101B"
     }, 
     "15": {
      "lid": "OmniPth00117501fa000404 L105B", 
      "remotePort": "39", 
      "name": "OmniPth00117501fa000404 L105B"
     }, 
     "48": {
      "lid": "OmniPth00117501fa000404 L102A", 
      "remotePort": "25", 
      "name": "OmniPth00117501fa000404 L102A"
     }, 
     "17": {
      "lid": "OmniPth00117501fa000404 L109B", 
      "remotePort": "41", 
      "name": "OmniPth00117501fa000404 L109B"
     }, 
     "33": {
      "lid": "OmniPth00117501fa000404 L118B", 
      "remotePort": "33", 
      "name": "OmniPth00117501fa000404 L118B"
     }, 
     "18": {
      "lid": "OmniPth00117501fa000404 L111B", 
      "remotePort": "42", 
      "name": "OmniPth00117501fa000404 L111B"
     }, 
     "31": {
      "lid": "OmniPth00117501fa000404 L114B", 
      "remotePort": "31", 
      "name": "OmniPth00117501fa000404 L114B"
     }, 
     "37": {
      "lid": "OmniPth00117501fa000404 L102B", 
      "remotePort": "37", 
      "name": "OmniPth00117501fa000404 L102B"
     }, 
     "32": {
      "lid": "OmniPth00117501fa000404 L116B", 
      "remotePort": "32", 
      "name": "OmniPth00117501fa000404 L116B"
     }
    }
   }, 
   "OmniPth00117501fa000404 L105A": {
    "lid": "OmniPth00117501fa000404 L105A", 
    "name": "OmniPth00117501fa000404 L105A", 
    "ports": {
     "43": {
      "lid": "OmniPth00117501fa000404 S204A", 
      "remotePort": "6", 
      "name": "OmniPth00117501fa000404 S204A"
     }, 
     "25": {
      "lid": "OmniPth00117501fa000404 S203A", 
      "remotePort": "24", 
      "name": "OmniPth00117501fa000404 S203A"
     }, 
     "46": {
      "lid": "OmniPth00117501fa000404 S207A", 
      "remotePort": "3", 
      "name": "OmniPth00117501fa000404 S207A"
     }, 
     "47": {
      "lid": "OmniPth00117501fa000404 S208A", 
      "remotePort": "2", 
      "name": "OmniPth00117501fa000404 S208A"
     }, 
     "44": {
      "lid": "OmniPth00117501fa000404 S205A", 
      "remotePort": "5", 
      "name": "OmniPth00117501fa000404 S205A"
     }, 
     "45": {
      "lid": "OmniPth00117501fa000404 S206A", 
      "remotePort": "4", 
      "name": "OmniPth00117501fa000404 S206A"
     }, 
     "42": {
      "lid": "OmniPth00117501fa000404 S208B", 
      "remotePort": "42", 
      "name": "OmniPth00117501fa000404 S208B"
     }, 
     "29": {
      "lid": "OmniPth00117501fa000404 S201A", 
      "remotePort": "20", 
      "name": "OmniPth00117501fa000404 S201A"
     }, 
     "40": {
      "lid": "OmniPth00117501fa000404 S206B", 
      "remotePort": "40", 
      "name": "OmniPth00117501fa000404 S206B"
     }, 
     "41": {
      "lid": "OmniPth00117501fa000404 S207B", 
      "remotePort": "41", 
      "name": "OmniPth00117501fa000404 S207B"
     }, 
     "1": {
      "lid": "node253 hfi1_0", 
      "remotePort": "1", 
      "name": "node253 hfi1_0"
     }, 
     "3": {
      "lid": "node249 hfi1_0", 
      "remotePort": "1", 
      "name": "node249 hfi1_0"
     }, 
     "2": {
      "lid": "node256 hfi1_0", 
      "remotePort": "1", 
      "name": "node256 hfi1_0"
     }, 
     "5": {
      "lid": "node250 hfi1_0", 
      "remotePort": "1", 
      "name": "node250 hfi1_0"
     }, 
     "7": {
      "lid": "node255 hfi1_0", 
      "remotePort": "1", 
      "name": "node255 hfi1_0"
     }, 
     "6": {
      "lid": "node251 hfi1_0", 
      "remotePort": "1", 
      "name": "node251 hfi1_0"
     }, 
     "9": {
      "lid": "node233 hfi1_0", 
      "remotePort": "1", 
      "name": "node233 hfi1_0"
     }, 
     "8": {
      "lid": "node254 hfi1_0", 
      "remotePort": "1", 
      "name": "node254 hfi1_0"
     }, 
     "39": {
      "lid": "OmniPth00117501fa000404 S205B", 
      "remotePort": "39", 
      "name": "OmniPth00117501fa000404 S205B"
     }, 
     "38": {
      "lid": "OmniPth00117501fa000404 S204B", 
      "remotePort": "38", 
      "name": "OmniPth00117501fa000404 S204B"
     }, 
     "11": {
      "lid": "node239 hfi1_0", 
      "remotePort": "1", 
      "name": "node239 hfi1_0"
     }, 
     "10": {
      "lid": "node243 hfi1_0", 
      "remotePort": "1", 
      "name": "node243 hfi1_0"
     }, 
     "13": {
      "lid": "node246 hfi1_0", 
      "remotePort": "1", 
      "name": "node246 hfi1_0"
     }, 
     "12": {
      "lid": "node237 hfi1_0", 
      "remotePort": "1", 
      "name": "node237 hfi1_0"
     }, 
     "15": {
      "lid": "node248 hfi1_0", 
      "remotePort": "1", 
      "name": "node248 hfi1_0"
     }, 
     "14": {
      "lid": "node245 hfi1_0", 
      "remotePort": "1", 
      "name": "node245 hfi1_0"
     }, 
     "16": {
      "lid": "node247 hfi1_0", 
      "remotePort": "1", 
      "name": "node247 hfi1_0"
     }, 
     "30": {
      "lid": "OmniPth00117501fa000404 S202A", 
      "remotePort": "19", 
      "name": "OmniPth00117501fa000404 S202A"
     }, 
     "37": {
      "lid": "OmniPth00117501fa000404 S203B", 
      "remotePort": "37", 
      "name": "OmniPth00117501fa000404 S203B"
     }, 
     "36": {
      "lid": "OmniPth00117501fa000404 S202B", 
      "remotePort": "36", 
      "name": "OmniPth00117501fa000404 S202B"
     }, 
     "35": {
      "lid": "OmniPth00117501fa000404 S201B", 
      "remotePort": "35", 
      "name": "OmniPth00117501fa000404 S201B"
     }
    }
   }, 
   "OmniPth00117501fa000404 L105B": {
    "lid": "OmniPth00117501fa000404 L105B", 
    "name": "OmniPth00117501fa000404 L105B", 
    "ports": {
     "29": {
      "lid": "OmniPth00117501fa000404 S205B", 
      "remotePort": "44", 
      "name": "OmniPth00117501fa000404 S205B"
     }, 
     "26": {
      "lid": "OmniPth00117501fa000404 S208B", 
      "remotePort": "47", 
      "name": "OmniPth00117501fa000404 S208B"
     }, 
     "27": {
      "lid": "OmniPth00117501fa000404 S207B", 
      "remotePort": "46", 
      "name": "OmniPth00117501fa000404 S207B"
     }, 
     "48": {
      "lid": "OmniPth00117501fa000404 S203B", 
      "remotePort": "25", 
      "name": "OmniPth00117501fa000404 S203B"
     }, 
     "44": {
      "lid": "OmniPth00117501fa000404 S201B", 
      "remotePort": "29", 
      "name": "OmniPth00117501fa000404 S201B"
     }, 
     "28": {
      "lid": "OmniPth00117501fa000404 S206B", 
      "remotePort": "45", 
      "name": "OmniPth00117501fa000404 S206B"
     }, 
     "43": {
      "lid": "OmniPth00117501fa000404 S202B", 
      "remotePort": "30", 
      "name": "OmniPth00117501fa000404 S202B"
     }, 
     "3": {
      "lid": "node260 hfi1_0", 
      "remotePort": "1", 
      "name": "node260 hfi1_0"
     }, 
     "2": {
      "lid": "node266 hfi1_0", 
      "remotePort": "1", 
      "name": "node266 hfi1_0"
     }, 
     "5": {
      "lid": "node258 hfi1_0", 
      "remotePort": "1", 
      "name": "node258 hfi1_0"
     }, 
     "4": {
      "lid": "node257 hfi1_0", 
      "remotePort": "1", 
      "name": "node257 hfi1_0"
     }, 
     "7": {
      "lid": "node262 hfi1_0", 
      "remotePort": "1", 
      "name": "node262 hfi1_0"
     }, 
     "6": {
      "lid": "node261 hfi1_0", 
      "remotePort": "1", 
      "name": "node261 hfi1_0"
     }, 
     "9": {
      "lid": "node236 hfi1_0", 
      "remotePort": "1", 
      "name": "node236 hfi1_0"
     }, 
     "8": {
      "lid": "node264 hfi1_0", 
      "remotePort": "1", 
      "name": "node264 hfi1_0"
     }, 
     "39": {
      "lid": "OmniPth00117501fa000404 S201A", 
      "remotePort": "15", 
      "name": "OmniPth00117501fa000404 S201A"
     }, 
     "38": {
      "lid": "OmniPth00117501fa000404 S202A", 
      "remotePort": "14", 
      "name": "OmniPth00117501fa000404 S202A"
     }, 
     "11": {
      "lid": "node244 hfi1_0", 
      "remotePort": "1", 
      "name": "node244 hfi1_0"
     }, 
     "10": {
      "lid": "node241 hfi1_0", 
      "remotePort": "1", 
      "name": "node241 hfi1_0"
     }, 
     "13": {
      "lid": "node240 hfi1_0", 
      "remotePort": "1", 
      "name": "node240 hfi1_0"
     }, 
     "12": {
      "lid": "node238 hfi1_0", 
      "remotePort": "1", 
      "name": "node238 hfi1_0"
     }, 
     "15": {
      "lid": "node234 hfi1_0", 
      "remotePort": "1", 
      "name": "node234 hfi1_0"
     }, 
     "14": {
      "lid": "node235 hfi1_0", 
      "remotePort": "1", 
      "name": "node235 hfi1_0"
     }, 
     "16": {
      "lid": "node242 hfi1_0", 
      "remotePort": "1", 
      "name": "node242 hfi1_0"
     }, 
     "33": {
      "lid": "OmniPth00117501fa000404 S207A", 
      "remotePort": "9", 
      "name": "OmniPth00117501fa000404 S207A"
     }, 
     "32": {
      "lid": "OmniPth00117501fa000404 S208A", 
      "remotePort": "8", 
      "name": "OmniPth00117501fa000404 S208A"
     }, 
     "30": {
      "lid": "OmniPth00117501fa000404 S204B", 
      "remotePort": "43", 
      "name": "OmniPth00117501fa000404 S204B"
     }, 
     "37": {
      "lid": "OmniPth00117501fa000404 S203A", 
      "remotePort": "13", 
      "name": "OmniPth00117501fa000404 S203A"
     }, 
     "36": {
      "lid": "OmniPth00117501fa000404 S204A", 
      "remotePort": "12", 
      "name": "OmniPth00117501fa000404 S204A"
     }, 
     "35": {
      "lid": "OmniPth00117501fa000404 S205A", 
      "remotePort": "11", 
      "name": "OmniPth00117501fa000404 S205A"
     }, 
     "34": {
      "lid": "OmniPth00117501fa000404 S206A", 
      "remotePort": "10", 
      "name": "OmniPth00117501fa000404 S206A"
     }
    }
   }, 
   "OmniPth00117501fa000404 S208B": {
    "lid": "OmniPth00117501fa000404 S208B", 
    "name": "OmniPth00117501fa000404 S208B", 
    "ports": {
     "29": {
      "lid": "OmniPth00117501fa000404 L119B", 
      "remotePort": "44", 
      "name": "OmniPth00117501fa000404 L119B"
     }, 
     "25": {
      "lid": "OmniPth00117501fa000404 L115B", 
      "remotePort": "48", 
      "name": "OmniPth00117501fa000404 L115B"
     }, 
     "26": {
      "lid": "OmniPth00117501fa000404 L101B", 
      "remotePort": "47", 
      "name": "OmniPth00117501fa000404 L101B"
     }, 
     "20": {
      "lid": "OmniPth00117501fa000404 L112B", 
      "remotePort": "29", 
      "name": "OmniPth00117501fa000404 L112B"
     }, 
     "21": {
      "lid": "OmniPth00117501fa000404 L110B", 
      "remotePort": "28", 
      "name": "OmniPth00117501fa000404 L110B"
     }, 
     "23": {
      "lid": "OmniPth00117501fa000404 L106B", 
      "remotePort": "26", 
      "name": "OmniPth00117501fa000404 L106B"
     }, 
     "47": {
      "lid": "OmniPth00117501fa000404 L105B", 
      "remotePort": "26", 
      "name": "OmniPth00117501fa000404 L105B"
     }, 
     "44": {
      "lid": "OmniPth00117501fa000404 L111B", 
      "remotePort": "29", 
      "name": "OmniPth00117501fa000404 L111B"
     }, 
     "45": {
      "lid": "OmniPth00117501fa000404 L109B", 
      "remotePort": "28", 
      "name": "OmniPth00117501fa000404 L109B"
     }, 
     "42": {
      "lid": "OmniPth00117501fa000404 L105A", 
      "remotePort": "42", 
      "name": "OmniPth00117501fa000404 L105A"
     }, 
     "43": {
      "lid": "OmniPth00117501fa000404 L113B", 
      "remotePort": "30", 
      "name": "OmniPth00117501fa000404 L113B"
     }, 
     "40": {
      "lid": "OmniPth00117501fa000404 L109A", 
      "remotePort": "40", 
      "name": "OmniPth00117501fa000404 L109A"
     }, 
     "1": {
      "lid": "OmniPth00117501fa000404 L116B", 
      "remotePort": "48", 
      "name": "OmniPth00117501fa000404 L116B"
     }, 
     "2": {
      "lid": "OmniPth00117501fa000404 L102B", 
      "remotePort": "47", 
      "name": "OmniPth00117501fa000404 L102B"
     }, 
     "6": {
      "lid": "OmniPth00117501fa000404 L118B", 
      "remotePort": "43", 
      "name": "OmniPth00117501fa000404 L118B"
     }, 
     "8": {
      "lid": "OmniPth00117501fa000404 L102A", 
      "remotePort": "32", 
      "name": "OmniPth00117501fa000404 L102A"
     }, 
     "13": {
      "lid": "OmniPth00117501fa000404 L116A", 
      "remotePort": "37", 
      "name": "OmniPth00117501fa000404 L116A"
     }, 
     "12": {
      "lid": "OmniPth00117501fa000404 L118A", 
      "remotePort": "36", 
      "name": "OmniPth00117501fa000404 L118A"
     }, 
     "39": {
      "lid": "OmniPth00117501fa000404 L111A", 
      "remotePort": "39", 
      "name": "OmniPth00117501fa000404 L111A"
     }, 
     "38": {
      "lid": "OmniPth00117501fa000404 L113A", 
      "remotePort": "38", 
      "name": "OmniPth00117501fa000404 L113A"
     }, 
     "15": {
      "lid": "OmniPth00117501fa000404 L112A", 
      "remotePort": "39", 
      "name": "OmniPth00117501fa000404 L112A"
     }, 
     "14": {
      "lid": "OmniPth00117501fa000404 L114A", 
      "remotePort": "38", 
      "name": "OmniPth00117501fa000404 L114A"
     }, 
     "16": {
      "lid": "OmniPth00117501fa000404 L110A", 
      "remotePort": "40", 
      "name": "OmniPth00117501fa000404 L110A"
     }, 
     "19": {
      "lid": "OmniPth00117501fa000404 L114B", 
      "remotePort": "30", 
      "name": "OmniPth00117501fa000404 L114B"
     }, 
     "18": {
      "lid": "OmniPth00117501fa000404 L106A", 
      "remotePort": "42", 
      "name": "OmniPth00117501fa000404 L106A"
     }, 
     "30": {
      "lid": "OmniPth00117501fa000404 L117B", 
      "remotePort": "43", 
      "name": "OmniPth00117501fa000404 L117B"
     }, 
     "37": {
      "lid": "OmniPth00117501fa000404 L115A", 
      "remotePort": "37", 
      "name": "OmniPth00117501fa000404 L115A"
     }, 
     "36": {
      "lid": "OmniPth00117501fa000404 L117A", 
      "remotePort": "36", 
      "name": "OmniPth00117501fa000404 L117A"
     }, 
     "35": {
      "lid": "OmniPth00117501fa000404 L119A", 
      "remotePort": "35", 
      "name": "OmniPth00117501fa000404 L119A"
     }, 
     "32": {
      "lid": "OmniPth00117501fa000404 L101A", 
      "remotePort": "32", 
      "name": "OmniPth00117501fa000404 L101A"
     }
    }
   }, 
   "OmniPth00117501fa000404 L113B": {
    "lid": "OmniPth00117501fa000404 L113B", 
    "name": "OmniPth00117501fa000404 L113B", 
    "ports": {
     "25": {
      "lid": "OmniPth00117501fa000404 S201B", 
      "remotePort": "48", 
      "name": "OmniPth00117501fa000404 S201B"
     }, 
     "39": {
      "lid": "OmniPth00117501fa000404 S205A", 
      "remotePort": "15", 
      "name": "OmniPth00117501fa000404 S205A"
     }, 
     "38": {
      "lid": "OmniPth00117501fa000404 S206A", 
      "remotePort": "14", 
      "name": "OmniPth00117501fa000404 S206A"
     }, 
     "43": {
      "lid": "OmniPth00117501fa000404 S206B", 
      "remotePort": "30", 
      "name": "OmniPth00117501fa000404 S206B"
     }, 
     "48": {
      "lid": "OmniPth00117501fa000404 S207B", 
      "remotePort": "25", 
      "name": "OmniPth00117501fa000404 S207B"
     }, 
     "46": {
      "lid": "OmniPth00117501fa000404 S203B", 
      "remotePort": "27", 
      "name": "OmniPth00117501fa000404 S203B"
     }, 
     "42": {
      "lid": "OmniPth00117501fa000404 S202A", 
      "remotePort": "18", 
      "name": "OmniPth00117501fa000404 S202A"
     }, 
     "31": {
      "lid": "OmniPth00117501fa000404 S201A", 
      "remotePort": "7", 
      "name": "OmniPth00117501fa000404 S201A"
     }, 
     "30": {
      "lid": "OmniPth00117501fa000404 S208B", 
      "remotePort": "43", 
      "name": "OmniPth00117501fa000404 S208B"
     }, 
     "37": {
      "lid": "OmniPth00117501fa000404 S207A", 
      "remotePort": "13", 
      "name": "OmniPth00117501fa000404 S207A"
     }, 
     "36": {
      "lid": "OmniPth00117501fa000404 S208A", 
      "remotePort": "12", 
      "name": "OmniPth00117501fa000404 S208A"
     }, 
     "40": {
      "lid": "OmniPth00117501fa000404 S204A", 
      "remotePort": "16", 
      "name": "OmniPth00117501fa000404 S204A"
     }, 
     "41": {
      "lid": "OmniPth00117501fa000404 S203A", 
      "remotePort": "17", 
      "name": "OmniPth00117501fa000404 S203A"
     }, 
     "47": {
      "lid": "OmniPth00117501fa000404 S202B", 
      "remotePort": "26", 
      "name": "OmniPth00117501fa000404 S202B"
     }, 
     "45": {
      "lid": "OmniPth00117501fa000404 S204B", 
      "remotePort": "28", 
      "name": "OmniPth00117501fa000404 S204B"
     }, 
     "44": {
      "lid": "OmniPth00117501fa000404 S205B", 
      "remotePort": "29", 
      "name": "OmniPth00117501fa000404 S205B"
     }
    }
   }, 
   "OmniPth00117501fa000404 L113A": {
    "lid": "OmniPth00117501fa000404 L113A", 
    "name": "OmniPth00117501fa000404 L113A", 
    "ports": {
     "25": {
      "lid": "OmniPth00117501fa000404 S207A", 
      "remotePort": "24", 
      "name": "OmniPth00117501fa000404 S207A"
     }, 
     "26": {
      "lid": "OmniPth00117501fa000404 S202A", 
      "remotePort": "23", 
      "name": "OmniPth00117501fa000404 S202A"
     }, 
     "27": {
      "lid": "OmniPth00117501fa000404 S203A", 
      "remotePort": "22", 
      "name": "OmniPth00117501fa000404 S203A"
     }, 
     "43": {
      "lid": "OmniPth00117501fa000404 S208A", 
      "remotePort": "6", 
      "name": "OmniPth00117501fa000404 S208A"
     }, 
     "48": {
      "lid": "OmniPth00117501fa000404 S201A", 
      "remotePort": "1", 
      "name": "OmniPth00117501fa000404 S201A"
     }, 
     "38": {
      "lid": "OmniPth00117501fa000404 S208B", 
      "remotePort": "38", 
      "name": "OmniPth00117501fa000404 S208B"
     }, 
     "33": {
      "lid": "OmniPth00117501fa000404 S203B", 
      "remotePort": "33", 
      "name": "OmniPth00117501fa000404 S203B"
     }, 
     "32": {
      "lid": "OmniPth00117501fa000404 S202B", 
      "remotePort": "32", 
      "name": "OmniPth00117501fa000404 S202B"
     }, 
     "31": {
      "lid": "OmniPth00117501fa000404 S201B", 
      "remotePort": "31", 
      "name": "OmniPth00117501fa000404 S201B"
     }, 
     "30": {
      "lid": "OmniPth00117501fa000404 S206A", 
      "remotePort": "19", 
      "name": "OmniPth00117501fa000404 S206A"
     }, 
     "28": {
      "lid": "OmniPth00117501fa000404 S204A", 
      "remotePort": "21", 
      "name": "OmniPth00117501fa000404 S204A"
     }, 
     "29": {
      "lid": "OmniPth00117501fa000404 S205A", 
      "remotePort": "20", 
      "name": "OmniPth00117501fa000404 S205A"
     }, 
     "35": {
      "lid": "OmniPth00117501fa000404 S205B", 
      "remotePort": "35", 
      "name": "OmniPth00117501fa000404 S205B"
     }, 
     "34": {
      "lid": "OmniPth00117501fa000404 S204B", 
      "remotePort": "34", 
      "name": "OmniPth00117501fa000404 S204B"
     }, 
     "36": {
      "lid": "OmniPth00117501fa000404 S206B", 
      "remotePort": "36", 
      "name": "OmniPth00117501fa000404 S206B"
     }, 
     "37": {
      "lid": "OmniPth00117501fa000404 S207B", 
      "remotePort": "37", 
      "name": "OmniPth00117501fa000404 S207B"
     }
    }
   }, 
   "OmniPth00117501fa000404 L110B": {
    "lid": "OmniPth00117501fa000404 L110B", 
    "name": "OmniPth00117501fa000404 L110B", 
    "ports": {
     "43": {
      "lid": "OmniPth00117501fa000404 S204B", 
      "remotePort": "6", 
      "name": "OmniPth00117501fa000404 S204B"
     }, 
     "48": {
      "lid": "OmniPth00117501fa000404 S205B", 
      "remotePort": "1", 
      "name": "OmniPth00117501fa000404 S205B"
     }, 
     "46": {
      "lid": "OmniPth00117501fa000404 S201B", 
      "remotePort": "3", 
      "name": "OmniPth00117501fa000404 S201B"
     }, 
     "44": {
      "lid": "OmniPth00117501fa000404 S203B", 
      "remotePort": "5", 
      "name": "OmniPth00117501fa000404 S203B"
     }, 
     "45": {
      "lid": "OmniPth00117501fa000404 S202B", 
      "remotePort": "4", 
      "name": "OmniPth00117501fa000404 S202B"
     }, 
     "28": {
      "lid": "OmniPth00117501fa000404 S208B", 
      "remotePort": "21", 
      "name": "OmniPth00117501fa000404 S208B"
     }, 
     "29": {
      "lid": "OmniPth00117501fa000404 S207B", 
      "remotePort": "20", 
      "name": "OmniPth00117501fa000404 S207B"
     }, 
     "40": {
      "lid": "OmniPth00117501fa000404 S202A", 
      "remotePort": "40", 
      "name": "OmniPth00117501fa000404 S202A"
     }, 
     "41": {
      "lid": "OmniPth00117501fa000404 S201A", 
      "remotePort": "41", 
      "name": "OmniPth00117501fa000404 S201A"
     }, 
     "1": {
      "lid": "node027 hfi1_0", 
      "remotePort": "1", 
      "name": "node027 hfi1_0"
     }, 
     "3": {
      "lid": "node126 hfi1_0", 
      "remotePort": "1", 
      "name": "node126 hfi1_0"
     }, 
     "2": {
      "lid": "node030 hfi1_0", 
      "remotePort": "1", 
      "name": "node030 hfi1_0"
     }, 
     "5": {
      "lid": "node036 hfi1_0", 
      "remotePort": "1", 
      "name": "node036 hfi1_0"
     }, 
     "7": {
      "lid": "node123 hfi1_0", 
      "remotePort": "1", 
      "name": "node123 hfi1_0"
     }, 
     "6": {
      "lid": "node031 hfi1_0", 
      "remotePort": "1", 
      "name": "node031 hfi1_0"
     }, 
     "9": {
      "lid": "node006 hfi1_0", 
      "remotePort": "1", 
      "name": "node006 hfi1_0"
     }, 
     "8": {
      "lid": "node122 hfi1_0", 
      "remotePort": "1", 
      "name": "node122 hfi1_0"
     }, 
     "39": {
      "lid": "OmniPth00117501fa000404 S203A", 
      "remotePort": "39", 
      "name": "OmniPth00117501fa000404 S203A"
     }, 
     "38": {
      "lid": "OmniPth00117501fa000404 S204A", 
      "remotePort": "38", 
      "name": "OmniPth00117501fa000404 S204A"
     }, 
     "11": {
      "lid": "node034 hfi1_0", 
      "remotePort": "1", 
      "name": "node034 hfi1_0"
     }, 
     "10": {
      "lid": "node033 hfi1_0", 
      "remotePort": "1", 
      "name": "node033 hfi1_0"
     }, 
     "13": {
      "lid": "node004 hfi1_0", 
      "remotePort": "1", 
      "name": "node004 hfi1_0"
     }, 
     "12": {
      "lid": "node016 hfi1_0", 
      "remotePort": "1", 
      "name": "node016 hfi1_0"
     }, 
     "15": {
      "lid": "node125 hfi1_0", 
      "remotePort": "1", 
      "name": "node125 hfi1_0"
     }, 
     "14": {
      "lid": "node002 hfi1_0", 
      "remotePort": "1", 
      "name": "node002 hfi1_0"
     }, 
     "30": {
      "lid": "OmniPth00117501fa000404 S206B", 
      "remotePort": "19", 
      "name": "OmniPth00117501fa000404 S206B"
     }, 
     "37": {
      "lid": "OmniPth00117501fa000404 S205A", 
      "remotePort": "37", 
      "name": "OmniPth00117501fa000404 S205A"
     }, 
     "36": {
      "lid": "OmniPth00117501fa000404 S206A", 
      "remotePort": "36", 
      "name": "OmniPth00117501fa000404 S206A"
     }, 
     "35": {
      "lid": "OmniPth00117501fa000404 S207A", 
      "remotePort": "35", 
      "name": "OmniPth00117501fa000404 S207A"
     }, 
     "34": {
      "lid": "OmniPth00117501fa000404 S208A", 
      "remotePort": "34", 
      "name": "OmniPth00117501fa000404 S208A"
     }
    }
   }, 
   "OmniPth00117501fa000404 L110A": {
    "lid": "OmniPth00117501fa000404 L110A", 
    "name": "OmniPth00117501fa000404 L110A", 
    "ports": {
     "43": {
      "lid": "OmniPth00117501fa000404 S206A", 
      "remotePort": "30", 
      "name": "OmniPth00117501fa000404 S206A"
     }, 
     "25": {
      "lid": "OmniPth00117501fa000404 S205A", 
      "remotePort": "48", 
      "name": "OmniPth00117501fa000404 S205A"
     }, 
     "27": {
      "lid": "OmniPth00117501fa000404 S201A", 
      "remotePort": "46", 
      "name": "OmniPth00117501fa000404 S201A"
     }, 
     "44": {
      "lid": "OmniPth00117501fa000404 S207A", 
      "remotePort": "29", 
      "name": "OmniPth00117501fa000404 S207A"
     }, 
     "45": {
      "lid": "OmniPth00117501fa000404 S208A", 
      "remotePort": "28", 
      "name": "OmniPth00117501fa000404 S208A"
     }, 
     "28": {
      "lid": "OmniPth00117501fa000404 S202A", 
      "remotePort": "45", 
      "name": "OmniPth00117501fa000404 S202A"
     }, 
     "29": {
      "lid": "OmniPth00117501fa000404 S203A", 
      "remotePort": "44", 
      "name": "OmniPth00117501fa000404 S203A"
     }, 
     "40": {
      "lid": "OmniPth00117501fa000404 S208B", 
      "remotePort": "16", 
      "name": "OmniPth00117501fa000404 S208B"
     }, 
     "1": {
      "lid": "node195 hfi1_0", 
      "remotePort": "1", 
      "name": "node195 hfi1_0"
     }, 
     "3": {
      "lid": "node193 hfi1_0", 
      "remotePort": "1", 
      "name": "node193 hfi1_0"
     }, 
     "5": {
      "lid": "node200 hfi1_0", 
      "remotePort": "1", 
      "name": "node200 hfi1_0"
     }, 
     "4": {
      "lid": "node194 hfi1_0", 
      "remotePort": "1", 
      "name": "node194 hfi1_0"
     }, 
     "7": {
      "lid": "node199 hfi1_0", 
      "remotePort": "1", 
      "name": "node199 hfi1_0"
     }, 
     "6": {
      "lid": "node197 hfi1_0", 
      "remotePort": "1", 
      "name": "node197 hfi1_0"
     }, 
     "9": {
      "lid": "node127 hfi1_0", 
      "remotePort": "1", 
      "name": "node127 hfi1_0"
     }, 
     "8": {
      "lid": "node198 hfi1_0", 
      "remotePort": "1", 
      "name": "node198 hfi1_0"
     }, 
     "39": {
      "lid": "OmniPth00117501fa000404 S207B", 
      "remotePort": "15", 
      "name": "OmniPth00117501fa000404 S207B"
     }, 
     "38": {
      "lid": "OmniPth00117501fa000404 S206B", 
      "remotePort": "14", 
      "name": "OmniPth00117501fa000404 S206B"
     }, 
     "11": {
      "lid": "node018 hfi1_0", 
      "remotePort": "1", 
      "name": "node018 hfi1_0"
     }, 
     "10": {
      "lid": "node015 hfi1_0", 
      "remotePort": "1", 
      "name": "node015 hfi1_0"
     }, 
     "13": {
      "lid": "node001 hfi1_0", 
      "remotePort": "1", 
      "name": "node001 hfi1_0"
     }, 
     "12": {
      "lid": "node005 hfi1_0", 
      "remotePort": "1", 
      "name": "node005 hfi1_0"
     }, 
     "15": {
      "lid": "node003 hfi1_0", 
      "remotePort": "1", 
      "name": "node003 hfi1_0"
     }, 
     "14": {
      "lid": "node017 hfi1_0", 
      "remotePort": "1", 
      "name": "node017 hfi1_0"
     }, 
     "16": {
      "lid": "node013 hfi1_0", 
      "remotePort": "1", 
      "name": "node013 hfi1_0"
     }, 
     "33": {
      "lid": "OmniPth00117501fa000404 S201B", 
      "remotePort": "9", 
      "name": "OmniPth00117501fa000404 S201B"
     }, 
     "30": {
      "lid": "OmniPth00117501fa000404 S204A", 
      "remotePort": "43", 
      "name": "OmniPth00117501fa000404 S204A"
     }, 
     "37": {
      "lid": "OmniPth00117501fa000404 S205B", 
      "remotePort": "13", 
      "name": "OmniPth00117501fa000404 S205B"
     }, 
     "36": {
      "lid": "OmniPth00117501fa000404 S204B", 
      "remotePort": "12", 
      "name": "OmniPth00117501fa000404 S204B"
     }, 
     "35": {
      "lid": "OmniPth00117501fa000404 S203B", 
      "remotePort": "11", 
      "name": "OmniPth00117501fa000404 S203B"
     }, 
     "34": {
      "lid": "OmniPth00117501fa000404 S202B", 
      "remotePort": "10", 
      "name": "OmniPth00117501fa000404 S202B"
     }
    }
   }, 
   "OmniPth00117501fa000404 L116A": {
    "lid": "OmniPth00117501fa000404 L116A", 
    "name": "OmniPth00117501fa000404 L116A", 
    "ports": {
     "25": {
      "lid": "OmniPth00117501fa000404 S208A", 
      "remotePort": "48", 
      "name": "OmniPth00117501fa000404 S208A"
     }, 
     "26": {
      "lid": "OmniPth00117501fa000404 S203A", 
      "remotePort": "47", 
      "name": "OmniPth00117501fa000404 S203A"
     }, 
     "27": {
      "lid": "OmniPth00117501fa000404 S204A", 
      "remotePort": "46", 
      "name": "OmniPth00117501fa000404 S204A"
     }, 
     "48": {
      "lid": "OmniPth00117501fa000404 S202A", 
      "remotePort": "25", 
      "name": "OmniPth00117501fa000404 S202A"
     }, 
     "33": {
      "lid": "OmniPth00117501fa000404 S204B", 
      "remotePort": "9", 
      "name": "OmniPth00117501fa000404 S204B"
     }, 
     "28": {
      "lid": "OmniPth00117501fa000404 S205A", 
      "remotePort": "45", 
      "name": "OmniPth00117501fa000404 S205A"
     }, 
     "31": {
      "lid": "OmniPth00117501fa000404 S202B", 
      "remotePort": "7", 
      "name": "OmniPth00117501fa000404 S202B"
     }, 
     "30": {
      "lid": "OmniPth00117501fa000404 S207A", 
      "remotePort": "43", 
      "name": "OmniPth00117501fa000404 S207A"
     }, 
     "42": {
      "lid": "OmniPth00117501fa000404 S201B", 
      "remotePort": "18", 
      "name": "OmniPth00117501fa000404 S201B"
     }, 
     "36": {
      "lid": "OmniPth00117501fa000404 S207B", 
      "remotePort": "12", 
      "name": "OmniPth00117501fa000404 S207B"
     }, 
     "35": {
      "lid": "OmniPth00117501fa000404 S206B", 
      "remotePort": "11", 
      "name": "OmniPth00117501fa000404 S206B"
     }, 
     "29": {
      "lid": "OmniPth00117501fa000404 S206A", 
      "remotePort": "44", 
      "name": "OmniPth00117501fa000404 S206A"
     }, 
     "47": {
      "lid": "OmniPth00117501fa000404 S201A", 
      "remotePort": "26", 
      "name": "OmniPth00117501fa000404 S201A"
     }, 
     "37": {
      "lid": "OmniPth00117501fa000404 S208B", 
      "remotePort": "13", 
      "name": "OmniPth00117501fa000404 S208B"
     }, 
     "32": {
      "lid": "OmniPth00117501fa000404 S203B", 
      "remotePort": "8", 
      "name": "OmniPth00117501fa000404 S203B"
     }, 
     "9": {
      "lid": "node023 hfi1_0", 
      "remotePort": "1", 
      "name": "node023 hfi1_0"
     }, 
     "8": {
      "lid": "node024 hfi1_0", 
      "remotePort": "1", 
      "name": "node024 hfi1_0"
     }, 
     "34": {
      "lid": "OmniPth00117501fa000404 S205B", 
      "remotePort": "10", 
      "name": "OmniPth00117501fa000404 S205B"
     }
    }
   }, 
   "OmniPth00117501fa000404 L116B": {
    "lid": "OmniPth00117501fa000404 L116B", 
    "name": "OmniPth00117501fa000404 L116B", 
    "ports": {
     "25": {
      "lid": "OmniPth00117501fa000404 S202B", 
      "remotePort": "24", 
      "name": "OmniPth00117501fa000404 S202B"
     }, 
     "26": {
      "lid": "OmniPth00117501fa000404 S201B", 
      "remotePort": "23", 
      "name": "OmniPth00117501fa000404 S201B"
     }, 
     "38": {
      "lid": "OmniPth00117501fa000404 S207A", 
      "remotePort": "38", 
      "name": "OmniPth00117501fa000404 S207A"
     }, 
     "43": {
      "lid": "OmniPth00117501fa000404 S207B", 
      "remotePort": "6", 
      "name": "OmniPth00117501fa000404 S207B"
     }, 
     "48": {
      "lid": "OmniPth00117501fa000404 S208B", 
      "remotePort": "1", 
      "name": "OmniPth00117501fa000404 S208B"
     }, 
     "16": {
      "lid": "node119 hfi1_0", 
      "remotePort": "1", 
      "name": "node119 hfi1_0"
     }, 
     "46": {
      "lid": "OmniPth00117501fa000404 S204B", 
      "remotePort": "3", 
      "name": "OmniPth00117501fa000404 S204B"
     }, 
     "32": {
      "lid": "OmniPth00117501fa000404 S201A", 
      "remotePort": "32", 
      "name": "OmniPth00117501fa000404 S201A"
     }, 
     "31": {
      "lid": "OmniPth00117501fa000404 S202A", 
      "remotePort": "31", 
      "name": "OmniPth00117501fa000404 S202A"
     }, 
     "42": {
      "lid": "OmniPth00117501fa000404 S203A", 
      "remotePort": "42", 
      "name": "OmniPth00117501fa000404 S203A"
     }, 
     "37": {
      "lid": "OmniPth00117501fa000404 S208A", 
      "remotePort": "37", 
      "name": "OmniPth00117501fa000404 S208A"
     }, 
     "39": {
      "lid": "OmniPth00117501fa000404 S206A", 
      "remotePort": "39", 
      "name": "OmniPth00117501fa000404 S206A"
     }, 
     "40": {
      "lid": "OmniPth00117501fa000404 S205A", 
      "remotePort": "40", 
      "name": "OmniPth00117501fa000404 S205A"
     }, 
     "41": {
      "lid": "OmniPth00117501fa000404 S204A", 
      "remotePort": "41", 
      "name": "OmniPth00117501fa000404 S204A"
     }, 
     "3": {
      "lid": "node118 hfi1_0", 
      "remotePort": "1", 
      "name": "node118 hfi1_0"
     }, 
     "4": {
      "lid": "node019 hfi1_0", 
      "remotePort": "1", 
      "name": "node019 hfi1_0"
     }, 
     "47": {
      "lid": "OmniPth00117501fa000404 S203B", 
      "remotePort": "2", 
      "name": "OmniPth00117501fa000404 S203B"
     }, 
     "45": {
      "lid": "OmniPth00117501fa000404 S205B", 
      "remotePort": "4", 
      "name": "OmniPth00117501fa000404 S205B"
     }, 
     "44": {
      "lid": "OmniPth00117501fa000404 S206B", 
      "remotePort": "5", 
      "name": "OmniPth00117501fa000404 S206B"
     }
    }
   }, 
   "OmniPth00117501fa000404 L102A": {
    "lid": "OmniPth00117501fa000404 L102A", 
    "name": "OmniPth00117501fa000404 L102A", 
    "ports": {
     "25": {
      "lid": "OmniPth00117501fa000404 S201A", 
      "remotePort": "48", 
      "name": "OmniPth00117501fa000404 S201A"
     }, 
     "26": {
      "lid": "OmniPth00117501fa000404 S208A", 
      "remotePort": "47", 
      "name": "OmniPth00117501fa000404 S208A"
     }, 
     "48": {
      "lid": "OmniPth00117501fa000404 S207A", 
      "remotePort": "25", 
      "name": "OmniPth00117501fa000404 S207A"
     }, 
     "46": {
      "lid": "OmniPth00117501fa000404 S205A", 
      "remotePort": "27", 
      "name": "OmniPth00117501fa000404 S205A"
     }, 
     "47": {
      "lid": "OmniPth00117501fa000404 S206A", 
      "remotePort": "26", 
      "name": "OmniPth00117501fa000404 S206A"
     }, 
     "44": {
      "lid": "OmniPth00117501fa000404 S203A", 
      "remotePort": "29", 
      "name": "OmniPth00117501fa000404 S203A"
     }, 
     "45": {
      "lid": "OmniPth00117501fa000404 S204A", 
      "remotePort": "28", 
      "name": "OmniPth00117501fa000404 S204A"
     }, 
     "42": {
      "lid": "OmniPth00117501fa000404 S206B", 
      "remotePort": "18", 
      "name": "OmniPth00117501fa000404 S206B"
     }, 
     "43": {
      "lid": "OmniPth00117501fa000404 S202A", 
      "remotePort": "30", 
      "name": "OmniPth00117501fa000404 S202A"
     }, 
     "40": {
      "lid": "OmniPth00117501fa000404 S204B", 
      "remotePort": "16", 
      "name": "OmniPth00117501fa000404 S204B"
     }, 
     "41": {
      "lid": "OmniPth00117501fa000404 S205B", 
      "remotePort": "17", 
      "name": "OmniPth00117501fa000404 S205B"
     }, 
     "1": {
      "lid": "node138 hfi1_0", 
      "remotePort": "1", 
      "name": "node138 hfi1_0"
     }, 
     "3": {
      "lid": "node140 hfi1_0", 
      "remotePort": "1", 
      "name": "node140 hfi1_0"
     }, 
     "2": {
      "lid": "node142 hfi1_0", 
      "remotePort": "1", 
      "name": "node142 hfi1_0"
     }, 
     "4": {
      "lid": "node136 hfi1_0", 
      "remotePort": "1", 
      "name": "node136 hfi1_0"
     }, 
     "7": {
      "lid": "node132 hfi1_0", 
      "remotePort": "1", 
      "name": "node132 hfi1_0"
     }, 
     "6": {
      "lid": "node129 hfi1_0", 
      "remotePort": "1", 
      "name": "node129 hfi1_0"
     }, 
     "9": {
      "lid": "node135 hfi1_0", 
      "remotePort": "1", 
      "name": "node135 hfi1_0"
     }, 
     "8": {
      "lid": "node130 hfi1_0", 
      "remotePort": "1", 
      "name": "node130 hfi1_0"
     }, 
     "39": {
      "lid": "OmniPth00117501fa000404 S203B", 
      "remotePort": "15", 
      "name": "OmniPth00117501fa000404 S203B"
     }, 
     "38": {
      "lid": "OmniPth00117501fa000404 S202B", 
      "remotePort": "14", 
      "name": "OmniPth00117501fa000404 S202B"
     }, 
     "11": {
      "lid": "node131 hfi1_0", 
      "remotePort": "1", 
      "name": "node131 hfi1_0"
     }, 
     "10": {
      "lid": "node143 hfi1_0", 
      "remotePort": "1", 
      "name": "node143 hfi1_0"
     }, 
     "13": {
      "lid": "node137 hfi1_0", 
      "remotePort": "1", 
      "name": "node137 hfi1_0"
     }, 
     "12": {
      "lid": "node139 hfi1_0", 
      "remotePort": "1", 
      "name": "node139 hfi1_0"
     }, 
     "15": {
      "lid": "node133 hfi1_0", 
      "remotePort": "1", 
      "name": "node133 hfi1_0"
     }, 
     "14": {
      "lid": "node141 hfi1_0", 
      "remotePort": "1", 
      "name": "node141 hfi1_0"
     }, 
     "16": {
      "lid": "node144 hfi1_0", 
      "remotePort": "1", 
      "name": "node144 hfi1_0"
     }, 
     "32": {
      "lid": "OmniPth00117501fa000404 S208B", 
      "remotePort": "8", 
      "name": "OmniPth00117501fa000404 S208B"
     }, 
     "31": {
      "lid": "OmniPth00117501fa000404 S207B", 
      "remotePort": "7", 
      "name": "OmniPth00117501fa000404 S207B"
     }, 
     "37": {
      "lid": "OmniPth00117501fa000404 S201B", 
      "remotePort": "13", 
      "name": "OmniPth00117501fa000404 S201B"
     }
    }
   }, 
   "OmniPth00117501fa000404 L102B": {
    "lid": "OmniPth00117501fa000404 L102B", 
    "name": "OmniPth00117501fa000404 L102B", 
    "ports": {
     "42": {
      "lid": "OmniPth00117501fa000404 S208A", 
      "remotePort": "42", 
      "name": "OmniPth00117501fa000404 S208A"
     }, 
     "25": {
      "lid": "OmniPth00117501fa000404 S207B", 
      "remotePort": "24", 
      "name": "OmniPth00117501fa000404 S207B"
     }, 
     "26": {
      "lid": "OmniPth00117501fa000404 S206B", 
      "remotePort": "23", 
      "name": "OmniPth00117501fa000404 S206B"
     }, 
     "27": {
      "lid": "OmniPth00117501fa000404 S205B", 
      "remotePort": "22", 
      "name": "OmniPth00117501fa000404 S205B"
     }, 
     "48": {
      "lid": "OmniPth00117501fa000404 S201B", 
      "remotePort": "1", 
      "name": "OmniPth00117501fa000404 S201B"
     }, 
     "47": {
      "lid": "OmniPth00117501fa000404 S208B", 
      "remotePort": "2", 
      "name": "OmniPth00117501fa000404 S208B"
     }, 
     "28": {
      "lid": "OmniPth00117501fa000404 S204B", 
      "remotePort": "21", 
      "name": "OmniPth00117501fa000404 S204B"
     }, 
     "29": {
      "lid": "OmniPth00117501fa000404 S203B", 
      "remotePort": "20", 
      "name": "OmniPth00117501fa000404 S203B"
     }, 
     "1": {
      "lid": "node163 hfi1_0", 
      "remotePort": "1", 
      "name": "node163 hfi1_0"
     }, 
     "3": {
      "lid": "node162 hfi1_0", 
      "remotePort": "1", 
      "name": "node162 hfi1_0"
     }, 
     "2": {
      "lid": "node161 hfi1_0", 
      "remotePort": "1", 
      "name": "node161 hfi1_0"
     }, 
     "5": {
      "lid": "node149 hfi1_0", 
      "remotePort": "1", 
      "name": "node149 hfi1_0"
     }, 
     "4": {
      "lid": "node165 hfi1_0", 
      "remotePort": "1", 
      "name": "node165 hfi1_0"
     }, 
     "7": {
      "lid": "node152 hfi1_0", 
      "remotePort": "1", 
      "name": "node152 hfi1_0"
     }, 
     "6": {
      "lid": "node150 hfi1_0", 
      "remotePort": "1", 
      "name": "node150 hfi1_0"
     }, 
     "9": {
      "lid": "node145 hfi1_0", 
      "remotePort": "1", 
      "name": "node145 hfi1_0"
     }, 
     "8": {
      "lid": "node151 hfi1_0", 
      "remotePort": "1", 
      "name": "node151 hfi1_0"
     }, 
     "11": {
      "lid": "node148 hfi1_0", 
      "remotePort": "1", 
      "name": "node148 hfi1_0"
     }, 
     "10": {
      "lid": "node146 hfi1_0", 
      "remotePort": "1", 
      "name": "node146 hfi1_0"
     }, 
     "13": {
      "lid": "node154 hfi1_0", 
      "remotePort": "1", 
      "name": "node154 hfi1_0"
     }, 
     "12": {
      "lid": "node147 hfi1_0", 
      "remotePort": "1", 
      "name": "node147 hfi1_0"
     }, 
     "15": {
      "lid": "node156 hfi1_0", 
      "remotePort": "1", 
      "name": "node156 hfi1_0"
     }, 
     "14": {
      "lid": "node155 hfi1_0", 
      "remotePort": "1", 
      "name": "node155 hfi1_0"
     }, 
     "16": {
      "lid": "hfi1_0", 
      "remotePort": "1", 
      "name": "hfi1_0"
     }, 
     "33": {
      "lid": "OmniPth00117501fa000404 S205A", 
      "remotePort": "33", 
      "name": "OmniPth00117501fa000404 S205A"
     }, 
     "32": {
      "lid": "OmniPth00117501fa000404 S206A", 
      "remotePort": "32", 
      "name": "OmniPth00117501fa000404 S206A"
     }, 
     "31": {
      "lid": "OmniPth00117501fa000404 S207A", 
      "remotePort": "31", 
      "name": "OmniPth00117501fa000404 S207A"
     }, 
     "30": {
      "lid": "OmniPth00117501fa000404 S202B", 
      "remotePort": "19", 
      "name": "OmniPth00117501fa000404 S202B"
     }, 
     "37": {
      "lid": "OmniPth00117501fa000404 S201A", 
      "remotePort": "37", 
      "name": "OmniPth00117501fa000404 S201A"
     }, 
     "36": {
      "lid": "OmniPth00117501fa000404 S202A", 
      "remotePort": "36", 
      "name": "OmniPth00117501fa000404 S202A"
     }, 
     "35": {
      "lid": "OmniPth00117501fa000404 S203A", 
      "remotePort": "35", 
      "name": "OmniPth00117501fa000404 S203A"
     }, 
     "34": {
      "lid": "OmniPth00117501fa000404 S204A", 
      "remotePort": "34", 
      "name": "OmniPth00117501fa000404 S204A"
     }
    }
   }, 
   "OmniPth00117501fa000404 S204A": {
    "lid": "OmniPth00117501fa000404 S204A", 
    "name": "OmniPth00117501fa000404 S204A", 
    "ports": {
     "28": {
      "lid": "OmniPth00117501fa000404 L102A", 
      "remotePort": "45", 
      "name": "OmniPth00117501fa000404 L102A"
     }, 
     "20": {
      "lid": "OmniPth00117501fa000404 L111A", 
      "remotePort": "29", 
      "name": "OmniPth00117501fa000404 L111A"
     }, 
     "21": {
      "lid": "OmniPth00117501fa000404 L113A", 
      "remotePort": "28", 
      "name": "OmniPth00117501fa000404 L113A"
     }, 
     "22": {
      "lid": "OmniPth00117501fa000404 L115A", 
      "remotePort": "27", 
      "name": "OmniPth00117501fa000404 L115A"
     }, 
     "23": {
      "lid": "OmniPth00117501fa000404 L117A", 
      "remotePort": "26", 
      "name": "OmniPth00117501fa000404 L117A"
     }, 
     "46": {
      "lid": "OmniPth00117501fa000404 L116A", 
      "remotePort": "27", 
      "name": "OmniPth00117501fa000404 L116A"
     }, 
     "47": {
      "lid": "OmniPth00117501fa000404 L118A", 
      "remotePort": "26", 
      "name": "OmniPth00117501fa000404 L118A"
     }, 
     "44": {
      "lid": "OmniPth00117501fa000404 L112A", 
      "remotePort": "29", 
      "name": "OmniPth00117501fa000404 L112A"
     }, 
     "45": {
      "lid": "OmniPth00117501fa000404 L114A", 
      "remotePort": "28", 
      "name": "OmniPth00117501fa000404 L114A"
     }, 
     "42": {
      "lid": "OmniPth00117501fa000404 L118B", 
      "remotePort": "42", 
      "name": "OmniPth00117501fa000404 L118B"
     }, 
     "43": {
      "lid": "OmniPth00117501fa000404 L110A", 
      "remotePort": "30", 
      "name": "OmniPth00117501fa000404 L110A"
     }, 
     "40": {
      "lid": "OmniPth00117501fa000404 L114B", 
      "remotePort": "40", 
      "name": "OmniPth00117501fa000404 L114B"
     }, 
     "41": {
      "lid": "OmniPth00117501fa000404 L116B", 
      "remotePort": "41", 
      "name": "OmniPth00117501fa000404 L116B"
     }, 
     "1": {
      "lid": "OmniPth00117501fa000404 L119A", 
      "remotePort": "48", 
      "name": "OmniPth00117501fa000404 L119A"
     }, 
     "4": {
      "lid": "OmniPth00117501fa000404 L101A", 
      "remotePort": "45", 
      "name": "OmniPth00117501fa000404 L101A"
     }, 
     "7": {
      "lid": "OmniPth00117501fa000404 L119B", 
      "remotePort": "31", 
      "name": "OmniPth00117501fa000404 L119B"
     }, 
     "6": {
      "lid": "OmniPth00117501fa000404 L105A", 
      "remotePort": "43", 
      "name": "OmniPth00117501fa000404 L105A"
     }, 
     "12": {
      "lid": "OmniPth00117501fa000404 L105B", 
      "remotePort": "36", 
      "name": "OmniPth00117501fa000404 L105B"
     }, 
     "10": {
      "lid": "OmniPth00117501fa000404 L101B", 
      "remotePort": "34", 
      "name": "OmniPth00117501fa000404 L101B"
     }, 
     "39": {
      "lid": "OmniPth00117501fa000404 L112B", 
      "remotePort": "39", 
      "name": "OmniPth00117501fa000404 L112B"
     }, 
     "38": {
      "lid": "OmniPth00117501fa000404 L110B", 
      "remotePort": "38", 
      "name": "OmniPth00117501fa000404 L110B"
     }, 
     "15": {
      "lid": "OmniPth00117501fa000404 L111B", 
      "remotePort": "39", 
      "name": "OmniPth00117501fa000404 L111B"
     }, 
     "14": {
      "lid": "OmniPth00117501fa000404 L109B", 
      "remotePort": "38", 
      "name": "OmniPth00117501fa000404 L109B"
     }, 
     "17": {
      "lid": "OmniPth00117501fa000404 L115B", 
      "remotePort": "41", 
      "name": "OmniPth00117501fa000404 L115B"
     }, 
     "16": {
      "lid": "OmniPth00117501fa000404 L113B", 
      "remotePort": "40", 
      "name": "OmniPth00117501fa000404 L113B"
     }, 
     "19": {
      "lid": "OmniPth00117501fa000404 L109A", 
      "remotePort": "30", 
      "name": "OmniPth00117501fa000404 L109A"
     }, 
     "18": {
      "lid": "OmniPth00117501fa000404 L117B", 
      "remotePort": "42", 
      "name": "OmniPth00117501fa000404 L117B"
     }, 
     "30": {
      "lid": "OmniPth00117501fa000404 L106A", 
      "remotePort": "43", 
      "name": "OmniPth00117501fa000404 L106A"
     }, 
     "36": {
      "lid": "OmniPth00117501fa000404 L106B", 
      "remotePort": "36", 
      "name": "OmniPth00117501fa000404 L106B"
     }, 
     "34": {
      "lid": "OmniPth00117501fa000404 L102B", 
      "remotePort": "34", 
      "name": "OmniPth00117501fa000404 L102B"
     }
    }
   }, 
   "OmniPth00117501fa000404 L114A": {
    "lid": "OmniPth00117501fa000404 L114A", 
    "name": "OmniPth00117501fa000404 L114A", 
    "ports": {
     "25": {
      "lid": "OmniPth00117501fa000404 S207A", 
      "remotePort": "48", 
      "name": "OmniPth00117501fa000404 S207A"
     }, 
     "26": {
      "lid": "OmniPth00117501fa000404 S202A", 
      "remotePort": "47", 
      "name": "OmniPth00117501fa000404 S202A"
     }, 
     "27": {
      "lid": "OmniPth00117501fa000404 S203A", 
      "remotePort": "46", 
      "name": "OmniPth00117501fa000404 S203A"
     }, 
     "43": {
      "lid": "OmniPth00117501fa000404 S208A", 
      "remotePort": "30", 
      "name": "OmniPth00117501fa000404 S208A"
     }, 
     "48": {
      "lid": "OmniPth00117501fa000404 S201A", 
      "remotePort": "25", 
      "name": "OmniPth00117501fa000404 S201A"
     }, 
     "38": {
      "lid": "OmniPth00117501fa000404 S208B", 
      "remotePort": "14", 
      "name": "OmniPth00117501fa000404 S208B"
     }, 
     "33": {
      "lid": "OmniPth00117501fa000404 S203B", 
      "remotePort": "9", 
      "name": "OmniPth00117501fa000404 S203B"
     }, 
     "32": {
      "lid": "OmniPth00117501fa000404 S202B", 
      "remotePort": "8", 
      "name": "OmniPth00117501fa000404 S202B"
     }, 
     "31": {
      "lid": "OmniPth00117501fa000404 S201B", 
      "remotePort": "7", 
      "name": "OmniPth00117501fa000404 S201B"
     }, 
     "30": {
      "lid": "OmniPth00117501fa000404 S206A", 
      "remotePort": "43", 
      "name": "OmniPth00117501fa000404 S206A"
     }, 
     "28": {
      "lid": "OmniPth00117501fa000404 S204A", 
      "remotePort": "45", 
      "name": "OmniPth00117501fa000404 S204A"
     }, 
     "29": {
      "lid": "OmniPth00117501fa000404 S205A", 
      "remotePort": "44", 
      "name": "OmniPth00117501fa000404 S205A"
     }, 
     "35": {
      "lid": "OmniPth00117501fa000404 S205B", 
      "remotePort": "11", 
      "name": "OmniPth00117501fa000404 S205B"
     }, 
     "34": {
      "lid": "OmniPth00117501fa000404 S204B", 
      "remotePort": "10", 
      "name": "OmniPth00117501fa000404 S204B"
     }, 
     "36": {
      "lid": "OmniPth00117501fa000404 S206B", 
      "remotePort": "12", 
      "name": "OmniPth00117501fa000404 S206B"
     }, 
     "37": {
      "lid": "OmniPth00117501fa000404 S207B", 
      "remotePort": "13", 
      "name": "OmniPth00117501fa000404 S207B"
     }
    }
   }, 
   "OmniPth00117501fa000404 L106A": {
    "lid": "OmniPth00117501fa000404 L106A", 
    "name": "OmniPth00117501fa000404 L106A", 
    "ports": {
     "43": {
      "lid": "OmniPth00117501fa000404 S204A", 
      "remotePort": "30", 
      "name": "OmniPth00117501fa000404 S204A"
     }, 
     "25": {
      "lid": "OmniPth00117501fa000404 S203A", 
      "remotePort": "48", 
      "name": "OmniPth00117501fa000404 S203A"
     }, 
     "46": {
      "lid": "OmniPth00117501fa000404 S207A", 
      "remotePort": "27", 
      "name": "OmniPth00117501fa000404 S207A"
     }, 
     "47": {
      "lid": "OmniPth00117501fa000404 S208A", 
      "remotePort": "26", 
      "name": "OmniPth00117501fa000404 S208A"
     }, 
     "44": {
      "lid": "OmniPth00117501fa000404 S205A", 
      "remotePort": "29", 
      "name": "OmniPth00117501fa000404 S205A"
     }, 
     "45": {
      "lid": "OmniPth00117501fa000404 S206A", 
      "remotePort": "28", 
      "name": "OmniPth00117501fa000404 S206A"
     }, 
     "42": {
      "lid": "OmniPth00117501fa000404 S208B", 
      "remotePort": "18", 
      "name": "OmniPth00117501fa000404 S208B"
     }, 
     "29": {
      "lid": "OmniPth00117501fa000404 S201A", 
      "remotePort": "44", 
      "name": "OmniPth00117501fa000404 S201A"
     }, 
     "40": {
      "lid": "OmniPth00117501fa000404 S206B", 
      "remotePort": "16", 
      "name": "OmniPth00117501fa000404 S206B"
     }, 
     "41": {
      "lid": "OmniPth00117501fa000404 S207B", 
      "remotePort": "17", 
      "name": "OmniPth00117501fa000404 S207B"
     }, 
     "1": {
      "lid": "node168 hfi1_0", 
      "remotePort": "1", 
      "name": "node168 hfi1_0"
     }, 
     "3": {
      "lid": "node167 hfi1_0", 
      "remotePort": "1", 
      "name": "node167 hfi1_0"
     }, 
     "2": {
      "lid": "node166 hfi1_0", 
      "remotePort": "1", 
      "name": "node166 hfi1_0"
     }, 
     "5": {
      "lid": "node159 hfi1_0", 
      "remotePort": "1", 
      "name": "node159 hfi1_0"
     }, 
     "4": {
      "lid": "node164 hfi1_0", 
      "remotePort": "1", 
      "name": "node164 hfi1_0"
     }, 
     "7": {
      "lid": "node157 hfi1_0", 
      "remotePort": "1", 
      "name": "node157 hfi1_0"
     }, 
     "6": {
      "lid": "node160 hfi1_0", 
      "remotePort": "1", 
      "name": "node160 hfi1_0"
     }, 
     "9": {
      "lid": "node180 hfi1_0", 
      "remotePort": "1", 
      "name": "node180 hfi1_0"
     }, 
     "8": {
      "lid": "node158 hfi1_0", 
      "remotePort": "1", 
      "name": "node158 hfi1_0"
     }, 
     "39": {
      "lid": "OmniPth00117501fa000404 S205B", 
      "remotePort": "15", 
      "name": "OmniPth00117501fa000404 S205B"
     }, 
     "38": {
      "lid": "OmniPth00117501fa000404 S204B", 
      "remotePort": "14", 
      "name": "OmniPth00117501fa000404 S204B"
     }, 
     "11": {
      "lid": "node183 hfi1_0", 
      "remotePort": "1", 
      "name": "node183 hfi1_0"
     }, 
     "10": {
      "lid": "node184 hfi1_0", 
      "remotePort": "1", 
      "name": "node184 hfi1_0"
     }, 
     "13": {
      "lid": "node172 hfi1_0", 
      "remotePort": "1", 
      "name": "node172 hfi1_0"
     }, 
     "12": {
      "lid": "node175 hfi1_0", 
      "remotePort": "1", 
      "name": "node175 hfi1_0"
     }, 
     "15": {
      "lid": "node169 hfi1_0", 
      "remotePort": "1", 
      "name": "node169 hfi1_0"
     }, 
     "14": {
      "lid": "node171 hfi1_0", 
      "remotePort": "1", 
      "name": "node171 hfi1_0"
     }, 
     "16": {
      "lid": "node170 hfi1_0", 
      "remotePort": "1", 
      "name": "node170 hfi1_0"
     }, 
     "30": {
      "lid": "OmniPth00117501fa000404 S202A", 
      "remotePort": "43", 
      "name": "OmniPth00117501fa000404 S202A"
     }, 
     "37": {
      "lid": "OmniPth00117501fa000404 S203B", 
      "remotePort": "13", 
      "name": "OmniPth00117501fa000404 S203B"
     }, 
     "36": {
      "lid": "OmniPth00117501fa000404 S202B", 
      "remotePort": "12", 
      "name": "OmniPth00117501fa000404 S202B"
     }, 
     "35": {
      "lid": "OmniPth00117501fa000404 S201B", 
      "remotePort": "11", 
      "name": "OmniPth00117501fa000404 S201B"
     }
    }
   }, 
   "OmniPth00117501fa000404 S204B": {
    "lid": "OmniPth00117501fa000404 S204B", 
    "name": "OmniPth00117501fa000404 S204B", 
    "ports": {
     "43": {
      "lid": "OmniPth00117501fa000404 L105B", 
      "remotePort": "30", 
      "name": "OmniPth00117501fa000404 L105B"
     }, 
     "26": {
      "lid": "OmniPth00117501fa000404 L117B", 
      "remotePort": "47", 
      "name": "OmniPth00117501fa000404 L117B"
     }, 
     "27": {
      "lid": "OmniPth00117501fa000404 L115B", 
      "remotePort": "46", 
      "name": "OmniPth00117501fa000404 L115B"
     }, 
     "21": {
      "lid": "OmniPth00117501fa000404 L102B", 
      "remotePort": "28", 
      "name": "OmniPth00117501fa000404 L102B"
     }, 
     "48": {
      "lid": "OmniPth00117501fa000404 L119B", 
      "remotePort": "25", 
      "name": "OmniPth00117501fa000404 L119B"
     }, 
     "45": {
      "lid": "OmniPth00117501fa000404 L101B", 
      "remotePort": "28", 
      "name": "OmniPth00117501fa000404 L101B"
     }, 
     "28": {
      "lid": "OmniPth00117501fa000404 L113B", 
      "remotePort": "45", 
      "name": "OmniPth00117501fa000404 L113B"
     }, 
     "29": {
      "lid": "OmniPth00117501fa000404 L111B", 
      "remotePort": "44", 
      "name": "OmniPth00117501fa000404 L111B"
     }, 
     "40": {
      "lid": "OmniPth00117501fa000404 L101A", 
      "remotePort": "40", 
      "name": "OmniPth00117501fa000404 L101A"
     }, 
     "3": {
      "lid": "OmniPth00117501fa000404 L116B", 
      "remotePort": "46", 
      "name": "OmniPth00117501fa000404 L116B"
     }, 
     "2": {
      "lid": "OmniPth00117501fa000404 L118B", 
      "remotePort": "47", 
      "name": "OmniPth00117501fa000404 L118B"
     }, 
     "5": {
      "lid": "OmniPth00117501fa000404 L112B", 
      "remotePort": "44", 
      "name": "OmniPth00117501fa000404 L112B"
     }, 
     "4": {
      "lid": "OmniPth00117501fa000404 L114B", 
      "remotePort": "45", 
      "name": "OmniPth00117501fa000404 L114B"
     }, 
     "6": {
      "lid": "OmniPth00117501fa000404 L110B", 
      "remotePort": "43", 
      "name": "OmniPth00117501fa000404 L110B"
     }, 
     "9": {
      "lid": "OmniPth00117501fa000404 L116A", 
      "remotePort": "33", 
      "name": "OmniPth00117501fa000404 L116A"
     }, 
     "8": {
      "lid": "OmniPth00117501fa000404 L118A", 
      "remotePort": "32", 
      "name": "OmniPth00117501fa000404 L118A"
     }, 
     "38": {
      "lid": "OmniPth00117501fa000404 L105A", 
      "remotePort": "38", 
      "name": "OmniPth00117501fa000404 L105A"
     }, 
     "11": {
      "lid": "OmniPth00117501fa000404 L112A", 
      "remotePort": "35", 
      "name": "OmniPth00117501fa000404 L112A"
     }, 
     "10": {
      "lid": "OmniPth00117501fa000404 L114A", 
      "remotePort": "34", 
      "name": "OmniPth00117501fa000404 L114A"
     }, 
     "12": {
      "lid": "OmniPth00117501fa000404 L110A", 
      "remotePort": "36", 
      "name": "OmniPth00117501fa000404 L110A"
     }, 
     "14": {
      "lid": "OmniPth00117501fa000404 L106A", 
      "remotePort": "38", 
      "name": "OmniPth00117501fa000404 L106A"
     }, 
     "16": {
      "lid": "OmniPth00117501fa000404 L102A", 
      "remotePort": "40", 
      "name": "OmniPth00117501fa000404 L102A"
     }, 
     "33": {
      "lid": "OmniPth00117501fa000404 L115A", 
      "remotePort": "33", 
      "name": "OmniPth00117501fa000404 L115A"
     }, 
     "32": {
      "lid": "OmniPth00117501fa000404 L117A", 
      "remotePort": "32", 
      "name": "OmniPth00117501fa000404 L117A"
     }, 
     "31": {
      "lid": "OmniPth00117501fa000404 L119A", 
      "remotePort": "31", 
      "name": "OmniPth00117501fa000404 L119A"
     }, 
     "30": {
      "lid": "OmniPth00117501fa000404 L109B", 
      "remotePort": "43", 
      "name": "OmniPth00117501fa000404 L109B"
     }, 
     "36": {
      "lid": "OmniPth00117501fa000404 L109A", 
      "remotePort": "36", 
      "name": "OmniPth00117501fa000404 L109A"
     }, 
     "35": {
      "lid": "OmniPth00117501fa000404 L111A", 
      "remotePort": "35", 
      "name": "OmniPth00117501fa000404 L111A"
     }, 
     "34": {
      "lid": "OmniPth00117501fa000404 L113A", 
      "remotePort": "34", 
      "name": "OmniPth00117501fa000404 L113A"
     }, 
     "19": {
      "lid": "OmniPth00117501fa000404 L106B", 
      "remotePort": "30", 
      "name": "OmniPth00117501fa000404 L106B"
     }
    }
   }, 
   "OmniPth00117501fa000404 S206B": {
    "lid": "OmniPth00117501fa000404 S206B", 
    "name": "OmniPth00117501fa000404 S206B", 
    "ports": {
     "42": {
      "lid": "OmniPth00117501fa000404 L101A", 
      "remotePort": "42", 
      "name": "OmniPth00117501fa000404 L101A"
     }, 
     "43": {
      "lid": "OmniPth00117501fa000404 L109B", 
      "remotePort": "30", 
      "name": "OmniPth00117501fa000404 L109B"
     }, 
     "25": {
      "lid": "OmniPth00117501fa000404 L111B", 
      "remotePort": "48", 
      "name": "OmniPth00117501fa000404 L111B"
     }, 
     "27": {
      "lid": "OmniPth00117501fa000404 L119B", 
      "remotePort": "46", 
      "name": "OmniPth00117501fa000404 L119B"
     }, 
     "21": {
      "lid": "OmniPth00117501fa000404 L106B", 
      "remotePort": "28", 
      "name": "OmniPth00117501fa000404 L106B"
     }, 
     "23": {
      "lid": "OmniPth00117501fa000404 L102B", 
      "remotePort": "26", 
      "name": "OmniPth00117501fa000404 L102B"
     }, 
     "47": {
      "lid": "OmniPth00117501fa000404 L101B", 
      "remotePort": "26", 
      "name": "OmniPth00117501fa000404 L101B"
     }, 
     "45": {
      "lid": "OmniPth00117501fa000404 L105B", 
      "remotePort": "28", 
      "name": "OmniPth00117501fa000404 L105B"
     }, 
     "28": {
      "lid": "OmniPth00117501fa000404 L117B", 
      "remotePort": "45", 
      "name": "OmniPth00117501fa000404 L117B"
     }, 
     "29": {
      "lid": "OmniPth00117501fa000404 L115B", 
      "remotePort": "44", 
      "name": "OmniPth00117501fa000404 L115B"
     }, 
     "40": {
      "lid": "OmniPth00117501fa000404 L105A", 
      "remotePort": "40", 
      "name": "OmniPth00117501fa000404 L105A"
     }, 
     "1": {
      "lid": "OmniPth00117501fa000404 L112B", 
      "remotePort": "48", 
      "name": "OmniPth00117501fa000404 L112B"
     }, 
     "5": {
      "lid": "OmniPth00117501fa000404 L116B", 
      "remotePort": "44", 
      "name": "OmniPth00117501fa000404 L116B"
     }, 
     "4": {
      "lid": "OmniPth00117501fa000404 L118B", 
      "remotePort": "45", 
      "name": "OmniPth00117501fa000404 L118B"
     }, 
     "6": {
      "lid": "OmniPth00117501fa000404 L114B", 
      "remotePort": "43", 
      "name": "OmniPth00117501fa000404 L114B"
     }, 
     "38": {
      "lid": "OmniPth00117501fa000404 L109A", 
      "remotePort": "38", 
      "name": "OmniPth00117501fa000404 L109A"
     }, 
     "11": {
      "lid": "OmniPth00117501fa000404 L116A", 
      "remotePort": "35", 
      "name": "OmniPth00117501fa000404 L116A"
     }, 
     "10": {
      "lid": "OmniPth00117501fa000404 L118A", 
      "remotePort": "34", 
      "name": "OmniPth00117501fa000404 L118A"
     }, 
     "13": {
      "lid": "OmniPth00117501fa000404 L112A", 
      "remotePort": "37", 
      "name": "OmniPth00117501fa000404 L112A"
     }, 
     "12": {
      "lid": "OmniPth00117501fa000404 L114A", 
      "remotePort": "36", 
      "name": "OmniPth00117501fa000404 L114A"
     }, 
     "14": {
      "lid": "OmniPth00117501fa000404 L110A", 
      "remotePort": "38", 
      "name": "OmniPth00117501fa000404 L110A"
     }, 
     "16": {
      "lid": "OmniPth00117501fa000404 L106A", 
      "remotePort": "40", 
      "name": "OmniPth00117501fa000404 L106A"
     }, 
     "33": {
      "lid": "OmniPth00117501fa000404 L119A", 
      "remotePort": "33", 
      "name": "OmniPth00117501fa000404 L119A"
     }, 
     "18": {
      "lid": "OmniPth00117501fa000404 L102A", 
      "remotePort": "42", 
      "name": "OmniPth00117501fa000404 L102A"
     }, 
     "30": {
      "lid": "OmniPth00117501fa000404 L113B", 
      "remotePort": "43", 
      "name": "OmniPth00117501fa000404 L113B"
     }, 
     "37": {
      "lid": "OmniPth00117501fa000404 L111A", 
      "remotePort": "37", 
      "name": "OmniPth00117501fa000404 L111A"
     }, 
     "36": {
      "lid": "OmniPth00117501fa000404 L113A", 
      "remotePort": "36", 
      "name": "OmniPth00117501fa000404 L113A"
     }, 
     "35": {
      "lid": "OmniPth00117501fa000404 L115A", 
      "remotePort": "35", 
      "name": "OmniPth00117501fa000404 L115A"
     }, 
     "34": {
      "lid": "OmniPth00117501fa000404 L117A", 
      "remotePort": "34", 
      "name": "OmniPth00117501fa000404 L117A"
     }, 
     "19": {
      "lid": "OmniPth00117501fa000404 L110B", 
      "remotePort": "30", 
      "name": "OmniPth00117501fa000404 L110B"
     }
    }
   }, 
   "OmniPth00117501fa000404 S206A": {
    "lid": "OmniPth00117501fa000404 S206A", 
    "name": "OmniPth00117501fa000404 S206A", 
    "ports": {
     "22": {
      "lid": "OmniPth00117501fa000404 L119A", 
      "remotePort": "27", 
      "name": "OmniPth00117501fa000404 L119A"
     }, 
     "24": {
      "lid": "OmniPth00117501fa000404 L111A", 
      "remotePort": "25", 
      "name": "OmniPth00117501fa000404 L111A"
     }, 
     "26": {
      "lid": "OmniPth00117501fa000404 L102A", 
      "remotePort": "47", 
      "name": "OmniPth00117501fa000404 L102A"
     }, 
     "20": {
      "lid": "OmniPth00117501fa000404 L115A", 
      "remotePort": "29", 
      "name": "OmniPth00117501fa000404 L115A"
     }, 
     "21": {
      "lid": "OmniPth00117501fa000404 L117A", 
      "remotePort": "28", 
      "name": "OmniPth00117501fa000404 L117A"
     }, 
     "48": {
      "lid": "OmniPth00117501fa000404 L112A", 
      "remotePort": "25", 
      "name": "OmniPth00117501fa000404 L112A"
     }, 
     "44": {
      "lid": "OmniPth00117501fa000404 L116A", 
      "remotePort": "29", 
      "name": "OmniPth00117501fa000404 L116A"
     }, 
     "45": {
      "lid": "OmniPth00117501fa000404 L118A", 
      "remotePort": "28", 
      "name": "OmniPth00117501fa000404 L118A"
     }, 
     "28": {
      "lid": "OmniPth00117501fa000404 L106A", 
      "remotePort": "45", 
      "name": "OmniPth00117501fa000404 L106A"
     }, 
     "43": {
      "lid": "OmniPth00117501fa000404 L114A", 
      "remotePort": "30", 
      "name": "OmniPth00117501fa000404 L114A"
     }, 
     "40": {
      "lid": "OmniPth00117501fa000404 L118B", 
      "remotePort": "40", 
      "name": "OmniPth00117501fa000404 L118B"
     }, 
     "2": {
      "lid": "OmniPth00117501fa000404 L101A", 
      "remotePort": "47", 
      "name": "OmniPth00117501fa000404 L101A"
     }, 
     "4": {
      "lid": "OmniPth00117501fa000404 L105A", 
      "remotePort": "45", 
      "name": "OmniPth00117501fa000404 L105A"
     }, 
     "6": {
      "lid": "OmniPth00117501fa000404 L109A", 
      "remotePort": "43", 
      "name": "OmniPth00117501fa000404 L109A"
     }, 
     "8": {
      "lid": "OmniPth00117501fa000404 L101B", 
      "remotePort": "32", 
      "name": "OmniPth00117501fa000404 L101B"
     }, 
     "39": {
      "lid": "OmniPth00117501fa000404 L116B", 
      "remotePort": "39", 
      "name": "OmniPth00117501fa000404 L116B"
     }, 
     "12": {
      "lid": "OmniPth00117501fa000404 L109B", 
      "remotePort": "36", 
      "name": "OmniPth00117501fa000404 L109B"
     }, 
     "10": {
      "lid": "OmniPth00117501fa000404 L105B", 
      "remotePort": "34", 
      "name": "OmniPth00117501fa000404 L105B"
     }, 
     "13": {
      "lid": "OmniPth00117501fa000404 L111B", 
      "remotePort": "37", 
      "name": "OmniPth00117501fa000404 L111B"
     }, 
     "38": {
      "lid": "OmniPth00117501fa000404 L114B", 
      "remotePort": "38", 
      "name": "OmniPth00117501fa000404 L114B"
     }, 
     "15": {
      "lid": "OmniPth00117501fa000404 L115B", 
      "remotePort": "39", 
      "name": "OmniPth00117501fa000404 L115B"
     }, 
     "14": {
      "lid": "OmniPth00117501fa000404 L113B", 
      "remotePort": "38", 
      "name": "OmniPth00117501fa000404 L113B"
     }, 
     "17": {
      "lid": "OmniPth00117501fa000404 L119B", 
      "remotePort": "41", 
      "name": "OmniPth00117501fa000404 L119B"
     }, 
     "16": {
      "lid": "OmniPth00117501fa000404 L117B", 
      "remotePort": "40", 
      "name": "OmniPth00117501fa000404 L117B"
     }, 
     "19": {
      "lid": "OmniPth00117501fa000404 L113A", 
      "remotePort": "30", 
      "name": "OmniPth00117501fa000404 L113A"
     }, 
     "32": {
      "lid": "OmniPth00117501fa000404 L102B", 
      "remotePort": "32", 
      "name": "OmniPth00117501fa000404 L102B"
     }, 
     "30": {
      "lid": "OmniPth00117501fa000404 L110A", 
      "remotePort": "43", 
      "name": "OmniPth00117501fa000404 L110A"
     }, 
     "37": {
      "lid": "OmniPth00117501fa000404 L112B", 
      "remotePort": "37", 
      "name": "OmniPth00117501fa000404 L112B"
     }, 
     "36": {
      "lid": "OmniPth00117501fa000404 L110B", 
      "remotePort": "36", 
      "name": "OmniPth00117501fa000404 L110B"
     }, 
     "34": {
      "lid": "OmniPth00117501fa000404 L106B", 
      "remotePort": "34", 
      "name": "OmniPth00117501fa000404 L106B"
     }
    }
   }, 
   "OmniPth00117501fa000404 L118B": {
    "lid": "OmniPth00117501fa000404 L118B", 
    "name": "OmniPth00117501fa000404 L118B", 
    "ports": {
     "25": {
      "lid": "OmniPth00117501fa000404 S203B", 
      "remotePort": "24", 
      "name": "OmniPth00117501fa000404 S203B"
     }, 
     "26": {
      "lid": "OmniPth00117501fa000404 S202B", 
      "remotePort": "23", 
      "name": "OmniPth00117501fa000404 S202B"
     }, 
     "27": {
      "lid": "OmniPth00117501fa000404 S201B", 
      "remotePort": "22", 
      "name": "OmniPth00117501fa000404 S201B"
     }, 
     "46": {
      "lid": "OmniPth00117501fa000404 S205B", 
      "remotePort": "3", 
      "name": "OmniPth00117501fa000404 S205B"
     }, 
     "47": {
      "lid": "OmniPth00117501fa000404 S204B", 
      "remotePort": "2", 
      "name": "OmniPth00117501fa000404 S204B"
     }, 
     "44": {
      "lid": "OmniPth00117501fa000404 S207B", 
      "remotePort": "5", 
      "name": "OmniPth00117501fa000404 S207B"
     }, 
     "45": {
      "lid": "OmniPth00117501fa000404 S206B", 
      "remotePort": "4", 
      "name": "OmniPth00117501fa000404 S206B"
     }, 
     "42": {
      "lid": "OmniPth00117501fa000404 S204A", 
      "remotePort": "42", 
      "name": "OmniPth00117501fa000404 S204A"
     }, 
     "43": {
      "lid": "OmniPth00117501fa000404 S208B", 
      "remotePort": "6", 
      "name": "OmniPth00117501fa000404 S208B"
     }, 
     "40": {
      "lid": "OmniPth00117501fa000404 S206A", 
      "remotePort": "40", 
      "name": "OmniPth00117501fa000404 S206A"
     }, 
     "41": {
      "lid": "OmniPth00117501fa000404 S205A", 
      "remotePort": "41", 
      "name": "OmniPth00117501fa000404 S205A"
     }, 
     "3": {
      "lid": "node009 hfi1_0", 
      "remotePort": "1", 
      "name": "node009 hfi1_0"
     }, 
     "2": {
      "lid": "node010 hfi1_0", 
      "remotePort": "1", 
      "name": "node010 hfi1_0"
     }, 
     "5": {
      "lid": "node121 hfi1_0", 
      "remotePort": "1", 
      "name": "node121 hfi1_0"
     }, 
     "7": {
      "lid": "node120 hfi1_0", 
      "remotePort": "1", 
      "name": "node120 hfi1_0"
     }, 
     "6": {
      "lid": "node022 hfi1_0", 
      "remotePort": "1", 
      "name": "node022 hfi1_0"
     }, 
     "9": {
      "lid": "node020 hfi1_0", 
      "remotePort": "1", 
      "name": "node020 hfi1_0"
     }, 
     "8": {
      "lid": "node117 hfi1_0", 
      "remotePort": "1", 
      "name": "node117 hfi1_0"
     }, 
     "39": {
      "lid": "OmniPth00117501fa000404 S207A", 
      "remotePort": "39", 
      "name": "OmniPth00117501fa000404 S207A"
     }, 
     "10": {
      "lid": "node011 hfi1_0", 
      "remotePort": "1", 
      "name": "node011 hfi1_0"
     }, 
     "13": {
      "lid": "node115 hfi1_0", 
      "remotePort": "1", 
      "name": "node115 hfi1_0"
     }, 
     "38": {
      "lid": "OmniPth00117501fa000404 S208A", 
      "remotePort": "38", 
      "name": "OmniPth00117501fa000404 S208A"
     }, 
     "33": {
      "lid": "OmniPth00117501fa000404 S201A", 
      "remotePort": "33", 
      "name": "OmniPth00117501fa000404 S201A"
     }, 
     "32": {
      "lid": "OmniPth00117501fa000404 S202A", 
      "remotePort": "32", 
      "name": "OmniPth00117501fa000404 S202A"
     }, 
     "31": {
      "lid": "OmniPth00117501fa000404 S203A", 
      "remotePort": "31", 
      "name": "OmniPth00117501fa000404 S203A"
     }
    }
   }, 
   "OmniPth00117501fa000404 L118A": {
    "lid": "OmniPth00117501fa000404 L118A", 
    "name": "OmniPth00117501fa000404 L118A", 
    "ports": {
     "11": {
      "lid": "node021 hfi1_0", 
      "remotePort": "1", 
      "name": "node021 hfi1_0"
     }, 
     "26": {
      "lid": "OmniPth00117501fa000404 S204A", 
      "remotePort": "47", 
      "name": "OmniPth00117501fa000404 S204A"
     }, 
     "27": {
      "lid": "OmniPth00117501fa000404 S205A", 
      "remotePort": "46", 
      "name": "OmniPth00117501fa000404 S205A"
     }, 
     "15": {
      "lid": "node012 hfi1_0", 
      "remotePort": "1", 
      "name": "node012 hfi1_0"
     }, 
     "36": {
      "lid": "OmniPth00117501fa000404 S208B", 
      "remotePort": "12", 
      "name": "OmniPth00117501fa000404 S208B"
     }, 
     "48": {
      "lid": "OmniPth00117501fa000404 S203A", 
      "remotePort": "25", 
      "name": "OmniPth00117501fa000404 S203A"
     }, 
     "33": {
      "lid": "OmniPth00117501fa000404 S205B", 
      "remotePort": "9", 
      "name": "OmniPth00117501fa000404 S205B"
     }, 
     "32": {
      "lid": "OmniPth00117501fa000404 S204B", 
      "remotePort": "8", 
      "name": "OmniPth00117501fa000404 S204B"
     }, 
     "31": {
      "lid": "OmniPth00117501fa000404 S203B", 
      "remotePort": "7", 
      "name": "OmniPth00117501fa000404 S203B"
     }, 
     "30": {
      "lid": "OmniPth00117501fa000404 S208A", 
      "remotePort": "43", 
      "name": "OmniPth00117501fa000404 S208A"
     }, 
     "28": {
      "lid": "OmniPth00117501fa000404 S206A", 
      "remotePort": "45", 
      "name": "OmniPth00117501fa000404 S206A"
     }, 
     "29": {
      "lid": "OmniPth00117501fa000404 S207A", 
      "remotePort": "44", 
      "name": "OmniPth00117501fa000404 S207A"
     }, 
     "35": {
      "lid": "OmniPth00117501fa000404 S207B", 
      "remotePort": "11", 
      "name": "OmniPth00117501fa000404 S207B"
     }, 
     "34": {
      "lid": "OmniPth00117501fa000404 S206B", 
      "remotePort": "10", 
      "name": "OmniPth00117501fa000404 S206B"
     }, 
     "46": {
      "lid": "OmniPth00117501fa000404 S201A", 
      "remotePort": "27", 
      "name": "OmniPth00117501fa000404 S201A"
     }, 
     "2": {
      "lid": "node116 hfi1_0", 
      "remotePort": "1", 
      "name": "node116 hfi1_0"
     }, 
     "7": {
      "lid": "node008 hfi1_0", 
      "remotePort": "1", 
      "name": "node008 hfi1_0"
     }, 
     "47": {
      "lid": "OmniPth00117501fa000404 S202A", 
      "remotePort": "26", 
      "name": "OmniPth00117501fa000404 S202A"
     }, 
     "41": {
      "lid": "OmniPth00117501fa000404 S201B", 
      "remotePort": "17", 
      "name": "OmniPth00117501fa000404 S201B"
     }, 
     "42": {
      "lid": "OmniPth00117501fa000404 S202B", 
      "remotePort": "18", 
      "name": "OmniPth00117501fa000404 S202B"
     }
    }
   }, 
   "OmniPth00117501fa000404 S203A": {
    "lid": "OmniPth00117501fa000404 S203A", 
    "name": "OmniPth00117501fa000404 S203A", 
    "ports": {
     "24": {
      "lid": "OmniPth00117501fa000404 L105A", 
      "remotePort": "25", 
      "name": "OmniPth00117501fa000404 L105A"
     }, 
     "25": {
      "lid": "OmniPth00117501fa000404 L118A", 
      "remotePort": "48", 
      "name": "OmniPth00117501fa000404 L118A"
     }, 
     "20": {
      "lid": "OmniPth00117501fa000404 L109A", 
      "remotePort": "29", 
      "name": "OmniPth00117501fa000404 L109A"
     }, 
     "21": {
      "lid": "OmniPth00117501fa000404 L111A", 
      "remotePort": "28", 
      "name": "OmniPth00117501fa000404 L111A"
     }, 
     "48": {
      "lid": "OmniPth00117501fa000404 L106A", 
      "remotePort": "25", 
      "name": "OmniPth00117501fa000404 L106A"
     }, 
     "23": {
      "lid": "OmniPth00117501fa000404 L115A", 
      "remotePort": "26", 
      "name": "OmniPth00117501fa000404 L115A"
     }, 
     "46": {
      "lid": "OmniPth00117501fa000404 L114A", 
      "remotePort": "27", 
      "name": "OmniPth00117501fa000404 L114A"
     }, 
     "47": {
      "lid": "OmniPth00117501fa000404 L116A", 
      "remotePort": "26", 
      "name": "OmniPth00117501fa000404 L116A"
     }, 
     "44": {
      "lid": "OmniPth00117501fa000404 L110A", 
      "remotePort": "29", 
      "name": "OmniPth00117501fa000404 L110A"
     }, 
     "45": {
      "lid": "OmniPth00117501fa000404 L112A", 
      "remotePort": "28", 
      "name": "OmniPth00117501fa000404 L112A"
     }, 
     "42": {
      "lid": "OmniPth00117501fa000404 L116B", 
      "remotePort": "42", 
      "name": "OmniPth00117501fa000404 L116B"
     }, 
     "29": {
      "lid": "OmniPth00117501fa000404 L102A", 
      "remotePort": "44", 
      "name": "OmniPth00117501fa000404 L102A"
     }, 
     "40": {
      "lid": "OmniPth00117501fa000404 L112B", 
      "remotePort": "40", 
      "name": "OmniPth00117501fa000404 L112B"
     }, 
     "41": {
      "lid": "OmniPth00117501fa000404 L114B", 
      "remotePort": "41", 
      "name": "OmniPth00117501fa000404 L114B"
     }, 
     "1": {
      "lid": "OmniPth00117501fa000404 L117A", 
      "remotePort": "48", 
      "name": "OmniPth00117501fa000404 L117A"
     }, 
     "2": {
      "lid": "OmniPth00117501fa000404 L119A", 
      "remotePort": "47", 
      "name": "OmniPth00117501fa000404 L119A"
     }, 
     "5": {
      "lid": "OmniPth00117501fa000404 L101A", 
      "remotePort": "44", 
      "name": "OmniPth00117501fa000404 L101A"
     }, 
     "7": {
      "lid": "OmniPth00117501fa000404 L117B", 
      "remotePort": "31", 
      "name": "OmniPth00117501fa000404 L117B"
     }, 
     "8": {
      "lid": "OmniPth00117501fa000404 L119B", 
      "remotePort": "32", 
      "name": "OmniPth00117501fa000404 L119B"
     }, 
     "13": {
      "lid": "OmniPth00117501fa000404 L105B", 
      "remotePort": "37", 
      "name": "OmniPth00117501fa000404 L105B"
     }, 
     "11": {
      "lid": "OmniPth00117501fa000404 L101B", 
      "remotePort": "35", 
      "name": "OmniPth00117501fa000404 L101B"
     }, 
     "39": {
      "lid": "OmniPth00117501fa000404 L110B", 
      "remotePort": "39", 
      "name": "OmniPth00117501fa000404 L110B"
     }, 
     "15": {
      "lid": "OmniPth00117501fa000404 L109B", 
      "remotePort": "39", 
      "name": "OmniPth00117501fa000404 L109B"
     }, 
     "22": {
      "lid": "OmniPth00117501fa000404 L113A", 
      "remotePort": "27", 
      "name": "OmniPth00117501fa000404 L113A"
     }, 
     "17": {
      "lid": "OmniPth00117501fa000404 L113B", 
      "remotePort": "41", 
      "name": "OmniPth00117501fa000404 L113B"
     }, 
     "16": {
      "lid": "OmniPth00117501fa000404 L111B", 
      "remotePort": "40", 
      "name": "OmniPth00117501fa000404 L111B"
     }, 
     "18": {
      "lid": "OmniPth00117501fa000404 L115B", 
      "remotePort": "42", 
      "name": "OmniPth00117501fa000404 L115B"
     }, 
     "31": {
      "lid": "OmniPth00117501fa000404 L118B", 
      "remotePort": "31", 
      "name": "OmniPth00117501fa000404 L118B"
     }, 
     "37": {
      "lid": "OmniPth00117501fa000404 L106B", 
      "remotePort": "37", 
      "name": "OmniPth00117501fa000404 L106B"
     }, 
     "35": {
      "lid": "OmniPth00117501fa000404 L102B", 
      "remotePort": "35", 
      "name": "OmniPth00117501fa000404 L102B"
     }
    }
   }, 
   "OmniPth00117501fa000404 S202B": {
    "lid": "OmniPth00117501fa000404 S202B", 
    "name": "OmniPth00117501fa000404 S202B", 
    "ports": {
     "42": {
      "lid": "OmniPth00117501fa000404 L117A", 
      "remotePort": "42", 
      "name": "OmniPth00117501fa000404 L117A"
     }, 
     "24": {
      "lid": "OmniPth00117501fa000404 L116B", 
      "remotePort": "25", 
      "name": "OmniPth00117501fa000404 L116B"
     }, 
     "26": {
      "lid": "OmniPth00117501fa000404 L113B", 
      "remotePort": "47", 
      "name": "OmniPth00117501fa000404 L113B"
     }, 
     "27": {
      "lid": "OmniPth00117501fa000404 L111B", 
      "remotePort": "46", 
      "name": "OmniPth00117501fa000404 L111B"
     }, 
     "48": {
      "lid": "OmniPth00117501fa000404 L115B", 
      "remotePort": "25", 
      "name": "OmniPth00117501fa000404 L115B"
     }, 
     "23": {
      "lid": "OmniPth00117501fa000404 L118B", 
      "remotePort": "26", 
      "name": "OmniPth00117501fa000404 L118B"
     }, 
     "46": {
      "lid": "OmniPth00117501fa000404 L119B", 
      "remotePort": "27", 
      "name": "OmniPth00117501fa000404 L119B"
     }, 
     "47": {
      "lid": "OmniPth00117501fa000404 L117B", 
      "remotePort": "26", 
      "name": "OmniPth00117501fa000404 L117B"
     }, 
     "28": {
      "lid": "OmniPth00117501fa000404 L109B", 
      "remotePort": "45", 
      "name": "OmniPth00117501fa000404 L109B"
     }, 
     "43": {
      "lid": "OmniPth00117501fa000404 L101B", 
      "remotePort": "30", 
      "name": "OmniPth00117501fa000404 L101B"
     }, 
     "41": {
      "lid": "OmniPth00117501fa000404 L119A", 
      "remotePort": "41", 
      "name": "OmniPth00117501fa000404 L119A"
     }, 
     "3": {
      "lid": "OmniPth00117501fa000404 L112B", 
      "remotePort": "46", 
      "name": "OmniPth00117501fa000404 L112B"
     }, 
     "2": {
      "lid": "OmniPth00117501fa000404 L114B", 
      "remotePort": "47", 
      "name": "OmniPth00117501fa000404 L114B"
     }, 
     "4": {
      "lid": "OmniPth00117501fa000404 L110B", 
      "remotePort": "45", 
      "name": "OmniPth00117501fa000404 L110B"
     }, 
     "7": {
      "lid": "OmniPth00117501fa000404 L116A", 
      "remotePort": "31", 
      "name": "OmniPth00117501fa000404 L116A"
     }, 
     "6": {
      "lid": "OmniPth00117501fa000404 L106B", 
      "remotePort": "43", 
      "name": "OmniPth00117501fa000404 L106B"
     }, 
     "9": {
      "lid": "OmniPth00117501fa000404 L112A", 
      "remotePort": "33", 
      "name": "OmniPth00117501fa000404 L112A"
     }, 
     "8": {
      "lid": "OmniPth00117501fa000404 L114A", 
      "remotePort": "32", 
      "name": "OmniPth00117501fa000404 L114A"
     }, 
     "38": {
      "lid": "OmniPth00117501fa000404 L101A", 
      "remotePort": "38", 
      "name": "OmniPth00117501fa000404 L101A"
     }, 
     "10": {
      "lid": "OmniPth00117501fa000404 L110A", 
      "remotePort": "34", 
      "name": "OmniPth00117501fa000404 L110A"
     }, 
     "12": {
      "lid": "OmniPth00117501fa000404 L106A", 
      "remotePort": "36", 
      "name": "OmniPth00117501fa000404 L106A"
     }, 
     "14": {
      "lid": "OmniPth00117501fa000404 L102A", 
      "remotePort": "38", 
      "name": "OmniPth00117501fa000404 L102A"
     }, 
     "19": {
      "lid": "OmniPth00117501fa000404 L102B", 
      "remotePort": "30", 
      "name": "OmniPth00117501fa000404 L102B"
     }, 
     "32": {
      "lid": "OmniPth00117501fa000404 L113A", 
      "remotePort": "32", 
      "name": "OmniPth00117501fa000404 L113A"
     }, 
     "31": {
      "lid": "OmniPth00117501fa000404 L115A", 
      "remotePort": "31", 
      "name": "OmniPth00117501fa000404 L115A"
     }, 
     "30": {
      "lid": "OmniPth00117501fa000404 L105B", 
      "remotePort": "43", 
      "name": "OmniPth00117501fa000404 L105B"
     }, 
     "36": {
      "lid": "OmniPth00117501fa000404 L105A", 
      "remotePort": "36", 
      "name": "OmniPth00117501fa000404 L105A"
     }, 
     "34": {
      "lid": "OmniPth00117501fa000404 L109A", 
      "remotePort": "34", 
      "name": "OmniPth00117501fa000404 L109A"
     }, 
     "33": {
      "lid": "OmniPth00117501fa000404 L111A", 
      "remotePort": "33", 
      "name": "OmniPth00117501fa000404 L111A"
     }, 
     "18": {
      "lid": "OmniPth00117501fa000404 L118A", 
      "remotePort": "42", 
      "name": "OmniPth00117501fa000404 L118A"
     }
    }
   }, 
   "OmniPth00117501fa000404 S202A": {
    "lid": "OmniPth00117501fa000404 S202A", 
    "name": "OmniPth00117501fa000404 S202A", 
    "ports": {
     "25": {
      "lid": "OmniPth00117501fa000404 L116A", 
      "remotePort": "48", 
      "name": "OmniPth00117501fa000404 L116A"
     }, 
     "26": {
      "lid": "OmniPth00117501fa000404 L118A", 
      "remotePort": "47", 
      "name": "OmniPth00117501fa000404 L118A"
     }, 
     "21": {
      "lid": "OmniPth00117501fa000404 L109A", 
      "remotePort": "28", 
      "name": "OmniPth00117501fa000404 L109A"
     }, 
     "22": {
      "lid": "OmniPth00117501fa000404 L111A", 
      "remotePort": "27", 
      "name": "OmniPth00117501fa000404 L111A"
     }, 
     "23": {
      "lid": "OmniPth00117501fa000404 L113A", 
      "remotePort": "26", 
      "name": "OmniPth00117501fa000404 L113A"
     }, 
     "46": {
      "lid": "OmniPth00117501fa000404 L112A", 
      "remotePort": "27", 
      "name": "OmniPth00117501fa000404 L112A"
     }, 
     "47": {
      "lid": "OmniPth00117501fa000404 L114A", 
      "remotePort": "26", 
      "name": "OmniPth00117501fa000404 L114A"
     }, 
     "45": {
      "lid": "OmniPth00117501fa000404 L110A", 
      "remotePort": "28", 
      "name": "OmniPth00117501fa000404 L110A"
     }, 
     "42": {
      "lid": "OmniPth00117501fa000404 L114B", 
      "remotePort": "42", 
      "name": "OmniPth00117501fa000404 L114B"
     }, 
     "43": {
      "lid": "OmniPth00117501fa000404 L106A", 
      "remotePort": "30", 
      "name": "OmniPth00117501fa000404 L106A"
     }, 
     "40": {
      "lid": "OmniPth00117501fa000404 L110B", 
      "remotePort": "40", 
      "name": "OmniPth00117501fa000404 L110B"
     }, 
     "41": {
      "lid": "OmniPth00117501fa000404 L112B", 
      "remotePort": "41", 
      "name": "OmniPth00117501fa000404 L112B"
     }, 
     "1": {
      "lid": "OmniPth00117501fa000404 L115A", 
      "remotePort": "48", 
      "name": "OmniPth00117501fa000404 L115A"
     }, 
     "3": {
      "lid": "OmniPth00117501fa000404 L119A", 
      "remotePort": "46", 
      "name": "OmniPth00117501fa000404 L119A"
     }, 
     "2": {
      "lid": "OmniPth00117501fa000404 L117A", 
      "remotePort": "47", 
      "name": "OmniPth00117501fa000404 L117A"
     }, 
     "7": {
      "lid": "OmniPth00117501fa000404 L115B", 
      "remotePort": "31", 
      "name": "OmniPth00117501fa000404 L115B"
     }, 
     "6": {
      "lid": "OmniPth00117501fa000404 L101A", 
      "remotePort": "43", 
      "name": "OmniPth00117501fa000404 L101A"
     }, 
     "9": {
      "lid": "OmniPth00117501fa000404 L119B", 
      "remotePort": "33", 
      "name": "OmniPth00117501fa000404 L119B"
     }, 
     "8": {
      "lid": "OmniPth00117501fa000404 L117B", 
      "remotePort": "32", 
      "name": "OmniPth00117501fa000404 L117B"
     }, 
     "38": {
      "lid": "OmniPth00117501fa000404 L106B", 
      "remotePort": "38", 
      "name": "OmniPth00117501fa000404 L106B"
     }, 
     "12": {
      "lid": "OmniPth00117501fa000404 L101B", 
      "remotePort": "36", 
      "name": "OmniPth00117501fa000404 L101B"
     }, 
     "14": {
      "lid": "OmniPth00117501fa000404 L105B", 
      "remotePort": "38", 
      "name": "OmniPth00117501fa000404 L105B"
     }, 
     "17": {
      "lid": "OmniPth00117501fa000404 L111B", 
      "remotePort": "41", 
      "name": "OmniPth00117501fa000404 L111B"
     }, 
     "16": {
      "lid": "OmniPth00117501fa000404 L109B", 
      "remotePort": "40", 
      "name": "OmniPth00117501fa000404 L109B"
     }, 
     "19": {
      "lid": "OmniPth00117501fa000404 L105A", 
      "remotePort": "30", 
      "name": "OmniPth00117501fa000404 L105A"
     }, 
     "18": {
      "lid": "OmniPth00117501fa000404 L113B", 
      "remotePort": "42", 
      "name": "OmniPth00117501fa000404 L113B"
     }, 
     "31": {
      "lid": "OmniPth00117501fa000404 L116B", 
      "remotePort": "31", 
      "name": "OmniPth00117501fa000404 L116B"
     }, 
     "30": {
      "lid": "OmniPth00117501fa000404 L102A", 
      "remotePort": "43", 
      "name": "OmniPth00117501fa000404 L102A"
     }, 
     "36": {
      "lid": "OmniPth00117501fa000404 L102B", 
      "remotePort": "36", 
      "name": "OmniPth00117501fa000404 L102B"
     }, 
     "32": {
      "lid": "OmniPth00117501fa000404 L118B", 
      "remotePort": "32", 
      "name": "OmniPth00117501fa000404 L118B"
     }
    }
   }, 
   "OmniPth00117501fa000404 L106B": {
    "lid": "OmniPth00117501fa000404 L106B", 
    "name": "OmniPth00117501fa000404 L106B", 
    "ports": {
     "43": {
      "lid": "OmniPth00117501fa000404 S202B", 
      "remotePort": "6", 
      "name": "OmniPth00117501fa000404 S202B"
     }, 
     "26": {
      "lid": "OmniPth00117501fa000404 S208B", 
      "remotePort": "23", 
      "name": "OmniPth00117501fa000404 S208B"
     }, 
     "27": {
      "lid": "OmniPth00117501fa000404 S207B", 
      "remotePort": "22", 
      "name": "OmniPth00117501fa000404 S207B"
     }, 
     "48": {
      "lid": "OmniPth00117501fa000404 S203B", 
      "remotePort": "1", 
      "name": "OmniPth00117501fa000404 S203B"
     }, 
     "44": {
      "lid": "OmniPth00117501fa000404 S201B", 
      "remotePort": "5", 
      "name": "OmniPth00117501fa000404 S201B"
     }, 
     "28": {
      "lid": "OmniPth00117501fa000404 S206B", 
      "remotePort": "21", 
      "name": "OmniPth00117501fa000404 S206B"
     }, 
     "29": {
      "lid": "OmniPth00117501fa000404 S205B", 
      "remotePort": "20", 
      "name": "OmniPth00117501fa000404 S205B"
     }, 
     "1": {
      "lid": "node178 hfi1_0", 
      "remotePort": "1", 
      "name": "node178 hfi1_0"
     }, 
     "3": {
      "lid": "node186 hfi1_0", 
      "remotePort": "1", 
      "name": "node186 hfi1_0"
     }, 
     "2": {
      "lid": "node188 hfi1_0", 
      "remotePort": "1", 
      "name": "node188 hfi1_0"
     }, 
     "5": {
      "lid": "node189 hfi1_0", 
      "remotePort": "1", 
      "name": "node189 hfi1_0"
     }, 
     "4": {
      "lid": "node185 hfi1_0", 
      "remotePort": "1", 
      "name": "node185 hfi1_0"
     }, 
     "7": {
      "lid": "node192 hfi1_0", 
      "remotePort": "1", 
      "name": "node192 hfi1_0"
     }, 
     "6": {
      "lid": "node190 hfi1_0", 
      "remotePort": "1", 
      "name": "node190 hfi1_0"
     }, 
     "9": {
      "lid": "node181 hfi1_0", 
      "remotePort": "1", 
      "name": "node181 hfi1_0"
     }, 
     "8": {
      "lid": "node191 hfi1_0", 
      "remotePort": "1", 
      "name": "node191 hfi1_0"
     }, 
     "39": {
      "lid": "OmniPth00117501fa000404 S201A", 
      "remotePort": "39", 
      "name": "OmniPth00117501fa000404 S201A"
     }, 
     "38": {
      "lid": "OmniPth00117501fa000404 S202A", 
      "remotePort": "38", 
      "name": "OmniPth00117501fa000404 S202A"
     }, 
     "11": {
      "lid": "node182 hfi1_0", 
      "remotePort": "1", 
      "name": "node182 hfi1_0"
     }, 
     "10": {
      "lid": "node177 hfi1_0", 
      "remotePort": "1", 
      "name": "node177 hfi1_0"
     }, 
     "13": {
      "lid": "node173 hfi1_0", 
      "remotePort": "1", 
      "name": "node173 hfi1_0"
     }, 
     "12": {
      "lid": "node179 hfi1_0", 
      "remotePort": "1", 
      "name": "node179 hfi1_0"
     }, 
     "14": {
      "lid": "node174 hfi1_0", 
      "remotePort": "1", 
      "name": "node174 hfi1_0"
     }, 
     "16": {
      "lid": "node187 hfi1_0", 
      "remotePort": "1", 
      "name": "node187 hfi1_0"
     }, 
     "33": {
      "lid": "OmniPth00117501fa000404 S207A", 
      "remotePort": "33", 
      "name": "OmniPth00117501fa000404 S207A"
     }, 
     "32": {
      "lid": "OmniPth00117501fa000404 S208A", 
      "remotePort": "32", 
      "name": "OmniPth00117501fa000404 S208A"
     }, 
     "30": {
      "lid": "OmniPth00117501fa000404 S204B", 
      "remotePort": "19", 
      "name": "OmniPth00117501fa000404 S204B"
     }, 
     "37": {
      "lid": "OmniPth00117501fa000404 S203A", 
      "remotePort": "37", 
      "name": "OmniPth00117501fa000404 S203A"
     }, 
     "36": {
      "lid": "OmniPth00117501fa000404 S204A", 
      "remotePort": "36", 
      "name": "OmniPth00117501fa000404 S204A"
     }, 
     "35": {
      "lid": "OmniPth00117501fa000404 S205A", 
      "remotePort": "35", 
      "name": "OmniPth00117501fa000404 S205A"
     }, 
     "34": {
      "lid": "OmniPth00117501fa000404 S206A", 
      "remotePort": "34", 
      "name": "OmniPth00117501fa000404 S206A"
     }
    }
   }, 
   "OmniPth00117501fa000404 S203B": {
    "lid": "OmniPth00117501fa000404 S203B", 
    "name": "OmniPth00117501fa000404 S203B", 
    "ports": {
     "28": {
      "lid": "OmniPth00117501fa000404 L111B", 
      "remotePort": "45", 
      "name": "OmniPth00117501fa000404 L111B"
     }, 
     "24": {
      "lid": "OmniPth00117501fa000404 L118B", 
      "remotePort": "25", 
      "name": "OmniPth00117501fa000404 L118B"
     }, 
     "25": {
      "lid": "OmniPth00117501fa000404 L105B", 
      "remotePort": "48", 
      "name": "OmniPth00117501fa000404 L105B"
     }, 
     "26": {
      "lid": "OmniPth00117501fa000404 L115B", 
      "remotePort": "47", 
      "name": "OmniPth00117501fa000404 L115B"
     }, 
     "27": {
      "lid": "OmniPth00117501fa000404 L113B", 
      "remotePort": "46", 
      "name": "OmniPth00117501fa000404 L113B"
     }, 
     "20": {
      "lid": "OmniPth00117501fa000404 L102B", 
      "remotePort": "29", 
      "name": "OmniPth00117501fa000404 L102B"
     }, 
     "48": {
      "lid": "OmniPth00117501fa000404 L117B", 
      "remotePort": "25", 
      "name": "OmniPth00117501fa000404 L117B"
     }, 
     "47": {
      "lid": "OmniPth00117501fa000404 L119B", 
      "remotePort": "26", 
      "name": "OmniPth00117501fa000404 L119B"
     }, 
     "44": {
      "lid": "OmniPth00117501fa000404 L101B", 
      "remotePort": "29", 
      "name": "OmniPth00117501fa000404 L101B"
     }, 
     "42": {
      "lid": "OmniPth00117501fa000404 L119A", 
      "remotePort": "42", 
      "name": "OmniPth00117501fa000404 L119A"
     }, 
     "29": {
      "lid": "OmniPth00117501fa000404 L109B", 
      "remotePort": "44", 
      "name": "OmniPth00117501fa000404 L109B"
     }, 
     "1": {
      "lid": "OmniPth00117501fa000404 L106B", 
      "remotePort": "48", 
      "name": "OmniPth00117501fa000404 L106B"
     }, 
     "3": {
      "lid": "OmniPth00117501fa000404 L114B", 
      "remotePort": "46", 
      "name": "OmniPth00117501fa000404 L114B"
     }, 
     "2": {
      "lid": "OmniPth00117501fa000404 L116B", 
      "remotePort": "47", 
      "name": "OmniPth00117501fa000404 L116B"
     }, 
     "5": {
      "lid": "OmniPth00117501fa000404 L110B", 
      "remotePort": "44", 
      "name": "OmniPth00117501fa000404 L110B"
     }, 
     "4": {
      "lid": "OmniPth00117501fa000404 L112B", 
      "remotePort": "45", 
      "name": "OmniPth00117501fa000404 L112B"
     }, 
     "7": {
      "lid": "OmniPth00117501fa000404 L118A", 
      "remotePort": "31", 
      "name": "OmniPth00117501fa000404 L118A"
     }, 
     "9": {
      "lid": "OmniPth00117501fa000404 L114A", 
      "remotePort": "33", 
      "name": "OmniPth00117501fa000404 L114A"
     }, 
     "8": {
      "lid": "OmniPth00117501fa000404 L116A", 
      "remotePort": "32", 
      "name": "OmniPth00117501fa000404 L116A"
     }, 
     "39": {
      "lid": "OmniPth00117501fa000404 L101A", 
      "remotePort": "39", 
      "name": "OmniPth00117501fa000404 L101A"
     }, 
     "11": {
      "lid": "OmniPth00117501fa000404 L110A", 
      "remotePort": "35", 
      "name": "OmniPth00117501fa000404 L110A"
     }, 
     "10": {
      "lid": "OmniPth00117501fa000404 L112A", 
      "remotePort": "34", 
      "name": "OmniPth00117501fa000404 L112A"
     }, 
     "13": {
      "lid": "OmniPth00117501fa000404 L106A", 
      "remotePort": "37", 
      "name": "OmniPth00117501fa000404 L106A"
     }, 
     "15": {
      "lid": "OmniPth00117501fa000404 L102A", 
      "remotePort": "39", 
      "name": "OmniPth00117501fa000404 L102A"
     }, 
     "33": {
      "lid": "OmniPth00117501fa000404 L113A", 
      "remotePort": "33", 
      "name": "OmniPth00117501fa000404 L113A"
     }, 
     "32": {
      "lid": "OmniPth00117501fa000404 L115A", 
      "remotePort": "32", 
      "name": "OmniPth00117501fa000404 L115A"
     }, 
     "31": {
      "lid": "OmniPth00117501fa000404 L117A", 
      "remotePort": "31", 
      "name": "OmniPth00117501fa000404 L117A"
     }, 
     "37": {
      "lid": "OmniPth00117501fa000404 L105A", 
      "remotePort": "37", 
      "name": "OmniPth00117501fa000404 L105A"
     }, 
     "35": {
      "lid": "OmniPth00117501fa000404 L109A", 
      "remotePort": "35", 
      "name": "OmniPth00117501fa000404 L109A"
     }, 
     "34": {
      "lid": "OmniPth00117501fa000404 L111A", 
      "remotePort": "34", 
      "name": "OmniPth00117501fa000404 L111A"
     }
    }
   }, 
   "OmniPth00117501fa000404 L112A": {
    "lid": "OmniPth00117501fa000404 L112A", 
    "name": "OmniPth00117501fa000404 L112A", 
    "ports": {
     "43": {
      "lid": "OmniPth00117501fa000404 S207A", 
      "remotePort": "30", 
      "name": "OmniPth00117501fa000404 S207A"
     }, 
     "25": {
      "lid": "OmniPth00117501fa000404 S206A", 
      "remotePort": "48", 
      "name": "OmniPth00117501fa000404 S206A"
     }, 
     "26": {
      "lid": "OmniPth00117501fa000404 S201A", 
      "remotePort": "47", 
      "name": "OmniPth00117501fa000404 S201A"
     }, 
     "27": {
      "lid": "OmniPth00117501fa000404 S202A", 
      "remotePort": "46", 
      "name": "OmniPth00117501fa000404 S202A"
     }, 
     "44": {
      "lid": "OmniPth00117501fa000404 S208A", 
      "remotePort": "29", 
      "name": "OmniPth00117501fa000404 S208A"
     }, 
     "28": {
      "lid": "OmniPth00117501fa000404 S203A", 
      "remotePort": "45", 
      "name": "OmniPth00117501fa000404 S203A"
     }, 
     "29": {
      "lid": "OmniPth00117501fa000404 S204A", 
      "remotePort": "44", 
      "name": "OmniPth00117501fa000404 S204A"
     }, 
     "1": {
      "lid": "node026 hfi1_0", 
      "remotePort": "1", 
      "name": "node026 hfi1_0"
     }, 
     "3": {
      "lid": "node113 hfi1_0", 
      "remotePort": "1", 
      "name": "node113 hfi1_0"
     }, 
     "2": {
      "lid": "node028 hfi1_0", 
      "remotePort": "1", 
      "name": "node028 hfi1_0"
     }, 
     "5": {
      "lid": "node025 hfi1_0", 
      "remotePort": "1", 
      "name": "node025 hfi1_0"
     }, 
     "4": {
      "lid": "node029 hfi1_0", 
      "remotePort": "1", 
      "name": "node029 hfi1_0"
     }, 
     "7": {
      "lid": "node128 hfi1_0", 
      "remotePort": "1", 
      "name": "node128 hfi1_0"
     }, 
     "6": {
      "lid": "node035 hfi1_0", 
      "remotePort": "1", 
      "name": "node035 hfi1_0"
     }, 
     "8": {
      "lid": "node014 hfi1_0", 
      "remotePort": "1", 
      "name": "node014 hfi1_0"
     }, 
     "39": {
      "lid": "OmniPth00117501fa000404 S208B", 
      "remotePort": "15", 
      "name": "OmniPth00117501fa000404 S208B"
     }, 
     "38": {
      "lid": "OmniPth00117501fa000404 S207B", 
      "remotePort": "14", 
      "name": "OmniPth00117501fa000404 S207B"
     }, 
     "11": {
      "lid": "node114 hfi1_0", 
      "remotePort": "1", 
      "name": "node114 hfi1_0"
     }, 
     "13": {
      "lid": "node112 hfi1_0", 
      "remotePort": "1", 
      "name": "node112 hfi1_0"
     }, 
     "12": {
      "lid": "node111 hfi1_0", 
      "remotePort": "1", 
      "name": "node111 hfi1_0"
     }, 
     "33": {
      "lid": "OmniPth00117501fa000404 S202B", 
      "remotePort": "9", 
      "name": "OmniPth00117501fa000404 S202B"
     }, 
     "32": {
      "lid": "OmniPth00117501fa000404 S201B", 
      "remotePort": "8", 
      "name": "OmniPth00117501fa000404 S201B"
     }, 
     "30": {
      "lid": "OmniPth00117501fa000404 S205A", 
      "remotePort": "43", 
      "name": "OmniPth00117501fa000404 S205A"
     }, 
     "37": {
      "lid": "OmniPth00117501fa000404 S206B", 
      "remotePort": "13", 
      "name": "OmniPth00117501fa000404 S206B"
     }, 
     "36": {
      "lid": "OmniPth00117501fa000404 S205B", 
      "remotePort": "12", 
      "name": "OmniPth00117501fa000404 S205B"
     }, 
     "35": {
      "lid": "OmniPth00117501fa000404 S204B", 
      "remotePort": "11", 
      "name": "OmniPth00117501fa000404 S204B"
     }, 
     "34": {
      "lid": "OmniPth00117501fa000404 S203B", 
      "remotePort": "10", 
      "name": "OmniPth00117501fa000404 S203B"
     }
    }
   }, 
   "OmniPth00117501fa000404 L112B": {
    "lid": "OmniPth00117501fa000404 L112B", 
    "name": "OmniPth00117501fa000404 L112B", 
    "ports": {
     "11": {
      "lid": "node110 hfi1_0", 
      "remotePort": "1", 
      "name": "node110 hfi1_0"
     }, 
     "39": {
      "lid": "OmniPth00117501fa000404 S204A", 
      "remotePort": "39", 
      "name": "OmniPth00117501fa000404 S204A"
     }, 
     "38": {
      "lid": "OmniPth00117501fa000404 S205A", 
      "remotePort": "38", 
      "name": "OmniPth00117501fa000404 S205A"
     }, 
     "43": {
      "lid": "OmniPth00117501fa000404 S205B", 
      "remotePort": "6", 
      "name": "OmniPth00117501fa000404 S205B"
     }, 
     "48": {
      "lid": "OmniPth00117501fa000404 S206B", 
      "remotePort": "1", 
      "name": "OmniPth00117501fa000404 S206B"
     }, 
     "46": {
      "lid": "OmniPth00117501fa000404 S202B", 
      "remotePort": "3", 
      "name": "OmniPth00117501fa000404 S202B"
     }, 
     "47": {
      "lid": "OmniPth00117501fa000404 S201B", 
      "remotePort": "2", 
      "name": "OmniPth00117501fa000404 S201B"
     }, 
     "44": {
      "lid": "OmniPth00117501fa000404 S204B", 
      "remotePort": "5", 
      "name": "OmniPth00117501fa000404 S204B"
     }, 
     "30": {
      "lid": "OmniPth00117501fa000404 S207B", 
      "remotePort": "19", 
      "name": "OmniPth00117501fa000404 S207B"
     }, 
     "37": {
      "lid": "OmniPth00117501fa000404 S206A", 
      "remotePort": "37", 
      "name": "OmniPth00117501fa000404 S206A"
     }, 
     "29": {
      "lid": "OmniPth00117501fa000404 S208B", 
      "remotePort": "20", 
      "name": "OmniPth00117501fa000404 S208B"
     }, 
     "35": {
      "lid": "OmniPth00117501fa000404 S208A", 
      "remotePort": "35", 
      "name": "OmniPth00117501fa000404 S208A"
     }, 
     "36": {
      "lid": "OmniPth00117501fa000404 S207A", 
      "remotePort": "36", 
      "name": "OmniPth00117501fa000404 S207A"
     }, 
     "1": {
      "lid": "node007 hfi1_0", 
      "remotePort": "1", 
      "name": "node007 hfi1_0"
     }, 
     "3": {
      "lid": "node109 hfi1_0", 
      "remotePort": "1", 
      "name": "node109 hfi1_0"
     }, 
     "40": {
      "lid": "OmniPth00117501fa000404 S203A", 
      "remotePort": "40", 
      "name": "OmniPth00117501fa000404 S203A"
     }, 
     "45": {
      "lid": "OmniPth00117501fa000404 S203B", 
      "remotePort": "4", 
      "name": "OmniPth00117501fa000404 S203B"
     }, 
     "41": {
      "lid": "OmniPth00117501fa000404 S202A", 
      "remotePort": "41", 
      "name": "OmniPth00117501fa000404 S202A"
     }, 
     "42": {
      "lid": "OmniPth00117501fa000404 S201A", 
      "remotePort": "42", 
      "name": "OmniPth00117501fa000404 S201A"
     }
    }
   }, 
   "OmniPth00117501fa000404 L114B": {
    "lid": "OmniPth00117501fa000404 L114B", 
    "name": "OmniPth00117501fa000404 L114B", 
    "ports": {
     "25": {
      "lid": "OmniPth00117501fa000404 S201B", 
      "remotePort": "24", 
      "name": "OmniPth00117501fa000404 S201B"
     }, 
     "39": {
      "lid": "OmniPth00117501fa000404 S205A", 
      "remotePort": "39", 
      "name": "OmniPth00117501fa000404 S205A"
     }, 
     "38": {
      "lid": "OmniPth00117501fa000404 S206A", 
      "remotePort": "38", 
      "name": "OmniPth00117501fa000404 S206A"
     }, 
     "43": {
      "lid": "OmniPth00117501fa000404 S206B", 
      "remotePort": "6", 
      "name": "OmniPth00117501fa000404 S206B"
     }, 
     "48": {
      "lid": "OmniPth00117501fa000404 S207B", 
      "remotePort": "1", 
      "name": "OmniPth00117501fa000404 S207B"
     }, 
     "46": {
      "lid": "OmniPth00117501fa000404 S203B", 
      "remotePort": "3", 
      "name": "OmniPth00117501fa000404 S203B"
     }, 
     "42": {
      "lid": "OmniPth00117501fa000404 S202A", 
      "remotePort": "42", 
      "name": "OmniPth00117501fa000404 S202A"
     }, 
     "31": {
      "lid": "OmniPth00117501fa000404 S201A", 
      "remotePort": "31", 
      "name": "OmniPth00117501fa000404 S201A"
     }, 
     "30": {
      "lid": "OmniPth00117501fa000404 S208B", 
      "remotePort": "19", 
      "name": "OmniPth00117501fa000404 S208B"
     }, 
     "37": {
      "lid": "OmniPth00117501fa000404 S207A", 
      "remotePort": "37", 
      "name": "OmniPth00117501fa000404 S207A"
     }, 
     "36": {
      "lid": "OmniPth00117501fa000404 S208A", 
      "remotePort": "36", 
      "name": "OmniPth00117501fa000404 S208A"
     }, 
     "40": {
      "lid": "OmniPth00117501fa000404 S204A", 
      "remotePort": "40", 
      "name": "OmniPth00117501fa000404 S204A"
     }, 
     "41": {
      "lid": "OmniPth00117501fa000404 S203A", 
      "remotePort": "41", 
      "name": "OmniPth00117501fa000404 S203A"
     }, 
     "47": {
      "lid": "OmniPth00117501fa000404 S202B", 
      "remotePort": "2", 
      "name": "OmniPth00117501fa000404 S202B"
     }, 
     "45": {
      "lid": "OmniPth00117501fa000404 S204B", 
      "remotePort": "4", 
      "name": "OmniPth00117501fa000404 S204B"
     }, 
     "44": {
      "lid": "OmniPth00117501fa000404 S205B", 
      "remotePort": "5", 
      "name": "OmniPth00117501fa000404 S205B"
     }
    }
   }, 
   "OmniPth00117501fa000404 L111A": {
    "lid": "OmniPth00117501fa000404 L111A", 
    "name": "OmniPth00117501fa000404 L111A", 
    "ports": {
     "37": {
      "lid": "OmniPth00117501fa000404 S206B", 
      "remotePort": "37", 
      "name": "OmniPth00117501fa000404 S206B"
     }, 
     "25": {
      "lid": "OmniPth00117501fa000404 S206A", 
      "remotePort": "24", 
      "name": "OmniPth00117501fa000404 S206A"
     }, 
     "26": {
      "lid": "OmniPth00117501fa000404 S201A", 
      "remotePort": "23", 
      "name": "OmniPth00117501fa000404 S201A"
     }, 
     "27": {
      "lid": "OmniPth00117501fa000404 S202A", 
      "remotePort": "22", 
      "name": "OmniPth00117501fa000404 S202A"
     }, 
     "39": {
      "lid": "OmniPth00117501fa000404 S208B", 
      "remotePort": "39", 
      "name": "OmniPth00117501fa000404 S208B"
     }, 
     "43": {
      "lid": "OmniPth00117501fa000404 S207A", 
      "remotePort": "6", 
      "name": "OmniPth00117501fa000404 S207A"
     }, 
     "38": {
      "lid": "OmniPth00117501fa000404 S207B", 
      "remotePort": "38", 
      "name": "OmniPth00117501fa000404 S207B"
     }, 
     "33": {
      "lid": "OmniPth00117501fa000404 S202B", 
      "remotePort": "33", 
      "name": "OmniPth00117501fa000404 S202B"
     }, 
     "32": {
      "lid": "OmniPth00117501fa000404 S201B", 
      "remotePort": "32", 
      "name": "OmniPth00117501fa000404 S201B"
     }, 
     "44": {
      "lid": "OmniPth00117501fa000404 S208A", 
      "remotePort": "5", 
      "name": "OmniPth00117501fa000404 S208A"
     }, 
     "30": {
      "lid": "OmniPth00117501fa000404 S205A", 
      "remotePort": "19", 
      "name": "OmniPth00117501fa000404 S205A"
     }, 
     "28": {
      "lid": "OmniPth00117501fa000404 S203A", 
      "remotePort": "21", 
      "name": "OmniPth00117501fa000404 S203A"
     }, 
     "29": {
      "lid": "OmniPth00117501fa000404 S204A", 
      "remotePort": "20", 
      "name": "OmniPth00117501fa000404 S204A"
     }, 
     "35": {
      "lid": "OmniPth00117501fa000404 S204B", 
      "remotePort": "35", 
      "name": "OmniPth00117501fa000404 S204B"
     }, 
     "34": {
      "lid": "OmniPth00117501fa000404 S203B", 
      "remotePort": "34", 
      "name": "OmniPth00117501fa000404 S203B"
     }, 
     "3": {
      "lid": "node103 hfi1_0", 
      "remotePort": "1", 
      "name": "node103 hfi1_0"
     }, 
     "36": {
      "lid": "OmniPth00117501fa000404 S205B", 
      "remotePort": "36", 
      "name": "OmniPth00117501fa000404 S205B"
     }, 
     "4": {
      "lid": "i220", 
      "remotePort": "6", 
      "name": "i220"
     }, 
     "6": {
      "lid": "node093 hfi1_0", 
      "remotePort": "1", 
      "name": "node093 hfi1_0"
     }, 
     "8": {
      "lid": "active730 hfi1_0", 
      "remotePort": "1", 
      "name": "active730 hfi1_0"
     }
    }
   }, 
   "OmniPth00117501fa000404 L111B": {
    "lid": "OmniPth00117501fa000404 L111B", 
    "name": "OmniPth00117501fa000404 L111B", 
    "ports": {
     "43": {
      "lid": "OmniPth00117501fa000404 S205B", 
      "remotePort": "30", 
      "name": "OmniPth00117501fa000404 S205B"
     }, 
     "48": {
      "lid": "OmniPth00117501fa000404 S206B", 
      "remotePort": "25", 
      "name": "OmniPth00117501fa000404 S206B"
     }, 
     "46": {
      "lid": "OmniPth00117501fa000404 S202B", 
      "remotePort": "27", 
      "name": "OmniPth00117501fa000404 S202B"
     }, 
     "47": {
      "lid": "OmniPth00117501fa000404 S201B", 
      "remotePort": "26", 
      "name": "OmniPth00117501fa000404 S201B"
     }, 
     "44": {
      "lid": "OmniPth00117501fa000404 S204B", 
      "remotePort": "29", 
      "name": "OmniPth00117501fa000404 S204B"
     }, 
     "45": {
      "lid": "OmniPth00117501fa000404 S203B", 
      "remotePort": "28", 
      "name": "OmniPth00117501fa000404 S203B"
     }, 
     "42": {
      "lid": "OmniPth00117501fa000404 S201A", 
      "remotePort": "18", 
      "name": "OmniPth00117501fa000404 S201A"
     }, 
     "29": {
      "lid": "OmniPth00117501fa000404 S208B", 
      "remotePort": "44", 
      "name": "OmniPth00117501fa000404 S208B"
     }, 
     "40": {
      "lid": "OmniPth00117501fa000404 S203A", 
      "remotePort": "16", 
      "name": "OmniPth00117501fa000404 S203A"
     }, 
     "41": {
      "lid": "OmniPth00117501fa000404 S202A", 
      "remotePort": "17", 
      "name": "OmniPth00117501fa000404 S202A"
     }, 
     "1": {
      "lid": "i220", 
      "remotePort": "5", 
      "name": "i220"
     }, 
     "6": {
      "lid": "node078 hfi1_0", 
      "remotePort": "1", 
      "name": "node078 hfi1_0"
     }, 
     "9": {
      "lid": "node096 hfi1_0", 
      "remotePort": "1", 
      "name": "node096 hfi1_0"
     }, 
     "11": {
      "lid": "node095 hfi1_0", 
      "remotePort": "1", 
      "name": "node095 hfi1_0"
     }, 
     "10": {
      "lid": "node091 hfi1_0", 
      "remotePort": "1", 
      "name": "node091 hfi1_0"
     }, 
     "39": {
      "lid": "OmniPth00117501fa000404 S204A", 
      "remotePort": "15", 
      "name": "OmniPth00117501fa000404 S204A"
     }, 
     "38": {
      "lid": "OmniPth00117501fa000404 S205A", 
      "remotePort": "14", 
      "name": "OmniPth00117501fa000404 S205A"
     }, 
     "14": {
      "lid": "node092 hfi1_0", 
      "remotePort": "1", 
      "name": "node092 hfi1_0"
     }, 
     "30": {
      "lid": "OmniPth00117501fa000404 S207B", 
      "remotePort": "43", 
      "name": "OmniPth00117501fa000404 S207B"
     }, 
     "37": {
      "lid": "OmniPth00117501fa000404 S206A", 
      "remotePort": "13", 
      "name": "OmniPth00117501fa000404 S206A"
     }, 
     "36": {
      "lid": "OmniPth00117501fa000404 S207A", 
      "remotePort": "12", 
      "name": "OmniPth00117501fa000404 S207A"
     }, 
     "35": {
      "lid": "OmniPth00117501fa000404 S208A", 
      "remotePort": "11", 
      "name": "OmniPth00117501fa000404 S208A"
     }
    }
   }, 
   "OmniPth00117501fa000404 L101A": {
    "lid": "OmniPth00117501fa000404 L101A", 
    "name": "OmniPth00117501fa000404 L101A", 
    "ports": {
     "25": {
      "lid": "OmniPth00117501fa000404 S201A", 
      "remotePort": "24", 
      "name": "OmniPth00117501fa000404 S201A"
     }, 
     "26": {
      "lid": "OmniPth00117501fa000404 S208A", 
      "remotePort": "23", 
      "name": "OmniPth00117501fa000404 S208A"
     }, 
     "48": {
      "lid": "OmniPth00117501fa000404 S207A", 
      "remotePort": "1", 
      "name": "OmniPth00117501fa000404 S207A"
     }, 
     "46": {
      "lid": "OmniPth00117501fa000404 S205A", 
      "remotePort": "3", 
      "name": "OmniPth00117501fa000404 S205A"
     }, 
     "47": {
      "lid": "OmniPth00117501fa000404 S206A", 
      "remotePort": "2", 
      "name": "OmniPth00117501fa000404 S206A"
     }, 
     "44": {
      "lid": "OmniPth00117501fa000404 S203A", 
      "remotePort": "5", 
      "name": "OmniPth00117501fa000404 S203A"
     }, 
     "45": {
      "lid": "OmniPth00117501fa000404 S204A", 
      "remotePort": "4", 
      "name": "OmniPth00117501fa000404 S204A"
     }, 
     "42": {
      "lid": "OmniPth00117501fa000404 S206B", 
      "remotePort": "42", 
      "name": "OmniPth00117501fa000404 S206B"
     }, 
     "43": {
      "lid": "OmniPth00117501fa000404 S202A", 
      "remotePort": "6", 
      "name": "OmniPth00117501fa000404 S202A"
     }, 
     "40": {
      "lid": "OmniPth00117501fa000404 S204B", 
      "remotePort": "40", 
      "name": "OmniPth00117501fa000404 S204B"
     }, 
     "41": {
      "lid": "OmniPth00117501fa000404 S205B", 
      "remotePort": "41", 
      "name": "OmniPth00117501fa000404 S205B"
     }, 
     "1": {
      "lid": "node201 hfi1_0", 
      "remotePort": "1", 
      "name": "node201 hfi1_0"
     }, 
     "3": {
      "lid": "node216 hfi1_0", 
      "remotePort": "1", 
      "name": "node216 hfi1_0"
     }, 
     "2": {
      "lid": "node215 hfi1_0", 
      "remotePort": "1", 
      "name": "node215 hfi1_0"
     }, 
     "5": {
      "lid": "node212 hfi1_0", 
      "remotePort": "1", 
      "name": "node212 hfi1_0"
     }, 
     "4": {
      "lid": "node211 hfi1_0", 
      "remotePort": "1", 
      "name": "node211 hfi1_0"
     }, 
     "6": {
      "lid": "node206 hfi1_0", 
      "remotePort": "1", 
      "name": "node206 hfi1_0"
     }, 
     "9": {
      "lid": "node213 hfi1_0", 
      "remotePort": "1", 
      "name": "node213 hfi1_0"
     }, 
     "8": {
      "lid": "node207 hfi1_0", 
      "remotePort": "1", 
      "name": "node207 hfi1_0"
     }, 
     "39": {
      "lid": "OmniPth00117501fa000404 S203B", 
      "remotePort": "39", 
      "name": "OmniPth00117501fa000404 S203B"
     }, 
     "38": {
      "lid": "OmniPth00117501fa000404 S202B", 
      "remotePort": "38", 
      "name": "OmniPth00117501fa000404 S202B"
     }, 
     "11": {
      "lid": "node202 hfi1_0", 
      "remotePort": "1", 
      "name": "node202 hfi1_0"
     }, 
     "10": {
      "lid": "node203 hfi1_0", 
      "remotePort": "1", 
      "name": "node203 hfi1_0"
     }, 
     "13": {
      "lid": "node214 hfi1_0", 
      "remotePort": "1", 
      "name": "node214 hfi1_0"
     }, 
     "12": {
      "lid": "node208 hfi1_0", 
      "remotePort": "1", 
      "name": "node208 hfi1_0"
     }, 
     "15": {
      "lid": "node209 hfi1_0", 
      "remotePort": "1", 
      "name": "node209 hfi1_0"
     }, 
     "14": {
      "lid": "node210 hfi1_0", 
      "remotePort": "1", 
      "name": "node210 hfi1_0"
     }, 
     "16": {
      "lid": "node205 hfi1_0", 
      "remotePort": "1", 
      "name": "node205 hfi1_0"
     }, 
     "32": {
      "lid": "OmniPth00117501fa000404 S208B", 
      "remotePort": "32", 
      "name": "OmniPth00117501fa000404 S208B"
     }, 
     "31": {
      "lid": "OmniPth00117501fa000404 S207B", 
      "remotePort": "31", 
      "name": "OmniPth00117501fa000404 S207B"
     }, 
     "37": {
      "lid": "OmniPth00117501fa000404 S201B", 
      "remotePort": "37", 
      "name": "OmniPth00117501fa000404 S201B"
     }
    }
   }, 
   "OmniPth00117501fa000404 L101B": {
    "lid": "OmniPth00117501fa000404 L101B", 
    "name": "OmniPth00117501fa000404 L101B", 
    "ports": {
     "42": {
      "lid": "OmniPth00117501fa000404 S208A", 
      "remotePort": "18", 
      "name": "OmniPth00117501fa000404 S208A"
     }, 
     "25": {
      "lid": "OmniPth00117501fa000404 S207B", 
      "remotePort": "48", 
      "name": "OmniPth00117501fa000404 S207B"
     }, 
     "26": {
      "lid": "OmniPth00117501fa000404 S206B", 
      "remotePort": "47", 
      "name": "OmniPth00117501fa000404 S206B"
     }, 
     "27": {
      "lid": "OmniPth00117501fa000404 S205B", 
      "remotePort": "46", 
      "name": "OmniPth00117501fa000404 S205B"
     }, 
     "48": {
      "lid": "OmniPth00117501fa000404 S201B", 
      "remotePort": "25", 
      "name": "OmniPth00117501fa000404 S201B"
     }, 
     "47": {
      "lid": "OmniPth00117501fa000404 S208B", 
      "remotePort": "26", 
      "name": "OmniPth00117501fa000404 S208B"
     }, 
     "28": {
      "lid": "OmniPth00117501fa000404 S204B", 
      "remotePort": "45", 
      "name": "OmniPth00117501fa000404 S204B"
     }, 
     "29": {
      "lid": "OmniPth00117501fa000404 S203B", 
      "remotePort": "44", 
      "name": "OmniPth00117501fa000404 S203B"
     }, 
     "1": {
      "lid": "node231 hfi1_0", 
      "remotePort": "1", 
      "name": "node231 hfi1_0"
     }, 
     "3": {
      "lid": "node218 hfi1_0", 
      "remotePort": "1", 
      "name": "node218 hfi1_0"
     }, 
     "2": {
      "lid": "node224 hfi1_0", 
      "remotePort": "1", 
      "name": "node224 hfi1_0"
     }, 
     "5": {
      "lid": "node221 hfi1_0", 
      "remotePort": "1", 
      "name": "node221 hfi1_0"
     }, 
     "4": {
      "lid": "node227 hfi1_0", 
      "remotePort": "1", 
      "name": "node227 hfi1_0"
     }, 
     "6": {
      "lid": "node222 hfi1_0", 
      "remotePort": "1", 
      "name": "node222 hfi1_0"
     }, 
     "9": {
      "lid": "node220 hfi1_0", 
      "remotePort": "1", 
      "name": "node220 hfi1_0"
     }, 
     "8": {
      "lid": "node217 hfi1_0", 
      "remotePort": "1", 
      "name": "node217 hfi1_0"
     }, 
     "11": {
      "lid": "node226 hfi1_0", 
      "remotePort": "1", 
      "name": "node226 hfi1_0"
     }, 
     "10": {
      "lid": "node228 hfi1_0", 
      "remotePort": "1", 
      "name": "node228 hfi1_0"
     }, 
     "13": {
      "lid": "node219 hfi1_0", 
      "remotePort": "1", 
      "name": "node219 hfi1_0"
     }, 
     "12": {
      "lid": "node230 hfi1_0", 
      "remotePort": "1", 
      "name": "node230 hfi1_0"
     }, 
     "15": {
      "lid": "node232 hfi1_0", 
      "remotePort": "1", 
      "name": "node232 hfi1_0"
     }, 
     "14": {
      "lid": "node223 hfi1_0", 
      "remotePort": "1", 
      "name": "node223 hfi1_0"
     }, 
     "16": {
      "lid": "node229 hfi1_0", 
      "remotePort": "1", 
      "name": "node229 hfi1_0"
     }, 
     "33": {
      "lid": "OmniPth00117501fa000404 S205A", 
      "remotePort": "9", 
      "name": "OmniPth00117501fa000404 S205A"
     }, 
     "32": {
      "lid": "OmniPth00117501fa000404 S206A", 
      "remotePort": "8", 
      "name": "OmniPth00117501fa000404 S206A"
     }, 
     "31": {
      "lid": "OmniPth00117501fa000404 S207A", 
      "remotePort": "7", 
      "name": "OmniPth00117501fa000404 S207A"
     }, 
     "30": {
      "lid": "OmniPth00117501fa000404 S202B", 
      "remotePort": "43", 
      "name": "OmniPth00117501fa000404 S202B"
     }, 
     "37": {
      "lid": "OmniPth00117501fa000404 S201A", 
      "remotePort": "13", 
      "name": "OmniPth00117501fa000404 S201A"
     }, 
     "36": {
      "lid": "OmniPth00117501fa000404 S202A", 
      "remotePort": "12", 
      "name": "OmniPth00117501fa000404 S202A"
     }, 
     "35": {
      "lid": "OmniPth00117501fa000404 S203A", 
      "remotePort": "11", 
      "name": "OmniPth00117501fa000404 S203A"
     }, 
     "34": {
      "lid": "OmniPth00117501fa000404 S204A", 
      "remotePort": "10", 
      "name": "OmniPth00117501fa000404 S204A"
     }
    }
   }, 
   "OmniPth00117501fa000404 L117B": {
    "lid": "OmniPth00117501fa000404 L117B", 
    "name": "OmniPth00117501fa000404 L117B", 
    "ports": {
     "25": {
      "lid": "OmniPth00117501fa000404 S203B", 
      "remotePort": "48", 
      "name": "OmniPth00117501fa000404 S203B"
     }, 
     "26": {
      "lid": "OmniPth00117501fa000404 S202B", 
      "remotePort": "47", 
      "name": "OmniPth00117501fa000404 S202B"
     }, 
     "27": {
      "lid": "OmniPth00117501fa000404 S201B", 
      "remotePort": "46", 
      "name": "OmniPth00117501fa000404 S201B"
     }, 
     "43": {
      "lid": "OmniPth00117501fa000404 S208B", 
      "remotePort": "30", 
      "name": "OmniPth00117501fa000404 S208B"
     }, 
     "33": {
      "lid": "OmniPth00117501fa000404 S201A", 
      "remotePort": "9", 
      "name": "OmniPth00117501fa000404 S201A"
     }, 
     "32": {
      "lid": "OmniPth00117501fa000404 S202A", 
      "remotePort": "8", 
      "name": "OmniPth00117501fa000404 S202A"
     }, 
     "31": {
      "lid": "OmniPth00117501fa000404 S203A", 
      "remotePort": "7", 
      "name": "OmniPth00117501fa000404 S203A"
     }, 
     "45": {
      "lid": "OmniPth00117501fa000404 S206B", 
      "remotePort": "28", 
      "name": "OmniPth00117501fa000404 S206B"
     }, 
     "42": {
      "lid": "OmniPth00117501fa000404 S204A", 
      "remotePort": "18", 
      "name": "OmniPth00117501fa000404 S204A"
     }, 
     "39": {
      "lid": "OmniPth00117501fa000404 S207A", 
      "remotePort": "15", 
      "name": "OmniPth00117501fa000404 S207A"
     }, 
     "40": {
      "lid": "OmniPth00117501fa000404 S206A", 
      "remotePort": "16", 
      "name": "OmniPth00117501fa000404 S206A"
     }, 
     "41": {
      "lid": "OmniPth00117501fa000404 S205A", 
      "remotePort": "17", 
      "name": "OmniPth00117501fa000404 S205A"
     }, 
     "46": {
      "lid": "OmniPth00117501fa000404 S205B", 
      "remotePort": "27", 
      "name": "OmniPth00117501fa000404 S205B"
     }, 
     "38": {
      "lid": "OmniPth00117501fa000404 S208A", 
      "remotePort": "14", 
      "name": "OmniPth00117501fa000404 S208A"
     }, 
     "47": {
      "lid": "OmniPth00117501fa000404 S204B", 
      "remotePort": "26", 
      "name": "OmniPth00117501fa000404 S204B"
     }, 
     "44": {
      "lid": "OmniPth00117501fa000404 S207B", 
      "remotePort": "29", 
      "name": "OmniPth00117501fa000404 S207B"
     }
    }
   }, 
   "OmniPth00117501fa000404 L117A": {
    "lid": "OmniPth00117501fa000404 L117A", 
    "name": "OmniPth00117501fa000404 L117A", 
    "ports": {
     "34": {
      "lid": "OmniPth00117501fa000404 S206B", 
      "remotePort": "34", 
      "name": "OmniPth00117501fa000404 S206B"
     }, 
     "26": {
      "lid": "OmniPth00117501fa000404 S204A", 
      "remotePort": "23", 
      "name": "OmniPth00117501fa000404 S204A"
     }, 
     "27": {
      "lid": "OmniPth00117501fa000404 S205A", 
      "remotePort": "22", 
      "name": "OmniPth00117501fa000404 S205A"
     }, 
     "48": {
      "lid": "OmniPth00117501fa000404 S203A", 
      "remotePort": "1", 
      "name": "OmniPth00117501fa000404 S203A"
     }, 
     "16": {
      "lid": "passive730 hfi1_0", 
      "remotePort": "1", 
      "name": "passive730 hfi1_0"
     }, 
     "33": {
      "lid": "OmniPth00117501fa000404 S205B", 
      "remotePort": "33", 
      "name": "OmniPth00117501fa000404 S205B"
     }, 
     "28": {
      "lid": "OmniPth00117501fa000404 S206A", 
      "remotePort": "21", 
      "name": "OmniPth00117501fa000404 S206A"
     }, 
     "31": {
      "lid": "OmniPth00117501fa000404 S203B", 
      "remotePort": "31", 
      "name": "OmniPth00117501fa000404 S203B"
     }, 
     "30": {
      "lid": "OmniPth00117501fa000404 S208A", 
      "remotePort": "19", 
      "name": "OmniPth00117501fa000404 S208A"
     }, 
     "42": {
      "lid": "OmniPth00117501fa000404 S202B", 
      "remotePort": "42", 
      "name": "OmniPth00117501fa000404 S202B"
     }, 
     "29": {
      "lid": "OmniPth00117501fa000404 S207A", 
      "remotePort": "20", 
      "name": "OmniPth00117501fa000404 S207A"
     }, 
     "35": {
      "lid": "OmniPth00117501fa000404 S207B", 
      "remotePort": "35", 
      "name": "OmniPth00117501fa000404 S207B"
     }, 
     "41": {
      "lid": "OmniPth00117501fa000404 S201B", 
      "remotePort": "41", 
      "name": "OmniPth00117501fa000404 S201B"
     }, 
     "1": {
      "lid": "router hfi1_0", 
      "remotePort": "1", 
      "name": "router hfi1_0"
     }, 
     "46": {
      "lid": "OmniPth00117501fa000404 S201A", 
      "remotePort": "3", 
      "name": "OmniPth00117501fa000404 S201A"
     }, 
     "36": {
      "lid": "OmniPth00117501fa000404 S208B", 
      "remotePort": "36", 
      "name": "OmniPth00117501fa000404 S208B"
     }, 
     "5": {
      "lid": "node077 hfi1_0", 
      "remotePort": "1", 
      "name": "node077 hfi1_0"
     }, 
     "47": {
      "lid": "OmniPth00117501fa000404 S202A", 
      "remotePort": "2", 
      "name": "OmniPth00117501fa000404 S202A"
     }, 
     "6": {
      "lid": "node088 hfi1_0", 
      "remotePort": "1", 
      "name": "node088 hfi1_0"
     }, 
     "32": {
      "lid": "OmniPth00117501fa000404 S204B", 
      "remotePort": "32", 
      "name": "OmniPth00117501fa000404 S204B"
     }
    }
   }, 
   "OmniPth00117501fa000404 L115A": {
    "lid": "OmniPth00117501fa000404 L115A", 
    "name": "OmniPth00117501fa000404 L115A", 
    "ports": {
     "25": {
      "lid": "OmniPth00117501fa000404 S208A", 
      "remotePort": "24", 
      "name": "OmniPth00117501fa000404 S208A"
     }, 
     "13": {
      "lid": "node108 hfi1_0", 
      "remotePort": "1", 
      "name": "node108 hfi1_0"
     }, 
     "27": {
      "lid": "OmniPth00117501fa000404 S204A", 
      "remotePort": "22", 
      "name": "OmniPth00117501fa000404 S204A"
     }, 
     "15": {
      "lid": "node106 hfi1_0", 
      "remotePort": "1", 
      "name": "node106 hfi1_0"
     }, 
     "48": {
      "lid": "OmniPth00117501fa000404 S202A", 
      "remotePort": "1", 
      "name": "OmniPth00117501fa000404 S202A"
     }, 
     "16": {
      "lid": "node105 hfi1_0", 
      "remotePort": "1", 
      "name": "node105 hfi1_0"
     }, 
     "33": {
      "lid": "OmniPth00117501fa000404 S204B", 
      "remotePort": "33", 
      "name": "OmniPth00117501fa000404 S204B"
     }, 
     "32": {
      "lid": "OmniPth00117501fa000404 S203B", 
      "remotePort": "32", 
      "name": "OmniPth00117501fa000404 S203B"
     }, 
     "31": {
      "lid": "OmniPth00117501fa000404 S202B", 
      "remotePort": "31", 
      "name": "OmniPth00117501fa000404 S202B"
     }, 
     "30": {
      "lid": "OmniPth00117501fa000404 S207A", 
      "remotePort": "19", 
      "name": "OmniPth00117501fa000404 S207A"
     }, 
     "28": {
      "lid": "OmniPth00117501fa000404 S205A", 
      "remotePort": "21", 
      "name": "OmniPth00117501fa000404 S205A"
     }, 
     "26": {
      "lid": "OmniPth00117501fa000404 S203A", 
      "remotePort": "23", 
      "name": "OmniPth00117501fa000404 S203A"
     }, 
     "35": {
      "lid": "OmniPth00117501fa000404 S206B", 
      "remotePort": "35", 
      "name": "OmniPth00117501fa000404 S206B"
     }, 
     "29": {
      "lid": "OmniPth00117501fa000404 S206A", 
      "remotePort": "20", 
      "name": "OmniPth00117501fa000404 S206A"
     }, 
     "1": {
      "lid": "node107 hfi1_0", 
      "remotePort": "1", 
      "name": "node107 hfi1_0"
     }, 
     "3": {
      "lid": "node104 hfi1_0", 
      "remotePort": "1", 
      "name": "node104 hfi1_0"
     }, 
     "36": {
      "lid": "OmniPth00117501fa000404 S207B", 
      "remotePort": "36", 
      "name": "OmniPth00117501fa000404 S207B"
     }, 
     "37": {
      "lid": "OmniPth00117501fa000404 S208B", 
      "remotePort": "37", 
      "name": "OmniPth00117501fa000404 S208B"
     }, 
     "47": {
      "lid": "OmniPth00117501fa000404 S201A", 
      "remotePort": "2", 
      "name": "OmniPth00117501fa000404 S201A"
     }, 
     "34": {
      "lid": "OmniPth00117501fa000404 S205B", 
      "remotePort": "34", 
      "name": "OmniPth00117501fa000404 S205B"
     }, 
     "42": {
      "lid": "OmniPth00117501fa000404 S201B", 
      "remotePort": "42", 
      "name": "OmniPth00117501fa000404 S201B"
     }
    }
   }, 
   "OmniPth00117501fa000404 L119A": {
    "lid": "OmniPth00117501fa000404 L119A", 
    "name": "OmniPth00117501fa000404 L119A", 
    "ports": {
     "26": {
      "lid": "OmniPth00117501fa000404 S205A", 
      "remotePort": "23", 
      "name": "OmniPth00117501fa000404 S205A"
     }, 
     "27": {
      "lid": "OmniPth00117501fa000404 S206A", 
      "remotePort": "22", 
      "name": "OmniPth00117501fa000404 S206A"
     }, 
     "48": {
      "lid": "OmniPth00117501fa000404 S204A", 
      "remotePort": "1", 
      "name": "OmniPth00117501fa000404 S204A"
     }, 
     "33": {
      "lid": "OmniPth00117501fa000404 S206B", 
      "remotePort": "33", 
      "name": "OmniPth00117501fa000404 S206B"
     }, 
     "32": {
      "lid": "OmniPth00117501fa000404 S205B", 
      "remotePort": "32", 
      "name": "OmniPth00117501fa000404 S205B"
     }, 
     "31": {
      "lid": "OmniPth00117501fa000404 S204B", 
      "remotePort": "31", 
      "name": "OmniPth00117501fa000404 S204B"
     }, 
     "42": {
      "lid": "OmniPth00117501fa000404 S203B", 
      "remotePort": "42", 
      "name": "OmniPth00117501fa000404 S203B"
     }, 
     "28": {
      "lid": "OmniPth00117501fa000404 S207A", 
      "remotePort": "21", 
      "name": "OmniPth00117501fa000404 S207A"
     }, 
     "29": {
      "lid": "OmniPth00117501fa000404 S208A", 
      "remotePort": "20", 
      "name": "OmniPth00117501fa000404 S208A"
     }, 
     "35": {
      "lid": "OmniPth00117501fa000404 S208B", 
      "remotePort": "35", 
      "name": "OmniPth00117501fa000404 S208B"
     }, 
     "34": {
      "lid": "OmniPth00117501fa000404 S207B", 
      "remotePort": "34", 
      "name": "OmniPth00117501fa000404 S207B"
     }, 
     "46": {
      "lid": "OmniPth00117501fa000404 S202A", 
      "remotePort": "3", 
      "name": "OmniPth00117501fa000404 S202A"
     }, 
     "40": {
      "lid": "OmniPth00117501fa000404 S201B", 
      "remotePort": "40", 
      "name": "OmniPth00117501fa000404 S201B"
     }, 
     "47": {
      "lid": "OmniPth00117501fa000404 S203A", 
      "remotePort": "2", 
      "name": "OmniPth00117501fa000404 S203A"
     }, 
     "45": {
      "lid": "OmniPth00117501fa000404 S201A", 
      "remotePort": "4", 
      "name": "OmniPth00117501fa000404 S201A"
     }, 
     "41": {
      "lid": "OmniPth00117501fa000404 S202B", 
      "remotePort": "41", 
      "name": "OmniPth00117501fa000404 S202B"
     }
    }
   }, 
   "OmniPth00117501fa000404 L115B": {
    "lid": "OmniPth00117501fa000404 L115B", 
    "name": "OmniPth00117501fa000404 L115B", 
    "ports": {
     "25": {
      "lid": "OmniPth00117501fa000404 S202B", 
      "remotePort": "48", 
      "name": "OmniPth00117501fa000404 S202B"
     }, 
     "26": {
      "lid": "OmniPth00117501fa000404 S201B", 
      "remotePort": "47", 
      "name": "OmniPth00117501fa000404 S201B"
     }, 
     "48": {
      "lid": "OmniPth00117501fa000404 S208B", 
      "remotePort": "25", 
      "name": "OmniPth00117501fa000404 S208B"
     }, 
     "46": {
      "lid": "OmniPth00117501fa000404 S204B", 
      "remotePort": "27", 
      "name": "OmniPth00117501fa000404 S204B"
     }, 
     "47": {
      "lid": "OmniPth00117501fa000404 S203B", 
      "remotePort": "26", 
      "name": "OmniPth00117501fa000404 S203B"
     }, 
     "44": {
      "lid": "OmniPth00117501fa000404 S206B", 
      "remotePort": "29", 
      "name": "OmniPth00117501fa000404 S206B"
     }, 
     "45": {
      "lid": "OmniPth00117501fa000404 S205B", 
      "remotePort": "28", 
      "name": "OmniPth00117501fa000404 S205B"
     }, 
     "42": {
      "lid": "OmniPth00117501fa000404 S203A", 
      "remotePort": "18", 
      "name": "OmniPth00117501fa000404 S203A"
     }, 
     "43": {
      "lid": "OmniPth00117501fa000404 S207B", 
      "remotePort": "30", 
      "name": "OmniPth00117501fa000404 S207B"
     }, 
     "40": {
      "lid": "OmniPth00117501fa000404 S205A", 
      "remotePort": "16", 
      "name": "OmniPth00117501fa000404 S205A"
     }, 
     "41": {
      "lid": "OmniPth00117501fa000404 S204A", 
      "remotePort": "17", 
      "name": "OmniPth00117501fa000404 S204A"
     }, 
     "1": {
      "lid": "node086 hfi1_0", 
      "remotePort": "1", 
      "name": "node086 hfi1_0"
     }, 
     "3": {
      "lid": "node085 hfi1_0", 
      "remotePort": "1", 
      "name": "node085 hfi1_0"
     }, 
     "2": {
      "lid": "node087 hfi1_0", 
      "remotePort": "1", 
      "name": "node087 hfi1_0"
     }, 
     "4": {
      "lid": "node090 hfi1_0", 
      "remotePort": "1", 
      "name": "node090 hfi1_0"
     }, 
     "8": {
      "lid": "node089 hfi1_0", 
      "remotePort": "1", 
      "name": "node089 hfi1_0"
     }, 
     "10": {
      "lid": "node094 hfi1_0", 
      "remotePort": "1", 
      "name": "node094 hfi1_0"
     }, 
     "39": {
      "lid": "OmniPth00117501fa000404 S206A", 
      "remotePort": "15", 
      "name": "OmniPth00117501fa000404 S206A"
     }, 
     "38": {
      "lid": "OmniPth00117501fa000404 S207A", 
      "remotePort": "14", 
      "name": "OmniPth00117501fa000404 S207A"
     }, 
     "15": {
      "lid": "node076 hfi1_0", 
      "remotePort": "1", 
      "name": "node076 hfi1_0"
     }, 
     "32": {
      "lid": "OmniPth00117501fa000404 S201A", 
      "remotePort": "8", 
      "name": "OmniPth00117501fa000404 S201A"
     }, 
     "31": {
      "lid": "OmniPth00117501fa000404 S202A", 
      "remotePort": "7", 
      "name": "OmniPth00117501fa000404 S202A"
     }, 
     "37": {
      "lid": "OmniPth00117501fa000404 S208A", 
      "remotePort": "13", 
      "name": "OmniPth00117501fa000404 S208A"
     }
    }
   }
  }
 }
}
