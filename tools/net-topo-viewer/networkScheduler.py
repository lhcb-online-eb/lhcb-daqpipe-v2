#!/usr/bin/python
######################################################
#            PROJECT  : lhcb-daqpipe                 #
#            VERSION  : 2.5.0-dev                    #
#            DATE     : 12/2017                      #
#            AUTHOR   : Rainer Schwemmer  - CERN     #
#            LICENSE  : CeCILL-C                     #
######################################################

from optparse import OptionParser
import math
import json

class ThetaNumberBase:
  def __init__(self, baseNumbers):
    self.n = len(baseNumbers)
    self.theta = baseNumbers[:]
    #Not a switch radix but the radix of the digit at number position N
    self.radix = [1,]
    self.calcNumberRadix()
    self.numMax = 1
    for i in self.theta:
      self.numMax *= i  

  def calcNumberRadix(self):
    r = 1
    for i in range(0, (self.n)):
      r*=self.theta[i]
      self.radix.append(r)
    print self.theta
    print self.radix

  def convertToTheta(self, number):
    res = []
    #print self.theta
    tmp = self.radix[:]
    tmp.reverse()
    #print self.radix
    for r in tmp:
      k = number/r
      res.append(k)
      number -= k * r
    res.reverse()

    return res      
   
  def convertToNumber(self, thetaNumber):
    number = 0
    for i in range(0, len(self.radix)):
      if i < len(thetaNumber):
        number += thetaNumber[i] * self.radix[i]

    return number

  def printout(self):
    print "Number Base:"
    print self.theta

    print "Number radix:"
    print self.radix

    print "Max Number:"
    print self.numMax

class VariableBaseNumber:
  def __init__(self, numberBase):
    self.numberBase = numberBase
    self.thetaNotation = [0]
    self.number = 0

  def setNumber(self, number):
    self.thetaNotation = self.numberBase.convertToTheta(number)
    self.number = number

  def setTheta(self, theta):
    self.thetaNotation = theta
    self.number = self.numberBase.convertToNumber(theta)

  def printout(self):
    print self.number
    print self.thetaNotation

def buildClosNetwork(radix, nodes, blocking_factor):
  nodesPerEdge =  math.floor(float(radix) * float(blocking_factor)/float(blocking_factor+1))
  edges = math.ceil(float(nodes) / nodesPerEdge)
  
  b = radix/edges
  edge_core = radix-nodesPerEdge
  cores = math.ceil(float(edge_core)/float(b))

  print "Edges:          %d" % edges
  print "Nodes per edge: %d" % nodesPerEdge
  print "Cores:          %d" % cores
  print "Core links      %d" % edge_core

  return (edges, cores, nodesPerEdge)

def calculateDestination(source, phase, base, reverseBase):
  s = VariableBaseNumber(reverseBase)
  s.setNumber(source)

  p = VariableBaseNumber(reverseBase)
  p.setNumber(phase)
  
  baseLength = len(base.theta)
  alpha = [0] * (baseLength)
  #print "s:"
  #s.printout()
  #print "p:"
  #p.printout()

  for i in range(0, baseLength):
    d = (s.thetaNotation[i] + p.thetaNotation[i]) % reverseBase.theta[i]
#    print " a%d: %d" % (i,d)
    alpha[baseLength - 1 - i] = d

  d = VariableBaseNumber(base)
  d.setTheta(alpha)
  ret = d.number

#  print s.thetaNotation
#  print p.thetaNotation
#  print "==>"
#  print alpha, ret
#  print ""

  #print ret

  return ret


def printNetworkSchedule(topology):
  #First create the number base in which we are going to work

  baseNumbers = topology[1]
  base = ThetaNumberBase(baseNumbers)
  baseNumbersReverse = list(baseNumbers)
  baseNumbersReverse.reverse()
  reverseBase = ThetaNumberBase(baseNumbersReverse)

  print "Created number base for topology:"
  print topology

  print "Forward base:"
  base.printout()
  print "Reverse base:"
  reverseBase.printout()

  header = "Phase\t"

  numMax = base.numMax

  for phase in range(0,numMax):
    header += "%d\t" % phase

  sched = [];
#  print header
  for phase in range(0,numMax):
    tmp = {}
    for source in range(0,numMax):
      tmp[source] = calculateDestination(source, phase, base, reverseBase)
    sched.append(tmp)
  file = open("sched.js","w") 
  file.write("customSched = "+json.dumps(sched,indent=4)+";")

def main():
  parser=OptionParser()
  parser.add_option("-r", "--radix", dest="radix", help="Switch radix")
  parser.add_option("-n", "--nodes", dest="nodes", help="Number of network nodes")
  parser.add_option("-b", "--blocking_factor", dest="blocking_factor", help="Network overcommit. 1:1 = 1, 2:1 = 2, etc")

  (options, args) = parser.parse_args()

  radix = int(options.radix)
  nodes = int(options.nodes)
  blocking_factor = float(options.blocking_factor)

  network = buildClosNetwork(radix, nodes, blocking_factor)

  topology = (2, (int(network[2]), int(network[0])))

# base = ThetaNumberBase(topology[1])

#  base.printout()

#  for i in range(0,8):
#    n = VariableBaseNumber(base)
#    n.setNumber(i)
#    n.printout()
#    print ""

  printNetworkSchedule(topology)

if __name__ == "__main__":
  main()

