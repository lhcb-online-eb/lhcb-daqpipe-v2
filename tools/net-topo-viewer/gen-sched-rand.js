/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

var fs = require('fs');

/********************  GLOBALS  *********************/
var nodes = 64;
var steps = nodes;

/*******************  FUNCTION  *********************/
function barrel(nodes, node, step)
{
	return (node-step+3*nodes)%nodes
}

/*******************  FUNCTION  *********************/
function isAvail(step,comm)
{
	if (comm == null)
		return false;
	for (var i in step)
		if (i == comm.start ||step[i] == comm.target)
			return false;
	return true;
}

/****************************************************/
//gen list
var lst = {};
var id = 0;
for (var step = 1 ; step < steps ; step++)
{
	for (var node = 0 ; node < nodes ; node++)
	{
		lst[id] = {id:id,start:node,target:barrel(nodes,node,step)};
		id++;
	}
}

/****************************************************/
//setup vars
var sched = [];
var step = {};

/****************************************************/
//gen steps
while (id > 0)
{
	//gen candidate list
	var candidate=[];
	for (var i in lst)
	{
		if (isAvail(step,lst[i]))
			candidate.push(lst[i]);
	}
	
	//if has something or not
	if (candidate.length == 0)
	{
		sched.push(step);
		step = {};
		console.log("Done step "+sched.length);
	} else {
		//randomly select one
		var sel = Math.floor(Math.random() * candidate.length);
		var selected = candidate[sel];
		
		//apply
		step[selected.start] = selected.target;
		
		//remove in list
		lst[selected.id] = undefined;
		id--;
	}
}

/****************************************************/
//dump
fs.writeFileSync("sched.js","customSched = "+JSON.stringify(sched, null,"\t")+";");
