var fs = require('fs');

var linkSize = 5;
var switchSize = 60;
var rootSwitches;

var load = fs.readFileSync('topo.js');
var topo;
eval(load.toString());

if (topo.spine == undefined)
{
	rootSwitches = {
                237: true,
                234: true,
                235: true,
                244: true,
                242: true,
                226: true,
                238: true,
                231: true,
                239: true,
                247: true,
	};
} else {
	rootSwitches = topo.spine;
}

//numer switches to give ID
var id = [0,0];
var switches = [];
for (var i in topo.links.switch)
{
	topo.links.switch[i].type = 'switch';
	if (rootSwitches[i]) {
		topo.links.switch[i].level = 0;
		topo.links.switch[i].id = id[0];
		id[0]++;
	} else {
		topo.links.switch[i].level = 1;
		topo.links.switch[i].id = id[1];
		id[1]++;
	}
	switches.push(topo.links.switch[i]);
}
//console.log(topo.links.switch);

//create nodes
var nodes = {};
var nodeList  = [];
for (var i in switches)
	nodes[switches[i].lid] = switches[i];
for (var i in switches)
{
	for (var p in switches[i].ports)
	{
		var port = switches[i].ports[p];
		if (nodes[port.lid] == undefined)
		{
			nodes[port.lid] = {type:"node", level: 3,name: port.name, lid: port.lid, ports:{1:{}}};
			nodeList.push(nodes[port.lid]);
		}
	}
}

//make contiguous
cleanNodes = [];
var c = 0;
for (var i in nodeList)
{
	if (nodeList[i].name.substring(0,3) == "nxt")
	{
		cleanNodes.push({id:c++, node:nodeList[i]});
		console.log(nodeList[i].name.split(' ')[0]);
	}
}
