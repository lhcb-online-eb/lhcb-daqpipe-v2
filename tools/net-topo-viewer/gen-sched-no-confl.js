/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

var fs = require('fs');

/********************  GLOBALS  *********************/
var nodeCnt = 64;
var steps = nodeCnt;
var topoScript = fs.readFileSync("topo2.js").toString();
eval(topoScript);

/********************  GLOBALS  *********************/
var rootSwitches;

if (topo.spine == undefined)
{
	rootSwitches = {
		"7": true,
		"5": true,
		"4": true,
	};
} else {
	rootSwitches = topo.spine;
}

/*******************  FUNCTION  *********************/
function getRemotePort(obj, lid)
{
	if (obj.type == 'node')
		return 1;
	for (var i in obj.ports)
		if (obj.ports[i].lid == lid)
			return i;
	console.log('error');
	return 0;
}

/*******************  FUNCTION  *********************/
function enableLinks(links,start,end,delta)
{
	var collide = false;
	if (delta == undefined)
		delta = 1;
	
	//find node link
	var startLink = null;
	for (var i in links)
	{
		if (links[i].dest.device.lid == start.lid)
		{
			startLink = links[i];
			break;
		}
	}

	//color it
	startLink.show = true;
	startLink.usedUp += delta;

	//jump to switch
	var cur = startLink.src.device;
	var curLevel = cur.level;
	var cnt = 0;
	while (cur.lid != end.lid && cnt++ < 10)
	{
		//select port
		//console.log(cur.lid);
		var port = topo.routes[cur.lid][end.lid];
		//console.log(end.lid);
		var link = cur.ports[port].link;
		if (link.src.device.lid == cur.lid)
			cur = link.dest.device;
		else
			cur = link.src.device;
		link.show = true;
		if (cur.level < curLevel)
			link.usedUp +=delta;
		else
			link.usedDown +=delta;
		if ((link.usedUp > 1 && cur.level < curLevel) || (link.usedDown > 1 && cur.level > curLevel))
		{
			//console.log("Collide "+start.id + " => " + end.id +" ; "+start.name+" => "+end.name);
			collide = true;
		}
		var curLevel = cur.level;
	}
	
	return collide;
}

/*******************  FUNCTION  *********************/
function barrel(nodes, node, step)
{
	return (node-step+3*nodes)%nodes
}

/*******************  FUNCTION  *********************/
function isAvail(step,comm)
{
	if (comm == null)
		return false;
	for (var i in step)
		if (i == comm.start ||step[i] == comm.target)
			return false;
	return true;
}

/*******************  FUNCTION  *********************/
function resetLinkRouting(links)
{
	for (var i in links)
	{
		links[i].show = false;
		links[i].usedUp = 0;
		links[i].usedDown = 0;
	}
}

/****************************************************/
//numer switches to give ID
var id = [0,0];
var switches = [];
for (var i in topo.links.switch)
{
	topo.links.switch[i].type = 'switch';
	if (rootSwitches[i]) {
		topo.links.switch[i].level = 0;
		topo.links.switch[i].id = id[0];
		id[0]++;
	} else {
		topo.links.switch[i].level = 1;
		topo.links.switch[i].id = id[1];
		id[1]++;
	}
	switches.push(topo.links.switch[i]);
}

/****************************************************/
//create nodes
var nodes = {};
var nodeList  = [];
for (var i in switches)
	nodes[switches[i].lid] = switches[i];
for (var i in switches)
{
	for (var p in switches[i].ports)
	{
		var port = switches[i].ports[p];
		if (nodes[port.lid] == undefined)
		{
			nodes[port.lid] = {type:"node", level: 3,name: port.name, lid: port.lid, ports:{1:{}}};
			nodeList.push(nodes[port.lid]);
		}
	}
}

/****************************************************/
//build links
var links = [];
for (var i in switches)
{
	for (var p in switches[i].ports)
	{
		if (switches[i].ports[p].link == undefined)
		{
			var link = {};
			link.src = {device:switches[i],port:p};
			var destLid = switches[i].ports[p].lid;
			var destPort = getRemotePort(nodes[destLid],switches[i].lid);
			//console.log(destPort);
			destPort = switches[i].ports[p].remotePort;
			link.dest = {device:nodes[destLid],port:destPort};
			//console.log(link);

			switches[i].ports[p].link = link;
			//console.log(destLid + " => " +destPort);
			nodes[destLid].ports[destPort].link = link;
			
			links.push(link);
		} else {
			//console.log("already done");
		}
	}
}

/****************************************************/
//clean node names
var cleanNodes = [];
for (var i = 0 ; i < 800 ; i++)
{
	var name;
	if (i >= 100)
		name = "node"+i;
	else if (i >= 10)
		name = "node0"+i;
	else
		name = "node00"+i;
	for (var j in nodes)
	{
		if (nodes[j].name.split(" ")[0] == name)
		{
			cleanNodes.push({id:i,node:nodes[j]});
			nodes[j].id = i;
		}
	}
}

/****************************************************/
//gen list
var lst = {};
var id = 0;
for (var step = 1 ; step < steps ; step++)
{
	for (var node = 0 ; node < nodeCnt ; node++)
	{
		lst[id] = {id:id,start:node,target:barrel(nodeCnt,node,step)};
		id++;
	}
}

/****************************************************/
//setup vars
var sched = [];
var step = {};

/****************************************************/
//gen steps
resetLinkRouting(links);
var cnt = 0;
while (id > 0)
{
	//reset links
	resetLinkRouting(links);
	
	//replay
	for (var i in step)
	{
		var nodeStart = cleanNodes[i].node;
		var nodeEnd = cleanNodes[step[i]].node;
		if (enableLinks(links,nodeStart,nodeEnd,1) == true)
			console.log("error");
	}
	
	//gen candidate list
	var candidate=[];
	for (var i in lst)
	{
		if (isAvail(step,lst[i]))
		{
			var nodeStart = cleanNodes[lst[i].start].node;
			var nodeEnd = cleanNodes[lst[i].target].node;
			if (enableLinks(links,nodeStart,nodeEnd,1) == false)
				candidate.push(lst[i]);
			enableLinks(links,nodeStart,nodeEnd,-1);
		}
	}
	
	//if has something or not
	if (candidate.length == 0)
	{
		sched.push(step);
		step = {};
		console.log("Done step "+sched.length+ " ("+cnt+")");
		cnt = 0;
	} else {
		//randomly select one
		var sel = Math.floor(Math.random() * candidate.length);
		var selected = candidate[sel];
		
		//apply
		step[selected.start] = selected.target;
		
		//remove in list
		lst[selected.id] = undefined;
		id--;
		cnt++;
	}
}

/****************************************************/
//dump
fs.writeFileSync("sched.js","customSched = "+JSON.stringify(sched, null,"\t")+";");
