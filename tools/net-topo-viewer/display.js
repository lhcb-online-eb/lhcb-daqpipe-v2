/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

var linkSize = 5;
var switchSize = 60;
var rootSwitches;

if (topo.spine == undefined)
{
	rootSwitches = {
		237: true,
		234: true,
		235: true,
		244: true,
		242: true,
		226: true,
		238: true,
		231: true,
		239: true,
		247: true,
		
	};
} else {
	rootSwitches = topo.spine;
}
if (topo.spine2 == undefined)
{
	rootSwitches2 = topo.spine2 = {};
	hasSwitch2 = false;
} else {
	rootSwitches2 = topo.spine2;
	hasSwitch2 = true;
}

function getRemotePort(obj, lid)
{
	if (obj.type == 'node')
		return 1;
	for (var i in obj.ports)
		if (obj.ports[i].lid == lid)
			return i;
	console.log('error');
	return 0;
}

function portCoord(device, port)
{
	if (device.type == 'switch') 
	{
		return {
			x: device.id *linkSize * switchSize + port * linkSize,
			y: device.level * 200 + 25
		}
	} else if (device.type == 'node') {
		return {
			x: device.ports[port].link.src.device.id  *linkSize * switchSize + device.ports[port].link.src.port * linkSize,
			y: device.level * 200 + 25
		}
	} else {
		alert('error');
	}
}

function linkCoord(link)
{
	var p1 = portCoord(link.src.device,link.src.port);
	var p2 = portCoord(link.dest.device,link.dest.port);
	var res = {
		x1:p1.x,
		y1:p1.y,
		x2:p2.x,
		y2:p2.y
	}
	return res;
}

function resetLinkRouting(links)
{
	for (var i in links)
	{
		links[i].show = false;
		links[i].usedUp = 0;
		links[i].usedDown = 0;
		links[i].moveUp = undefined;
		links[i].moveDown = undefined;
	}
}

var t = 0;

function enableLinks(links,start,end)
{
	//find node link
	var startLink = null;
	for (var i in links)
	{
		if (links[i].dest.device.lid == start.lid)
		{
			startLink = links[i];
			break;
		}
	}

	//color it
	startLink.show = true;
	startLink.usedUp ++;

	//jump to switch
	var cur = startLink.src.device;
	var curLevel = cur.level;
	var cnt = 0;
	while (cur.lid != end.lid && cnt++ < 10)
	{
		//select port
		//console.log(cur.lid);
		var port = topo.routes[cur.lid][end.lid];
		console.log(port);
		var c = cur;
		//console.log(end.lid);
		var link = cur.ports[port].link;
		if (link.src.device.lid == cur.lid)
			cur = link.dest.device;
		else
			cur = link.src.device;
		link.show = true;
		if (cur.level < curLevel)
			link.usedUp ++;
		else
			link.usedDown ++;
		if ((link.usedUp > 1 && cur.level < curLevel) || (link.usedDown > 1 && cur.level > curLevel))
		{
			if (cur.level < curLevel)
				link.moveUp = {lid:end.lid, sw:c};
			else
				link.moveDown = {lid:end.lid, sw:c, dest:cur};
			console.log("Collide "+start.id + " => " + end.id +" ; "+start.name+" => "+end.name);
			console.log("   with "+(cur.level < curLevel ? link.whichUp : link.whichDown));
//if (t++ == 2)
	return 10;
		} else {
			if (cur.level < curLevel)
				link.whichUp = start.id + " up=> " + end.id +" ; "+start.name+" => "+end.name;
			else
				link.whichDown = start.id + " do=> " + end.id +" ; "+start.name+" => "+end.name;
		}
		var curLevel = cur.level;
	}
}

var rebalance = [];
function rebalanceRouting(links)
{
	rebalance = [];
	for (var i in links)
	{
		var link = links[i];
		if (link.moveUp != undefined)
		{
			//search a non used link
			var lnk = null;
			for (var j in links)
			{
				if (links[j].usedUp == 0 && (links[j].src.device.lid == link.moveUp.sw.lid || links[j].dest.device.lid == link.moveUp.sw.lid))
				{
					links[j].usedUp++;
					lnk = links[j];
					break;
				}
			}
			
			//return
			if (lnk == null)
				continue;
			
			//compute port
			var port;
			if (link.dest.device.lid == link.moveUp.sw.lid)
				port = links[j].dest.port;
			else if (link.src.device.lid == link.moveUp.sw.lid)
				port = links[j].src.port;
			else
				alert('error');
			
			//rebalance
			rebalance.push({sw:link.moveUp.sw,lid:link.moveUp.lid,repl:port})
		}
	}
	//console.log(rebalance);
}

function rebalanceRoutingDown(links)
{
	rebalance = [];
	for (var i in links)
	{
		var link = links[i];
		if (link.moveDown != undefined)
		{
			//search a non used link
			var lnk = null;
			for (var j in links)
			{
				if (links[j].usedDown == 0 && (links[j].src.device.lid == link.moveDown.sw.lid || links[j].dest.device.lid == link.moveDown.sw.lid) && (links[j].src.device.lid == link.moveDown.dest.lid || links[j].dest.device.lid == link.moveDown.dest.lid))
				{
					links[j].usedDown++;
					lnk = links[j];
					break;
				}
			}
			
			//return
			if (lnk == null)
				continue;
			
			//compute port
			var port;
			if (link.dest.device.lid == link.moveDown.sw.lid)
				port = links[j].dest.port;
			else if (link.src.device.lid == link.moveDown.sw.lid)
				port = links[j].src.port;
			else
				alert('error');
			
			//rebalance
			rebalance.push({sw:link.moveDown.sw,lid:link.moveDown.lid,repl:port})
		}
	}
	//console.log(rebalance);
}

function applyBalancing()
{
	for (var i in rebalance)
		topo.routes[rebalance[i].sw.lid][rebalance[i].lid] = rebalance[i].repl;
}

function barrel(nodes, node, step)
{
	return (node-step+3*nodes)%nodes
}

function xor(nodes,node,step)
{
	return (node ^ step) %nodes;
}

function custom(nodes,node,step)
{
	if (customSched[step] == undefined)
		return undefined;
	return customSched[step][node];
}

var gblLinks;
var gblNodes;
var gblCleanNodes;
function calcCollisions(nodes,step,func)
{
	//clear
	resetLinkRouting(gblLinks);

	//run
	for (var i = 0 ; i < nodes ; i++)
	{
		var nodeStart = gblCleanNodes[i].node;
		var isEnd = func(nodes,i,step);//(i-delta+3*nodes)%nodes;
// 		var isEnd = func(nodes,gblCleanNodes[i].id,step);//(i-delta+3*nodes)%nodes;
		if (isEnd == undefined)
			continue;
		var nodeEnd = gblCleanNodes[isEnd].node;
// 		for (var j in gblCleanNodes)
// 		{
// 			if (isEnd == gblCleanNodes[j].id)
// 			{
// 				nodeEnd = gblCleanNodes[j].node;
// 				break;
// 			}
// 		}
		//console.log(isEnd + " => "+nodeEnd.name );
		if (enableLinks(gblLinks,nodeStart,nodeEnd) == 10)
			break;
	}

	//count collisions
	var colLinks = 0;
	var maxCol = 0;
	var mean = 0;
	var meanCnt = 0;
	for (var i in gblLinks)
	{
		var link = gblLinks[i];
		if (link.usedUp > maxCol)
			maxCol = link.usedUp;
		if (link.usedUp > 1)
			colLinks++;
		if (link.usedDown > maxCol)
			maxCol = link.usedDown;
		if (link.usedDown > 1)
			colLinks++;
		meanCnt += 2;
		mean += link.usedDown + link.usedUp;
		
	}

	//console.log("Cols ["+step+"] : "+maxCol +" ; " + colLinks);
	return {cols:colLinks,maxCol:maxCol,mean:mean/meanCnt};
}

function isAvail(step,comm)
{
	if (comm == null)
		return false;
	for (var i in step)
		if (i == comm.start ||step[i] == comm.target)
			return false;
	return true;
}

function genSchedule(nodes,steps)
{
	//gen list
	/*var lst = {};
	var id = 0;
	for (var step = 1 ; step < steps ; step++)
		for (var node = 0 ; node < nodes ; node++)
			lst[id++] = {start:node,target:barrel(nodes,node,step)};

	var sched = [];
	var step = {};
	var found = true;
	var count = 0;
	while (id > 0)
	{
		//search one which fit
		found = false;
		var selected = null;
		var c = 0;
		while(c < 100*10)
		{
			var i = Math.floor(Math.random() * id);
			var sel;
			var cnt = 0;
			for (var j in lst)
			{
				if (cnt == i)
				{
					sel = j;
					break;
				}
				cnt++;
			}
				
			if (isAvail(step,lst[sel]))
			{
				selected = lst[sel];
				lst[sel] = undefined;
				found = true;
				id--;
				break;
			}
			c++;
		}

		if (found)
			step[selected.start] = selected.target;
		count++;
		if (count == nodes || found == false)
		{
			sched.push(step);
			step = {};
			count = 0;
		}
	}

	return sched;*/
}

function run()
{
	//numer switches to give ID
	var id = [0,0,0];
	var switches = [];
	for (var i in topo.links.switch)
	{
		topo.links.switch[i].type = 'switch';
		base = 0;
		if (hasSwitch2)
			base = 1;
		if (rootSwitches2[i]) {
			topo.links.switch[i].level = 0;
			topo.links.switch[i].id = id[0];
			id[0]++;
		} else if (rootSwitches[i]) {
			topo.links.switch[i].level = base;
			topo.links.switch[i].id = id[base];
			id[base]++;
		} else {
			topo.links.switch[i].level = base+1;
			topo.links.switch[i].id = id[base+1];
			id[base+1]++;
		}
		switches.push(topo.links.switch[i]);
	}
	//console.log(topo.links.switch);

	var svg = d3.select("#main").append("svg")
			.attr("width",(Math.max(id[1],id[0])+2)*(linkSize*switchSize))
			.attr("height",500+base*200)
		.append("g");


	//draw switch
	svg.selectAll(".switch")
		.data(switches)
		.enter().append("rect")
			.attr("class","switch")
			.attr("x",function(d){return d.id * linkSize*switchSize;})
			.attr("y",function(d){return d.level * 200;})
			.attr("width",linkSize*48)
			.attr("height",50);
	//create nodes
	var nodes = {};
	var nodeList  = [];
	for (var i in switches)
		nodes[switches[i].lid] = switches[i];
	for (var i in switches)
	{
		for (var p in switches[i].ports)
		{
			var port = switches[i].ports[p];
			if (nodes[port.lid] == undefined)
			{
				nodes[port.lid] = {type:"node", level: base+1.8,name: port.name, lid: port.lid, ports:{1:{}}};
				nodeList.push(nodes[port.lid]);
			}
		}
	}
	

	//build links
	var links = [];
	for (var i in switches)
	{
		for (var p in switches[i].ports)
		{
			if (switches[i].ports[p].link == undefined)
			{
				var link = {};
				link.src = {device:switches[i],port:p};
				var destLid = switches[i].ports[p].lid;
				var destPort = getRemotePort(nodes[destLid],switches[i].lid);
				//console.log(destPort);
				destPort = switches[i].ports[p].remotePort;
				link.dest = {device:nodes[destLid],port:destPort};
				//console.log(link);

				switches[i].ports[p].link = link;
				console.log(destLid + " => " +destPort);
				if (nodes[destLid] == undefined)
					nodes[destLid] = {};
				if (nodes[destLid].ports[destPort] == undefined)
					nodes[destLid].ports[destPort] = {};
				nodes[destLid].ports[destPort].link = link;
				
				links.push(link);
			} else {
				console.log("already done");
			}
		}
	}

	//clean node names	
	var cleanNodes = [];
	for (var i = 0 ; i < 800 ; i++)
	{
		var name;
		if (i >= 100)
			name = "node"+i;
//			name = "nxt0"+i;
		else if (i >= 10)
			name = "node"+i;
//			name = "nxt00"+i;
		else
			name = "node"+i;
//			name = "nxt000"+i;
		for (var j in nodes)
		{
			if (nodes[j].name.split(" ")[0] == name)
			{
				cleanNodes.push({id:i,node:nodes[j]});
				nodes[j].id = i;
			}
		}
	}
	
	//make contiguous
 	cleanNodes = [];
 	var c = 0;
 	for (var i in nodeList)
 	{
 		if (nodeList[i].name.substring(0,2) == "bu" || nodeList[i].name.substring(0,2) == "ru" || nodeList[i].name.substring(0,3) == "skl")
 		//if (nodeList[i].name.substring(0,3) == "nxt")
 			cleanNodes.push({id:c++, node:nodeList[i]});
 	}
/*
	var hostfile = [ "nxt0219" , "nxt0217" , "nxt0226" , "nxt0220" , "nxt0218" , "nxt0215" , "nxt0113" , "nxt0225" , "nxt0114" , "nxt0216" ,
 "nxt0118" , "nxt0117" , "nxt0123" , "nxt0122" , "nxt0115" , "nxt0121" , "nxt0119" , "nxt0124" , "nxt0116" , "nxt0120" , "nxt0311" ,
"nxt0307" , "nxt0314" , "nxt0304" , "nxt0357" , "nxt0356" , "nxt0360" , "nxt0359" , "nxt0312" , "nxt0303" , "nxt0310" , "nxt0358" ,
"nxt0355" , "nxt0305" , "nxt0308" , "nxt0309" ,
"nxt0306" , "nxt0313" , "nxt0323" ,
 "nxt0321" ,  "nxt0329" , "nxt0322" , "nxt0330" , "nxt0324" , "nxt0103" , "nxt0107" , "nxt0105" , "nxt0106" , "nxt0108" , "nxt0104" , 
"nxt0101" , "nxt0237" , "nxt0241" ,"nxt0242" , "nxt0244" , "nxt0240" , "nxt0236" , "nxt0231" , "nxt0239" , "nxt0229" , "nxt0230" , 
"nxt0232" , "nxt0227" , "nxt0243" , 
"nxt0235" , "nxt0234" , "nxt0238" , "nxt0228" , "nxt0233" , "nxt0211" , "nxt0213" , "nxt0209"
	];
	cleanNodes = [];
       	var c = 0;
	for (var j in hostfile){
	       	for (var i in nodeList)
       		{
	               	if (nodeList[i].name.substring(0,7) == hostfile[j])
                	       	cleanNodes.push({id:c++, node:nodeList[i]});
       		}
	}
	console.log(cleanNodes);*/

	
	svg.append("text").attr("id","title1").attr("x",0).text(0);
	svg.append("text").attr("id","title2").attr("y",0).text(0);

	//display link
	svg.selectAll(".link")
		.data(links)
		.enter().append("line")
			.attr("x1",function(d){return linkCoord(d).x1;})
			.attr("x2",function(d){return linkCoord(d).x2;})
			.attr("y1",function(d){return linkCoord(d).y1;})
			.attr("y2",function(d){return linkCoord(d).y2;})
               		.attr("stroke-width", 1)
			.attr("stroke", "black")
			.on('click',function(d) {
				d3.selectAll("#title1")
					.attr("x",linkCoord(d).x1-10)
					.attr("y",linkCoord(d).y2+40)
					.text(d.dest.device.name);
				start.selected = false; 
				d = d.dest.device;
				d.selected = true;
				start = d;
				d3.selectAll("a")
					.attr("class",function(d) { return d.selected?"buttonSelected":"button";});
			})
			.on("mouseover", function(d) {
				d3.selectAll("#title2")
					.attr("x",linkCoord(d).x1-10)
					.attr("y",linkCoord(d).y2+20)
					.text(d.dest.device.name);
				d = d.dest.device;
				resetLinkRouting(links);
				enableLinks(links,start,d);
				svg.selectAll("line")
					.attr("stroke-width", function(d) {return d.show?4:1;})
					.attr("stroke",function(d){ return d.show?"red":"black"});
			});

	var start = nodeList[0];
	var buttons = d3.select('body')
		.data(nodeList)
		.enter().append("a")
			.text(function(d) {return d.name+ " " + d.lid;})
			.attr('class','button')
			.on('click',function(d) {
				start.selected = false; 
				d.selected = true;
				start = d;
				d3.selectAll("a")
					.attr("class",function(d) { return d.selected?"buttonSelected":"button";});
			})
			.on("mouseover", function(d) {
				resetLinkRouting(links);
				enableLinks(links,start,d);
				svg.selectAll("line")
					.attr("stroke-width", function(d) {return d.show?4:1;})
					.attr("stroke",function(d){ return d.show?"red":"black"});
			});


	var startClean = cleanNodes[0];
	var buttons = d3.select('body')
		.data(cleanNodes)
		.enter().append("a")
			.text(function(d) {return d.id;})
			.attr('class','button')
			.on('click',function(d) {
				d3.selectAll("#title1")
					.attr("x",linkCoord(d.node.ports[1].link).x1-10)
					.attr("y",linkCoord(d.node.ports[1].link).y2+40)
					.text(d.node.name);
				startClean.node.selected = false; 
				d.node.selected = true;
				startClean = d;
				d3.selectAll("a")
					.attr("class",function(d) { return d.node.selected?"buttonSelected":"button";});
			})
			.on("mouseover", function(d) {
				d3.selectAll("#title2")
					.attr("x",linkCoord(d.node.ports[1].link).x1-10)
					.attr("y",linkCoord(d.node.ports[1].link).y2+20)
					.text(d.node.name);
				resetLinkRouting(links);
				enableLinks(links,startClean.node,d.node);
				svg.selectAll("line")
					.attr("stroke-width", function(d) {return d.show?4:1;})
					.attr("stroke",function(d){ return d.show?"red":"black"});
			});

	console.log("==================================");
	for (var i in nodes)
		if (nodes[i].name.substr(0,3) == "skl" || nodes[i].name.substr(0,2) == "ru" || nodes[i].name.substr(0,2) == "bu")
		console.log(nodes[i].name.substr(0,6));
	console.log("==================================");
	
	gblNodes = nodes;
	gblCleanNodes = cleanNodes;
	gblLinks = links;

	for (var j = 172 ; j < 173 ; j++) {
	//customSched = genSchedule(64,65);
	var algo = barrel;
// 	var algo = custom;
	var nodes = j;
	var steps = nodes;
	
	var sum = 0;
	var confl = [];
	var max = 0;
	var mean = 0;
	var mean2 = 0;
	//for (var i = 0 ; i < steps ; i++)
	var i = 48;
	{
		var c = calcCollisions(nodes,i,algo);
		confl.push(c);
		sum += c.cols;
		if (c.maxCol > max)
			max = c.maxCol;
		mean += c.maxCol;
		mean2 += c.mean;
	}
	//console.log("=============== " + sum +" ======================");
	console.log(j+"\t"+sum+"\t"+max+"\t"+mean/steps+"\t"+mean2/steps);
	}
	
/*	var algo = custom;
	
	var sum = 0;
	var confl2 = [];
	for (var i = 0 ; i < steps ; i++)
	{
		var c = calcCollisions(nodes,i,algo);
		confl2.push(c);
		sum += c;
	}
	console.log("=============== " + sum +" ======================");
	
	calcCollisions(nodes,18,algo);*/

 	/*var sum = 0;
 	for (var i = 0 ; i < 172 ; i++)
 		sum += calcCollisions(172,i,algo);
 	var sum3 = sum;
 	console.log("=============== " + sum +" ======================");*/
 	
// 	sum = 0;
// 	for (var i = 0 ; i < 64 ; i++)
// 	{
// 		calcCollisions(64,i,algo);
// 		rebalanceRouting(gblLinks);
// // 		applyBalancing();
// // 		sum += calcCollisions(64,i,algo);
// // 		rebalanceRoutingDown(gblLinks);
// 	}
// 	var sum2 = sum;
// 	console.log("=============== " + sum +" ======================");
// 	applyBalancing();
// 	sum = 0;
// 	for (var i = 0 ; i < 64 ; i++)
// 		sum += calcCollisions(64,i,algo);
// 	console.log("=============== " + sum +" ====================== "+ sum2 + " === "+sum3);
// 	
// 	calcCollisions(64,18,algo);
// 	rebalanceRouting(gblLinks);
// 	applyBalancing();
// 	console.log("---------------------------");
// 	calcCollisions(64,18,algo);
// 	rebalanceRoutingDown(gblLinks);
// 	applyBalancing();
// 	console.log("---------------------------");
// 	calcCollisions(64,18,custom);

	svg.selectAll("line")
		.attr("stroke-width", function(d) {return (d.usedUp > 1 || d.usedDown > 1)?4:1;})
		.attr("stroke",function(d){ return (d.usedUp > 1 || d.usedDown > 1)?"red":(d.usedUp == 0 && d.usedDown == 0)?"blue":"black"});

	Highcharts.chart('container', {

		title: {
			text: 'Conflicts'
		},

		yAxis: {
			title: {
				text: 'Confilcts'
			}
		},
		legend: {
			layout: 'vertical',
			align: 'right',
			verticalAlign: 'middle'
		},

		plotOptions: {
			series: {
				pointStart: 2010
			}
		},

		series: [{
			name: 'Barrel',
			data: confl
		},{
			name: 'Custom',
			data: confl2
		}]
	});
}

