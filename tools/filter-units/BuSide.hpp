/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_FU_BU_SIDE_HPP
#define DAQ_FU_BU_SIDE_HPP

/********************  HEADERS  *********************/
#include <string>
#include <thread>
#include <mutex>
#include <functional>
#include "common/Config.hpp"
#include "common/Throughput.hpp"
#include "gochannels/GoChannel.hpp"
#include "FuProtocol.hpp"

/*******************  NAMESPACE  ********************/
namespace DAQFU
{

/********************  STRUCT  **********************/
/**
 * Define a job for the sending thread. A job is essentially a data segement
 * to send on the network and a segement idientifier (rail, Readout Unit ID, meta/data)
 * and size.
**/
struct FuJob
{
	/** Buffer to transmit. **/
	char * buffer;
	/** Info to send as protocol header to the recieveing node before sending the data **/
	FuProtSendData infos;
};

/********************  STRUCT  **********************/
/**
 * As we send multiple segments for every event we need to keep track of the progress.
 * This struct contain the counter to know where we are.
 * It also associate a callback lambda function to be called when sending is finished
 * for the related event.
**/
struct FuStatus
{
	/** Number of segment already sent **/
	int sent;
	/** Callblack to call when finish **/
	std::function<void()> callback;
};

/*********************  CLASS  **********************/
/**
 * Implement the Builder Unit side of the BU-to-FU protocole. This object should be embedded
 * into the builder unit.
**/
class FuBuSide
{
	public:
		FuBuSide(const DAQ::Config * config,int nbReadoutUnits, bool startThreads = true);
		~FuBuSide();
		void sendData(int credit,size_t eventId,bool meta,int ru,int segId,char * buffer,size_t size);
		void setCallback(int credit, int messages, std::function< void() > callback);
		void resetMessageCount(int credit,int messages);
		void wait();
		void startLocalThread(int id);
	protected:
		void startWorkerThreads();
		void stopWorkerThreads();
		void threadMain(int id);
	private:
		/** Keep track of config **/
		const DAQ::Config * config;
		/** Used to dipatch jobs to threads **/
		DAQ::GoChannel<FuJob> * jobs;
		/** Number of RU in the system to know how many segements we need to send per event **/
		int nbReadoutUnits;
		/** Keep track of progress and contain handler (have one per credit) **/
		FuStatus * status;
		/** Struct to handle running threads **/
		std::thread * threadHandler;
		/** To be set to false when stop running to exit threads **/
		bool running;
		/** Number of credits (how many data we send at same time). Should be multiple of threads (or equal) **/
		int credits;
		/** Total number of worker thread to use to send data over the network **/
		int threads;
		/** NUmber of threads per filter unit **/
		int localThreads;
		/** Mutex to protect acess to bw field from worker threads **/
		std::mutex mutex;
		/** Keep track of total bandwdith **/
		DAQ::Throughput bw;
		/** NUmber of filter units **/
		int fus;
};

}

#endif //DAQ_FU_BU_SIDE_HPP