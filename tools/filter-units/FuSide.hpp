/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQFU_FU_SIDE_HPP
#define DAQFU_FU_SIDE_HPP

/********************  HEADERS  *********************/
#include "FuProtocol.hpp"
#include "common/Config.hpp"
#include "transport/Transport.hpp"
#include "units/BuilderUnitData.hpp"
#include "writer/Writer.hpp"
#include "common/Throughput.hpp"
#include <semaphore.h>
#include <thread>
#include <mutex>

/********************  MACROS  **********************/
/** Maximum credits handled by the filter unit **/
#define DAQ_FU_MAX_CERDITS 32

/*******************  NAMESPACE  ********************/
namespace DAQFU
{

/*********************  CLASS  **********************/
/**
 * Define the filter unit side handler. It connect to the DAQPIPE
 * builder unit and fetch data from TCP protocol. The user has
 * to register a handler to be notified that an event arrive.
**/
class FuSide
{
	public:
		FuSide(DAQ::Config * config,DAQ::Transport * transport);
		~FuSide();
		void start();
		void setCallback(const std::function<void(int,size_t,DAQ::BuilderUnitData & meta,DAQ::BuilderUnitData & data)> & callback);
		void free(int id);
		DAQ::BuilderUnitData & getData(bool meta);
	protected:
		void startThreads();
		void stopThreads();
		void threadMain( std::string host, int id );
	private:
		/** pointer to the config **/
		DAQ::Config * config;
		/** pointer to the transport (to know how many builder readout we have **/
		DAQ::Transport * transport;
		/** Handler for internal recieving threads **/
		std::thread * threadHandler;
		/** Data storage **/
		DAQ::BuilderUnitData * data;
		/** Meta data storage **/
		DAQ::BuilderUnitData * meta;
		/** Writer responsible of the data conversion and file writing. **/
		DAQ::Writer * writer;
		/** Keep track of ow many segment we already recieved for a given credit (to know wgen finish)**/
		int recvCnt[DAQ_FU_MAX_CERDITS];
		/** Number of thread in use **/
		int threads;
		/** Number of credits in use **/
		int credits;
		/** Number of readout units **/
		int units;
		/** Enable file writing **/
		bool writeFile;
		/** Buffer to recive invalid data (in case of connection restarted in middle of something **/
		char * bufNull;
		/** Call back to call when an event finish to be recived **/
		std::function<void(int,size_t,DAQ::BuilderUnitData & meta,DAQ::BuilderUnitData & data)> callback;
		/** Event ids actually fetched, indexed by credit id **/
		size_t eventId[DAQ_FU_MAX_CERDITS];
		/** Protect global metrics like bw **/
		std::mutex mutex;
		/** Track global throughtput **/
		DAQ::Throughput bw;
		/** Semaphore to keep track of storage, also manage backpressure by making recever blocked **/
		sem_t storageSem;
		/** Base port to connect to **/
		int basePort;
};

}

#endif //DAQFU_FU_SIDE_HPP
