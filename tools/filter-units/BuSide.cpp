/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include <cstring>
#include "BuSide.hpp"
#include "portability/TcpHelper.hpp"
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <ifaddrs.h>
#include <netinet/in.h> 
#include <arpa/inet.h>
#include <signal.h>

/*******************  NAMESPACE  ********************/
namespace DAQFU
{

/*******************  FUNCTION  *********************/
/**
 * Constructor.
 * @param config Define the global config to consider.
 * @param nbReadoutUnits Define the number of readout unit (to know how many segments to send)
 * @param startThreads Automatically start the worker threads if set to true, do nothing otherwise.
**/
FuBuSide::FuBuSide( const DAQ::Config* config, int nbReadoutUnits, bool startThreads )
{
	//avoid being killed by sigpipe
	signal(SIGPIPE, SIG_IGN);
	
	//infos
	this->config = config;
	this->nbReadoutUnits = nbReadoutUnits;
	this->running = true;
	
	//extract config
	this->fus = config->getAndSetConfigInt("filterUnitTCP","filterUnits",1);
	this->threads = config->getAndSetConfigInt("filterUnitTCP","threads",4);
	this->localThreads = threads;
	this->credits = config->filterCredits;
	
	//real
	this->threads *= this->fus;
	
	if (this->threads%credits != 0)
		DAQ_WARNING("Filter credits should be muliple of total threads to well balance between them !");
	
	//allocate mem
	this->status = new FuStatus[credits];
	this->threadHandler = new std::thread[threads];
	this->jobs = new DAQ::GoChannel<FuJob>[threads];
	
	//start
	this->bw.start();
	if (startThreads)
		this->startWorkerThreads();
}

/*******************  FUNCTION  *********************/
/**
 * Destructor. Mostly stop the running worker threads.
**/
FuBuSide::~FuBuSide()
{
	this->stopWorkerThreads();
}

/*******************  FUNCTION  *********************/
/**
 * Start all the worker thread for the given filter unit (id).
 * To be used inside FilterUnitTCP to handle every unit separetly.
 * @param id Index of the filter unit for which to start the threads/
**/
void FuBuSide::startLocalThread ( int id )
{
	//compute start
	id *= localThreads;
	
	//create
	for (int i = 0 ; i < localThreads ; i++)
	{
		this->threadHandler[id+i] = std::thread([=](){
			this->threadMain(id+i);
		});
	}
}

/*******************  FUNCTION  *********************/
/**
 * Start all the local threads.
 * To be used in main-daq test executable.
**/
void FuBuSide::startWorkerThreads()
{
	//create
	for (int i = 0 ; i < threads ; i++)
	{
		this->threadHandler[i] = std::thread([=](){
			this->threadMain(i);
		});
	}
}

/*******************  FUNCTION  *********************/
/**
 * Stop all the running threads.
**/
void FuBuSide::stopWorkerThreads()
{
	//mark stop
	this->running = false;
	
	//wait
	/** @todo handle case of BU integration where not all the threads are running **/
	for (int i = 0 ; i < threads ; i++)
	{
		jobs[i].close();
		threadHandler[i].join();
	}
}

/*******************  FUNCTION  *********************/
/**
 * Wait all the pending threads
**/
void FuBuSide::wait ( )
{
	//wait
	for (int i = 0 ; i < threads ; i++)
		threadHandler[i].join();
}

/*******************  FUNCTION  *********************/
/**
 * Set a new callback for the given credit.
 * @param messages Number of message to send
 * @param credit Credit to assign.
 * @param callback Callback function to call when finish
**/
void FuBuSide::setCallback(int credit,int messages, std::function< void ()> callback)
{
	status[credit].sent = messages;
	status[credit].callback = callback;
}

/*******************  FUNCTION  *********************/
/**
 * Similar to setCallback but just reset the state counter and do not set again the callback.
 * @param messages Number of message to send
 * @param credit Credit to assign.
**/
void FuBuSide::resetMessageCount(int credit,int messages)
{
	status[credit].sent = messages;
}

/*******************  FUNCTION  *********************/
/**
 * Push data into the work queue.
 * @param credit Credit to assign the data sending to.
 * @param eventId Set event ID of data to detect resetart or recieving side.
 * @param meta Define if segment is meta or data
 * @param ru Define readout unit id
 * @param rail Define rail ID.
 * @param buffer Define data to send.
 * @param size Define size of data to send.
**/
void FuBuSide::sendData(int credit, size_t eventId, bool meta, int ru, int rail, char* buffer, size_t size)
{
	//setup job
	FuJob job;
	job.infos.magick = DAQ_FU_MAGICK;
	job.infos.creditId = credit;
	job.infos.eventId = eventId;
	job.infos.meta = meta;
	job.infos.ru = ru;
	job.infos.rail = rail;
	job.infos.size = size;
	job.buffer = buffer;
	
	//push
	job >> jobs[credit%threads];
}

/*******************  FUNCTION  *********************/
/**
 * Main function of worker thread. It mostly consist in a sending loop.
 * This function also handle disconnection/reconnection.
**/
void FuBuSide::threadMain(int id)
{
	//vars
	struct sockaddr_storage ss;
	socklen_t slen = sizeof(ss);
	int fd = -1;
	//DAQ::Throughput bw;
	//bw.start();

	
	//get base port
	int basePort = config->getAndSetConfigInt("filterUnitTCP","basePort",9160);
	
	//open listening socket
	DAQ_INFO_ARG("Open listen port %1").arg(id).end();
	int lstSock = DAQ::TcpHelper::makeListeningSocket(basePort+id);
	
	//loop
	while(running)
	{
		FuJob job;
		while (job << jobs[id])
		{
			//reconnect
			if (fd == -1)
			{
				DAQ_INFO("Reconnect !");
				//mutex.lock();
				fd = accept(lstSock, (struct sockaddr*)&ss, &slen);
				//mutex.unlock();
				DAQ_INFO("Connected !");
			}
			
			//send header
			bool ret = DAQ::TcpHelper::safeSend(fd,&job.infos,sizeof(job.infos),true);
			if (ret == false)
			{
				DAQ_ERROR_ARG("Loose FU connection for credit %1 while sending header").arg(id).end();
				fd = -1;
			}	
			
			//send send data
			if (fd != -1)
			{
				ret = DAQ::TcpHelper::safeSend(fd,job.buffer,job.infos.size,true);
				if (ret == false)
				{
					DAQ_ERROR_ARG("Loose FU connection for credit %1 while sending data").arg(id).end();
					fd = -1;
				}
			}
			
			//count
			mutex.lock();
				//if elapsed need print
				bw.reg(1,job.infos.size);
				if (bw.getElapsedTime() > 2.0 && id == 0) {
					DAQ_MESSAGE_ARG("[MON] rates : %1").arg(bw).end();
					bw.restart();
				}
			
				//count down
				status[job.infos.creditId].sent--;
				if (status[job.infos.creditId].sent == 0)
					status[job.infos.creditId].callback();
			mutex.unlock();
		}
	}
}

}
