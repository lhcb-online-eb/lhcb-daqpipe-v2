/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <ifaddrs.h>
#include <netinet/in.h> 
#include <arpa/inet.h>
#include <fcntl.h>
#include "common/Debug.hpp"
#include "transport/UnitType.hpp"
#include "portability/TcpHelper.hpp"
#include "datasource/DataSource.hpp"
#include "FuSide.hpp"

/*******************  NAMESPACE  ********************/
namespace DAQFU
{

/*******************  FUNCTION  *********************/
/**
 * Constrcutor. It init all the internal states and spwan the internal threads.
 * @param config Define the global configuration.
 * @param transport Define the transport layer to know how many readout unit we have.
**/
FuSide::FuSide ( DAQ::Config* config, DAQ::Transport* transport )
{
	//setup
	this->config = config;
	this->transport = transport;
	this->credits = config->filterCredits;
	this->writeFile = config->getAndSetConfigBool("general","writeFile",false);
	this->threads = config->getAndSetConfigInt("filterUnitTCP","threads",4);
	this->units = transport->countUnits(DAQ::UNIT_READOUT_UNIT);
	this->basePort = config->getAndSetConfigInt("filterUnitTCP","basePort",9160);
	int storage = config->getAndSetConfigInt("filterUnitTCP","storage",8);
	
	//check
	assume(credits % threads == 0,"Credits should be a multiple of thread number to well balance the ressource usage !");
	assumeArg(credits <= DAQ_FU_MAX_CERDITS,"Too many credits for filter unit : %1, max is %2").arg(credits).arg(DAQ_FU_MAX_CERDITS).end();
	assume(storage > credits,"Filter storage should be at least equal to credits !");
	if (storage < credits*2)
		DAQ_WARNING("Ideally you should have storage at least equal to two times the credits for performance !");
	
	//allocate
	for (int i = 0 ; i < credits ; i++)
		eventId[i] = -1;
		
	//setup storage
	this->data = new DAQ::BuilderUnitData(config->eventRailMaxDataSize,config->filterCredits,storage,this->units,transport);
	this->meta = new DAQ::BuilderUnitData(config->getDefaultMetaDataSize(),config->filterCredits,storage,this->units,transport);
	this->writer = new DAQ::Writer(*config,this->units,meta,data);
	
	//fill
	//(use -1 because storage = 0 is used for backpressure in BuilderUnitData into BU)
	sem_init(&storageSem,0,storage-1);
	
	//allocate
	this->bufNull = new char[config->eventRailMaxDataSize];
	
	//start
	bw.start();
	
	//assign credits
	for (int i = 0 ; i < credits ; i++)
	{
		sem_wait(&storageSem);
		assume(meta->assign(i),"Failed to assign credit !");
		assume(data->assign(i),"Failed to assign credit !");
	}
}

/*******************  FUNCTION  *********************/
FuSide::~FuSide ( )
{
}

/*******************  FUNCTION  *********************/
/**
 * Free the storage ID such we can reuse it to fetch next data.
 * @param id Storage ID to free.
**/
void FuSide::free ( int id )
{
	mutex.lock();
		meta->free(id);
		data->free(id);
	mutex.unlock();
	sem_post(&storageSem);
}

/*******************  FUNCTION  *********************/
/**
 * get access to the meta/data container.
 * @param meta Define if you want the data or meta-data.
**/
DAQ::BuilderUnitData& FuSide::getData ( bool meta )
{
	if (meta)
		return *(this->meta);
	else
		return *(this->data);
}

/*******************  FUNCTION  *********************/
/**
 * Start all the internal threads
**/
void FuSide::startThreads ( )
{
	//get host
	Json::Value & hosts = config->getConfig("filterUnitTCP.hosts");
	assume(hosts.isArray(),"filterUnitTCP.hosts field must be an array !");
	int cnt = (int)hosts.size();
	
	//alloc
	threadHandler = new std::thread[cnt*threads];
	
	//create
	for (int j = 0 ; j < cnt ; j++) {
		//extract
		std::string host = hosts[j].asString();
		int start = j * threads;
		
		//spawn threads
		for (int i = 0 ; i < threads ; i++)
		{
			threadHandler[start+i] = std::thread([=](){
				threadMain(host,i);
			});
		}
	}
}

/*******************  FUNCTION  *********************/
/**
 * Setup callback to be called when an event have been recieved. When called, the callback provide
 * the storage id, the event and data/meta-data handler to access the data.
 * When called, the user need to free the data/meta-data storage to make it available for fetching next
 * event. 
 * 
 * Caution this callback is called into threads.
**/
void FuSide::setCallback(const std::function<void(int,size_t,DAQ::BuilderUnitData & meta,DAQ::BuilderUnitData & data)> & callback)
{
	this->callback = callback;
}

/*******************  FUNCTION  *********************/
/**
 * Stop and wait all the pending threads for exit
**/
void FuSide::stopThreads ( )
{
	for (int i = 0 ; i < threads ; i++)
		threadHandler[i].join();
	delete [] threadHandler;
}

/*******************  FUNCTION  *********************/
/**
 * Start all the threads and wait exit. This function is blocking.
**/
void FuSide::start ( )
{
	//start threads
	this->startThreads();
	
	//join
	for (int i = 0 ; i < threads ; i++)
		threadHandler[i].join();
}

/*******************  FUNCTION  *********************/
/**
 * Establish connection to the given ip and port.
 * @param ip Define the ip to connect to
 * @param port Define the port to connect to.
**/
static int doConnect(const std::string & ip,int port)
{
	struct sockaddr_in sin;
	int fd;
	int status;
	
	/* Allocate a new socket */
	bool useIpV6 = false;
	fd = socket( useIpV6 ? AF_INET6 : AF_INET , SOCK_STREAM, 0);
	if (fd<0)
	{
		DAQ_ERROR_ARG("Error while calling socket() : %1").argStrErrno().end();
		return -1;
	}
	
	/* Prepare connection parameters. */
	sin.sin_family = useIpV6 ? AF_INET6 : AF_INET;
	sin.sin_port = htons(port);
	inet_aton(ip.c_str(), &sin.sin_addr);
	DAQ_DEBUG_ARG("tcp","Try to connect to %1:%2").arg(ip).arg(sin.sin_port).end();
	
	/* Connect to the remote host. */
	status = connect(fd, (struct sockaddr*) &sin, sizeof(sin));
	if(status != 0)
	{
		DAQ_ERROR_ARG("Error while calling connect() : %1").argStrErrno().end();
		close(fd);
		fd = -1;
	}
	
	return fd;
}

/*******************  FUNCTION  *********************/
/**
 * Main function of threads. If connect and start to fetch data.
 * In case of disconnection, it automatically try to reconnect until
 * it succeed.
**/
void FuSide::threadMain ( std::string host, int id )
{
	//run
	int fd = -1;
	while(true)
	{
		//reconnect
		if (fd == -1)
		{
			DAQ_INFO_ARG("Connect to id %1:%2").arg(host).arg(transport->getRank() * threads + id).end();
			fd = doConnect(DAQ::TcpHelper::getHostIp(host),basePort+transport->getRank() * threads + id);
			if (fd == -1)
			{
				sleep(1);
				continue;
			} else {
				DAQ_INFO("Ok connected !");
			}
		}
		
		//fetch header
		FuProtSendData header;
		//DAQ_INFO("Recv data!");
		bool status = DAQ::TcpHelper::safeRecv(fd,&header,sizeof(header),true);
		if (status == false)
		{
			DAQ_ERROR_ARG("Loose BU connection for credit %1").arg(id).end();
			fd = -1;
			continue;
		}
		
		//check
		assume(header.magick == DAQ_FU_MAGICK,"Invalid Magick value from protocol !");
		
		//if different event
		mutex.lock();
		if (eventId[header.creditId] != header.eventId)
		{
			if (recvCnt[header.creditId] != 0)
				DAQ_WARNING_ARG("Skip event for credit %1, event %2 due to new event data which override it : %3").arg(header.creditId).arg(eventId[header.creditId]).arg(header.eventId).end();
			eventId[header.creditId] = header.eventId;
			recvCnt[header.creditId] = 0;
		}
		mutex.unlock();
		
		//get data
		char * buffer;
		if (meta->getCreditId(header.creditId) == -1)
			buffer = bufNull;
		else if (header.meta)
			buffer = meta->getCreditPtr(header.creditId,header.rail,header.ru);
		else
			buffer = data->getCreditPtr(header.creditId,header.rail,header.ru);
		
		//read
		//DAQ_INFO("Recv real data!");
		status = DAQ::TcpHelper::safeRecv(fd,buffer,header.size,true);
		if (status == false)
		{
			DAQ_ERROR_ARG("Loose BU connection for credit %1").arg(id).end();
			fd = -1;
			continue;
		}
		
		//inc
		mutex.lock();
		assume(eventId[header.creditId] == header.eventId,"event has change while fetching !");
		recvCnt[header.creditId]++;
		
		//if elapsed need print
		bw.reg(1,header.size);
		//DAQ_INFO("compute bw");
		if (bw.getElapsedTime() > 2.0) {
			DAQ_MESSAGE_ARG("[MON] rates : %1").arg(bw).end();
			bw.restart();
		}
		
		//check full
		if (recvCnt[header.creditId] == 2 * units * DAQ_DATA_SOURCE_RAILS)
		{
			//infos
			//DAQ_INFO_ARG("Event finish : %1").arg(header.creditId).end();
			
			//get pos
			int id = meta->getCreditId(header.creditId);
			
			//unassign
			if (id != -1)
			{
				meta->unassign(header.creditId,false);
				data->unassign(header.creditId,false);
			}
			
			//reset
			recvCnt[header.creditId] = 0;
			
			//locck
			mutex.unlock();
			
			//callbacl
			if (id == -1)
				DAQ_WARNING_ARG("Skip event %1 due to storage missing").arg(eventId[header.creditId]).end();
			else
				this->callback(id,header.eventId,*meta,*data);
			
			//reassign, we use a go channel so it automatically wait if all are used
			sem_wait(&storageSem);
			mutex.lock();
				assume(meta->assign(header.creditId),"Failed to assign ID for meta !");
				assume(data->assign(header.creditId),"Failed to assign ID for data !");
			mutex.unlock();
		} else {
			mutex.unlock();
		}
	}
}

}
