Extenral filter unit
====================

This directory contain the implementation of an external filter unit to
get data from DAQPIPE via TCP.

Configuration
-------------

It should use the same config file than daqpipe withe interesting entries :

```json
{
	"general": {
		"writeFile": false,
		"credits": 4
	},
	"filterUnitTCP": {
		"basePort": 9600,
		"portRange": 128,
		"threads": 4,
		"hosts": [ "localhost" ],
		"daqNodes": 2,
		"storage": 16
	}
}
```
