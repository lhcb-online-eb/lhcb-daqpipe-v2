######################################################
#            PROJECT  : lhcb-daqpipe                 #
#            VERSION  : 2.5.0-dev                    #
#            DATE     : 12/2017                      #
#            AUTHOR   : Valat Sébastien  - CERN      #
#            LICENSE  : CeCILL-C                     #
######################################################

######################################################
include_directories(${CMAKE_SOURCE_DIR}/extern-deps/jsoncpp/dist)
include_directories(${CMAKE_SOURCE_DIR}/src)
include_directories(${CMAKE_BINARY_DIR}/src)

######################################################
find_package(Htopml QUIET)
if (HTOPML_FOUND)
	set(OPTIONAL_EXTRA $<TARGET_OBJECTS:daqpipe-htopml-plugins>)
	set(HAVE_HTOPML yes)
	include_directories(${HTOPML_INCLUDE_DIRS})
	list(APPEND DAQPIPE_EXTERN_LIBS ${HTOPML_LIBRARY})
endif (HTOPML_FOUND)

######################################################
find_package(LhcbPcie40 QUIET)
if(LHCB_PCIE40_FOUND)
	set(HAVE_PCIE40 yes)
	include_directories(${LHCB_PCIE40_INCLUDE_DIRS})
	list(APPEND DAQPIPE_EXTERN_LIBS ${LHCB_PCIE40_LIBRARY})
endif(LHCB_PCIE40_FOUND)

######################################################
add_library(daqpipe-fu-tcp OBJECT BuSide.cpp)
add_library(fu STATIC main-daq.cpp BuSide.cpp FuSide.cpp $<TARGET_OBJECTS:daqpipe-common> $<TARGET_OBJECTS:daqpipe-gochannels> $<TARGET_OBJECTS:daqpipe-portability-net> $<TARGET_OBJECTS:daqpipe-datasource-amc40-gen> $<TARGET_OBJECTS:daqpipe-writer>)
target_link_libraries(fu libjsoncpp ${DAQPIPE_EXTERN_LIBS})

######################################################
set(TRANSPORT_COMPONENTS $<TARGET_OBJECTS:daqpipe-topology> 
                         $<TARGET_OBJECTS:daqpipe-transport>
                         $<TARGET_OBJECTS:daqpipe-units>
                         $<TARGET_OBJECTS:daqpipe-gather>
                         $<TARGET_OBJECTS:daqpipe-datasource>
                         $<TARGET_OBJECTS:daqpipe-drivers-local>
                         $<TARGET_OBJECTS:daqpipe-datasource-amc40>
                         $<TARGET_OBJECTS:daqpipe-launcher-fake>
                         ${OPTIONAL_EXTRA})

######################################################
add_executable(filter-unit main-fu.cpp ${TRANSPORT_COMPONENTS}) 
target_link_libraries(filter-unit fu libjsoncpp ${DAQPIPE_EXTERN_LIBS})

######################################################
add_executable(daq-sim main-daq.cpp ${TRANSPORT_COMPONENTS}) 
target_link_libraries(daq-sim fu libjsoncpp ${DAQPIPE_EXTERN_LIBS})