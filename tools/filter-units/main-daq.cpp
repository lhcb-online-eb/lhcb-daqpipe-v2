/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include "launcher/LauncherFake.hpp"
#include "topology/Topology.hpp"
#include "transport/TransportMT.hpp"
#include "units/BuilderUnitData.hpp"
#include "BuSide.hpp"

/*******************  NAMESPACE  ********************/
using namespace DAQ;
using namespace DAQFU;

/*******************  FUNCTION  *********************/
/**
 * Send the meta/data over the network to filter unit
 * @param handler Handler to be used to send data over TCP connection.
 * @param transport Define transport layer to count readout units
 * @param meta Define meta data container to know what to send
 * @param data Define data container to know what to send
 * @param credit Define the credit to use to send the given data
 * @param eventId Define the event ID to send (to fill protocol header struct)
**/
void sendData(FuBuSide & handler,Transport & transport,BuilderUnitData & meta,BuilderUnitData & data,int credit,size_t eventId)
{
	int units = transport.countUnits(DAQ::UNIT_READOUT_UNIT);
	for (int unit = 0 ; unit < units ; unit++) {
		for (int rail = 0 ; rail < DAQ_DATA_SOURCE_RAILS ; rail++) {
			handler.sendData(credit,eventId,true,unit,rail,meta.getIdPtr(credit,rail,unit),meta.getEventSize());
			handler.sendData(credit,eventId,false,unit,rail,data.getIdPtr(credit,rail,unit),data.getEventSize());
		}
	}
}

/*******************  FUNCTION  *********************/
int main(int argc,const char ** argv)
{
	//load config
	Config config;
	config.parseArgs(argc,argv,true);
	
	//extract params
	int nodes = config.getAndSetConfigInt("filterUnitTCP","daqNodes",-1);
	int credits = config.filterCredits;
	assume(nodes >= 0, "Invalid node number, check filterUnit.daqNodes in your config file !");
	
	//local map
	config.localMap = new bool[nodes];
	for (int i = 0 ; i < nodes ; i++)
		config.localMap[i] = false;
	
	//override rank
	config.launcherRank = 0;
	
	//setup topology
	LauncherFake launcher(&config,nodes);
	DriverLocal driver;
	TransportMT transport(&config,&launcher,&driver);
	Topology::setupTopology(transport,config,false);
	
	//instantiate units
	transport.buildLocalUnits();
	
	//build storage
	BuilderUnitData data(config.eventRailMaxDataSize,credits,config.filterCredits,transport.countUnits(DAQ::UNIT_READOUT_UNIT),&transport);
	BuilderUnitData meta(config.getDefaultMetaDataSize(),credits,config.filterCredits,transport.countUnits(DAQ::UNIT_READOUT_UNIT),&transport);
	
	//handler
	FuBuSide handler(&config,transport.countUnits(DAQ::UNIT_READOUT_UNIT));
	
	//send in loop
	size_t messages = 2 * transport.countUnits(DAQ::UNIT_READOUT_UNIT) * DAQ_DATA_SOURCE_RAILS;
	size_t eventId = 1;
	std::mutex mutex;
	for (int i = 0 ; i < credits ; i++) {
		meta.assign(i);
		data.assign(i);
		handler.setCallback(i,messages,[&,i,credits](){
			mutex.lock();
			//DAQ_INFO("resend");
			handler.resetMessageCount(i,messages);
			sendData(handler,transport,meta,data,i,eventId++);
			mutex.unlock();
		});
	}

	//send fir stdata
	for (int i = 0 ; i < credits ; i++) {
		sendData(handler,transport,meta,data,i,eventId++);
	}
	
	//wait threads indefinitly
	handler.wait();
	
	//exit
	return EXIT_SUCCESS;
}
