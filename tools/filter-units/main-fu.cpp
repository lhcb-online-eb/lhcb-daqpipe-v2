/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include "launcher/LauncherFake.hpp"
#include "topology/Topology.hpp"
#include "transport/TransportMT.hpp"
#include "units/BuilderUnitData.hpp"
#include "writer/Writer.hpp"
#include "FuSide.hpp"
#include "gochannels/GoChannel.hpp"
#include <unistd.h>

/*******************  NAMESPACE  ********************/
using namespace DAQ;
using namespace DAQFU;

/********************  STRUCT  **********************/
/**
 * Protocol to sync the writer and filter unit fetchers.
**/
struct Event
{
	/** Define stoage id in data/meta **/
	int storageId;
	/** Define event id **/
	uint64_t eventId;
};

/*******************  FUNCTION  *********************/
/**
 * Main function of writer thread, used to dump the input meta/data into files.
 * @param config Define the config objet.
 * @param transport Define the transport layer (to know how many RU)
 * @param handler Define the filter unit handler to access meta/data
 * @param queue Define the input queue to fetch complete event ids.
**/
void writerMain(Config & config, Transport & transport, FuSide & handler, GoChannel<Event> & queue)
{
	DAQ::BuilderUnitData * meta = &(handler.getData(true));
	DAQ::BuilderUnitData * data = &(handler.getData(false));
	DAQ::Writer writer(config,transport.countUnits(UNIT_READOUT_UNIT),meta,data);
	
	Event cur;
	while (cur << queue)
	{
		writer.flush(cur.storageId,[&handler,cur]{
			handler.free(cur.storageId);
			//DAQ_INFO("Event flushed to file!");
		});
	}
}

/*******************  FUNCTION  *********************/
/**
 * Function used to check data after fetching from BU.
 * @param config Define global config.
 * @param transport Define transport layer to know how many RU we have
 * @param meta Define meta data container.
 * @param data Define data container.
 * @param storageId Define entry to check.
 * @param eventId Define event ID to knwo which values to expect.
**/
void doDataChecking (Config * config,Transport * transport,BuilderUnitData * meta, BuilderUnitData * data,int storageId,uint64_t eventId)
{
	BucketInfo bucket;
	int units = transport->countUnits(UNIT_READOUT_UNIT);
	
	for (int unit = 0 ; unit < units ; unit++) {
		bucket.eventId = eventId;
		bucket.id = 0;
		for (int rail = 0 ; rail < DAQ_DATA_SOURCE_RAILS ; rail++) {
			bucket.dataFragments[rail].base = data->getIdPtr(storageId,rail,unit);
			bucket.dataFragments[rail].size = 0;
			bucket.metaFragments[rail].base = meta->getIdPtr(storageId,rail,unit);
			bucket.metaFragments[rail].size = 0;
		}
		DAQ_DEBUG_ARG("datasource"," Gather::doDataChecking() unitId %1").arg(unit).end();
		bucket.check(*config,unit);
	}
}

/*******************  FUNCTION  *********************/
/**
 * Main function of data checking to validate full data flow. Used if the user
 * enable `testing.dataChecking` in config file.
 * @param config Define the config objet.
 * @param transport Define the transport layer (to know how many RU)
 * @param handler Define the filter unit handler to access meta/data
 * @param queue Define the input queue to fetch complete event ids.
**/
void checkerMain(Config & config, Transport & transport, FuSide & handler, GoChannel<Event> & queue)
{
	DAQ::BuilderUnitData * meta = &(handler.getData(true));
	DAQ::BuilderUnitData * data = &(handler.getData(false));
	
	Event cur;
	while (cur << queue)
	{
		doDataChecking(&config,&transport,meta,data,cur.storageId,cur.eventId);
		handler.free(cur.storageId);
	}
}

/*******************  FUNCTION  *********************/
int main(int argc,const char ** argv)
{
	//load config
	Config config;
	config.parseArgs(argc,argv,true);
	
	//extract params
	#ifdef NDEBUG
		bool check = false;
	#else
		bool check = true;
	#endif
	check = config.getAndSetConfigBool("testing","dataChecking",check);
	int nodes = config.getAndSetConfigInt("filterUnitTCP","daqNodes",-1);
	//int credits = config.filterCredits;
	assume(nodes >= 0, "Invalid node number, check filterUnit.daqNodes in your config file !");
	
	//local map
	config.localMap = new bool[nodes];
	for (int i = 0 ; i < nodes ; i++)
		config.localMap[i] = false;
	
	//setup topology
	LauncherFake launcher(&config,nodes);
	DriverLocal driver;
	TransportMT transport(&config,&launcher,&driver);
	Topology::setupTopology(transport,config,false);
	
	//instantiate units
	transport.buildLocalUnits();
	
	//handler
	FuSide handler(&config,&transport);
	
	//send in loop
	//size_t messages = 2 * transport.countUnits(DAQ::UNIT_READOUT_UNIT) * DAQ_DATA_SOURCE_RAILS;
	
	//start writer
	bool writer = config.getAndSetConfigBool("general","writeFile",false);
	GoChannel<Event> queue;
	if (writer || check)
	{
		std::thread th([&]{
			if (writer)
				writerMain(config,transport,handler,queue);
			else if (check)
				checkerMain(config,transport,handler,queue);
			else
				DAQ_FATAL("Invalid setup !");
		});
		th.detach();
	}
	
	//event
	handler.setCallback([&handler,&queue,writer,check](int id,size_t eventId,DAQ::BuilderUnitData & meta,DAQ::BuilderUnitData & data) {
		//DAQ_INFO_ARG("Finish event %1").arg(eventId).end();
		//sleep(1);
		struct Event evt = {id,eventId};
		if (writer || check)
			queue << evt;
		else
			handler.free(id);
	});

	//wait threads indefinitly
	handler.start();
	
	//exit
	return EXIT_SUCCESS;
}
