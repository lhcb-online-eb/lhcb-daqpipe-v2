/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/


#ifndef DAQ_FU_PROTOCOL_HPP
#define DAQ_FU_PROTOCOL_HPP

/********************  MACROS  **********************/
#define DAQ_FU_MAGICK 0x32

/********************  STRUCT  **********************/
/**
 * Define the header structure to be used to sync the builder and filter unit link
 * It mostly define the data IDs to know where to store the data and provide
 * event ID to detect reconnection and trash the data if event ID changed between
 * the last connection.
**/
struct FuProtSendData
{
	/** Magik number to check if we have to packet header and detect lost of sync. **/
	size_t magick;
	/** Credit ID used to store the data**/
	size_t creditId;
	/** Event ID to detect resync **/
	size_t eventId;
	/** If is meta or data **/
	bool meta;
	/** readout unit ID **/
	int ru;
	/** Rail ID **/
	int rail;
	/** Size of the data to be send/recieve **/
	size_t size;
};

#endif //DAQ_FU_PROTOCOL_HPP