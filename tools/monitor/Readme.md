Monitor
=======

The monitor server can be used to extract some events (eg. every 1000) from the pipeline and send them
to the monitor server. In this server it is possible (the current main is just an example) to 
fetch the data then build histogram from them.

Config
------

To be used you need to setup in the config file :

```json
	{
		"general": {
				"writeFile": true
		},
		"monitor": {
				"ipv6": false,
				"clients": 2,
				"ip": "127.0.0.1"
		},
		"filter": {
				"mode": "local"
		},
		"writer": {
				"meps": 10
		},
		"testing": {
			"skipMemcpy": false
		}
	}
```

Take care, the event routing is done in the file writer to you need to enable file writing. You also
need to precice the number of clients you want to run.

Run
---

To run, first start the server :

```sh
	./tools/monitor/monitor config.json
```

Then run daqpipe (you need to run after starting the monitor) : 

```sh
	mpirun -np 2 ./src/daqpipe -f config.json -t 100
```

Hook
----

The monotor server is just a library hence, you can start the server providing a hook to be called
for each captured event :

```cpp
	#include <cstdlib>
	#include <monitor/Monitor.hpp>
	
	int main(int argc, char ** argv) 
	{
		//run
		DAQ::runDAQMonitor(argv[1],[&](DAQ::EventHandler & handler){
			cout << "Get event " << handler.getEventId() << endl;
		});
		
		return EXIT_SUCCESS;
	}
```

From the event handler, you can request the various information using the next member funcitons.
Remark that you can get each information for a particular *unit id* (from 0 to the number of ReadoutUnits
in use in daqpipe). You also need to provide the rail which correspond to the channel for the PCIe-40
board (2 channels). With the AMC-40, data are in channel 0 and TFC in channel 1.

```cpp
	//get event ID
	handler.getEventId();
	
	//get data from unit 0 and rail 0 (return a pointer to it)
	const char * data = handler.getData(0, 0);
	
	//get bxi from unit 0 and rail 0
	uint16_t bxi = handler.getBXI(0, 0);
	
	//get data size in bits from unit 0 and rail 0 :
	uint32_t bitSIze = handler.getDataBitSize(0, 0);
	
	//get data size in bytes (aligned on 64 bits) from unit 0 and rail 0 :
	size_t byteSize = handler.getBytesAlignedSize(0, 0);
```
