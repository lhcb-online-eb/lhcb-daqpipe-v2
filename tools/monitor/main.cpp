/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include <cstdlib>
#include <monitor/Monitor.hpp>
#include <iostream>

/*******************  NAMESPACE  ********************/
using namespace std;

/*******************  FUNCTION  *********************/
int main(int argc, char ** argv)
{
	//get args
	if (argc != 2) {
		cerr << "You must provide a config file as parameter ! " << endl;
		abort();
	}
	
	//start
	cout << "Start" << endl;

	//run
	DAQ::runDAQMonitor(argv[1],[&](DAQ::EventHandler & handler){
		cout << "Get event " << handler.getEventId() << endl;
	});
	
	//finish
	cout << "Finish" << endl;
	
	//ok
	return EXIT_SUCCESS;
}
