######################################################
#            PROJECT  : lhcb-daqpipe                 #
#            VERSION  : 2.5.0-dev                    #
#            DATE     : 12/2017                      #
#            AUTHOR   : Valat Sébastien  - CERN      #
#            LICENSE  : CeCILL-C                     #
######################################################

######################################################
if (HTOPML_FOUND)
	set(OPTIONAL_EXTRA $<TARGET_OBJECTS:daqpipe-htopml-plugins>)
endif (HTOPML_FOUND)

######################################################
include_directories(${CMAKE_SOURCE_DIR}/extern-deps/jsoncpp/dist)
include_directories(${CMAKE_SOURCE_DIR}/src)
include_directories(${CMAKE_BINARY_DIR}/src)
include_directories(${CMAKE_CURRENT_SOURCE_DIR}/../)

######################################################
add_library(daqpipe-monitor-recv OBJECT MonitorRecv.cpp Monitor.cpp)
SET_TARGET_PROPERTIES(daqpipe-monitor-recv PROPERTIES COMPILE_FLAGS -fPIC)

######################################################
add_executable(monitor main.cpp 
                       $<TARGET_OBJECTS:daqpipe-monitor-recv> 
                       $<TARGET_OBJECTS:daqpipe-reader-obj> 
                       $<TARGET_OBJECTS:daqpipe-common> 
                       $<TARGET_OBJECTS:daqpipe-portability-net> 
                       $<TARGET_OBJECTS:daqpipe-gochannels>) 

target_link_libraries(monitor libjsoncpp ${DAQPIPE_EXTERN_LIBS})

######################################################
add_library(daqpipe-monitor SHARED 
                       $<TARGET_OBJECTS:daqpipe-monitor-recv> 
                       $<TARGET_OBJECTS:daqpipe-reader-obj> 
                       $<TARGET_OBJECTS:daqpipe-common> 
                       $<TARGET_OBJECTS:daqpipe-portability-net> 
                       $<TARGET_OBJECTS:daqpipe-gochannels>) 

target_link_libraries(daqpipe-monitor libjsoncpp ${DAQPIPE_EXTERN_LIBS})

######################################################
configure_file(Makefile.in "${CMAKE_CURRENT_BINARY_DIR}/gen/Makefile" @ONLY)

######################################################
INSTALL(TARGETS daqpipe-monitor
	ARCHIVE DESTINATION lib${LIBSUFFIX}
	LIBRARY DESTINATION lib${LIBSUFFIX})
INSTALL(FILES Monitor.hpp DESTINATION include/daqpipe/monitor)
INSTALL(FILES main.cpp Readme.md ${CMAKE_CURRENT_BINARY_DIR}/gen/Makefile DESTINATION share/daqpipe/monitor-example)
