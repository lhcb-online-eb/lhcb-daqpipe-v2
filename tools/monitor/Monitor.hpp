/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_MONITOR_HPP
#define DAQ_MONITOR_HPP

/********************  HEADERS  *********************/
#include <cstdlib>
#include <cstdint>
#include "../reader/EventHandler.hpp"
#include <functional>

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*******************  FUNCTION  *********************/
void runDAQMonitor(const std::string & configFile, std::function<void(EventHandler&)> callback);

}

#endif //DAQ_MONITOR_HPP
