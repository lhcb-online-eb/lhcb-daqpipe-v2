/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include "MonitorRecv.hpp"
#include <portability/TcpHelper.hpp>
#include <sys/types.h>
#include <sys/socket.h>
#include "writer/FileStruct.hpp"

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*******************  FUNCTION  *********************/
MonitorRecv::MonitorRecv ( Config& config )
{
	//config
	useIpV6 = config.getAndSetConfigBool("monitor","ipv6",false);
	port = config.getAndSetConfigInt("monitor","port",3800);
	clients = config.getAndSetConfigInt("monitor","clients",0);
	
	//check
	assume(clients > 0,"You must define the number of client to be used (monitor.clients) !");
}

/*******************  FUNCTION  *********************/
MonitorRecv::~MonitorRecv ( void )
{

}

/*******************  FUNCTION  *********************/
void MonitorRecv::run ( std::function< void(EventHandler&) > callback )
{
	//save
	this->callback = callback;
	
	//listen
	int listenFd = TcpHelper::makeListeningSocket(port,useIpV6);
	
	//start thread
	threads = new std::thread[clients];
	for (int i = 0 ; i < clients ; i++) {
		//vars
		struct sockaddr_storage ss;
		socklen_t slen = sizeof(ss);
		
		//accept the incomming connection
		DAQ_DEBUG("monitor","Waiting for accept");
		int fd = accept(listenFd, (struct sockaddr*)&ss, &slen);
		assumeArg(fd > 2,"Invalid file descriptor on accept : %1").argStrErrno().end();
		DAQ_DEBUG("monitor","Has accepted one");
		
		//start thread
		threads[i] = std::thread([=](){threadMain(fd);});
	}
	
	//wait all
	std::thread th([&](){
		for (int i = 0 ; i < clients ; i++) {
			threads[i].join();
		}
		jobs.close();
	});
	
	//run
	mainLoop();
	th.join();
	
	DAQ_DEBUG("monitor","All threads closed");
}

/*******************  FUNCTION  *********************/
void MonitorRecv::threadMain ( int fd )
{
	//vars
	MonitorDataHeader header;
	
	//while get events
	while (TcpHelper::safeRecv(fd,&header,sizeof(header))) {
		//read data
		char * data = new char[header.size];
		bool status = TcpHelper::safeRecv(fd,data,header.size);
		
		//use
		if (status) {
			//build
			EventHandler * handler = new EventHandler;
			handler->affect(header,data);
			handler->autoFreeMem();
			
			//push
			handler >> jobs;
		} else {
			return;
		}
	}
}

/*******************  FUNCTION  *********************/
void MonitorRecv::mainLoop ( void )
{
	EventHandler * handler;
	while (jobs >> handler) {
		this->callback(*handler);
		delete handler;
	}
}

}
