/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_MONITOR_RECV_HPP
#define DAQ_MONITOR_RECV_HPP

/********************  HEADERS  *********************/
#include <cstdlib>
#include <cstdint>
#include "../reader/EventHandler.hpp"
#include "gochannels/GoChannel.hpp"
#include "common/Config.hpp"
#include <functional>
#include <thread>

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*********************  CLASS  **********************/
class MonitorRecv
{
	public:
		MonitorRecv(Config & config);
		~MonitorRecv(void);
		void run(std::function<void(EventHandler&)> callback);
	private:
		void threadMain(int fd);
		void mainLoop(void);
	private:
		bool useIpV6;
		int port;
		int clients;
		GoChannel<EventHandler*> jobs;
		std::function<void(EventHandler&)> callback;
		std::thread * threads;
};

}

#endif //DAQ_MONITOR_RECV_HPP
