/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include <cstdlib>
#include <common/Config.hpp>
#include "MonitorRecv.hpp"
#include <iostream>
#include "Monitor.hpp"

/*******************  NAMESPACE  ********************/
using namespace std;

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/*******************  FUNCTION  *********************/
void runDAQMonitor (const std::string & configFile,std::function< void(EventHandler&)> callback )
{
	const char * argv[3];
	argv[1] = "-f";
	argv[2] = configFile.c_str();
	
	Config config;
	config.parseArgs(3,(const char**)argv);

	MonitorRecv monitor(config);
	
	monitor.run(callback);
}

}
