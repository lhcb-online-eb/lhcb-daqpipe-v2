######################################################
#            PROJECT  : lhcb-daqpipe                 #
#            VERSION  : 2.5.0-dev                    #
#            DATE     : 12/2017                      #
#            AUTHOR   : Valat Sébastien  - CERN      #
#            LICENSE  : CeCILL-C                     #
######################################################

######################################################
include_directories(${CMAKE_SOURCE_DIR}/extern-deps/jsoncpp/dist)
include_directories(${CMAKE_SOURCE_DIR}/src)
include_directories(${CMAKE_BINARY_DIR}/src)

######################################################
add_executable(amc40-emulator main.cpp $<TARGET_OBJECTS:daqpipe-common> $<TARGET_OBJECTS:daqpipe-datasource-amc40-gen>) 
target_link_libraries(amc40-emulator libjsoncpp ${DAQPIPE_EXTERN_LIBS})