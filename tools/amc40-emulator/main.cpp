/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
//std
#include <cstdlib>
#include <fstream>
#include <cstring>
#include <cstdio>
//daqpipe
#include "common/Debug.hpp"
#include "common/Config.hpp"
#include "datasource/amc40/Amc40StreamStruct.hpp"
#include "datasource/amc40/Amc40Generator.hpp"
//json
#include <json/json.h>
//for sockets
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <endian.h>

/*******************  FUNCTION  *********************/
void loadConfig(const char * filename)
{
	//setup json file reader
	Json::Reader reader;
	
	//setup stream and open file
	std::ifstream file(filename, std::ifstream::binary);
	assumeArg(file.is_open(),"Fail to open configuration file : %1").arg(filename).end();
	
	//load
	Json::Value tmpObj;

	//parse
	bool status = reader.parse(file, tmpObj);
	assumeArg(status,"Fail to load configuration file '%1' : \n%2")
		.arg(filename)
		.arg(reader.getFormattedErrorMessages())
		.end();
}

/*******************  FUNCTION  *********************/
int openSocket ( size_t bufferSize, const char * addr, int port )
{
	int sock;
	
	//create socket
	assumeArg((sock = socket(AF_INET, SOCK_DGRAM, 0)) != -1,"Failed to create DGRAM socket : %1").argStrErrno().end();
	
	//return
	return sock;
}

/*******************  FUNCTION  *********************/
int main(int argc, char ** argv)
{
	//check argument
	assume(argc == 2, "You must provide the daqpipe config file as parameter");
	
	//load config file
	DAQ::Config config;
	config.loadConfigFile(argv[1]);
	
	//extract infos
	size_t packetMaxSize = config.getAndSetConfigUInt64("dataSources.amc40","packetMaxSize",16777216);
	int dataPort = config.getAndSetConfigInt("dataSources.amc40","dataPort",3600);
	int tfcPort = config.getAndSetConfigInt("dataSources.amc40","tfcPort",3601);
	size_t eventCollisions = config.getEventCollisions();
	
	//open socket
	int line1Sock = openSocket(packetMaxSize,"127.0.0.1",dataPort);
	
	//setup address
	struct sockaddr_in address;
	memset(&address, 0, sizeof address);
	address.sin_family = AF_INET;
	//address.sin_addr.s_addr = htonl(INADDR_ANY);
	address.sin_addr.s_addr = inet_addr("127.0.0.1");
	address.sin_port = htons(dataPort);
	
	//tfc
	struct sockaddr_in tfcAddress;
	memset(&tfcAddress, 0, sizeof tfcAddress);
	tfcAddress.sin_family = AF_INET;
	//address.sin_addr.s_addr = htonl(INADDR_ANY);
	tfcAddress.sin_addr.s_addr = inet_addr("127.0.0.1");
	tfcAddress.sin_port = htons(tfcPort);
	
	//ininit loop to send packets
	char buffer[MAX_UDP_SIZE * 2];
	DAQ::Amc40GenState dataState;
	DAQ::Amc40GenState tfcState;
	while (true) {
		if (dataState.meps == tfcState.meps) {
			//gen
			size_t size = DAQ::genTfcPacket(tfcState,buffer,MAX_UDP_SIZE,eventCollisions);
			
			//send packet
			printf("Sending packet %u (%lu) -> %lu...\n",tfcState.seqNum,size,tfcState.collisionId);
			sendto(line1Sock,buffer,size,0,(sockaddr*)&tfcAddress,sizeof(address));
		}
		
		//gen
		size_t size = DAQ::genPacket(dataState,buffer,MAX_UDP_SIZE,eventCollisions);
		
		//send packet
		printf("Sending packet %u (%lu) -> %lu...\n",dataState.seqNum,size,dataState.collisionId);
		sendto(line1Sock,buffer,size,0,(sockaddr*)&address,sizeof(address));
		usleep(100000);
	}
}
