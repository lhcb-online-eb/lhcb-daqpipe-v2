/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
//std
#include <string>
#include <iostream>
#include "common/Debug.hpp"
//linux
#include <sys/mman.h>
//json
#include <json/json.h>
//local
#include "writer/FileStruct.hpp"
#include "Reader.hpp"

/**********************  USING  *********************/
using namespace DAQ;
using namespace std;

/*******************  FUNCTION  *********************/
std::string dataToHex(const char * data, size_t size)
{
	std::string tmp;
	char buffer[4];
	for (size_t i = 0 ; i < size ; i++) {
		sprintf(buffer,"%02x",(int)data[i]);
		//Json::Value v = buffer;
		//out.append(v);
		if (i % 32 == 0 && i != 0)
			tmp += "\n";
		else if (i > 0)
			tmp+=" ";
		tmp += buffer;
		
	}
	return tmp;
}

/*******************  FUNCTION  *********************/
int main(int argc,char ** argv)
{
	//check
	assumeArg(argc == 2,"You must provide arguments : %1 {FILE}").arg(argv[0]).end();
	
	//extract params
	std::string filename = argv[1];
	
	//open
	Reader reader(filename);
	
	//print infos
	cout << "Date : " << reader.getDateTime() << endl;
	cout << "baseEventId : " << reader.getBaseEventId() << endl;
	cout << "events : " << reader.getEvents() << endl;
	cout << "units : " << reader.getUnits() << endl;
	cout << "rails : " << reader.getRails() << endl;
	cout << endl;

	for (uint64_t i = 0 ; i < reader.getEvents() ; i++)
	{
		//pick event
		EventHandler event;
		reader.mapEvent(event,i);
		
		//print header
		cout << "======================================= Event Id : " << i << " ==========================================" << endl;
		for (int unit = 0 ; unit < reader.getUnits() ; unit++) {
			for (int rail = 0 ; rail < reader.getRails() ; rail++) {
				cout << "UNIT : " << unit << " , RAIL : " << rail << " , BXI : " << event.getBXI(unit,rail) << " , SIZE : " << event.getBytesAlignedSize(unit,rail) << " , BITS : " << event.getDataBitSize(unit,rail) << endl;
				cout << dataToHex(event.getData(unit,rail),event.getBytesAlignedSize(unit,rail)) << endl;
				cout << endl;
			}
		}
		
	}

	return EXIT_SUCCESS;
}
