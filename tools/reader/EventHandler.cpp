/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include "Reader.hpp"
#include "EventHandler.hpp"
#include <sys/mman.h>
#include <cstdio>
#include "common/Debug.hpp"

/********************  NAMESPACE  *******************/
namespace DAQ
{

/*******************  FUNCTION  *********************/
void EventHandler::affect ( uint64_t eventId, uint16_t units, uint16_t rails, const char* base )
{
	//trivial
	this->eventId = eventId;
	
	//setup
	size_t frags = rails * units;
	this->rails = rails;
	this->fragmentTable = (FileMetaEntry*)base;
	
	//move
	base = (char*)(fragmentTable + frags);
	
	//align
	if ((size_t)base % 8 != 0)
		base += 8 - (size_t)base % 8;

	
	//memory
	if (frags > fragmentNb) {
		delete [] fragments;
	}
	if (frags > fragmentNb || fragments == NULL) {
		fragments = new const char*[frags];
	}
	this->fragmentNb = frags;
	
	//fill table
	for (int unit = 0 ; unit < units ; unit++) {
		for (int rail = 0 ; rail < rails ; rail++) {
			int id = rail+rails * unit;
			fragments[id] = base;
			base += fragmentTable[id].size;
		}
	}
}

/*******************  FUNCTION  *********************/
void EventHandler::affect ( MonitorDataHeader& header, const char* base )
{
	affect(header.eventId,header.units,header.rails,base);
}

/*******************  FUNCTION  *********************/
uint16_t EventHandler::getBXI ( uint16_t unitId, uint8_t rail ) const
{
	size_t id = rail+rails * unitId;
	assumeArg(id < fragmentNb, "Invalid fragment id, please check unitId (%1) or rail (%2) parameter !").arg(unitId).arg(rail).end();
	return ((FileFragmentHeader*)fragments[id])->bxi;
}

/*******************  FUNCTION  *********************/
uint32_t EventHandler::getDataBitSize ( uint16_t unitId, uint8_t rail ) const
{
	//special case
	if (getBytesAlignedSize(unitId,rail) == 0)
		return 0;
	
	//read header
	size_t id = rail+rails * unitId;
	assumeArg(id < fragmentNb, "Invalid fragment id, please check unitId (%1) or rail (%2) parameter !").arg(unitId).arg(rail).end();
	return ((FileFragmentHeader*)fragments[id])->size;
}

/*******************  FUNCTION  *********************/
const char* EventHandler::getData ( uint16_t unitId, uint8_t rail ) const
{
	size_t id = rail+rails * unitId;
	assumeArg(id < fragmentNb, "Invalid fragment id, please check unitId (%1) or rail (%2) parameter !").arg(unitId).arg(rail).end();
	return fragments[id];//+sizeof(FileFragmentHeader);
}

/*******************  FUNCTION  *********************/
size_t EventHandler::getBytesAlignedSize ( uint16_t unitId, uint8_t rail ) const
{
	size_t id = rail+rails * unitId;
	assumeArg(id < fragmentNb, "Invalid fragment id, please check unitId (%1) or rail (%2) parameter !").arg(unitId).arg(rail).end();
	return fragmentTable[id].size;
}

/*******************  FUNCTION  *********************/
uint64_t EventHandler::getEventId ( void ) const
{
	return eventId;
}

/*******************  FUNCTION  *********************/
void EventHandler::autoFreeMem ( void )
{
	this->autoFree = true;
}

/*******************  FUNCTION  *********************/
EventHandler::EventHandler ( void )
{
	this->autoFree = false;
	this->fragmentNb = 0;
	this->fragments = NULL;
	this->fragmentTable = NULL;
	this->eventId = -1;
	this->rails = 0;
}

/*******************  FUNCTION  *********************/
EventHandler::~EventHandler ( void )
{
	if (fragments != NULL)
		delete [] fragments; 
	if (autoFree && fragmentTable != NULL)
		delete fragmentTable;
}

}
