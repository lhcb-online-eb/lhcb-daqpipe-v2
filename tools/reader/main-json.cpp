/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
//std
#include <string>
#include <iostream>
#include "common/Debug.hpp"
//linux
#include <sys/mman.h>
//json
#include <json/json.h>
//local
#include "writer/FileStruct.hpp"
#include "Reader.hpp"

/**********************  USING  *********************/
using namespace DAQ;
using namespace std;

/*******************  FUNCTION  *********************/
void dataToHex(Json::Value & out,const char * data, size_t size)
{
	std::string tmp;
	char buffer[4];
	for (size_t i = 0 ; i < size ; i++) {
		sprintf(buffer,"%02x",(int)data[i]);
		//Json::Value v = buffer;
		//out.append(v);
		if (i > 0)
			tmp+=",";
		tmp += buffer;
		out = tmp;
	}
}

/*******************  FUNCTION  *********************/
int main(int argc,char ** argv)
{
	//check
	assumeArg(argc == 3,"You must provide arguments : %1 {FILE} {EVENT_ID}").arg(argv[0]).end();
	
	//extract params
	std::string filename = argv[1];
	size_t eventId = atol(argv[2]);
	
	//open
	Reader reader(filename);
	
	//start to gen json file from header
	Json::Value json;
	Json::Value & jsonHeader = json["header"];
	jsonHeader["date"] = (Json::UInt64)reader.getDateTime();;
	jsonHeader["baseEventId"] = (Json::UInt64)reader.getBaseEventId();
	jsonHeader["events"] = (Json::UInt64)reader.getEvents();
	jsonHeader["units"] = (Json::UInt64)reader.getUnits();
	jsonHeader["rails"] = (Json::UInt64)reader.getRails();
	//jsonHeader["fileFormatVersion"] = (Json::UInt64)header->fileFormatVersion;
	
	//pick event
	EventHandler event;
	reader.mapEvent(event,eventId);
	
	//convert data
	json["eventId"] = (Json::UInt64)event.getEventId();
	Json::Value & jsonData = json["data"];
	Json::Value & jsonUnits = jsonData["units"];
	for (int unit = 0 ; unit < reader.getUnits() ; unit++) {
		Json::Value & jsonUnit = jsonUnits[unit];
		for (int rail = 0 ; rail < reader.getRails() ; rail++) {
			Json::Value & jsonFrag = jsonUnit[rail];
			
			//extract infos
			jsonFrag["bxi"] = (Json::UInt64)event.getBXI(unit,rail);
			jsonFrag["size"] = (Json::UInt64)event.getDataBitSize(unit,rail);
			
			//convert data
			size_t size = event.getBytesAlignedSize(unit,rail);
			const char * fragData = event.getData(unit,rail);
			dataToHex(jsonFrag["data"],fragData,size);
			//for debug
			jsonFrag["string"] = fragData;
		}
	}
	
	cout << json << endl;

	return EXIT_SUCCESS;
}
