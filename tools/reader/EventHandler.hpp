/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_EVENT_HANDLER_HPP
#define DAQ_EVENT_HANDLER_HPP

/********************  HEADERS  *********************/
#include <string>
#include <cstdint>
#include "writer/FileStruct.hpp"

/********************  NAMESPACE  *******************/
namespace DAQ
{

/*********************  STRUCT  *********************/
struct EventHandler
{
	public:
		EventHandler(void);
		~EventHandler(void);
		uint64_t getEventId(void) const;
		const char * getData(uint16_t unitId,uint8_t rail) const;
		uint16_t getBXI( uint16_t unitId, uint8_t rail ) const;
		uint32_t getDataBitSize(uint16_t unitId,uint8_t rail) const;
		size_t getBytesAlignedSize(uint16_t unitId,uint8_t rail) const;
		void affect( DAQ::MonitorDataHeader& header, const char* base );
		void affect(uint64_t eventId, uint16_t units, uint16_t rails, const char * base);
		void autoFreeMem(void);
	private:
		EventHandler(const EventHandler & orig);
		EventHandler & operator =(const EventHandler & reader);
	private:
		FileMetaEntry * fragmentTable;
		const char ** fragments;
		uint64_t eventId;
		size_t fragmentNb;
		int rails;
		bool autoFree;
};

};

#endif //DAQ_EVENT_HANDLER_HPP
