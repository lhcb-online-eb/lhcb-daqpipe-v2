/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_READER_HPP
#define DAQ_READER_HPP

/********************  HEADERS  *********************/
#include "EventHandler.hpp"

/********************  NAMESPACE  *******************/
namespace DAQ
{

/*********************  CLASS  **********************/
class Reader
{
	public:
		Reader(const std::string & filename = "");
		~Reader(void);
		bool open(const std::string & filename, bool abortOnFail = false);
		void mapEvent(EventHandler & handler,uint64_t eventId);
		int getUnits(void) const;
		int getRails(void) const;
		uint64_t getEvents(void) const;
		uint64_t getDateTime(void) const;
		uint64_t getBaseEventId(void) const;
	private:
		char * data;
		size_t size;
		FileHeader * fileHeader;
		FileEventMapEntry * eventTable;
		char ** eventDataBase;
};

};

#endif //DAQ_READER_HPP
