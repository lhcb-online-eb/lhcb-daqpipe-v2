/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include "Reader.hpp"
#include <sys/mman.h>
#include <cstdio>
#include "common/Debug.hpp"

/********************  NAMESPACE  *******************/
namespace DAQ
{

/*******************  FUNCTION  *********************/
Reader::Reader(const std::string& filename)
{
	//init
	this->data = NULL;
	this->size = 0;
	
	//open if has file
	if (filename.empty() == false)
		this->open(filename,true);
}

/*******************  FUNCTION  *********************/
Reader::~Reader(void)
{
	if (data != NULL)
		munmap(data,size);
}

/*******************  FUNCTION  *********************/
bool Reader::open(const std::string& filename, bool abortOnFail)
{
	//check
	assume(filename.empty() == false,"Try to open file without any name !");
	
	//open file
	FILE * fp = fopen(filename.c_str(),"r");
	if (abortOnFail) {
		assumeArg(fp != NULL,"Fail to open file %1 : %2").arg(filename).argStrErrno().end();
	} else if (fp == NULL) {
		return false;
	}
	
	//get size
	int status = fseek(fp,0,SEEK_END);
	assumeArg(status == 0, "Failed to seek at end of file : %1").argStrErrno().end();
	size_t fileSize = ftell(fp);
	status = fseek(fp,0,SEEK_SET);
	assumeArg(status == 0, "Failed to seek at begin of file : %1").argStrErrno().end();
	
	//map in memory
	void * data = mmap(NULL,fileSize + 4096 - fileSize % 4096,PROT_READ,MAP_PRIVATE,fileno(fp),0);
	assumeArg(data != MAP_FAILED, "Failed to map file : %1").argStrErrno().end();
	this->size = fileSize;
	
	//setup
	this->fileHeader = (FileHeader*)data;
	this->eventTable = (FileEventMapEntry*)(this->fileHeader+1);
	
	//check
	assume(fileHeader->magik == DAQ_FILE_MAGIK,"Invalid file format !");
	assume(fileHeader->fileFormatVersion == DAQ_FILE_FORMAT_VERSION,"Invalid file format !");
	
	//build pointer database to avoid loop for each access
	this->eventDataBase = new char*[this->fileHeader->events];
	this->eventDataBase[0] = (char*)(this->eventTable + this->fileHeader->events);
	for (size_t i = 1 ; i < this->fileHeader->events ; i++) {
		this->eventDataBase[i] = this->eventDataBase[i-1] + this->eventTable[i-1].size;
	}
	
	//ack ok
	return true;
}

/*******************  FUNCTION  *********************/
int Reader::getRails ( void ) const
{
	return this->fileHeader->rails;
}

/*******************  FUNCTION  *********************/
int Reader::getUnits ( void ) const
{
	return this->fileHeader->units;
}

/*******************  FUNCTION  *********************/
void Reader::mapEvent ( EventHandler& handler, uint64_t entryId )
{
	assumeArg(entryId < fileHeader->events,"Invalid event ID, expect lower than %1 but get %2").arg(fileHeader->events).arg(entryId).end();
	handler.affect(this->eventTable[entryId].eventId,getUnits(),getRails(),eventDataBase[entryId]);
}

/*******************  FUNCTION  *********************/
uint64_t Reader::getEvents ( void ) const
{
	return fileHeader->events;
}

/*******************  FUNCTION  *********************/
uint64_t Reader::getDateTime ( void ) const
{
	return fileHeader->dateTime;
}

/*******************  FUNCTION  *********************/
uint64_t Reader::getBaseEventId ( void ) const
{
	return fileHeader->baseEventId;
}

}
