######################################################
#            PROJECT  : lhcb-daqpipe                 #
#            VERSION  : 2.5.0-dev                    #
#            DATE     : 12/2017                      #
#            AUTHOR   : Valat Sébastien  - CERN      #
#            LICENSE  : CeCILL-C                     #
######################################################

######################################################
add_subdirectory(amc40-emulator)
add_subdirectory(reader)
add_subdirectory(viewer)
add_subdirectory(internal-launcher)
add_subdirectory(monitor)
add_subdirectory(net-topo-viewer)
add_subdirectory(filter-units)