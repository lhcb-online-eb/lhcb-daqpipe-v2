/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include "Server.hpp"
#include "Protocol.hpp"
#include <portability/TcpHelper.hpp>
//std
#include <cstring>
//daqpipe
#include <common/Debug.hpp>
//unix
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/epoll.h>

/********************  MACROS  **********************/
#define MAX_EVENTS 32

/*******************  NAMESPACE  ********************/
namespace InternalLauncher
{

/*******************  FUNCTION  *********************/
Server::Server ( int nodes )
{
	this->expectedNodes = nodes;
	this->sockets = new int[nodes];
	this->barrier = new bool[nodes];
	this->db = new LauncherDb[nodes];
	this->resetBarrier();
	
	for (int i = 0 ; i < expectedNodes ; i++)
		sockets[i] = -1;
}

/*******************  FUNCTION  *********************/
Server::~Server ( void )
{
	delete [] sockets;
	delete [] barrier;
	delete [] db;
}

/*******************  FUNCTION  *********************/
void Server::close ( void )
{
	for (int i = 0 ; i < expectedNodes ; i++) {
		::close(sockets[i]);
		sockets[i] = -1;
	}
}

/*******************  FUNCTION  *********************/
void Server::waitConnect ( int port )
{
	//create listening socket
	int socket = DAQ::TcpHelper::makeListeningSocket(port);
	
	//accept all
	for (int i = 0 ; i < expectedNodes ; i++)
		acceptIncoming(socket);
	
	//close listening socket
	::close(socket);
}

/*******************  FUNCTION  *********************/
void Server::acceptIncoming ( int listenSocket )
{
	//vars
	struct sockaddr_storage ss;
	socklen_t slen = sizeof(ss);
	
	//accept the incomming connection
	DAQ_DEBUG("server","Waiting for accept");
	int fd = accept(listenSocket, (struct sockaddr*)&ss, &slen);
	assumeArg(fd > 2,"Invalid file descriptor on accept : %1").argStrErrno().end();
	DAQ_DEBUG("server","Has accepted one");
	
	//read the handshake to know what and who it is
	Handshake handshake;
	int status = recv(fd, &handshake, sizeof(handshake), 0);
	assume(status == sizeof(handshake),"Invalid size from handshake on incomming connection !");
	assumeArg(handshake.type == INTERNAL_LAUNCHER_HANDSHAKE,"Invalid message type (%1) for handshake of incomming connection !").arg(handshake.type).end();
	assumeArg(sockets[handshake.rank] == -1,"Rank already connected, bad information received from peers : %1 ").arg(handshake.rank).end();
	
	//store
	sockets[handshake.rank] = fd;
	DAQ_INFO_ARG("Get connection of rank %1").arg(handshake.rank).end();
}

/*******************  FUNCTION  *********************/
void Server::run ( void )
{
	//information
	DAQ_INFO("Get all nodes connected, can start !");
	
	//say we are ready to everybody
	sendReady();
	
	//setup epoll
	int epollFd = epoll_create(expectedNodes);
	assume(epollFd != -1,"Failed to setup epoll !");
	
	//add all
	struct epoll_event event;
	for (int i = 0 ; i < expectedNodes ; i++) {
		event.data.fd = sockets[i];
		event.events = EPOLLIN;
		
		//regs and check
		int status = epoll_ctl(epollFd, EPOLL_CTL_ADD, sockets[i], &event);
		assumeArg(status >= 0,"Error at epoll_ctl() : %1").argStrErrno().end();
	}
	
	//polling
	struct epoll_event events[MAX_EVENTS];
	while (expectedNodes > 0) {
		int cnt = epoll_wait(epollFd, events, MAX_EVENTS, -1);
		assumeArg(cnt != -1,"Failed to use epoll_wait : %1").argStrErrno().end();
		
		for (int i = 0 ; i < cnt ; i++) {
			if ((events[i].events & EPOLLERR) || (events[i].events & EPOLLHUP) ||
				(!(events[i].events & EPOLLIN))) {
				/* An error has occured on this fd, or the socket is not
				ready for reading (why were we notified then?) */
				DAQ_ERROR_ARG("epoll error").argStrErrno().end();
				::close(events[i].data.fd);
			} else {
				ssize_t count;
				Command command;
				count = recv(events[i].data.fd, (void *)&command, sizeof(command), 0);
				if (count == 0 ) {
					DAQ_INFO("Client diconnected !");
					int status = epoll_ctl(epollFd, EPOLL_CTL_DEL, events[i].data.fd, NULL);
					assumeArg(status >= 0,"Error at epoll_ctl() : %1").argStrErrno().end();
					expectedNodes--;
				} else {
					assumeArg(count != -1, "Cmd Receive count = -1 occurred : %1").argStrErrno().end();
					assumeArg(count == sizeof(command), "Cmd Receive invalid size %1 !").arg(count).end();
					runCommand(events[i].data.fd, command);
				}
			}
		}
	}
}

/*******************  FUNCTION  *********************/
void Server::runCommand ( int fd, Command& command )
{
	switch (command.type) {
		case INTERNAL_LAUNCHER_WAIT_BARRIER:
			markBarrier(command.rank);
			break;
		case INTERNAL_LAUNCHER_SET_DB_ENTRY:
			db[command.rank][command.key] = command.value;
			break;
		case INTERNAL_LAUNCHER_REQ_DB_ENTRY:
			dbRequest(fd,command);
			break;
		default:
			DAQ_ERROR_ARG("Invalid command : %1").arg(command.type).end();
	}
}

/*******************  FUNCTION  *********************/
void Server::dbRequest ( int fd, Command& command )
{
	//search
	LauncherDb::const_iterator it = db[command.rank].find(command.key);
	assumeArg(it != db[command.rank].end(),"Invalid key request in DB from rank %1 : %2").arg(command.rank).arg(command.key).end();
	
	//craft
	Command out;
	out.type = INTERNAL_LAUNCHER_RET_DB_ENTRY;
	out.rank = command.rank;
	strcpy(out.key,command.key);
	strcpy(out.value,it->second.c_str());
	
	//send
	int status = send(fd,&out,sizeof(out),0);
	assume(status == sizeof(out), "Invalid size !");
}

/*******************  FUNCTION  *********************/
void Server::resetBarrier ( void )
{
	this->barrierCount = 0;
	memset(barrier,0,sizeof(bool)*expectedNodes);
}

/*******************  FUNCTION  *********************/
void Server::markBarrier ( int rank )
{
	//check
	assumeArg(rank >= 0 && rank < expectedNodes,"Invalid rank : %1").arg(rank).end();
	assumeArg(barrier[rank] == false, "Invalid barrier state for rank %1").arg(rank).end();
	
	//mark
	barrier[rank] = true;
	barrierCount++;
	
	//if all
	if (barrierCount == expectedNodes) {
		resetBarrier();
		ackBarrier();
	}
}

/*******************  FUNCTION  *********************/
void Server::ackBarrier ( void )
{
	Command command;
	command.type = INTERNAL_LAUNCHER_ACK_BARRIER;
	
	DAQ_INFO("Ack barrier");
	for (int i = 0 ; i < expectedNodes ; i++) {
		int status = send(sockets[i],&command,sizeof(command),0);
		assume(status == sizeof(command), "Invalid size !");
	}
}

/*******************  FUNCTION  *********************/
void Server::sendReady ( void )
{
	//setup message
	ReadyToStart message;
	message.type = INTERNAL_LAUNCHER_READY_START;
	message.worldSize = expectedNodes;
	
	//send to everybody
	for (int i = 0 ; i < expectedNodes ; i++) {
		int status = send(sockets[i],&message,sizeof(message),0);
		assumeArg(status == sizeof(message), "Failed to send start message to rank %1 : %2").arg(i).argStrErrno().end();
	}
}

}
