/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include <cstdlib>
#include <common/Debug.hpp>
#include <cstdio>
#include "Client.hpp"
#include <iostream>

/*******************  NAMESPACE  ********************/
using namespace std;
using namespace InternalLauncher;

/*******************  FUNCTION  *********************/
int main(int argc, char ** argv)
{
	//check args
	assume(argc == 3,"Invalid argument, you need to provide the number of expected : [server] [rank] !");
	std::string server = argv[1];
	int rank = atoi(argv[2]);
	
	//prepare server
	Client client(server,rank,8086);
	
	cout << "Size = " << client.getWorldSize() << endl;
	cout << "Rank = " << client.getRank() << endl;
	
	char buffer[128];
	sprintf(buffer,"test %d",rank);
	client.set("test",buffer);
	
	cout << "Run barrier !" << endl;
	client.barrier();
	cout << "Exit Barrtier !" << endl;
	
	for (int i = 0 ; i < client.getWorldSize() ; i++)
		cout << "Value [" << i << "] = " << client.get("test",i) << endl;
	
	client.barrier();
	
	//ok finish
	return EXIT_SUCCESS;
}