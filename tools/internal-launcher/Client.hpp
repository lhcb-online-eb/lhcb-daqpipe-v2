/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_INTERNAL_LAUNCHER_CLIENT_HPP
#define DAQ_INTERNAL_LAUNCHER_CLIENT_HPP

/********************  HEADERS  *********************/
#include <string>

/*******************  NAMESPACE  ********************/
namespace InternalLauncher
{

/*********************  CLASS  **********************/
class Client
{
	public:
		Client( const std::string& server, int rank, int port );
		~Client(void);
		void barrier(void);
		void set(const std::string key,const std::string value);
		std::string get(const std::string key, int rank);
		int getRank(void) const;
		int getWorldSize(void) const;
	private:
		void connect( const std::string server, int port, bool useIpV6 );
	private:
		int fd;
		int rank;
		int worldSize;
};

}

#endif //DAQ_INTERNAL_LAUNCHER_CLIENT_HPP
