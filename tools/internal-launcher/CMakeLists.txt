######################################################
#            PROJECT  : lhcb-daqpipe                 #
#            VERSION  : 2.5.0-dev                    #
#            DATE     : 12/2017                      #
#            AUTHOR   : Valat Sébastien  - CERN      #
#            LICENSE  : CeCILL-C                     #
######################################################

######################################################
if (HTOPML_FOUND)
	set(OPTIONAL_EXTRA $<TARGET_OBJECTS:daqpipe-htopml-plugins>)
endif (HTOPML_FOUND)

######################################################
include_directories(${CMAKE_SOURCE_DIR}/extern-deps/jsoncpp/dist)
include_directories(${CMAKE_SOURCE_DIR}/src)
include_directories(${CMAKE_BINARY_DIR}/src)

######################################################
add_executable(internal-launcher main.cpp Server.cpp $<TARGET_OBJECTS:daqpipe-common> $<TARGET_OBJECTS:daqpipe-portability-net>) 
target_link_libraries(internal-launcher libjsoncpp ${DAQPIPE_EXTERN_LIBS})

######################################################
add_library(daqpipe-internal-launcher-client OBJECT Client.cpp)

######################################################
add_executable(internal-launcher-test test.cpp $<TARGET_OBJECTS:daqpipe-common> $<TARGET_OBJECTS:daqpipe-portability-net> $<TARGET_OBJECTS:daqpipe-internal-launcher-client>) 
target_link_libraries(internal-launcher-test libjsoncpp ${DAQPIPE_EXTERN_LIBS})