/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_INTERNAL_LAUNCHER_SERVER_HPP
#define DAQ_INTERNAL_LAUNCHER_SERVER_HPP

/********************  HEADERS  *********************/
#include <map>

/*******************  NAMESPACE  ********************/
namespace InternalLauncher
{

/*********************  TYPES  **********************/
typedef std::map<std::string,std::string> LauncherDb;

/*********************  CLASS  **********************/
class Server
{
	public:
		Server(int nodes);
		~Server(void);
		void waitConnect(int port);
		void run(void);
		void close(void);
	private:
		void acceptIncoming(int listenSocket);
		void sendReady(void);
		void runCommand(int fd, struct Command & command);
		void resetBarrier(void);
		void markBarrier(int rank);
		void ackBarrier(void);
		void dbRequest(int fd,struct Command & command);
	private:
		int expectedNodes;
		int * sockets;
		bool * barrier;
		LauncherDb * db;
		int barrierCount;
};

}

#endif //DAQ_INTERNAL_LAUNCHER_SERVER_HPP
