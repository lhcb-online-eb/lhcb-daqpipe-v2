/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include <cstdlib>
#include <common/Debug.hpp>
#include "Server.hpp"

/*******************  NAMESPACE  ********************/
using namespace InternalLauncher;

/*******************  FUNCTION  *********************/
int main(int argc, char ** argv)
{
	//check args
	assume(argc == 2,"Invalid argument, you need to provide the number of expected nodes !");
	int nodes = atoi(argv[1]);
	
	//prepare server
	Server server(nodes);
	server.waitConnect(8086);
	server.run();
	server.close();
	
	DAQ_INFO("Exit server, not more clients !");
	
	//ok finish
	return EXIT_SUCCESS;
}