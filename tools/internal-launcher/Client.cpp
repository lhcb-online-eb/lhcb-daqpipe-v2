/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include "Client.hpp"
#include "Protocol.hpp"
//std
#include <cstring>
//daqpipe
#include <common/Debug.hpp>
//unix
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>

/*******************  NAMESPACE  ********************/
namespace InternalLauncher
{

/*******************  FUNCTION  *********************/
Client::Client ( const std::string& server, int rank, int port )
{
	this->rank = rank;
	this->connect(server, port, false);
}

/*******************  FUNCTION  *********************/
Client::~Client ( void )
{
	close(fd);
}

/*******************  FUNCTION  *********************/
void Client::connect ( const std::string server, int port, bool useIpV6 )
{
	struct sockaddr_in sin;
	int status;
	
	/* Allocate a new socket */
	fd = socket( useIpV6 ? AF_INET6 : AF_INET , SOCK_STREAM, 0);
	assumeArg(fd>=0, "Error while calling socket() : %1").argStrErrno().end();
	
	/* Prepare connection parameters. */
	sin.sin_family = useIpV6 ? AF_INET6 : AF_INET;
	sin.sin_port = htons(port);
	inet_aton(server.c_str(), &sin.sin_addr);
	DAQ_DEBUG_ARG("tcp","Try to connect to %1:%2").arg(server).arg(sin.sin_port).end();
	
	/* Connect to the remote host. */
	status = ::connect(fd, (struct sockaddr*) &sin, sizeof(sin));
	assumeArg(status == 0, "Error while calling connect() : %1").argStrErrno().end();
	
	//setup handshake
	Handshake handshake;
	handshake.type = INTERNAL_LAUNCHER_HANDSHAKE;
	handshake.rank = rank;
	
	//send into socket
	status = send(fd,&handshake,sizeof(handshake),0);
	assume(status == sizeof(handshake),"Failed to fully send the handshake when creating TCP connection (client side) !");
	
	//wait ack
	ReadyToStart ack;
	status = recv(fd, &ack,sizeof(ack),0);
	assume(status == sizeof(ack),"Failed to fully send the handshake when creating TCP connection (client side) !");
	assume(ack.type == INTERNAL_LAUNCHER_READY_START,"Invalid response to handshake !");
	this->worldSize = ack.worldSize;
}

/*******************  FUNCTION  *********************/
void Client::barrier ( void )
{
	//prepare command
	Command command;
	command.type = INTERNAL_LAUNCHER_WAIT_BARRIER;
	command.rank = rank;
	
	//send into socket
	int status = send(fd,&command,sizeof(command),0);
	assume(status == sizeof(command),"Failed to fully send the handshake when creating TCP connection (client side) !");
	
	//wait ack
	status = recv(fd, &command,sizeof(command),0);
	assume(status == sizeof(command),"Failed to fully send the handshake when creating TCP connection (client side) !");
	assume(command.type == INTERNAL_LAUNCHER_ACK_BARRIER,"Invalid response to barrier !");
}

/*******************  FUNCTION  *********************/
void Client::set ( const std::string key, const std::string value )
{
	//prepare command
	Command command;
	command.type = INTERNAL_LAUNCHER_SET_DB_ENTRY;
	command.rank = rank;
	strcpy(command.key,key.c_str());
	strcpy(command.value,value.c_str());
	
	//send into socket
	int status = send(fd,&command,sizeof(command),0);
	assume(status == sizeof(command),"Failed to fully send the handshake when creating TCP connection (client side) !");
}

/*******************  FUNCTION  *********************/
std::string Client::get ( const std::string key, int rank )
{
	//prepare command
	Command command;
	command.type = INTERNAL_LAUNCHER_REQ_DB_ENTRY;
	command.rank = rank;
	strcpy(command.key,key.c_str());
	
	//send into socket
	int status = send(fd,&command,sizeof(command),0);
	assume(status == sizeof(command),"Failed to fully send the handshake when creating TCP connection (client side) !");
	
	//wait ack
	status = recv(fd, &command,sizeof(command),0);
	assume(status == sizeof(command),"Failed to fully send the handshake when creating TCP connection (client side) !");
	assume(command.type == INTERNAL_LAUNCHER_RET_DB_ENTRY,"Invalid response to get db entry !");
	
	return command.value;
}

/*******************  FUNCTION  *********************/
int Client::getRank ( void ) const
{
	return rank;
}

/*******************  FUNCTION  *********************/
int Client::getWorldSize ( void ) const
{
	return worldSize;
}

}
