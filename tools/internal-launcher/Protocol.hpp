/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

#ifndef DAQ_INTERNAL_LAUNCHER_PROTOCOL_HPP
#define DAQ_INTERNAL_LAUNCHER_PROTOCOL_HPP

/********************  HEADERS  *********************/

/*******************  NAMESPACE  ********************/
namespace InternalLauncher
{

/********************  MACROS  **********************/
#define DAQ_MAX_MESSAGE_SIZE 32

/********************  ENUM  ************************/
enum Commands
{
	//flow
	INTERNAL_LAUNCHER_HANDSHAKE,
	INTERNAL_LAUNCHER_READY_START,
	//barrier
	INTERNAL_LAUNCHER_WAIT_BARRIER,
	INTERNAL_LAUNCHER_ACK_BARRIER,
	//db
	INTERNAL_LAUNCHER_SET_DB_ENTRY,
	INTERNAL_LAUNCHER_REQ_DB_ENTRY,
	INTERNAL_LAUNCHER_RET_DB_ENTRY,
};

/********************  STRUCT  **********************/
struct Handshake
{
	Commands type;
	int rank;
};

/********************  STRUCT  **********************/
struct ReadyToStart
{
	Commands type;
	int worldSize;
};

/********************  STRUCT  **********************/
struct Command
{
	Commands type;
	int rank;
	char key[DAQ_MAX_MESSAGE_SIZE];
	char value[DAQ_MAX_MESSAGE_SIZE];
};

}

#endif //DAQ_INTERNAL_LAUNCHER_PROTOCOL_HPP
