/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.5.0-dev
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
             LICENSE  : CeCILL-C
*****************************************************/

/****************************************************/
const child_process = require('child_process');
const http = require('http');
const url = require("url");
const querystring = require('querystring');
const fs = require('fs');

/****************************************************/
if (process.argv.length != 5) {
	console.error("Usage : "+process.argv[0] + " "+process.argv[1] + " {www} {reader} {file}");
	process.exit();
}

/********************  GLOBALS  *********************/
var www = process.argv[2];
var reader = process.argv[3];
var file = process.argv[4];

/****************************************************/
function run(res,id) 
{
	//console.log("Gen id = "+id+", reader="+reader);
	const bat = child_process.spawn(reader, [file,id]);
	var ret = "";
	
	bat.stdout.on('data', function(data){
		ret += data.toString();
	});

	bat.stderr.on('data', function(data) {
		ret += data.toString();
	});

	bat.on('exit', function(code){
		res.statusCode = 200;
		res.setHeader('Content-Type', 'text/plain');
		res.end(ret);
	});
}

/****************************************************/
function sendFile(res,path)
{
	//console.log(path);
	fs.readFile(path, function(err, data) {
		//console.log(data);
		if (err) throw err;
		res.statusCode = 200;
		res.setHeader('Content-Type', 'text/html');
		res.end(data);
	});
}

/****************************************************/
const server = http.createServer(function(req, res) {
	var u = url.parse(req.url);
	var params = querystring.parse(u.query);
	//console.log(JSON.stringify(u));
	if (u.pathname == "/data.json") {
		run(res,params['id']);
	} else if (u.pathname == "/" || u.pathname == "/index.html") {
		sendFile(res,www + "/index.html");
	} else {
		res.statusCode = 404;
		res.setHeader('Content-Type', 'text/plain');
		res.end("Error 404");
	}
});

/****************************************************/
server.listen(8080, "127.0.0.1", function () {
	console.log("Server running at http://127.0.0.1:8080/");
});
