Viewer usage
============

Use the viewer script generated into your build directory :

```sh
	./tools/viewer/viewer ./lhcb-daq-000000.raw
```

The open your brower onto address http://localhost:8080/.