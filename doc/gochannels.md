Go channels
===========

The Go language (https://golang.org/) provide an interesting feature with this channels to sync threads. On of the most interesting feature is also the `select` keyword to wait for multiple channels and switch to the good one quite easyly (https://tour.golang.org/concurrency/5). Thanks to this we can implement this kind of things : 

I wanted to make the same thing to ease implementation of drivers and maybe units. This can be found into src/gochannels, maybe could be good to find an already existing implementation. We can find https://github.com/ahorn/cpp-channel but it don't provide the select mechanism.

If you compile DAQPIPE with OpenPA (http://trac.mpich.org/projects/openpa/) support, the GoChannel can remove all the internal locks by using atomic queue. By default it will only remove the locking in push(). But, if you are using a single reader, you can disable all the locks by calling the setUnitReader() function. If you are using the GoChannel selector it automatically call it in subscribe() method.

Single channel usage
--------------------

A single channel can be read and write by multiple threads.

```cpp
#include <GoChannel.hpp>
#include <thread>
#include <iostream>

using namespace std;

void run(GoChannel<long> * channel)
{
	long value;
	while((*channel >> value))
		cout << "Get " << value << endl;
}

int main(void)
{
	//setup channel and launche thread
	GoChannel<long> channel;
	thread t(run,&channel);

	//enable optim when using OpenPA
	//do it only if you have a uniq reader
	//The GoChannelSelector does it automatically in subscribe()
	channel.setUnitReader();

	//feed
	for (long i = 0 ; i < 10 ; i++)
		channel << i;

	//close and wai
	channel.close();
	t.join();

	//finish
	return EXIT_SUCCESS;
}
```

Multiple channel usage
----------------------

Our implementation provide a `selector` object to wait from multiple channel on which it subscribe. This implementation only support one reader and multiple writer on each channel. Here an example : 

```cpp
#include <GoChannel.hpp>
#include <thread>
#include <iostream>

using namespace std;

void run(GoChannel<long> * channelA,GoChannel<float> * channelB)
{
	long valueA;
	float valueB;

	//register selector
	GoChannelSelector selector;
	selector.subscribe(channelA);
	selector.subscrive(channelB);
	
	//wait one channel
	GoChannelBase * channel;
	while((channel = selector.select()))
	{
		//switch on good one
		if (channel == channelA) {
			*channelA >> valueA;
			cout << "Get long " << valueA << endl;
		} else if (channel == channelB) {
			*channelB >> valueB;
			cout << "Get float " << valueB << endl;
		}
	}
}

int main(void)
{
	//setup channel and launche thread
	GoChannel<long> channelA;
	GoChannel<float> channelB;
	thread t(run,&channelA,&channelB);

	//feed
	for (long i = 0 ; i < 10 ; i++)
	{
		channelA << i;
		channelB << (float)(i*10);
	}

	//close and wai
	channelA.close();
	channelB.close();
	t.join();

	//finish
	return EXIT_SUCCESS;
}
```

Selector used with acceptor
---------------------------

When you subscribe to some channels, you can provide an `acceptor` function to only fetch messages from this channel if some conditions are fulfilled. This can help to manage the multi channel things into driver implementation.
We used the C++ lambda function as in the given example : 

```cpp
bool status = false;
selector.subscribe(channel,[&status]() {return status;});
```

You have to take care at exit that the thread is not sleeping without reading all his data. You can wake it up with another channel or ensure to be able to read all the data when calling close() as it notify the selector. You can also call the notify function on this selector to wake-up if the conditions have changed without adding new data to the channels.

A complete example which read the channelB only when all the messaged have been pushed.

```cpp
#include <GoChannel.hpp>
#include <thread>
#include <iostream>

using namespace std;

bool readyToReadB = false;

void run(GoChannel<long> * channelA,GoChannel<float> * channelB)
{
	long valueA;
	float valueB;

	//register selector
	GoChannelSelector selector;
	selector.subscribe(channelA);
	selector.subscrive(channelB,[](){return readyToReadB;});
	
	//wait one channel
	GoChannelBase * channel;
	while((channel = selector.select()))
	{
		//switch on good one
		if (channel == channelA) {
			*channelA >> valueA;
			cout << "Get long " << valueA << endl;
		} else if (channel == channelB) {
			*channelB >> valueB;
			cout << "Get float " << valueB << endl;
		}
	}
}

int main(void)
{
	//setup channel and launche thread
	GoChannel<long> channelA;
	GoChannel<float> channelB;
	thread t(run,&channelA,&channelB);

	//feed
	for (long i = 0 ; i < 10 ; i++)
	{
		channelA << i;
		channelB << (float)(i*10);
	}

	//unlock B reading
	//Caution, don't do it after close otherwise the thread is likely to stay deadlocked.
	//or ensure to wake up the selector through another channel
	readyToReadB = true

	//close and wait
	channelA.close();
	channelB.close();
	t.join();

	//finish
	return EXIT_SUCCESS;
}
```

Usage in single thread
----------------------

You can use the `GoChannel` and `GoChannelSelector` in single threaded codes by calling the `disableWaitOnEmpty()` function on the channel of on the selector. In that case it will eat all the data from the queue then exit like if you close the channel.

```cpp
int main(void)
{
	//setup channels
	GoChannel<long> channelA;
	GOChannel<float> channelB;

	//setup selector
	GoChannelSelector selector;
	selector.subscrive(&channelA);
	selector.subscrive(&channelB);
	selector.disableWaitOnEmpty();

	//feed
	for (long i = 0 ; i < 10 ; i++)
	{
		channelA << i;
		channelB << (float)(i*10);
	}

	//read all
	GoChannelBase * channel;
	while((channel = selector.select()))
	{
		//switch on good one
		if (channel == channelA) {
			channelA >> valueA;
			cout << "Get long " << valueA << endl;
		} else if (channel == channelB) {
			channelB >> valueB;
			cout << "Get float " << valueB << endl;
		}
	}
}
```

OpenPA optimization
--------------------------

GoChannel can optionaly be compiled with OpenPA which permit to use atomics and remove some mutexes to speed up the code. Install OpenPA and use --with-openpa on configure script to provide the path if not in /usr or /usr/local.