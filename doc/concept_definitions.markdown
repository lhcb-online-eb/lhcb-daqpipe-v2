Concept definitions
===================

Here we tried to defined clearly which are the main concepts used inside the DAQPIPE code.

 * **An event** correspond to a bunch interaction (collision) it is associate to a uniq event ID and the related data (spread over the Readout Units) might require in the order of 100KB to be stored.
 * **A bucket** is an aggregation of multiple event with contiguous IDs. We aggregate them in order to get in the order of 1MB or 10MB of data to exchange per Readout Unit. So a packet of 500MB (1MB * 500 nodes) in total to be received by the Builder Unit and to be transmitted to the Filter Unit.
 * **event/bucket meta data** represent structures associated to the event datas to descrive them. Mostly to provide their size. They will be transmitted in parallel of the data and stored out of it. It mostly represent a couple of integers/long per event and per readout unit.
 * **fragment** the PCI-E40 board will be seen from the user-side as two board writing in memory in parallel. So to get the event data from a readout unit we need to fetch two **fragments**.
 * **unit** it represent an entity in the system which can communicate with others throw messages (commands). They can be ReadoutUnit, BuilderUnit, EventManager.