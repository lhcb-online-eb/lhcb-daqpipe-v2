Using alternate internal launcher
=================================

DAQPIPE optionally provide an internal launcher which is just a server launched by hand then the processes need to be launched by hand and connect (via config file) to the server.

Compile
-------

At compile time you need to enable this launcher, it works with all the transport layer except MPI.

```sh
../configure --enable-internal-launcher --with-transport=PSM2
```

Config
------

Then you need to setup the config file to give the IP address of the launcher manager : 

```json
{
    "launcher": {
        "internal": {
            "server": "127.0.0.1",
            "port": 8086
        }
    }
}
```

Launch the server
-----------------

You first need to launch the server. For multiple you need to launch it again every time as it exit when daqpipe finish.

```sh
./tools/internal-launcher/internal-launcher {NUMBER_OF_NODES}
```

Launch the clients
------------------

You can launch by using ssh, static launching as service :

```sh
./daqpipe -f config.json -r {RANK}
```

An alternative is to create a wrapper to add rank option and use mpirun as dispatcher : 

```sh
#!/bin/bash

"$@" -r "${OMPI_COMM_WORLD_RANK}"
```
Then to run :

```sh
mpirun -np {NODES} --hostfile hosts ./daqpipe-rank-wrapper.sh ./daqpipe -f config.json
```