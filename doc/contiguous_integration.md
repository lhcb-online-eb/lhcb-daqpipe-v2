Continuous integration
======================

For the DAQPIPE project we use some continuous integration with Jenkins (https://jenkins.cern.ch/lhcp-daq-upgrade/) and Gitlab-CI (https://gitlab.cern.ch/svalat/lhcb-daqpipe-v2/builds).

To work with this tools daqpipe provide two helper script : 
 * [dev/run-into-jenkins.sh](/dev/run-into-jenkins.sh) which contain the needs to run the configure build and test. It require an option saying the transport to be used (LOCAL, TCP, LIBFABRIC or MPI).
 * [dev/install-local-deps-for-jenkins.sh](/dev/install-local-deps-for-jenkins.sh) which setup a usr subdirectory to install all the dependency requires in case you are not root on the system. This script can be called into jenkins to install the needs on the remote machine. This script can also use some local installation to not do it every time looking into `/opt/lhcb-daqpipe-deps` (useful for gitlab-ci as it cleanup the test directory before each run).

For gitlab-ci you also need to look on `.gitlab-ci.yml`