Failure handlong and recovery
=============================

To handle failure recovery the current prototype include a failure simulation system in the transport class which permit to simulate a dead not throwing away all the messages (command and rdma) except exit message. This can append based on a random probability setup from config file (testing.failureRate).

When a failure is detected (generated) a message UNIT\_CMD\_NODE\_FAILED is sent to everybody to notify the failure to each transport layer can update the "deadNode" array for requestes from the units.

Then each unit has to flush their state marking eventually the current event as NON\_COMPLETE. This need to be done by notifying the gather that the event is potentially not complete if the related parts have not yet been received. On the RU side we need to flush the related buffer if in wait as for the BU. Currently is seams simpler to notify the driver layer so it can flush any pending messages send sending back the ack command as for normal termination.

Then the failred node need to be ignored in all futur communications and keep all the transmitted events as NON\_COMPLETE.

For the recovery it is not yet implemented but the idea might be to first use a flush operation on the EM side to retain all the credits, then send a NODE\_REINSERT command to everybody and send again the credits.

Details on implementation
--------------------------

One can read the [report from Rafal](docs/report-rafal-failure-support.pdf).

There is still some work to do, mainly tag the data as non complete (waiting to have the PICE40 implemented to do it).