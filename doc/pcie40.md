PCIE-40
=======

To use the real data acquisition card, we need to use the `daq.h` file installed by https://gitlab.cern.ch/lhcb-daq40/lhcb-daq40-software. The header file can be found [here](https://gitlab.cern.ch/lhcb-daq40/lhcb-daq40-software/blob/master/pcie40_daq/daq.h).

And you need to link to `libpcie40_daq`.

Usage example
-------------

We can give the simple example :

```cpp
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "lhcb/pcie40/daq.h"

//compiler with gcc main.c  -lpcie40_daq

//open main stream of sub-card 0
int stream = p40_stream_open(0,P40_DAQ_STREAM_MAIN);

//map to memory
void * mem = p40_stream_map(stream);

//enable it
p40_stream_enable(stream);

//check
assert(p40_stream_enabled(stream));
assert(p40_stream_ready(stream));

//poll
loff_t base_offset;
size_t size = p40_stream_poll(stream,&base_offset,100);

//do stuff with data

//mark as free
p40_stream_free_host_buf_bytes(stream,size);

//disable
p40_stream_disable(stream);

//unmap & close
p40_stream_unmap(stream,&mem);
p40_stream_close(stream,&mem);
```

Enable generator
----------------

To enable generator mode you need to :

```sh
pcie40_daq -g main
pcie40_daq -g meta
```