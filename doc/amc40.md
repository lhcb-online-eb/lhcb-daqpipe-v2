Support carte AMC40
===================

DAQPIPE now also include a support of the AMC40 board. This board communicate to the readout PC through UDP network packet hence DAQPIPE implement the listining sockets.

The AMC40 can send two data steam : 
 * MEP (Multi Event Packet) containing the data for multiple collisions.
 * TFC (TODO : define the memory layout)

MEP packets
----------------

The MEP packets are send prefixed with the given header : 

```c
struct __attribute__((__packed__)) Amc40MEPHeader
{
	/** Define the UDP packed global sequence ID **/
	uint32_t seqnum;
	/** Define the data (no header) bytes in the packet **/
	uint16_t bytes;
	/** Define the number of fragments inside the packet **/
	uint16_t frags;
	/** Define the base event ID (first fragment) **/
	uint64_t evid;
};
```

Remark that a MEP contain data or TFC and no mix of those two. We know the type depending on the port in use as explain later.

Caution, all those fields are encoded for network so in big endian. Hence, you need to convert them in C program with `ntohl`, `ntohs` and `be64toh`.

Then this header is followed by the data fragments (frags) with a small header of 32 bits in-between (this header is encoded in little endian, so no need of conversion) : 

```c
struct __attribute__((__packed__)) Amc40MEPFragmentHeader
{
	/** Fragment data size in bits **/
	uint32_t size:20;
	uint16_t bxi:12;
};
```

The reader need to take care, each fragment is aligned to 64 bits so the size need to be incremented to jump to the next fragment.

Here a view of the dataflow :

![AMC40 Flow](images/amc40-flow.png)

Network
-------

The AMC40 can support 2 ethernet links hence we need to handle up to 2 links per board. This make 4 stream. 2 links and a data stream on one port plus the tfc on another one.

Conversion
----------

The AMC40 input stream is not compatible with the meta-data/data format from the PCI board. Hence, daqpipe provide a conversion function to simulate the pci40 format from the AMC40. Then the standard DAQPIPE functions can be used to get buckets on the data source.

Synchronisation question
------------------------

There is a synchronisation question in DAQPIPE. When DAQPIPE start it will start to receive the packet, but in this way, there is no way to synchronise the data stream on each node. And DAQPIPE do not use the collision id (eventID in AMC40 language).

The current solution is to start DAQPIPE without any data incoming, then enable the data taking with the TFC so the ACM40 cards start to send packets. This way we can ensure to have a synchronized data stream comming into DAQPIPE in all nodes.

Data/MetaData
-------------------

When converting the data stream into the Data/MetaData format compatible with the PCI-E40 board the data contain the same stream than the AMC40 data (in other words we keep the data header (Amc40MEPFragmentHeader) in the data stream. We just generate the revelant size into meta-data to help the application to manage the data stream.

Config file
--------------

To run with one process you can use the given config file : 

```json
{
        "general": {
                "dataSource": "amc40",
                "writeFile": true
        },
        "dataSource": {
                "amc40": {
                        "lines": 1,
                        "iface-rail1-1": "lo"
                }
        },
        "testing": {
                "dataChecking": false,
                "customTopology": true
        },
        "topology": [
                [ "EM", "PU", "BU", "RU", "FM", "FU" ],
                [ "BU", "RU", "FM", "FU" ]
        ],
        "filter": {
                "mode": "local"
        },
        "writer": {
                "meps": 10
        }
}
```

Then : 
 - Run daqpipe which will wait incoming data
 - Start the AMC40 data taking (or launch the emulator on each node with the same config file).