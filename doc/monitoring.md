Mintoring
======

DAQPIPE support the concept of data monitoring asked for the beam test. It consist in extracting some of the collision and send them to a monitoring process. The capture is done in the filter unit and it is sent to the monitoring process via TCP connection.

You can find more content into the related [readme](https://gitlab.cern.ch/svalat/lhcb-daqpipe-v2/blob/master/tools/monitor/Readme.md)
