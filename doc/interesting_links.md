Intersting links and pointers
=============================

This page aims to store some of the interesting links and papers which are relevant to
the topic of this project.

Slides and reports on DAQPIPE
-----------------------------

If you search for the data performance archives and all slides and report you can access them in the htcc shared repository in CernBox (https://cernbox.cern.ch/index.php/apps/files/?dir=%2Fhtcc_shared%20%28%2310482662%29%2Fdaqpipe).

For version 2 : 
 * HTCC meeting, Macroni scale test results (HTCC coffee - 18/07/2016) : https://indico.cern.ch/event/558907/
 * DAQPIPE status (HTCC coffee - 08/02/2016) : https://indico.cern.ch/event/495237/
 * Omni-Path benchmarking results from november 2015 (26/01/2016) : https://indico.cern.ch/event/491855/
 * Preparing Omni-Path testing (16/10/2015) : https://indico.cern.ch/event/455610/
 * DAQPIPE status and future in LHCb week (14/09/2015) : https://indico.cern.ch/event/382495/
 * Bechmarking results of DAQPIPE : https://svalat.web.cern.ch/svalat/lhcb-daqpipe-benchmark

For version 1 : 
 * Source code of V1 : https://gitlab.cern.ch/dcampora/lhcb-daqpipe
 * Proceeding : https://lbdokuwiki.cern.ch/\_media/upgrade:100g_proceeding.pdf
 * Published paper (RealTime 2014) : http://dx.doi.org/10.1109/RTC.2014.7097440
 * Another published paper in 2015 : http://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=7118260&isnumber=7122977

About LHCb DAQ
--------------

 * TDR for 2020 upgrade (LHCb Trigger and Online Upgrade Technical Design Report) : https://cds.cern.ch/record/1701361?ln=en
 * Infiniband Event-Builder Architecture Test-beds for Full Rate Data Acquisition in LHCb : http://stacks.iop.org/1742-6596/331/i=2/a=022008

About DAQ from other experiments
--------------------------------

 * LHC DAQ experiment second joint worshop 2016 : https://indico.cern.ch/event/471309/timetable/
 * Publication of ALICE DAQ team : http://aliceinfo.cern.ch/DAQ/public/publications-and-presentations
 * About ATLAS DAQ : http://www.atlas.ch/acquisition.html
 * Slides to check about ATLAS DAQ : http://hep.ps.uci.edu/~wclhc07/ATLAS%20TDAQ.pdf
 * TDR DAQ alice : https://cdsweb.cern.ch/record/2011297
 * Slides DAQ CMS : http://www.slideshare.net/AndreaPetrucci2/xdaqaplnlinfn27112014
 * Doc RU/BU DAQ CMS : http://cms-ru-builder.web.cern.ch/cms-ru-builder/RUBUILDER_G_V1_13_0.pdf
 * Paper from CMS : http://ieeexplore.ieee.org/xpl/articleDetails.jsp?arnumber=7097439&punumber%3D7086910%26filter%3DAND%28p_IS_Number%3A7097401%29%26pageNumber%3D2
 * Presentation from Niko : http://lhcb-doc.web.cern.ch/lhcb-doc/presentations/TalksNotDirectlyRelatedLHCb/Postcript/NikoNeufeld_rt2012.pptx
 * Joint DAQ workshop : https://indico.cern.ch/event/471309/other-view?view=standard
 * NetIO library from ATLAS to support many network : http://atlas-felix.cern.ch/netio/
 * Evaluation of InfiniBand for CBM experiment (2013) : https://indico.cern.ch/event/218156/contributions/1519982/attachments/351732/490099/CBM-IB.pdf
 * Infiniband cluster for Future DAQ: http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.161.8688&rep=rep1&type=pdf
 * InfiniBand in ALICE HLT : https://indico.cern.ch/event/218156/contributions/1519981/attachments/351731/490098/IBinALICEHLT.pdf
 * Workshop about Infiniband for DAQ (2013) : https://indico.cern.ch/event/218156/overview

Contact on other experiments:
-----------------------------

Some people working on hight speed network testing and interested in OPA & IB on the other CERN experiments:

 * Niko Neufeld (LHCb, 2017)
 * Sébastien Valat (LHCb, 2017)
 * Balazs Voneki (LHCb, 2017)
 * Flavio Pisano (LHCb, 2017)
 * Tomaso Colombo (LHCb, 2017)
 * joern schumacher (ATLAS, 2016)
 * ioannis charalampidis (ALICE, 2016)
 * adam wegrzynek (ALICE, 2016)
 * sylvain chapeland (ALICE, 2016)
 * Alexandru Grigore (ALICE, 2016)

About networking
----------------

 * MPI implementations
   * OpenMPI : http://www.open-mpi.org/
   * MPICH : https://www.mpich.org/
   * MPC : http://mpc.paratools.com/. This is a CEA implementation of MPI/OpenMP which support MPI_THREAD_MULTIPLE. Might be interesting to test.

 * New API to programm QLogick PSM/verbs/tcp from OFA : OFI/libfabric : 
   * Main website (manpages, source : libfabric, demo : fabtests) : http://ofiwg.github.io/libfabric/
   * A presentation about intel : http://downloads.openfabrics.org/downloads/WorkGroups/ofiwg/F2F_Hillsboro_2014_0819/Intel-OFA-MPI2.pptx
   * Intel webminar : http://www.intel.com/content/www/us/en/events/online.html#7,all
   * QLogick PSM API : http://filedownloads.qlogic.com/files/manual/68842/IB6054601-00G.pdf
   * A tutorial presentation : http://fr.slideshare.net/dgoodell/ofi-libfabric-tutorial
   * A quick start guide : http://fr.slideshare.net/JianxinXiong/getting-started-with-libfabric?ref=http://ofiwg.github.io/libfabric/

 * Alternative to MPI:
   * GASPI : http://www.gaspi.de/projekt.html. Interesting as it provide RDMA base messaging and support IB.

 * Benchmarks
   * IPerf3 : https://iperf.fr/ (has some issues on 100G networks)
  * Iperf2 :  https://sourceforge.net/projects/iperf2/
   * Pktgen : https://github.com/pktgen/Pktgen-DPDK
   * B\_eff (a little bit old, 2001, but looks interesting) : https://fs.hlrs.de/projects/par/mpi//b_eff/ and the paper http://icc.dur.ac.uk/~tt/b_eff.pdf

 * About InfiniBand usage : 
  * A nice glossary of IB terms : http://www.cisco.com/c/en/us/td/docs/server_nw_virtual/2-9-0_update1/2-9-0_release/element_manager/element/appA.pdf
   * A well detailed presentatation on IB from CMS : https://agenda.cnaf.infn.it/conferenceDisplay.py?confId=673
   * A good tutorial and usefull links to complete : https://blog.zhaw.ch/icclab/infiniband-an-introduction-simple-ib-verbs-program-with-rdma-write/
   * http://arxiv.org/pdf/1105.1827.pdf
   * http://gridbus.csse.unimelb.edu.au/~raj/superstorage/chap42.pdf
   * http://ieeexplore.ieee.org/stamp/stamp.jsp?arnumber=4589204
   * Infiniband spec : http://www.infinibandta.org/content/pages.php?pg=technology_public_specification

 * PSM2, the low level API for OPA:
    * Manual :  http://www.intel.com/content/dam/support/us/en/documents/network/omni-adptr/sb/Intel_PSM2_PG_H76473_v1_0.pdf
    * Routing on OPA : https://www.openfabrics.org/images/eventpresos/2017presentations/309_OmniPathFabric_RWeber.pdf

 * About RoCE (RMDA over ethernet)
    * http://www.lanl.gov/projects/national-security-education-center/information-science-technology/_assets/docs/2010-si-docs/Team_CYAN_Implementation_and_Comparison_of_RDMA_Over_Ethernet_Presentation.pdf

Interesting paper ressources : 
------------------------------------

 * Bandwidth-optimal All-to-all Exchanges in Fat Tree Networks : https://htor.inf.ethz.ch/publications/index.php?pub=165
 * Scaling All-to-All Multicast on Fat-tree networkds https://charm.cs.illinois.edu/newPapers/03-11/paper.ps
 * Fat-tree routing and node ordering providing contention free traffic for MPI
global collectives http://tx.technion.ac.il/~ezahavi/papers/contention_free_mpi_collectives.pdf
 * Scaling Alltoall Collective on Multi-core Systems https://pdfs.semanticscholar.org/ef8a/a6d52c9d1644a89bd87921e4a58d682905ee.pdf
 * Contention-free Routing for Shift-based Communication in MPI Applications on Large-scale Infiniband Clusters https://e-reports-ext.llnl.gov/pdf/380228.pdf
 * A Measurement Study of Congestion in an
InfiniBand Network : http://tma.ifip.org/wordpress/wp-content/uploads/2017/06/tma2017_paper18.pdf


About fault tolerance : 
-----------------------

 * In MPI : 
    * Paper ANL (2002) : from http://www.mcs.anl.gov/~lusk/papers/fault-tolerance.pdf
    * Recent work (2015) on top of mpich : http://www.mcs.anl.gov/publication/lessons-learned-implementing-user-level-failure-mitigation-mpich

About running on HPC clusters
-----------------------------

Job manager :
 * Slurm : https://computing.llnl.gov/linux/slurm/
 * PBS/Torque : http://www.arc.ox.ac.uk/content/pbs

Launcher libraries :
 * Hydra from mpich project : 
   * Some doc : https://wiki.mpich.org/mpich/index.php/Hydra_Process_Management_Framework
   * Usage as user : https://wiki.mpich.org/mpich/index.php/Using_the_Hydra_Process_Manager
   * Sources : inside mpich sources : https://www.mpich.org/
   * Check usage by another project (hydra/hdyra-simple) : http://mpc.paratools.com/

Tools to profile MPI applications
---------------------------------

 * Tau : http://www.cs.uoregon.edu/research/tau/users/samrai.php
 * Scalasca : http://www.scalasca.org/
 * Vampir : https://www.vampir.eu/
 * Profiling tool community for HPC : http://www.vi-hps.org/

About IB Verbs
---------------------------------
* Reference RDMA IB application: http://www.digitalvampire.org/rdma-tutorial-2007/notes.pdf
* A detailed comprehensive manual with <rdma_cma.h> and <infiniband/verbs.h> functions description: http://www.mellanox.com/related-docs/prod_software/RDMA_Aware_Programming_user_manual.pdf 
* Tutorial to use ibv - no info is given on RDMA usage: http://www.csm.ornl.gov/workshops/openshmem2013/documents/presentations_and_tutorials/Tutorials/Verbs%20programming%20tutorial-final.pdf
* Non-RDMA IB application with some explanation of IB - it lack RDMA: https://blog.zhaw.ch/icclab/infiniband-an-introduction-simple-ib-verbs-program-with-rdma-write/
* The "lseb" project - different approach to using verbs with boost - the IB functionality is provided: https://github.com/manzali/lseb

About PTP:
 * Slides from Fujutsu : https://events.linuxfoundation.org/sites/events/files/slides/lcjp14_ichikawa_0.pdf
 * Mixing PTP and NTP on rehdat : http://rhelblog.redhat.com/2016/07/20/combining-ptp-with-ntp-to-get-the-best-of-both-worlds/