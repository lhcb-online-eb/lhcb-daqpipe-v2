About the filter farm
=====================

Introduction
------------

By default DAQPIPE do not simulate the filter farm but it can be enabled by switching the `filterSubfarms` and `subFarmSize` in the `general` section of the config file.

The model will lead to the given topology :

![Filter farm topology](images/filter-farm-topo.png)

Communications
-------------

For the communication the data flow will follow :

 - At the application start all the FilterUnits registered themself to the FilterManager associated to the subfarm.
 - BuilderUnit notify the FilterManager that an event is fully fetched and aggregated
 - The FilterManager select a FilterUnit and send the ids to the BuilderUnit
 - The BuilderUnit used the ids to do RDMAWrite on the targetted FilterUnit. 
 - Once it is finished (all the peaces sent). The FilterUnit notify the FilterManager that it is free again.

Each FilterUnit support multiple credits (currently the same number of credit that for the BU).

![Filter farm topology](images/filter-farm-exchange.png)

This implementation is like a PULL protocol. I do not use a PUSH one because it is more complex by required ANY_RANK support in the postRDMWrite syntaxe and on the driver side.