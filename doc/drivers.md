Drivers
=======

This page describe a little bit what you need to provide to implement a new network driver in DAQPIPE and how to reuse some of the predefined components.

What you need to provide
------------------------

Basically you first need to extend the [Driver](src/drivers/Driver.hpp) class a provide all the virtual functions it required. In DAQPIPE, a driver need to implement at least : 
 * Functions register memory segments to the kernel and NIC for RDMA communications. (can be ignore if you don't support hardware RDMA).
 * A function to request the remote keys used for the registered segments (can be ignore if you don't support RDMA, just return address and 0 to fill the RemoteIOV struct).
 * Functions to wait and send commands. A command is a structure of a fixed size (a few bytes : ~64). You need to provide the necessary to send such a struct through the network to a remote target. You will see `postCommandRecv` function which is required for MPI and libfabric to post some receiving requests. It is not needed for non request based drivers like TCP.
 * You need to receive the incoming commands into your driver thread and automatically forward the struct to the transport object (mainly done in your thread main loop or in `updateNoThread`).
 * You need to provide two functions to handle RDMA communications (or emulate). One to make RDMA write and another one to register a notification when a remote write has been received (on the receiver side).

About RDMA notification
-----------------------

In the current implementation RDMA operations are notified to the caller unit by sending back a command structure pre-registered by the caller. In your driver you mostly need to store this command linked to the request (eg. with a std::map over ptr or uuid) and send it back when the request is finished.

Setting up connections
----------------------

At init time your driver need to established some connexions and link them to the rank ids used to identify the process. You can use the launcher object to get your rank and the related DB to store and request the necessary informations to interconnect your processes (ip, ports...)

```cpp
//get my rank
int rank = launcher.getRank();

//get my ip from system
string ip = get_my_ip_from_system();

//reg in db
launcher.set("ip",ip);

//sync db
launcher.commit();
launcher.barrier();

//use to interconnect
for (int i = 0 ; i < launcher.getWorldSize() ; i++)
	if (i != rank)
	{
		//get ip of node i
		string remoteIp = launcher.get("ip",i);
		setup_connection(remoteIp);
	}
```

Connection procedure
--------------------

If you need to setup connection pairs in a client/server mode you can reuse the approach from TCP. Considering N nodes to connect : 

 * Node 0 listen for N-1 incoming connection (you can send a small header in the pipe to say who you are)
 * Node 1 connect to node 0 as client, then listen for N-2 incoming connections.
 * Node 2 connect to node 0 and 1 as client, then listen for N-3 incoming connections.
 * Node X connect to node 0-(X-1) as client, then listen for N-X-1 incoming connections.

It is not optimal in time but simpler to implement.

Starting code
-------------

To start to implement a new driver you can start from DriverLocal or use the slot based approach which manage some request based scheduling for you if you fit to the related semantic, see next section.

Slot based driver
-----------------

In the [drivers](src/drivers) directory you can find the [DriverSlots](src/drivers/DriverSlots.hpp) class which is a base to implement the drivers managing some request objects.

This class aimed at managing a finite number of slots (requests) and to wait for them before sending new request to the low level layer. It also provides a small scheduling management by controlling the number of communication from each type.

It is configured from config file with : 

```json
{
	"driver": {
		"slots": {
			"maxOnFly": 16,
			"cmdRecver": 2,
			"maxRdmaRecv": (16-2)/2,
			"maxRdmaSend": (16-2)/2,
			"listenerSleep": 100
		}
	}
```

The model applied is to set-up a limit on number of on-fly (`maxOnFly`) communications. Then you can can set-up fine grain limits on : 

 * `cmdRecver` : configure the number of waiting requests to get commands. In MPI this is MPI_IRecv(ANY_SOURCE), quite the same in Libfabric. As we use ANY_SOURCE we only post typically 2 requests.
 * `maxRdmaRecv`, `maxRdmaSend` : set-up the number of parallel RDMA recv and send. We let rooms to always be able to run the two otherwise you can finish with some deadlock is you fill all the requests with recv and not beeing able to push any send.

Slot base driver can call the `setnoAnySource()` inside `initialize()` function if the low level driver do not support ANY_SOURCE approach. In this case the object will automatically instantiate one request per target and notify you by giving a rank to setup the command channel. You can see usage of this option in more detail in MSG mode support of libfabric.

Threading
---------

The driver need to be able to run with or without thread (or force one by checking with `MALT_FATAL` or `assume`). The threading mode must be fetched from `config.useDriverThread`. The only exception is `LocalDriver` which use `config.useLocalDriverThread`. You need to provide the methods : 

 * `startThread()` to start your threads (one or more), set-up your things and return. It is called inside transport layer just after all the objects have been created and linked together.
 * `stop()` to stop your threads or non multi-threading things if threading is disabled.
 * `updateNoThread()` used in non threaded mode to give hand on the CPU. It is mostly the body of your thread main loop or each of them if you have more than one thread. Don't block here except if you cannot accept more requests and need to flush at least one to continue. If you use GoChannel or GoChannelSelector as loop controller you can use the `disableWaitOnEmpty` function to not wait if there is nothing to read and exit the loop (see DriverSlots as example).

About postCommand
-----------------

By implementing the driver interface, you will notice the presence of the postCommand parameter on RDMA functions. Those commands are used to notify the caller that the RDMA send of recv is finished. You need to store those commands into a map indexed by uuid or ptr and use it when the related operation if finished. It is the key point to keep the asynchonous status between driver and units.

Source and destination is already setup so just transport the object to the transport layer with the SendCommand version with a single command argument.

You don't have to take care of this if you are using the DriverSlots base class which does it for you.

About commands
--------------

For the command channel (recv and sending), you just need to forward the commands you receive directly to the transport layer through the sendCommand function with a single argument. Don't try to manage the destination by yourself as it is done in DriverLocal as transport might apply some transformation on the message (to handle CMD_EXIT for example).

Todo list to provide new driver
-------------------------------

If you want to provide a new driver you need to make some changes in the DAQPIPE files to register it and set-up the build methods.

 * Add your driver code to include and set-up you driver into [https://gitlab.cern.ch/svalat/lhcb-daqpipe-v2/blob/master/src/Layout.hpp](/src/Layout.hpp).
 * Add the `#cmakedefine` entry into [/src/config.h.in](https://gitlab.cern.ch/svalat/lhcb-daqpipe-v2/blob/master/src/config.h.in)
 * Add the necessary `find_package` and `EXTRA_DRIVER` into [/src/CMakeLists.txt](https://gitlab.cern.ch/svalat/lhcb-daqpipe-v2/blob/master/src/CMakeLists.txt), see libfabric support as example.
 * If you need hydra as launcher, don't forget to setup `NEED_HYDRA` info root  [/CMakeLists.txt](https://gitlab.cern.ch/svalat/lhcb-daqpipe-v2/blob/master/CMakeLists.txt).
 * Build your files [/src/drivers/CMakeLists.txt](https://gitlab.cern.ch/svalat/lhcb-daqpipe-v2/blob/master/src/drivers/CMakeLists.txt)
 * Create your driver files in [/src/drivers/](https://gitlab.cern.ch/svalat/lhcb-daqpipe-v2/blob/master/src/drivers/) (DriverXXX.(hpp|cpp)) and inherit from `Driver` or `DriverSlots`
 * If you need an external library, add `find_package` call into [/src/CMakeLists.txt](/src/CMakeLists.txt), the require searching file into [/cmake/](https://gitlab.cern.ch/svalat/lhcb-daqpipe-v2/blob/master/cmake/) (copy paste one of the existing one) and add the necessary option into [/configure](/configure).

For testing, before running the full DAQPIPE process you can use the binaries into `src/tests` which provide simple command and RDMA ping pong on top of the DAQPIPE transport layer.

Htopml integration for comm scheduling
--------------------------------------

DAQPIPE provide an integration with htopml to extract the last 1000 communication scheduling. It is the responsibility to the driver to notify the profiling module when a communication start and finish. The work is already done inside DriverSlots so if you use it you have nothing to do. But if you inherit from Driver you have to do it yourself.

You can use any pointer of integer to identify you communications as long as it stay consistent between start and finished and being uniq until the communication if finished.

```cpp
	//to include
	#include "CommTracer.hpp"

	//in you class
	CommTracer tracer;

	//in init function
	this->useTracer = config.useTracer;

	//when starting a new comm :
	if (useTracer)
		//last booleans : 1 -> isCommand, 2 -> isSend
		tracer.startComm(&requests[id],sizeof(UnitCommand),true,false);

	//when finishing comm :
	if (useTracer)
		tracer.finishComm(&requests[id]);
```

Then you can get the communication scheduling view in htopml, see README.

![Htopml comm sched](images/htopml.jpg)

Contiguous integration
----------------------

To be integrated into the contiguous integration cycle you need to patch : 
 * [dev/run-into-jenkins.sh](dev/run-into-jenkins.sh) To add your build mode into the list.
 * [dev/install-local-deps-for-jenkins.sh](dev/install-local-deps-for-jenkins.sh) To install the new dependencies used by your driver.
 * [.gitlab-ci.yml](.gitlab-ci.yml) to register your build mode into gitlab-ci.