Gathers
=======

For the PULL protocol, you will find different PullGather implementations which are used by the BuilderUnits to manage the request scheduling and state stacking to fetch the event datas.

We currently provide two implementations : PullGatherSimple which simply send all the requests at same time in order, this is more a template implementation to validate the approach (might be removed at some point). And PullGatherSched which provide a management of scheduling limiting the number of onfly request and providing different implementation of communication ordering : 

 * simple, send from target 0 to X.
 * barrel, send by starting from current rank ID
 * rdbarrel, send by starting from a random shift
 * random, send in random order (different random every time)
