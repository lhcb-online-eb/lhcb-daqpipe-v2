Coding style
============

This document aims to get a uniform coding style over the project, it can help the developers to understand the rules which are currently used in the source code.

Indentation
-----------

Lines must be indented with tabulations. We might use the given coding rules for the different constructions : 

```cpp
namespace DAQ
{

struct MyStruct
{
	int member;
};

class MyClass
{
	public:
		MyClass(void);
	private:
		int members;
};

MyClass::MyClass(void)
{
	//for loop
	for (int i = 0 ; i < members ; i++) {
		//do stuff
	}

	//while loop
	while(true) {
		//stuff
	}

	//do while
	do {
	} while(true);

	//if/else
	if (member == 0) {
		//stuff
	} else {
		//stuff
	}

	//switches
	switch(stuff) {
		case VALUE:
			//stuff
			break;
	}
}

}
```

Namings
-------

For variables and types we use the java naming rules with upper case on first letter of words and no seperator between words. Type must start with upper case letter, variable with lower case.

Macros and enum values must be full upper case with underscore to separate words. Macros must start with DAQ_.

```cpp
//macro
#define DAQ_MY_MACRO

//enum values
enum MyEnum
{
	MY_ENUM_V1,
	MY_ENUM_V2
};

//type
class MyClass;

//global variables
MyClass gblMyObject;

//constant global variables
int cstMemorySize;

//variable
MyClass myClass;
```

File header
-----------

Each file must use the given header for C++ files : 

```cpp
/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.0.0
             DATE     : 07/2015
             AUTHOR   : AUTHOR_NAME - CERN
             LICENSE  : CeCILL-C
*****************************************************/
```

For shell or cmake scripts : 

```shell
######################################################
#            PROJECT  : lhcb-daqpipe                 #
#            VERSION  : 2.0.0                        #
#            DATE     : 17/2015                      #
#            AUTHOR   : AUTHOR           - CERN      #
#            LICENSE  : CeCILL-C                     #
######################################################
```

Code line separator
-------------------

This is not *mandadory* but make the code looking good to my point of view. Put them in snippet into your code editor to ease use. 
If you are using kdevelop/kate you can use the snippet file from [dev/kdevelop](dev/kdevelop) directory.

```cpp
/********************  HEADERS  *********************/
#include <blabla.h>

/***************** USING NAMESPACE ******************/
using namespace std;

/*******************  NAMESPACE  ********************/
namespace DAQ
{

/********************  GLOBALS  *********************/
const char * gblNames = "";

/********************  MACRO  ***********************/
#define DAQ_MAX_NODE_PER_PROCESS 8

/********************  ENUM  ************************/
enum NodeType
{
};

/********************  STRUCT  **********************/
struct NodeCommand
{
};

/*********************  TYPES  **********************/
typedef int MyInt;

/*********************  CLASS  **********************/
class MyClass
{
	public:
		MyClass(void);
	private:
		int member;
}

/*******************  FUNCTION  *********************/
MyClass::MyClass(void)
{
}

}
```