About multi-threading
=====================

The current implement can use threads to run the different components of DAQPIPE. We mostly spwan threads for : 

 * Each unit (RU,BU,EM)
 * Each network drivers (at least one thread, can use more depending on internals)

To sync the threads we don't rely too much on locking everywhere but prefer to use a shared queue approach which avoid to finish in deadlocks. To this end we currently provided a scheme similar to Go with what we called GoChannels. You can find more on this topic in [Go channels](gochannels).

Single thread run
-----------------

The approached based on Go channel also provide a nice way to run sequentially by disabling threads. The Go channel implementation (and GoChannelSelector) support a method `disableWaitOnEmpty()`. Thanks to this your main function on each object can be run only while getting some messages then exit when the queues become empty. It is mostly an issue with drivers doing active polling. The issue is not present for passive polling (see TCP approach).

In the current TransportMT it will automatically call the `startNoThread()` method on your objects (unit and drivers) instead of `startThread()` so you can call `disableWaitOnEmpty()` on all the channels and selector you are using.

Thanks to this we can manage a simple infinite loop from Transport to give CPU to each objects one after one by calling their `updateNoThread()` methods. Just ensure to net sleep or wait indefinitly in this method.

The `stop()` method stay common and be calling in threaded and non-threaded case.

Current limitation
------------------

Currently it is slower to use thread on some drivers because we go too often to the locking which make the code slower. Two ways to improve this performance : 

 * Not anymore play with the `listener` trick to get wake up all the time (eg. in DriverSlots). Need to provide a default output channel if we want to be wakeup all the time, it will avoid doing many useless allocations.
 * Use a lockfree list instead of std::queue + locks in GoChannel, it will make the implementation really more performance and permit to avoid memory allocations.