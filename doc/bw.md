Real sub-detector bandwidth
===========================

Evaluation
----------

In reality each sub-detector will have a different data bandwidth entering into the PCIE-40 cards. It is summarized here from an evaluation from rainer in summer 2017 : 

| Detector | Cards | Bandwidth per card (Gb/s) |
|----------|-------|---------------------------|
|Velo      |    52 |                         55|
|UT        |    68 |                         70|
|SCIFI     |   144 |                         46|
|RICH1     |    65 |                         71|
|RICH2     |    36 |                         71|
|Calo      |    52 |                       90.6|
|muon      |    28 |                      92.16|
|Total     |   445 |                    28706.7|

Simulation
----------

DAQPIPE provide capability to simulate this behavior with the DummyDataSource. It can be enabled via config file :

```json
{
        "dataSources": {
                "dummy":{
                        "realSize" : true
                }
        }
}
```

This automaticaly compute the percendage of node with each bandwidth depening on the number of nodes you launch.

The layout is currently in order, so the first N nodes correspond to velo, M next to UT...The N and M values are automatically rescaled depending on the number of nodes to fit in percentage to the number in the previous table.