Writing final file
==============

Enabling
-----------

To write the final file in the filter unit, you need to enable in the config file : 

```json
{
	"general": {
		"writeFile": true
	},
	"writer": {
		"meps": 20,
		"filename": "lhcb-daq-%06lu.raw"
	}
}
```

File format
--------------

The file format is described in the given picture and the structure can be included by using the   [src/writer/FileStruct.hpp](https://gitlab.cern.ch/svalat/lhcb-daqpipe-v2/blob/master/src/writer/FileStruct.hpp) file.

![File format](images/daqpipe-file.png)

Reader
---------

To read the file you can refer in the code from [/tools/reader](https://gitlab.cern.ch/svalat/lhcb-daqpipe-v2/blob/master/tools/reader):