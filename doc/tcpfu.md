Using TCP filter unit with DAQPIPE
==================================

DAQPIPE provide two kind of filter units, a `local` one using the main communication protocol of DAQPIPE. It needs to be hosted in the same global all-to-all connected network as it use the communication driver from DAQPIPE.

Or you can use the TCP version. For this you need to enable it in config file:

```json
{
    "filter": {
        "type": "tcp"
    }
}
```

You can then configure the behavior :

```json
{
	"general": {
		"writeFile": false,
		"credits": 4
	},
	"filterUnitTCP": {
		"basePort": 9600,
		"portRange": 128,
		"threads": 4,
		"hosts": [ "localhost" ],
		"daqNodes": 2,
		"storage": 16
	}
}
```

If you want to use the custom topology to place your filter unit :

```json
{
        "topology": [
                [ "EM", "BU", "RU", "PU", "FM", "FU" ]
        ]
}
```

Remark the filter unit will automatically be attached to the previous filter manager. same for the builder unit (but next).

You can then run DAQPIPE : 

```sh
./src/daqpipe -f config.json
```

Then you run the filter unit:

```sh
./tools/filter-units/filter-unit -f config.json -r 0
```

Notice the option `-r 0` which select the ID of the filter unit.

Remark that you might need to produce one config file for each sub-farm because they connect to different builder unit so need a different `filterUnitTCP.host` entry.

Don't forget you can override just one entry by profiling multiple config file :

```sh
./tools/filter-units/filter-unit -f global-config.json -f farm01-setup.json
```

Multi builder unit
------------------

As seen in the configuration file, we can provide a list of host `filterUnitTCP.hosts` such a way we can connect to different BU which are linked into the same farm.

The implementation currently use a pool of threads for every connection. On interesting fix would be to make it using epoll and using on only one pool of threads for all the target hosts.
