Pseudo code to understand the communication scheduling
====================================

Here is a pseudo code to help understanding how daqpipe schedule the communicaitons. Here we consider the MPI driver but it looks mostly the same with the others: 

```cpp
//with PPN=2 the process is either a ReadoutUnit or a BuiderUnit
//rank 0 is an event manager
if (isBuilderUnit)
{
    //there is one sender per node (if ppn=1 | ppn=2)
    //node 0 is event manager so we skip one
    int readoutUnits = nodes - 1;

    //to manage parallel
    int pending = 0;

    //setup some recv buffers
    //2 of each type to receive each fragments of the event
    char metaData[CREDITS][2][10KB * senders];
    char data[CREDITS][2][1MB * senders];

    while (!timeout)
    {
        //each credit run independently of the others
        //there is no pragma parallel for this in the code but it can be understood like this for the semantic
        #pragma omp parallel
        for (int c = 0 ; c < CREDITS ; c++)
        {
            //we receive the event ID assignement via a command from the event manager (rank 0).
            MPI_recv(cmd,64 bytes,0);
            //we wait the cmd to be received
            //this could be a waitall here but the code is unfoled, in practice all the MPI test append at the same place
            //which require a TestAny to not block the rest of the code (event based)
            while(!MPI_TestAny()){};
            event = cmd.nextEvent;
            pending = 0

            //we have to gather all the fragments for the given event, from each sender
            for (int step = 0 ; step < readoutUnits ; step++)
            {
                //I'm using a barrel shifting here so node 1 start to request from 1, then 2
                //node 2 start from 2 then 3..... this avoid (in theory) that everybody rush on 1 first.
                int ru = (step+rank) % readoutUnits;

                //post recievers
                MPI_irecv(&metaData[c][0][sender * 10KB],10KB, rank[ru])
                MPI_irecv(&metaData[c][1][sender * 10KB],10KB, rank[ru])
                MPI_irecv(&data[c][0][sender * 1MB],1MB, rank[ru])
                MPI_irecv(&data[c][1][sender * 1MB],1MB, rank[ru])

                //send the request command with infos to sender
                cmd.type = CMD_REQUEST_EVENT_FRAGMENTS;
                cmd.event = event;
                cmd.infos = ..... //address for rdma if not MPI, ids, tags....
                MPI_isend(cmd,64 bytes, rank[ru]);

                //we manage the number of pending communications
                pending ++;
                if (pending >= PARALLEL)
                {
                    while(!MPI_TestAny()) {};
                    pending--;
                }
            }

            //we wait all to be finished
            while (pending > 0)
            {
                    while(!MPI_TestAny()){};
                    pending--;
            }
        }

        /*
        with credit 1 we ensure we communicate with PARALLEL node max.
        with credit 2, this is between PARALLEL and 2*PARALLEL due to async between credits
        */
    }
} else if (isReadoutUnit) {
    //local big buffers
    char data[2][512 * 1MB];
    char metaData[2][512 * 10KB];

    while (!timeout)
    {
        //we post a recv for a command
        MPI_irecv(&cmd,64,MPI_ANY_TAG,MPI_ANY_SOURCE);

        //we wait the command
        while (MPI_TestAny() != cmdRequest) {};

        //we have something to do
        //this is an abstraction but can be understood like this
        int id = cmd.event % 512;

        //we send the 4 fragments to the reciver node who make the request
        MPI_isend(&metaData[0][id * 10KB], 10KB,cmd.targetRank,cmd.tagMeta0);
        MPI_isend(&metaData[1][id * 10KB], 10KB,cmd.targetRank,cmd.tagMeta1);
        MPI_isend(&data[0][id * 1MB], 1MB,cmd.targetRank,cmd.tagData0);
        MPI_isend(&data[1][id * 1MB], 1MB,cmd.targetRank,cmd.tagData1);
    }

    /*
    The readout unit do not control how many nodes he communicate with, this is all done
    by controling the pending requets on receiver side.
    If the recievers start to become too async it can append that the sender has to communicate
    with more nodes. But the app try to keep this balance (in theory)
    */
}
```