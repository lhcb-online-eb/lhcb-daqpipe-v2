Configuration documentation
===========================

Here you can find some documentation about the configuration file. You can find
and example in the main README of the project.

General section
-------------------

This section contain the global parameters which are used by DAQPIPE in any case to control the core part of DAQPIPE.

```json
{
    "general": {
        "credits": 1,
        "ruStoredEvents": 1024,
        "eventRailMaxDataSize": 1048576,
        "collisionSize": 100,
        "runTimeout": 5,
        "bandwidthDelay": 2,
        "mode":"PULL",
        "gatherType": "sched", //sched | simple
        "dataSizeRandomRadio": 10,
        "dataSource": "dummy",
        "writeFile": false,
        "gatherTimer": 0,
        "startDelay": 1
    }
}
```

It contains :


*  **Credits**: From 1 to 32, it gives the number of on-fly events to be managed by the builder units. On IB a good value is 2-3 but it has to be explore depending on the number of on-fly communication we allow.
* **ruStoredEvents**, define the number of event stores into the readout unit (buffering size). In the final system it will define the size of the segment to fetch the data from the PCIe-40 card.
* **eventRailMaxDataSize**, define the maximum data size of an event considering one rail. A good value is 512KB, 1MB or 2MB. With some drivers it is better to aligned this size on the page size (4KB).
* **collisionSize**, define the size of a data fragment of a collision. We will typically use 100 bytes here. Deviding *eventRailMaxDataSize* by *collisionSize* define the number of collisions stored into an event.
* **runTimeout**, define for how long DAQPIPE will run (equivalent of option -t given to DAQPIPE). You can use 0 for infinite.
* **bandwidthDelay**, time interval between two bandwidth outputs.
* **mode**, Define the DAQPIPE running mode, can be PULL (default), PUSH or PULL_NO_EM.
* **gatherType**, Define the type of gether scheduling to be used. Can be **sched** (which manage the communication scheduling with various alorithms) or **simple** which send all the requests in one go and let the network managing the scheduling.
* **dataSizeRandomRadio**, define the variability of the data size we generate. By default 20%. (fragment size can be up to 20% smaller than the maximum size)
* **dataSource**, define which data generator or reader to use. Can be *dummy* (fake data generator) or *amc40*.
* **writeFile**, Enable or disable the file writing.
* **gatherTimer**, enable usage of timings in DAQPIPE gather schduling. If non 0, will send a new message every X nano second.
* **startDelay**, usefull when using timers to be sync, wait 1 second to start.

Datasource
--------------

```json
{
 "dataSource": {
        "amc40": {
            "packetMaxSize": 9216,
            "dataPort": 3600,
            "tfcPort": 3601,
            "lines": 2,
            "iface-rail1-1": "eth0",
            "iface-rail1-2": "eth1",
            "threads": 4
        }, 
        "dummy": {
            "threads": 1,
            "realSize": false
        }
    },
}
```

Configure the various data sources, could contain amc40 config : 

* **packetMaxSize** Maximum size of a packet to be received (default is jumbo frames)
* **dataPort** Port on which to recieve the data stream
* **tfcPort Port on which to recieve the TFC stream
* **lines** How many links we have incoming from the AMC40
* **iface-rail1-{X}** Define the network interface of each lanes starting from 1.
* **threads** How many threads we use to fetch the data and convert them to have the same layout than the PCIe-40.

It also permit to configure the *dummy* data source  : 

* **threads**, define the number of threads to be used for the data generation.
* **realSize**, simulate the bandwidth imbalance in all readout units by using the expected size. Size configuration is hardly defined in the code (see DummyDataSource.cpp).

Filter
------

Define the filter unit topology.

```json
{
   "filter": {
        "type": "local",
        "credits": 1,
        "subFarms": 0,
        "farmSize": 0,
        "mode": "farms"
    },
}
```

It contains:
* **type** Type of units to use. You can choose within `local` or `tcp`. 
* **credits**, define number of credits to be used inside filter unit. If using FilterUnitTCP, it should be a multiple of number of threads.
* **subFarms**, define how many sub-farms we are using. It create one filter manager for each.
* **farmSize** define how many nodes we have in a farm.
* **mode** Define the topology to be used, can be *farms* to enable sub-farm approach or *local* to use a filter manager and filter unit close to each builder unit.

Gather
-------

Configure the gather algorithms : 

```json
{
    "gather":{
        "sched":{
            "parallel":2,
            "policy": "barrel"
        }
    },
}
```

It contains the configuration for the *sched* algorithm which defines : 

* **parallel** : number of communication to request at the same time on the network.
* **policy** : define the scheduling policy to be used, can be *barrel* to use barrel shifting, *random*, *xor* or *simple*

Debug
--------

Define the debug verbosity (equivalent of -v option from daqpipe) : 

```json
    "debug": {
        "show": ["amc40", "udp"] 
    },
```

Testing
---------

It defines options to be used for performance and test exploration.

```json
{
    "testing": {
        "skipMemcpy": false,
        "skipMeta": false,
        "useUnitThreads": false,
        "useDriverThreads": false,
        "useLocalDriverThreads": true,
        "useTracer": false,
        "oneWay": false,
        "realisticMemcpyRate": 0,
        "sameSegment": false,
        "failureRate": 0,
        "customTopology": false,
        "dataChecking": false,
        "warmup": 0,
        "EventManagerAlone": true,
        "profile": false
    },
}
```

It contains : 
* **skipMemcpy** : to not call the local memcopy to explore performance inpact as they are slower than 100g networks.
* **skipMeta**: To not transfert the meta-data to evaluation possible effects on network.
* **useUnitThreads** : Run each unit in its own thread (disabled by default). This seems to provide bad performances.
* **useDriverThreads** : Run the driver in its own threads. Needed only for the TCP driver.
* **useLocalDriverThreads** : Run the local driver in its own thread. Required for better performances.
* **useTracer**: Enable communication tracing, usefull when using htopml.
* **oneWay**:  Run only one task per process so we can run only bu or ru per node.
* **realisticMemcpyRate**: Do only a fraction of the memcopy to go faster as if we run on 500 nodes. 0 to disable.
* **sameSegment**: Use only one segment for all communication to detect possible network optimization (to be removed).
* **failureRate**: Generate some fake software failures and try ton continue to run.
* **customTopology** : Do not use the default unit topology but load it from the config file (*topology* definition).
* **dataChecking**: Check the recieved data for correctness. Disable automatically in non debug mode and enabled in debug mode.
* **warmup**: skip the display of N first bandwidth printing considered as warm up.
* **EventManagerAlone**: When running with multiple processes per rank, when enabled, it make such that the event manager is alone on the first node without any other unit there. It is automatically disabled when running on one node to ease debugging.
* **profile**: Enable printing of global bandwidth and average rate. This require message send from RU to profiling unit. Disabled by default to not impact performance. Really interesting only when using real size to get the average view.

Driver slots
--------------

Configure the communication scheduling of the libfabric, PSM2, verbs and MPI drivers.

```json
{
 "drivers": {
        "slots": {
            "maxOnFly": 18,
            "cmdRecver": 2,
            "maxRdmaRecv": 8,
            "maxRdmaSend": 8,
        },
    }
}
```

It contains : 

* **maxOnFly** Total number of pending communications. Ideally it should be the sum of the other parameters.
* **cmdRecver** Number of posted receiving requests for commands.
* **maxRdmaRecv** Maximum number of posted recieving requets for rdma (has a real impact only in MPI). Better to use a big value here.
* **maxRdmaSend** Maximum number of sending requests for rdma.

Driver libfabric
------------------

This defines the parameters for the libfabric network driver : 

```json
{
    "drivers": {
        "libfabric": {
            "first port":4444,
            "port range": 1024,
            "ep type": "RDM",
            "iface": "",
            "provider name": ""
        },
}
```

It contains : 

* **first port** Define the first base port to use.
* **port range** How many ports we can use from the base.
* **ep type** Define the endpoint type to be used. *RDM* or *MSG*
* **iface** Define the interface name.
* **provider name** Define the provider name if needed to force it.

Driver TCP
-------------

Define the config for the TCP driver : 

```json
{
    "drivers": {
      "tcp" : {
            "iface" : "eth0",
            "ipv6" : false,
            "portBase" : 2000,
            "portRange" : 256,
            "rdmaChannels": 1,
            "rdmaThreads" : 8,
            "cmdThreads": 8
        },
    }
}
```

* **iface** Define the network interface to be used.
* **ipv6** Enable ipv6 support
* **portBase** First port to be used
* **portRange** Maximum number of port to be used from base.
* **rdmaChannels** How many ports we dedicated on each process for the rdma transfers.
* **rdmaThreads** Number of threads to handle the rdmas (mostly the related memcopy)
* **cmdThreads** Number of threads to handle the commands.

Driver UDP
-------------

Configure the UDP network driver : 

```json
{
    "drivers": {
        "udp": {
            "ipv6": false,
            "portBase": 2000,
            "portRange": 512,
            "cmdIface": "eth0",
            "rdmaIface": "eth0",
            "rdmaChannels": 4,
            "rdmaThreads": 4,
            "cmdThreads": 4,
            "packetSize": 9000,
            "delay": 1000,
            "kernelBuf": 1677721
        }
    }
}
```

* **ipv6** Enable IPV6 support
* **portBase** Define the base port to use.
* **portRange** How many ports to use from base.
* **cmdIface** Network interface to be used for the command channels.
* **rdmaIface** Network interface to be used to the RDMA channels.
* **rdmaChannels** Number of channels for the RDMA (reciving ports and threads)
* **rdmaThreads** Number of sender threads for RDMA.
* **packetSize** Define the packet size to be used (without accounting the header).
* **delay** Add a delay after sending the last RDMA UDP packet before sending the acknowledgement on the TCP command channel.
* **kernelBuf** Setup the buffer size of the rdma channels.

Driver VERBS
------------

Configure the VERBS network driver : 

```json
{
    "drivers": {
        "verbs": {
            "ipv6": false,
            "portBase": 2000,
            "portRange": 256,
            "iface": "ib0",
            "resolveTimeout": 5000,
            "listenBackLog": 128,
            "totalSGEs": 8
        }
    }
}
```

* **ipv6** Enable IPv6 support to access the ib interface (might be false is most cases, true never been tested).
* **portBase** Base port number to be used to setup connections
* **portRange** Use port in given range starting from portBase (apply a modulo to get a different port in close processes).
 * **iface** Name of the network iterface to use. It should have an IP assigned to setup connection.
 * **resolveTimeout** Failed if took too long to resolve remote node verbs address.
 * **listenBackLog** When listening for conneciton, how many connection to backlog.
Ideally this should be number of processes to launch to be sure no-one is rejected while connecting.
 * **totalSGEs** Max number of segments to be attachable to the queue-pairs (TODO give more details on this).

Binding
---------

Define on which core to pin the threads.

```json
{
    "binding": {
        "netDevice": "eth0"
    },
}
```

* **netDevice** pin the threads close the the given network device.

Topology
-----------

When enabling *testin.customTopology* it defines which units to place where. This is defined as an array or nodes and each node contain an array of unit to host.

```json
{
    "topology": [
        [ "EM" ],
        [ "BU", "RU", "PU" ],
        [ "BU", "RU" ],
        [ "BU", "RU" ],
        [ "FM", "FU" ],
        [ "FU" ],
        [ "FM", "FU" ],
        [ "FU" ]
    ],
}
```

The unit names are : 
 * EM : Event Manager (you need one exactly)
 * BU : Builder Unit
 * RU: Readout Unit
 * PU: Profiling Unit (you need one exactly, preferably not on the same node than the event manager).
 * FM : Filter manager (you need one per filter farm) [optional]
 * FU: Filter Unit [optional]

Writer:
--------

```json
{
   "writer": {
        "meps": 20,
        "filename": "lhcb-daq-%06lu.raw",
        "threads": 4,
        "maxBuffers": 32
    },
}
```

Configure the file writing.

* **meps** How many multi event packet (events) we pus into one file.
* **filename** Template to generate the filename using printf format with one argument (the id).
* **threads** Number of threads to convert the data and writing into files.
* **maxBuffers** How many buffers to enqueue the requests.

Internal launcher
----------------------

```json
{
    "launcher": {
        "internal": {
            "server": "127.0.0.1",
            "port": 8086
        }
    },
}
```

* **server** Define the server running the launcher manager.
* **port** Define the port to be used by the launcher manager to accept incoming connection. Also for the client to know where to connect.

Monitor
----------

```json
{
    "monitor": {
        "ipv6": false,
        "clients": 2,
        "ip": "",
        "ratio": 1000
    }
}
```

Configure the monitor client in DAQPIPE. This is used to extract partial data to be send to a monitoring process.

* **ipv6** Enable ipv6.
* **clients** For the monitoring process, how many clients we expect.
* **ip** If empty disable monitor, if defined, provide the server IP of the monitoring process.
* **ratio** Send on event on every *raito* events.

TCP filter unit
---------------

This part configure the tcp implementation of filter unit (selected via `filter.type`.

```json
{
    "filterUnitTCP": {
        "basePort": 9600,
        "portRange": 128,
        "threads": 4,
        "host": "localhost",
        "storage": 16
    }
}
```

It contains: 
* **basePort**: Base port to be used, then it increment one per thread
* **portRange**: Limit range of ports and loopback when reaching this limit.
* **threads**: Number of threads to be used. Caution, filter credits should be a multiple of this number to well used the ressources.
* **host": Hostname to which we want to connect (BU).
* **storage**: Number of event to store as buffers. Ideally should be at least 2 times number of credits to fetch one while we use one.
