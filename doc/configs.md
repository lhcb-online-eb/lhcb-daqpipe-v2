Special configuration
===================

This page provide some configuration example for some paricular situation that DAQPIPE can handle as corner cases.

Multiple OPA devices
--------------------------

For some tests we wanted to put multiple omni-path cards in the same node. To handle this case you need to use the given configuration by defining the interface to use for each rank (processes). Here an example considering 4 processes and 4 cards per node and using 2 nodes :

```json
{
    "drivers": {
        "libfabric": {
            "iface": "{rank}",
            "ifaces" : [
                "ib0", 
                "ib1", 
                "ib2", 
                "ib3", 
                "ib0", 
                "ib1", 
                "ib2", 
                "ib3"
            ]
        }
    }
}
```

Then run : 

```sh
mpirun -np 8 --map-by ppr:4:node ./daqpipe -f config.json
```

Using this way on Omni-Path will let the PSM2 driver to use shared memory for local communications, if you want to force the usage of the devices, according to the [PSM2 manuel](http://www.intel.com/content/dam/support/us/en/documents/network/omni-adptr/sb/Intel_PSM2_PG_H76473_v1_0.pdf). Also need to disable the PSM thread pinning if you use two boards per NUMA node as it pin the threads on the same core.

```sh
PSM2_DEVICES="self,hfi" HFI_NO_CPUAFFINITY=1 mpirun -np 8 --map-by ppr:4:node ./daqpipe -f config.json
```

Using internal launcer in place of hydra
-------------------------------------

If you don't whant to use the hdyra launcher for all drivers expept MPI, you can use the internal one.
In that case, you need to compile daqpipe with :

```shell
	cmake .. -DTRANSPORT=XXXX -DENABLE_INTERNAL_LAUNCHER=ON
	#or
	../configure --with-transport=XXX --enable-internal-launcher
```

Then, you need to launch the server by using :

```shell
	./tools/internal-launcher/internal-launcher {NB_CLIENTS}
```

Then you need to configure the IP of the driver server for DAQPIPE : 

```json
	{
		"launcher": {
			"internal": {
				"server": "127.0.0.1"
			}
		}
	}
```

Then you need to provide the rank (starting from 0) while lauching each daqpipe process :

```shell
	./daqpipe -f config.json -r {RANK}
```