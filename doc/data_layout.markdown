Data layout
===========

This page aims to help understanding the data layout and transferts which append inside DAQPIPE.

Full data flow
--------------

Here the full data flow from detector to filter units: 

![Data flow](images/flow.png)

Data transferts between ReadoutUnit and BuilerUnit
--------------------------------------------------

This picture can help to understand how the datas are exchanged beteen ReadoutUnit and BuilderUnit by taking care
of the interaction with the incomming PCIE40 board.

![Data exchanges](images/data_layout.png)

Data transfers
--------------

![Data exchanges](images/comms.png)

Pull protocol
-------------

![Pull protocol](images/pull.png)

Push protocol
-------------

![Push protocol](images/push.png)

Buffer uage
-----------

![Buffer usage](images/read-write-buffers.png)
