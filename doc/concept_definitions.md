Concept definitions
===================

Here we tried to defined clearly which are the main concepts used inside the DAQPIPE code.

 * **A collision** correspond to a bunch interaction (collision) it is associate to a uniq collision ID and the related data (spread over the Readout Units) might require in the order of 100KB to be stored.
 * **An event** is an aggregation of multiple collisions with contiguous IDs. We aggregate them in order to get in the order of 512Kb or 10MB of data to exchange per Readout Unit. So a packet of 500MB (1MB * 500 nodes) in total to be received by the Builder Unit and to be transmitted to the Filter Unit.
 * **event meta data** represent structures associated to the collisions of an event datas to descrive them. Mostly to provide their size and ID. They will be transmitted in parallel of the data and stored out of it. It mostly represent a couple of integers/long per event and per readout unit.
 * **bucket** It is the aggregation of the meta data and data (2 for each as we see the board as 2 input). It is used internally to answer event requests. A bucket contain 4 segments to send throught the network.
 * **unit** it represent an entity in the system which can communicate with others throw messages (commands). They can be ReadoutUnit, BuilderUnit, EventManager.

How DAQPIPE measures speed
-------------------------------------
   * It's the ReadoutUnit that handles the measurements with the help of the Throughput class (onWriteFinished)
   * Each measurement starts when the RU starts writing data
   * The measurement is printed when all pieces ( 2 data segments + 2 metadata segments) of an event are accounted for. At that time we know the total amount of data.
   * The measurement is printed periodically, depending on the bandwidth delay. By default this is every 2 seconds.
   * The rate should be read as "the speed achieved for transferring data for *this* event" - i.e. it is only an accumulation of the time and data for that one event.
   * Since there is an RU on each BU, there exists same node transfers. These are not counted towards the reported speed, i.e. only external node transfers are counted.
   * The speed includes the time (but not data) to take care of the orchestration of transferring one event.
   * For an ideal measurement, you would use -t X seconds to run longer than the default 5 seconds. You also need to take the mean or median of the printed values plus error bars. The first measured rate may be discarded to discount the start and the time to sync.
   * Km/s or Kmsg/s means "Kilo messages per seconds", i.e. how many messages that were sent on the network and find if there is a limit.
   * The data segments are only initialized in Debug mode (fillForCheck). In Release mode, the data segments are not written - whatever data is there is what we'll send -> may be zeroes.