Details on units
================

Event manager
-------------

The event manager is responsible of the management of the credit affection to builder units. On start it doesn't do anything apart waiting receiving the UNIT_READY signal from all units.

When all the units are ready the event manager start to send all the credits to the builder units (PULL) or readout units (PUSH). It does all in one for this step, it could be improve by adding some delays.

When a builder unit finished to receive an event it send REG_CREDIT to the event manager which send back the credit affection to the builder unit (PULL) or readout units (PUSH).

Readout units
-------------

The readout unit is basically under the control of the event manager (PUSH) or builder units (PULL). On startup it register (to be removed) his internal segments to the builder units and wait the registration of builder unit segments (for PUSH protocol, to know where to write).

When all the segment addresses have been exchanged, the unit send the UNIT_READY signal to the event manager and wait instructions.

In PULL mode, the readout unit will get PULL_REQUEST_WRITE from builder unit and need to send the requested event ID on the addresses provided by the request. It will get back a PULL_WRITE_FINISHED signal from the driver when the sending is finished to mark the event as send and letting the PCI board reusing the segment for another event.

TODO : PUSH

Builder unit
------------

On startup the builder unit exchange its segment addresses with the readout units for the PUSH protocol. (need to remove the receiving of RU buffers). When the addresses have been exchanged, the unit become ready and notify the event manager.

In PULL mode, the event manager will send PULL_PUBLISH_CREDIT to the builder unit so it start a new gather operation for the given credit and eventID. The gather class will ensure the management of the requests to the readout unit and tracking the progress states. Remark that the unit will receive the PULL_WRITE_FINISHED to be forwarded to the gather object as the commands are received by the unit (it can be avoided if we use std::function instead of playing with commands to notify the caller, but in that case we need to take locks everywhere due to potential usage of threads in drivers).

When the gather operation is finished, the builder unit will receive PULL_GATHER_FINISHED from the gather class and notify the event manager with REG_CREDIT.

TODO : PUSH

Profile unit
------------

DAQPIPE now handle a Profile Unit (PU) to be used to aggregate some global statistics from all nodes and to report them to htopml modules. It is a really simple unit only receiving messages (PROFILE) to update it's internal data state. This unit is by default created into node 1 so we can also get traffic view in htopml which is not the case if we host it in node 0 (as this node don't do any data exchanges, only commands).

About PULL_NO_EM
----------------

This mode mostly ignore the event manager. In this case, the start procedure is based on `UNIT_CMD_PULL_NO_EM_START` to be sent to builder units. Then the builder units will make the pull requests to readout units using a round robin computation of the eventIds and never send back the credits to the event manager. To manage exit uses the same protocol than standard PULL mode, the builder units send again the credits to the event manager which will capture them until completion and then send exit message to everybody.
