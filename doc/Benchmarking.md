Benchmarking
============


Setting up the environnement
----------------------------

In order to ease benchmarking, daqpipe V2 provide a nice series of script.

First download the [setup.sh](benchmarking/runner/setup.sh) file into [benchmarking/runner](benchmarking/runner), put it into a new empty directiry and run it from there :

```shell
 ./setup.sh
```

This script will automatically download and install all the dependencies, some external benchmark and the two versions of daqpipe.

Configuring
-----------

Then you can tunned the config file [benchmarking/runner/config.example.json](config.json) depending on you needs.

Depending on you needs, you might need to patch the wrapper scripts to call mpirun/hdyra, for example to handle a job manager. Look in [./lhcb-daqpipe-v2/benchmarking/runner/scripts](benchmarking/runner/scripts).

Running
-------

Now you can run by using the run script : 

```shell
 ./run.sh
```

You can then connect your brower to http://localhost:8080 to get the online web GUI.

![WEB online bench GUI](images/bench-screenshot-online-gui.png)

Generating webreport
--------------------

Once your run is done, you can download the generated directory with all the data into your local computer, then go into the [webreport](benchmarking/webreport) subdirectory and run (you need an internet connexion) :

```
npm install
npm install grunt-cli
bower install
node ./gen-all ../ > app/scripts/daqpipe-bench.js
```

You can then run the grunt server :

```
grunt server
```

Or generate the static files :

```
grunt build
cp app/scripts/daqpipe-bench.js dist/scripts/
```

![Bench web report](images/bench-screenshot-webreport.png)