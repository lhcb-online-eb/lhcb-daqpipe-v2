Code layout
===========

This page aimed at describing shortly how the code is structured in term of files and class diagram.

UML class diagram
-----------------

A simplified version of the UML diagram (not all class) :

![UML class diagram](images/uml.png)

The full version :

![UML class diagram, full](images/uml-full.png)

File structure
--------------

The root directory contains :

 * **cmake** : This directory contain all the cmake script to be used by the 
    project. Mostly the find scripts and some macros for unit testing.
 * **dev** : This directory contain some scripts to be used by developpers to 
   maintain some aspects of the code.
 * **doc** : This is a copy of the project Wiki, please do not edit the 
   documentation here as it is overriden some times with de wiki one.
 * **extern-deps** : Store some of the external dependencies which must be 
   embeded in the project. Typically `gtest` as they to not provide `make 
   install` anymore.
 * **src** : Contain all the sources of the project.
 * **ubenchmark** : Contain some micro benchmark files, mostly for MPI.

The `./src` subdirectory is divided as : 
 * **common** : Provide most of the common function used by the project (debug, 
   timers...)
 * **drivers** : Provide de low level network implementation (MPI, TCP...)
 * **gather** : Provide the gather implementation to control the exchange 
   sequence between ReadoutUnit and BuilderUnit.
 * **launcher** : Contain the launchers (MPI, hydra, Local) to be used to init 
   the program on multiple nodes.
 * **portability** : Abstract usage of some OS dependent functions to ease port 
   on other plateforms.
 * **transport** : Define and implement the transport layer responsible of the 
   message exchanging between units.
 * **units** : Implement the unit entities (Builder, Readout, EventManager) which 
   communicate with commands dispatched by the transport layer.
 * **datasource** : Define and implement the data source. For benchmark we use a dummy implementation, we will provide GPU and the real PCI-E40 one to connect to the kernel module from lhcb board.
 * **htopml** : This directory contains the optional htopml plugins.
 * **writer** : it contains the files used to write the final file into the filter unit.
