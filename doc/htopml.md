# Htopml        
___________
___________


__Htopml__ (https://github.com/svalat/htopml) is a light profiling tool which provides a framework to visualize in html/javascript 
data exported from DAQPIPE. 

Some screenshots of Htopml plugins.    
* __Home__     
![home](images/home.png )   

    
    
    

* __Top__    
![top](images/top.png)
The first graph (on the top of the page) shows the usage of CPU of the launched processes 
running in parallel, and the second graph (on the bottom of the page) the total usage of CPU.

    
    
    

* __Binding__   
![binding](images/binding.png)
This entry uses hwloc (Portable Hardware Locality) to display the location and binding of threads. 

    
    
    

* __Pull gather__    
![pullGather](images/pullGather.png)
Displays the usage of the buffers. 

    
    
    

* __Communication tracer__    
![commTracer](images/commTracer.png)
Provides a view of the communication scheduling.

    
    
    

*__Histogram__     
![histogram](images/histogram.png)
This entry shows the distribution of time in the communications. 

    
    
    


*__Bandwidth__    
![bandwidth](images/bandwidth.png)
This plug-in contains 2 graphs.  The graphic on the top shows the bandwidth distribution over
nodes, while the bottom graph displays the aggregated bandwidth of all the nodes over time. 

    
    
    

* __Balance__     
![balance](images/balance.png)
This plugin displays th number of events built in the different BU's. 

    
    
    

*__Layout__     
![layout](images/layout.png)
This plugin shows the internal configuration of the different nodes or ranks that conform the 
DAQ network, i.e. (BU, RU, EM, PU, FU or FM) and the state (functional or failing) of a node
when the failure simulation mode of DAQPIPE  is active



    
    
    

* __Gather histogram__    
![gatherHistogram](images/gatherHistogram.png)
Histogram of the gathering times. 


    
    
    

* __Filled percentage__    
![filledPercentage](images/filledPercentage.png)
Shows the filled percentage of the buffers. 


    
    
    

*__Omnipath__ 
![omnipath](images/omnipath.png)





