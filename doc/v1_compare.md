Grid to compare to V1
=====================

Here a synthetic grid to get status of V2 compared to V1 : 

| Function         | V1                  | V2                 |
|------------------|---------------------|--------------------|
| MODE             |                     |                    |
|  + pull          |  Buggy              | DONE / Default     |
|  + pull-timing   |  MISSING            | DONE               |
|  + pull-no-em    |  MISSING            | DONE               |
|  + push          |  DONE / Default     | DONE               |
| DRIVERS          |                     |                    |
|  + IB            |  DONE               | DONE               |
|  + tcp           |  DONE               | DONE               |
|  + udp           |  DONE               | DONE               |
|  + mpi           |  DONE / Default     | DONE               |
|  + local         |  ?                  | DONE / Default     |
|  + libfabric     |  DONE (need threads)| DONE               |
|  + rapidio       | MISSING             | DONE               |
|  +psm2           | MISSING             | DONE               |
| COMM SCHED       |  STATIC             | POLICIES           |
|  + barrel        |                     | DONE / Default     |
|  + random        |                     | DONE               |
|  + all in one    | DONE / Default      | DONE               |
| DATA CHECKING    | ?                   | DONE               | 
| HTOPML integr.   |                     | DONE               |
| HWLOC THR BIND   |                     | DONE               |
| LINES            | 21065               | 27811              |
|  + tests         | ?                   | 3236               |
|  + assert/assume | 137                 | 668                |
|  + comments      | 5051                | 6000               |
|  + driver        | 15752               | 9400               |