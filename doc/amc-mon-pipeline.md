Running the full pipeline (AMC40 + daqpipe + monitor )
================================================

The goal of this tutorial is to run the full daqpipe stack using the AMC40 emulator, dumping to file and using the monitor to extract partial data. We will consider the basic case with only one readout unit, so no real event building.

Install
--------

Build and install daqpipe using no deps (run with only one process, so one readout unit)

```sh
PREFIX=$HOME/usr-daqpipe

git clone https://gitlab.cern.ch/svalat/lhcb-daqpipe-v2.git

cd lhcb-daqpipe-v2
mkdir build
cd build
../configure --prefix=$PREFIX
make
make install

export PATH=$PREFIX/bin:$PATH
export LD_LIBRARY_PATH=$PREFIX/lib:$LD_LIBRARY_PATH
```

Config file
-------------

You need to setup the config file (config.json) like this :

```json
{
        "general": {
                "writeFile": true
        },
        "monitor": {
                "clients": 1,
                "ip": "127.0.0.1",
                "ratio": 10000
        },
        "filter": {
                "mode": "custom",
                "subFarms": 1,
                "farmSize": 1
        },
        "writer": {
                "meps": 10
        },
        "testing": {
                "customTopology": true,
                "skipMemcpy": false
        },
        "topology": [
                [ "EM", "BU", "RU", "PU", "FM", "FU" ],
        ]
}
```

Run demo monitor
------------------------

You need to run it before launching daqpipe as daqpipe try to connect to it, otherwise it will fail with error message.

From the build directory :

```sh
./tools/monitor/monitor config.json
```

Run daqpipe
----------------

In another terminal, run daqpipe for 200 seconds :

```sh
export PATH=$PREFIX/bin:$PATH
daqpipe -f config.json -t 200

#or in place in the build dir :

./src/daqpipe -f config.json -t 200
```

You might see the monitor getting some data.

Running with the AMC40 emulator
-------------------------------------------

Setup the config file (config-amc40.json) :

```json
{
    "general": {
        "dataSource": "amc40"
    },
     "dataSource": {
        "amc40": {
            "lines": 1,
            "iface-rail1-1": "lo"
        }
        },
    "testing": {
        "dataChecking": false
    }
}
```

Run monitor like previously.

Run daqpipe with the two config files :

```sh
daqpipe -f config.json -f config-amc40.json -t 200
```

Run the amc40 emulator in another terminal (after launching daqpipe) :

```sh
./tools/amc40-emulator/amc40-emulator config-amc40.json
```

Tweak your own monitor.
-------------------------------

Go to the install director $PREFIX/share/daqpipe/monitor-example, then you can modify the main.cpp and build with make.

Documentation of the API is here at the end of the page : https://gitlab.cern.ch/svalat/lhcb-daqpipe-v2/blob/master/tools/monitor/Readme.md, or in the readme inside the directory.

If you play with the full pipeline (with amc40-emulator) the data contain text like "collision 20000" or "tfc 20000" so you can easily print them with :

```cpp
    DAQ::runDAQMonitor(argv[1],[&](DAQ::EventHandler & handler){
        cout << "Get event :" << handler.getEventId() << endl;
        cout << "     Data :" << handler.getData(0,0) << endl;
        cout << "      Tfc :" << handler.getData(0,1) << endl;
    });
```
