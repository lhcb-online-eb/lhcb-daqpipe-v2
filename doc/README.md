PLEASE DO NO EDIT THE DOC HERE
==============================

This directory is sync with gitlab wiki through ./dev/sync-doc-with-wiki.sh so
please do not edit the doc here it will be erased on the next update.

This directory is only here to keep the doc with the code in case of future migration
or loss of the main repos.
