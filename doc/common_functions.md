Common functions of library
===========================

Messages
--------

To print error messages with object formatting you can use the FormattedMessage class. It is used
as base class for Debug messages defined into common/Debug.h. Debug messages can be quicly displayed
with :

```cpp
	DAQ_FATAL("Describe error");
```

If you need arguments :

```cpp
	DAQ_FATAL_ARG("Describe error with arg %1 and %2").arg(x).arg(y).end();
```

Please do not forget the call to end() method to print the message and apply exit method (abort or raise exception...)

Message levels are :

- Debug
- Fatal
- Error
- Warning
- Message
- Info

Debug message also take a `category` name which can be filtered thanks to the `-v` option :

```cpp
	DAQ_DEBUG_ARG("mpi","Describe error with arg %1 and %2").arg(x).arg(y).end();
```

If you use some system functions which return errors through errno, you can format you message with : 

```cpp
	DAQ_DEBUG_ARG("system","Failed to open file %1, error : %2").arg(filename).argStrErrno().end();
```

And then enable at runtime :

```shell
	./daqpipe -v mpi,launcher
	./daqpipe -v all
	./daqpipe -v help
```

Caution, DEBUG message are only printed if you build the project in Debug mode (to not have the common NDEBUG definition from release mode) : 
```shell
../configure --enable-debug
#or
cmake .. -DCMAKE_BUILD_TYPE=Debug
```

Assert / assume
---------------

Assertions are disabled for production but DAQPIPE provide a second macro (assume and assumeArg) which
stay active but need an error message.

```cpp
	#include <common/Debug.h>

	assumeArg(ok == true,"Failed to get OK, get %1").arg(ok).end();
```

When checking commands from glibc, you can easily use the error message from errno with :

```cpp
	 #include <common/Debug.h>

	 FILE * fp = ....
	 assumeArg(fp != NULL,"Failed to open file %1 : %2").arg(file).argStrErrno().end();
```

Measuring time
--------------

To measure time, DAQPIPE provide the Timer class which can be use like this for example to
cumulate times :

```cpp
	#include <common/Timer.hpp>
	
	Timer timer;
	for (...)
	{
		timer.start();
		//stuff
		timer.stop();
		//stuff not measured
	}
	cout << timer.get() << endl;
```

You can also simply get time since start :

```cpp
	#include <common/Timer.hpp>
	
	Timer timer;
	timer.start();
	for (...)
	{
		//stuff
		if (timer.getElapsedTime() > TIMEOUT)
		{
			//stuff
			timer.flush();
		}
	}
```

Measuring throughput
--------------------

To measure throughput you can use the class Throughput :

```cpp
	#include <common/Throughput.hpp>
	
	Throughput throughput;
	for (...)
	{
		//stuff
		throughput.reg(messages,totalSize);
	}
	//print
	cout << "Throughput : " << throughput << endl;
	//reset
	throughput.restart();
```