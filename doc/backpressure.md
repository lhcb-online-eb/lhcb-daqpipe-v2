Back pressure
=============

DAQPIPE try to handle back pressure if one unit start to go slower in order to continue to operate safely.

There is two point where it is implements :

* In the Builder Unit. It append ff the buffer used between the receiving from RU and sending to FU start to become full. In this case, a fake segemnt is allocated and all the data going in will be lost. A warning is displayed to notify the user. We know we need to through away those data because they are identified by allocation ID : `DAQ_SKIP_ID`
* In the writer there could be backpressure due to final write to disk done in threads. In this case the implementation made the caller (the FU) waiting which will propagate the backpressure up to the BU.