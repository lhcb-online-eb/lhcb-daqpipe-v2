Intersting links and pointers
=============================

This page aims to store some of the interesting links and papers which are relevant to
the topic of this project.

About DAQ from other experiments
--------------------------------

 * Publication of ALICE DAQ team : http://aliceinfo.cern.ch/DAQ/public/publications-and-presentations
 * About ATLAS DAQ : http://www.atlas.ch/acquisition.html
 * Slides to check about ATLAS DAQ : http://hep.ps.uci.edu/~wclhc07/ATLAS%20TDAQ.pdf
 * TDR DAQ alice : https://cdsweb.cern.ch/record/2011297
 * Slides DAQ CMS : http://www.slideshare.net/AndreaPetrucci2/xdaqaplnlinfn27112014
 * Doc RU/BU DAQ CMS : http://cms-ru-builder.web.cern.ch/cms-ru-builder/RUBUILDER_G_V1_13_0.pdf
 * Paper from CMS : http://ieeexplore.ieee.org/xpl/articleDetails.jsp?arnumber=7097439&punumber%3D7086910%26filter%3DAND%28p_IS_Number%3A7097401%29%26pageNumber%3D2
 * Presentation from Niko : http://lhcb-doc.web.cern.ch/lhcb-doc/presentations/TalksNotDirectlyRelatedLHCb/Postcript/NikoNeufeld_rt2012.pptx

About networking
----------------

 * MPI implementations
   * OpenMPI : http://www.open-mpi.org/
   * MPICH : https://www.mpich.org/
   * MPC : http://mpc.paratools.com/. This is a CEA implementation of MPI/OpenMP which support MPI_THREAD_MULTIPLE. Might be interesting to test.

 * New API to programm QLogick PSM/verbs/tcp from OFA : OFI/libfabric : 
   * Main website (manpages, source : libfabric, demo : fabtests) : http://ofiwg.github.io/libfabric/
   * A presentation about intel : http://downloads.openfabrics.org/downloads/WorkGroups/ofiwg/F2F_Hillsboro_2014_0819/Intel-OFA-MPI2.pptx

 * Alternative to MPI:
   * GASPI : http://www.gaspi.de/projekt.html. Interesting as it provide RDMA base messaging and support IB.

About fault tolerance : 
-----------------------

 * In MPI : 
    * Paper ANL (2002) : from http://www.mcs.anl.gov/~lusk/papers/fault-tolerance.pdf
    * Recent work (2015) on top of mpich : http://www.mcs.anl.gov/publication/lessons-learned-implementing-user-level-failure-mitigation-mpich

About running on HPC clusters
-----------------------------

Job manager :
 * Slurm : https://computing.llnl.gov/linux/slurm/
 * PBS/Torque : http://www.arc.ox.ac.uk/content/pbs

Launcher libraries :
 * Hydra from mpich project : 
   * Some doc : https://wiki.mpich.org/mpich/index.php/Hydra_Process_Management_Framework
   * Usage as user : https://wiki.mpich.org/mpich/index.php/Using_the_Hydra_Process_Manager
   * Sources : inside mpich sources : https://www.mpich.org/
   * Check usage by another project (hydra/hdyra-simple) : http://mpc.paratools.com/

Tools to profile MPI applications
---------------------------------

 * Tau : http://www.cs.uoregon.edu/research/tau/users/samrai.php
 * Scalasca : http://www.scalasca.org/
 * Vampir : https://www.vampir.eu/
 * Profiling tool community for HPC : http://www.vi-hps.org/
