RapidIO
=======

RapdIO had some contrains which forced us to tweak a little bit daqpipe, this page contain some notes about this support. 

Major constrain
--------------------

The major constrains for daqpipe on rapidio were : 

 * The driver only permit to allocate up to 2MB for RDMA segments.
 * The rdma segments need to be allocated by the driver (for contiguous memory) and not registered to the driver as for IB/OPA.
 * The number of RDMA segments were limited to 8 (some patches are in progress in the driver to fix this, we wait to test it)

Benchmarking
------------------

Due to lower number of nodes available at developpement time and the specific constrains the rapidio benchmarking has been done with some specific scripts in [benchmarking/rapidio](https://gitlab.cern.ch/svalat/lhcb-daqpipe-v2/blob/master/benchmarking/rapidio). 

```sh
multisegm_prep.py:
#       credits == 1, because BU allocates credits x units number of segments
#                       Otherwise, credits is the number of events handled at the same time by the SW
```

```sh
normal_prep.py:
# Parameters:
#       eventRailMaxDataSize
#               (max size of BU buffer which is a RX buf)
#               eventRailMaxDataSize x units <= 2M
#               units = nodes - 1, more precisely the number of readout units but in the implementation the number of builder units are the same as the number readout units.
#               -> nodes = 3 -> units == 2 -> eventRailMaxDataSize = 1M
#               -> nodes = 4 -> units == 3 -> eventRailMaxDataSize = 682*1024 = 698368
#       parallell 
#               (TODO describe this) Do this with only the largest buffer size
#               Number of outgoig requests per credit per builder unit
#       ruStoredEvents
#               Decides the size of the transmission buffer. How many (parts of events) we should be able to store from
#               the PCIe cards. Not a parameter per se, just needs to be large enough otherwise we don't have enough to feed
#               the builder units:
#               ruStoredEvents >= credits x units
#               Max credits = 4, max units = 3 -> 12
```
Some notes and information
-----------------------------------

   * An event consists of several segments, currently 2 for data and 2 for metadata
   * The number of segments depend mainly on the hardware design and is decided at compile time
   * Today, there are  two PCIe boards feeding data into the PC on two different memory areas -> 2 x 2 segments
   * The metadata decides how many collisions are included in one event (this was decided in "config/board"?)
   * Each event may be of different size
   * Today, with the hardware trigger, the sorting of samples into specific collions is done at the triggering level. With the upgrade, this will be done at a later stage (after DAQPIPE)

More informations
-----------------------

You can find a full log of the working progress on https://twiki.cern.ch/twiki/bin/view/Openlab/RapidIODAQPIPE