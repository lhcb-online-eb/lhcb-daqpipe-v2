/*****************************************************
             PROJECT  : lhcb-daqpipe
             VERSION  : 2.0.0
             DATE     : 12/2017
             AUTHOR   : Valat Sébastien - CERN
                      : Xiong Jianxin - INTEL
             LICENSE  : CeCILL-C
*****************************************************/

/********************  HEADERS  *********************/
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include <time.h>
#include <string.h>
#include <rdma/fabric.h>
#include <rdma/fi_domain.h>
#include <rdma/fi_endpoint.h>
#include <rdma/fi_cm.h>
#include <rdma/fi_errno.h>

/********************  MACROS  **********************/
#define BUFFER_SIZE (10UL*1024UL*1024UL)
#define REPEAT 10000
#define CREDITS 2
#define MPI_MASTER 0
#define CMD_SIZE 64

#define bool	int
#define true	1
#define false	0

int nodes;
int rank;
unsigned long count = 0;

/******************** OFI ***************************/
struct fid_fabric *fabric;
struct fid_domain *domain;
struct fid_cq *cq;
struct fid_av *av;
struct fid_ep *ep;

struct my_addr { char raw[16]; } my_addr, *all_addrs;
size_t my_addr_len = sizeof(struct my_addr);
fi_addr_t *direct_addrs;

init_ofi(void)
{
	int err;
	struct fi_info *hints, *fi;
	struct fi_cq_attr cq_attr;
	struct fi_av_attr av_attr;

	memset(&cq_attr, 0, sizeof(cq_attr));
	memset(&av_attr, 0, sizeof(av_attr));

	direct_addrs = calloc(nodes, sizeof(fi_addr_t));
	all_addrs = calloc(nodes, sizeof (struct my_addr));
	if (!direct_addrs || !all_addrs) {
		printf("calloc failed\n");
		exit(1);
	}
	hints = fi_allocinfo();
	if (!hints) {
		printf("fi_allocinfo failed\n");
		exit(1);
	}
	hints->caps = FI_MSG;
	hints->mode = FI_CONTEXT;
	hints->ep_attr->type = FI_EP_RDM;
	err = fi_getinfo(FI_VERSION(1,0), NULL, NULL, 0, hints, &fi);
	if (err) {
		printf("fi_getinfo returns err %d\n", err);
		exit(1);
	}
	err = fi_fabric(fi->fabric_attr, &fabric, NULL);
	if (err) {
		printf("fi_fabric returns err %d\n", err);
		exit(1);
	}
	err = fi_domain(fabric, fi, &domain, NULL);
	if (err) {
		printf("fi_domain returns err %d\n", err);
		exit(1);
	}
	cq_attr.format = FI_CQ_FORMAT_CONTEXT;
	cq_attr.size = 100;
	err = fi_cq_open(domain, &cq_attr, &cq, NULL);
	if (err) {
		printf("fi_cq_open returns err %d\n", err);
		exit(1);
	}
	err = fi_endpoint(domain, fi, &ep, NULL);
	if (err) {
		printf("fi_endpoint returns err %d\n", err);
		exit(1);
	}
	err = fi_ep_bind(ep, (struct fid *)cq, FI_RECV | FI_SEND);
	if (err) {
		printf("fi_ep_bind returns err %d\n", err);
		exit(1);
	}
	av_attr.type = FI_AV_MAP;
	err = fi_av_open(domain, &av_attr, &av, NULL);
	if (err) {
		printf("fi_av_open returns err %d\n", err);
		exit(1);
	}
	err = fi_getname((struct fid *)ep, &my_addr, &my_addr_len);
	if (err) {
		printf("fi_getname returns err %d\n", err);
		exit(1);
	}

	MPI_Allgather(&my_addr, my_addr_len, MPI_CHAR,
			all_addrs, my_addr_len, MPI_CHAR,
			MPI_COMM_WORLD);

	err = fi_av_insert(av, all_addrs, nodes, direct_addrs, 0, NULL);
	if (err != nodes) {
		printf("fi_av_insert returns err %d instead of %d\n", err, nodes);
		exit(1);
	}

	fi_freeinfo(hints);
	fi_freeinfo(fi);

	return 0;
}

void finalize_ofi(void)
{
	fi_close((fid_t)ep);
	fi_close((fid_t)av);
	fi_close((fid_t)cq);
	fi_close((fid_t)domain);
	fi_close((fid_t)fabric);
}

/*******************  FUNCTION  *********************/
void isend ( void* buffer, size_t size, int target, int tag )
{
	struct fi_context *context = malloc(sizeof (struct fi_context));
	fi_send(ep,buffer,size,NULL,direct_addrs[target],context);
	count++;
}

/*******************  FUNCTION  *********************/
void irecv ( void* buffer, size_t size )
{
	struct fi_context *context = malloc(sizeof (struct fi_context));
	fi_recv(ep,buffer,size,NULL,FI_ADDR_UNSPEC,context);
	count++;
}

/*******************  FUNCTION  *********************/
void waitAll ()
{
	int i, n;
	struct fi_cq_entry cqe;

	while (count) {
		n = fi_cq_read(cq,&cqe,1);
		if (n == -FI_EAGAIN)
			continue;
		if (n < 0) {
			printf("fi_cq_read returns err %d\n", n);
			exit(1);
		}
		count -= n;
		free(cqe.op_context);
	}
}

/*******************  FUNCTION  *********************/
char *getHostname()
{
	static char buffer[1024];
	gethostname(buffer,sizeof(buffer));
	return buffer;
}

/*******************  FUNCTION  *********************/
void runReadoutUnit(int rank,int nodes)
{
	int i,j;
	
	//allocate memory
	char cmd[CMD_SIZE];
	char * buffer = malloc(BUFFER_SIZE);
	
	//info
	printf("Rank %d on %s is RU\n",rank,getHostname());

	//loops on steps
	for (i = 0 ; i < REPEAT ; i++)
		for (j = 1 ; j < nodes ; j += 2)
		{
			irecv(cmd,CMD_SIZE);
			isend(buffer,BUFFER_SIZE,j,j);
		}
	waitAll();
	
	//free mem
	free(buffer);
}

/*******************  FUNCTION  *********************/
void runBuilderUnit(int rank,int nodes)
{
	int i;

	//allocate memory
	char cmd[CMD_SIZE];
	char * buffer = malloc(BUFFER_SIZE);

	//info
	printf("Rank %d on %s is BU\n",rank,getHostname());

	//need to receive all
	for (i = 0 ; i < REPEAT * nodes/2 ; i++)
	{
		isend(cmd,CMD_SIZE,(i*2)%nodes,(i*2)%nodes);
		irecv(buffer,BUFFER_SIZE);
	}
	waitAll();

	//free mem
	free(buffer);
}

/*******************  FUNCTION  *********************/
int main(int argc, char ** argv)
{
	struct timespec tstart, tend;
	long long int timeDiff;
	float elapsed;

	//setup MPI
	MPI_Init(&argc,&argv);
	
	//get info
	MPI_Comm_rank(MPI_COMM_WORLD,&rank);
	MPI_Comm_size(MPI_COMM_WORLD,&nodes);
	
	//print
	if (rank == MPI_MASTER)
	{
		printf("Try round robin gather on %d nodes\n",nodes);
		printf("Use : \n  - buffer size : %lu\n  - repeat : %d\n  - credits : %d  - onfly requests : unlimited\n",BUFFER_SIZE,REPEAT,CREDITS);
		if (nodes % 2 != 0)
		{
			fprintf(stderr,"You need a world size multiple of 2\n");
			MPI_Abort(MPI_COMM_WORLD,1);
		}
	}
	
	init_ofi();

	//wait all
	MPI_Barrier(MPI_COMM_WORLD);
	
	//get start time
	clock_gettime(CLOCK_REALTIME, &tstart);
	
	//run
	if (rank % 2 == 0)
		runReadoutUnit(rank,nodes);
	else
		runBuilderUnit(rank,nodes);
	
	//get end time
	clock_gettime(CLOCK_REALTIME, &tend);
	
	//get elapsed
	timeDiff = (tend.tv_sec - tstart.tv_sec) * 1000000000 + (tend.tv_nsec - tstart.tv_nsec);
	elapsed = timeDiff / 1000000000.0f;
	
	//print rate
	printf("Rank %d as rate : %f.2 Gb/s\n",rank,(float)((8*BUFFER_SIZE*(long)REPEAT*nodes/2)/(1024UL*1024UL*1024UL))/elapsed);
	
	finalize_ofi();

	//finish MPI
	MPI_Finalize();
	
	return EXIT_SUCCESS;
}
