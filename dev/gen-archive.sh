#!/bin/bash
######################################################
#            PROJECT  : lhcb-daqpipe                 #
#            VERSION  : 2.5.0-dev                    #
#            DATE     : 12/2017                      #
#            AUTHOR   : Valat Sébastien  - CERN      #
#            LICENSE  : CeCILL-C                     #
######################################################

######################################################
#This script aimed at generating archive to distribute
#the project. Simply call it from root directory without
#any agruments.

######################################################
#extract version
version=2.5.0-dev
prefix=lhcb-daqpipe-${version}

######################################################
echo "Generate ${prefix}.tar.gz..."
git archive --format=tar --prefix=${prefix}/ HEAD | gzip > ${prefix}.tar.gz
echo "Finished"
