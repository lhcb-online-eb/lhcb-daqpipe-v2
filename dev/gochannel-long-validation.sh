######################################################
#            PROJECT  : lhcb-daqpipe
#            DATE     : 12/2017
#            AUTHOR   : Sébastien Valat - CERN
######################################################
#!/bin/bash

######################################################
#This script aimed at running the go tests in loop
#infinitly (for example for a night) to ensure that
#there is no issues with the locks (no deadlock of
#missprotection of values).
#ARGS :
# - $1 : path to root build directory (./build)
# - $2 : Optional, number of repetition into TestGoChannelMT, use ~100-400 for long test (~15min - 1h).

######################################################
if [ -z "$1" ]; then
	echo "You need to provide path to build directory as parameter" 1>&2
	exit 1
fi

######################################################
BUILD_DIR=$1
LONG=1

######################################################
if [ ! -z "$2" ]; then
	LONG=$2
fi

######################################################
set -e
set -x

######################################################
while :
do
	make -C $BUILD_DIR/src/gochannels/tests test
	for th in 1 2 4 8 16 32 64
	do
		$BUILD_DIR/src/gochannels/tests/TestGoChannelMT $th $LONG
	done
done
