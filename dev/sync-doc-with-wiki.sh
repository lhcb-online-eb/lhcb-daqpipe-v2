#!/bin/bash
######################################################
#            PROJECT  : lhcb-daqpipe                 #
#            VERSION  : 2.5.0-dev                    #
#            DATE     : 12/2017                      #
#            AUTHOR   : Valat Sébastien  - CERN      #
#            LICENSE  : CeCILL-C                     #
######################################################

######################################################
#Download the doc from the wiki to sync it

######################################################
PROJECT_NAME="lhcb-daqpipe-v2"
#PROJECT_URL="https://:@gitlab.cern.ch:8443/lhcb-daqpipe/lhcb-daqpipe-v2.wiki.git"
PROJECT_URL="ssh://git@gitlab.cern.ch:7999/lhcb-daqpipe/lhcb-daqpipe-v2.wiki.git"

######################################################
if [ ! -d ./dev ] || [ ! -f configure ]; then
	echo "This script must be executed from the project root directory !" 1>&2
	exit 1
fi

######################################################
set -e
set -x

######################################################
#clone the wiki repository
if [ -d ${PROJECT_NAME}.wiki ]; then
	cd ${PROJECT_NAME}.wiki
	git pull
else
	git clone ${PROJECT_URL}
	cd ${PROJECT_NAME}.wiki
fi

######################################################
#copy all the files in doc
git archive --format=tar HEAD > all.tar
tar -xf all.tar -C ../doc

######################################################
#replace linkgs
for file in ../doc/*.markdown
do
	name=$(basename $file | sed -e 's/\.markdown//g')
	sed -i "s/(${name})/(${name}.markdown)/g" ../doc/*.markdown
done

######################################################
#cleanup
cd ..
rm -rfd ${PROJECT_NAME}.wiki
