#!/bin/bash

echo "You might have login onto gitlab registry before doing this : docker login gitlab-registry.cern.ch"

docker build -t gitlab-registry.cern.ch/lhcb-daqpipe/lhcb-daqpipe-v2 .
docker push gitlab-registry.cern.ch/lhcb-daqpipe/lhcb-daqpipe-v2
