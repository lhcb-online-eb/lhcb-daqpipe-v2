#!/bin/bash
######################################################
#            PROJECT  : lhcb-daqpipe                 #
#            VERSION  : 2.5.0-dev                    #
#            DATE     : 12/2017                      #
#            AUTHOR   : Valat Sébastien  - CERN      #
#            LICENSE  : CeCILL-C                     #
######################################################

set -e
set -x

mkdir -p "${HOME}/.kde4/share/apps/ktexteditor_snippets/data"
cp "kde-snippets-lhcb-daqpipe.xml" "${HOME}/.kde4/share/apps/ktexteditor_snippets/data"
