#!/bin/bash
######################################################
#            PROJECT  : lhcb-daqpipe                 #
#            VERSION  : 2.5.0-dev                    #
#            DATE     : 12/2017                      #
#            AUTHOR   : Valat Sébastien  - CERN      #
#            LICENSE  : CeCILL-C                     #
######################################################

######################################################
#This run all the things to be run into jenkins, it avoid
#to put all the knowledge in the jenkins itself so the
#shell script on the jenkins side only call this script.

######################################################
SOURCE_DIR=$PWD
MODE=$1
TESTFAIL=$2
SKIP_DEP_PREFIX=$3

######################################################
set -e
set -x

######################################################
SOURCE_DIR=$PWD
if [ "$3" = "skip" ]; then
	DEP_PREFIX=/usr
else
	DEP_PREFIX=$PWD/deps-prefix/usr
	export PATH="$DEP_PREFIX/bin:$PATH"
	export LD_LIBRARY_PATH="$DEP_PREFIX/lib64:$DEP_PREFIX/lib:$LD_LIBRARY_PATH"
fi

######################################################
if [ -z "$MODE" ]; then
	MODE=LOCAL
fi

######################################################
case $MODE in
	'LOCAL')
		BUILD_OPTIONS="--with-transport=LOCAL"
		;;
	'MPI')
		BUILD_OPTIONS="--with-transport=MPI"
		;;
	'LIBFABRIC')
		BUILD_OPTIONS="--with-transport=LIBFABRIC --with-libfabric=${DEP_PREFIX} --with-hydra=${DEP_PREFIX} --with-hydra-mpiexec=hydra-mpiexec"
		;;
	'TCP')
		BUILD_OPTIONS="--with-transport=TCP --with-hydra=${DEP_PREFIX} --with-hydra-mpiexec=hydra-mpiexec"
		;;
	'UDP')
		BUILD_OPTIONS="--with-transport=UDP --with-hydra=${DEP_PREFIX} --with-hydra-mpiexec=hydra-mpiexec"
		;;
	'VERBS')
		BUILD_OPTIONS="--with-transport=VERBS --with-verbs=${DEP_PREFIX} --with-hydra=${DEP_PREFIX} --with-hydra-mpiexec=hydra-mpiexec"
		;;
	*)
		echo "Invalid mode : $MODE" 1>&2
		exit 1
		;;
esac

######################################################
rm -rfd build test-install
mkdir build
cd build
../configure --enable-jenkins --prefix=${SOURCE_DIR}/test-install --with-hwloc=${DEP_PREFIX} --with-htopml=${DEP_PREFIX} ${BUILD_OPTIONS}
make
if [ "$TESTFAIL" = "testfail" ]; then
	make test
else
	make test || echo "Has failure"
fi
make install

######################################################
#cd ..
#cppcheck --enable=all --report-progress --std=c++11 --inline-suppr --xml ./src 2> lhcb-daqpipe-v2.cppcheck.xml
