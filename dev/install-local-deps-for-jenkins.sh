#!/bin/bash
######################################################
#            PROJECT  : lhcb-daqpipe                 #
#            VERSION  : 2.5.0-dev                    #
#            DATE     : 12/2017                      #
#            AUTHOR   : Valat Sébastien  - CERN      #
#            LICENSE  : CeCILL-C                     #
######################################################

######################################################
#This is to download and install the test dependencies localy, mostly to run into CERN jenkins instance
#as we cannot install those packaging with the system (not root).

######################################################
set -x
set -e

######################################################
SUBDIR="deps-prefix"
PREFIX="${PWD}/${SUBDIR}/usr"

######################################################
export PATH="${PREFIX}/bin:${PATH}"
export LD_LIBRARY_PATH="${PREFIX}/lib:${LD_LIBRARY_PATH}"

######################################################
#setup dir
if [ -d /opt/lhcb-daqpipe-deps ]; then
	echo "Used /opt/lhcb-daqpipe-deps"
	mkdir -p ${SUBDIR}
	ln -sf /opt/lhcb-daqpipe-deps $PREFIX
	exit 0
else
	mkdir -p "${SUBDIR}/usr"
	mkdir -p "${PREFIX}/installed"
	cd "${SUBDIR}"
fi

######################################################
#download function
function download()
{
	URL="$1"
	FNAME="$2"

	if [ ! -f "$FNAME" ]; then
		wget -O "$FNAME" "$URL"
	fi
}

######################################################
#download files
which mpicxx || download "http://www.open-mpi.org/software/ompi/v1.10/downloads/openmpi-1.10.0.tar.bz2" "openmpi-1.10.0.tar.bz2"
which valgrind || download "http://valgrind.org/downloads/valgrind-3.10.1.tar.bz2" "valgrind-3.10.1.tar.bz2"
which cppcheck || download "http://downloads.sourceforge.net/project/cppcheck/cppcheck/1.69/cppcheck-1.69.tar.bz2?r=http%3A%2F%2Fsourceforge.net%2Fprojects%2Fcppcheck%2F&ts=1441206728&use_mirror=kent" "cppcheck-1.69.tar.bz2" 
which automake-1.13 || download "http://ftp.gnu.org/gnu/automake/automake-1.13.4.tar.gz" "automake-1.13.4.tar.gz"
which autoconf || download "http://ftp.gnu.org/gnu/autoconf/autoconf-2.69.tar.gz" "autoconf-2.69.tar.gz"
which libtool || download "http://ftpmirror.gnu.org/libtool/libtool-2.4.2.tar.gz" "libtool-2.4.2.tar.gz"
download "https://github.com/svalat/htopml/archive/v0.1.0.tar.gz" "htopml-0.1.0.tar.gz"
download "https://www.open-mpi.org/software/hwloc/v1.11/downloads/hwloc-1.11.2.tar.bz2" "hwloc-1.11.2.tar.bz2"
download "https://github.com/ofiwg/libfabric/archive/v1.1.0.tar.gz" "libfabric-1.1.0.tar.gz"
download "http://www.mpich.org/static/downloads/3.1.4/hydra-3.1.4.tar.gz" "hydra-3.1.4.tar.gz"
download "http://trac.mpich.org/projects/openpa/raw-attachment/wiki/Downloads/openpa-1.0.4.tar.gz" "openpa-1.0.4.tar.gz"

######################################################
#helper function
function buildAutotools()
{
	PACKAGE_FILE=$1
	PACKAGE_SUBDIR=$2

	if [ ! -f "${PREFIX}/installed/${PACKAGE_FILE}.installed" ]; then
		rm -rfd ${PACKAGE_SUBDIR}
		tar -xf ${PACKAGE_FILE}
		pushd ${PACKAGE_SUBDIR}
		if [ ! -f configure ] && [ -f autogen.sh ]; then
			./autogen.sh
		fi
		./configure --prefix="${PREFIX}"
		make -j8
		make install
		touch "${PREFIX}/installed/${PACKAGE_FILE}.installed"
		popd
		rm -rfd ${PACKAGE_SUBDIR}
	fi
}

######################################################
#helper function
function buildPatchedAutotools()
{
	PACKAGE_PATCH=$1
	PACKAGE_FILE=$2
	PACKAGE_SUBDIR=$3
	shift 3

	if [ ! -f "${PREFIX}/installed/${PACKAGE_FILE}.installed" ]; then
		rm -rfd ${PACKAGE_SUBDIR}
		tar -xf ${PACKAGE_FILE}
		pushd ${PACKAGE_SUBDIR}
		patch -p1 < ../${PACKAGE_PATCH}
		#if [ ! -f configure ] && [ -f autogen.sh ]; then
			./autogen.sh
		#fi
		./configure --prefix="${PREFIX}" "$@"
		make -j8
		make install
		touch "${PREFIX}/installed/${PACKAGE_FILE}.installed"
		popd
		rm -rfd ${PACKAGE_SUBDIR}
	fi
}

######################################################
#helper function
function buildCmake()
{
	PACKAGE_FILE=$1
	PACKAGE_SUBDIR=$2

	if [ ! -f "${PREFIX}/installed/${PACKAGE_FILE}.installed" ]; then
		rm -rfd ${PACKAGE_SUBDIR}
		tar -xf ${PACKAGE_FILE}
		mkdir ${PACKAGE_SUBDIR}/build
		pushd ${PACKAGE_SUBDIR}/build
		cmake .. -DCMAKE_INSTALL_PREFIX="${PREFIX}"
		make -j8
		make install
		touch "${PREFIX}/installed/${PACKAGE_FILE}.installed"
		popd
		rm -rfd ${PACKAGE_SUBDIR}
	fi
}

######################################################
which mpicxx || buildAutotools "openmpi-1.10.0.tar.bz2" "openmpi-1.10.0"
which valgrind || buildAutotools "valgrind-3.10.1.tar.bz2" "valgrind-3.10.1"
which automake-1.13 || buildAutotools "automake-1.13.4.tar.gz" "automake-1.13.4"
which autoconf || buildAutotools "autoconf-2.69.tar.gz" "autoconf-2.69"
which libtool || buildAutotools "libtool-2.4.2.tar.gz" "libtool-2.4.2"
which cppcheck || buildCmake "cppcheck-1.69.tar.bz2" "cppcheck-1.69"
buildAutotools "libfabric-1.1.0.tar.gz" "libfabric-1.1.0"
buildPatchedAutotools "../extern-deps/hydra-3.1.4.patch" "hydra-3.1.4.tar.gz" "hydra-3.1.4" "--program-prefix=hydra-"
buildAutotools "openpa-1.0.4.tar.gz" "openpa-1.0.4"
buildCmake "htopml-0.1.0.tar.gz" "htopml-0.1.0"
buildAutotools "hwloc-1.11.2.tar.gz" "hwloc-1.11.2"
